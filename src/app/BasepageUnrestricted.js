import React, { Suspense } from "react";
import { Redirect, Switch } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";
import SearchPage from "./pages/SearchPage";
import SignUp from "./pages/signup";
import Search from "./pages/Search";
import MailSignUp from "./pages/MailSignUp";
import CorporateSolutions from "./pages/CorporateSolutions";
import Volunteer from "./pages/Volunteer";
import Home from "./pages/Home";
import AboutUs from "./pages/AboutUs";
import ContactUs from "./pages/ContactUs";
import HowItWorks from "./pages/HowItWorks";
import { Layout } from "../_metronic/layout";

export default function BasepageUnrestricted() {
    return (
        <Suspense fallback={<LayoutSplashScreen />}>
            <Switch>
                <Layout>
                    <ContentRoute path="/home" component={Home} />
                    <ContentRoute path="/search" component={SearchPage} />
                    <ContentRoute path="/signup" component={SignUp} />
                    <ContentRoute path="/searchtab" component={Search} />
                    <ContentRoute path="/help" component={Volunteer} />
                    <ContentRoute path="/mailSignUp" component={MailSignUp} />
                    <ContentRoute path="/corporateSolutions" component={CorporateSolutions} />
                    <ContentRoute path="/AboutUs" component={AboutUs} />
                    <ContentRoute path="/ContactUs" component={ContactUs} />
                    <ContentRoute path="/HowitWorks" component={HowItWorks} />
                    {/* <Redirect to="error/error-v1" /> */}
                </Layout>
            </Switch>
        </Suspense>
    );
}
