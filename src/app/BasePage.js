import React, { Suspense, lazy } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";
import { BuilderPage } from "./pages/BuilderPage";
import MyPage from "./pages/MyPage";
import SearchPage from "./pages/SearchPage";
import Task from "./pages/task";
// import ProjectPage from "./pages/Project";
import DonationPage from "./pages/Donations";
import PeoplePage from "./pages/People";
import UserLogin from "./pages/UserLogin";
import NGOLogin from "./pages/NGOlogin";
import CorporateLogin from "./pages/CorporateLogin";
import TermsandConditions from "./pages/tnc";
import Secret from "./pages/Secret";
import faq from "./pages/faq";
import CSR from "./pages/CSR";
import NGOList from "./pages/NGOList";
import Test from "./pages/test";
import EditMilestone from "./pages/editMilestone";
import CloseMilestone from "./pages/closeMilestone";
import SurveyForm from "./components/Form";
import ReportForm from "./components/ReportForm";
import MyDonation from "./pages/MyDonation";
import SignUp from "./pages/signup";
import ViewProfile from "./pages/ViewProfile";
import MyProfile from "./pages/MyProfile";
import MyNetwork from "./pages/MyNetwork";
import Settings from "./pages/Settings";
import InviteFriends from "./pages/Invite Friends";
import MyBeneficiaries from "./pages/MyBeneficiaries";
import TaskDetails from "./pages/TaskDetails";
import TaskDetailsUser from "./pages/TaskDetailsUser";
import Timeline from "./pages/Timeline";
import VerticalLinearStepper from "./pages/TimelineStepper";
import Search from "./pages/Search";
// import MilestoneDetail from "./pages/MilestoneDetail";
import SearchBeneficiary from "./pages/searchBeneficiary";
// import { DashboardPage } from "./pages/DashboardPage";
// import BlogPage from "./pages/BlogPage";
import CreateProjects from "./pages/CreateProject";
import getProjects from "./pages/getProjects";
import CreateDonationDrive from "./pages/createDonationDrive";
import CreateTaskOption from "./pages/create-task";
import CreateTask from "./pages/createTask";
import MyTasks from "./pages/myTasks";
import EditProfilePage from "./pages/EditProfilePage";
import AddBeneficiary from "./pages/AddBeneficiary";
import UpdateBeneficiary from "./pages/UpdateBeneficiary";
import MailSignUp from "./pages/MailSignUp";
import Notification from "./pages/Notifications";
import CorporateSolutions from "./pages/CorporateSolutions";
import Volunteer from "./pages/Volunteer";
// import Help from "./pages/Help";
import DonationDashboard from "./pages/DonationDashboard";
import SurveyTemplate from "./pages/SurveyTemplate";
import HideAppBar from "./pages/Scroll";
import MediaContact from "./pages/MediaContact";
import Home from "./pages/Home";
import CreateReport from "./pages/createReport";
import Testing from "./pages/testing";
import EditTask from "./pages/editTask";
import Survey from "./pages/Survey";
import VolunteeringCertificates from "./pages/VolunteeringCertificates";
import PopularQuestionTags from "./components/PopularQuestionTags";
import SurveyFormsList from "./pages/SurveyFormsList";
import SurveyMainPage from "./pages/SurveyMainPage";
// import SurveyFormFill from "./pages/SurveyFormFill";
import SurveyFill from "./components/SurveyFill";
import UserProfile from "./pages/UserProfile";
import MyProjects from "./pages/MyProjects"
import { FeedPage } from "./pages/FeedPage";

import AboutUs from "./pages/AboutUs";
import ContactUs from "./pages/ContactUs";
import HowItWorks from "./pages/HowItWorks";
import EditReport from "./pages/EditReport";
import DonationReports from "./pages/DonationReports"
import EditProfileUser from "./pages/editProfileUser";
import MyAchievements from "./pages/MyAchievements"
import ecommerce_home from "./pages/e-commerce/home";
import ecommerce_product from "./pages/e-commerce/product";
import ecommerce_cart from "./pages/e-commerce/shoppingCart";
import ecommerce_search from "./pages/e-commerce/searchProducts";
import ecommerce_orders from "./pages/e-commerce/orderPage";
import EditProject from "./pages/EditProject";
import ngoregistration from "./pages/ngo-registration";
import DonationDetailPage from "./pages/DonationDetailPage"
const GoogleMaterialPage = lazy(() =>
  import("./modules/GoogleMaterialExamples/GoogleMaterialPage")
);
const ReactBootstrapPage = lazy(() =>
  import("./modules/ReactBootstrapExamples/ReactBootstrapPage")
);
const ECommercePage = lazy(() => import("./modules/ECommerce/pages/eCommercePage"));
const UserProfilepage = lazy(() => import("./modules/UserProfile/UserProfilePage"));

export default function BasePage() {
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/signup" />
        }
        {/* <ContentRoute path="/dashboard" component={DashboardPage} /> */}
        <ContentRoute path="/search" component={SearchPage} />
        {/* <ContentRoute path="/project" component={ProjectPage} /> */}
        <ContentRoute path="/task" component={Task} />
        <ContentRoute path="/media" component={MediaContact} />
        <ContentRoute path="/scroll" component={HideAppBar} />
        <ContentRoute path="/survey_template" component={SurveyTemplate} />
        <ContentRoute path="/donationdashboard" component={DonationDashboard} />
        <ContentRoute path="/timeline" component={Timeline} />
        <ContentRoute path="/timeline-stepper" component={VerticalLinearStepper} />
        <ContentRoute path="/settings" component={Settings} />
        <ContentRoute path="/edit-milestone" component={EditMilestone} />
        <ContentRoute path="/close-milestone" component={CloseMilestone} />
        <ContentRoute path="/ngolist" component={NGOList} />
        <ContentRoute path="/signup" component={SignUp} />
        {/* <ContentRoute path="/feed" component={RecipeReviewCard} /> */}
        <ContentRoute path="/feed" component={FeedPage} />
        <ContentRoute path="/test" component={Test} />
        <ContentRoute path="/userprofile" component={UserProfile} />
        <ContentRoute path="/csr" component={CSR} />
        <ContentRoute path="/searchtab" component={Search} />
        <ContentRoute path="/surveyform" component={SurveyFill} />
        <ContentRoute path="/help" component={Volunteer} />
        <ContentRoute path="/task-detail" component={TaskDetails} />
        <ContentRoute path="/task-detail-user" component={TaskDetailsUser} />

        <ContentRoute path="/network" component={MyNetwork} />
        <ContentRoute path="/my_donation" component={MyDonation} />
        <ContentRoute path="/Secret" component={Secret} />
        <ContentRoute path="/invite" component={InviteFriends} />
        <ContentRoute path="/form" component={SurveyForm} />
        <ContentRoute path="/beneficiaries" component={MyBeneficiaries} />
        <ContentRoute path="/report-form" component={ReportForm} />
        <ContentRoute path="/faq" component={faq} />
        {/* <ContentRoute path="/donation-details" component={DonationDetails} /> */}
        <ContentRoute path="/invite" component={InviteFriends} />
        <ContentRoute path="/ngo" component={ViewProfile} />

        <ContentRoute path="/terms-and-conditions" component={TermsandConditions} />
        {/* <ContentRoute path="/corporatelogin" component={CorporateLogin} /> */}
        {/* <ContentRoute path="/donation" component={DonationPage} /> */}
        {/* <ContentRoute path="/people" component={PeoplePage} /> */}
        <ContentRoute path="/userlogin" component={UserLogin} />
        <ContentRoute path="/ngologin" component={NGOLogin} />
        <ContentRoute path="/terms-and-conditions" component={TermsandConditions} />
        <ContentRoute path="/corporatelogin" component={CorporateLogin} />
        <ContentRoute path="/donation" component={DonationPage} />
        <ContentRoute path="/people" component={PeoplePage} />
        <ContentRoute path="/my-profile" component={MyProfile} />
        <ContentRoute path="/builder" component={BuilderPage} />
        <ContentRoute path="/createProjects" component={CreateProjects} />
        <ContentRoute path="/getProjects" component={getProjects} />
        <ContentRoute path="/createDonationDrive" component={CreateDonationDrive} />
        <ContentRoute path="/create-task" component={CreateTaskOption} />
        <ContentRoute path="/createTask" component={CreateTask} />
        <ContentRoute path="/myTasks" component={MyTasks} />
        <ContentRoute path="/editProfilePage" component={EditProfilePage} />
        <ContentRoute path="/addBeneficiary" component={AddBeneficiary} />
        <ContentRoute path="/updateBeneficiary" component={UpdateBeneficiary} />
        <ContentRoute path="/mailSignUp" component={MailSignUp} />
        <ContentRoute path="/getNotification" component={Notification} />

        <ContentRoute path="/corporateSolutions" component={CorporateSolutions} />

        <ContentRoute path="/home" component={Home} />
        <ContentRoute path="/createReport" component={CreateReport} />
        <ContentRoute path="/testing" component={Testing} />
        <ContentRoute path="/editTask" component={EditTask} />
        <ContentRoute path="/survey" component={Survey} />
        <ContentRoute path="/popularQuestionTags" component={PopularQuestionTags} />
        <ContentRoute path="/surveyFormList" component={SurveyFormsList} />
        <ContentRoute path="/surveyMainPage" component={SurveyMainPage} />
        <ContentRoute path="/AboutUs" component={AboutUs} />
        <ContentRoute path="/ContactUs" component={ContactUs} />
        <ContentRoute path="/HowitWorks" component={HowItWorks} />
        <ContentRoute path="/searchBeneficiary" component={SearchBeneficiary} />
        <ContentRoute path="/editReport" component={EditReport} />
        <ContentRoute path="/editProfileUser" component={EditProfileUser} />
        <ContentRoute path="/myAchievements" component={MyAchievements} />
        <ContentRoute path="/e-commerce/home" component={ecommerce_home} />
        <ContentRoute path="/e-commerce/product" component={ecommerce_product} />
        <ContentRoute path="/e-commerce/cart" component={ecommerce_cart} />
        <ContentRoute path="/e-commerce/search" component={ecommerce_search} />
        <ContentRoute path="/e-commerce/orders" component={ecommerce_orders} />
        <ContentRoute path="/editProject" component={EditProject} />
        <ContentRoute path="/ngo-registration" component={ngoregistration} />
        <ContentRoute path="/view-certificates" component={VolunteeringCertificates} />
        <ContentRoute path="/myProjects" component={MyProjects} />
        <ContentRoute path="/DonationReports" component={DonationReports} />
        <ContentRoute path="/DonationDetailPage" component={DonationDetailPage} />
        <ContentRoute path="/me" component={MyPage} />

        <Route path="/google-material" component={GoogleMaterialPage} />
        <Route path="/react-bootstrap" component={ReactBootstrapPage} />
        <Route path="/e-commerce" component={ECommercePage} />
        <Route path="/user-profile" component={UserProfilepage} />
        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
}
