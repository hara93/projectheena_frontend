import React from 'react';
// import './SearchPage.css';
import * as ReactBootstrap from 'react-bootstrap';
class DonationPage extends React.Component {
  render() {
    return (
      <>
        <h1 style={{ color: 'gray', paddingLeft: '3vw', marginTop: '3vh', marginBottom: '3vh', size: '30px', fontWeight: '500' }}>
          Search: 5 Donations found that matches your query
        </h1>
        <div className="container">
          <div className="row">
            <div className="col-md-3">
            </div>
            <div className="col-md-6">
              <div className="tabs" style={{ borderRadius: '50%' }}>
                <div className="btn-group btn-group-justified">
                  <a href="/people" className="btn btn-white btn-sm "> People </a>
                  <a href="/search" className="btn btn-white btn-sm "> Tasks</a>
                  <a href="/project" className="btn btn-white btn-sm "> Project </a>
                  <a href="/donation" className="btn btn-white btn-sm active"> Donation </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-lg-3" style={{ height: '40%', marginTop: '4vh' }}>
              <div class="filters">
                <div className="filter_heading" style={{ marginTop: '1vh' }} >Min Amount</div>
                <input className="contri" type="number" style={{ width: '100%', textAlign: 'left' }} placeholder="Enter Amount" />
                <div className="filter_heading" style={{ marginTop: '2vh', marginBottom: '1vh' }}>Causes</div>
                <select className="cause">
                  <option >All Causes</option>
                  <option>Animal Welfare</option>
                  <option>Youth and Children </option>
                  <option>Women </option>
                </select>
                <div className="filter_heading" style={{ marginTop: '2vh', marginBottom: '1vh' }}>Donation Types</div>
                <select className="cause">
                  <option >All Types</option>
                  <option>Periodic</option>
                  <option>One Time </option>
                </select>
                <div className="filter_heading" style={{ marginTop: '1vh', display: 'block' }}>Tag</div>
                <input className="tags" type="" />
                <input type="checkbox" />
                <span className="filter_heading" style={{ marginLeft: '1.5%' }}>IT Benefit</span><br />\
                <div>
                  <button type="submit" className="task">Search Donations</button>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-lg-8">
              <div className="row">
                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-7">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 1vw', width: '100%' }}>Donation without excerpts</h5>
                        <p style={{ padding: '1% 0 0 5%' }}>
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                          ligula eget dolor. Aenean massa.Cum sociis natoque penatibus et magnis dis
                          parturient montes, nascetur ridiculus mus.Donec quam felis, ultricies nec,
                          pellentesque eu, pretium quis, se ...
                        </p>
                      </div>
                      <div className="col-5">
                        <img style={{ width: '85%', height: '45%', padding: '1vh 0vw 1vh 1vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" alt="" />
                        <button className="count_me_in" style={{ marginTop: '1vh' }}>Contribute</button>
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '3vh', marginLeft: '3vw' }}>
                      <div>
                        Requires
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '1%', marginLeft: '3vw' }}>
                      <div>
                        &#8377; 30,000
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-7">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 1vw', width: '100%' }}>Laptop for visually challenged student</h5>
                        <p style={{ padding: '1% 0 0 5%' }}>
                          Like regular students, blind students also need to take notes for future reference. Papers written using
                          Braille slate may not last for long, are not suitable for notes taken in bulk and difficult to carry around. bulky. ...
                        </p>
                      </div>
                      <div className="col-5">
                        <img style={{ width: '85%', height: '45%', padding: '1vh 0vw 1vh 1vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" alt="" />
                        <button className="count_me_in" style={{ marginTop: '1vh' }}>Contribute</button>
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '2vh', marginLeft: '3vw' }}>
                      <div>
                        Requires
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '1%', marginBottom: '1vh', marginLeft: '3vw' }}>
                      <div>
                        &#8377; 30,000
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-7">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 1vw', width: '100%' }}>Setting blood donation drive in Akola</h5>
                        <p style={{ padding: '1% 0 0 5%' }}>
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                          ligula eget dolor. Aenean massa.Cum sociis natoque penatibus et magnis dis
                          parturient montes, nascetur ridiculus mus.Donec quam felis, ultricies nec,
                          pellentesque eu, pretium quis, se ...
                        </p>
                      </div>
                      <div className="col-5">
                        <img style={{ width: '85%', height: '45%', padding: '1vh 0vw 1vh 1vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" alt="" />
                        <button className="count_me_in" style={{ marginTop: '1vh' }}>Contribute</button>
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '2vh', marginLeft: '3vw' }}>
                      <div>
                        Requires
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '1%', marginBottom: '1vh', marginLeft: '3vw' }}>
                      <div>
                        &#8377; 30,000
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-7">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 1vw', width: '100%' }}>Raise funds for physically challenged childs</h5>
                        <p style={{ padding: '1% 0 0 5%' }}>
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                          ligula eget dolor. Aenean massa.Cum sociis natoque penatibus et magnis dis
                          parturient montes, nascetur ridiculus mus.Donec quam felis, ultricies nec,
                          pellentesque eu, pretium quis, se ...
                        </p>
                      </div>
                      <div className="col-5">
                        <img
                          style={{ width: '85%', height: '45%', padding: '1vh 0vw 1vh 1vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" alt="" />
                        <button className="count_me_in" style={{ marginTop: '1vh' }}>Contribute</button>
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '2vh', marginLeft: '3vw' }}>
                      <div>
                        Requires
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '1%', marginBottom: '1vh', marginLeft: '3vw' }}>
                      <div>
                        &#8377; 30,000
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-7">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 1vw', width: '100%' }}>Your Support Brings Hope</h5>
                        <p style={{ padding: '1% 0 0 5%' }}>
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                          ligula eget dolor. Aenean massa.Cum sociis natoque penatibus et magnis dis
                          parturient montes, nascetur ridiculus mus.Donec quam felis, ultricies nec,
                          pellentesque eu, pretium quis, se ...
                        </p>
                      </div>
                      <div className="col-5">
                        <img
                          style={{ width: '85%', height: '45%', padding: '1vh 0vw 1vh 1vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" alt="" />
                        <button className="count_me_in" style={{ marginTop: '1vh' }}>Contribute</button>
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '2vh', marginLeft: '3vw' }}>
                      <div>
                        Requires
                      </div>
                    </div>
                    <div className="flex-container" style={{ marginTop: '1%', marginBottom: '1vh', marginLeft: '3vw', }}>
                      <div>
                        &#8377; 30,000
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-lg-1">
            </div>
          </div>
        </div>
        <ReactBootstrap.Pagination style={{ marginLeft: '40%' }}>
          <ReactBootstrap.Pagination.Prev />
          <ReactBootstrap.Pagination.Item>{1}</ReactBootstrap.Pagination.Item>
          <ReactBootstrap.Pagination.Item>{2}</ReactBootstrap.Pagination.Item>
          <ReactBootstrap.Pagination.Item>{3}</ReactBootstrap.Pagination.Item>
          <ReactBootstrap.Pagination.Item active>{4}</ReactBootstrap.Pagination.Item>
          <ReactBootstrap.Pagination.Ellipsis />
          <ReactBootstrap.Pagination.Item>{20}</ReactBootstrap.Pagination.Item>
          <ReactBootstrap.Pagination.Next />
        </ReactBootstrap.Pagination>
      </>
    )
  }
}

export default DonationPage;