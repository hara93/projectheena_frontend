/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import { Card } from "react-bootstrap";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "33.33%",
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

const HowItWorks = () => {
  const [component, setComponent] = useState("");
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  let howitworks;
  howitworks = (
    <>
      <div style={{ padding: "20px 0" }}>
        <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
          How to get things done with ProjectHeena
        </h3>
        <ul className="fa-ul">
          <li>
            <i
              class="fas fa-long-arrow-alt-right"
              style={{ marginRight: "10px", color: "black" }}
            ></i>
            Volunteers create a profile and highlight the three important attributes - The time,
            location and skills available to contribute
          </li>
          <li>
            <i
              class="fas fa-long-arrow-alt-right"
              style={{ marginRight: "10px", color: "black" }}
            ></i>
            People who need help create clear detailed tasks (sharable and searchable)
          </li>
          <li>
            <i
              class="fas fa-long-arrow-alt-right"
              style={{ marginRight: "10px", color: "black" }}
            ></i>
            A task always has a start date and an end date. Every task has an application deadline
            before which volunteers can apply
          </li>
          <li>
            <i
              class="fas fa-long-arrow-alt-right"
              style={{ marginRight: "10px", color: "black" }}
            ></i>
            Volunteers directly pitch by 'Offering' help or they can be requested to help on a task
            by sending a 'Seek' request
          </li>
          <li>
            <i
              class="fas fa-long-arrow-alt-right"
              style={{ marginRight: "10px", color: "black" }}
            ></i>
            Once the task has started the task creator and volunteers can collaborate together on
            how the task will be executed
          </li>
          <li>
            <i
              class="fas fa-long-arrow-alt-right"
              style={{ marginRight: "10px", color: "black" }}
            ></i>
            Post completion the task owner credits people who helped in closing the task.
          </li>
          <li>
            <i
              class="fas fa-long-arrow-alt-right"
              style={{ marginRight: "10px", color: "black" }}
            ></i>
            Credited Volunteers get the number of hours the task carried added to their profiles
            with the testimonials.
          </li>
        </ul>
        <hr className="form-horizontal-line" />
        <p>
          {" "}
          A task goes through the following phase Open (while applications can be made to offer
          help), On going (Once the task date has passed), Awaiting Closure (post the task end date
          has passed) and Closed (Once the creator formally closes the task by acknowledging all the
          volunteers)
        </p>
        <h5 style={{ color: "#4CAF50", fontSize: "12px", fontWeight: "600" }}>
          More Data & Video will be added to this section. We are working on it !!
        </h5>
      </div>
    </>
  );

  let otherhowtos;
  otherhowtos = (
    <>
      <p>
        Our intention behind developing ProjectHeena was to build something so simple to use and
        contribute that you can easily imbibe it in your day to day activity. However things might
        seem tricky at times and we would like to remind you that we are always there for help. You
        can simply ask us a question on @ProjectHeena or reach us out via the feedback widget on
        every page. The content on this page is a work in progress. Let us know if you want us to
        specifically include something here and we will consider the request asap.
      </p>
      <div>
        <Accordion square expanded={expanded === "panel1"} onChange={handleChange("panel1")}>
          <AccordionSummary
            style={{
              backgroundColor: "#4caf4f",
              color: "white",
              border: "1px solid #ddd",
              borderColor: "#ddd",
              minHeight: "10px",
            }}
          >
            <Typography style={{ fontSize: "13px" }}>
              <i class="fas fa-plus rotate-icon" style={{ color: "white" }}></i> How to create an
              account/profile on ProjectHeena
            </Typography>
          </AccordionSummary>
          <AccordionDetails style={{ width: "100%" }}>
            <div style={{ display: "flex", flexDirection: "column" }}>
              <ul className="fa-ul">
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Visit the homepage @ https://ProjectHeena.com.
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Click on the green colored Join the Revolution Button
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Select a social network you are active with to create an account
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Approve the login request on the respective Network (Please be aware we never
                  auto-post or spam your timeline)
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Post approval you wll be redirected to ProjectHeena page
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Enter a user name for your account. This can be done only once and hence chose the
                  best user name. We recommend 'firstName-lastName'
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  On Submit you be be directed to the Profile page
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Input and submit all the required details here. These details will be part of your
                  Karma profile and will be available publicly. Take care and provide as detailed
                  inputs as possible.
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  On submission you will be directed to the Dashboard Or main screen in ProjectHeena
                  where you can see your latest actionable, tasks and notifications
                </li>
              </ul>
              <p>
                Congratulations you have just created an account on ProjectHeena. For any help feel
                free to get in touch with us on team@projectheena.com
              </p>
            </div>
          </AccordionDetails>
        </Accordion>

        <Accordion square expanded={expanded === "panel2"} onChange={handleChange("panel2")}>
          <AccordionSummary
            style={{
              backgroundColor: "#4caf4f",
              color: "white",
              border: "1px solid #ddd",
              borderColor: "#ddd",
              minHeight: "10px",
            }}
          >
            <Typography style={{ fontSize: "13px" }}>
              <i class="fas fa-plus" style={{ color: "white" }}></i>
              How to register an NGO on ProjectHeena
            </Typography>
          </AccordionSummary>
          <AccordionDetails style={{ width: "100%" }}>
            <div style={{ display: "flex", flexDirection: "column" }}>
              <ul className="fa-ul">
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Visit the homepage @ https://ProjectHeena.com.
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Click on the green colored Join the Revolution Button
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  On the Join page, select the 'For NGOs' tab rather than the default open in front
                  of you
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Click the 'Register Here' link. This will take you to
                  https://ProjectHeena.com/ngo-registration
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  On this page fill all the fields provided in as much detail as possible
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  On submission if there are no issues you will receive an alert that registration
                  has been received.
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Post this step we (Team ProjectHeena) go through your application and approve it
                  post one round of discussion. You will receive an auto generated mail with
                  credentials to use your account
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Once you receive the mail click on the link to again visit the join page and this
                  time enter the credentials provided
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  On submitting the same an edit profile screen will be displayed where you can
                  enter all the details which will be displayed on the NGOs public profile
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Be as precise and detailed as possible while filling this form. Use right images
                  for Logo and Profile pic
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Once this form is submitted you will be able to view the profile available to
                  public. Your URL to share would be https://ProjectHeena.com/ngo/YourNGOName
                </li>
              </ul>
              <p>
                Congratulations your NGO has been registered on ProjetHeena. The Admin user will get
                an option to select the NGO name when he is creating help requests on your behalf
                (tasks) For any help feel free to get in touch with us on team@projectheena.com
              </p>
            </div>
          </AccordionDetails>
        </Accordion>

        <Accordion square expanded={expanded === "panel3"} onChange={handleChange("panel3")}>
          <AccordionSummary
            style={{
              backgroundColor: "#4caf4f",
              color: "white",
              border: "1px solid #ddd",
              borderColor: "#ddd",
              minHeight: "10px",
            }}
          >
            <Typography style={{ fontSize: "13px" }}>
              <i class="fas fa-plus" style={{ color: "white" }}></i> How to create a task on
              ProjectHeena
            </Typography>
          </AccordionSummary>
          <AccordionDetails style={{ width: "100%" }}>
            <div style={{ display: "flex", flexDirection: "column" }}>
              <p>Following are the steps to create a task on ProjectHeena</p>
              <ul className="fa-ul">
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  You will require a ProjectHeena account to create the same. See "How to create an
                  account/profile on ProjectHeena" for more details
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Visit the homepage @ https://ProjectHeena.com.
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Click on the green colored Join the Revolution Button
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Login with your account (one chose while registration Facebook or LinkedIN)
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  You will be directed to the Dashboard. Click the Task tab.
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  In the Tasks tab click the Create tasks button on this tab
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  On the next screen select How you would like to create this task as. Options like
                  Startup, Student, NGO and Individual are available. This will help right
                  volunteers get in touch with you as soon as possible.
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Do note only registered NGOs are allowed to create unlimited tasks. Others can
                  take help twice as many times as they provide help. That if for every two times
                  you accept help from volunteers we expect you to help others atleast once.
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  On the task page. Fill all the required details as detailed as possible.
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Ensure that you provide enough details and leave nothing to guess work. You can
                  read our tenets to create best task here ***
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  You can select maximum of 5 skills for a task
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Keep sufficient days buffer between today, Application deadline for your task and
                  Task close date
                </li>
                <li>
                  <i
                    class="fas fa-long-arrow-alt-right"
                    style={{ marginRight: "10px", color: "black" }}
                  ></i>
                  Post submitting the form, do ensure that you share your task on social media so
                  that more people can either help you or share the same post further to increase
                  your reach
                </li>
              </ul>
              <p>
                Congratulations, You have created your task successfully. You can click on Go to
                Task page to look how the same will appear to the public. Next Step Search and seek
                help from volunteers for your task.
              </p>
            </div>
          </AccordionDetails>
        </Accordion>
      </div>
    </>
  );
  let componentToShow;
  if (component === "otherhowtos") {
    componentToShow = otherhowtos;
  } else {
    componentToShow = howitworks;
  }

  return (
    <>
      <div className="banner">
        <div className="banner-img">
          <div
            id="parallex-bg"
            style={{
              backgroundImage: "url('http://projectheena.in/assets/img/static/superhero-kid.jpg') ",
            }}
          ></div>
        </div>
      </div>
      <div className="container">
        <Card style={{ padding: "0 15px" }}>
          <div style={{ marginTop: "30px", boxSizing: "box-border" }}>
            <h3 className="static-title">How it Works</h3>
          </div>
          <div>
            <ul className="howitworks-tab">
              <li>
                <a className="active" href="#howitworks" onClick={() => setComponent("howitworks")}>
                  <i class="fas fa-bullhorn active-focus "> HOW IT WORKS</i>
                </a>
              </li>
              <li>
                <a
                  className="active"
                  href="#otherhowtos"
                  onClick={() => setComponent("otherhowtos")}
                >
                  <i class="fas fa-bullhorn active-focus "> OTHER HOW TOS</i>
                </a>
              </li>
            </ul>
            <div>{componentToShow}</div>
          </div>
        </Card>
      </div>
    </>
  );
};
export default HowItWorks;
