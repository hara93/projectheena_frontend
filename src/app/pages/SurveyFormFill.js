import React, { useState, useEffect } from 'react';
import { Container, Form, Card, Row, Col } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { values } from 'lodash';
import { FormControl, RadioGroup, Radio, FormControlLabel, FormLabel } from '@material-ui/core';
import Input from '../components/Input';
import RadioItem from '../components/RadioGroup'
import SelectOptions from '../components/SelectOptions';

import CheckboxInput from '../components/CheckboxInput';



const theme = createMuiTheme({
    spacing: 4,
});

const useStyle = makeStyles(theme => ({
    root: {
        '& .MuiFormControl-root MuiTextField-root': {
            width: '100%',
            margin: theme.spacing(5)
        }

    }
}))







const initialFieldValues = {
    id: 0,
    fullName: '',
    age: '',
    email: '',
    mobile: '',
    city: '',
    gender: 'male',
    hobby: '',
    date: new Date(),
    time: '',
    location: '',
    sign: '',
    image: '',
    number: '',
    info: '',
    geotag: '',
    transport: '',
    video: ''

}

const genderItems = [
    { id: 'male', title: 'Male' },
    { id: 'female', title: 'Female' },
    { id: 'other', title: 'Other' },

]

const hobbies = [
    { id: 'singing', title: 'Singing' },
    { id: 'dancing', title: 'Dancing' },
    { id: 'reading', title: 'Reading' },

]



export default function SurveyFormFill() {


    const [values, setValues] = useState(initialFieldValues)
    const classes = useStyle



    const handleInputChange = e => {
        const { name, value } = e.target
        setValues({
            ...values,
            [name]: value
        })
    }


    return (
        <>
            <h1 style={{ fontWeight: '900', color: '#4caf4f', textAlign: 'center' }}>Survey </h1>
            <Card className="p-5">
                <Form className={classes.root} autocomplete="off" >
                    <Row>
                        <Col md={6}>
                            <Input name="fullName" label="Name" value={values.fullName} onChange={handleInputChange} />
                            <Input name="email" label="Email" value={values.email} onChange={handleInputChange} />
                        </Col>
                        <Col md={6}>
                            <RadioItem
                                name="gender"
                                label="Gender"
                                value={values.gender}
                                onChange={handleInputChange}
                                items={genderItems}
                            />
                            <SelectOptions
                                name="hobby"
                                label="Hobby"
                                value={values.hobby}
                                onChange={handleInputChange}
                                option={hobbies}


                            />

                            <CheckboxInput
                                name="hobby"
                                label="Hobbies"
                                value={values.hobby}
                                onChange={handleInputChange}
                                choice={hobbies}

                            />


                        </Col>

                    </Row>




                </Form>

            </Card>

        </>

    )
}