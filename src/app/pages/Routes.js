/**
 * High level router.
 *
 * Note: It's recommended to compose related routes in internal router
 * components (e.g: `src/app/modules/Auth/pages/AuthPage`, `src/app/BasePage`).
 */

import React from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import { Layout, LayoutAuth } from "../../_metronic/layout";
import BasePage from "../BasePage";
import { Logout, AuthPage } from "../modules/Auth";
import ErrorsPage from "../modules/ErrorsExamples/ErrorsPage";
import SignUp from "../pages/signup";
import GlenmarkRoutes from "../pages/glenmark/GlenmarkRoutes"
import BasepageUnrestricted from '../BasepageUnrestricted';

export function Routes() {
  var url = window.location.href

  const { isAuthorized } = useSelector(
    ({ phAuth }) => ({
      isAuthorized: phAuth.user != null,
    }),
    shallowEqual
  );
  const profileDeatils = useSelector(
    (state) => (state.phAuth && state.phAuth.profileDeatils) || null
  );
  console.log("profileDeatils", profileDeatils);
  var TokenExpiry = (profileDeatils && profileDeatils.expired_in) || new Date();

  const checkExpiry = (firstDate, secondDate) => {
    if (firstDate.setHours(0, 0, 0, 0) - secondDate.setHours(0, 0, 0, 0) >= 0) {
      //first date is in future, or it is today
      return false;
    }
    return true;
  };
  console.log("profileDetails", profileDeatils);
  console.log("checkExpiry", checkExpiry(new Date(TokenExpiry), new Date()));
  return (
    <Switch>
      {!isAuthorized ? (
        /*Render auth page when user at `/auth` and not authorized.*/
        // <Route>
        // {/* <AuthPage /> */ }
        // {/* <SignUp /> */}
        // {/* </Route> */}
        <BasepageUnrestricted />
      ) : (
        /*Otherwise redirect to root page (`/`)*/
        <Redirect from="/auth" to="/" />
      )}

      <Route path="/error" component={ErrorsPage} />
      <Route path="/logout" component={Logout} />

      {
        !isAuthorized ?
          // <Redirect to="/auth/login" />
          null : (
            url.match(/glenmark/gi) ?
              <Layout>
                <GlenmarkRoutes />
              </Layout>
              :
              <Layout>
                <BasePage />
              </Layout>
          )
      }
    </Switch>
  );
}
