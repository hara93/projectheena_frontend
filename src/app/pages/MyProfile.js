import React from 'react';
import HorizontalTabNav from './HorizontalTabNav';
import TabContent from './TabContent';
import { Row, Col } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';
import SuggestedTasks from '../components/SuggestedTasks';
import DonationVolunteer from './DonationVolunteer';
import HorizontalTabContent from './HorizonatalTabContent';
import Donate from './Donate';
import DonationCollaborate from './DonationCollaborate';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import TouchCarousel from 'react-touch-carousel'
// import MultiCarouselPage from '../components/MultiCarouselPage';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

import OwlCarousel from 'react-owl-carousel2';
// import 'react-owl-carousel2/style.css';

const options = {
    // items: 1,
    nav: true,
    rewind: true,
    autoplay: true
};

const responsive = {
    // superLargeDesktop: {
    //     // the naming can be any, depends on you.
    //     breakpoint: { max: 4000, min: 3000 },
    //     items: 5
    // },
    desktop: {
        // breakpoint: { max: 3000, min: 1024 },
        breakpoint: { max: 1024, min: 464 },
        items: 3,
        slidesToSlide: 3, // optional, default to 1.
        // breakpoint: { max: 1024, min: 464 },
        partialVisibilityGutter: 40
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
    }
};

const listOfData = [
    // your data array here
    {
        id: 1,
        url: '',
    },
    {
        id: 2,
        url: '',
    },
    {
        id: 3,
        url: '',
    }
]

function CarouselContainer(props) {
    // render the carousel structure
    // render() {
    return (
        <></>
    );
    // }
}

function renderCard(index, modIndex, cursor) {
    const item = listOfData[modIndex]
    // render the item
    return (
        <div style={{ width: '32%' }}>
            <Card>


                <img src={Images.Children} style={{ width: '100%' }} />
                <span style={{ display: 'block', fontWeight: '600', height: '20px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', color: '#4CAF50', padding: '2vh 1vw 5vh 1vw' }}> title</span>
                <div style={{ marginBottom: '2vh' }}>
                    <small style={{ paddingLeft: '1vw', paddingBottom: '2vh' }}>date</small>

                </div>



            </Card>
        </div>
    );
}

const Accordion = withStyles({
    root: {
        backgroundColor: '#e7eaec',
        borderBottom: '1px solid #919fa8',
        border: '1px solid rgba(0, 0, 0, .125)',
        boxShadow: 'none',
        '&:not(:last-child)': {
            borderBottom: 0,
        },
        '&:before': {
            display: 'none',
        },
        '&$expanded': {
            margin: 'auto',
        },
    },
    expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
    root: {
        backgroundColor: '#e7eaec',
        borderBottom: '1px solid #919fa8',
        marginBottom: -1,
        minHeight: 56,
        '&$expanded': {
            minHeight: 56,
        },
    },
    content: {
        '&$expanded': {
            margin: '12px 0',
        },
    },
    expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
        backgroundColor: 'white',
        borderBottom: '1px solid #919fa8',
    },
}))(MuiAccordionDetails);

class MyProfile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: 'Volunteer',
            expanded: 'panel1'
        }
    }

    setSelected = (tab) => {
        this.setState({ selected: tab });
    }

    handleChange = (panel) => (event, newExpanded) => {
        this.setState({ expanded: newExpanded ? panel : false });
        // setExpanded(newExpanded ? panel : false);
    };

    render() {
        return (
            <div className="container">
                <div className="container-fluid">
                    <h2 className="font-bold">
                        <span id="page-title">Dashboard</span></h2>
                    <Row className="p-6">

                        <Col md={9}>
                            <Card>
                                {/* <li className="nav-item" key="Volunteer" style={{ textAlign: 'center' }}> */}
                                <Row>
                                    {/* <Card> */}
                                    <Col md={3}>
                                        <Row>
                                            <div style={{ display: "flex", flexDirection: "row" }}>

                                                <div style={{ textAlign: 'center' }}>
                                                    <a className={"nav-link justify-content-center"}
                                                        // onClick={() => this.props.setSelected(tab.name)}
                                                        style={{ flexDirection: "column" }} >

                                                        <img src="http://projectheena.in/assets/img/dashboard/volunteer.png" />
                                                        Volunteer
                                                        {/* {tab.name} */}
                                                    </a>
                                                </div>
                                                <div style={{ textAlign: 'center', backgroundColor: "blueviolet" }}>
                                                    <a className={"nav-link active horizontal-tabs justify-content-center"}
                                                        // onClick={() => this.props.setSelected(tab.name)}
                                                        style={{ flexDirection: "column" }} >

                                                        <img src="http://projectheena.in/assets/img/dashboard/donate.png" />
                                                        Donate
                                                        {/* {tab.name} */}
                                                    </a>
                                                </div>
                                            </div>
                                        </Row>
                                        <Row>
                                            <div style={{ display: "flex", flexDirection: "row", backgroundColor: "red" }}>

                                                <div style={{ textAlign: 'center' }}>
                                                    <a className={"nav-link active horizontal-tabs justify-content-center"}
                                                        // onClick={() => this.props.setSelected(tab.name)}
                                                        style={{ flexDirection: "column" }} >

                                                        <img src="http://projectheena.in/assets/img/dashboard/advocate.png" />
                                                        Advocate
                                                        {/* {tab.name} */}
                                                    </a>
                                                </div>
                                                <div style={{ textAlign: 'center', backgroundColor: "blueviolet" }}>
                                                    <a className={"nav-link active horizontal-tabs justify-content-center"}
                                                        // onClick={() => this.props.setSelected(tab.name)}
                                                        style={{ flexDirection: "column" }} >

                                                        <img src="http://projectheena.in/assets/img/dashboard/collaborate.png" />
                                                        Collaborate
                                                        {/* {tab.name} */}
                                                    </a>
                                                </div>
                                            </div>
                                        </Row>

                                    </Col>

                                    <Col md={9}>
                                        <Card style={{ marginLeft: '-2%' }}>
                                            <Row className="p-5 nav-link">

                                                <Col md={3} >
                                                    <p style={{ textAlign: 'center' }}>NGOs</p>
                                                    <h1 style={{ textAlign: 'center' }}>31</h1>
                                                </Col>
                                                <Col md={3}>
                                                    <p style={{ textAlign: 'center' }}>Projects</p>
                                                    <h1 style={{ textAlign: 'center' }}>31</h1>
                                                </Col>
                                                <Col md={3}>
                                                    <p style={{ textAlign: 'center' }}>Causes</p>
                                                    <h1 style={{ textAlign: 'center' }}>308</h1>
                                                </Col>
                                                <Col md={3}>
                                                    <p style={{ textAlign: 'center' }}>Beneficiaries</p>
                                                    <h1 style={{ textAlign: 'center' }}>572887</h1>
                                                </Col>

                                            </Row>
                                        </Card>
                                    </Col>

                                </Row>

                            </Card>
                            {/* </Card> */}

                            {/* </li> */}
                            {/* <HorizontalTabNav tabs={[{ name: 'Volunteer', img: 'http://projectheena.in/assets/img/dashboard/volunteer.png' }, { name: 'Donate', img: 'http://projectheena.in/assets/img/dashboard/donate.png' }, { name: 'Collaborate', img: 'http://projectheena.in/assets/img/dashboard/collaborate.png' }, { name: 'Advocate', img: 'http://projectheena.in/assets/img/dashboard/advocate.png' }]} selected={this.state.selected} setSelected={this.setSelected}>
                                <HorizontalTabContent isSelected={this.state.selected === 'Volunteer'}>
                                    <DonationVolunteer />
                                </HorizontalTabContent>

                                <HorizontalTabContent isSelected={this.state.selected === 'Donate'}>
                                    <Donate />
                                </HorizontalTabContent>

                                <HorizontalTabContent isSelected={this.state.selected === 'Collaborate'}>
                                    <DonationCollaborate />
                                </HorizontalTabContent>

                                <HorizontalTabContent isSelected={this.state.selected === 'Advocate'}>
                                    <Card className="p-5">
                                        <h3 style={{ marginLeft: '38%', color: '#c4c4c4' }}>Coming Soon</h3>

                                    </Card>
                                </HorizontalTabContent>
                            </HorizontalTabNav> */}
                            <Row className="mt-3">
                                <div className="boxy-ibox " style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
                                    <h3 className="ibox-title boxy-ibox-title" >Karma Points</h3>
                                    <div className="ibox-content boxy-ibox-content" >

                                        <Row className="nav-link">
                                            <Col md={3} >
                                                <p style={{ textAlign: 'center' }}>Current Year</p>

                                            </Col>
                                            <Col md={3}>
                                                <h1 style={{ textAlign: 'center' }}>31</h1>
                                            </Col>
                                            <Col md={3}>
                                                <p style={{ textAlign: 'center' }}>Total</p>
                                            </Col>
                                            <Col md={3}>
                                                <h1 style={{ textAlign: 'center' }}>572887</h1>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </Row>
                            <Row className="mt-3">
                                <Row className="px-3">
                                    <h3> Task Gallery</h3>
                                </Row>

                                <Row>
                                    <Col md={12}>
                                        <div>
                                            {/* <OwlCarousel ref="car" options={options}
                                            //  events={events} 
                                            >
                                                <div><Card>


                                                    <img src={Images.Children} style={{ width: '50%' }} />
                                                    <span style={{ display: 'block', fontWeight: '600', height: '20px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', color: '#4CAF50', padding: '2vh 1vw 5vh 1vw' }}> by User</span>
                                                    <div style={{ marginBottom: '2vh' }}>
                                                        <small style={{ paddingLeft: '1vw', paddingBottom: '2vh' }}>Clean Beach</small>

                                                    </div>



                                                </Card></div>
                                                <div><Card>


                                                    <img src={Images.Children} style={{ width: '50%' }} />
                                                    <span style={{ display: 'block', fontWeight: '600', height: '20px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', color: '#4CAF50', padding: '2vh 1vw 5vh 1vw' }}> by User</span>
                                                    <div style={{ marginBottom: '2vh' }}>
                                                        <small style={{ paddingLeft: '1vw', paddingBottom: '2vh' }}>Clean Beach</small>

                                                    </div>



                                                </Card></div>
                                                <div><Card>


                                                    <img src={Images.Children} style={{ width: '50%' }} />
                                                    <span style={{ display: 'block', fontWeight: '600', height: '20px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', color: '#4CAF50', padding: '2vh 1vw 5vh 1vw' }}> by User</span>
                                                    <div style={{ marginBottom: '2vh' }}>
                                                        <small style={{ paddingLeft: '1vw', paddingBottom: '2vh' }}>Clean Beach</small>

                                                    </div>



                                                </Card></div>
                                            </OwlCarousel> */}

                                        </div>
                                    </Col>
                                </Row>


                            </Row>
                            <Row className="mt-3">
                                <div className="boxy-ibox " style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
                                    <h3 className="ibox-title boxy-ibox-title" >My Volunteering Log</h3>
                                    <div className="ibox-content boxy-ibox-content" >


                                        <Accordion square
                                            expanded='panel'
                                        // expanded={this.props.expanded === 'panel2'} onChange={handleChange('panel2')}
                                        >
                                            <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
                                                <h3> Tasks you are working on</h3>
                                            </AccordionSummary>
                                            <AccordionDetails >
                                                <Typography style={{ width: '100%' }}>
                                                    {/* <div style={{ float: 'right' }} onClick={handleShow1}>

                                                        <button className="p-2 add_budget" >
                                                            <i class="fas fa-rupee-sign" style={{ color: 'white' }}></i>
                                Add Budget
                            </button>
                                                    </div> */}

                                                    <Row>
                                                        <Col md={5}>
                                                            <h6>Name</h6>

                                                        </Col>
                                                        <Col md={2}>
                                                            <h6>Status</h6>

                                                        </Col>
                                                        <Col md={3}>
                                                            <h6>Location</h6>

                                                        </Col>
                                                        <Col md={2}>
                                                            <h6>Hours</h6>

                                                        </Col>

                                                    </Row>
                                                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                                                    <Row>
                                                        <Col md={5}>
                                                            <img src={Images.Social_work} style={{ marginLeft: "2%", width: '20%', }} />

                                                            <span style={{ marginLeft: "2%" }}>Survey Of Wards In PCMC Area</span>

                                                        </Col>
                                                        <Col md={2}>

                                                            <span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>Open</span>
                                                        </Col>
                                                        <Col md={3}>
                                                            <span>Mumbai, Maharashtra, India</span>
                                                        </Col>
                                                        <Col md={2}>
                                                            <span>3 Hours</span>
                                                        </Col>
                                                    </Row>

                                                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                                                    <Row>
                                                        <Col md={5}>
                                                            <img src={Images.Social_work} style={{ marginLeft: "2%", width: '20%', }} />

                                                            <span style={{ marginLeft: "2%" }}>Survey Of Wards In PCMC Area</span>

                                                        </Col>
                                                        <Col md={2}>

                                                            <span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>Open</span>
                                                        </Col>
                                                        <Col md={3}>
                                                            <span>Mumbai, Maharashtra, India</span>
                                                        </Col>
                                                        <Col md={2}>
                                                            <span>3 Hours</span>
                                                        </Col>
                                                    </Row>
                                                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                                                    <Row>
                                                        <Col md={5}>
                                                            <img src={Images.Social_work} style={{ marginLeft: "2%", width: '20%', }} />

                                                            <span style={{ marginLeft: "2%" }}>Survey Of Wards In PCMC Area</span>

                                                        </Col>
                                                        <Col md={2}>

                                                            <span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>Open</span>
                                                        </Col>
                                                        <Col md={3}>
                                                            <span>Mumbai, Maharashtra, India</span>
                                                        </Col>
                                                        <Col md={2}>
                                                            <span>3 Hours</span>
                                                        </Col>
                                                    </Row>
                                                </Typography>
                                            </AccordionDetails>
                                        </Accordion>

                                        <Accordion square
                                            expanded='panel'
                                            className="mt-2"
                                        // expanded={this.props.expanded === 'panel2'} onChange={handleChange('panel2')}
                                        >
                                            <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
                                                <h3> Tasks you want help on</h3>
                                            </AccordionSummary>
                                            <AccordionDetails >
                                                <Typography style={{ width: '100%' }}>
                                                    {/* <div style={{ float: 'right' }} onClick={handleShow1}>

                                                        <button className="p-2 add_budget" >
                                                            <i class="fas fa-rupee-sign" style={{ color: 'white' }}></i>
                                Add Budget
                            </button>
                                                    </div> */}

                                                    <Row>
                                                        <Col md={5}>
                                                            <h6>Name</h6>

                                                        </Col>
                                                        <Col md={2}>
                                                            <h6>Status</h6>

                                                        </Col>
                                                        <Col md={3}>
                                                            <h6>Location</h6>

                                                        </Col>
                                                        <Col md={2}>
                                                            <h6>Hours</h6>

                                                        </Col>

                                                    </Row>
                                                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                                                    <Row>
                                                        <Col md={5}>
                                                            <img src={Images.Social_work} style={{ marginLeft: "2%", width: '20%', }} />

                                                            <span style={{ marginLeft: "2%" }}>Survey Of Wards In PCMC Area</span>

                                                        </Col>
                                                        <Col md={2}>

                                                            <span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>Open</span>
                                                        </Col>
                                                        <Col md={3}>
                                                            <span>Mumbai, Maharashtra, India</span>
                                                        </Col>
                                                        <Col md={2}>
                                                            <span>3 Hours</span>
                                                        </Col>
                                                    </Row>

                                                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                                                    <Row>
                                                        <Col md={5}>
                                                            <img src={Images.Social_work} style={{ marginLeft: "2%", width: '20%', }} />

                                                            <span style={{ marginLeft: "2%" }}>Survey Of Wards In PCMC Area</span>

                                                        </Col>
                                                        <Col md={2}>

                                                            <span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>Open</span>
                                                        </Col>
                                                        <Col md={3}>
                                                            <span>Mumbai, Maharashtra, India</span>
                                                        </Col>
                                                        <Col md={2}>
                                                            <span>3 Hours</span>
                                                        </Col>
                                                    </Row>
                                                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                                                    <Row>
                                                        <Col md={5}>
                                                            <img src={Images.Social_work} style={{ marginLeft: "2%", width: '20%', }} />

                                                            <span style={{ marginLeft: "2%" }}>Survey Of Wards In PCMC Area</span>

                                                        </Col>
                                                        <Col md={2}>

                                                            <span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>Open</span>
                                                        </Col>
                                                        <Col md={3}>
                                                            <span>Mumbai, Maharashtra, India</span>
                                                        </Col>
                                                        <Col md={2}>
                                                            <span>3 Hours</span>
                                                        </Col>
                                                    </Row>
                                                </Typography>
                                            </AccordionDetails>
                                        </Accordion>

                                    </div>
                                </div>
                            </Row>
                        </Col>

                        <Col md={3}>

                            <div className="boxy-ibox " style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
                                <h3 className="ibox-title boxy-ibox-title" >Certificates</h3>
                                <div className="ibox-content boxy-ibox-content" style={{ color: '#4CAF50' }} >
                                    Download Vounteering Certificate
                                </div>
                            </div>

                            <div className="boxy-ibox mt-3" style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
                                <h3 className="ibox-title boxy-ibox-title" >Share with your Friends</h3>
                                <div className="ibox-content boxy-ibox-content" style={{
                                    display: 'flex',
                                    justifyContent: 'center', flexDirection: 'column'
                                }} >

                                    <i className="fab fa-facebook-square fa-3x" style={{ color: '#3c5997', marginRight: '26px' }}></i>
                                    <i className="fab fa-twitter-square fa-3x" style={{ color: '#04abed', marginRight: '26px', marginTop: '2%' }}></i>
                                    <i className="fab fa-google-plus-square fa-3x" style={{ color: '#dd4b39', marginRight: '26px', marginTop: '2%' }}></i>

                                </div>
                            </div>



                            <div className="boxy-ibox mt-3" style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
                                <h3 className="ibox-title boxy-ibox-title" >Suggested Tasks</h3>
                                <div className="ibox-content boxy-ibox-content" >
                                    <Row style={{ borderBottom: '1px solid gainsboro', marginTop: '2%', paddingBottom: '2%', justifyContent: 'center', alignItems: 'center' }}>
                                        <Col md={3}>
                                            <img src={Images.Social_work} style={{ width: '150%', borderRadius: '50%' }} />
                                        </Col>
                                        <Col md={9}>
                                            Clean Mumbai Beaches
                                        </Col>
                                    </Row>
                                    <Row style={{ borderBottom: '1px solid gainsboro', marginTop: '2%', paddingBottom: '2%', justifyContent: 'center', alignItems: 'center' }}>
                                        <Col md={3}>
                                            <img src={Images.Social_work} style={{ width: '150%', borderRadius: '50%' }} />
                                        </Col>
                                        <Col md={9} >
                                            Clean Mumbai Beaches
                                        </Col>
                                    </Row>
                                </div>
                            </div>



                            {/* </Col>

                        <Col md={3}> */}

                        </Col>
                    </Row>

                </div>

            </div>
        )
    }
}

export default MyProfile