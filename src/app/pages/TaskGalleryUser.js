/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import ImageUploader from "react-images-upload";
import * as ReactBootstrap from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { Card } from "@material-ui/core";
import { Images } from "../config/Images";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import CustomButton from "../components/CustomButton";
import Form from "react-bootstrap/Form";
import { useDropzone } from "react-dropzone";
import ReactGallery from "reactive-blueimp-gallery";
const TaskGalleryUser = (props) => {
  const images = [
    {
      source: Images.coins,
    },
    {
      source: Images.animals,
    },
    {
      source: Images.ironman,
    },
  ];
  const thumbsContainer = {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  };

  const thumb = {
    display: "inline-flex",
    borderRadius: 2,
    border: "1px solid #eaeaea",
    marginBottom: 8,
    marginRight: 8,
    width: 100,
    height: 100,
    padding: 4,
    boxSizing: "border-box",
  };

  const thumbInner = {
    display: "flex",
    minWidth: 0,
    overflow: "hidden",
  };

  const img = {
    display: "block",
    width: "auto",
    height: "100%",
  };
  const [files, setFiles] = useState([]);
  console.log("files", files);
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/jpeg, image/png",
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
    },
  });
  const thumbs = files.map((file) => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} style={img} />
      </div>
    </div>
  ));
  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );
  return (
    <>
      {/* <div style={{ marginBottom: "25px" }}>
        <div className="box-ibox-title">
          <h3 style={{ fontSize: "16px", fontWeight: "600" }}>Upload Gallery Images</h3>
        </div>
        <div
          style={{
            padding: "15px",
            border: "1px solid #e7eaec",
          }}
        >
          <div style={{ border: "1px solid #0000004d", padding: "15px" }}>
            <div
              {...getRootProps({ className: "dropzone" })}
              style={{ display: "flex", justifyContent: "center", padding: "30px" }}
            >
              <input {...getInputProps()} />
              <p>Drop files here to upload</p>
            </div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <button className="gallery-upload-btn">Drop Images and Click here to Upload</button>
            </div>
          </div>
          <aside style={thumbsContainer}>{thumbs}</aside>
        </div>
        <div className="help-block">
          <p>
            Every participant in this task can upload images, however only creator can delete
            uploaded images.
          </p>
          <p>Maximum photo upload size is 5 MB</p>
        </div>
      </div> */}
      <div>
        <h3 style={{ fontSize: "16px", fontWeight: "600", margin: "0" }}>Gallery</h3>
      </div>
      <div>
        <ReactGallery source={images} sizes="100px 100px" withControls />
      </div>
    </>
  );
};
export default TaskGalleryUser;
