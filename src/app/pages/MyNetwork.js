/* eslint-disable no-restricted-imports */
import React from "react";
import "../tnc.css";
import FollowerCard from "../components/Follower";
import FollowingCard from "../components/Following";
import Contact from "./Contact";

const followerlist = [
  {
    icon: "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    name: "Sahil Iyer",
    details: "user",
  },
  {
    icon: "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    name: "Rutuja Nighot",
    details: "user",
  },
  {
    icon: "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    name: "Tanmay Kale",
    details: "user",
  },
  {
    icon: "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    name: "Navya",
    details: "user",
  },
  {
    icon: "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    name: "Tanmay Kale",
    details: "user",
  },
  {
    icon: "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    name: "Tanmay Kale",
    details: "user",
  },
];

class MyNetwork extends React.Component {
  constructor() {
    super();
    this.state = {
      clickedFollower: true,
      clickedFollowing: false,
      clickedContact: false,
    };
  }

  handleClick = () => {
    this.setState({
      clickedFollower: true,
      clickedFollowing: false,
      clickedContact: false,
    });
  };
  handleFollowing = () => {
    this.setState({
      clickedFollowing: true,
      clickedFollower: false,
      clickedContact: false,
    });
  };

  handleContact = () => {
    this.setState({
      clickedContact: true,
      clickedFollower: false,
      clickedFollowing: false,
    });
  };

  render() {
    return (
      <div className="container">
        <h2 className="font-bold" style={{ padding: "0", margin: "20px 0 10px 0" }}>
          My Network of Impact
        </h2>

        <p>
          The following sections highlight people you follow and people who follow you. We will
          regularly update you about what all the do gooders in your network are upto.
          <br />
          Follow NGOs, Companies and People you would like to track and engage with.
        </p>
        <div>
          <ul className="howitworks-tab">
            <li>
              <a
                className="active"
                href="#howitworks"
                // onClick={() => setComponent("howitworks")}
                onClick={this.handleClick}
              >
                <i class="fas fa-bullhorn active-focus "> MY FOLLOWERS</i>
              </a>
            </li>
            <li>
              <a
                className="active"
                href="#otherhowtos"
                onClick={this.handleFollowing}
              //  onClick={() => setComponent("otherhowtos")}
              >
                <i class="fas fa-bullhorn active-focus "> FOLLOWING</i>
              </a>
            </li>{" "}
            <li>
              <a
                className="active"
                href="#howitworks"
                onClick={this.handleContact}

              // onClick={() => setComponent("howitworks")}
              >
                <i class="fas fa-bullhorn active-focus "> CONTACT FOLLOWERS</i>
              </a>
            </li>
          </ul>
        </div>
        {/* <div>
          <ul className="ph-nav-bar tabs">
            <li
              role="presentation"
              style={{
                display: "inline-block",
                marginRight: "3vw",
                marginTop: "1vh",
              }}
            >
              <a
                href="#Follower"
                className="active"
                role="tab"
                data-toggle="tab"
                onClick={this.handleClick}
                id="tnc"
              >
                <i
                  className="fa fa-newspaper-o"
                  style={{ color: "#a0a0a0", fontSize: "18px" }}
                >
                  My Followers
                </i>
              </a>
            </li>
            <li
              role="presentation"
              style={{
                display: "inline-block",
                marginRight: "3vw",
                marginTop: "1vh",
              }}
            >
              <a
                href="#Follower"
                role="tab"
                data-toggle="tab"
                onClick={this.handleFollowing}
                id="Following"
              >
                <i
                  className="fa fa-bullhorn"
                  style={{ color: "#a0a0a0", fontSize: "18px" }}
                >
                  Following
                </i>
              </a>
            </li>
            <li
              role="presentation"
              style={{
                display: "inline-block",
                marginRight: "3vw",
                marginTop: "1vh",
              }}
            >
              <a
                href="#Follower"
                role="tab"
                data-toggle="tab"
                onClick={this.handleContact}
                id="cancel"
              >
                <i
                  className="fa fa-bullhorn"
                  style={{ color: "#a0a0a0", fontSize: "18px" }}
                >
                  Contact Followers
                </i>
              </a>
            </li>
          </ul>
        </div> */}
        {this.state.clickedFollower === true ? (
          <div style={{ padding: "30px 0" }}>
            <FollowerCard followerlist={followerlist} />
          </div>
        ) : null}

        {this.state.clickedFollowing === true ? (
          <div style={{ padding: "30px 0" }}>
            <FollowingCard followinglist={followerlist} />
          </div>
        ) : null}

        {this.state.clickedContact === true ? (
          <div style={{ padding: "30px 0" }}>
            <Contact />
          </div>
        ) : null}
      </div>
    );
  }
}

export default MyNetwork;
