/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import "../../index.scss";

import Form from "react-bootstrap/Form";

import {
  Row,
  Col,
  Table,
  Card,
  Dropdown,
  DropdownButton,
  FormControl,
  InputGroup,
} from "react-bootstrap";
import CustomButton from "../components/CustomButton";
import "bootstrap/dist/css/bootstrap.min.css";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { useFormik, ErrorMessage } from "formik";
import { string, object, array } from "yup";
import axios from "../config/axios";
import { useSelector } from "react-redux"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useHistory } from "react-router";

const AddBeneficiary = () => {
  const history = useHistory()
  const [type, setType] = useState();
  const [location, setLocation] = useState("");
  const [imagebeneficiary, setImage] = useState("");
  const [is_location, setlocationflag] = useState(false)
  const [is_image, setimageflag] = useState(false)
  const ngo_id = useSelector((state) => state.phAuth && state.phAuth.ngo && state.phAuth.ngo._id || 0)
  useEffect(() => {
    toast.success("Hola!")
  }, [])
  // *********
  const defaultValues = {
    name: "",
    code: "",
    gender: "",
    type: "",
    dob: "",
    gender: "",
    phone: "",
    mobile: "",
    blood_grp: "",
    caste: "",
    religion: "",
    aadhar: "",
    pancard: "",
    height: "",
    weight: "",
    weight_type: "",
    qualification: "",
    location: "",
    address: "",
    pincode: 0,
    requirement: "",
    has_cause: "",
    is_backed: "",
    backed_by: "",
    status: "",
    created_by: "",
    datetime: "",
  };
  const validationSchema = object().shape({
    name: string().required("Beneficiary name is required"),
    type: string().required("Beneficiary type is required"),
    requirement: string().required("Requirement is required"),
  });
  const onSubmit = (values) => {
    if (!values.image && !values.location) {
      !values.image ? setimageflag(true) : setimageflag(false)
      !values.location ? setlocationflag(true) : setlocationflag(false)
    } else {
      values.is_backed = (values.backed_by.length > 0) ? 1 : 0
      values.backed_by = values.backed_by.join(",")
      values.created_by = ngo_id
      console.log("IMAGE NOT THERE", values)

      const data = new FormData();

      const keys = Object.keys(values);
      keys.forEach((key, index) => {
        data.append(`${key}`, `${values[key]}`);
      });

      data.append("image", imagebeneficiary)
      axios
        .post("beneficiary/create", data)
        .then(({ data, status, message }) => {
          console.log("data, status, message", data, status, message)
          if (status == 1) {
            toast.success(message, { onClose: () => history.push("/beneficiaries") })
          } else {
            toast.error(message, { onClose: () => history.push("/beneficiaries/beneficiaries") })
          }
        })
        .catch((err) => {
          alert(err);
        });
    }
    return;
  };

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    values,
    errors,
    touched,
    isSubmitting,
    isValidating,
    setFieldValue,
  } = useFormik({
    initialValues: defaultValues,
    validationSchema,
    onSubmit,
  });
  // *********




  const handleType = (e) => {
    setType(e.target.value);
  };
  let personType;
  personType = (
    <>
      {" "}
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          DOB
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} name="dob" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Blood Group
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} name="blood_grp" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Phone
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} name="phone" type="number" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Mobile
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} name="mobile" type="number" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Caste
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} name="caste" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Religion
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} name="religion" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Aadhar Card
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} name="aadhar" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Pancard
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} type="text" name="pancard" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Qualification
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} type="text" name="qualification" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Height
        </Form.Label>
        <Col>
          <Form.Control onChange={handleChange} onBlur={handleBlur} name="height" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Weight
        </Form.Label>
        <Col>
          <InputGroup>
            <FormControl onChange={handleChange} onBlur={handleBlur} name="weight" type="text" className="green-focus" />
            <DropdownButton
              as={InputGroup.Append}
              variant="outline-secondary"
              title="POUND"
              id="input-group-dropdown-2"
            >
              <Dropdown.Item href="#">KG</Dropdown.Item>
              <Dropdown.Item href="#">POUND</Dropdown.Item>
            </DropdownButton>
          </InputGroup>
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Gender
        </Form.Label>
        <Col className="form-input-align-center">
          <RadioGroup
            name="gender"
            onChange={handleChange}
            onBlur={handleBlur}
          >
            <div>
              <FormControlLabel
                value="Male"
                control={<Radio color="primary" />}
                label="Male"
              //  onChange={(e) => handleMileStone(e)}
              />
              <FormControlLabel
                value="Female"
                control={<Radio color="primary" />}
                label="Female"
              //   onChange={(e) => handleMileStone(e)}
              />
            </div>
          </RadioGroup>
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
    </>
  );
  return (
    <div className="container">
      <ToastContainer />
      <div style={{ marginBottom: "30px" }}>
        <h2 className="font-bold">Add Beneficiary</h2>
        <p>
          Use the following form to add beneficiaries, which can be later mapped
          to projects. Apart from individuals beneficiaries can be abstract like
          villages, environment, etc.
        </p>
      </div>
      <Card style={{ padding: "30px " }}>
        <Form onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Name
              </Form.Label>
              <Col>
                <Form.Control
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={errors.name}
                  value={values.name}
                  touched={touched.name}
                  name="name"
                  type="text"
                  placeholder="Enter beneficiary name"
                  className="green-focus"
                />
                {touched.name && errors.name && (
                  <div>
                    <span className="text-danger">{errors.name}</span>
                  </div>
                )}
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Code
              </Form.Label>
              <Col>
                <Form.Control
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="code"
                  type="text"
                  placeholder="Enter beneficiary code"
                  className="green-focus"
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Image
              </Form.Label>
              <Col>
                <input name="image" type="file"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  // error={errors.image}
                  // value={values.image}
                  // touched={touched.image}
                  className="position-relative"
                  // onChange={handleFileSelect}
                  onChange={(event) => {
                    const files = event.target.files;
                    let myFiles = Array.from(files);
                    setFieldValue("image", myFiles)
                    setImage(files[0])
                  }}
                />
                {/* {touched.image && errors.image && (
                  <div>
                    <span className="text-danger">{errors.image}</span>
                  </div>
                )} */}
                {/* <ErrorMessage className="text-danger" name="image" /> */}
                {is_image ? (<span className="text-danger">Image is required</span>) : null}
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Beneficiary Type
              </Form.Label>
              <Col className="form-input-align-center">
                <RadioGroup
                  name="type"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={errors.type}
                  value={values.type}
                  touched={touched.type}
                >
                  <div>
                    <FormControlLabel
                      value="Person"
                      control={<Radio color="primary" />}
                      label="Person"

                      onChange={(e) => handleType(e)}
                    />
                    <FormControlLabel
                      value="Abstract"
                      control={<Radio color="primary" />}
                      label="Abstract"
                      onChange={(e) => handleType(e)}
                    />
                  </div>
                  {touched.type && errors.type && (
                    <div>
                      <span className="text-danger">{errors.type}</span>
                    </div>
                  )}
                </RadioGroup>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            {type === "Person" ? personType : null}
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Location
              </Form.Label>
              <Col>
                <PlacesAutoComplete
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={errors.location}
                  value={values.location}
                  touched={touched.location}
                  placeholder="Write first few characters of your city"
                  location={(location) => {
                    setFieldValue("location", location)
                  }}
                />
                {is_location ? (<span className="text-danger">Location is required</span>) : null}
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />

            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Address
              </Form.Label>
              <Col>
                <Form.Control
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="address"
                  type="text"
                  as="textarea"
                  className="green-focus"
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Pincode
              </Form.Label>
              <Col>
                <Form.Control name="pincode" onChange={handleChange}
                  onBlur={handleBlur} type="number" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Beneficiary Requirement
              </Form.Label>
              <Col className="form-input-align-center">
                <RadioGroup
                  name="requirement"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={errors.requirement}
                  value={values.requirement}
                  touched={touched.requirement}
                >
                  <div>
                    <FormControlLabel
                      value="1"
                      control={<Radio color="primary" />}
                      label="One time"
                    />
                    <FormControlLabel
                      value="2"
                      control={<Radio color="primary" />}
                      label="Continuous"
                    />
                  </div>
                  {touched.requirement && errors.requirement && (
                    <div>
                      <span className="text-danger">{errors.requirement}</span>
                    </div>
                  )}
                </RadioGroup>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                name="backed_by"
                className="form-label-custom p-r-15"
              >
                Backed by
              </Form.Label>
              <Col>
                <Table responsive="lg" style={{ marginTop: "-10px" }}>
                  <tr>
                    <td className="table-data ">
                      <FormControlLabel
                        control={<Checkbox color="primary" />}
                        label="NGO"
                        value="NGO"
                        name="backed_by"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </td>
                    <td className="table-data ">
                      <FormControlLabel
                        control={<Checkbox color="primary" />}
                        label="Corporate"
                        value="Corporate"
                        name="backed_by"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </td>
                    <td className="table-data ">
                      <FormControlLabel
                        control={<Checkbox color="primary" />}
                        label="Government"
                        value="Government"
                        name="backed_by"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </td>
                  </tr>
                </Table>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2}></Form.Label>
              <Col>
                <CustomButton content="Create Beneficiary" type="submit" />
              </Col>
            </Form.Row>
          </Form.Group>
        </Form>
      </Card>
    </div>
  );
};
export default AddBeneficiary;
