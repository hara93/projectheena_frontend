import React from "react";
import "../../index.scss";

import * as ReactBootstrap from "react-bootstrap";

import FileUpload from "../components/FileUpload";
import ActivityAccordion from "../components/ActivityAccordion";

import { Card, Row, Col } from "react-bootstrap";
import { FaListOl } from "react-icons/fa";

const activityTracker = [
  {
    heading: "FY 2016-17 - Phase I - Advocacy, Mobilization And Survey",
    list: [
      "Support classes at Communities",
      "Parent awareness for education",
      "Preparatory Camps",
      "Volunteer Mobilization",
    ],
    description: [
      "The survey of out-of-school children will start in the month of April, with the objective of covering all the 64 wards in PCMC area.",
      "Meetings will be organized in various locations of Program area. The aim of these meetings is to make parents aware of the importance of education for their children and RTE provisions.",
      "Preparatory camps will be setup in few locations to prepare parents on their role in enrolment and continuous attendance, also to introduce the children to the idea of formal schooling.",
      "We will approach companies in the month of May and June for mobilizing volunteers. Efforts will be made to reach out to new companies and volunteer groups.",
    ],
    collaborator: [
      "Umang Shah	",
      "Umang Shah	",
      "Umang Shah	",
      "Umang Shah	",
      "Umang Shah	",
    ],

    status: ["WIP	", "WIP	", "WIP	", "WIP	"],

    start_date: [
      "01st Apr, 2016",
      "01st Apr, 2016",
      "01st Apr, 2016",
      "01st Apr, 2016",
    ],
    end_date: [
      "30th Apr, 2016",
      "30th Apr, 2016",
      "30th Apr, 2016",
      "30th Apr, 2016",
    ],
  },
  {
    heading: "FY 2016-17 - Phase I - Advocacy, Mobilization And Survey",
    list: [
      "Support classes at Communities",
      "Parent awareness for education",
      "Preparatory Camps",
      "Volunteer Mobilization",
    ],
    description: [
      "The survey of out-of-school children will start in the month of April, with the objective of covering all the 64 wards in PCMC area.",
      "Meetings will be organized in various locations of Program area. The aim of these meetings is to make parents aware of the importance of education for their children and RTE provisions.",
      "Preparatory camps will be setup in few locations to prepare parents on their role in enrolment and continuous attendance, also to introduce the children to the idea of formal schooling.",
      "We will approach companies in the month of May and June for mobilizing volunteers. Efforts will be made to reach out to new companies and volunteer groups.",
    ],
    collaborator: [
      "Umang Shah	",
      "Umang Shah	",
      "Umang Shah	",
      "Umang Shah	",
      "Umang Shah	",
    ],

    status: ["WIP	", "WIP	", "WIP	", "WIP	"],

    start_date: [
      "01st Apr, 2016",
      "01st Apr, 2016",
      "01st Apr, 2016",
      "01st Apr, 2016",
    ],
    end_date: [
      "30th Apr, 2016",
      "30th Apr, 2016",
      "30th Apr, 2016",
      "30th Apr, 2016",
    ],
  },
];

class ActivityTracker extends React.Component {
  render() {
    return (

      <div className="ibox p-3">
        <div >
          <Row>
            <Col md={12}>
              <div className="item">
                <h3 className="csr_heading"><i class="fas fa-list-ol" style={{ marginRight: '5px' }}></i>Activity Tracker</h3>
              </div>
              <div className="item">
                <ActivityAccordion activityTracker={activityTracker} />

              </div>
            </Col>
          </Row>

        </div>

      </div>
    );
  }
}

export default ActivityTracker;
