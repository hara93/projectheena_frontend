import React from 'react';
import { Col, Row, Table, Dropdown, Modal, Button } from "react-bootstrap";


import { FaFacebook, FaTwitterSquare, FaGooglePlus, FaYoutube, FaThumbsUp, FaTwitter } from 'react-icons/fa';
import { Images } from '../config/Images';
import { Card } from 'react-bootstrap';
import axios from "../config/axios";
import {
    FaShieldAlt
} from "react-icons/fa";
import { connect } from 'react-redux';
import moment from "moment"

class ProjectDetails extends React.Component {
    constructor(props) {
        super(props);
        var start = moment(props.projectdata?.project_start_date);
        var end = moment(props.projectdata?.project_end_date);
        this.state = {
            show: false,
            show1: false,
            duration: Math.abs(moment.duration(start.diff(end)).asDays())
        }
    }

    handleClose = () => this.setState({ show: false });
    handleClose1 = () => this.setState({ show1: false });
    handleShow = () => this.setState({ show: true });
    handleShow1 = () => this.setState({ show1: true });

    render() {
        return (
            <>
                {/* <div className="tab-details" > */}
                <Card className="ibox p-3" >
                    <Row>
                        <Col md={8}>
                            <div className="item">
                                <h3 className="csr_heading"><i class="fas fa-shield-alt"></i> Details About Project</h3>
                            </div>

                            <div className="item project-img">
                                <img src={Images.CSRDetail} alt="project-detail" />
                            </div>
                            <div >
                                <p >{this.props.projectdata?.project_desc}</p>
                                {/* <p >Here few facts & stats on effects of indoor smoke on the health</p>
                                <ul className="facts">
                                    <li>Household air pollution (HAP) is the second largest risk factor for disease burden and accounts for 7.6% of all deaths in children aged under 5 years</li>
                                    <li>According to the 2011 Census, 142 million rural homes that’s almost 85% of total rural households in India depend on traditional biomass fuel for cooking</li>
                                    <li>Among neonates, the mortality rate is almost eight times higher among those exposed to indoor pollution</li>
                                    <li>Nearly two-thirds of India’s population continue to rely on traditional cookstoves</li>
                                    <li>One-third higher risk of ARI  for children living in households that use wood or animal dung than the children living in households that use cleaner fuel, even after controlling for a number of other variables</li>
                                    <li>1.3 million deaths occur in India alone due to indoor air pollution, every year from smoke from cooking, heating & lighting activities</li>
                                    <li >About 780 million people in India are affected by household air pollution, mainly caused by smoke from burning solid fuels for cooking</li>
                                    <li>12-14 kgs of wood is consumed in a day for cooking on traditional stoves</li>
                                    <li>According to WHO, more than 50% of premature deaths due to pneumonia occur among children under 5 are caused by the particulate matter (soot) inhaled from household air pollution</li>
                                    <li>Various research studies suggest strong link of associated between in-house air pollution & premature births, low birth weights, stillbirth, reduced birth length, & congenital anomalies</li>
                                </ul> */}

                            </div>

                            <div className="item project-stats">
                                <h3 className="csr_heading" style={{ marginLeft: '2px', marginTop: '32px' }}><i class="fas fa-tag"></i>    Other Statistics</h3>
                                <ul>
                                    <li style={{ listStyleType: 'none' }}>
                                        <div className="th" style={{ display: 'inline-block' }}>External Link</div>
                                        <div className="tc " style={{ display: 'inline-block' }}>{this.props.projectdata?.external_url}</div>
                                    </li>

                                    <li style={{ listStyleType: 'none' }}>
                                        <div className="th" style={{ display: 'inline-block' }}>Starting From</div>
                                        <div className="tc " style={{ display: 'inline-block' }}>{moment(this.props.projectdata?.project_start_date).format("Do MMM, YYYY")}</div>
                                    </li>

                                    <li style={{ listStyleType: 'none' }}>
                                        <div className="th" style={{ display: 'inline-block' }}>Ending On</div>
                                        <div className="tc " style={{ display: 'inline-block' }}>{moment(this.props.projectdata?.project_end_date).format("Do MMM, YYYY")}</div>
                                    </li>
                                </ul>
                            </div>
                            <Dropdown>
                                <Dropdown.Toggle variant="success" id="dropdown-basic">
                                    Change Project Status
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1" onClick={this.handleShow}>Close Project</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2" onClick={this.handleShow1}>Delete Project</Dropdown.Item>

                                </Dropdown.Menu>
                            </Dropdown>

                            <div className="col-md-12 item project-share">
                                <div className="item-title">
                                    Share with your friends

                                </div>
                                <div style={{ marginTop: '5px', marginLeft: '8px' }}>
                                    <i class="fab fa-facebook-square fa-2x" style={{ color: '#3c5997', marginRight: '26px' }}></i>
                                    <i class="fab fa-twitter-square fa-2x" style={{ color: '#04abed', marginRight: '26px' }}></i>
                                    <i class="fab fa-google-plus-square fa-2x" style={{ color: '#dd4b39', marginRight: '26px' }}></i>
                                    <i class="fab fa-linkedin fa-2x" style={{ color: '#027bb6', marginRight: '26px' }}></i>
                                </div>

                            </div>
                        </Col>
                        <Col md={4}>
                            <div>
                                <h3 className="csr_heading"><i class="fas fa-chart-line"></i> Statistics <a href=""><i class="fas fa-pencil-alt"></i></a></h3>
                                <ul className="donation-stats">
                                    <li
                                        style={{
                                            display: "table-row",
                                            listStyleType: "none",
                                        }}
                                    >
                                        <div className="th">Status</div>
                                        <div className="tc"><span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>{!this.props.projectdata?.project_status || this.props.projectdata?.project_status == 0 ? `Open` : `Open`}</span></div>
                                    </li>
                                    <li
                                        style={{
                                            display: "table-row",
                                            listStyleType: "none",
                                        }}
                                    >
                                        <div className="th">Location</div>
                                        <div className="tc">Khandwa, Madhya Pradesh, India</div>
                                    </li>
                                    <li
                                        style={{
                                            display: "table-row",
                                            listStyleType: "none",
                                        }}
                                    >
                                        <div className="th">Duration</div>
                                        <div className="tc">{this.state.duration} Days</div>
                                    </li>
                                    <li
                                        style={{
                                            display: "table-row",
                                            listStyleType: "none",
                                        }}
                                    >
                                        <div className="th">Budget</div>
                                        <div className="tc">
                                            ₹ {this.props.projectdata?.estimated_cost}
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div className="item causes-supported">
                                <h3 style={{ fontSize: '16px', fontWeight: '600', marginTop: '33px' }}>Causes supported</h3>
                                <div style={{ fontWeight: '500' }}>
                                    {(this.props.projectdata?.causes && this.props.projectdata?.causes.length > 0) ?
                                        this.props.projectdata?.causes.map((element, index) => (

                                            <button
                                                className="p-1 mx-1"
                                                style={{
                                                    backgroundColor: "gainsboro",
                                                    color: "gray",
                                                    border: "1px solid gainsboro",
                                                    marginRight: "1%",
                                                    marginBottom: "1%",
                                                }}
                                            >
                                                {element.cause_name}
                                            </  button>

                                        ))
                                        : `No causes selected`
                                    }
                                </div>
                            </div>
                            <div className="item causes-supported">
                                <h3 style={{ fontSize: '16px', fontWeight: '600', marginTop: '33px' }}>Tags</h3>
                                <div style={{ fontWeight: '500' }}>
                                    {(this.props.projectdata?.tags && this.props.projectdata?.tags.length > 0) ?
                                        this.props.projectdata?.tags.map((element, index) => (

                                            <button
                                                className="p-1 mx-1"
                                                style={{
                                                    backgroundColor: "gainsboro",
                                                    color: "gray",
                                                    border: "1px solid gainsboro",
                                                    marginRight: "1%",
                                                    marginBottom: "1%",
                                                }}
                                            >
                                                {element.name}
                                            </  button>

                                        ))
                                        : `No tags selected`
                                    }
                                </div>
                            </div>
                            <div className="item causes-supported">
                                <h3 style={{ fontSize: '16px', fontWeight: '600' }}>Collaborated With</h3>
                                <div style={{ fontWeight: '500' }}>
                                    {(this.props.projectdata?.project_collaborators && this.props.projectdata?.project_collaborators.length > 0) ?
                                        this.props.projectdata?.project_collaborators.map((element, index) => (
                                            <button
                                                className="p-1 mx-1"
                                                style={{
                                                    backgroundColor: "gainsboro",
                                                    color: "gray",
                                                    border: "1px solid gainsboro",
                                                    marginRight: "1%",
                                                    marginBottom: "1%",
                                                }}
                                            >
                                                {element.collaborator_name}
                                            </  button>
                                        ))
                                        : `No collabortors`
                                    }
                                </div>
                            </div>
                            <div className="item author">
                                <div className="author-content">
                                    <div className="content-holder">
                                        <h3 style={{ fontSize: '16px', fontWeight: '600' }}>{this.props.user.first_name + ' ' + this.props.user.last_name}</h3>
                                        <p className="no-margins" style={{ fontWeight: '500', width: '105%' }}>With its limited resources, it is working amongst the marginalized Tribal & Dalit communities of Khalwa Block in Khandwa district of Madhya Pradesh. Its involvements includes: Awareness Generation on Food Security related schemes, Research & Documentation</p>
                                        <a href="http://projectheena.in/ngo/spandan" class="btn  btn-outline">View Creator's Profile</a>
                                    </div>
                                    <div class="img-holder">
                                        <a href="http://projectheena.in/ngo/spandan">
                                            <img style={{ width: '100%' }} src="http://projectheena.in/uploads/ngo/35147072124470/profileImage/images/spandan.jpg" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <Modal show={this.state.show} onHide={this.handleClose} style={{ opacity: '1' }}>
                                <Modal.Header closeButton >
                                    <Modal.Title>Attention: Read this immediately <a onClick={this.handleClose}><i class="fas fa-times" style={{ marginLeft: '38%' }} ></i></a></Modal.Title>
                                </Modal.Header>
                                <Modal.Body style={{ margin: '20px 20px 20px 6px', width: '100%' }}>In order to close this project you have to set all milestones as achieved first.</Modal.Body>
                            </Modal>
                            <Modal show={this.state.show1} onHide={this.handleClose1} style={{ opacity: '1' }}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Delete Project <a onClick={this.handleClose1}><i class="fas fa-times" style={{ marginLeft: '69%' }} ></i></a></Modal.Title>
                                </Modal.Header>
                                <Modal.Body>Do note: you are about to delete a project. This activity can't be undone.

                                    We hope you have updated all your stakeholders about this activity.</Modal.Body>
                                <Modal.Footer>
                                    <button className="p-2" style={{ backgroundColor: '#c2c2c2', border: '1px solid #c2c2c2', color: 'white' }} onClick={this.handleClose1}>Cancel</button>
                                    <button className="btn btn-outline"> Delete Changes</button>
                                </Modal.Footer>
                            </Modal>
                        </Col>
                    </Row>
                </Card>
            </>
        )
    }
}

const mapStateToProps = function (state) {
    return { user: state.phAuth.user, projectdata: state.project.projectdata }
};
export default connect(mapStateToProps)(ProjectDetails);