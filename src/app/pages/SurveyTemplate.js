import React, {useState}from 'react';
import { Tab, Tabs, Nav, Row, Col , InputGroup, FormControl} from 'react-bootstrap';
import FormCard from '../components/FormCard';


// import {Link} from 'react-scroll';

const details =[
    {    
        heading:<i class="fas fa-plus fa-3x" style={{marginLeft:'37%'}}></i>,
        content:<p style={{marginLeft:'35%'}}>Blank Form</p>
    },
    {
        heading:<i class="fas fa-file-excel fa-3x" style={{marginLeft:'37%'}}></i>,
        content:<p style={{marginLeft:'25%'}}>Import XLS form</p>
    },
    {
        heading:"Personal Information Survey",
        content:"Explore Collect's capabilities by using this form with different question types."
    },
    {
        heading:"Expense Tracking Form",
        content:"A standard form to track all kinds of expenses."
    },
    {
        heading:"Expense Tracking Form",
        content:"A standard form to track all kinds of expenses."
    },
    {
        heading:"Expense Tracking Form",
        content:"A standard form to track all kinds of expenses."
    },
    {
        heading:"Expense Tracking Form",
        content:"A standard form to track all kinds of expenses."
    },
    {
        heading:"Expense Tracking Form",
        content:"A standard form to track all kinds of expenses."
    },
]


const SurveyTemplate = () =>{
  const [chosen, setChosen] = useState("Details");

  
  
    return(
        <>
      
       <Tab.Container id="left-tabs-example" defaultActiveKey="first" >
  <Row style={{marginTop:'7%'}}>
    <Col sm={3}>
      <Nav variant="pills" className="flex-column">
        <Nav.Item>
          <Nav.Link eventKey="first" >Most Popular</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="second" >Health </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="third" >Market Research </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="fourth" >Education</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="fifth" >Monitoring and evaluation</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="sixth" >Nutrition</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="seventh" >Energy</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="eighth" >Agriculture</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="ninth" >Water and Sanitation</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="tenth" >Audits and Inspections</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="eleventh" >Net Promotor Score</Nav.Link>
        </Nav.Item>
      </Nav>

    
    </Col>
    <Col sm={9} >
      <Tab.Content style={{backgroundColor:'#f0f0f0',height:'100%'}}>
        <Tab.Pane eventKey="first" style={{opacity:'100'}}>
         
           <InputGroup  style={{marginBottom:'8%'}}>
    <FormControl
      placeholder="Search from 47 Templates"
      aria-label="Recipient's username"
      aria-describedby="basic-addon2"
    />
    <InputGroup.Append>
      <InputGroup.Text  id="basic-addon2"><i class="fas fa-search"></i></InputGroup.Text>
    </InputGroup.Append>
  </InputGroup>

<div style={{height:'100%'}} >
  <h3   style={{color:'#4caf4f', fontWeight:'600'}}>Most Popular</h3>
  <div style={{overflow:'scroll', maxHeight:'400px'}}>
  <FormCard details={details} />     
  </div>
  
</div>
 

 
 

         
        </Tab.Pane>
        <Tab.Pane eventKey="second" style={{opacity:'100'}}>
       
        </Tab.Pane>
      </Tab.Content>
    </Col>
  </Row>
</Tab.Container>
        </>
    )
}
export default SurveyTemplate;