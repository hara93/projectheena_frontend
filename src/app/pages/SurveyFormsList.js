/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import { Container, Form, Row, Col, Modal, Button } from "react-bootstrap";
import CustomButton from "../components/CustomButton";
import SurveyFormName from "../components/SurveyFormName";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const SurveyFormsList = () => {
  const [number, setNumber] = useState(0);
  const [forms, setForms] = useState([
    { name: "first survey", type: "draft" },
    { name: "second survey", type: "draft" },
    { name: "third survey", type: "live" },
    { name: "fourth survey", type: "draft" },
    { name: "fifth survey", type: "live" },
  ]);
  const [count, setCount] = useState(0);

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <Container style={{ padding: "30px" }}>
        <Row style={{ margin: "15px 0 15px 0" }}>
          <Col xs={9} style={{ margin: "0", padding: "0" }}>
            <div
              style={{
                display: "flex",
                border: "1px solid #cccccc",
                alignItems: "center",
                width: "100%",

                backgroundColor: "white",
              }}
            >
              <Form.Control
                type="text"
                placeholder={`search ${number} forms`}
                style={{ border: "0" }}
              />
              <i class="fas fa-search" style={{ marginRight: "10px" }}></i>
            </div>
          </Col>
          <Col style={{ margin: "0", padding: "0" }}>
            <div style={{ margin: "0 10px 0 10px", width: "100%" }}>
              {/* <CustomButton content="CREATE FORM" style={{ width: "100%" }} /> */}
              <button
                type="button"
                style={{
                  border: "0",
                  backgroundColor: "green",
                  color: "white",
                  width: "100%",
                  height: "32px",
                }}
                onClick={handleShow}
              >
                CREATE FORM
              </button>
            </div>
          </Col>
        </Row>
        <Row
          style={{ margin: "0", display: "flex", justifyContent: "flex-end" }}
        >
          <div style={{ display: "flex", alignItems: "center", margin: "0" }}>
            <span style={{ color: "gray" }}>Show</span>

            <Form.Control
              as="select"
              style={{
                border: "0",
                marginRight: "25px",
                width: "fit-content",
                backgroundColor: "#ffffff00",
              }}
              onChange={(e) => {
                const select = e.target.value;
              }}
            >
              <option value="all">All forms</option>
              <option style={{ color: "green" }} value="live">
                Live
              </option>
              <option value="draft">Draft</option>
            </Form.Control>

            <span style={{ color: "gray" }}>Sort By</span>

            <Form.Control
              as="select"
              style={{
                border: "0",
                width: "fit-content",
                backgroundColor: "#ffffff00",
              }}
            >
              <option>Modified:New to Old</option>
              <option>Modified:Old to New</option>
              <option>Published:New to Old</option>
              <option>Published:Old to New</option>
            </Form.Control>
          </div>
        </Row>
        <Row style={{ margin: "15px 0 15px 0" }}>
          <div
            style={{
              width: "100%",
              overflow: "auto",
              maxHeight: "600px",
              paddingRight: "15px",
            }}
          >
            {/* <SurveyFormName name="first survey" type="Draft" />
            <SurveyFormName name="2nd survey" type="Draft" />

            <SurveyFormName name="3rd survey" type="Draft" />
            <SurveyFormName name="4th survey" type="Draft" />
            <SurveyFormName name="5th survey" type="Draft" /> */}
            {forms.map((form, index) => (
              <SurveyFormName name={form.name} type={form.type} />
            ))}
          </div>
        </Row>
      </Container>

      <Modal
        show={show}
        onHide={handleClose}
        animation={false}
        size="xs"
        centered
      >
        <div
          style={{
            height: "60px",
            borderBottom: "1px solid #cccccc",
            padding: "15px 10px 5px 10px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h4>Set a title of your new form</h4>
          <Button
            variant="secondary"
            style={{ height: "30px" }}
            onClick={handleClose}
          >
            <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div
            style={{
              minHeight: "250px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <div style={{ position: "absolute", width: "25rem" }}>
              <Form.Control
                type="text"
                maxLength={220}
                placeholder="Ex: My First Form"
                size="lg"
                onChange={(e) => setCount(e.target.value.length)}
              />
            </div>

            <div
              style={{
                position: "relative",
                top: "30px",
                left: "127px",
                fontSize: "16px",
              }}
            >
              <Form.Text
                className="text-muted"
                muted
                style={{ justifyContent: "flex-end" }}
              >
                {count}/220
              </Form.Text>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <FormControlLabel
            control={<Checkbox name="formtomobile" />}
            label="Send this form to mobile"
          />
          <CustomButton content="Add Questions" />
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default SurveyFormsList;
