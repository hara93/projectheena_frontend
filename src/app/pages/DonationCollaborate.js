import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';

class DonationCollaborate extends React.Component {
    render() {
        return (
            <>
                <Card style={{ marginBottom: '5%' }}>
                    <Row className="p-5">
                        <Col md={3} >
                            <p style={{ textAlign: 'center' }}>Projects</p>
                            <h1 style={{ textAlign: 'center' }}>31</h1>
                        </Col>
                        <Col md={3}>
                            <p style={{ textAlign: 'center' }}>Amount</p>
                            <h1 style={{ textAlign: 'center' }}>31</h1>
                        </Col>
                        <Col md={3}>
                            <p style={{ textAlign: 'center' }}>Unhealthy Projects</p>
                            <h1 style={{ textAlign: 'center' }}>2M</h1>
                        </Col>
                        <Col md={3}>
                            <p style={{ textAlign: 'center' }}>Beneficiaries</p>
                            <h1 style={{ textAlign: 'center' }}>0</h1>
                        </Col>
                    </Row>
                </Card>


                <Row  >
                    <Col md={6} >
                        <Card>
                            <h5 className="px-3 py-1" style={{ marginTop: '2%', marginLeft: '2%' }}>Project Stats</h5>
                            <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                            <Row>
                                <Col md={2}>

                                    <img src='http://projectheena.in/assets/img/team-dashboard/team-engagement.png' style={{ width: '200%', marginLeft: '28%' }}></img>

                                </Col>
                                <Col md={1}></Col>
                                <Col md={9}>
                                    <p>This section highlights health for each project that you are part of</p>
                                </Col>
                            </Row>
                            <div className="p-4" >
                                <Table striped bordered hover  >
                                    <thead>
                                        <tr>
                                            <th>Sr. No</th>
                                            <th>Project Health</th>
                                            <th>Count</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Excellent</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Excellent</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Excellent</td>
                                            <td>1</td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </div>

                        </Card>

                    </Col>

                    <Col md={6}>
                        <Card>
                            <h5 className="px-3 py-1" style={{ marginTop: '2%', marginLeft: '2%' }}>Budget Stats</h5>
                            <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                            <Row>
                                <Col md={2}>
                                    <img src='http://projectheena.in/assets/img/team-dashboard/team-engagement.png' style={{ width: '200%', marginLeft: '28%' }}></img>
                                </Col>
                                <Col md={1}></Col>
                                <Col md={9}>
                                    <p>	This section highlights total budget for all your project and the spent and pending amount.</p>
                                </Col>
                            </Row>
                            <div className="p-4" >
                                <Table striped bordered hover  >

                                    <tbody>
                                        <tr>
                                            <td>Total Budget</td>
                                            <td>76154917</td>
                                        </tr>
                                        <tr>
                                            <td>Spent Budget </td>
                                            <td>76154917</td>

                                        </tr>
                                        <tr>
                                            <td>Pending Budget</td>

                                            <td>76154917</td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </div>
                        </Card>
                    </Col>
                </Row>


            </>
        )
    }
}
export default DonationCollaborate;