/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import MuiAccordion from "@material-ui/core/Accordion";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import {
  Card,
  Row,
  Col,
  Form,
  Dropdown,
  DropdownButton,
  Button,
  Tabs,
  Tab,
} from "react-bootstrap";
import InputGroup from "react-bootstrap/InputGroup";
import DatePicker from "react-datepicker";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
import Modal from "react-bootstrap/Modal";
import ProfessionalTab from "../components/ProfessionalTab";
import CreativeTab from "../components/CreativeTab";
import SocialTab from "../components/SocialTab";
import CustomButton from "../components/CustomButton";
import { connect, useSelector, useDispatch } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import GoogleLocation from "../components/GoogleLocation"
import Geocode from "react-geocode";
import { getSkillsList } from "../../redux/reducers/common/actionCreator";
import axios from "../config/axios";
import { setUserData } from "../../redux/reducers/auth/actionCreator";
import { ToastContainer, toast } from 'react-toastify';
import { useHistory } from "react-router-dom";
import { Images } from "../config/Images";

Geocode.setApiKey("AIzaSyAI_qrVCRazEORV3rSrDrP97S8NzsBqfFc");
Geocode.setLanguage("en");
Geocode.setRegion("in");
Geocode.setLocationType("ROOFTOP");
Geocode.enableDebug();
const Accordion = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 0,
    },
    "&:before": {
      display: "none",
    },
    "&$expanded": {
      margin: "0",
    },
  },

  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 37,
    "&$expanded": {
      minHeight: 37,
      color: "#4caf4f",
    },
  },
  content: {
    "&$expanded": {
      margin: "0",
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiAccordionDetails);

const EditProfileUser = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [lat, setLat] = useState("")
  const [lng, setLng] = useState("")
  const [toggleState, setToggleState] = useState(false);
  const skillList = useSelector((state) => (state.common && state.common.skillsList) || null);
  console.log("skillList ========> ", skillList)
  useEffect(() => {
    dispatch(getSkillsList());
    // console.log(milestones);
  }, []);
  const handleExactAddress = () => {
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    function success(pos) {
      var crd = pos.coords;
      setLng(crd.longitude)
      setLat(crd.latitude)
      getAddress(crd.latitude, crd.longitude)
    }

    function error(err) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    }

    navigator.geolocation.getCurrentPosition(success, error, options);
  }

  const getAddress = (lat, lng) => {
    Geocode.fromLatLng(lat, lng).then(
      (response) => {
        const address = response.results[0].formatted_address;
        let city, state, country, postal_code, political;
        for (let i = 0; i < response.results[0].address_components.length; i++) {
          for (let j = 0; j < response.results[0].address_components[i].types.length; j++) {
            // console.log("HERE", response.results[0].address_components[i].types[j], response.results[0].address_components[i].long_name)
            switch (response.results[0].address_components[i].types[j]) {
              case "locality":
                city = response.results[0].address_components[i].long_name;
                break;
              case "administrative_area_level_1":
                state = response.results[0].address_components[i].long_name;
                break;
              case "country":
                country = response.results[0].address_components[i].long_name;
                break;
              case "postal_code":
                postal_code = response.results[0].address_components[i].long_name;
                break;
            }
          }
        }
        var pincode = (!postal_code || postal_code == '') ? '' : ' - ' + postal_code
        setExactAddress(city + ', ' + state + ', ' + country + pincode)
      },
      (error) => {
        console.error(error);
      }
    );
  }

  const useStyles = makeStyles((theme) => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
  }));
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState("panel1");

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  const [count, setCount] = useState(0);
  var maxCount = 600;
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [show, setShow] = useState(false);
  const [location, setLocation] = useState("")
  // const handleClose = () => setShow(false);
  const handleClose = () => {
    setShow(false);
    setSkill([]);
  };
  const handleShow = () => setShow(true);
  const [key, setKey] = useState("home");
  const [profilePic, setProfilePic] = useState("")
  const [showLocationModal, setShowLocationModal] = useState(false)
  const [address, setAddress] = useState("")
  const [exact_address, setExactAddress] = useState("")
  const [skill, setSkill] = useState([]);
  const handleDelete = () => {
    console.log("DELETE")
    axios.post(`user/update/${props.user._id}`, { image: "" })
      .then(({ data, message, status }) => {
        if (status == 1) {
          toast.success(message, { onClose: () => history.push("/userprofile") })
        } else {
          toast.error(message)
        }
        dispatch(setUserData(data))
      })
  }
  const handleSkill = (e) => {
    if (e.target.checked) {
      setSkill([...skill, e.target.value]);
    } else {
      skill.splice(skill.indexOf(e.target.value), 1);
      setSkill([...skill]);
    }
  };
  const selectSkill = () => {
    setShow(false);
  };
  const [ProfileData, setProfileData] = useState({
    first_name: props.user.first_name,
    last_name: props.user.last_name,
    gender: props.user.gender,
    dob: !props.user.dob || props.user.dob == '' ? '' : props.user.dob,
    employer: props.user.employer,
    details: props.user.details,
    overview: props.user.overview,
    user_location: props.user.user_location,
    url: props.user.url,
    latitude: !props.user.latitude || props.user.latitude == '' ? 'undefined' : props.user.latitude,
    longitude: !props.user.longitude || props.user.longitude == '' ? '' : props.user.longitude,
    current_location: props.user.current_location,
    country_id: 0,
    current_location_altitude: !props.user.current_location_altitude || props.user.current_location_altitude == '' ? '' : props.user.current_location_altitude,
    current_location_latitude: !props.user.current_location_latitude || props.user.current_location_latitude == '' ? '' : props.user.current_location_latitude,
    current_location_longitude: !props.user.current_location_longitude || props.user.current_location_longitude == '' ? '' : props.user.current_location_longitude,
    user_contact_number: props.user.user_contact_number,
    country_code: "+91",
    email_id: props.user.email_id,
    user_primary_email: props.user.user_primary_email,
    designation: props.user.designation,
    skills_mapped: props.user.skills_mapped,
    cases_mapped: props.user.cases_mapped,
    available_day: props.user.available_day,// 'possible values are 0-6 where 0 is Sunday, 7=Weekday, 8=Weekend, 9=Not available'
    available_slot: props.user.available_slot, // 'values are 0/1/2/3/4/9 for morning/afternoon/evening/night/flexible/unavailable'
    twitter_handle: props.user.twitter_handle,
    linkedin_id: props.user.linkedin_id,
    linkedin_link: props.user.linkedin_link,
    fb_id: props.user.fb_id,
    fb_link: props.user.fb_link,
    image: props.user.image,
  });
  const handleAddress = () => {
    // console.log("SET ADDRESS HERE", address)
    setShowLocationModal(false)
  }
  const handleLocationModalClose = () => {
    setShowLocationModal(false)
  }
  const handleLocationModalShow = () => {
    setShowLocationModal(true)
  }

  const handleChangeInputFields = e => {
    const { name, value } = e.target;
    setProfileData(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleLogoSelect = (fileData) => {
    setProfilePic(fileData.currentTarget.files[0]);
  };
  var temp = [];

  if (skill) {
    skillList.Professional.forEach((ele) => {
      if (skill.includes(ele.skill_name)) {
        temp.push(ele.skill_name);
      }
    });
    skillList.Social.forEach((ele) => {
      if (skill.includes(ele.skill_name)) {
        temp.push(ele.skill_name);
      }
    });
    skillList.Creative.forEach((ele) => {
      if (skill.includes(ele.skill_name)) {
        temp.push(ele.skill_name);
      }
    });
  }
  const handleSubmit = () => {
    const toUpdate = new FormData();
    const keys = Object.keys(ProfileData);
    keys.forEach((key, index) => {
      toUpdate.append(`${key}`, `${ProfileData[key]}`);
    });
    if (skill.length > 0) {
      toUpdate.append("skills_mapped", skill)
    }
    if (lat) {
      toUpdate.append("latitude", lat)
      toUpdate.append("current_location_latitude", lat)
    }
    if (lng) {
      toUpdate.append("longitude", lng)
      toUpdate.append("current_location_longitude", lng)
    }
    if (profilePic) {
      toUpdate.append("image", profilePic)
    }
    // console.log("FIRST_NAME", toUpdate.get("first_name"))
    // for (var pair of toUpdate.entries()) {
    // console.log(pair[0] + ', ' + pair[1]);
    // }
    axios.post(`user/update/${props.user._id}`, toUpdate)
      .then(({ data, message, status }) => {
        if (status == 1) {
          toast.success(message, { onClose: () => history.push("/userprofile") })
        } else {
          toast.error(message)
        }
        dispatch(setUserData(data))
      })
  }

  return (
    <div>
      <ToastContainer />
      <Card style={{ padding: "15px" }}>
        <Accordion
          square
          expanded={expanded === "panel1"}
          onChange={handleChange("panel1")}
          style={{ boxShadow: "none", marginBottom: "10px" }}
        >
          <AccordionSummary
            aria-controls="panel1d-content"
            id="panel1d-header"
            style={{ minHeight: "37px", margin: "0" }}
            className="userprofile-accordion-title"
          >
            Personal details
          </AccordionSummary>
          <AccordionDetails>
            <Form style={{ width: "100%" }}>
              <Form.Group>
                <Form.Row style={{ marginTop: "5px" }}>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    First Name
                  </Form.Label>
                  <Col>
                    <Form.Control
                      type="text"
                      placeholder="Enter first name"
                      className="green-focus"
                      name="first_name"
                      value={ProfileData.first_name}
                      onChange={handleChangeInputFields}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Last name
                  </Form.Label>
                  <Col>
                    <Form.Control
                      type="text"
                      placeholder="Enter last name"
                      name="last_name"
                      className="green-focus"
                      value={ProfileData.last_name}
                      onChange={handleChangeInputFields}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Gender
                  </Form.Label>
                  <Col className="form-input-align-center">
                    <RadioGroup
                      name="gender"
                      onChange={handleChangeInputFields}
                      value={ProfileData.gender}
                    >
                      <div>
                        <FormControlLabel
                          value="Male"
                          control={<Radio color="primary" />}
                          label="Male"
                        />
                        <FormControlLabel
                          value="Female"
                          control={<Radio color="primary" />}
                          label="Female"
                        />
                      </div>
                    </RadioGroup>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Date of Birth
                  </Form.Label>
                  <Col>
                    {/* <DatePicker
                      placeholderText="Date of Birth"
                      selected={startDate}
                      dateFormat="dd-MM-yyyy"
                      selectsStart
                      startDate={startDate}
                      onChange={(date) => setStartDate(date)}
                      popperPlacement="top-end"
                    /> */}
                    <TextField
                      onChange={handleChangeInputFields}
                      id="date"
                      name="dob"
                      label="Date of Birth"
                      type="date"
                      value={ProfileData.dob}
                      // defaultValue="2017-05-24"
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    User email
                  </Form.Label>
                  <Col>
                    <Form.Control
                      onChange={handleChangeInputFields}
                      type="email"
                      name="email_id"
                      placeholder="Enter email"
                      className="green-focus"
                      value={ProfileData.email_id}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Profile Image
                  </Form.Label>
                  <Col className="form-input-align-center ">
                    <input name="logo" type="file" onChange={handleLogoSelect} />
                  </Col>
                </Form.Row>
                <div style={{ marginLeft: "17%" }}>
                  <img style={{ width: '10%' }} src={!ProfileData.image || ProfileData.image == '' ? Images.Social_work : `http://localhost:5000/uploads/images/user/${ProfileData.image}`}></img>
                </div>
                <div style={{ color: "#4caf50", marginLeft: "20%" }}>
                  <a onClick={handleDelete}>Delete</a>
                </div>
                <hr className="form-horizontal-line-20m" />
              </Form.Group>
            </Form>
          </AccordionDetails>
        </Accordion>
        <Accordion
          square
          expanded={expanded === "panel2"}
          onChange={handleChange("panel2")}
          style={{ boxShadow: "none", marginBottom: "10px" }}
        >
          <AccordionSummary
            aria-controls="panel2d-content"
            id="panel2d-header"
            className="userprofile-accordion-title"
          >
            Karma profile detail
          </AccordionSummary>
          <AccordionDetails>
            <Form style={{ width: "100%" }}>
              <Form.Group>
                <Form.Row style={{ marginTop: "5px" }}>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom  p-r-15"
                  >
                    City
                  </Form.Label>
                  <Col>
                    <PlacesAutoComplete
                      placeholder="Write first few characters of your city"
                      location={(location) => {
                        setLocation(location);
                      }}
                      lat={(lat) => setLat(lat)}
                      lng={(lng) => setLng(lng)}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom  p-r-15"
                  >
                    Exact Location (Optional)
                  </Form.Label>
                  <Col>
                    <div style={{ display: "flex" }}>
                      <button
                        type="button"
                        className="userprofile-locate-btn"
                        style={{ fontSize: "15px" }}
                        onClick={handleExactAddress}
                      >
                        <i class="fas fa-crosshairs"></i> Let Browser locate me
                      </button>
                      {" or "}
                      <button
                        type="button"
                        className="userprofile-map-btn"
                        style={{ fontSize: "15px" }}
                        onClick={handleLocationModalShow}
                      >
                        {" "}
                        <i class="fas fa-location-arrow "></i> Use Map{" "}
                      </button>
                    </div>
                    <spam className="help-block">Selected Location: {exact_address}</spam>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom  p-r-15"
                  >
                    Preferred Causes
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      className="green-focus"
                    />
                    <spam className="help-block">
                      No Cause selected. Click the box to choose
                    </spam>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Skills Required
                  </Form.Label>
                  <Col className="form-input-align-center">
                    <InputGroup>
                      <Form.Control
                        size="lg"
                        type="text"
                        onClick={handleShow}
                        id="skills"
                        name="skills_mapped"
                        className="green-focus"
                        value={skill}
                      />
                      <InputGroup.Append style={{ width: "45px" }}>
                        <Button
                          variant="secondary"
                          id=""
                          onClick={() => {
                            setSkill([]);
                          }}
                          style={{ width: "70px" }}
                        >
                          <i class="fas fa-times" style={{ color: "white" }}></i>
                        </Button>
                      </InputGroup.Append>
                    </InputGroup>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Contact Number
                  </Form.Label>
                  <Col>
                    <InputGroup>
                      <DropdownButton
                        as={InputGroup.Prepend}
                        variant="outline-secondary"
                        title="+91"

                        id="input-group-dropdown-1"
                      >
                        <Dropdown.Item href="#">+91</Dropdown.Item>
                      </DropdownButton>
                      <Form.Control
                        type="text"
                        onChange={handleChangeInputFields}
                        name="user_contact_number"
                        aria-describedby="basic-addon1"
                        value={ProfileData.user_contact_number}
                      />
                    </InputGroup>
                    <spam className="help-block">We do not spam</spam>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Connect other accounts
                  </Form.Label>
                  <Col>
                    <div style={{ displpay: "flex" }}>
                      <button type="button" className="userprofile-map-btn mx-1">
                        Facebook connect
                      </button>
                      <button type="button" className="userprofile-map-btn mx-1">
                        LinkedIn connect
                      </button>
                    </div>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
              </Form.Group>
            </Form>
          </AccordionDetails>
        </Accordion>
        <Accordion
          square
          expanded={expanded === "panel3"}
          onChange={handleChange("panel3")}
          style={{ boxShadow: "none", marginBottom: "10px" }}
        >
          <AccordionSummary
            aria-controls="panel3d-content"
            id="panel3d-header"
            className="userprofile-accordion-title"
          >
            Optional details
          </AccordionSummary>
          <AccordionDetails>
            <Form style={{ width: "100%" }}>
              <Form.Group>
                <Form.Row style={{ marginTop: "5px" }}>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Time to help
                  </Form.Label>
                  <Col lg={5}>
                    <Form.Control
                      as="select"
                      custom
                      onChange={handleChangeInputFields}
                      name="available_day"
                      value={ProfileData.available_day}
                    //onClick={(e) => setFreq(e.target.value)}
                    >
                      <option>Select Day</option>

                      <option value='0'>Sunday</option>
                      <option value='1'>Monday</option>
                      <option value='2'>Tuesday</option>
                      <option value='3'>Wednesday</option>
                      <option value='4'>Thursday</option>
                      <option value='5'>Friday</option>
                      <option value='6'>Saturday</option>
                      <option value='7'>Weekdays</option>
                      <option value='8'>Weekends</option>
                      <option value='9'>Unavailable</option>
                    </Form.Control>
                  </Col>
                  <Col lg={5}>
                    {" "}
                    <Form.Control
                      as="select"
                      custom
                      name="available_slot"
                      //onClick={(e) => setFreq(e.target.value)}
                      onChange={handleChangeInputFields}
                      value={ProfileData.available_slot}
                    >
                      <option>Select Time</option>
                      <option value="0">Morning</option>
                      <option value="1">Afternoon</option>
                      <option value="2">Evening </option>
                      <option value="3">Night</option>
                      <option value="4">Flexible</option>
                      <option value="9">Unavailable</option>
                    </Form.Control>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom  p-r-15"
                  >
                    About Yourself
                  </Form.Label>
                  <Col>
                    <Form.Control
                      type="text"
                      as="textarea"
                      maxLength="600"
                      minLength="25"
                      rows="5"
                      name="overview"
                      placeholder="A detailed description about the kinds of opportunities you want to volunteer for. Your work experience, preferred mode of helping and your big idea of how we can change the world"
                      onChange={(e) => setCount(e.target.value.length)}
                      onChange={handleChangeInputFields}
                      className="green-focus"
                      style={{ borderRadius: "0" }}
                      value={ProfileData.overview}
                    />
                    <span className="help-block">
                      Min: 25 chars | Max: 600 chars | Remaining Chars:{" "}
                      {maxCount - count}
                    </span>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Your Website
                  </Form.Label>
                  <Col>
                    <Form.Control
                      type="text"
                      placeholder=""
                      onChange={handleChangeInputFields}
                      className="green-focus"
                      name="url"
                      value={ProfileData.url}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Twitter handle
                  </Form.Label>
                  <Col>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text
                          style={{
                            marginRight: "0",
                            background: "transparent",
                          }}
                          size="lg"
                          className="green-focus"
                        >
                          @
                        </InputGroup.Text>
                      </InputGroup.Prepend>

                      <Form.Control
                        type="text"
                        onChange={handleChangeInputFields}
                        className="green-focus"
                        name="twitter_handle"
                        value={ProfileData.twitter_handle}
                      />
                    </InputGroup>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Employer
                  </Form.Label>
                  <Col>
                    <Form.Control
                      type="text"
                      placeholder=""
                      onChange={handleChangeInputFields}
                      className="green-focus"
                      name="employer"
                      value={ProfileData.employer}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Designation
                  </Form.Label>
                  <Col>
                    <Form.Control
                      type="text"
                      placeholder=""
                      name="designation"
                      className="green-focus"
                      onChange={handleChangeInputFields}
                      value={ProfileData.designation}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line-20m" />
              </Form.Group>
            </Form>
          </AccordionDetails>
        </Accordion>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <CustomButton content="Update Profile" type="submit" onClick={handleSubmit} />
        </div>
      </Card>
      <Modal show={showLocationModal} onHide={handleLocationModalClose} style={{ opacity: '1' }} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Select your location on map</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ height: "450px" }} >
          <GoogleLocation address_selected={(e) => setAddress(e)} address_lng={(e) => setLng(e)} address_lat={(e) => setLat(e)} />
          <div style={{ position: "absolute", bottom: 0, width: '100%', margin: '0 auto', textAlign: "center" }}>
            <p>{"Selected Address: " + address}</p>
            <a style={{ color: "#4caf4f" }} onClick={handleAddress}>Set this location</a>
          </div>
        </Modal.Body>
      </Modal>
      <Modal show={show} onHide={handleClose} animation={false} centered>
        {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
        <div className="modal-header-custom">
          <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
            Select Skills
          </h4>
          <Button variant="secondary" style={{ height: "30px" }} onClick={handleClose}>
            <i class="fas fa-times" style={{ paddingRight: "0", color: "white" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ borderBottom: "1px solid #cccccc" }}>
            <Tabs id="controlled-tab-example" activeKey={key} onSelect={(k) => setKey(k)}>
              <Tab eventKey="home" title="Professional" style={{ opacity: "100" }}>
                <ProfessionalTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
              <Tab eventKey="profile" title="Creative" style={{ opacity: "100" }}>
                <CreativeTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
              <Tab eventKey="contact" title="Social" style={{ opacity: "100" }}>
                <SocialTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
            </Tabs>
          </div>
          <div style={{ marginTop: "50px" }}>
            <p>Maximum skills allowed: 5. ({skill.length} Of 5 Skills selected)</p>
            <p>Select as many skills relevant, for better result</p>
            <p>Selected Skills -</p>
            {skill.map((ele) => (
              <div style={{ display: "flex", flexDirection: "column", width: "fit-content" }}>
                <p className="skill-tags">{ele}</p>
              </div>
            ))}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Select Skills" onClick={selectSkill} />
        </Modal.Footer>
      </Modal>
    </div>
  );
};

const mapStateToProps = function (state) {
  return {
    user: state.phAuth.user,
  };
};

export default connect(mapStateToProps)(EditProfileUser);
