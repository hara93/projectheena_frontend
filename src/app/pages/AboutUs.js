import React from "react";
import { Card, Row, Col } from "react-bootstrap";

const AboutUs = () => {
  return (
    <>

      <div className="banner">
        <div className="banner-img">
          <div id="parallex-bg" style={{ backgroundImage: "url('http://projectheena.in/assets/img/static/superhero-kid.jpg') " }}></div>
        </div>
      </div>

      <div className="container">
        <div className="p-5">
          <Card style={{ padding: "0 15px" }}>
            <div style={{ marginTop: "10px" }}>
              <h3 className="static-title">About Us</h3>
            </div>
            <div style={{ padding: "10px 0" }}>
              <p>
                We are team of crazy guys who have a dream to change the world and
                we met several others who would like to make a dent with us. That's
                when we created ProjectHeena.
              </p>
              <p>Following is just a small excerpt of who we are and what we do.</p>
            </div>
            <div style={{ padding: "20px 0" }}>
              <h3 style={{ marginTop: "5px", marginBottom: "10px" }}>Our Team</h3>
              <Row>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">

                    <Row >
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/you.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Yes, Yes you!</strong>
                        </h3>
                        <p>
                          <i class="fas fa-suitcase"></i> Change maker
                        </p>
                        <p>
                          Uses ProjectHeena and advocates doing good. Does his bit
                          for changing the world.
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/himanshuchanda.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Himanshi Chanda</strong>
                        </h3>
                        <p>
                          <i class="fas fa-suitcase"></i> Founder CRO
                        </p>
                        <p>
                          Incharge product development & strategy. Chief
                          Responsibility Officer
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/nikhil.png"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Nikhil Kataria</strong>
                        </h3>
                        <p>
                          <i class="fas fa-suitcase"></i> Product Lead
                        </p>
                        <p>
                          Handles complete technology stack. Burns the midnight oil
                          to keep the servers running.
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>

              <Row>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/gaurav.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Gaurav Puranik</strong>
                        </h3>
                        <p>
                          {" "}
                          <i class="fas fa-suitcase"></i> Solutions Head
                        </p>
                        <p>
                          Handles alliances & partnerships. Develops solutions for
                          corporate teams
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/neeraj.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Neeraj Rathi</strong>
                        </h3>
                        <p>
                          <i class="fas fa-suitcase"></i> Tech Consultant
                        </p>
                        <p>
                          Geekiest amongst all. Adds scale to our magic and passion
                          to our technology
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/dinesh.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Dinesh Kumar</strong>
                        </h3>
                        <p>
                          <i class="fas fa-suitcase"></i> Web developer
                        </p>
                        <p>
                          Supports the team in day to day tech. Good at hacking
                          quick solutions.
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>

              <Row>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/jigar.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Jigar Jain</strong>
                        </h3>
                        <p>
                          <i class="fas fa-suitcase"></i> UI/UX Consultant
                        </p>
                        <p>
                          Square peg in the round hole. Assists in improving user
                          experience.
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/umang.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Umang Shah</strong>
                        </h3>
                        <p>
                          {" "}
                          <i class="fas fa-suitcase"></i> Community Manager
                        </p>
                        <p>
                          Manages all the engagement for our stakeholder. Go to guy
                          for everything non-tech
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </div>

            <div style={{ padding: "20px 0" }}>
              <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
                Our Friends and Mentors
              </h3>
              <blockquote>
                If you want to go quickly, go alone. If you want to go far, go
                together.
              </blockquote>
              <p>
                We wouldn't have seen the light of the day without the support of
                the following friends.
              </p>

              <Row>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/anaggh.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Anaggh Desai</strong>
                        </h3>
                        <p>
                          {" "}
                          <i class="fas fa-suitcase"></i> Co-founder 1+99 Experience
                          Consulting
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/karon.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Karon Shaiva</strong>
                        </h3>
                        <p>
                          <i class="fas fa-suitcase"></i> Chief Impact Officer,
                          IDOBRO
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/aditya.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Aditya Mishra</strong>
                        </h3>
                        <p>
                          {" "}
                          <i class="fas fa-suitcase"></i> CEO, Switchme.in
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>

              <Row>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/annkur.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Annkur Agarwal</strong>
                        </h3>
                        <p>
                          <i class="fas fa-suitcase"></i> Co-founder, PriceBaba.com
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
                <Col xs={12} lg={4}>
                  <Card className="contact-box blob">
                    <Row>
                      <Col xs={4}>
                        <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                          <img
                            className="img-circle"
                            src="http://projectheena.in/assets/img/ph-team/tirthesh.jpg"
                            alt=""
                          ></img>
                        </div>
                      </Col>
                      <Col xs={8}>
                        <h3 style={{ fontSize: "16px" }}>
                          <strong>Tirthesh Ganatra</strong>
                        </h3>
                        <p>
                          {" "}
                          <i class="fas fa-suitcase"></i> Co-founder, PriceBaba.com
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </div>

          </Card>

        </div>
      </div>


    </>
  );
};
export default AboutUs;
