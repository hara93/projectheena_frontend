import React from "react";
import { Row, Col, Card } from "react-bootstrap";
import { Form, FormGroup } from "react-bootstrap";
import CustomButtonOutline from "../components/CustomButtonOutline";
import { Link } from "react-router-dom"
class InviteFriends extends React.Component {
  render() {
    return (
      <div className="container">
        <div>
          <h2 style={{ fontSize: "24px", fontWeight: "600" }}>
            Invite Friends
          </h2>
          <p style={{ padding: "0" }}>
            We believe Social media channels just rock! However we are cool
            which ever way you would like to connect. Just a small headsup in
            case if you feel there is an unwanted error bugging you or feature
            you want to request; use the on page Feedback buttons to intimate
            us. Else you can use the following channels.
          </p>

          <p style={{ padding: "0" }}>
            We reach out to the users you invite and for every invited user we
            will add karma points to your profile.
          </p>
        </div>

        <Row style={{ marginBottom: "20px" }}>
          <Col md={6}>
            <Card style={{ marginBottom: "25px" }}>
              <div
                className="box-ibox-title"
                style={{ borderBottom: "1px solid rgb(231 231 231)" }}
              >
                <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                  Invite Your Friends from Gmail
                </h5>
              </div>
              <div
                style={{
                  padding: "15px 20px 20px 20px",
                  border: "1px solid rgb(231 231 231)",
                }}
              >
                <p>
                  Connect to your Gmail account to import all the email address
                  of your friends you wish to send email asking them to invite
                  this Awesome site
                </p>
                <button className="invite-facebook" style={{ padding: "5px 10px" }}>
                  <Link to="" style={{ color: "inherit" }}>
                    <div >
                      {" "}
                      <img
                        src="http://projectheena.in/assets/img/social/gmail.gif"
                        alt=""
                      ></img>{" "}
                      Gmail
                    </div>
                  </Link>
                </button>
              </div>
            </Card>
          </Col>
          <Col>
            <Card style={{ marginBottom: "25px" }}>
              <div
                className="box-ibox-title"
                style={{ borderBottom: "1px solid rgb(231 231 231)" }}
              >
                <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                  Invite Your Friends from Facebook
                </h5>
              </div>
              <div
                style={{
                  padding: "15px 20px 20px 20px",
                  border: "1px solid rgb(231 231 231)",
                }}
              >
                <button className="invite-facebook">
                  {" "}
                  <i class="fab fa-facebook-f invite-facebook-log">
                    {"  "}
                  </i>Invite
                </button>
              </div>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <Card style={{ marginBottom: "25px" }}>
              <div
                className="box-ibox-title"
                style={{ borderBottom: "1px solid rgb(231 231 231)" }}
              >
                <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                  Invite Your Friends using Excel Upload
                </h5>
              </div>
              <div
                style={{
                  padding: "15px 20px 20px 20px",
                  border: "1px solid rgb(231 231 231)",
                }}
              >
                {/* <FileUpload /> */}
                <Form>
                  <FormGroup>
                    <Form.Row>
                      <Form.Label
                        column="lg"
                        lg={3}
                        className="form-invite-friends p-r-15"
                      >
                        Excel
                      </Form.Label>
                      <Col>
                        <Form.File required name="file" />
                        <span className="help-block">
                          You can download Sample Excelsheet from{" "}
                          <Link
                            to="http://projectheena.in/vms/downloadExcel"
                            className="basecolor"
                          >
                            here
                          </Link>
                        </span>
                        <CustomButtonOutline content="Upload Excel" />
                      </Col>
                    </Form.Row>
                  </FormGroup>
                </Form>
              </div>
            </Card>
          </Col>
          <Col md={6}>
            <Card>
              <div
                className="box-ibox-title"
                style={{ borderBottom: "1px solid rgb(231 231 231)" }}
              >
                <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                  Invite Friends
                </h5>
              </div>
              <div
                style={{
                  padding: "15px 20px 20px 20px",
                  border: "1px solid rgb(231 231 231)",
                }}
              >
                <Form>
                  <FormGroup>
                    <Form.Row>
                      <Form.Label
                        column="lg"
                        lg={3}
                        className="form-invite-friends p-r-15"
                      >
                        First Name
                      </Form.Label>
                      <Col>
                        <Form.Control type="text" className="green-focus" />
                      </Col>
                    </Form.Row>
                  </FormGroup>
                  <hr
                    className="form-horizontal-line"
                    style={{ margin: "20px 0" }}
                  />

                  <FormGroup>
                    <Form.Row>
                      <Form.Label
                        column="lg"
                        lg={3}
                        className="form-invite-friends p-r-15"
                      >
                        Last Name
                      </Form.Label>
                      <Col>
                        <Form.Control type="text" className="green-focus" />
                      </Col>
                    </Form.Row>
                  </FormGroup>
                  <hr
                    className="form-horizontal-line"
                    style={{ margin: "20px 0" }}
                  />

                  <FormGroup>
                    <Form.Row>
                      <Form.Label
                        column="lg"
                        lg={3}
                        className="form-invite-friends p-r-15"
                      >
                        Email ID
                      </Form.Label>
                      <Col>
                        <Form.Control type="email" className="green-focus" />
                      </Col>
                    </Form.Row>
                  </FormGroup>
                  <hr
                    className="form-horizontal-line"
                    style={{ margin: "20px 0" }}
                  />

                  <FormGroup>
                    <Form.Row>
                      <Form.Label
                        column="lg"
                        lg={3}
                        className="form-invite-friends p-r-15"
                      >
                        Contact Number
                      </Form.Label>
                      <Col>
                        <Form.Control type="number" className="green-focus" />
                      </Col>
                    </Form.Row>
                  </FormGroup>
                  <hr
                    className="form-horizontal-line"
                    style={{ margin: "20px 0" }}
                  />
                  <FormGroup>
                    <Form.Row>
                      <Form.Label column="lg" lg={3}></Form.Label>
                      <Col>
                        <div>
                          <CustomButtonOutline content={`Invite`} />
                        </div>
                      </Col>
                    </Form.Row>
                  </FormGroup>
                </Form>
              </div>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default InviteFriends;
