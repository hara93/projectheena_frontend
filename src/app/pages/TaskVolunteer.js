import React, { useState, useEffect } from "react";
import { Row } from "react-bootstrap";
import axios from "../config/axios";

const TaskVolunteer = (props) => {
  const task_id = localStorage.getItem("task_data_id");

  const [taskVolunteer, setTaskVolunteer] = useState([]);

  const taskVolunteersList = () => {
    axios
      .get(`/taskTransaction/taskVolunteersList/${task_id}`)
      .then((res) => {
        console.log("taskVolunteersList", res);
        setTaskVolunteer(res.data);
      })
      .catch((err) => console.log("err", err));
  };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => taskVolunteersList(), []);
  return (
    <>
      <strong>
        <p className="pl-1 pt-4" style={{ color: "#676A6C", fontSize: 20 }}>
          Volunteer Details
        </p>
      </strong>
      <Row className="px-4">
        <table className="custom-table table-bordered" bordered size="sm">
          <thead>
            <tr>
              <th>Sr.No</th>
              <th>Volunteer Name</th>
              <th>Email</th>
              <th>Contact</th>
            </tr>
          </thead>
          <tbody>
            {taskVolunteer.map((ele, idx) => (
              <tr>
                <td>{idx + 1}</td>
                <td>{`${ele.first_name} ${ele.last_name}`}</td>
                <td>{ele.email_id}</td>
                <td>{ele.user_contact_number}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </Row>
      <p className="pl-1" style={{ color: "grey" }}>
        <span style={{ color: "#4caf4f" }}>Click here</span> to download volunteer details
      </p>
    </>
  );
};

export default TaskVolunteer;
