import React from "react";
import "../login.css";
// import '../../index.scss';
import { Row, Col } from "react-bootstrap";
import { useFormik } from "formik";
import { string, object } from "yup";
import { useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";

import axios from "../config/axios";
import { Images } from "../config/Images";
import Input from "../components/form/Input";
import { setNgoData } from "../../redux/reducers/auth/actionCreator";
import { ToastContainer, toast } from 'react-toastify';

export default function UserLogin() {
  const history = useHistory();
  const dispatch = useDispatch();

  const defaultValues = {
    username_email: "",
    password: "",
  };

  const validationSchema = object().shape({
    username_email: string().required("Username or email is required"),
    password: string().required("Password is required"),
  });

  const onSubmit = (values) => {
    console.log("values", values)
    try {
      axios.post("ngo/login", { ...values })
        .then(({ data }) => {
          console.info(data);
          dispatch(setNgoData(data));
          localStorage.setItem('token', data.token)
          history.push("/ngo/" + data.ngo_username);
        })
        .catch(e => {
          // console.log("ERROR", e)
          toast.error("Login unsuccessful")
        })
    } catch (e) {
      alert(e.stack)
    }
  };

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    values,
    errors,
    touched,

  } = useFormik({
    initialValues: defaultValues,
    validationSchema,
    onSubmit,
  });

  return (
    <div className="login-container" style={{ maxWidth: '830px', margin: "0 auto" }}>
      <ToastContainer />
      <div className="d-flex flex-row">
        <div style={{ maxWidth: '80px' }}>
          <img
            src={Images.NGOSignup}

            alt=""
            className="login-avatar"
          />
        </div>
        <div >
          <div style={{ display: "flex", flexDirection: "column" }}>
            <h2 className="fw-bold">Login as an NGO/Nonprofit</h2>
            <p>
              Do note new signups take 24-48 hours for approval. Please
              provide as detailed information as possible on all forms.
              For any issues connect with us on team@projectheena.com
            </p>
          </div>
        </div>
      </div>
      <Row>
        <Col xs={12} md={12} lg={6} className="col-divider-line">
          <Link to="/ngo-registration" className="mail mt-3">
            {" "}
            <i
              className="fas fa-envelope social-icons"
              style={{ color: "white", marginRight: "2%" }}
            ></i>
            Sign up with Mail
          </Link>
        </Col>
        <Col xs={12} md={12} lg={6}>
          <form id="login-form" onSubmit={handleSubmit}>
            <div>
              <Input
                type="text"
                name="username_email"
                onChange={handleChange}
                onBlur={handleBlur}
                error={errors.username_email}
                value={values.username_email}
                touched={touched.username_email}
                aria-required="true"
                placeholder="Username"
              ></Input>
            </div>
            <div>
              <Input
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                error={errors.password}
                value={values.password}
                touched={touched.password}
                aria-required="true"
                placeholder="Password"
              ></Input>
            </div>
            <div>
              <button type="submit" className="login">
                Login
              </button>
            </div>
            <div className="d-flex justify-content-between mt-3">
              <Link
                to="#"
                style={{ color: "#4CAF50" }}
              >
                Forgot Password?
              </Link>
              <Link
                to="#"
                style={{ color: "#4CAF50" }}
              >
                Resend Confirmation email
              </Link>
            </div>
          </form>
        </Col>
      </Row>
    </div>
  );
}
