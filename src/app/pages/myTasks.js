import React from "react";
import TabNav from "./TabNav";
import TabContent from "./TabContent";
// import "../../index.scss";

import Current from "./Current";
import Deleted from "./Deleted";
import NGOlog from "./NGOlog";
import CreateTaskOptions from "./create-task.js";

const tabs = ["Current", "Deleted", "NGO Log", "Create Task", "Create Karmalog"]
class MyTasks extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: "Current",
    };
  }

  setSelected = (tab) => {
    this.setState({ selected: tab.name });
  };

  render() {
    return (
      <div className="container">
        <TabNav
          tabs={tabs}
          selected={this.state.selected}
          setSelected={(tab) => {
            this.setState({ selected: tab })
          }}
          style={{ backgroundColor: "white" }}
        >
          <TabContent isSelected={this.state.selected === "Current"}>
            <Current />
          </TabContent>
          <TabContent isSelected={this.state.selected === "Deleted"}>
            <Deleted />
          </TabContent>
          <TabContent isSelected={this.state.selected === "NGO Log"}>
            <NGOlog />
          </TabContent>

          <TabContent isSelected={this.state.selected === "Create Task"}>
            <CreateTaskOptions />
          </TabContent>
        </TabNav>
      </div>
    );
  }
}

export default MyTasks;
