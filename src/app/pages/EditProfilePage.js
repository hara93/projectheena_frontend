/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import "../../index.scss";

import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import {
  Col,
  Dropdown,
  DropdownButton,
  FormControl,
  Card,
} from "react-bootstrap";
import CustomButton from "../components/CustomButton";
import "bootstrap/dist/css/bootstrap.min.css";
import TextEditor from "../components/textEditor";
import DatePicker from "react-datepicker";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
import CheckboxTable from "../components/checkboxTable";
import { useSelector } from "react-redux";
import RadiobuttonTable from "../components/radiobuttonTable";
//froala editor
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/froala_editor.pkgd.min.css";
import FroalaEditorComponent from "react-froala-wysiwyg";
import "froala-editor/js/plugins.pkgd.min.js";
import { Images } from "../config/Images";
import moment from "moment";
import axios from "../config/axios";
import { useHistory } from "react-router";
import { ToastContainer, toast } from 'react-toastify';
import { setNgoData } from "../../redux/reducers/auth/actionCreator";
import { useDispatch } from "react-redux";
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const EditProfilePage = () => {
  const history = useHistory()
  const dispatch = useDispatch();

  const ngo = useSelector(state => state.phAuth.ngo)
  const [startDate, setStartDate] = useState(ngo.ngo_establishment_date);
  const [endDate, setEndDate] = useState(new Date());
  const [count, setCount] = useState(0);
  const [countAwards, setCountAward] = useState(0);
  const [ngo_location, setLocation] = useState(ngo.ngo_location);
  const [ngo_served_causes, setCauseSupported] = useState(ngo.ngo_served_causes);
  const [desc, setDesc] = useState(ngo.ngo_details);
  const [overview, setOverview] = useState(ngo.ngo_overview);

  const [logo, setLogo] = useState(ngo.logo)
  const [cover, setCover] = useState(ngo.cover)

  const handleLogoSelect = (fileData) => {
    var data = new FormData()
    data.append("logo", fileData.currentTarget.files[0])
    data.append("ngo_id", ngo._id)

    axios.post("/ngo/update", data)
      .then(({ status, data, message }) => {
        console.log("status, data, message", status, data, message)
        if (status == 1) {
          setLogo(data.logo);
          dispatch(setNgoData(data));
        }
      })
      .catch((err) => {
      })
  };
  const handleCoverSelect = (fileData) => {
    var data = new FormData()
    data.append("cover", fileData.currentTarget.files[0])
    data.append("ngo_id", ngo._id)
    axios.post("/ngo/update", data)
      .then(({ status, data, message }) => {
        console.log("status, data, message", status, data, message)
        if (status == 1) {
          setCover(data.cover);
          dispatch(setNgoData(data));
        }
      })
      .catch((err) => {
      })
  };
  const handleModelChange = (e, editor) => {
    const char = e;
    var plainText = char.replace(/<[^>]+>/g, "");
    setDesc(char);
  };
  const handleModelChangeVolunteer = (e, editor) => {
    const char = e;
    var plainText = char.replace(/<[^>]+>/g, "");
    setOverview(char);
  };
  console.log(ngo_served_causes);
  const config = {
    charCounterMax: 2000,
  };
  const useStyles = makeStyles((theme) => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
  }));
  const classes = useStyles();
  useEffect(() => {
    console.log("ESTABLISHMENTdATE", moment(ngo.ngo_establishment_date).format("YYYY-MM-DD"))
  }, [])
  const [NgoData, setNgoDetails] = useState({
    ngo_username: ngo.ngo_username,
    ngo_name: ngo.ngo_name,
    ngo_location: ngo.ngo_location,
    country_id: ngo.country_id,
    volunter_count: ngo.volunter_count,
    ngo_overview: ngo.ngo_overview,
    ngo_details: ngo.ngo_details,
    ngo_tag: ngo.ngo_tag,
    ngo_served_causes: ngo.ngo_served_causes,
    ngo_establishment_date: moment(ngo.ngo_establishment_date).format("YYYY-MM-DD"),
    country_code: ngo.country_code,
    ngo_contact_number: ngo.ngo_contact_number,
    preferred_day: ngo.preferred_day,
    preferred_slot: ngo.preferred_slot,
    ngo_url: ngo.ngo_url,
    ngo_email_id: ngo.ngo_email_id,
    ngo_admin: ngo.ngo_admin,
    ngo_facebook_url: ngo.ngo_facebook_url,
    ngo_twitter_handle: ngo.ngo_twitter_handle,
    // logo: ngo.logo,
    // cover: ngo.cover,
    // befriend_heena: ngo.befriend_heena,
    // reason_to_help: ngo.reason_to_help,
    // ngo_comments: ngo.ngo_comments,
    // cp_first_name: ngo.cp_first_name,
    // cp_last_name: ngo.cp_last_name,
    // cp_mobile: ngo.cp_mobile,
    // cp_email: ngo.cp_email,
    // vendor_code: ngo.vendor_code,
    company_code: ngo.company_code,
    // status: ngo.status,
    // registration_date: ngo.registration_date,
    ngo_nps_id: ngo.ngo_nps_id,
    // ngo_type: ngo.ngo_type,
    ngo_address: ngo.ngo_address,
    staff_count: ngo.staff_count,
    awards_won: ngo.awards_won,
    // campaign_id: ngo.campaign_id,
    // password: ngo.password,
    // sms_pref_mapped: ngo.sms_pref_mapped,
  });
  var maxCount = 256;
  var maxCountAwards = 1000;

  console.log("ngo", ngo)

  const handleDeleteLogo = () => {
    setLogo("")
    axios.post("/ngo/update", { logo: "", ngo_id: ngo._id })
      .then(({ status, data, message }) => {
        console.log("status, data, message", status, data, message)
        if (status == 1) {
          dispatch(setNgoData(data));
        }
      })
      .catch((err) => {
      })
  }
  const handleDeleteCover = () => {
    setCover("")
    axios.post("/ngo/update", { cover: "", ngo_id: ngo._id })
      .then(({ status, data, message }) => {
        console.log("status, data, message", status, data, message)
        if (status == 1) {
          dispatch(setNgoData(data));
        }
      })
      .catch((err) => {
      })
  }
  const handleChangeInputFields = e => {
    const { name, value } = e.target;
    console.log("name, value", name, value)
    setNgoDetails(prevState => ({
      ...prevState,
      [name]: value
    }));
  };
  const handleSubmit = (values) => {
    var data_to_update = {}
    var data = new FormData()
    const keys = Object.keys(NgoData);
    keys.forEach((key, index) => {
      if (key != 'ngo_location' && key != 'country_code' && key != 'ngo_details' && key != 'ngo_overview' && key != 'ngo_served_causes' && key != 'logo' && key != 'cover' && key != 'ngo_establishment_date') {
        // console.log(`${key}`, `${NgoData[key]}`)
        // data.append(`${key}`, `${NgoData[key]}`);
        data_to_update[key] = NgoData[key]
      }
    });
    data_to_update.ngo_id = ngo._id
    data_to_update.ngo_location = ngo_location
    data_to_update.country_code = "+91"
    data_to_update.ngo_details = desc
    data_to_update.ngo_overview = overview
    data_to_update.ngo_served_causes = ngo_served_causes

    data_to_update.ngo_establishment_date = NgoData.ngo_establishment_date

    // data.append("ngo_id", ngo._id)
    // data.append("ngo_location", ngo_location)
    // data.append("country_code", "+91")
    // data.append("ngo_details", desc)
    // data.append("ngo_overview", overview)
    // data.append("ngo_served_causes", ngo_served_causes)
    // data.append("logo", logo)
    // data.append("cover", cover)
    // data.append("ngo_establishment_date", !startDate || startDate == '' ? '' : new Date(startDate).valueOf())
    // for (var value of data.values()) {
    //   console.log(value);
    // }
    console.log("ERROR HERE", data_to_update)
    axios.post("/ngo/update", data_to_update)
      .then(({ status, data, message }) => {
        console.log("status, data, message", status, data, message)
        if (status == 1) {
          dispatch(setNgoData(data));
          toast.success(message, { onClose: () => history.push("/ngo/" + data.ngo_username) })
        } else {
          toast.error(message, { onClose: () => history.push("/ngo/" + data.ngo_username) })
        }
      })
      .catch((err) => {
        toast.error("Error!!", { onClose: () => history.push("/ngo/" + data.ngo_username) })
      })
    // .then(({ status, data, message }) => {
    //   if (data) {
    //     console.log("status, data, message", status, data, message)
    //     toast.success(message, { onClose: () => history.push("/viewprofile") })
    //   } else {
    //     toast.error(message, { onClose: () => history.push("/viewprofile") })
    //   }
    // })
    // .catch((err) => {
    //   console.log("err", err)
    //   toast.error("Update unsuccessful")
    // });
  };
  return (
    <>
      <ToastContainer />
      <div style={{ marginBottom: "30px" }}>
        <h2 className="font-bold">Edit Profile</h2>
        <p>
          Currently, donation can only be created by approved NGOs. Please get
          in touch with us before filling the below form for easy approval.
        </p>
      </div>
      <Card style={{ padding: "30px " }}>
        <Form>
          <Form.Group>
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                NGO name
              </Form.Label>
              <Col className="form-input-align-center">
                <Form.Control value={NgoData.ngo_name} name="ngo_name" type="text" className="green-focus" onChange={handleChangeInputFields} />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />

            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Email ID
              </Form.Label>
              <Col>
                <Form.Control value={NgoData.ngo_email_id} name="ngo_email_id" type="Email" className="green-focus" onChange={handleChangeInputFields} />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Your City
              </Form.Label>
              <Col>
                <PlacesAutoComplete
                  location={(ngo_location) => {
                    setLocation(ngo_location);
                  }}
                  // lat={(lat) => {
                  //   setLat(lat);
                  // }}
                  // lng={(lng) => setLng(lng)}
                  address={ngo_location}
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Address
              </Form.Label>
              <Col>
                <Form.Control
                  type="text"
                  as="textarea"
                  rows="5"
                  name="ngo_address"
                  value={NgoData.ngo_address}
                  maxLength="200"
                  minLength="5"
                  onChange={handleChangeInputFields}
                  // onChange={(e) => setCount(e.target.value.length)}
                  style={{ borderRadius: "0" }}
                  className="green-focus"
                />
                <span className="help-block">
                  {" "}
                  Min: 5 chars | Max: 200 chars | Remaining Chars:{" "}
                  {maxCount - count}
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Contact Number
              </Form.Label>
              <Col>
                <InputGroup className="mb-3">
                  <DropdownButton
                    as={InputGroup.Prepend}
                    variant="outline-secondary"
                    title="+91"
                    id="input-group-dropdown-1"
                  >
                    <Dropdown.Item href="#">+91</Dropdown.Item>
                  </DropdownButton>
                  <FormControl type="text" name="ngo_contact_number" onChange={handleChangeInputFields} value={NgoData.ngo_contact_number} className="green-focus" />
                </InputGroup>
                <span className="help-block">We do not span</span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Your NGO Reg ID
              </Form.Label>
              <Col>
                <Form.Control value={NgoData.ngo_nps_id} name="ngo_nps_id" type="text" onChange={handleChangeInputFields} className="green-focus" />
              </Col>
            </Form.Row>

            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Cause Supported
              </Form.Label>
              <Col>
                {/* <RadiobuttonTable
                  name="cause_supported"
                  handleChange={(radio) => setRadioValue(radio)}
                  value={NgoData.ngo_served_causes}
                  alreadyChecked={radioValue.split()}
                /> */}
                <CheckboxTable
                  cause={(causes) => {
                    setCauseSupported(causes);
                    console.log(causes, "table");
                  }}
                  alreadyChecked={ngo_served_causes}
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Preferred timing for voluntary help
              </Form.Label>
              <Col className="form-input-align-center">
                <Form.Control
                  as="select"
                  custom
                  onChange={handleChangeInputFields}
                  name="preferred_day"
                  value={NgoData.preferred_day}
                //onClick={(e) => setFreq(e.target.value)}
                >
                  <option>Select Day</option>

                  <option value='0'>Sunday</option>
                  <option value='1'>Monday</option>
                  <option value='2'>Tuesday</option>
                  <option value='3'>Wednesday</option>
                  <option value='4'>Thursday</option>
                  <option value='5'>Friday</option>
                  <option value='6'>Saturday</option>
                  <option value='7'>Weekdays</option>
                  <option value='8'>Weekends</option>
                  <option value='9'>Unavailable</option>
                </Form.Control>
              </Col>
              <Col className="form-input-align-center">
                <Form.Control
                  as="select"
                  custom
                  name="preferred_slot"
                  //onClick={(e) => setFreq(e.target.value)}
                  onChange={handleChangeInputFields}
                  value={NgoData.preferred_slot}
                >
                  <option>Select Time</option>
                  <option value="0">Morning</option>
                  <option value="1">Afternoon</option>
                  <option value="2">Evening </option>
                  <option value="3">Night</option>
                  <option value="4">Flexible</option>
                  <option value="9">Unavailable</option>
                </Form.Control>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Details
              </Form.Label>
              <Col>
                <div id="task_description" />
                <div style={{ maxWidth: "100%" }}>
                  <FroalaEditorComponent
                    tag="textarea"
                    config={config}
                    onModelChange={handleModelChange}
                    model={desc}
                  />
                </div>
                {/* <TextEditor /> */}
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Logo (Max 1 MB)
              </Form.Label>
              <Col className="form-input-align-center ">
                <input name="logo" type="file" onChange={handleLogoSelect} />
              </Col>
            </Form.Row>
            <div style={{ marginLeft: "17%" }}>
              <img style={{ width: '10%' }} src={!logo || logo == '' ? Images.Social_work : `http://localhost:5000/uploads/images/ngo/${logo}`}></img>
            </div>
            {logo == '' ? null :
              <div style={{ color: "#4caf50", marginLeft: "20%" }}>
                <a onClick={handleDeleteLogo}>Delete</a>
              </div>
            }
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Cover
              </Form.Label>
              <Col>
                <input name="logo" type="file" onChange={handleCoverSelect} />
                <span className="help-block">
                  {" "}
                  Please upload an image of your successful activity as this
                  will be showcased to all the stakeholders
                </span>
              </Col>
            </Form.Row>
            <div style={{ marginLeft: "17%" }}>
              <img style={{ width: '10%' }} src={!cover || cover == '' ? Images.Social_work : `http://localhost:5000/uploads/images/ngo/${cover}`}></img>
            </div>
            {cover == '' ? null :
              <div style={{ color: "#4caf50", marginLeft: "20%" }}>
                <a onClick={handleDeleteCover}>Delete</a>
              </div>
            }
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Establishment date
              </Form.Label>
              <Col>
                <TextField
                  onChange={handleChangeInputFields}
                  id="date"
                  name="ngo_establishment_date"
                  label="Date of Establishment"
                  type="date"
                  value={NgoData.ngo_establishment_date}
                  defaultValue="2017-05-24"
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                {/* <InputGroup>
                  <DatePicker
                    name="ngo_establishment_date"
                    selected={startDate}
                    selectsStart
                    startDate={startDate}
                    endDate={endDate}
                    onChange={(date) => setStartDate(date)}
                    className="green-focus"
                  />
                  <InputGroup.Append>
                    <i class="fas fa-calendar-alt"></i>
                  </InputGroup.Append>
                </InputGroup> */}
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                No. of Employees
              </Form.Label>
              <Col>
                <Form.Control name="staff_count" min={1} onChange={handleChangeInputFields} value={NgoData.staff_count} type="number" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Existing Volunteer Count
              </Form.Label>
              <Col>
                <Form.Control value={NgoData.volunter_count} min={1} onChange={handleChangeInputFields} name="volunter_count" type="number" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Tag line
              </Form.Label>
              <Col>
                <Form.Control name="ngo_tag" onChange={handleChangeInputFields} value={NgoData.ngo_tag} type="text" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Why volunteer for us
              </Form.Label>
              <Col>
                {/* <TextEditor />
                <Form.Text className="text-muted" muted>
                  Max: 1500 chars
                </Form.Text> */}
                <FroalaEditorComponent
                  tag="textarea"
                  config={config}
                  onModelChange={handleModelChangeVolunteer}
                  model={overview}
                />
                {/* </div> */}
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Your Website
              </Form.Label>
              <Col>
                <Form.Control name="ngo_url" value={NgoData.ngo_url} onChange={handleChangeInputFields} type="text" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Your Facebook URL
              </Form.Label>
              <Col>
                <Form.Control value={NgoData.ngo_facebook_url} onChange={handleChangeInputFields} name="ngo_facebook_url" type="text" className="green-focus" />

                <span className="help-block">
                  {" "}
                  Please ensure that it is a fan page & not profile page
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Twitter handle
              </Form.Label>
              <Col>
                <InputGroup>
                  <InputGroup.Prepend>
                    <InputGroup.Text
                      style={{ marginRight: "0", background: "transparent" }}

                      className="green-focus"
                    >
                      @
                    </InputGroup.Text>
                  </InputGroup.Prepend>

                  <FormControl type="text" name="ngo_twitter_handle" onChange={handleChangeInputFields} value={NgoData.ngo_twitter_handle} className="green-focus" />
                </InputGroup>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom  p-r-15"
              >
                Awards Won (Comma Separated)
              </Form.Label>
              <Col>
                <Form.Control
                  type="text"
                  as="textarea"
                  maxLength="1000"
                  name="awards_won"
                  onChange={handleChangeInputFields}
                  // onChange={(e) => setCountAward(e.target.value.length)}
                  className="green-focus"
                  style={{ borderRadius: "0" }}
                  value={NgoData.awards_won}
                />
                {/* <Form.Text className="text-muted" muted>
                  Max: 1000 chars | Remaining Chars:{" "}
                  {maxCountAwards - countAwards}
                </Form.Text> */}
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2}></Form.Label>
              <Col>
                <CustomButton content="Update" type="button" onClick={handleSubmit} />
              </Col>
            </Form.Row>
            <br />
          </Form.Group>
        </Form>
      </Card>
    </>
  );
};

export default EditProfilePage;
