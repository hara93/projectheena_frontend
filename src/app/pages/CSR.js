import React, { useEffect, useState } from "react";
import TabNav from "./TabNav";
import TabContent from "./TabContent";
import "../../index.scss";
import { Images } from "../config/Images";
import CustomButton from "../components/CustomButton";
import {
  FaFacebook,
  FaTwitterSquare,
  FaGooglePlus,
  FaYoutube,
  FaThumbsUp,
  FaTwitter,
} from "react-icons/fa";
import CSRDashboard from "./CSRDashboard";
import Milestone from "./Milestones";
import BudgetTracker from "./BudgetTracker";
import ActivityTracker from "./ActivityTracker";
import SurveyPage from "./SurveyPage";
import ProjectDetails from "./ProjectDetails";
import Report from "./ReportPage";
import Comments from "./comments";
import Collaborators from "./Collaborators";
import ProjectData from "./ProjectData";
import Files from "./Files";
import Beneficiary from "./Beneficiaries";
import Reviews from "./Reviews";
import axios from "../config/axios";
import { connect, useDispatch } from 'react-redux';
import { setProjectData } from '../../redux/reducers/project/actionCreator';

const tabs = [
  {
    name: "Details",
    icon: <i class="far fa-newspaper" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Dashboard",
    icon: <i class="fas fa-tv" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Milestone",
    icon: <i class="fas fa-road" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Budget Tracker",
    icon: <i class="fas fa-rupee-sign" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Activity Tracker",
    icon: <i class="fas fa-list-ol" style={{ marginRight: "1px", marginLeft: "0px" }}></i>,
  },
  {
    name: "Surveys",
    icon: <i class="fas fa-chart-area" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Reports",
    icon: <i class="fas fa-chart-bar" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Comments",
    icon: <i class="fas fa-comments" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Files & Photos",
    icon: <i class="fas fa-images" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Collaborators",
    icon: <i class="fas fa-user-secret" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Project Data",
    icon: <i class="fas fa-exclamation" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Beneficiaries",
    icon: <i class="fas fa-users" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Review",
    icon: <i class="fas fa-quote-left" style={{ marginRight: "5px" }}></i>,
  },
  {
    name: "Social Map",
    icon: <i class="far fa-map" style={{ marginRight: "5px" }}></i>,
  },
];
function CSR(props) {
  const dispatch = useDispatch();
  const [projectdata, setprojectdata] = useState({})
  const [selected, setselected] = useState(`${tabs[0].name}`)
  function getProjectData() {
    var getSlug = props.location.pathname.split("/")[2]
    var query = { project_slug: getSlug, session_data: { logged_in_id: props.user._id, logged_in_type: 1 } }
    axios.post("project/project-details", query)
      .then(({ data }) => {
        setprojectdata(data)
        dispatch(setProjectData(data))
      });
  }
  useEffect(() => {
    getProjectData()
  }, []);
  return (
    <div className="container">
      <div style={{ margin: "50px 0px" }}>
        <h1 style={{ textAlign: "center" }}>{projectdata.project_name}</h1>
        <div style={{ textAlign: "center", fontSize: "1.8rem" }}>
          By <a style={{ color: "#4CAF50", fontSize: "1.5rem" }}>{props.user.first_name + ' ' + props.user.last_name}</a>,
          Location - Khandwa, Madhya Pradesh, India
        </div>
      </div>
      <TabNav
        tabs={tabs}
        selected={selected}
        setSelected={(tab) => {
          setselected(tab.name)
        }}
        style={{
          backgroundColor: "white",
        }}
      >
        <TabContent isSelected={selected === `${tabs[0].name}`}>
          <ProjectDetails />
        </TabContent>
        <TabContent isSelected={selected === "Dashboard"}>
          <CSRDashboard />
        </TabContent>
        <TabContent isSelected={selected === `${tabs[2].name}`}>
          <Milestone />
        </TabContent>
        <TabContent isSelected={selected === "Budget Tracker"}>
          <BudgetTracker />
        </TabContent>
        <TabContent isSelected={selected === "Activity Tracker"}>
          <ActivityTracker />
        </TabContent>
        <TabContent isSelected={selected === "Surveys"}>
          <SurveyPage />
        </TabContent>
        <TabContent isSelected={selected === "Reports"}>
          <Report />
        </TabContent>
        <TabContent isSelected={selected === "Comments"}>
          <Comments />
        </TabContent>
        <TabContent isSelected={selected === "Files & Photos"}>
          <Files />
        </TabContent>
        <TabContent isSelected={selected === "Collaborators"}>
          <Collaborators />
        </TabContent>
        <TabContent isSelected={selected === "Project Data"}>
          <ProjectData />
        </TabContent>
        <TabContent isSelected={selected === "Beneficiaries"}>
          <Beneficiary />
        </TabContent>
        <TabContent isSelected={selected === "Review"}>
          <Reviews />
        </TabContent>
      </TabNav>
    </div>
  );
}
const mapStateToProps = function (state) {
  return { user: state.phAuth.user }
};
export default connect(mapStateToProps)(CSR);