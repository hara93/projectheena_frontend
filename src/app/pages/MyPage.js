import React, { useState, useEffect } from 'react'
import { Row, Col, Card, Table } from "react-bootstrap"
import { Link } from "react-router-dom"
import { FaThumbsUp, FaTwitter } from "react-icons/fa";
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
const MyPage = () => {
  const TasksworkingOn = [
    {
      image: "http://projectheena.in/uploads/projects/20143920894594/thb/default.jpg",
      name: "Clean Beaches Mumbai",
      status: "Open",
      location: "Mumbai, Maharashtra, India",
      hours: 40
    },
    {
      image: "http://projectheena.in/uploads/projects/20143920894594/thb/default.jpg",
      name: "Clean Beaches Mumbai",
      status: "Open",
      location: "Mumbai, Maharashtra, India",
      hours: 40
    },
    {
      image: "http://projectheena.in/uploads/projects/20143920894594/thb/default.jpg",
      name: "Clean Beaches Mumbai",
      status: "Open",
      location: "Mumbai, Maharashtra, India",
      hours: 40
    }
  ]
  const TasksWantToHelp = [
    {
      image: "http://projectheena.in/uploads/projects/20143920894594/thb/default.jpg",
      name: "Clean Beaches Mumbai",
      status: "Open",
      location: "Mumbai, Maharashtra, India",
      hours: 40
    },
    {
      image: "http://projectheena.in/uploads/projects/20143920894594/thb/default.jpg",
      name: "Clean Beaches Mumbai",
      status: "Open",
      location: "Mumbai, Maharashtra, India",
      hours: 40
    },
    {
      image: "http://projectheena.in/uploads/projects/20143920894594/thb/default.jpg",
      name: "Clean Beaches Mumbai",
      status: "Open",
      location: "Mumbai, Maharashtra, India",
      hours: 40
    }
  ]
  const statusCardData = [
    {
      name: "Volunteer",
      ngos: 1,
      tasks: 2,
      skills: 3,
      hours: 4
    },
    {
      name: "Donate",
      ngos: 3,
      tasks: 5,
      skills: 6,
      hours: 2
    }, {
      name: "Advocate",
      ngos: 6,
      tasks: 22,
      skills: 13,
      hours: 54
    }, {
      name: "Collaborate",
      ngos: 11,
      tasks: 22,
      skills: 63,
      hours: 48
    }
  ]
  const [data, setData] = useState({
    ngos: 0,
    tasks: 0,
    skills: 0,
    hours: 0
  })
  const [selectedCard, setSelectedCard] = useState("Volunteer")
  const handleStatusCard = (name) => {
    setSelectedCard(name)
  }
  const handleDragStart = (e) => e.preventDefault();
  const items = [
    <Card className='p-2 mx-2' style={{ position: 'relative', height: '280px' }}>
      <img src="http://projectheena.in/uploads/projects/26152180609177/album/images/1588052908.jpg" style={{ width: "100%" }} onDragStart={handleDragStart} role="presentation" alt="" />
      <div className='p-2' style={{ minHeight: '150px' }}>
        <span><i class="fas fa-user" ></i> By <Link to="" className='basecolor'>Daksh</Link></span><br />
        <span style={{ overflow: 'hidden' }}> <i class="fas fa-tasks my-2" ></i>  <span className='basecolor'>Tree Plantation drive in mumbai</span> </span>
        <span style={{ position: 'absolute', right: '8px', bottom: '8px' }}>2 years ago</span>
      </div>
    </Card>,
    <Card className='p-2 mx-2' style={{ position: 'relative', height: '280px' }}>
      <img src="http://projectheena.in/uploads/projects/26152180609177/album/images/1588052908.jpg" style={{ width: "100%" }} onDragStart={handleDragStart} role="presentation" alt="" />
      <div className='p-2' style={{ minHeight: '150px' }}>
        <span><i class="fas fa-user" ></i> By <Link to="" className='basecolor'>Daksh</Link></span><br />
        <span style={{ overflow: 'hidden' }}> <i class="fas fa-tasks my-2" ></i> <span className='basecolor'>Tree Plantation drive in mumbai</span> </span>
        <span style={{ position: 'absolute', right: '8px', bottom: '8px' }}>2 years ago</span>
      </div>
    </Card>,
    <Card className='p-2 mx-2' style={{ position: 'relative', height: '280px' }}>
      <img src="http://projectheena.in/uploads/projects/26152180609177/album/images/1588052908.jpg" style={{ width: "100%" }} onDragStart={handleDragStart} role="presentation" alt="" />
      <div className='p-2' style={{ minHeight: '150px' }}>
        <span><i class="fas fa-user" ></i> By <Link to="" className='basecolor'>Daksh</Link></span><br />
        <span style={{ overflow: 'hidden' }}> <i class="fas fa-tasks my-2" ></i>  <span className='basecolor'>Tree Plantation drive in mumbai</span> </span>
        <span style={{ position: 'absolute', right: '8px', bottom: '8px' }}>2 years ago</span>
      </div>
    </Card>,
  ];
  const b = {
    0: {
      items: 3,
    },
    1024: {
      items: 3
    },
    1140: {
      items: 3
    }

  }
  useEffect(() => {
    setData(statusCardData.find((x) => x.name === selectedCard))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedCard])
  return (
    <div className='container'>

      <h2 style={{ fontSize: "24px", fontWeight: "600" }}>
        Dashboard
      </h2>
      <Row>
        <Col xs={12} lg={9} xl={9}>
          <Card className='dashboard-section '>
            <Row >
              <Col md={12} lg={4}>
                <Row className='m-0'>
                  <Col xs={3} lg={6} className={`d-flex flex-column dashboard-status-cards ${selectedCard === "Volunteer" && 'selected-card'}`} onClick={() => handleStatusCard('Volunteer')}>
                    <img src="http://projectheena.in/assets/img/dashboard/volunteer.png" alt="" width={50}></img>
                    <span>Volunteer</span>
                  </Col>
                  <Col xs={3} lg={6} className={`d-flex flex-column dashboard-status-cards ${selectedCard === "Donate" && 'selected-card'}`} onClick={() => handleStatusCard('Donate')}>
                    <img src="http://projectheena.in/assets/img/dashboard/donate.png" alt="" width={50}></img>
                    <span>Donate</span>
                  </Col>
                  <Col xs={3} lg={6} className={`d-flex flex-column dashboard-status-cards ${selectedCard === "Advocate" && 'selected-card'}`} onClick={() => handleStatusCard('Advocate')}>
                    <img src="http://projectheena.in/assets/img/dashboard/advocate.png" alt="" width={50}></img>
                    <span>Advocate</span>
                  </Col>
                  <Col xs={3} lg={6} className={`d-flex flex-column dashboard-status-cards ${selectedCard === "Collaborate" && 'selected-card'}`} onClick={() => handleStatusCard('Collaborate')}>
                    <img src="http://projectheena.in/assets/img/dashboard/collaborate.png" alt="" width={50}></img>
                    <span>Collaborate</span>
                  </Col>
                </Row>
              </Col>
              <Col lg={8} className='d-flex align-items-center justify-content-center' style={{ minHeight: '150px' }}>
                <Row className='w-100' >
                  <Col xs={6} md={3} lg={3} className='text-center '>
                    <span className='statuscard-subheading'>NGOS</span>
                    <h3 className='my-2 '>{data.ngos}</h3>
                  </Col>
                  <Col xs={6} md={3} lg={3} className='text-center '>
                    <span className='statuscard-subheading'>TASKS</span>
                    <h3 className='my-2 '>{data.tasks}</h3>
                  </Col>
                  <Col xs={6} md={3} lg={3} className='text-center '>
                    <span className='statuscard-subheading'>SKILLS</span>
                    <h3 className='my-2 '>{data.skills}</h3>
                  </Col>
                  <Col xs={6} md={3} lg={3} className='text-center '>
                    <span className='statuscard-subheading'>HOURS</span>
                    <h3 className='my-2 '>{data.hours}</h3>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Card>
          <div className='dashboard-section '>
            <div className="box-ibox-title">
              <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                Karma Points
              </h5>
            </div>
            <Card className='p-4 '>
              <div className='d-flex justify-content-around'>
                <h5 className='fw-bold'>Current Year</h5>
                <h1>12</h1>
                <h5 className='fw-bold'>Total</h5>
                <h1>117</h1>
              </div>
            </Card>
          </div>
          <div className='dashboard-section'>
            <h3 style={{ fontSize: "20px", fontWeight: "600" }}>
              Task Gallery
            </h3>
            <AliceCarousel controlsStrategy={"responsive"} items={items} responsive={b} disableButtonsControls={true} autoPlay={true} infinite={true} autoPlayStrategy={'default'} autoPlayInterval={3000} />
          </div>
          <div className='dashboard-section'>
            <div className="box-ibox-title">
              <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                My Volunteering Log
              </h5>
            </div>
            <Card className='p-4'>
              <div className='volunteerlog-subheading'>
                <h3>Tasks you are working on</h3>
              </div>
              <Table responsive="xl" striped className='mb-5'>
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>Status</td>
                    <td><i class="fas fa-map-marker-alt pr-2"></i>Location</td>
                    <td> <i class="far fa-clock pr-2"></i>Hours</td>
                  </tr>
                </thead>
                <tbody>
                  {TasksworkingOn.map((x, index) => (
                    <tr>
                      <td>
                        <div className='d-flex'>
                          <img src={x.image} alt="" width={50}></img>
                          <span className='volunteerlog-table-name'>{x.name}</span>
                        </div>
                      </td>
                      <td> <span
                        style={{
                          backgroundColor: "#4caf4f",
                          color: "white",
                          padding: "5px",
                          fontSize: "10px",
                        }}
                      >
                        {x.status}
                      </span></td>
                      <td>{x.location}</td>
                      <td>{x.hours}<br />Hours</td>
                    </tr>
                  ))}

                </tbody>
              </Table>
              <div className='volunteerlog-subheading'>
                <h3>Tasks you want to help on</h3>
              </div>
              <Table responsive="xl" striped className='mb-5'>
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>Status</td>
                    <td><i class="fas fa-map-marker-alt pr-2"></i>Location</td>
                    <td> <i class="far fa-clock pr-2"></i>Hours</td>
                  </tr>
                </thead>
                <tbody>
                  {TasksWantToHelp.map((x, index) => (
                    <tr>
                      <td>
                        <div className='d-flex'>
                          <img src={x.image} alt="" width={50}></img>
                          <span className='volunteerlog-table-name'>{x.name}</span>
                        </div>
                      </td>
                      <td> <span
                        style={{
                          backgroundColor: "#4caf4f",
                          color: "white",
                          padding: "5px",
                          fontSize: "10px",
                        }}
                      >
                        {x.status}
                      </span></td>
                      <td>{x.location}</td>
                      <td>{x.hours}<br />Hours</td>
                    </tr>
                  ))}

                </tbody>
              </Table>
            </Card>
          </div>

        </Col>
        <Col>
          <div className='dashboard-section '>
            <div className="box-ibox-title">
              <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                Certificates
              </h5>
            </div>
            <Card className="p-3">
              <Link to="http://projectheena.in/certificate/view-certificates" className='basecolor'>
                Download Volunteering Certificates
              </Link>
            </Card>
          </div>
          <div className='dashboard-section'>
            <div className="box-ibox-title">
              <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                Share with friends
              </h5>
            </div>
            <Card className="p-4">
              <div className='d-flex flex-column'>
                <div className='d-flex align-items-center'>
                  <i class="fab fa-facebook-square" style={{ color: "#3b5998", fontSize: "40px" }}></i>
                  <button style={{ backgroundColor: "#1877f2", color: "white", border: "0px", margin: '0 10px' }}>
                    <FaThumbsUp color="#FFFFFF" /><span className='px-2'>Like 1.6K</span>
                  </button>
                </div>
                <div className='d-flex align-items-center'>
                  <i class="fab fa-twitter-square my-2" style={{ color: "#00aced", fontSize: "40px" }}></i>
                  <button style={{ backgroundColor: " #00acee", color: "white", border: "0px", margin: '0 10px' }}>
                    <FaTwitter color="#FFFFFF" /><span className='px-2'>Follow</span>
                  </button>
                </div>
                <i class="fab fa-google-plus-square" style={{ color: "#dd4b39", fontSize: "40px" }}></i>
              </div>
            </Card>
          </div>

          <div className='dashboard-section'>
            <div className="box-ibox-title">
              <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                Suggested Task
              </h5>
            </div>
            <Card className='p-4'>
              <div className="py-2" style={{ borderBottom: "1px solid #dedede" }}>
                <img src="http://projectheena.in/uploads/projects/20143920894594/thb/default.jpg" alt="" className='rounded-circle ' width={50}></img>
                <span className='px-2' style={{ fontSize: "14px" }}>Clean mumbai Beach</span>
              </div>
            </Card>
          </div>
        </Col>
      </Row>
    </div >
  )
}

export default MyPage
