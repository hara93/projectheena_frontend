/* eslint-disable no-restricted-imports */
import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { Images } from "../config/Images";
import "../../index.scss";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <>
      <div
        style={{ marginTop: "3vh", marginBottom: "3vh" }}
        role="tabpanel"
        hidden={value !== index}
        id={`vertical-tabpanel-${index}`}
        aria-labelledby={`vertical-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    </>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    height: 224,
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
}));

export default function VerticalTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <h4>Help</h4>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab label="Volunteering" {...a11yProps(0)} />
        <Tab label="Donation" {...a11yProps(1)} />
        <Tab label="Advocacy" {...a11yProps(2)} />
        <Tab label="Collaboration" {...a11yProps(3)} />
      </Tabs>
      <TabPanel value={value} index={0}>
        <h5>Welcome to the World of Volunteering</h5>
        <hr style={{ backgroundColor: "lightgray", marginTop: "0px", width: "100%" }} />
        <div className="container">
          <div className="row">
            <div className="col-1">
              <img src={Images.volunteer} style={{ width: "100%", height: "100%" }} />
            </div>
            <div className="col-11">
              <p>
                Congratulations, on your decision to take action and Volunteer for Change. We all
                desire a better world and have ideas that can bring transformation. However it takes
                real action to make the impact the world needs. Ready to be a 'Doer' ? Have any
                doubts on Volunteering? Let us address these right away and welcome you to the world
                of doing good.
              </p>
            </div>
          </div>
        </div>

        <div>
          <h6 style={{ marginTop: "2vh" }}>What is Volunteering?</h6>
          <p style={{ marginTop: "2vh" }}>
            Volunteering has always been synonymously used with the terms Community Services or
            pro-bono help. While most of us have the privilege to lead a good life, we know that a
            huge community outside our four walls needs more help and care.
          </p>
        </div>
        <div>
          <blockquote>
            <p>Volunteers do not necessarily have the time; they just have the heart.</p>
            <small>Elizabeth Andrew</small>
          </blockquote>
        </div>
        <div>
          <h6 style={{ marginTop: "2vh" }}>
            Why Volunteer? What are the benefits of Volunteering?
          </h6>
          <p>
            While volunteering may seem a great contribution from your side, it also is a great
            experience and brings gain to the giver too! Surprised! Lets highlight why should one
            volunteer
          </p>
          <p style={{ fontStyle: "bold" }}>When you Volunteer You Give</p>
          <ul>
            <li>A better life to a needy person</li>
            <li>A way for someone to be self sustainable or access a resource</li>
            <li>A better society that cleaner, greener and altruistic in nature</li>
            <li>
              A community that cherishes all its achievements and shares all the challenges together
            </li>
            <li>A better planet that grows together</li>
            <li>And many more ....</li>
          </ul>
        </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        Item Two
      </TabPanel>
      <TabPanel value={value} index={2}>
        Item Three
      </TabPanel>
      <TabPanel value={value} index={3}>
        Item Four
      </TabPanel>
    </div>
  );
}
