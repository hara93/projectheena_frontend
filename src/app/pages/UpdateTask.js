/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import { Images } from "../config/Images";
import { Col, Row } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { useFormik } from "formik";
import axios from "../config/axios";
import { useSelector } from "react-redux";
import moment from "moment";
import { string, object } from "yup";

const UpdateTask = (props) => {
  var task_id = localStorage.getItem("task_data_id");
  const [image, setImage] = useState("");
  const [reply, setReply] = useState();
  const [visibility, setVisibility] = useState();
  const [posts, setPosts] = useState([]);
  const [postFeed, setPostFeed] = useState([]);
  const [postNumber, setPostNumber] = useState(0);
  const user = useSelector((state) => state.phAuth.user);
  const handleFileSelect = (fileData) => {
    setImage(fileData.currentTarget.files[0]);
  };
  const validationSchema = object().shape({
    update_message: string().required("Post message is required"),
  });
  const onSubmit = (values) => {
    const data = new FormData();
    data.set("image", image);
    data.set("update_for_id", props.data._id);
    data.set("update_for_type", 1);
    data.set("update_from_type", 1);
    data.set("update_from_id", user._id);
    data.set("update_from_name", user.first_name);
    data.set("update_from_slug", user.first_name);
    data.set("update_message", values.update_message);
    data.set("update_visibility", visibility);
    // console.log("image", data.get("image"));
    // console.log("update_for_id", data.get("update_for_id"));
    // console.log("update_for_type", data.get("update_for_type"));
    // console.log("update_from_type", data.get("update_from_type"));
    // console.log("update_from_id", data.get("update_from_id"));
    // console.log("update_from_name", data.get("update_from_name"));
    // console.log("update_from_slug", data.get("update_from_slug"));
    // console.log("update_message", data.get("update_message"));
    // console.log("update_visibility", data.get("update_visibility"));
    axios
      .post("/task/uploadComment", data)
      .then((res) => {
        console.log("res", res);
        window.location.reload();
      })
      .catch((err) => console.log("err", err));
  };
  const { handleSubmit, errors, handleChange, touched, values } = useFormik({
    initialValues: { update_message: "" },
    enableReinitialize: true,
    onSubmit,
    validationSchema,
  });

  const getCommentsById = () => {
    axios
      .get(`/task/comments/${task_id}`)
      .then((response) => {
        console.log("res", response);
        if (response.data.length > 0) {
          setPosts(response.data);
          response.data.map((e) => postFeed.push({ reply: "" }));
          var temp = [];
          response.data
            .filter((filt) => filt.update_ref_id == undefined)
            .map((ele) => temp.push(ele));
          props.updates(temp);
          setPostNumber(temp.length)
        } else {
          console.log("there are no posts");
        }
      })
      .catch((err) => console.log("err", err));
  };

  const handleFav = (ele) => {
    axios
      .post("/task/toggleFav", {
        is_favourite: ele.is_favourite,
        update_id: ele._id,
        task_id: task_id,
      })
      .then((res) => setPosts(res))
      .catch((err) => console.info("err", err));
  };
  const handleReplyChange = (e, idx) => {
    const values = [...postFeed];
    values[idx].reply = e.target.value;
    setPostFeed(values);
  };
  const handleReply = (ele, idx) => {
    var values = {};
    values.update_for_id = task_id;
    values.update_for_type = 1;
    values.update_from_type = 1;
    values.update_from_id = user._id;
    values.update_from_name = user.first_name;
    values.update_from_slug = user.first_name;
    values.update_message = postFeed[idx].reply;
    values.update_ref_id = ele._id;

    axios
      .post("/task/taskUpdateReply", values)
      .then((res) => {
        setPosts(res);
      })
      .catch((err) => console.log("err", err));
    const arr = [...postFeed];
    arr.forEach((ele) => (ele.reply = ""));

    setReply(false);
  };

  useEffect(() => {
    getCommentsById();
  }, []);

  return (
    <>
      <div>
        <div style={{ marginBottom: "25px" }}>
          {user._id === props.data.task_creator_id ? (
            <>
              <div className="box-ibox-title">
                <h3 style={{ fontSize: "16px", fontWeight: "600" }}>Post New Update</h3>
              </div>
              <div
                style={{
                  padding: "15px 20px 20px 20px",
                  border: "1px solid #e7eaec",
                }}
              >
                <Form onSubmit={handleSubmit}>
                  <Form.Group>
                    <Form.Row style={{ margin: "10px 0" }}>
                      <Form.Control
                        name="update_message"
                        as="textarea"
                        placeholder="Share any update or task status with fellow volunteers"
                        className="green-focus"
                        onChange={handleChange}
                        error={errors.update_message}
                        value={values.update_message}
                        touched={touched.update_message}
                      />
                      {touched && errors.update_message && (
                        <span className="text-danger" style={{ padding: "5px 0" }}>
                          {errors.update_message}
                        </span>
                      )}
                    </Form.Row>
                    <Form.Row style={{ margin: "10px 0" }}>
                      <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                        Attach File
                      </Form.Label>

                      <Col className="form-input-align-center">
                        <Form.Group style={{ marginBottom: "0" }}>
                          <input name="image" type="file" onChange={handleFileSelect} />
                        </Form.Group>
                      </Col>
                    </Form.Row>
                    <Form.Row style={{ margin: "10px 0" }}>
                      <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                        Visibility
                      </Form.Label>

                      <Col
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          alignItems: "normal",
                        }}
                      >
                        <RadioGroup
                          aria-label="gender"
                          name="milestone"
                        // value={location}
                        // onChange={(e) => handleChangeLocation(e)}
                        >
                          <div>
                            <FormControlLabel
                              value="0"
                              control={<Radio color="primary" />}
                              label="Everyone"
                              onChange={(e) => setVisibility(e.target.value)}
                            />
                            <FormControlLabel
                              value="1"
                              control={<Radio color="primary" />}
                              label="Logged in"
                              onChange={(e) => setVisibility(e.target.value)}
                            />
                            <FormControlLabel
                              value="2"
                              control={<Radio color="primary" />}
                              label="Engaged Volunteers"
                              onChange={(e) => setVisibility(e.target.value)}
                            />
                          </div>
                        </RadioGroup>
                        <div style={{ fontSize: "11px", maxWidth: "160px", paddingTop: "10px" }}>
                          <span>
                            Some updates are public, some available to logged in users and others
                            available to the ones who have engaged on the activity
                          </span>
                        </div>
                      </Col>
                    </Form.Row>
                    <Form.Row>
                      <Form.Label column="lg" lg={2}></Form.Label>

                      <Col className="form-input-align-center">
                        <button className="post-update-btn" type="submit">
                          {" "}
                          Post Update
                        </button>
                      </Col>
                    </Form.Row>
                  </Form.Group>
                </Form>
              </div>
            </>
          ) : null}
        </div>
        <div>
          <h3 className="font-bold" style={{ fontSize: "16px" }}>
            {postNumber} Updates Shared
          </h3>
        </div>
        <div>
          {posts.length > 0
            ? posts
              .filter((filt) => filt.update_ref_id == undefined)
              .map((ele, idx) => (
                <Row style={{ margin: "20px 0" }}>
                  <Col md={2}>
                    <div style={{ textAlign: "center" }}>
                      <div style={{ display: "flex", justifyContent: "center" }}>
                        {" "}
                        <img
                          height="48px"
                          width="48px"
                          className="img-circle"
                          src={Images.userlogo}
                          alt="himanshu chanda"
                        ></img>
                      </div>
                      <span style={{ whiteSpace: "nowrap", fontSize: "12px", fontWeight: "600" }}>
                        {ele.update_from_name}
                      </span>
                      <span style={{ whiteSpace: "nowrap", fontSize: "11px" }}>
                        {moment(ele.activity_date_time)
                          .startOf("hour")
                          .fromNow()}
                      </span>
                    </div>
                  </Col>
                  <Col>
                    <div>
                      <p>{ele.update_message}</p>
                    </div>
                    <div
                      style={{ display: "flex", justifyContent: "flex-end", color: "#4caf4f" }}
                    >
                      <div className="pointer" onClick={() => setReply(idx)}>
                        <i
                          class="fas fa-reply"
                          style={{ paddingRight: "5px", color: "#4caf4f", fontSize: "13px" }}
                        ></i>
                        Reply{" "}
                      </div>
                      {ele.is_favourite === 1 ? (
                        <div
                          className="isFav"
                          onClick={() => {
                            handleFav(ele);
                          }}
                        >
                          <i class="fas fa-star base-color"></i>
                        </div>
                      ) : (
                        <div
                          className="isFav"
                          onClick={() => {
                            handleFav(ele);
                          }}
                        >
                          <i class="far fa-star base-color"></i>
                        </div>
                      )}
                    </div>
                    {reply === idx ? (
                      <div style={{ margin: "10px 0" }}>
                        <Form.Control
                          type="text"
                          placeholder="Your reply here"
                          className="green-focus"
                          style={{ height: "50px", margin: "10px 0" }}
                          onChange={(e) => handleReplyChange(e, idx)}
                          value={postFeed[idx].reply}
                        />
                        <div style={{ display: "flex", justifyContent: "flex-end" }}>
                          <button className="reply-btn" onClick={() => handleReply(ele, idx)}>
                            Reply
                          </button>
                        </div>
                      </div>
                    ) : null}
                    {ele.filename != null ? (
                      <div>
                        <div style={{ border: "1px solid #ddd", width: "fit-content" }}>
                          <div>
                            <img
                              src={`http://localhost:5000/uploads/images/task/${ele.filename}`}
                              height="100px"
                              width="200px"
                              alt="post"
                            ></img>
                          </div>
                          <div style={{ padding: "10px", backgroundColor: "#f8f8f8" }}>
                            <span className="base-color">{ele.original_name}</span>
                            <br />
                            <br />
                            <span>
                              Added : {moment(ele.activity_date_time).format("Do MMM, YYYY")}
                            </span>
                          </div>
                        </div>
                      </div>
                    ) : null}
                    {posts
                      .filter((reply) => ele._id === reply.update_ref_id)
                      .map((ele, idx) => (
                        <div
                          style={{
                            backgroundColor: "#f8f8f8",
                            display: "flex",
                            justifyContent: "space-between",
                            border: "1px solid #ddd",
                            padding: "10px",
                            margin: "10px 0",
                          }}
                        >
                          <span>{ele.update_message}</span>

                          <div style={{ textAlign: "center", width: "130px" }}>
                            <div style={{ display: "flex", justifyContent: "center" }}>
                              {" "}
                              <img
                                height="48px"
                                width="48px"
                                className="img-circle"
                                src={Images.user_logo}
                                alt="himanshu chanda"
                              ></img>
                            </div>
                            <span
                              style={{
                                whiteSpace: "nowrap",
                                fontSize: "12px",
                                fontWeight: "600",
                              }}
                            >
                              {ele.update_from_name}
                            </span>
                            <br />
                            <span style={{ whiteSpace: "nowrap", fontSize: "11px" }}>
                              {moment(ele.activity_date_time)
                                .startOf("hour")
                                .fromNow()}
                            </span>
                          </div>
                        </div>
                      ))}
                  </Col>
                </Row>
              ))
            : null}
        </div>
      </div>
    </>
  );
};
export default UpdateTask;
