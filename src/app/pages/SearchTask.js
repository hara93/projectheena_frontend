import React, { useState, useEffect } from "react";
import { Col, Row, Card, OverlayTrigger, Tooltip, Button } from "react-bootstrap";
import axios from "../config/axios";
import TurndownService from "turndown";
import { Images } from "../config/Images";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { getAllTaskList } from "../../redux/reducers/common/actionCreator";

const SearchTask = (props) => {
  console.log("props", props.list);
  let history = useHistory();
  const dispatch = useDispatch();

  const user = useSelector((state) => state.phAuth.user);
  const taskList = useSelector((state) => state.common.taskList || []);
  useEffect(() => {
    setTask(taskList)
    props.count(taskList.length);
  }, [taskList]);
  // console.log("llllist", taskList);
  // const taskFilter = useSelector((state) => state.common.taskFilter);

  const [task, setTask] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [taskPerPage] = useState(6);
  const [taskId, setTaskId] = useState();
  const [helpReason, setHelpReason] = useState("");
  const [shareContact, setShareContact] = useState(0);
  const [shareEmail, setShareEmail] = useState(0);
  const [joining, setJoining] = useState(0);
  const [show, setShow] = useState(false);
  const [showModalAcknowledgement, setShowModalAcknowledgement] = useState(false);

  const [taskFilter, setTaskFilter] = useState({});

  var turndown = new TurndownService();

  const indexOfLastTask = currentPage * taskPerPage;
  const indexOfFirstTask = indexOfLastTask - taskPerPage;
  const currentTask = task.slice(indexOfFirstTask, indexOfLastTask);
  const pageNumber = [];
  const totalTask = task.length;

  for (let i = 1; i <= Math.ceil(totalTask / taskPerPage); i++) {
    pageNumber.push(i);
  }

  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      This is a continuous task
    </Tooltip>
  );
  const onSubmit = (values) => {
    var data = {};
    data.task_id = taskId;
    data.task_user_id = user._id;
    data.task_user_shared_email = shareEmail;
    data.task_user_shared_contact_number = shareContact;
    data.task_user_type = "0";
    data.task_comment = helpReason;
    if (joining === "Yes") {
      data.joining = 1;
    } else if (joining === "No") {
      data.joining = 0;
    }
    console.log("values", data);

    axios
      .post("/taskTransaction/offer_help", data)
      .then((res) => {
        console.log("res", res);
        handleShowModalAcknowledgement()
        // window.location.reload();
      })
      .catch((err) => console.log("err", err));
    setShow(false);
    setShareContact(0);
    setShareEmail(0);
    setHelpReason("");
    setJoining(0);
  };
  const { handleSubmit } = useFormik({
    initialValues: { update_message: "" },
    enableReinitialize: true,
    onSubmit,
  });
  const handleClose = () => {
    setShow(false);
    setTaskId(undefined);
  };
  const handleShow = (idx) => setShow(idx);
  const handleShowModalAcknowledgement = () => setShowModalAcknowledgement(true);
  const handleCloseModalAcknowledgement = () => setShowModalAcknowledgement(false);

  const handleShareContact = (e) => {
    e.target.checked ? setShareContact(1) : setShareContact(0);
  };
  const handleEmailContact = (e) => {
    e.target.checked ? setShareEmail(1) : setShareEmail(0);
  };
  const handleViewTask = (creatorId, taskId) => {
    if (user._id === creatorId) {
      history.push(`/task-detail/${taskId}`);
    } else {
      history.push(`/task-detail-user/${taskId}`);
    }
  };
  const getTaskData = () => {
    axios.post("/task/taskList", taskFilter).then((response) => {
      console.log("response -------> task list ->", response)
      if (response.status == 1) {
        const data = response.data;
        setTask(data);
        props.count(response.data.length);
      }
    });
  };
  useEffect(() => {
    dispatch(getAllTaskList({}))
    // getTaskData()
  }, []);
  return (
    <>
      <Row className="m-0">
        {currentTask.map((ele, idx) => (
          <>
            <Col md={6}>
              <Card className="searchTask-card">
                <Row className="m-0">
                  <Col xs={8} className="m-0-p-0">
                    <div style={{ padding: "15px 10px 5px 10px" }}>
                      <h4 className="font-bold" style={{ fontSize: "14px" }}>
                        <span
                          onClick={() => handleViewTask(ele.task_creator_id, ele._id)}
                          className="ph-link pointer"
                        >
                          {ele.task_name}
                        </span>
                      </h4>
                      <p style={{ color: "gray", minHeight: "130px" }}>
                        {(turndown.turndown(ele.task_description)).substring(0, 250)}{"..."}
                      </p>
                    </div>
                  </Col>
                  <Col
                    xs={4}
                    className="m-0-p-0"
                    style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
                  >
                    <div style={{ padding: "15px 10px 10px 10px " }}>
                      <img
                        style={{ maxHeight: "100px", maxWidth: "100px" }}
                        src={Images.View_Profile}
                        alt=""
                      ></img>
                    </div>
                    <div>
                      <button
                        className="ph-btn"
                        style={{ whiteSpace: "nowrap" }}
                        onClick={() => {
                          handleShow(idx);
                          setTaskId(ele._id);
                        }}
                      >
                        Count me in!
                      </button>
                    </div>
                  </Col>
                </Row>
                <div
                  style={{
                    display: "flex",
                    textAlign: "center",
                    justifyContent: "space-between",
                    color: "gray",
                    padding: "0 10px",
                  }}
                >
                  <div style={{ display: "flex", flexDirection: "column" }}>
                    Maximum Duration
                    <p>{ele.task_duration_hours} Hours</p>
                  </div>
                  <div style={{ display: "flex", flexDirection: "column" }}>
                    Max People Required
                    <p>{ele.people_required}</p>
                  </div>
                  <div style={{ display: "flex", flexDirection: "column" }}>
                    Apply Before
                    <OverlayTrigger
                      placement="bottom"
                      delay={{ show: 250, hide: 400 }}
                      overlay={renderTooltip}
                    >
                      <p>&#8734;</p>
                    </OverlayTrigger>
                  </div>
                </div>
              </Card>
            </Col>
            <Modal show={show === idx} onHide={handleClose} animation={false} centered>
              <div className="modal-header-custom">
                <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                  Offer Help on {`"${ele.task_name}"`}
                </h4>
                <Button variant="secondary" style={{ height: "30px" }} onClick={handleClose}>
                  <i class="fas fa-times" style={{ paddingRight: "0", color: "white" }}></i>
                </Button>
              </div>
              <Modal.Body>
                <div style={{ padding: "20px 10px" }}>
                  <Form onSubmit={handleSubmit}>
                    <Form.Group>
                      <Form.Control
                        as="textarea"
                        placeholder="Reason why you wish to help for this task"
                        className="green-focus"
                        value={helpReason}
                        onChange={(e) => setHelpReason(e.target.value)}
                      />
                      <span className="help-block">
                        Max: 500 chars | Remaining Chars: {500 - helpReason.length}
                      </span>
                    </Form.Group>
                    <hr className="form-horizontal-line" />
                    <Form.Group>
                      <Form.Row>
                        <Form.Label column="lg">
                          Will any of your family members be joining you?
                        </Form.Label>
                      </Form.Row>
                      <RadioGroup>
                        <div style={{ display: "flex" }}>
                          <FormControlLabel
                            id={1}
                            control={<Radio color="primary" />}
                            label="Yes"
                            value={"Yes"}
                            checked={joining === "Yes" ? 1 : 0}
                            onChange={(e) => {
                              setJoining(e.target.value);
                            }}
                          />
                          <FormControlLabel
                            id={2}
                            control={<Radio color="primary" />}
                            label="No"
                            value={"No"}
                            checked={joining === "No" ? 1 : 0}
                            onChange={(e) => {
                              setJoining(e.target.value);
                            }}
                          />
                        </div>
                      </RadioGroup>
                    </Form.Group>
                    <hr className="form-horizontal-line" />
                    <Form.Group>
                      <FormControlLabel
                        control={<Checkbox color="primary" />}
                        label={"Share Contact Number"}
                        value={shareContact}
                        onChange={handleShareContact}
                      />
                      <FormControlLabel
                        control={<Checkbox color="primary" />}
                        label={"Share Email Address"}
                        value={shareEmail}
                        onChange={handleEmailContact}
                      />
                    </Form.Group>
                    <hr className="form-horizontal-line" />
                    <Form.Group
                      as={Row}
                      controlId="formPlaintextPassword"
                      style={{ marginLeft: "10px" }}
                    >
                      <Form.Label column sm="4">
                        Offer help as:
                      </Form.Label>
                      <Col sm="6">
                        <Form.Control as="select">
                          <option>Individual</option>
                        </Form.Control>
                      </Col>
                    </Form.Group>

                    <hr className="form-horizontal-line" />
                    <Form.Group>
                      <button className="ph-btn" type="submit">
                        Save Changes
                      </button>
                    </Form.Group>
                  </Form>
                </div>
              </Modal.Body>
            </Modal>
            <Modal show={showModalAcknowledgement} onHide={handleCloseModalAcknowledgement} animation={false} >
              <div className="modal-header-custom">
                <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                  Offer Help on {`"${ele.task_name}"`}
                </h4>
              </div>
              <Modal.Body>
                <div style={{ padding: "20px 10px" }}>
                  <p>
                    You Have Successfully Offered Help for this Task
                  </p>
                  <p>
                    Your application has been successfully submitted. An NGO SPOC will get in touch with you within 48-72 hrs.
                  </p>
                </div>
              </Modal.Body>
            </Modal>
          </>
        ))}
      </Row>
      <nav>
        <ul className="pagination">
          {pageNumber.map((item) => (
            <li key={item} className="page-item">
              <button onClick={() => setCurrentPage(item)} className="page-link">
                {item}
              </button>
            </li>
          ))}
        </ul>
      </nav>
    </>
  );
};

export default SearchTask;