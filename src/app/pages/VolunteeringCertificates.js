import React from 'react'
import { Card } from "react-bootstrap"
const VolunteeringCertificates = () => {
    return (
        <div className="container">
            <div>
                <h3 className="fw-bold">Volunteering Certificates</h3>
                <p>All your volunteering related certificates will be stored here.</p>
                <p>Certificates are generated based on volunteering done in each financial year.</p>
            </div>
            <Card>

            </Card>
        </div>
    )
}

export default VolunteeringCertificates
