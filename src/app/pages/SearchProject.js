import React, { useState, useEffect } from "react";
import { Col, Row, Card } from "react-bootstrap";
// import { Card } from "@material-ui/core";
// import PeopleCard from "../components/PeopleCard";
import axios from "../config/axios";
import { Images } from "../config/Images";

// import SearchProjectCard from "../components/ProjectCard";

// const projectData = [
//   {
//     img: "",
//     name: "Clean Your place 124",
//     description:
//       "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
//   },
//   {
//     img: "",
//     name: "Clean Your place",
//     description:
//       "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
//   },
//   {
//     img: "",
//     name: "Clean Your place",
//     description:
//       "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
//   },
//   {
//     img: "",
//     name: "Clean Your place",
//     description:
//       "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
//   },
//   {
//     img: "",
//     name: "Clean Your place",
//     description:
//       "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
//   },
// ];
const SearchProject = (props) => {
  const [project, setproject] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [projectPerPage] = useState(6);

  const indexOfLastProject = currentPage * projectPerPage;
  const indexOfFirstProject = indexOfLastProject - projectPerPage;
  const currentProject = project.slice(indexOfFirstProject, indexOfLastProject);

  const pageNumber = [];
  const totalProject = project.length;
  // console.log(totalProject);
  const getProjectData = () => {
    axios.post("/project/projectListForSearch").then((response) => {
      const data = response.data;
      console.log("response", response);
      setproject(data);
      props.count(response.data.length);
    });
  };

  for (let i = 1; i <= Math.ceil(totalProject / projectPerPage); i++) {
    pageNumber.push(i);
  }
  useEffect(() => getProjectData(), []);
  // console.log("project data", project);

  return (
    <>
      <Row className="m-0">
        {currentProject.map((ele, index) => (
          <Col lg={6} style={{ margin: "10px 0" }}>
            <Card className="p-2 project-card ">
              <Row>
                <Col xs={8}>
                  <div style={{ padding: "5px" }}>
                    <h4 className="font-bold" style={{ fontSize: "14px" }}>
                      <a href={`/createProjects/${ele.project_name}`} className="ph-link">
                        {ele.project_name}
                      </a>
                    </h4>
                    <p style={{ color: "gray", minHeight: "130px" }}>{ele.project_excerpts}</p>
                  </div>
                </Col>
                <Col xs={4}>
                  <div style={{ padding: "5px" }}>
                    <img
                      style={{ width: "100%", height: "50%" }}
                      src={Images.View_Profile}
                      alt=""
                    ></img>
                  </div>
                </Col>
              </Row>
            </Card>
          </Col>
        ))}
      </Row>
      <nav>
        <ul className="pagination">
          {pageNumber.map((item) => (
            <li key={item} className="page-item">
              <button onClick={() => setCurrentPage(item)} className="page-link">
                {item}
              </button>
            </li>
          ))}
        </ul>
      </nav>
    </>
  );
};

export default SearchProject;
