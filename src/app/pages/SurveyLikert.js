import React, {useState} from "react";
import {
  Form,
  Card,
  Container,
  Row,
  Col,
  OverlayTrigger,
  Tooltip,
  Modal,
  Button,
} from "react-bootstrap";
import Question from "../components/Question";
import Example from "../components/Phone";

const SurveyLikert = () => {
    const [likert, setLikert] = useState(true);
   const handleChange = () =>{setLikert(true)}
   const handleEmoji = () =>{setLikert(false)}


  return (
      <>
    <Card className="p-5">
    <Question />

    <Form className="p-3">
    
      <Form.Label style={{color:'#9a9ba5',marginTop:'3%'}}>
       Options:
        
      </Form.Label>
      <div style={{color:'#9a9ba5'}}>
        <input type="radio" name="choice" onChange={handleChange}></input> <label >Likert Scale</label>
         <input type="radio" name="choice" style={{marginLeft:'5%'}} onChange={handleEmoji}></input> <label>Emoji</label>
          
      </div>

      {
          likert==true?
          <ul style={{marginTop:'5%'}}>
          <li style={{listStyleType:'circle'}}> <input type="text" placeholder="Extremely Unsatisfied"></input></li>
          <li style={{listStyleType:'circle'}}><input type="text" placeholder="Unsatisfied"></input></li>
          <li style={{listStyleType:'circle'}}><input type="text" placeholder="Neutral"></input></li>
          <li style={{listStyleType:'circle'}}><input type="text" placeholder="Satisfied"></input></li>
          <li style={{listStyleType:'circle'}}><input type="text" placeholder="Extremely Satisfied"></input></li>
      </ul>:
      <ul style={{marginTop:'5%'}}>
      <li style={{listStyleType:'none'}} className="emoli"> <input type="text" placeholder="Extremely Unsatisfied"></input></li>
      <li style={{listStyleType:'none'}} className="emoli1"><input type="text" placeholder="Unsatisfied"></input></li>
      <li style={{listStyleType:'none'}} className="emoli2"><input type="text" placeholder="Neutral"></input></li>
      <li style={{listStyleType:'none'}} className="emoli3"><input type="text" placeholder="Satisfied"></input></li>
      <li style={{listStyleType:'none'}} className="emoli4"><input type="text" placeholder="Extremely Satisfied"></input></li>
  </ul>


      }
      

      
     
     <hr></hr>
     <i class="fas fa-trash"> Delete</i>
     <i class="fas fa-compress-alt" style={{marginLeft:'2%'}}>Move</i>
     <i class="fas fa-copy" style={{marginLeft:'2%'}}> Copy</i>
         
    </Form>
  </Card>
  </>
  );
};

export default SurveyLikert;
