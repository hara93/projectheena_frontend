/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";
import { Table } from "react-bootstrap";
import { useSelector } from "react-redux";
import axios from "../config/axios";

const Deleted = () => {

  const user = useSelector((state) => state.phAuth.user);
  const getDeletedTaskList = () => {
    axios.post("/task/getDeletedTaskList", { creatorId: user._id }).then((response) => {
      const data = response.data;
      console.log("deleted List", data);
      setDeleted(data);
    });
  };
  useEffect(() => getDeletedTaskList(), []);
  const [deleted, setDeleted] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [deletedPerPage] = useState(4);

  const indexOfLastDeleted = currentPage * deletedPerPage;
  const indexOfFirstDeleted = indexOfLastDeleted - deletedPerPage;
  const currentDeleted = deleted.slice(indexOfFirstDeleted, indexOfLastDeleted);

  const pageNumber = [];
  const totalDeleted = deleted.length;

  for (let i = 1; i <= Math.ceil(totalDeleted / deletedPerPage); i++) {
    pageNumber.push(i);
  }

  return (
    <>
      <Card>
        <h3 className="ibox-title" style={{ fontSize: "16px", fontWeight: "600" }}>
          My Volunteering Log
        </h3>
        <div
          className="ibox-content"
          style={{
            borderStyle: "solid solid none",
            borderWidth: "1px 0",
            borderColor: "#e7eaec",
          }}
        >
          <Table responsive="xl" striped>
            <thead>
              <tr>
                <td>Name</td>
                <td>Status</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {currentDeleted.map((ele, index) => (
                <tr>
                  <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                    <a className="ph-link" href={`/task-detail/${ele._id}`}>
                      {ele.task_name}
                    </a>
                  </td>
                  <td style={{ paddingBottom: "5", fontSize: "11px" }}>
                    <span
                      style={{
                        backgroundColor: "#4caf4f",
                        color: "white",
                        padding: "5px",
                      }}
                    >
                      Deleted
                    </span>
                  </td>
                  <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                    <a href="#">
                      <i style={{ color: "#4caf4f" }} class="fas fa-eye"></i>
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </Card>
      <nav>
        <ul className="pagination">
          {pageNumber.map((item) => (
            <li key={item} className="page-item">
              <button onClick={() => setCurrentPage(item)} className="page-link">
                {item}
              </button>
            </li>
          ))}
        </ul>
      </nav>
    </>
  );
};
export default Deleted;
// <tr>
//               <td>
//                 <a style={{ color: "green" }} href="#">
//                   Old Age Home
//                 </a>
//               </td>
//               <td>
//                 <span
//                   style={{
//                     backgroundColor: "green",
//                     color: "white",
//                     padding: "5px",
//                   }}
//                 >
//                   Deleted
//                 </span>
//               </td>
//               <td>
//                 <a href="#">
//                   <i style={{ color: "green" }} class="fas fa-eye"></i>
//                 </a>
//               </td>
//             </tr>
//             <tr>
//               <td>
//                 <a style={{ color: "green" }} href="#">
//                   Basic Life Support Training for Volunteers
//                 </a>
//               </td>
//               <td>
//                 <span
//                   style={{
//                     backgroundColor: "green",
//                     color: "white",
//                     padding: "5px",
//                   }}
//                 >
//                   Deleted
//                 </span>
//               </td>
//               <td>
//                 <a href="#">
//                   <i style={{ color: "green" }} class="fas fa-eye"></i>
//                 </a>
//               </td>
//             </tr>
