import React from "react";
import { Form } from "react-bootstrap";
import { Col, Card } from "react-bootstrap";
// import { Card } from "@material-ui/core";
// import { Images } from "../config/Images";
import CustomButton from "../components/CustomButton";
import TextEditor from "../components/textEditor";
// import CKEditors from "../components/CKEditor";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/froala_editor.pkgd.min.css";
import FroalaEditorComponent from "react-froala-wysiwyg";
import "froala-editor/js/plugins.pkgd.min.js";

class Contact extends React.Component {
  constructor() {
    super();
    this.state = {
      data: 256,
      text: "",
      max: 2000,
    };
    this.handleChange = this.handleModelChange.bind(this);
  }
  handleModelChange = (e, editor) => {
    const char = e;
    var plainText = char.replace(/<[^>]+>/g, "");

    this.setState({
      text: plainText,
    });
  };

  config = {
    charCounterMax: 2000,
  };

  render() {
    console.log(this.state.text, "outside fn");
    return (
      <>
        <Card>
          <div className="box-ibox-title" style={{ borderBottom: "1px solid rgb(231 231 231)" }}>
            <h5 style={{ fontSize: "14px", fontWeight: "600" }}>Connect With Your Tribe</h5>
          </div>
          <div
            style={{
              padding: "15px 20px 20px 20px",
              border: "1px solid rgb(231 231 231)",
            }}
          >
            <p>
              Use the below section to send notifications to your followers about latest do good
              activities / campaigns you have been doing. People who follow you will get
              notifications on ProjectHeena, SMS or Email basis their preference set.
            </p>
            <hr
              className="form-horizontal-line"
              style={{ marginBottom: "20px ", marginTop: "0" }}
            />

            <Form>
              <Form.Group>
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Email Subject
                  </Form.Label>
                  <Col>
                    <Form.Control type="text" className="green-focus" />
                  </Col>
                </Form.Row>
              </Form.Group>
              <hr className="form-horizontal-line" style={{ margin: "20px 0" }} />
              <Form.Group>
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Email Body
                  </Form.Label>
                  <Col>
                    <div style={{ maxWidth: "100%" }}>
                      <FroalaEditorComponent
                        tag="textarea"
                        config={this.config}
                        onModelChange={this.handleModelChange}
                      />
                    </div>
                    <span className="help-block">
                      Min : 256 | Max : 2000 | Remaining : {this.state.max - this.state.text.length}
                    </span>
                  </Col>
                </Form.Row>
              </Form.Group>

              <hr className="form-horizontal-line" style={{ margin: "20px 0" }} />
              <Form.Group>
                <Form.Row>
                  <Form.Label column="lg" lg={2}></Form.Label>
                  <Col>
                    <CustomButton content={`Send Mail`} />
                  </Col>
                </Form.Row>
              </Form.Group>
            </Form>
          </div>
        </Card>
      </>
    );
  }
}
export default Contact;
