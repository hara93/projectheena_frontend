import React from 'react';
import { Card, Row, Col, Table } from 'react-bootstrap';

import { FaFacebook, FaTwitterSquare, FaGooglePlus, FaYoutube, FaThumbsUp, FaTwitter } from 'react-icons/fa';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FileTable from '../components/FileTable';
import { Images } from '../config/Images';


const records = [
    {
        Sr_No: "1",
        icon: "",
        file_name: "Preparatory_Camp.png",
        upload_by: "Social Work",
        visibility: "Collaborator and Admin",
        date: "31st May 2016"

    }
]






class Files extends React.Component {
    constructor() {
        super();
        this.state = {
            clicked: false,
            gallery_view: true,
            list_view: false
        }
    }

    handleClick() {

        this.state.clicked == false ? this.setState({ clicked: true }) : this.setState({ clicked: false })
    }

    handleList() {
        this.state.list_view == false ? this.setState({ list_view: true, gallery_view: false, clicked: false }) : this.setState({ list_view: false, gallery_view: true })
    }

    handleGallery() {
        this.state.gallery_view == true ? this.setState({ gallery_view: false, list_view: true }) : this.setState({ gallery_view: true, list_view: false })

    }


    render() {
        return (
            <>
                <div>
                    <Card className="p-4">


                        <Row>
                            <Col md={11}>
                                <div className="item">
                                    <h3 className="csr_heading"><i class="fa fa-picture-o"></i> Gallery</h3>
                                </div>

                                {/* <h3 style={{ marginBottom: '2vh', display: 'inline-block' }}> <i className="fa fa-picture-o fa-sm" style={{ marginRight: '1vw' }}></i></h3> */}
                            </Col>

                            <Col md={1}>
                                <div className="flex-direction-column">
                                    <a style={{ display: 'inline-block' }} onClick={this.handleGallery.bind(this)}><i class="fa fa-th p-1 " aria-hidden="true"></i></a>
                                    <a style={{ display: 'inline-block' }} onClick={this.handleList.bind(this)}><i class="fa fa-th-list ml-2 p-1" aria-hidden="true"></i></a>
                                </div>
                            </Col>
                        </Row>

                        <Row className="p-4">
                            <Col md={4}>
                                <Row>
                                    <Col md={6}>
                                        <span style={{ marginLeft: '-9%' }}>Collaborator Name</span>
                                    </Col>
                                    <Col md={6}>
                                        <select>
                                            <option >All</option>
                                            <option >Umang</option>
                                            <option>Social Work</option>
                                            <option >Glenmark</option>
                                        </select>
                                    </Col>
                                </Row>
                            </Col>

                            <Col md={4}>
                                <Row>
                                    <Col md={4}>
                                        <span>Date</span>
                                    </Col>
                                    <Col md={6}>
                                        <input type="date" />
                                    </Col>
                                </Row>
                            </Col>

                            <Col md={4}>
                                <Row>
                                    <Col md={6}>
                                        <span>File Type</span>
                                    </Col>
                                    <Col md={6}>
                                        <select>
                                            <option >All</option>
                                            <option >PDF</option>
                                            <option>Image</option>
                                            <option >zip</option>
                                        </select>
                                    </Col>
                                </Row>
                            </Col>

                        </Row>




                        {
                            this.state.gallery_view == true ?
                                <Row>
                                    <Col md={12}>
                                        <div className="item">
                                            <h3 className="csr_heading"> Milestones</h3>
                                        </div>
                                        {/* <h5 style={{ marginTop: '2vh', marginLeft: '1vw' }}>Milestones</h5> */}
                                        <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted' }} />

                                        <Row className="p-5">
                                            <Col md={2}>
                                                <a onClick={this.handleClick.bind(this)}> <i className="fa fa-folder fa-5x" aria-hidden="true" ></i> </a>
                                                <p>FY 2016-17 </p>

                                            </Col>
                                            <Col md={2}>
                                                <a onClick={this.handleClick.bind(this)}> <i className="fa fa-folder fa-5x" aria-hidden="true" ></i> </a>
                                                <p>FY 2016-17 </p>

                                            </Col>
                                            <Col md={2}>
                                                <a onClick={this.handleClick.bind(this)}> <i className="fa fa-folder fa-5x" aria-hidden="true" ></i> </a>
                                                <p>FY 2016-17 </p>

                                            </Col>
                                            <Col md={2}>
                                                <a onClick={this.handleClick.bind(this)}> <i className="fa fa-folder fa-5x" aria-hidden="true" ></i> </a>
                                                <p>FY 2016-17 </p>

                                            </Col>
                                            <Col md={2}>
                                                <a onClick={this.handleClick.bind(this)}> <i className="fa fa-folder fa-5x" aria-hidden="true" ></i> </a>
                                                <p>FY 2016-17 </p>

                                            </Col>
                                            <Col md={2}>
                                                <a onClick={this.handleClick.bind(this)}> <i className="fa fa-folder fa-5x" aria-hidden="true" ></i> </a>
                                                <p>FY 2016-17 </p>

                                            </Col>

                                        </Row>
                                        <Row className="p-5">
                                            <Col md={2}>
                                                <a onClick={this.handleClick.bind(this)}> <i className="fa fa-folder fa-5x" aria-hidden="true" ></i> </a>
                                                <p>FY 2016-17 </p>

                                            </Col>
                                            <Col md={2}>
                                                <a onClick={this.handleClick.bind(this)}> <i className="fa fa-folder fa-5x" aria-hidden="true" ></i> </a>
                                                <p>FY 2016-17 </p>

                                            </Col>
                                            <Col md={2}>
                                                <a onClick={this.handleClick.bind(this)}> <i className="fa fa-folder fa-5x" aria-hidden="true" ></i> </a>
                                                <p>FY 2016-17 </p>

                                            </Col>



                                        </Row>


                                    </Col>
                                </Row> :
                                null

                        }

                        {
                            this.state.list_view == true ?
                                <>
                                    <Row className="p-4">
                                        <Col md={4}>
                                            <Row>
                                                <Col md={6}>
                                                    <span>Sort By</span>
                                                </Col>
                                                <Col md={6}>
                                                    <select>
                                                        <option >Milestone</option>
                                                        <option >Date</option>

                                                    </select>
                                                </Col>
                                            </Row>
                                        </Col>

                                    </Row>
                                    <div>
                                        <Row>
                                            <div className="item">
                                                <h3 className="csr_heading"> Milestones</h3>
                                            </div>
                                            {/* <h5 style={{ marginTop: '2vh', marginLeft: '1vw' }}>Milestones</h5> */}
                                            <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted' }} />

                                        </Row>
                                        <Row className="p-3">
                                            <h5>FY 2016-17 - Phase I - Advocacy, Mobilization And Survey</h5>
                                        </Row>

                                        <Row className="p-3">

                                            <Col md={3}>
                                                <Card>


                                                    <img src={Images.Children} style={{ width: '100%' }} />
                                                    <span style={{ display: 'block', fontWeight: '600', height: '20px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', color: '#4CAF50', padding: '2vh 1vw 5vh 1vw' }}> Children_education</span>
                                                    <div style={{ marginBottom: '2vh' }}>
                                                        <small style={{ paddingLeft: '1vw', paddingBottom: '2vh' }}>23rd March 2017</small>

                                                    </div>



                                                </Card>
                                            </Col>
                                            <Col md={3}>
                                                <Card>


                                                    <img src={Images.Children} style={{ width: '100%' }} />
                                                    <span style={{ display: 'block', fontWeight: '600', height: '20px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', color: '#4CAF50', padding: '2vh 1vw 5vh 1vw' }}> Children_education</span>
                                                    <div style={{ marginBottom: '2vh' }}>
                                                        <small style={{ paddingLeft: '1vw', paddingBottom: '2vh' }}>23rd March 2017</small>

                                                    </div>



                                                </Card>
                                            </Col>

                                        </Row>


                                    </div>
                                </>
                                : null
                        }


                        {
                            this.state.clicked == true ?
                                <Row>
                                    <FileTable records={records} />
                                </Row> : null
                        }


                    </Card>


                </div>
            </>
        )
    }
}

export default Files;


