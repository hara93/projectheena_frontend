import React from 'react';
import {Row, Col} from 'react-bootstrap';
class SurveyTypes extends React.Component{
    render(){
        return(
            <>
             <h5
                   className="p-4"
                   style={{
                     fontWeight: "900",
                     color: "#616161",
                     marginTop: "5%",
                     display: "inline-block",
                   }}
                 >
                   Popular Question Types{" "}
                   <span
                     className="p-2"
                     style={{
                       backgroundColor: "#f4f5f5",
                       color: "#2d3e4e",
                       borderRadius: "6px",
                     }}
                   >
                     12
                   </span>
                 </h5>
                 <Row>
                   <Col md={3}>
                     <button
                     onClick={()=>{this.handleButtonClicks(1)}}
                       style={{
                         backgroundColor: "#f4f5f5",
                         border: "1px solid #f4f5f5",
                         width: "100%",
                         marginBottom:'5%',
                         marginRight:'3%'
                       }}
                       
                       className="p-3"
                       value="1"
                     >
                       <i class="fas fa-dot-circle" style={{marginRight:'5%'}}></i>
                       Image
                     </button>
                     <button
                       style={{
                         backgroundColor: "#f4f5f5",
                         border: "1px solid #f4f5f5",
                         width: "100%",
                         marginBottom:'5%',
                         marginRight:'3%'
                       }}
                       onClick={()=>{this.handleButtonClicks(2)}}
                       className="p-3"
                     >
                       <i class="fas fa-dot-circle" style={{marginRight:'5%'}}></i>
                       Video
                     </button>
                     <button
                       style={{
                         backgroundColor: "#f4f5f5",
                         border: "1px solid #f4f5f5",
                         width: "100%",
                         marginBottom:'5%',
                         marginRight:'3%'
                       }}
                       onClick={this.handleGeoTag}
                       className="p-3"
                     >
                       <i class="fas fa-dot-circle" style={{marginRight:'5%'}}></i>
                       Geo Tag
                     </button>
                     
                   </Col>
                   <Col md={3}></Col>
                   <Col md={3}></Col>
                 </Row>
            </>

        )
    }
}

export default SurveyTypes 