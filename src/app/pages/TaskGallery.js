/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";

// import { Images } from "../config/Images";
import { Form } from "react-bootstrap";
import { useDropzone } from "react-dropzone";
import ReactGallery from "reactive-blueimp-gallery";
import { useFormik } from "formik";
import { useHistory } from "react-router-dom";
import axios from "../config/axios";
import Gallery from "react-grid-gallery";
import { useSelector } from "react-redux";

const TaskGallery = (props) => {
  //http://localhost:5000/uploads/images/task/
  const user = useSelector((state) => state.phAuth.user);
  const [images, setImages] = useState([]);

  useEffect(() => {
    axios
      .post("/user/retrieve", { user_id: props.data.task_creator_id })
      .then((response) => {
        console.log(response);
        setAddedBy(response.data.first_name);
        setAddedByType(response.data.user_type);
        setAddedBySlug(response.data.first_name);
      })
      .catch((err) => console.log("err", err));
    axios
      .get(`/task/gallery/${props.data._id}`)
      .then((response) => {
        var temp = [];
        response.data.forEach((ele) =>
          temp.push({
            src: baseUrl + ele,
            thumbnail: baseUrl + ele,
            thumbnailWidth: 120,
            thumbnailHeight: 120,
          })
        );
        console.log("images- temp -----> ", temp);
        setImages(temp);
        console.log("images", response);
      })
      .catch((err) => console.log("err", err));
  }, []);
  var baseUrl = "http://localhost:5000/uploads/images/task/";
  const [deleteImage, setDeleteImage] = useState([]);


  // {
  //   source: "http://localhost:5000/uploads/images/task/task-image-1629726296370.jpeg",
  // },
  const [added_by, setAddedBy] = useState();
  console.log(added_by);
  const [added_by_type, setAddedByType] = useState();
  const [added_by_slug, setAddedBySlug] = useState();
  const history = useHistory();
  // const images = [
  //   {
  //     source: "http://localhost:5000/uploads/images/task/task-image-1629726296370.jpeg",
  //   },
  // {
  //   source: Images.animals,
  //   sizes: {
  //     height: "100px",
  //     width: "100px",
  //   },
  //   alt: "this is alt text ",
  // },
  // {
  //   source: Images.ironman,
  // },
  // ];
  const thumbsContainer = {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  };

  const thumb = {
    display: "inline-flex",
    borderRadius: 2,
    border: "1px solid #eaeaea",
    marginBottom: 8,
    marginRight: 8,
    width: 100,
    height: 100,
    padding: 4,
    boxSizing: "border-box",
  };

  const thumbInner = {
    display: "flex",
    minWidth: 0,
    overflow: "hidden",
  };

  const img = {
    display: "block",
    width: "auto",
    height: "100%",
  };
  const onSelectImage = (index, image) => {
    console.log("index, image", index, image)
    var img = images[index];
    if (img.hasOwnProperty("isSelected"))
      img.isSelected = !img.isSelected;
    else
      img.isSelected = true;
    setDeleteImage(images)
  }
  const onSubmit = (values) => {
    const data = new FormData();
    if (files) {
      console.log("hai files", files.length);
      data.append("image", files[0]);
      data.append("img_name", files[0].name);
      data.set("status", 1);
    }

    if (props) {
      data.set("task_id", props.data._id);
      data.set("added_by", props.data);
    }
    //   const [added_by, setAddedBy] = useState();
    // const [added_by_type, setAddedByType] = useState();
    // const [added_by_slug, setAddedBySlug] = useState();
    data.set("added_by", added_by);
    data.set("added_by_type", added_by_type);
    data.set("added_by_slug", added_by_slug);
    console.log("values", data.get("task_id"));
    axios
      .post("/task/uploadGallery", data)
      .then(({ data }) => {
        console.info(data);
        // history.push("/task-detail");
        window.location.reload();
      })
      .catch((err) => console.log("err", err));
  };
  const [files, setFiles] = useState([]);
  console.log("files", files);
  const { handleSubmit } = useFormik({
    initialValues: { image: files },
    enableReinitialize: true,
    onSubmit,
  });
  // console.log("files", files);
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/jpeg, image/png",
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
    },
  });
  const thumbs = files.map((file) => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} style={img} />
      </div>
    </div>
  ));
  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );
  return (
    <>
      {user._id === props.data.task_creator_id ? (
        <div style={{ marginBottom: "25px" }}>
          <div className="box-ibox-title">
            <h3 style={{ fontSize: "16px", fontWeight: "600" }}>Upload Gallery Images</h3>
          </div>
          <div
            style={{
              padding: "15px",
              border: "1px solid #e7eaec",
            }}
          >
            <Form onSubmit={handleSubmit}>
              <div style={{ border: "1px solid #0000004d", padding: "15px" }}>
                <div
                  {...getRootProps({ className: "dropzone" })}
                  style={{ display: "flex", justifyContent: "center", padding: "30px" }}
                >
                  <input {...getInputProps()} name="gallery" />
                  <p>Drop files here to upload</p>
                </div>
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <button className="gallery-upload-btn" type="submit">
                    Drop Images and Click here to Upload
                  </button>
                </div>
              </div>
              <aside style={thumbsContainer}>{thumbs}</aside>
            </Form>
          </div>
          <div className="help-block">
            <p>
              Every participant in this task can upload images, however only creator can delete
              uploaded images.
            </p>
            <p>Maximum photo upload size is 5 MB</p>
          </div>
        </div>
      ) : null}

      <div>
        <h3 style={{ fontSize: "16px", fontWeight: "600", margin: "0" }}>Gallery</h3>
      </div>
      <div>
        {images.length > 0 ? (
          <>
            {/* <ReactGallery source={images} sizes="100px 100px" withControls /> */}
            <Gallery images={images} onSelectImage={onSelectImage} backdropClosesModal={true}
            />
          </>
        ) : (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100px",
            }}
          >
            <span style={{ color: "#c4c4c4", fontSize: "1.7rem" }}>No Media to show</span>
          </div>
        )}
      </div>
    </>
  );
};
export default TaskGallery;
