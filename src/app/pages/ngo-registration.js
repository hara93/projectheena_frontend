import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import MuiAccordion from "@material-ui/core/Accordion";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
import {
    Card,
    Row,
    Col,
    Form,
    Dropdown,
    DropdownButton,
    FormControl
} from "react-bootstrap";
import InputGroup from "react-bootstrap/InputGroup";
import DatePicker from "react-datepicker";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
import CustomButton from "../components/CustomButton";
import { connect } from 'react-redux';
import CheckboxTable from "../components/checkboxTable";
import FroalaEditorComponent from "react-froala-wysiwyg";
import { loadCaptchaEnginge, LoadCanvasTemplate, validateCaptcha } from 'react-simple-captcha';
import axios from "../config/axios";
import { useHistory } from "react-router";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const Accordion = withStyles({
    root: {
        border: "1px solid rgba(0, 0, 0, .125)",
        boxShadow: "none",
        "&:not(:last-child)": {
            borderBottom: 0,
        },
        "&:before": {
            display: "none",
        },
        "&$expanded": {
            margin: "0",
        },
    },

    expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
    root: {
        backgroundColor: "rgba(0, 0, 0, .03)",
        borderBottom: "1px solid rgba(0, 0, 0, .125)",
        marginBottom: -1,
        minHeight: 37,
        "&$expanded": {
            minHeight: 37,
            color: "#4caf4f",
        },
    },
    content: {
        "&$expanded": {
            margin: "0",
        },
    },
    expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiAccordionDetails);

const EditProfileUser = (props) => {
    const history = useHistory()
    useEffect(() => {
        loadCaptchaEnginge(5);
    }, [])

    const max = 5000;

    const config = {
        charCounterMax: 2000,
    };
    const [expanded, setExpanded] = React.useState("panel1");
    const [startDate, setStartDate] = useState();
    const [ngo_location, setLocation] = useState("");
    const [cause, setCause] = useState([]);
    const [desc, setDesc] = useState("");
    const [overview, setOverview] = useState("");
    const [ngologo, setLogo] = useState(null);
    const [ngocover, setCover] = useState(null);
    const [nameerror, setnameerror] = useState(false)
    const [usernameerror, setusernameerror] = useState(false)
    const [emailerror, setemailerror] = useState(false)
    const [locationerror, setlocationerror] = useState(false)
    const [addresserror, setaddresserror] = useState(false)
    const [contactnumbererror, setcontactnumbererror] = useState(false)
    const [regiderror, setregiderror] = useState(false)
    const [causeerror, setcauseerror] = useState(false)
    const [preferreddayerror, setpreferreddayerror] = useState(false)
    const [preferredsloterror, setpreferredsloterror] = useState(false)
    const [detailserror, setdetailserror] = useState(false)
    const [logoerror, setlogoerror] = useState(false)
    const [covererror, setcovererror] = useState(false)
    const [cpfirstnameerror, setcpfirstnameerror] = useState(false)
    const [cplastnameerror, setcplastnameerror] = useState(false)
    const [cpcontacterror, setcpcontacterror] = useState(false)
    const [cpemailerror, setcpemailerror] = useState(false)
    const [dangerclass, setdangerclass] = useState("")
    const [NgoData, setNgoData] = useState({
        ngo_username: "",
        ngo_name: "",
        ngo_location: "",
        country_id: "",
        volunter_count: "",
        ngo_overview: "",
        ngo_details: "",
        ngo_tag: "",
        ngo_served_causes: "",
        ngo_establishment_date: "",
        country_code: "",
        ngo_contact_number: "",
        preferred_day: "",
        preferred_slot: "",
        ngo_url: "",
        ngo_email_id: "",
        ngo_admin: "",
        ngo_facebook_url: "",
        ngo_twitter_handle: "",
        befriend_heena: "",
        reason_to_help: "",
        ngo_comments: "",
        cp_first_name: "",
        cp_last_name: "",
        cp_mobile: "",
        cp_email: "",
        vendor_code: "",
        company_code: "",
        status: "",
        registration_date: "",
        ngo_nps_id: "",
        ngo_type: "",
        ngo_address: "",
        staff_count: "",
        awards_won: "",
        campaign_id: "",
        password: "",
        sms_pref_mapped: "",
    });

    const handleLogoSelect = (fileData) => {
        setLogo(fileData.currentTarget.files[0]);
    };
    const handleCoverSelect = (fileData) => {
        setCover(fileData.currentTarget.files[0]);
    };

    const handleModelChange = (e, editor) => {
        setDesc(e);
    };
    const handleOverviewChange = (e, editor) => {
        setOverview(e);
    };

    const handleChange = (panel) => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    };

    const handleChangeInputFields = e => {
        const { name, value } = e.target;
        setNgoData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleSubmit = (values) => {
        let user_captcha_value = document.getElementById('user_captcha_input').value;

        setnameerror(NgoData.ngo_name == '' ? true : false)
        setusernameerror(NgoData.ngo_username == '' ? true : false)
        setemailerror(NgoData.ngo_email_id == '' ? true : false)
        setlocationerror(ngo_location == '' ? true : false)
        setaddresserror(NgoData.ngo_address == '' ? true : false)
        setcontactnumbererror(NgoData.ngo_contact_number == '' ? true : false)
        setregiderror(NgoData.ngo_nps_id == '' ? true : false)
        setcauseerror(cause.length && cause.length > 0 ? false : true)
        setpreferreddayerror(NgoData.preferred_day == '' ? true : false)
        setpreferredsloterror(NgoData.preferred_slot == '' ? true : false)
        setdetailserror(desc == '' ? true : false)
        setlogoerror(!ngologo ? true : false)
        setcovererror(!ngocover ? true : false)
        setcpfirstnameerror(NgoData.cp_first_name == '' ? true : false)
        setcplastnameerror(NgoData.cp_last_name == '' ? true : false)
        setcpcontacterror(NgoData.cp_mobile == '' ? true : false)
        setcpemailerror(NgoData.cp_email == '' ? true : false)
        setdangerclass(user_captcha_value == "" && !validateCaptcha(user_captcha_value) ? "text-danger" : "")

        if (!nameerror && !usernameerror && !emailerror && !locationerror && !addresserror && !contactnumbererror && !regiderror && !causeerror && !preferreddayerror && !preferredsloterror && !detailserror && !logoerror && !covererror && !cpfirstnameerror && !cplastnameerror && !cpcontacterror && !cpemailerror && !dangerclass) {
            var data = new FormData()
            const keys = Object.keys(NgoData);
            keys.forEach((key, index) => {
                if (key != 'ngo_location' && key != 'country_code' && key != 'ngo_details' && key != 'ngo_overview' && key != 'ngo_served_causes' && key != 'logo' && key != 'cover' && key != 'ngo_establishment_date') {
                    console.log(`${key}`, `${NgoData[key]}`)
                    data.append(`${key}`, `${NgoData[key]}`);
                }
            });
            data.append("ngo_location", ngo_location)
            data.append("country_code", "+91")
            data.append("ngo_details", desc)
            data.append("ngo_overview", overview)
            data.append("ngo_served_causes", cause.join(","))
            data.append("logo", ngologo)
            data.append("cover", ngocover)
            data.append("ngo_establishment_date", !startDate || startDate == '' ? '' : startDate)

            axios.post("ngo/create", data)
                .then(({ data, message }) => {
                    console.log("data, message", data, message)
                    if (data) {
                        toast.success(message, { onClose: () => history.push("/signup") })
                    } else {
                        toast.error(message, { onClose: () => history.push("/signup") })
                    }
                })
                .catch((err) => {
                    toast.success(err)
                });
        }
    };

    return (
        <div>
            <ToastContainer />
            <Card style={{ padding: "15px" }}>
                <Accordion
                    square
                    expanded={expanded === "panel1"}
                    onChange={handleChange("panel1")}
                    style={{ boxShadow: "none", marginBottom: "10px" }}
                >
                    <AccordionSummary
                        aria-controls="panel1d-content"
                        id="panel1d-header"
                        style={{ minHeight: "37px", margin: "0" }}
                        className="userprofile-accordion-title"
                    >
                        Mandatory details
                    </AccordionSummary>
                    <AccordionDetails>
                        <Form style={{ width: "100%" }}>
                            <Form.Group>
                                <Form.Row style={{ marginTop: "5px" }}>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        NGO Name
                                    </Form.Label>
                                    <Col>
                                        {nameerror ? <p className="text-danger">Please provide your ngo name</p>
                                            : null}
                                        <Form.Control
                                            name="ngo_name"
                                            type="text"
                                            placeholder="Enter NGO name"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Username
                                    </Form.Label>
                                    <Col>
                                        {usernameerror ? <p className="text-danger">Please provide a username</p>
                                            : null}
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text
                                                    style={{
                                                        marginRight: "0",
                                                        background: "transparent",
                                                    }}
                                                    size="lg"
                                                    className="green-focus"
                                                >
                                                    projectheena.in/
                                                </InputGroup.Text>
                                            </InputGroup.Prepend>

                                            <Form.Control
                                                type="text"
                                                name="ngo_username"
                                                value={NgoData.ngo_name.replaceAll(" ", "-").toLowerCase()}
                                                className="green-focus"
                                                onChange={handleChangeInputFields}
                                            />
                                        </InputGroup>
                                        <p>
                                            Your NGO Page would look like "projectheena.in/ngo/<i><small>your-username</small></i>"
                                        </p>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Email Id
                                    </Form.Label>
                                    <Col>
                                        {emailerror ? <p className="text-danger">Please provide a email address</p>
                                            : null}

                                        <Form.Control
                                            name="ngo_email_id"
                                            type="text"
                                            placeholder="Your email"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom  p-r-15">
                                        Your City
                                    </Form.Label>
                                    <Col>
                                        {locationerror ? <p className="text-danger">Please provide your location</p>
                                            : null}
                                        <PlacesAutoComplete
                                            placeholder="Write first few characters of your city"
                                            location={(location) => {
                                                setLocation(location);
                                            }}
                                        />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row style={{ marginTop: "5px" }}>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Address
                                    </Form.Label>
                                    <Col>
                                        {addresserror ? <p className="text-danger">Please provide your address</p>
                                            : null}
                                        <Form.Control
                                            name="ngo_address"
                                            rows={5}
                                            type="text"
                                            as="textarea"
                                            placeholder="Address of your NGO"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Contact Number
                                    </Form.Label>
                                    <Col>
                                        {contactnumbererror ? <p className="text-danger">Please provide your contact number</p>
                                            : null}
                                        <InputGroup>
                                            <DropdownButton
                                                as={InputGroup.Prepend}
                                                variant="outline-secondary"
                                                title="+91"
                                                name="country_code"
                                                id="input-group-dropdown-1"
                                            >
                                                <Dropdown.Item >+91</Dropdown.Item>
                                            </DropdownButton>
                                            <Form.Control
                                                type="text"
                                                size="lg"
                                                name="ngo_contact_number"
                                                aria-describedby="basic-addon1"
                                                onChange={handleChangeInputFields}
                                            />
                                        </InputGroup>
                                        <spam className="help-block">We do not spam</spam>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row style={{ marginTop: "5px" }}>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Your NGO Reg ID
                                    </Form.Label>
                                    <Col>
                                        {regiderror ? <p className="text-danger">Please provide your registration ID</p>
                                            : null}
                                        <Form.Control
                                            name="ngo_nps_id"
                                            type="text"
                                            placeholder="Registration ID"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom  p-r-15">
                                        Cause Supported
                                    </Form.Label>
                                    <Col style={{ marginTop: "-10px" }}>
                                        {causeerror ? <p className="text-danger">Please provide cause supported</p>
                                            : null}
                                        <CheckboxTable
                                            cause={(causes) => {
                                                setCause(causes);
                                                console.log(causes, "table");
                                            }}
                                            alreadyChecked={cause}
                                        />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row style={{ marginTop: "5px" }}>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Preferred Timing for Voluntary Help
                                    </Form.Label>
                                    <Col lg={5}>
                                        {preferreddayerror ? <p className="text-danger">Please provide preferred day</p>
                                            : null}
                                        <Form.Control
                                            as="select"
                                            name="preferred_day"
                                            onChange={handleChangeInputFields}
                                            custom
                                        //onClick={(e) => setFreq(e.target.value)}
                                        >
                                            <option>Select Day</option>

                                            <option value='0'>Sunday</option>
                                            <option value='1'>Monday</option>
                                            <option value='2'>Tuesday</option>
                                            <option value='3'>Wednesday</option>
                                            <option value='4'>Thursday</option>
                                            <option value='5'>Friday</option>
                                            <option value='6'>Saturday</option>
                                            <option value='7'>Weekdays</option>
                                            <option value='8'>Weekends</option>
                                            <option value='9'>Unavailable</option>
                                        </Form.Control>
                                    </Col>
                                    <Col lg={5}>
                                        {preferredsloterror ? <p className="text-danger">Please provide preferred slot</p>
                                            : null}
                                        {" "}
                                        <Form.Control
                                            as="select"
                                            name="preferred_slot"
                                            onChange={handleChangeInputFields}
                                            custom
                                        //onClick={(e) => setFreq(e.target.value)}
                                        >
                                            <option>Select Time</option>
                                            <option value="0">Morning</option>
                                            <option value="1">Afternoon</option>
                                            <option value="2">Evening </option>
                                            <option value="3">Night</option>
                                            <option value="4">Flexible</option>
                                            <option value="9">Unavailable</option>
                                        </Form.Control>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                                        Details
                                    </Form.Label>
                                    <Col>
                                        {detailserror ? <p className="text-danger">Please tell us about your ngo</p>
                                            : null}
                                        {/* <TextEditor /> */}
                                        <div style={{ maxWidth: "100%" }}>
                                            <FroalaEditorComponent
                                                tag="textarea"
                                                config={config}
                                                onModelChange={handleModelChange}
                                            />
                                        </div>
                                        <span className="help-block">
                                            Min : 256 | Max : 2000 | Remaining : {max - desc.length}
                                        </span>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />

                                <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                                        Logo (Max 1 MB)
                                    </Form.Label>
                                    <Col>
                                        {logoerror ? <p className="text-danger">Please provide your logo</p>
                                            : null}
                                        <input name="logo" type="file" onChange={handleLogoSelect} />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                                        Cover Image
                                    </Form.Label>
                                    <Col>
                                        {covererror ? <p className="text-danger">Please provide a cover image</p>
                                            : null}
                                        <input name="cover" type="file" onChange={handleCoverSelect} />
                                        <p>Please upload an image of your successful activity as this will be showcased to all the stakeholders</p>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <h4>Contact Person</h4>
                                <Form.Row>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        First Name
                                    </Form.Label>
                                    <Col>
                                        {cpfirstnameerror ? <p className="text-danger">First name is required</p>
                                            : null}
                                        <Form.Control
                                            name="cp_first_name"
                                            type="text"
                                            placeholder="First name"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Last Name
                                    </Form.Label>
                                    <Col>
                                        {cplastnameerror ? <p className="text-danger">Last name is required</p>
                                            : null}
                                        <Form.Control
                                            name="cp_last_name"
                                            type="text"
                                            placeholder="Last name"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                </Form.Row>
                                <div className="w-100 p-3"></div>
                                <Form.Row>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Mobile
                                    </Form.Label>
                                    <Col>
                                        {cpcontacterror ? <p className="text-danger">Contact number is required</p>
                                            : null}
                                        <Form.Control
                                            name="cp_mobile"
                                            type="text"
                                            placeholder="Contact number"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Email
                                    </Form.Label>
                                    <Col>
                                        {cpemailerror ? <p className="text-danger">Email address is required</p>
                                            : null}
                                        <Form.Control
                                            name="cp_email"
                                            type="email"
                                            placeholder="Email Address"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                </Form.Row>
                            </Form.Group>
                        </Form>
                    </AccordionDetails>
                </Accordion>
                <Accordion
                    square
                    expanded={expanded === "panel2"}
                    onChange={handleChange("panel2")}
                    style={{ boxShadow: "none", marginBottom: "10px" }}
                >
                    <AccordionSummary
                        aria-controls="panel2d-content"
                        id="panel2d-header"
                        className="userprofile-accordion-title"
                    >
                        Optional Details
                    </AccordionSummary>
                    <AccordionDetails>
                        <Form style={{ width: "100%" }}>
                            <Form.Group>
                                <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                                        Establishment Date
                                    </Form.Label>
                                    <Col className="form-input-align-center">
                                        {" "}
                                        <DatePicker
                                            name="ngo_establishment_day"
                                            placeholderText="Your NGO Establishment day"
                                            selected={startDate}
                                            dateFormat="dd-MM-yyyy"
                                            selectsStart
                                            startDate={startDate}
                                            onChange={(date) => setStartDate(date.getTime())}
                                            popperPlacement="bottom-start"
                                            style={{ color: "yellow" }}
                                            required
                                            className="green-focus"
                                        />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                                        No of employees
                                    </Form.Label>
                                    <Col>
                                        <InputGroup>
                                            <FormControl
                                                type="number"
                                                name="staff_count"
                                                placeholder="Number of Employees"
                                                onChange={handleChangeInputFields}
                                                // value={values.project_duration}
                                                className="green-focus"
                                            />
                                            {/* <InputGroup.Append>
                                                <InputGroup.Text
                                                    style={{ background: "transparent", border: "1px solid #e5e6e7" }}
                                                >
                                                    Days
                                                </InputGroup.Text>
                                            </InputGroup.Append> */}
                                        </InputGroup>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                                        Existing Volunteer Count
                                    </Form.Label>
                                    <Col>
                                        <InputGroup>
                                            <FormControl
                                                type="number"
                                                name="volunteer_count"
                                                placeholder="Approximate volunteers your NGO have"
                                                onChange={handleChangeInputFields}
                                                className="green-focus"
                                            />
                                            {/* <InputGroup.Append>
                                                <InputGroup.Text
                                                    style={{ background: "transparent", border: "1px solid #e5e6e7" }}
                                                >
                                                    Days
                                                </InputGroup.Text>
                                            </InputGroup.Append> */}
                                        </InputGroup>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row style={{ marginTop: "5px" }}>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Tag Line
                                    </Form.Label>
                                    <Col>
                                        <Form.Control
                                            name="ngo_tag"
                                            type="text"
                                            placeholder="Tag Line of your NGO"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                                        Why Volunteer for Us
                                    </Form.Label>
                                    <Col>
                                        {/* <TextEditor /> */}
                                        <div style={{ maxWidth: "100%" }}>
                                            <FroalaEditorComponent
                                                tag="textarea"
                                                config={config}
                                                onModelChange={handleOverviewChange}
                                            />
                                        </div>
                                        <span className="help-block">
                                            Min : 256 | Max : 2000 | Remaining : {max - desc.length}
                                        </span>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row style={{ marginTop: "5px" }}>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Your website
                                    </Form.Label>
                                    <Col>
                                        <Form.Control
                                            name="ngo_url"
                                            type="text"
                                            placeholder="Your website"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row style={{ marginTop: "5px" }}>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Your Facebook URL
                                    </Form.Label>
                                    <Col>
                                        <Form.Control
                                            name="ngo_facebook_url"
                                            type="text"
                                            placeholder="Your website"
                                            className="green-focus"
                                            onChange={handleChangeInputFields}
                                        />
                                        <p>Please ensure that it is a fan page & not profile page</p>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom p-r-15"
                                    >
                                        Twitter handle
                                    </Form.Label>
                                    <Col>
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text
                                                    style={{
                                                        marginRight: "0",
                                                        background: "transparent",
                                                    }}
                                                    className="green-focus"
                                                >
                                                    @
                                                </InputGroup.Text>
                                            </InputGroup.Prepend>

                                            <Form.Control
                                                type="text"
                                                name="ngo_twitter_handle"
                                                className="green-focus"
                                                onChange={handleChangeInputFields}
                                            />
                                        </InputGroup>
                                    </Col>
                                </Form.Row>
                                <hr className="form-horizontal-line-20m" />
                                <Form.Row>
                                    <Form.Label
                                        column="lg"
                                        lg={2}
                                        className="form-label-custom  p-r-15"
                                    >
                                        Awards Won (Comma Separated)
                                    </Form.Label>
                                    <Col>
                                        <Form.Control
                                            type="text"
                                            as="textarea"
                                            name="awards_won"
                                            onChange={handleChangeInputFields}
                                            className="green-focus"
                                            style={{ borderRadius: "0" }}
                                        />
                                        {/* <span className="help-block">
                                            Min: 25 chars | Max: 1000 chars | Remaining Chars:{" "}
                                            {maxCount - count}
                                        </span> */}
                                    </Col>
                                </Form.Row>
                            </Form.Group>
                        </Form>
                    </AccordionDetails>
                </Accordion>
                <Card>
                    <Form.Row>
                        <Form.Label
                            column="lg"
                            lg={2}
                            className="form-label-custom  p-r-15"
                        >
                            <p className={dangerclass}>
                                Click on the Box Here
                                <br />
                                <strong>(this is to reduce spam)</strong>
                            </p>
                        </Form.Label>
                        <Col lg="3">
                            <div className="container">
                                <div className="form-group">
                                    <div className="col">
                                        <div className="p-3" style={{ border: "1px solid #BBEFBE", backgroundColor: "#BBEFBE" }}>
                                            <LoadCanvasTemplate />
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div>
                                            <Form.Control
                                                id="user_captcha_input"
                                                name="user_captcha_input"
                                                // size="lg"
                                                type="text"
                                                placeholder="Enter Captcha Value"
                                                className="green-focus"
                                            />
                                        </div>
                                    </div>
                                    {/* <div className="col mt-3">
                                        <div><button class="btn btn-primary" onClick={doSubmit}>Submit</button></div>
                                    </div> */}
                                </div>
                            </div>
                        </Col>
                    </Form.Row>
                </Card>
                <div
                    style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <CustomButton content="Register" type="submit" onClick={handleSubmit} />
                </div>
            </Card>
        </div>
    );
};

const mapStateToProps = function (state) {
    return {
        user: state.phAuth.user,
    };
};

export default connect(mapStateToProps)(EditProfileUser);
