import React from "react";
import { Card, Row, Col, Form } from "react-bootstrap";
import ReCAPTCHA from "react-google-recaptcha";
import CustomButton from "../components/CustomButton";

const ContactUs = () => {
  function onChange(value) {
    console.log("captcha value", value);
  }
  return (
    <>
      <div className="banner">
        <div className="banner-img">
          <div id="parallex-bg" style={{ backgroundImage: "url('http://projectheena.in/assets/img/static/superhero-kid.jpg') " }}></div>
        </div>
      </div>
      <div className="container">
        <Card style={{ padding: "0 15px" }}>
          <div style={{ marginTop: "30px" }}>
            <h3 className="static-title">Contact Us</h3>
          </div>
          <div style={{ padding: "20px 0" }}>
            <p>
              We believe Social media channels just rock! However, we are cool
              which ever way you would like to connect. Just a small heads up in
              case if you feel there is an unwanted error bugging you or feature
              you want to request; use the on page Feedback buttons to intimate
              us. Else you can use the following channels.
            </p>
          </div>
          <div style={{ padding: "20px 0" }}>
            <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
              Fill in the form to contact us
            </h3>
            <Form>
              <Form.Group>
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    First Name
                  </Form.Label>
                  <Col className="form-input-align-center">
                    <Form.Control size="lg" type="text" className="green-focus" />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Last Name
                  </Form.Label>
                  <Col className="form-input-align-center">
                    <Form.Control size="lg" type="text" className="green-focus" />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Last Name
                  </Form.Label>
                  <Col className="form-input-align-center">
                    <Form.Control
                      size="lg"
                      type="email"
                      className="green-focus"
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Query type
                  </Form.Label>
                  <Col>
                    <Form.Control as="select" size="lg" custom>
                      <option>General Inquery</option>
                      <option>Marketing</option>
                      <option>Advertising</option>
                      <option>Sponsoring</option>
                    </Form.Control>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Message
                  </Form.Label>
                  <Col className="form-input-align-center">
                    <Form.Control
                      as="textarea"
                      rows="8"
                      maxLength="1000"
                      className="green-focus"
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                  >
                    Click on the box here (this is to reduce spam)
                  </Form.Label>
                  <Col xs={12} lg={10} style={{ padding: "0 " }}>
                    <ReCAPTCHA
                      sitekey="6LcjScAaAAAAAJFEVrvGCWI-OTc9SeNuHWY5JFT3"
                      onChange={onChange}
                    />
                  </Col>
                </Form.Row>
                <hr className="hr-mailsignup" />
                <Form.Row>
                  <Form.Label column="lg" lg={2}></Form.Label>
                  <Col>
                    <CustomButton content="Contact Us" />
                  </Col>
                </Form.Row>
              </Form.Group>
            </Form>
          </div>
          <hr style={{ borderTop: "1px solid #eee" }} />
          <Row>
            <Col lg={6}>
              <h3 style={{ fontSize: "16px", fontWeight: "600" }}>Address</h3>

              <address>
                Coense Solutions Private Limited,
                <br />
                Dattani Centre, Akruli Road,
                <br />
                Near Kandivali Station,Above Jain Sweets,
                <br />
                Kandivali (E),
                <br />
                Mumbai, Maharashtra, India,
                <br />
                400101.
              </address>
            </Col>
            <Col lg={6}>
              <div
                style={{
                  border: "1px solid",
                  height: "150px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <p>api address</p>
              </div>
              <span className="help-block">
                View{" "}
                <a
                  style={{ color: "green" }}
                  href="https://www.google.co.in/maps/place/Coense+Solutions+Pvt.+Ltd.+-+ProjectHeena+%23JCombinator3/@19.0982953,72.8818068,17z/data=!3m1!4b1!4m5!3m4!1s0x3be7c86f75fad729:0x38290ab228b61742!8m2!3d19.0982902!4d72.8839955"
                >
                  Coense Solutions Pvt Ltd.
                </a>{" "}
                in a larger map
              </span>
            </Col>
          </Row>
        </Card>
      </div>

    </>
  );
};
export default ContactUs;
