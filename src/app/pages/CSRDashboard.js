import React from "react";
import "../../index.scss";
import SingleTableLayout from "../components/SingleTable";
import { Card } from "react-bootstrap";
import { FaTv } from "react-icons/fa";
import { connect } from 'react-redux';
var arr_status = ["Open", "On Going", "Awaiting Closure", "Closed", "Deleted", "Auto Closed", "", "", "", "", "", "", "Force Closed"]
class CSRDashboard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      status: !props.projectdata.project_status ? arr_status[0] : arr_status[props.projectdata.project_status]
    }
  }
  render() {
    const projectdetails = [
      {
        heading: "Project Related Data",
        m11: "Project Status",
        m12: this.state.status,
        m21: "Project Health",
        m22: "Very Poor",
        m31: "Beneficiary Count",
        m32: "12",
      },
      {
        heading: "Entities Involved",
        m11: "NGO Count",
        m12: "2",
        m21: "User Count",
        m22: "0",
        m31: "Team Count",
        m32: "0",
      },
      {
        heading: "Finance Related Data",
        m11: "Funds Deployed",
        m12: "₹ 42,56,284",
        m21: "Funds Executed",
        m22: "₹ 18,35,000",
        m31: "Funds Pending",
        m32: "₹ 24,21,284",
      },


      {
        heading: "Milestones Related Data",
        m11: "Total Milestones",
        m12: "13",
        m21: "Milestones Closed Count",
        m22: "0",
        m31: "Milestones Delayed Count",
        m32: "13",
      },

      {
        heading: "Collaborators Related Data",
        m11: "Collaborator Count",
        m12: "0",
        m21: "Admin Count",
        m22: "2",
      },
      {
        heading: "Disussion Related Data",
        m11: "Comment Count",
        m12: "0",
        m21: "Uploaded File Count",
        m22: "0",
      },
    ];
    return (
      <Card className="ibox p-3">
        <div className="item">
          <div className="clearfix">
            <div className="pull-left">
              <h3 className="csr_heading"><i class="fas fa-tv"></i> Project Dashboard</h3>
            </div>
            <div className="pull-right">
              <small>Created on 01<sup>st</sup> Sep,2020</small>
            </div>
          </div>
        </div>
        <div>
          <SingleTableLayout projectdetails={projectdetails} />
        </div>
        <div className="item">
          <h3 className="csr_heading"><i class="far fa-chart-bar"></i> Reports</h3>
        </div>
        <div className="no-content">
          <h3 className="no-content-msg"> No Reports to show</h3>
        </div>
      </Card>
    );
  }
}
const mapStateToProps = function (state) {
  return { user: state.phAuth.user, projectdata: state.project.projectdata }
};
export default connect(mapStateToProps)(CSRDashboard);
