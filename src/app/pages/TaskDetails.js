/* eslint-disable no-mixed-operators */
/* eslint-disable eqeqeq */
/* eslint-disable no-restricted-imports */
import React from "react";
import { Col, Row, Form, Modal, Card } from "react-bootstrap";
// import { Card } from "@material-ui/core";
// import Tooltip from "@material-ui/core/Tooltip";

import { Images } from "../config/Images";
// import { supportsInlineSVG } from "react-inlinesvg/lib/helpers";
import CustomButtonOutline from "../components/CustomButtonOutline";
import ManageTask from "./ManageTask";
import UpdateTask from "./UpdateTask";
import TaskGallery from "./TaskGallery";
import TaskVolunteer from "./TaskVolunteer";
import AttendanceDetail from "./AttendanceDetail";
import * as ReactBootstrap from "react-bootstrap";
// import CustomButton from "../components/CustomButton";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import { connect } from "react-redux";
import axios from "../config/axios";
import TurndownService from "turndown";
import ReactMarkdown from "react-markdown";
import moment from "moment";
import ReactTooltip from "react-tooltip";
import { Link } from "react-router-dom"
//http://localhost:5000/uploads/images/task/task-image-1629186822035.jpeg
class TaskDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      details: true,
      manageTask: false,
      update: false,
      image: false,
      volunteer: false,
      attendance: false,
      isOpen: false,
      plus: false,
      users: false,
      show: false,
      temp_id: this.props.location.pathname.split("/"),
      tags: [],
      task_data: {},
      desc: "",
      task_creator: {},
      noOfUpdates: 0,
      focusCSS: { color: "#525252" },
      volunteersList: [],
      task_status: 1,
      tranxStatus: {},
      task_skills: [],
    };
    // this.details = React.createRef();

    this.htmlToMarkdown = this.htmlToMarkdown.bind(this);
  }
  getTaskVolunteersList(task_id) {
    axios
      .get(`/taskTransaction/taskVolunteersList/${task_id}`)
      .then((res) => {
        this.setState({ volunteersList: res.data });
        console.log("taskVolunteersList", res);
      })
      .catch((err) => console.log("err", err));
  }
  componentDidMount() {
    // this.details.current.focus();
    var task_id = this.state.temp_id[this.state.temp_id.length - 1];

    axios
      .get(`/task/details/${task_id}`)
      .then((response) => {
        console.log("data", response);
        console.log("this", this.props.user._id);
        var s = [];
        response.data.task_skill_mapping.forEach((ele) => s.push(ele.skill_id));
        var markdown = this.htmlToMarkdown(response.data.task_description);
        this.setState({
          task_data: response.data,
          task_status: response.data.task_status,
          task_skills: s,
        });
        // ***
        var temp = []
        response.data && response.data.task_update && response.data.task_update.length > 0 && response.data.task_update
          .filter((filt) => filt.update_ref_id === undefined)
          .map((ele) => temp.push(ele))
        this.setState({ noOfUpdates: temp.length })
        // ***
        localStorage.setItem("task_data_id", response.data._id);
        this.setState({ desc: markdown });
        axios
          .post(`/tag/getTaskTags`, { task_id: response.data.task_id })
          .then((response) => {
            this.setState({ tags: response.data });
          })
          .catch((err) => console.log("err", err));
        axios
          .post("/user/retrieve", { user_id: response.data.task_creator_id })
          .then((response) => {
            this.setState({ task_creator: response.data });
          })
          .catch((err) => console.log("err", err));
      })
      .catch((err) => console.log("err", err));
    this.getTaskVolunteersList(task_id)
    axios
      .post(`/taskTransaction/checkTransactionStatus`, {
        taskId: task_id,
        userId: this.props.user._id,
      })
      .then((res) => {
        this.setState({ tranxStatus: res });
        console.log("status check", res);
      })
      .catch((err) => console.log("err", err));
  }
  componentDidUpdate(prevProps) {
    if (this.state.volunteersList.length !== prevProps.userID) {
    }
    console.log("desc", this.state.volunteersList);
    // console.log("data", this.state.task_data);
    // console.log("tags", this.state.tags);
    // console.log("userdata", this.state.task_creator);
  }
  htmlToMarkdown = (data) => {
    var turndown = new TurndownService();
    var desc = turndown.turndown(data);

    return desc;
  };
  handleDetails = () => {
    this.setState({
      details: true,
      manageTask: false,
      update: false,
      image: false,
      volunteer: false,
      attendance: false,
    });
  };

  handleUpdate = () => {
    this.setState({
      manageTask: false,
      details: false,
      update: true,
      image: false,
      volunteer: false,
      attendance: false,
    });
  };
  handleManage = () => {
    this.setState({
      manageTask: true,
      details: false,
      update: false,
      image: false,
      volunteer: false,
      attendance: false,
    });
  };
  handleGallery = () => {
    this.setState({
      manageTask: false,
      details: false,
      update: false,
      image: true,
      volunteer: false,
      attendance: false,
    });
  };
  handleVolunteer = () => {
    this.setState({
      manageTask: false,
      details: false,
      update: false,
      image: false,
      volunteer: true,
      attendance: false,
    });
  };
  handleAttendance = () => {
    this.setState({
      manageTask: false,
      details: false,
      update: false,
      image: false,
      volunteer: false,
      attendance: true,
    });
  };

  handlePlus = () => {
    this.setState({ plus: true });
  };

  handleMinus = () => {
    this.setState({ plus: false });
  };
  handleClick = () => {
    this.setState({ users: !this.state.users });
  };

  openModal = () => this.setState({ isOpen: true });
  closeModal = () => this.setState({ isOpen: false });

  handleClose = () => this.setState({ show: false });
  handleShow = () => this.setState({ show: true });

  render() {
    return (
      <div className="container" style={{ padding: "0" }}>
        <div style={{ margin: "25px 0", textAlign: "center" }}>
          <h1> {this.state.task_data.task_name}</h1>

          {/* <h2>By <p style={{ color: '#4caf4f', display: 'inline-block', fontSize: '23px', fontWeight: '600' }}>Social Work</p></h2> */}
          <div style={{ marginTop: 8.5, marginBottom: 25, display: "inline-block" }}>
            <h4 className="font-bold">
              By <Link className="basecolor">{this.props.user.first_name}</Link>
            </h4>
          </div>
        </div>

        <ul className="ph-nav-bar tabs" style={{ marginTop: "-2%" }}>
          <li>
            <a href="#terms" className="active" onClick={this.handleDetails} ref={this.details}>
              <div>
                <i
                  className="fa fa-newspaper-o active-focus  p-r-5"
                  style={this.state.details ? this.state.focusCSS : {}}
                ></i>
                <span
                  className="active-focus"
                  style={this.state.details ? this.state.focusCSS : {}}
                >
                  DETAILS
                </span>
              </div>
            </a>
          </li>
          <li>
            <a href="#terms" className="active" onClick={this.handleManage}>
              <div>
                <i
                  className="fa fa-briefcase active-focus p-r-5"
                  style={this.state.manageTask ? this.state.focusCSS : {}}
                ></i>
                <span
                  className="active-focus"
                  style={this.state.manageTask ? this.state.focusCSS : {}}
                >
                  MANAGE TASK
                </span>
              </div>
            </a>
          </li>
          <li>
            <a href="#terms" className="active" onClick={this.handleUpdate}>
              <div>
                <i
                  className="fa fa-bullhorn active-focus p-r-5"
                  style={this.state.update ? this.state.focusCSS : {}}
                ></i>
                <span className="active-focus" style={this.state.update ? this.state.focusCSS : {}}>
                  UPDATES & UPLOADS{" "}
                  {this.state.noOfUpdates > 0 ? `(${this.state.noOfUpdates})` : null}
                </span>
              </div>
            </a>
          </li>
          <li>
            <a href="#terms" className="active" onClick={this.handleGallery}>
              <div>
                <i
                  className="fa fa-image active-focus p-r-5"
                  style={this.state.image ? this.state.focusCSS : {}}
                ></i>
                <span className="active-focus" style={this.state.image ? this.state.focusCSS : {}}>
                  GALLERY ({this.state.task_data && this.state.task_data.task_gallery && this.state.task_data.task_gallery.length > 0 && this.state.task_data.task_gallery.length})
                </span>
              </div>
            </a>
          </li>
          <li>
            <a href="#terms" className="active" onClick={this.handleVolunteer}>
              <div>
                <i
                  className="fa fa-group active-focus p-r-5"
                  style={this.state.volunteer ? this.state.focusCSS : {}}
                ></i>{" "}
                <span
                  className="active-focus"
                  style={this.state.volunteer ? this.state.focusCSS : {}}
                >
                  VOLUNTEERS ({this.state.volunteersList.length})
                </span>
              </div>
            </a>
          </li>
          <li>
            <a href="#terms" className="active" onClick={this.handleAttendance}>
              <div>
                <i
                  className="fa fa-calendar active-focus p-r-5"
                  style={this.state.attendance ? this.state.focusCSS : {}}
                ></i>
                <span
                  className="active-focus"
                  style={this.state.attendance ? this.state.focusCSS : {}}
                >
                  ATTENDANCE
                </span>
              </div>
            </a>
          </li>
        </ul>

        <Card style={{ padding: "15px 0" }}>
          <Row className="m-0">
            <Col md={8}>
              {this.state.details == true ? (
                <>
                  <div style={{ marginBottom: "30px" }}>
                    <img
                      className="p-1"
                      // src={`http://localhost:5000/uploads/images/task/${this.state.task_data.image}`}
                      src="http://projectheena.in/uploads/projects/24163180116341/images/default.jpg"
                      alt="task-profile"
                      style={{ width: "100%" }}
                    ></img>
                  </div>

                  {this.state.desc ? (
                    <div style={{ color: "gray", padding: "0 5px", marginBottom: "25px" }}>
                      <ReactMarkdown children={this.state.desc} />
                    </div>
                  ) : null}

                  <div style={{ padding: "0 5px" }}>
                    <div>
                      <h3 style={{ fontSize: "22px", fontWeight: "600" }}>
                        Share with your Friends
                      </h3>
                    </div>

                    <FacebookIcon style={{ fontSize: 35, marginRight: "2vw", color: "#3c5997" }} />
                    <TwitterIcon style={{ fontSize: 35, marginRight: "2vw", color: "#04abed" }} />
                    <LinkedInIcon style={{ fontSize: 35, marginRight: "2vw", color: "#027bb6" }} />
                    <GroupAddIcon style={{ fontSize: 35, marginRight: "2vw", color: "#4caf4f" }} />
                  </div>

                  <ReactBootstrap.Modal
                    show={this.state.isOpen}
                    onHide={this.closeModal}
                    style={{ opacity: "1" }}
                    className="p-5"
                  >
                    <ReactBootstrap.Modal.Header closeButton>
                      <ReactBootstrap.Modal.Title>
                        Enter Recommended User's Email
                      </ReactBootstrap.Modal.Title>
                    </ReactBootstrap.Modal.Header>
                    <ReactBootstrap.Modal.Body className="p-5">
                      <Form>
                        <Form.Group as={Row} controlId="formPlaintextEmail">
                          <Form.Label column sm="2">
                            User Email
                          </Form.Label>
                          <Col sm="7">
                            <Form.Control type="password" placeholder="Email" />
                          </Col>
                          <Col sm="3">
                            <button
                              style={{ border: "1px solid #e7eaec", backgroundColor: "white" }}
                              onClick={this.handlePlus}
                            >
                              <i className="fa fa-plus"></i>
                            </button>
                            <button
                              style={{ border: "1px solid #e7eaec", backgroundColor: "white" }}
                              onClick={this.handleMinus}
                            >
                              <i className="fa fa-minus"></i>
                            </button>
                          </Col>
                        </Form.Group>
                      </Form>

                      {this.state.plus == true ? (
                        <Form>
                          <Form.Group as={Row} controlId="formPlaintextEmail">
                            <Form.Label column sm="2"></Form.Label>
                            <Col sm="7">
                              <Form.Control type="password" placeholder="Email" />
                            </Col>
                            <Col sm="3">
                              <button
                                style={{ border: "1px solid #e7eaec", backgroundColor: "white" }}
                                onClick={this.handlePlus}
                              >
                                <i className="fa fa-plus"></i>
                              </button>
                              <button
                                style={{ border: "1px solid #e7eaec", backgroundColor: "white" }}
                                onClick={this.handleMinus}
                              >
                                <i className="fa fa-minus"></i>
                              </button>
                            </Col>
                          </Form.Group>
                        </Form>
                      ) : null}
                    </ReactBootstrap.Modal.Body>
                    <ReactBootstrap.Modal.Footer>
                      {/* <ReactBootstrap.Button variant="secondary">Close</ReactBootstrap.Button> */}
                      <button
                        className="p-2"
                        onClick={this.closeModal}
                        style={{
                          backgroundColor: "#c2c2c2",
                          border: "1px solid #c2c2c2",
                          color: "white",
                        }}
                      >
                        Close
                      </button>

                      {/* <ReactBootstrap.Button variant="primary">Add meta data</ReactBootstrap.Button> */}
                      <button
                        className="p-2"
                        onClick={this.closeModal}
                        style={{
                          backgroundColor: "#4caf4f",
                          border: "1px solid #4caf4f",
                          color: "white",
                        }}
                      >
                        Submit
                      </button>
                    </ReactBootstrap.Modal.Footer>
                  </ReactBootstrap.Modal>
                </>
              ) : null}

              {this.state.manageTask == true ? (
                <>
                  <ManageTask data={this.state.task_data} status={this.state.task_status} getVolunteerList={(task_id) => {
                    this.getTaskVolunteersList(task_id);
                  }} />
                </>
              ) : null}

              {this.state.update == true ? (
                <>
                  <UpdateTask
                    data={this.state.task_data}
                    updates={(no) => {
                      this.setState({ noOfUpdates: no.length });
                    }}
                  />
                </>
              ) : null}
              {this.state.image == true ? (
                <>
                  <TaskGallery data={this.state.task_data} />
                </>
              ) : null}
              {this.state.volunteer == true ? (
                <>
                  <TaskVolunteer />
                </>
              ) : null}
              {this.state.attendance == true ? (
                <>
                  <AttendanceDetail status={this.state.tranxStatus} />
                </>
              ) : null}
            </Col>

            <Col md={4}>
              {this.state.task_status === 4 || this.state.task_status === 5 ? null : (
                <a href="/searchTab">
                  <button
                    className="p-2  search-volunteer-btn"
                    onClick={() => this.history.push("/searchTab")}
                  >
                    {" "}
                    Search Volunteers
                  </button>
                </a>
              )}

              <div style={{ margin: "25px 0" }}>
                <div>
                  <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
                    Task Statistics
                    {this.state.task_status === 4 || this.state.task_status === 5 ? null : (
                      <>
                        {" "}
                        <ReactTooltip
                          className="tooltip-for-edit"
                          id="edit"
                          effect="solid"
                          place="right"
                        >
                          Edit {`"${this.state.task_data.task_name}"`}
                        </ReactTooltip>
                        <a href={`/editTask/${this.state.task_data._id}`}>
                          <i
                            class="fas fa-pencil-alt"
                            data-tip
                            data-for="edit"
                            style={{ paddingLeft: "5px", color: "#666666" }}
                          ></i>
                        </a>
                      </>
                    )}
                  </h3>
                </div>
                <div>
                  <ul className="donation-stats">
                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">Status</div>
                      <div className="tc-task">
                        {this.state.task_status === 4 || this.state.task_status === 5 ? (
                          this.state.task_status === 4 ? (
                            <span className="label-task">Task is closed</span>
                          ) : (
                            <span className="label-task" style={{ backgroundColor: "gray" }}>
                              Deleted
                            </span>
                          )
                        ) : (
                          <span className="label-task">Task is open</span>
                        )}
                      </div>
                    </li>
                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">Duration (Hours)</div>
                      <div className="tc-task">
                        {" "}
                        Min:{this.state.task_data.task_duration_hours_min} &nbsp;|&nbsp; Max:
                        {this.state.task_data.task_duration_hours}
                      </div>
                    </li>
                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">Location</div>
                      <div className="tc-task"> {this.state.task_data.task_location}</div>
                    </li>
                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">People required</div>
                      <div className="tc-task">
                        {" "}
                        Min:{this.state.task_data.people_required_min} &nbsp;|&nbsp; Max:
                        {this.state.task_data.people_required}
                      </div>
                    </li>
                    {this.state.task_data.is_time_bound === 0 ? (
                      <li
                        style={{
                          display: "table-row",
                          listStyleType: "none",
                        }}
                      >
                        <div className="th-task">
                          Task timeline <br />
                          <br />
                          Flexible
                        </div>
                        <div className="tc-task">
                          This is continuous task, without any start or end date. You can join right
                          away and start doing good.
                        </div>
                      </li>
                    ) : (
                      <>
                        <li
                          style={{
                            display: "table-row",
                            listStyleType: "none",
                          }}
                        >
                          <div className="th-task">Application Deadline</div>
                          <div className="tc-task">
                            {moment(this.state.task_data.task_last_application).format(
                              "Do MMM YYYY"
                            )}
                          </div>
                        </li>
                        <li
                          style={{
                            display: "table-row",
                            listStyleType: "none",
                          }}
                        >
                          <div className="th-task">Starting From</div>
                          <div className="tc-task">
                            {moment(this.state.task_data.task_start_time).format("Do MMM YYYY LT")}
                          </div>
                        </li>
                        <li
                          style={{
                            display: "table-row",
                            listStyleType: "none",
                          }}
                        >
                          <div className="th-task">Ending On</div>
                          <div className="tc-task">
                            {moment(this.state.task_data.task_end_time).format("Do MMM YYYY LT")}
                          </div>
                        </li>
                      </>
                    )}

                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">External Links</div>
                      <div className="tc-task">
                        <a className="ph-link" href={`${this.state.task_data.vision_page}`}>
                          {this.state.task_data.vision_page}
                        </a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div style={{ margin: "25px 0" }}>
                <div style={{ marginBottom: "10px" }}>
                  <h3 style={{ fontSize: "16px", fontWeight: "600", margin: "0" }}>
                    Skills Needed
                  </h3>
                </div>

                <div style={{ display: "contents" }}>
                  {this.props.skillsList && this.props.skillsList.skills && this.props.skillsList.skills
                    .filter((fil) => this.state.task_skills.includes(fil._id))
                    .map((ele) => (
                      <a className="p-2 task-tags-skill" href="skill#">
                        {ele.skill_name}
                      </a>
                    )) || <span>no skills</span>}
                </div>
              </div>
              <div style={{ margin: "25px 0" }}>
                <div style={{ marginBottom: "10px" }}>
                  <h3 style={{ fontSize: "16px", fontWeight: "600", margin: "0" }}>Tags</h3>
                </div>

                <div style={{ display: "contents" }}>
                  {this.state.tags
                    ? this.state.tags.map((ele) => (
                      <Link className="p-2 task-tags-skill" to="#">
                        {ele.name}
                      </Link>
                    ))
                    : null}
                </div>
              </div>

              <div className="task-author-content">
                <div className="image-holder">
                  <img
                    height="75px"
                    style={{ borderRadius: "50%" }}
                    src={Images.Social_work}
                    alt=""
                  />
                </div>
                <div className="content-holder-task">
                  <h5
                    style={{
                      fontSize: "1.6rem",
                      fontWeight: "600px",
                    }}
                  >
                    {`${this.state.task_creator.first_name} ${this.state.task_creator.last_name} `}
                  </h5>
                  <p style={{ color: "gray" }}>
                    This is a ProjectHeena Operations account that can be tagged as the partner
                    organization where work was done, but there was no NGO involved. Several social
                    initiative executed by ProjectHeena team/volunteers will come here.
                  </p>
                  <CustomButtonOutline content={`View Creators Profile`} />
                </div>
              </div>

              <div style={{ margin: "25px 0" }}>
                <div style={{ marginBottom: "10px" }}>
                  <h3 style={{ fontSize: "16px", fontWeight: "600", margin: "0" }}>
                    Task Volunteers
                  </h3>
                </div>
                <div>
                  {this.state.volunteersList.map((ele, idx) => (
                    <>
                      <div class="container1" style={{ display: "inline-block" }}>
                        <img
                          src={Images.user_logo}
                          alt="Avatar"
                          class="image1"
                          style={{
                            borderRadius: "50%",
                            width: "70%",
                            marginLeft: "3%",
                          }}
                        />
                        <div class="overlay1" style={{ display: "inline-block" }}>
                          <div class="text1" style={{ display: "inline-block" }}>
                            {`${ele.first_name} ${ele.last_name}`}
                          </div>
                        </div>
                      </div>
                    </>
                  ))}
                </div>
              </div>
              {/* {this.state.users == true ? (
                <>
                  <div style={{ marginTop: "3%" }}>
                    <div class="container1" style={{ display: "inline-block" }}>
                      <img
                        src={Images.user_logo}
                        alt="Avatar"
                        class="image1"
                        style={{
                          borderRadius: "50%",
                          width: "70%",
                          marginLeft: "3%",
                        }}
                      />
                      <div class="overlay1" style={{ display: "inline-block" }}>
                        <div class="text1" style={{ display: "inline-block" }}>
                          Daksh Khatri
                        </div>
                      </div>
                    </div>

                    <div class="container1" style={{ display: "inline-block" }}>
                      <img
                        src={Images.user_logo}
                        alt="Avatar"
                        class="image1"
                        style={{
                          borderRadius: "50%",
                          width: "70%",
                          marginLeft: "3%",
                        }}
                      />
                      <div class="overlay1" style={{ display: "inline-block" }}>
                        <div class="text1" style={{ display: "inline-block" }}>
                          Reethika Renganathan{" "}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div style={{ marginTop: "3%" }}>
                    <a onClick={this.handleClick} style={{ paddingLeft: "29%", color: "#4caf4f" }}>
                      <i class="fas fa-arrow-up" style={{ color: "#4caf4f" }}></i> Show less{" "}
                    </a>
                  </div>
                </>
              ) : (
                <div style={{ marginTop: "3%" }}>
                  <a onClick={this.handleClick} style={{ paddingLeft: "29%", color: "#4caf4f" }}>
                    <i class="fas fa-arrow-down" style={{ color: "#4caf4f" }}></i> Show more{" "}
                  </a>
                </div>
              )} */}

              <Modal
                show={this.state.show}
                onHide={this.handleClose}
                style={{ opacity: "1" }}
                size="md"
              >
                <Modal.Header>
                  <h4
                    style={{
                      fontWeight: "600",
                      fontSize: "14px",
                      color: "#6b6e70",
                      paddingTop: "2%",
                    }}
                  >
                    Offer Help on "Social Work"
                  </h4>
                </Modal.Header>
                <Modal.Body>
                  {/* <textarea className="p-3" id="w3review" name="w3review" rows="2 " cols="51" style={{ marginLeft: '9px', marginTop: '20px', color: '#9a9a9a', borderColor: '#9a9a9a' }}>
                    Reason why you wish to help for this task.
                  </textarea>
                  <p style={{ marginLeft: '9px' }}>Max: 500 chars | Remaining Chars: 500</p> */}
                  <Form className="p-3 ">
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                      <Form.Control
                        as="textarea"
                        rows={3}
                        placeholder="Reason why you wish to help for this task."
                      />
                      <p
                        style={{
                          marginLeft: "4px",
                          marginTop: "4px",
                          fontWeight: "600",
                          color: "#686b6d",
                        }}
                      >
                        Max: 500 chars | Remaining Chars: 500
                      </p>
                      <hr
                        style={{
                          backgroundColor: "#e7eaec",
                          width: "100%",
                          borderStyle: "dashed",
                          marginTop: "30px",
                        }}
                      />

                      <p
                        style={{
                          marginLeft: "4px",
                          marginTop: "20px",
                          fontWeight: "600",
                          color: "#686b6d",
                        }}
                      >
                        Will any of your family member joining you?
                      </p>
                      <div class="form-check-inline">
                        <label
                          class="form-check-label"
                          for="radio1"
                          style={{ marginLeft: "40px", marginRight: "36px" }}
                        >
                          <input
                            type="radio"
                            class="form-check-input"
                            id="radio1"
                            name="optradio"
                            value="option1"
                          />
                          Option 1
                        </label>
                      </div>
                      <div class="form-check-inline">
                        <label class="form-check-label" for="radio2">
                          <input
                            type="radio"
                            class="form-check-input"
                            id="radio2"
                            name="optradio"
                            value="option2"
                          />
                          Option 2
                        </label>
                      </div>

                      <hr
                        style={{
                          backgroundColor: "#e7eaec",
                          width: "100%",
                          borderStyle: "dashed",
                          marginTop: "30px",
                          marginBottom: "20px",
                        }}
                      />
                      <div class="form-check-inline" style={{ marginLeft: "15px" }}>
                        <label class="checkbox-inline">
                          <input
                            type="checkbox"
                            value=""
                            style={{ marginLeft: "-18px", marginRight: "36px" }}
                          />
                          Share contact number
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" value="" />
                          Share Email address
                        </label>
                      </div>
                      <hr
                        style={{
                          backgroundColor: "#e7eaec",
                          width: "100%",
                          borderStyle: "dashed",
                          marginTop: "30px",
                          marginBottom: "20px",
                        }}
                      />
                      <Form.Group
                        as={Row}
                        controlId="formPlaintextPassword"
                        style={{ marginLeft: "10px" }}
                      >
                        <Form.Label column sm="4">
                          Offer help as:
                        </Form.Label>
                        <Col sm="6">
                          <Form.Control as="select">
                            <option>Individual</option>
                          </Form.Control>
                        </Col>
                      </Form.Group>
                    </Form.Group>
                  </Form>
                </Modal.Body>

                <Modal.Footer>
                  <div onClick={this.handleClose}>
                    {/* <ReactBootstrap.Button variant="secondary">Close</ReactBootstrap.Button> */}
                    <button
                      className="p-2"
                      onClick={this.handleClose}
                      style={{
                        backgroundColor: "#c2c2c2",
                        border: "1px solid #c2c2c2",
                        color: "white",
                      }}
                    >
                      Close
                    </button>
                  </div>
                  {/* <ReactBootstrap.Button variant="primary">Add meta data</ReactBootstrap.Button> */}
                  <button
                    className="p-2"
                    onClick={this.handleClose}
                    style={{
                      backgroundColor: "#4caf4f",
                      border: "1px solid #4caf4f",
                      color: "white",
                    }}
                  >
                    Submit
                  </button>
                </Modal.Footer>
              </Modal>
            </Col>
          </Row>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  console.log("state", state);
  return {
    user: state.phAuth.user,
    skillsList: state.common.skillsList,
  };
};
export default connect(mapStateToProps)(TaskDetails);

// export default connect(mapStateToProps)(TaskDetails);
