import React from 'react'
import { Table } from "react-bootstrap"
import { Link } from "react-router-dom"
const MyProjects = () => {
    const MyProjects = [
        {
            name: "Dristi",
            location: "mumbai, maharashtra, india",
            health: "poor",
        },
        {
            name: "Dristi",
            location: "mumbai, maharashtra, india",
            health: "poor",
        },
        {
            name: "Dristi",
            location: "mumbai, maharashtra, india",
            health: "poor",
        },
        {
            name: "Dristi",
            location: "mumbai, maharashtra, india",
            health: "poor",
        }
    ]
    return (
        <div className='container'>
            <h3 className='fw-bold'>Dashboard</h3>

            <div
                className="box-ibox-title"
                style={{ borderBottom: "1px solid rgb(231 231 231)" }}
            >
                <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                    My Projects
                </h5>
            </div>
            <div className='p-3 bg-white'>
                <Table responsive="xl" striped className='mb-5'>
                    <thead>
                        <tr>
                            <td className='title-gray'>Name</td>
                            <td className='title-gray'>location</td>
                            <td className='title-gray'>health</td>
                            <td className='title-gray'> Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        {MyProjects.map((x, index) => (
                            <tr>
                                <td className='title-gray'>
                                    <Link to="#" className="basecolor">{x.name}</Link>
                                </td>
                                <td className='title-gray'> {x.location}</td>
                                <td className='title-gray'> {x.health}</td>
                                <td className='title-gray'><Link><i style={{ color: "#4caf4f", fontSize: "18px" }} class="fas fa-eye"></i></Link></td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default MyProjects
