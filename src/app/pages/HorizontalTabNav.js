import React from 'react';
import '../../index.scss';

class HorizontalTabNav extends React.Component {


  render() {
    return (
      <>
        {/* style={{backgroundColor:"red"}} */}
        <div style={{ width: '100%' }}>

          {/* <ul className="nav nav-tabs " style={{marginLeft:'20%', borderBottom:'transparent'}} > */}

          <ul className="nav nav-tabs nav-fill" style={{ borderBottom: 'transparent' }} >
            {
              this.props.tabs.map(tab => {
                console.log("Tab value check", tab);
                const active = (tab.name === this.props.selected ? 'active ' : '');
                return (
                  <li className="nav-item" key={tab.name} style={{ textAlign: 'center' }}>

                    <a className={"nav-link " + active + "horizontal-tabs justify-content-center"} onClick={() => this.props.setSelected(tab.name)} style={{ flexDirection: "column" }} >

                      <img src={tab.img} />
                      {tab.name}
                    </a>
                  </li>
                );
              })
            }

          </ul>
          {this.props.children}

        </div>

      </>
    )
  }
}

export default HorizontalTabNav;