import React from 'react';
import MilestoneDetail from './MilestoneDetail';
import { Card, Row, Col } from 'react-bootstrap';
import ViewMilestone from '../components/viewMilestone';
import axios from '../config/axios';
import moment from "moment"
class Milestone extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            main: true,
            view: false,
            milestone_list: []
        }
    }
    handleView = () => {
        this.setState({ view: true, main: false })
    }
    componentDidMount() {
        axios.post("/project/milestone", { project_id: "610b9905aac55a27bca1fba0" })
            .then(({ data, message, status }) => {
                if (status == 1) {
                    this.setState({ ...this.state, milestone_list: data })
                }
            })
            .catch(e => console.log("e", e))
    }
    render() {
        return (
            <>{
                this.state.main == true ? <div className="ibox p-3">
                    <div>
                        <Row>
                            <Col sm={12}>
                                <div className="item">
                                    <h3 className="csr_heading"><i class="fas fa-road"></i> Project Milestones</h3>
                                </div>
                                <div className="table-responsive">
                                    <table width="100%">
                                        <thead>
                                            <tr style={{ lineHeight: '40px' }}>
                                                <th className="milestone-th">Est. Start Date</th>
                                                <th className="milestone-th">Est. End Date</th>
                                                <th width="50%" className="milestone-th">Milestone</th>
                                                <th width="10%"  >Status</th>
                                                <th width="10%" class="text-center " width="100">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            {/* {this.state.milestone_list.length > 0 ? */}
                                            {this.state.milestone_list.map((ele, index) => (
                                                <tr style={{ borderTop: '1px solid #dddddd', lineHeight: '40px' }}>
                                                    {/* <td style={{ fontSize: '13px', width: '14%' }}>01 <sup>st</sup> May, 2018</td> */}
                                                    <td style={{ fontSize: '13px', width: '14%' }}>{moment(ele.estimated_start_date).format("Do MMM, YYYY")}</td>
                                                    <td style={{ fontSize: '13px', width: '14%' }}>{moment(ele.estimated_close_date).format("Do MMM, YYYY")}</td>
                                                    <td style={{ fontSize: '13px', width: '14%' }}><a href="http://projectheena.in/project/smokeless-1526310597/view-milestone/304" target="_blank" class="text-muted" data-toggle="tooltip" data-placement="bottom" title=" &quot;Partner On-Boarding&quot;">
                                                        {ele.name}</a></td>
                                                    <td><small style={{
                                                        backgroundColor: '#9fa1a2', color: 'white', border: '1px solid #9fa1a2'
                                                    }}>Open</small></td>
                                                    <td style={{ textAlign: 'center' }}>
                                                        {/* <a onClick={this.handleView}><i class="far fa-eye" style={{ marginRight: '4px' }}></i></a>
                                                            <a href="/edit-milestone"><i class="fas fa-pencil-alt" style={{ marginRight: '4px' }}></i></a>
                                                            <a href="close-milestone"><i class="fas fa-trash" style={{ marginRight: '4px' }}></i></a>
                                                            <i class="fas fa-check"></i> */}
                                                    </td>
                                                </tr>
                                            ))}
                                            {/* // : null} */}
                                        </tbody>
                                    </table>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div> : false
            }
                {
                    this.state.view == true ?
                        <ViewMilestone /> : null
                }
            </>
        )
    }
}

export default Milestone