/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import { Card, Table } from "react-bootstrap";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";
import { makeStyles } from "@material-ui/core/styles";
import axios from "../config/axios";
import { connect, useSelector } from "react-redux";
import { Link } from "react-router-dom"

const Current = () => {
  const currentLog = [
    {
      name: "Record Audiobooks - Panchatantra and Jataka Tales",
      status: "Open",
    },
    {
      name: "Record Audiobooks - Panchatantra and Jataka Tales",
      status: "Open",
    },
    {
      name: "Record Audiobooks - Panchatantra and Jataka Tales",
      status: "Open",
    },
    {
      name: "Record Audiobooks - Panchatantra and Jataka Tales",
      status: "Open",
    },
    {
      name: "Record Audiobooks - Panchatantra and Jataka Tales",
      status: "Open",
    },
    {
      name: "Record Audiobooks - Panchatantra and Jataka Tales",
      status: "Open",
    },
    {
      name: "Record Audiobooks - Panchatantra and Jataka Tales",
      status: "Open",
    },
    {
      name: "Record Audiobooks - Panchatantra and Jataka Tales",
      status: "Open",
    },
    {
      name: "Record Audiobooks - Panchatantra and Jataka Tales",
      status: "Open",
    },
  ];
  const user = useSelector((state) => state.phAuth.user);
  console.log(user._id);
  const getTaskList = () => {
    axios.post("/task/getTaskByCreatorId", { creator_id: user._id }).then((response) => {
      const data = response.data;
      console.log("response", data);
      setCurrent(data);
    });
  };
  const getDeletedTaskList = () => {
    axios.post("/task/getDeletedTaskList", { creatorId: user._id }).then((response) => {
      const data = response.data;
      console.log("deleted List", data);
      setDeleted(data);
    });
  };
  useEffect(() => {
    getTaskList();
    getDeletedTaskList();
  }, []);
  const useStylesBootstrap = makeStyles((theme) => ({
    arrow: {
      color: theme.palette.common.black,
    },
    tooltip: {
      backgroundColor: theme.palette.common.black,
      fontSize: "12px",
      width: "130px",
    },
  }));
  function BootstrapTooltip(props) {
    const classes = useStylesBootstrap();

    return <Tooltip arrow classes={classes} {...props} />;
  }
  const [current, setCurrent] = useState([]);
  const [deleted, setDeleted] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentPerPage] = useState(10);
  const indexOfLastCurrent = currentPage * currentPerPage;
  const indexOfFirstCurrent = indexOfLastCurrent - currentPerPage;
  const currentCurrent = current.slice(indexOfFirstCurrent, indexOfLastCurrent);
  const pageNumber = [];
  const totalCurrent = current.length;

  for (let i = 1; i <= Math.ceil(totalCurrent / currentPerPage); i++) {
    pageNumber.push(i);
  }
  return (
    <>
      <Card>
        <h3 className="ibox-title" style={{ fontSize: "16px", fontWeight: "600" }}>
          My Volunteering Log
        </h3>
        <div
          className="ibox-content"
          style={{
            borderStyle: "solid solid none",
            borderWidth: "1px 0",
            borderColor: "#e7eaec",
          }}
        >
          <Table responsive="xl" striped>
            <thead>
              <tr>
                <td>Name</td>
                <td>Status</td>
                <td></td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {currentCurrent.map((ele, index) => (
                <tr>
                  <td style={{ fontSize: "13px", minWidth: "200px" }}>
                    <Link to={`/task-detail/${ele._id}`} className="ph-link">
                      {ele.task_name}
                    </Link>
                  </td>
                  <td style={{ fontSize: "13px" }}>
                    {ele.task_status === 4 ? (
                      <span
                        style={{
                          backgroundColor: "#4caf4f",
                          color: "white",
                          padding: "5px",
                          fontSize: "10px",
                        }}
                      >
                        Closed
                      </span>
                    ) : (
                      <span
                        style={{
                          backgroundColor: "#4caf4f",
                          color: "white",
                          padding: "5px",
                          fontSize: "10px",
                        }}
                      >
                        Open
                      </span>
                    )}
                  </td>
                  <td style={{ padding: "5px 8px 0 8px" }}>
                    <a href={`/task-detail/${ele._id}`}>
                      <i style={{ color: "#4caf4f", fontSize: "22px" }} class="fas fa-eye"></i>
                    </a>
                  </td>
                  <td style={{ fontSize: "13px" }}>
                    {ele.task_status === 4 ? null : (
                      <BootstrapTooltip
                        TransitionComponent={Fade}
                        TransitionProps={{ timeout: 300 }}
                        placement="right"
                        title={`Edit "${ele.task_name}"`}
                      >
                        <a href={`/editTask/${ele._id}`}>
                          <i style={{ color: "#4caf4f" }} class="fas fa-pencil-alt"></i>
                        </a>
                      </BootstrapTooltip>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </Card>
      <nav>
        <ul className="pagination">
          {pageNumber.map((item) => (
            <li key={item} className="page-item">
              <button onClick={() => setCurrentPage(item)} className="page-link">
                {item}
              </button>
            </li>
          ))}
        </ul>
      </nav>
    </>
  );
};

export default Current;
