/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
// import * as ReactBootstrap from "react-bootstrap";
import { Row, Col, Button, Tabs, Tab, Form } from "react-bootstrap";
import { Images } from "../../app/config/Images";
import { withStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import Modal from "react-bootstrap/Modal";
// import CustomButton from "../components/CustomButton";
import axios from "../config/axios";
import Dropdown from "react-bootstrap/Dropdown";
import Rating from "@material-ui/lab/Rating";

import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const ManageTask = (props) => {
  console.log("status", props.status);
  // const useStyles = makeStyles((theme) => ({
  //   root: {
  //     width: 300 + theme.spacing(3) * 2,
  //   },
  //   margin: {
  //     height: theme.spacing(3),
  //   },
  // }));
  const task_id = localStorage.getItem("task_data_id");
  const [userId, setUserId] = useState();
  console.log("uid", userId);
  const [show, setShow] = useState(false);
  const [accept, setAccept] = useState(false);
  const [key, setKey] = useState("Appreciate");
  const [defaultReason, setDefaultReason] = useState("");
  const [unutilize, setUnutilize] = useState("");
  const [rate, setRate] = useState(2);
  const [hours, setHours] = useState(0);
  const [appreciate, setAppreciate] = useState("");
  const [shareContact, setShareContact] = useState(0);
  const [shareEmail, setShareEmail] = useState(0);
  const handleShareContact = (e) => {
    e.target.checked ? setShareContact(1) : setShareContact(0);
  };
  const handleEmailContact = (e) => {
    e.target.checked ? setShareEmail(1) : setShareEmail(0);
  };
  const handleAcceptClose = () => {
    setAccept(false);
    setShareContact(0);
    setShareEmail(0);
  };
  const defaultUser = () => {
    var data = {};
    data.task_id = task_id;
    data.task_user_id = userId;
    data.reason = defaultReason;
    console.log("data", data);
    axios
      .post("/taskTransaction/defaultUser", data)
      .then((res) => console.log("default", res))
      .catch((err) => console.log("default err", err));
    setDefaultReason("");
    setUserId(undefined);
    setShow(false);
  };
  const unutilizeUser = () => {
    var data = {};
    data.task_id = task_id;
    data.task_user_id = userId;
    data.reason = unutilize;
    axios
      .post("/taskTransaction/unutilizeUser", data)
      .then((res) => console.log("default", res))
      .catch((err) => console.log("default err", err));
    console.log("data", data);
    setUnutilize("");
    setUserId(undefined);
    setShow(false);
  };
  const appreciateUser = () => {
    var data = {};
    data.task_id = task_id;
    data.task_user_id = userId;
    data.reason = appreciate;
    data.total_hours_credited = hours;
    data.doerStarRating = rate;
    console.log("data", data);
    axios
      .post("/taskTransaction/thankUser", data)
      .then((res) => console.log("thankuser", res))
      .catch((err) => console.log("thankuser err", err));
    setUserId(undefined);
    setAppreciate("");
    setRate();
    setHours();
    setShow(false);
  };
  const deleteTask = () => {
    if (window.confirm("Are you sure?")) {
      axios
        .post("/task/newDeleteTaskApi", { taskId: task_id })
        .then((res) => window.location.reload())
        .catch((err) => console.log("err", err));
    }
  };
  const closeTask = () => {
    axios
      .post("/task/newCloseTaskApi", { taskId: task_id })
      .then((res) => window.location.reload())
      .catch((err) => console.log("err", err));
  };
  const handleClose = () => {
    setShow(false);
    setDefaultReason("");
    setUserId(undefined);
  };
  const [closeTaskModal, setCloseTaskModal] = useState(false);
  const [showAcceptOfferModal, setShowAcceptOfferModal] = useState(false);
  const handleCloseTask = () => setCloseTaskModal(false);
  const handleShow = (idx) => setShow(idx);
  const handleShowAcceptOfferModal = () => setShowAcceptOfferModal(true)
  const handleCloseAcceptOfferModal = () => setShowAcceptOfferModal(false)
  const [pendingOfferHelp, setPendingOfferHelp] = useState([]);
  const [pendingSeekHelp, setPendingSeekHelp] = useState([]);
  const [taskVolunteer, setTaskVolunteer] = useState([]);

  console.log(taskVolunteer);
  const getPendingOfferHelpReq = () => {
    axios
      .get(`/taskTransaction/getPendingOfferHelpReq/${task_id}`)
      .then((res) => {
        console.log("getPendingOfferHelpReq", res);
        setPendingOfferHelp(res.data);
      })
      .catch((err) => console.log("err", err));
  };
  const taskVolunteersList = () => {
    axios
      .get(`/taskTransaction/taskVolunteersList/${task_id}`)
      .then((res) => {
        console.log("taskVolunteersList", res);
        setTaskVolunteer(res.data);
      })
      .catch((err) => console.log("err", err));
  };
  const getPendingSeekHelpReq = () => {
    axios
      .get(`/taskTransaction/getPendingSeekHelpReq/${task_id}`)
      .then((res) => {
        console.log("getPendingSeekHelpReq", res);
        setPendingSeekHelp(res.data);
      })
      .catch((err) => console.log("err", err));
  };
  const acceptOffer = (user_id) => {
    axios
      .post("/taskTransaction/acceptOfferHelp", { task_id: task_id, user_id: user_id })
      .then((res) => {
        setPendingOfferHelp(res.data);
        setTaskVolunteer(res.volunteer);
        handleShowAcceptOfferModal();
        props.getVolunteerList(task_id)
      })
      .catch((err) => console.log("err", err));
  };

  const handleRejectOffer = (user_id) => {
    if (window.confirm("Do you really want to turn down the help request?")) {
      console.log("yes", user_id);
      axios
        .post("/taskTransaction/TurnDownOfferHelp", { task_id: task_id, user_id: user_id })
        .then((res) => {
          console.log("turn down", res);
          // setPendingOfferHelp(res.data);
          // setTaskVolunteer(res.volunteer);
        })
        .catch((err) => console.log("err", err));
    }
  };
  useEffect(() => {
    getPendingOfferHelpReq();
    taskVolunteersList();
    getPendingSeekHelpReq();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  const PrettoSlider = withStyles({
    root: {
      color: "#52af77",
      height: 8,
    },
    thumb: {
      height: 24,
      width: 24,
      backgroundColor: "#fff",
      border: "2px solid currentColor",
      marginTop: -8,
      marginLeft: -12,
      "&:focus, &:hover, &$active": {
        boxShadow: "inherit",
      },
    },
    active: {},
    valueLabel: {
      left: "calc(-50% + 4px)",
    },
    track: {
      height: 8,
      borderRadius: 4,
    },
    rail: {
      height: 8,
      borderRadius: 4,
    },
  })(Slider);
  const handleHourChange = (e, newVal) => {
    setHours(newVal);
    console.log(newVal);
  };

  return (
    <>
      <div>
        {props.status === 4 || props.status === 5 ? (
          props.status === 4 ? (
            <div>
              <h3 className="font-bold" style={{ fontSize: "1.5rem" }}>
                Current Status - CLOSED
              </h3>
            </div>
          ) : (
            <div>
              <h3 className="font-bold" style={{ fontSize: "1.5rem" }}>
                Current Status - DELETED
              </h3>
            </div>
          )
        ) : (
          <>
            <div style={{ marginBottom: "25px" }}>
              <div>
                <h3 className="font-bold" style={{ fontSize: "1.5rem" }}>
                  Current Status - Open
                </h3>
              </div>
              <div>
                <Dropdown>
                  <Dropdown.Toggle variant="secondary">Change Task Status</Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item
                      href="#/action-1"
                      onClick={() => setCloseTaskModal(true)}
                      style={{
                        width: "150px",
                        color: "gray",
                        fontSize: "14px",
                        padding: "12px 20px",
                      }}
                    >
                      Close Task
                    </Dropdown.Item>
                    <Dropdown.Item
                      href="#/action-2"
                      onClick={() => deleteTask()}
                      style={{
                        width: "150px",
                        color: "gray",
                        fontSize: "14px",
                        padding: "12px 20px",
                      }}
                    >
                      Delete Task
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </div>
            <div style={{ marginBottom: "25px" }}>
              <div>
                <h3 className="font-bold" style={{ fontSize: "1.5rem" }}>
                  Following users are part of this task. Please acknowledge their work
                </h3>
              </div>
              <div>
                <Row>
                  {taskVolunteer.map((ele, idx) => (
                    <>
                      <Col md={6}>
                        <div className="result-card">
                          <div className="result-basic" style={{ width: "30%" }}>
                            <img src={Images.userlogo} className="img-circle" alt="logo"></img>
                          </div>
                          <div className="result-desc">
                            <h3 className="font-bold" style={{ fontSize: "16px" }}>
                              {ele.first_name}
                            </h3>

                            <div style={{ height: "110px" }}>
                              <div>
                                <div style={{ display: "table-cell", width: "25px" }}>
                                  <i
                                    class="fas fa-map-marker-alt"
                                    style={{
                                      fontSize: "13px",
                                      color: "#616161",
                                      paddingRight: "8px",
                                    }}
                                  ></i>
                                </div>
                                <div style={{ display: "table-cell" }}>
                                  <span> {ele.user_location}</span>
                                </div>
                              </div>
                              <div>
                                <div style={{ display: "table-cell", width: "25px" }}>
                                  <i
                                    class="fas fa-phone"
                                    style={{
                                      fontSize: "13px",
                                      color: "#616161",
                                      paddingRight: "8px",
                                    }}
                                  ></i>
                                </div>
                                <div style={{ display: "table-cell" }}>
                                  <span> {ele.user_contact_number}</span>
                                </div>
                              </div>
                              <div>
                                <div style={{ display: "table-cell", width: "25px" }}>
                                  <i
                                    class="fas fa-envelope"
                                    style={{
                                      fontSize: "13px",
                                      color: "#616161",
                                      paddingRight: "8px",
                                    }}
                                  ></i>
                                </div>
                                <div style={{ display: "table-cell" }}>
                                  <span> {ele.email_id}</span>
                                </div>
                              </div>
                              <button
                                className="ph-btn-outline"
                                style={{ margin: "15px 0" }}
                                onClick={() => {
                                  handleShow(idx);
                                  setUserId(ele._id);
                                }}
                              >
                                Evaluate
                              </button>
                            </div>
                          </div>
                        </div>
                      </Col>
                      <Modal show={show === idx} onHide={handleClose} animation={false} centered>
                        {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
                        <div className="modal-header-custom">
                          <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                            Evaluate "{ele.first_name}"
                          </h4>
                          <Button
                            variant="secondary"
                            style={{ height: "30px" }}
                            onClick={handleClose}
                          >
                            <i
                              class="fas fa-times"
                              style={{ paddingRight: "0", color: "white" }}
                            ></i>
                          </Button>
                        </div>
                        <Modal.Body>
                          <div>
                            <Tabs
                              id="controlled-tab-example"
                              activeKey={key}
                              onSelect={(k) => setKey(k)}
                            >
                              <Tab
                                eventKey="Appreciate"
                                title="Appreciate"
                                style={{ opacity: "100" }}
                              >
                                <div style={{ padding: "30px 10px" }}>
                                  <h3 className="font-bold" style={{ fontSize: "20px" }}>
                                    Worked For {hours} Hours
                                  </h3>
                                  <div></div>
                                  <Form>
                                    <div style={{ display: "flex", alignItems: "center" }}>
                                      <PrettoSlider
                                        valueLabelDisplay="auto"
                                        aria-label="pretto slider"
                                        defaultValue={0}
                                        max={50}
                                        step={1}
                                        value={hours}
                                        onChange={handleHourChange}
                                        style={{ maxWidth: "220px", margin: "20px 0" }}
                                      />
                                      <Rating
                                        name="simple-controlled"
                                        value={rate}
                                        onChange={(event, newValue) => {
                                          setRate(newValue);
                                        }}
                                        style={{ margin: "0 10px", fontSize: "25px" }}
                                      />
                                    </div>

                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                      <Form.Control
                                        as="textarea"
                                        rows={5}
                                        placeholder="Write a reason for defaulting this user on this task."
                                        className="green-focus"
                                        value={appreciate}
                                        onChange={(e) => setAppreciate(e.target.value)}
                                      />
                                      <span className="help-block">
                                        Max: 500 chars | Remaining Chars: {500}
                                      </span>
                                    </Form.Group>
                                    <div>
                                      <button
                                        type="button"
                                        className="ph-btn"
                                        onClick={() => appreciateUser()}
                                      >
                                        Appreciate
                                      </button>
                                    </div>
                                  </Form>
                                </div>
                              </Tab>
                              <Tab eventKey="profile" title="Default" style={{ opacity: "100" }}>
                                <div style={{ padding: "30px 10px" }}>
                                  <h3 className="font-bold" style={{ fontSize: "20px" }}>
                                    Reason for Defaulting
                                  </h3>
                                  <Form>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                      <Form.Control
                                        as="textarea"
                                        rows={5}
                                        placeholder="Write a reason for defaulting this user on this task."
                                        className="green-focus"
                                        value={defaultReason}
                                        onChange={(e) => setDefaultReason(e.target.value)}
                                      />
                                      <span className="help-block">
                                        Max: 500 chars | Remaining Chars:{" "}
                                        {500 - defaultReason.length}
                                      </span>
                                    </Form.Group>
                                    <div>
                                      <button
                                        type="button"
                                        className="ph-btn"
                                        onClick={() => defaultUser()}
                                      >
                                        Default
                                      </button>
                                      {/* <CustomButton content="Default" />{" "} */}
                                    </div>
                                  </Form>
                                </div>
                              </Tab>
                              <Tab eventKey="contact" title="Unutilized" style={{ opacity: "100" }}>
                                <div style={{ padding: "30px 10px" }}>
                                  <h3 className="font-bold" style={{ fontSize: "20px" }}>
                                    Reason for Unutilizing
                                  </h3>
                                  <Form>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                      <Form.Control
                                        as="textarea"
                                        rows={5}
                                        placeholder="Write a reason for defaulting this user on this task."
                                        className="green-focus"
                                        value={unutilize}
                                        onChange={(e) => setUnutilize(e.target.value)}
                                      />
                                      <span className="help-block">
                                        Max: 500 chars | Remaining Chars: {500 - unutilize.length}
                                      </span>
                                    </Form.Group>
                                    <div>
                                      <button
                                        type="button"
                                        className="ph-btn"
                                        onClick={() => unutilizeUser()}
                                      >
                                        Unutilize
                                      </button>
                                    </div>
                                  </Form>
                                </div>
                              </Tab>
                            </Tabs>
                          </div>
                        </Modal.Body>
                      </Modal>
                    </>
                  ))}
                </Row>
              </div>
            </div>
            {pendingOfferHelp.length > 0 ? (
              <>
                {" "}
                <div style={{ marginBottom: "25px" }}>
                  <div>
                    <h3 className="font-bold" style={{ fontSize: "1.5rem" }}>
                      Following people who have Offered help to You
                    </h3>
                  </div>
                  <div>
                    <Row>
                      {pendingOfferHelp.length > 0 &&
                        pendingOfferHelp.map((ele, idx) => (
                          <>
                            <Col md={6}>
                              <div className="result-card">
                                <div className="result-basic" style={{ width: "30%" }}>
                                  <img
                                    src={Images.userlogo}
                                    className="img-circle"
                                    alt="logo"
                                  ></img>
                                </div>
                                <div className="result-desc">
                                  <h3 className="font-bold" style={{ fontSize: "16px" }}>
                                    {`${ele.first_name} ${ele.last_name}`}
                                  </h3>

                                  <div>
                                    <div>
                                      <div style={{ display: "table-cell", width: "25px" }}>
                                        <i
                                          class="fas fa-map-marker-alt"
                                          style={{
                                            fontSize: "13px",
                                            color: "#616161",
                                            paddingRight: "8px",
                                          }}
                                        ></i>
                                      </div>
                                      <div style={{ display: "table-cell" }}>
                                        <span> {ele.user_location}</span>
                                      </div>
                                    </div>
                                    <div>
                                      <div style={{ display: "table-cell", width: "25px" }}>
                                        <i
                                          class="fas fa-phone"
                                          style={{
                                            fontSize: "13px",
                                            color: "#616161",
                                            paddingRight: "8px",
                                          }}
                                        ></i>
                                      </div>
                                      <div style={{ display: "table-cell" }}>
                                        <span> {ele.user_contact_number}</span>
                                      </div>
                                    </div>
                                    <div>
                                      <div style={{ display: "table-cell", width: "25px" }}>
                                        <i
                                          class="fas fa-envelope"
                                          style={{
                                            fontSize: "13px",
                                            color: "#616161",
                                            paddingRight: "8px",
                                          }}
                                        ></i>
                                      </div>
                                      <div style={{ display: "table-cell" }}>
                                        <span> {ele.email_id}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div style={{ display: "flex", justifyContent: "space-between" }}>
                                    <button
                                      className="ph-btn-outline"
                                      style={{ margin: "15px 0", whiteSpace: "nowrap" }}
                                      // onClick={() => acceptOffer(ele._id)}
                                      onClick={() => setAccept(idx)}
                                    >
                                      Accept Offer
                                    </button>
                                    <button
                                      className="ph-btn-dark-outline"
                                      style={{ margin: "15px 0" }}
                                      onClick={() => handleRejectOffer(ele._id)}
                                    >
                                      Reject Offer
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </Col>

                            <Modal
                              show={accept === idx}
                              onHide={handleAcceptClose}
                              animation={false}
                              centered
                            >
                              {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
                              <div className="modal-header-custom">
                                <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                                  Accept {`${ele.first_name} ${ele.last_name}'s`} Help On{" "}
                                  {props.data.task_name}
                                </h4>
                                <Button
                                  variant="secondary"
                                  style={{ height: "30px" }}
                                  onClick={handleAcceptClose}
                                >
                                  <i
                                    class="fas fa-times"
                                    style={{ paddingRight: "0", color: "white" }}
                                  ></i>
                                </Button>
                              </div>
                              <Modal.Body>
                                <div style={{ minHeight: "110px", padding: "10px 10px 40px 10px" }}>
                                  <p>
                                    Share your Contact Details with Umang to collaborate easily to
                                    accomplish this task
                                  </p>
                                  <Form.Group>
                                    <FormControlLabel
                                      // id={1}
                                      control={<Checkbox color="primary" />}
                                      label={"Share Contact Number"}
                                      value={shareContact}
                                      // checked={causessupported.includes(element._id)}
                                      onChange={handleShareContact}
                                    />
                                    <FormControlLabel
                                      // id={1}
                                      control={<Checkbox color="primary" />}
                                      label={"Share Email Address"}
                                      value={shareEmail}
                                      // checked={causessupported.includes(element._id)}
                                      onChange={handleEmailContact}
                                    />
                                  </Form.Group>
                                </div>
                              </Modal.Body>
                              <Modal.Footer>
                                <div>
                                  <Button
                                    variant="secondary"
                                    style={{ margin: "0 8px" }}
                                    onClick={handleAcceptClose}
                                  >
                                    Close
                                  </Button>
                                  <button
                                    className="ph-btn"
                                    onClick={() => {
                                      handleAcceptClose();
                                      acceptOffer(ele._id);
                                    }}
                                  >
                                    Accept Offer
                                  </button>
                                </div>
                              </Modal.Footer>
                            </Modal>
                            <Modal show={showAcceptOfferModal} onHide={handleCloseAcceptOfferModal} animation={false} >
                              {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
                              <div className="modal-header-custom">
                                <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                                  Accept  {`${ele.first_name} ${ele.last_name}'s`} Help On {props.data.task_name}
                                </h4>
                                <Button
                                  variant="secondary"
                                  style={{ height: "30px" }}
                                  onClick={handleCloseAcceptOfferModal}
                                >
                                  <i
                                    class="fas fa-times"
                                    style={{ paddingRight: "0", color: "white" }}
                                  ></i>
                                </Button>
                              </div>
                              <Modal.Body>
                                <p>You have successfully accepted the help offer from {ele.first_name} {ele.last_name}</p>
                              </Modal.Body>
                            </Modal>
                          </>
                        ))}
                    </Row>
                  </div>
                </div>
              </>
            ) : null}

            {pendingSeekHelp.length > 0 && (
              <div style={{ marginBottom: "25px" }}>
                <div>
                  <h3 className="font-bold" style={{ fontSize: "1.5rem" }}>
                    Following people are yet to respond to your Help Request
                  </h3>
                </div>
                <div>
                  <Row>
                    {pendingSeekHelp.length > 0 &&
                      pendingSeekHelp.map((ele, idx) => (
                        <>
                          <Col md={6}>
                            <div className="result-card">
                              <div className="result-basic" style={{ width: "30%" }}>
                                <img src={Images.userlogo} className="img-circle" alt="logo"></img>
                              </div>
                              <div className="result-desc">
                                <h3 className="font-bold" style={{ fontSize: "16px" }}>
                                  {`${ele.first_name} ${ele.last_name}`}
                                </h3>

                                <div style={{ height: "80px" }}>
                                  <div>
                                    <div style={{ display: "table-cell", width: "25px" }}>
                                      <i
                                        class="fas fa-map-marker-alt"
                                        style={{
                                          fontSize: "13px",
                                          color: "#616161",
                                          paddingRight: "8px",
                                        }}
                                      ></i>
                                    </div>
                                    <div style={{ display: "table-cell" }}>
                                      <span>{ele.user_location}</span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Col>
                        </>
                      ))}
                  </Row>
                </div>
              </div>
            )}
          </>
        )}
      </div>
      <Modal show={closeTaskModal} onHide={handleCloseTask} animation={false}>
        {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
        <div className="modal-header-custom">
          <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
            Close Task
          </h4>
          <Button variant="secondary" style={{ height: "30px" }} onClick={handleCloseTask}>
            <i class="fas fa-times" style={{ paddingRight: "0", color: "white" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ padding: "10px 10px 25px 10px" }}>
            <p>In order to close this task you have to credit doers for their work</p>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseTask}>
            {" "}
            Close{" "}
          </Button>
          <button className="ph-btn" onClick={() => closeTask()}>
            Close Task
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default ManageTask;
