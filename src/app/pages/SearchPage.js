import React from "react";

import HorizontalTabNav from "./HorizontalTabNav";
import HorizontalTabContent from "./HorizonatalTabContent";
import { Row, Col } from "react-bootstrap";
import { Card } from "@material-ui/core";
import { Images } from "../config/Images";
import "../../index.scss";
import CustomButtonOutline from "../components/CustomButtonOutline";

import * as ReactBootstrap from "react-bootstrap";
import SearchPeople from "./SearchPeople";
import TaskSearchBox from "../components/TaskSearchBox";
import PeopleSearchBox from "../components/PeopleSearchBox";
import SearchTask from "./SearchTask";
import SearchProject from "./SearchProject";
import ProjectSearchBox from "../components/ProjectSearchBox";
import DonationSearchBox from "../components/DonationSearchBox";
import SearchDonation from "./SearchDonation";

const icons = [
  { icon: <i className="fa fa-child"></i> },
  { icon: <i className="fa fa-tasks"></i> },
  { icon: <i className="fa fa-hand-paper-o"></i> },
  { icon: <i className="fa fa-rupee"></i> },
];

class SearchPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: "Tasks",
    };
  }

  setSelected = (tab) => {
    this.setState({ selected: tab });
  };

  render() {
    return (
      <div className="container">
        <div className="container">
          <h1 style={{ color: "#75787a", marginTop: "3%", marginBottom: "3%" }}>
            Search: 140 Tasks found that matches your query
          </h1>

          <HorizontalTabNav
            tabs={["People", "Tasks", "Project", "Donate"]}
            icons={icons}
            selected={this.state.selected}
            setSelected={this.setSelected}
          >
            <HorizontalTabContent
              isSelected={this.state.selected === "People"}
              onClick={this.handlePeople}
            >
              <Row>
                <Col md={3}>
                  <PeopleSearchBox />
                </Col>
                <Col md={9}>
                  <SearchPeople />
                </Col>
              </Row>
            </HorizontalTabContent>

            <HorizontalTabContent isSelected={this.state.selected === "Tasks"}>
              <Row>
                <Col md={3}>
                  <TaskSearchBox />
                </Col>
                <Col md={9}>
                  <SearchTask />
                </Col>
              </Row>
            </HorizontalTabContent>

            <HorizontalTabContent isSelected={this.state.selected === "Project"}>
              <Row>
                <Col md={3}>
                  <ProjectSearchBox />
                </Col>
                <Col md={9}>
                  <SearchProject />
                </Col>
              </Row>
            </HorizontalTabContent>

            <HorizontalTabContent isSelected={this.state.selected === "Donate"}>
              <Row>
                <Col md={3}>
                  <DonationSearchBox />
                </Col>
                <Col md={9}>
                  <SearchDonation />
                </Col>
              </Row>
            </HorizontalTabContent>
          </HorizontalTabNav>
        </div>
      </div>
    );
  }
}

export default SearchPage;
