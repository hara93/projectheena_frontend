import React from 'react';

const Timeline = () => {
    return (
        <div class="container bootdey">
            <div class="row gutters">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="timeline">
                                <div class="timeline-row">
                                    <div class="timeline-time">
                                        7:45PM<small>May 21</small>
                                    </div>
                                    <div class="timeline-dot fb-bg"></div>
                                    <div class="timeline-content">
                                        <i class="fa fa-map-marker"></i>
                                        <h4>Admin theme!</h4>
                                        <p>Milestone Admin Dashboard contains C3 graphs, flot graphs, data tables, calendar, drag &amp; drop and ion slider.</p>
                                        <div class="">
                                            <span class="badge badge-light">Design</span>
                                            <span class="badge badge-light">Admin</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-row">
                                    <div class="timeline-time">
                                        8:00 AM<small>May 18</small>
                                    </div>
                                    <div class="timeline-dot green-one-bg"></div>
                                    <div class="timeline-content green-one">
                                        <i class="fa fa-warning"></i>
                                        <h4>Admin theme!</h4>
                                        <p>
                                            Milestone Admin Dashboard contains C3 graphs, flot graphs, data tables, calendar.
                                        </p>
                                        <div class="">
                                            <span class="badge badge-light">Design</span>
                                            <span class="badge badge-light">Admin</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-row">
                                    <div class="timeline-time">
                                        7:25 PM<small>May 6</small>
                                    </div>
                                    <div class="timeline-dot green-two-bg"></div>
                                    <div class="timeline-content green-two">
                                        <i class="fa fa-list"></i>
                                        <h4>Best Admin Template!</h4>
                                        <p>Custom C3 graphs, Custom flot graphs, flot graphs, small graphs, Sass, profile and timeline.</p>
                                        <div>
                                            <span class="badge badge-light">Invoice</span>
                                            <span class="badge badge-light">Graphs</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-row">
                                    <div class="timeline-time">
                                        3:55 PM<small>Apr 26</small>
                                    </div>
                                    <div class="timeline-dot green-three-bg"></div>
                                    <div class="timeline-content green-three">
                                        <i class="icon-directions"></i>
                                        <h4>Milestone Admin</h4>
                                        <p>
                                            Admin theme includes graphs, invoice, timeline, widgets, projects, calendar, components, layouts, todo's.
                                        </p>
                                        <div>
                                            <span class="badge badge-light">Profile</span>
                                            <span class="badge badge-light">Dashboard</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-row">
                                    <div class="timeline-time">
                                        5:24 PM<small>Apr 12</small>
                                    </div>
                                    <div class="timeline-dot green-four-bg"></div>
                                    <div class="timeline-content green-four">
                                        <i class="fa fa-user"></i>
                                        <h4>Milestone Dashboard</h4>
                                        <p class="no-margin">Milestone Admin Dashboard includes invoice, profile, tasks, gallery, projects, maintanence.</p>
                                        <div>
                                            <span class="badge badge-light">Analytics</span>
                                            <span class="badge badge-light">Graphs</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-row">
                                    <div class="timeline-time">
                                        11:25 AM<small>Apr 19</small>
                                    </div>
                                    <div class="timeline-dot teal-bg"></div>
                                    <div class="timeline-content teal">
                                        <i class="fa fa-coffee"></i>
                                        <h4>Milestone Template</h4>
                                        <p class="no-margin">Panels, alerts, notifications, new input styles, pricing plans, project plan, signup, login and register.</p>
                                        <div>
                                            <span class="badge badge-light">Labels</span>
                                            <span class="badge badge-light">Filters</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-row">
                                    <div class="timeline-time">
                                        12:30 PM<small>May 25</small>
                                    </div>
                                    <div class="timeline-dot sea-green-bg"></div>
                                    <div class="timeline-content sea-green">
                                        <i class="fa fa-image"></i>
                                        <h4>Milestone dashboard</h4>
                                        <p>Milestone Admin Dashboard contains Ion slider, heatmap, alerts, breadcrumbs, alerts, pricing, signup, login and register.</p>
                                        <div>
                                            <span class="badge badge-light">BS 4</span>
                                            <span class="badge badge-light">Sass</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Timeline;