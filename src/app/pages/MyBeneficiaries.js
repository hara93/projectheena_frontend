/* eslint-disable no-restricted-imports */

import React, { useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import { withStyles } from "@material-ui/core/styles";
import MuiAccordion from "@material-ui/core/Accordion";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
import CustomButton from "../components/CustomButton";
import axios from "../config/axios";
import { useSelector } from "react-redux"
import { useHistory } from "react-router";

const Accordion = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none !important",
    "&:not(:last-child)": {
      borderBottom: 0,
    },
    "&:before": {
      display: "none",
    },
    "&$expanded": {
      margin: "auto",
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 37,
    "&$expanded": {
      minHeight: 37,
    },
    borderStyle: "solid solid none",
    borderWidth: "4px 0 0",
    borderColor: "#e7eaec",
    borderLeftWidth: "1px",
    borderRightWidth: " 1px",
    borderBottomWidth: "1px",
  },
  content: {
    "&$expanded": {
      margin: "0 ",
    },
    margin: "0 ",
  },

  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiAccordionDetails);

const MyBeneficiaries = () => {
  const history = useHistory()
  const ngo_id = useSelector((state) => state.phAuth && state.phAuth.ngo && state.phAuth.ngo._id || 0)
  const [names, setnames] = useState([{ name: "daksh" }, { name: "daksh" }, { name: "daksh" }, { name: "daksh" }])
  const getBeneficiaryList = () => {
    axios.post("beneficiary/list", { ngo_id })
      .then(({ data, status, message }) => {
        setnames(data)
      })
      .catch((err) => {
        alert(err);
      });
  }
  useEffect(() => {
    getBeneficiaryList()
  }, [])
  const handleDelete = (id) => {
    var query = { beneficiary_for: ngo_id, beneficiary_id: id }
    axios.post("beneficiary/remove", query)
      .then(({ data, status, message }) => {
        setnames(data)
      })
      .catch((err) => {
        alert(err);
      });
  }
  const handleEdit = (id) => {
    console.log("id", id)
    history.push({
      pathname: '/updateBeneficiary',
      state: { beneficiary_id: id }
    })
  }
  return (
    <div className="container">
      <Accordion square defaultExpanded style={{ boxShadow: "none" }}>
        <AccordionSummary style={{ minHeight: "37px", margin: "0" }}>
          <h5
            style={{
              fontSize: "14px",
              fontWeight: "600",
              borderBottom: "0",
              marginBottom: "0",
            }}
          >
            {" "}
            My Beneficiaries
          </h5>
        </AccordionSummary>
        <AccordionDetails style={{ diaply: "flex", flexDirection: "column" }}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-end",
            }}
          >
            <a href="/addBeneficiary">
              {" "}
              <CustomButton content="Create Beneficiary" />
            </a>
          </div>
          <Table striped hover size="sm">
            <thead>
              <tr>
                <th>Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {names.map((ele, index) => (
                <tr style={{ fontSize: "13px" }}>
                  <td>{ele.name}</td>
                  <td>
                    <div>
                      <button className="edit-beneficiary" onClick={(e) => handleEdit(ele._id)} style={{ border: "none", backgroundColor: "transparent" }}><i class="fas fa-pencil-alt edit-ben-link"></i></button>
                      <button className="edit-beneficiary" onClick={(e) => handleDelete(ele._id)} style={{ border: "none", backgroundColor: "transparent" }}><i class="fas fa-trash-alt edit-ben-link"></i></button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};
export default MyBeneficiaries;
