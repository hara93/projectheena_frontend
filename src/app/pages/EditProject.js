/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import "../../index.scss";
import DatePicker from "react-datepicker";
import Card from "react-bootstrap/Card";
import { Form, Col, InputGroup, FormControl } from "react-bootstrap";
import CheckboxTable from "../components/checkboxTable";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useFormik } from "formik";
import { string, object, number } from "yup";
import axios from "../config/axios";
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/froala_editor.pkgd.min.css";
import FroalaEditorComponent from "react-froala-wysiwyg";
import "froala-editor/js/plugins.pkgd.min.js";
import TagAutocomplete from "../components/tagAutocomplete";
import { connect } from "react-redux";

const EditProject = (props) => {
  // console.log("props", props.user._id);
  const [projectData, setProjectData] = useState({});

  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [estimatedCost, setEstimatedCost] = useState();
  const [excerpt, setExcerpt] = useState("");
  const [desc, setDesc] = useState("");
  const [projectContact, setProjectContact] = useState();
  const [externalUrl, setExternalUrl] = useState("");
  const [cause, setCause] = useState([]);

  var pathname = props.location.pathname.split("/");
  var project_slug = pathname.pop();

  const getProjectBySlugOrId = () => {
    axios
      .post("/project/project-details", {
        project_slug: project_slug,
        session_data: { logged_in_id: props.user._id, logged_in_type: 1 },
      })
      .then((response) => {
        console.log("data log", response.data);
        setProjectData(response.data);
        var c = Date.parse(response.data.project_start_date);
        var d = Date.parse(response.data.project_end_date);
        setStartDate(c);
        setEndDate(d);
        setEstimatedCost(response.data.estimated_cost);
        setExcerpt(response.data.project_excerpts);
        setDesc(response.data.project_desc);
        setProjectContact(response.data.project_contact);
        setExternalUrl(response.data.external_url);
        if (response.data.causes) {
          var caus = [];
          response.data.causes.map((ele) => caus.push(ele.cause_id));
          setCause(caus);
        }
      });
  };

  const history = useHistory();
  const dispatch = useDispatch();
  const defaultValues = {
    project_name: "",
    project_duration: "",
    estimated_cost: 0,
    project_contact: projectContact,
    external_url: externalUrl,
    project_excerpts: "",
  };
  const validationSchema = object().shape({
    estimated_cost: number().required("Please provide estimated cost"),
    // project_contact: string()
    //   .required("Please provide contact details")
    //   .min(5, "Contact details must be atleast 5 characters long"),
    external_url: string().url("Please enter a valid url"),
    project_excerpts: string(),
  });

  const onSubmit = (values) => {
    values.project_name = projectData.project_name;
    values.project_duration = projectData.project_duration;
    values.project_lat = projectData.project_lat;
    values.project_lng = projectData.project_lng;
    values.external_url = externalUrl;
    values.project_contact = projectContact;
    values.project_location = projectData.project_location;
    values.project_slug = projectData.project_slug;
    values.project_excerpts = excerpt;
    values.project_desc = desc;
    values.project_start_d = startDate;
    values.project_end_d = endDate;
    values.causes_supported = cause.join(",");
    values.project_id = projectData._id;
    values.logged_in_id = props.user._id;
    values.logged_in_type = 1;
    values.estimated_cost = estimatedCost;
    // values.tags = tag;
    axios.post("/project/editProjectData", { ...values }).then(({ data }) => {
      console.info(data);
      // dispatch(setUserData(data));
      history.push("/getProjects");
    });
    console.log("values", values);
  };
  const {
    handleChange,
    handleBlur,
    handleSubmit,
    values,
    errors,
    touched,
    initialValues,
  } = useFormik({
    initialValues: defaultValues,
    validationSchema,
    onSubmit,
  });
  const [count, setCount] = useState(0);
  var maxCount = 256;

  var temp_name = values.project_name.split(" ");
  var slug = temp_name.join("-");

  const handleExcerpt = (e) => {
    setExcerpt(e.target.value);
    console.log(excerpt.length);
  };
  const max = 5000;
  const handleModelChange = (e, editor) => {
    const char = e;
    var plainText = char.replace(/<[^>]+>/g, "");

    setDesc(char);
  };

  const config = {
    charCounterMax: 2000,
  };
  const error = {};
  error.name = errors.project_name ? errors.project_name : "no error";
  error.duration = errors.project_duration ? errors.project_duration : "no error";
  error.estimated_cost = errors.estimated_cost ? errors.estimated_cost : "no error";
  error.project_contact = errors.project_contact ? errors.project_contact : "no error";
  error.external_url = errors.external_url ? errors.external_url : "no error";
  error.project_excerpts = errors.project_excerpts ? errors.project_excerpts : "no error";
  // console.log(error);
  const [tag, setTag] = useState([]);
  const tag_array = tag.map((ele) => ele["name"]);
  var temp = [];

  useEffect(() => {
    if (tag.length > 3) {
      for (var i = 0; i < 3; i++) {
        temp.push(tag[i]);
      }
      console.log("if");
      tag.splice(0, tag.length);
      setTag(temp);
    }
    getProjectBySlugOrId();
    // setCauseCheck(checkedCauses);
  }, []);
  // console.log("check causes", checkedCauses);
  return (
    <>
      <div className="container">
        <div>
          <h2 className="font-bold">Edit Project</h2>

          <p>
            Projects are an avenue to collaborate with Corporate organizations. Do note approval is
            solely on our discretion.
          </p>
          <p>Please provide as detailed information as possible.</p>
        </div>
        <Card>
          <Card style={{ padding: "30px" }}>
            <form onSubmit={handleSubmit}>
              <Form.Group>
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Project Name
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      name="project_name"
                      value={projectData.project_name}
                      disabled
                      placeholder="Enter Project Name"
                      className="green-focus"
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Project Duration
                  </Form.Label>
                  <Col>
                    <InputGroup>
                      <FormControl
                        size="lg"
                        type="number"
                        name="project_duration"
                        placeholder="Number of Days"
                        disabled
                        value={projectData.project_duration}
                        className="green-focus"
                      />
                      <InputGroup.Append>
                        <InputGroup.Text
                          style={{ background: "transparent", border: "1px solid #e5e6e7" }}
                        >
                          Days
                        </InputGroup.Text>
                      </InputGroup.Append>
                    </InputGroup>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom  p-r-15">
                    Location
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      name="project_name"
                      value={projectData.project_location}
                      disabled
                      placeholder="Enter Project Name"
                      className="green-focus"
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom  p-r-15">
                    Cause Supported
                  </Form.Label>
                  <Col style={{ marginTop: "-10px" }}>
                    <CheckboxTable
                      cause={(causes) => {
                        setCause(causes);
                        console.log("table", causes);
                      }}
                      // alreadyChecked={[
                      //   "610a41913f7b0467c5ad20c9",
                      //   "610a41913f7b0467c5ad20ca",
                      //   "610a41913f7b0467c5ad20cb",
                      // ]}
                      alreadyChecked={cause}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom  p-r-15">
                    Project Excerpts
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      name="project_excerpts"
                      as="textarea"
                      maxLength="256"
                      minLength="50"
                      rows="5"
                      placeholder="Your Project info in maximum 256 Characters"
                      onChange={(e) => {
                        setCount(e.target.value.length);
                        handleExcerpt(e);
                      }}
                      value={excerpt}
                      onBlur={handleBlur}
                      className="green-focus"
                      style={{ borderRadius: "0" }}
                    />
                    <span className="help-block">
                      Min: 50 chars | Max: 256 chars | Remaining Chars: {maxCount - count}
                    </span>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Project Description
                  </Form.Label>
                  <Col>
                    {/* <TextEditor /> */}
                    <div style={{ maxWidth: "100%" }}>
                      <FroalaEditorComponent
                        tag="textarea"
                        config={config}
                        onModelChange={handleModelChange}
                        model={desc}
                      />
                    </div>
                    <span className="help-block">
                      Min : 256 | Max : 2000 | Remaining : {max - desc.length}
                    </span>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Project Image
                  </Form.Label>
                  <Col className="form-input-align-center ">
                    <Form.File className="position-relative" name="file" />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Select Start And End Date
                  </Form.Label>
                  <Col lg={5} className="form-input-align-center">
                    {" "}
                    <DatePicker
                      placeholderText="Start Date"
                      selected={startDate}
                      dateFormat="dd-MM-yyyy"
                      selectsStart
                      startDate={startDate}
                      endDate={endDate}
                      onChange={(date) => {
                        setStartDate(date);
                        console.log(date.getTime());
                      }}
                      popperPlacement="top-end"
                      style={{ color: "yellow" }}
                      required
                      className="green-focus"
                    />
                  </Col>
                  <Col lg={5} className="form-input-align-center">
                    {" "}
                    <DatePicker
                      placeholderText="End Date"
                      selected={endDate}
                      dateFormat="dd-MM-yyyy"
                      selectsEnd
                      startDate={startDate}
                      endDate={endDate}
                      minDate={startDate}
                      popperPlacement="top-end"
                      onChange={(date) => setEndDate(date)}
                      required
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Estimated Cost
                  </Form.Label>
                  <Col>
                    <InputGroup style={{ zIndex: 0 }}>
                      <InputGroup.Prepend>
                        <InputGroup.Text
                          id="basic-addon1"
                          style={{
                            margin: "0",
                            background: "transparent",
                            border: "1px solid #e5e6e7",
                          }}
                        >
                          <i class="fa fa-rupee"></i>
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        type="number"
                        size="lg"
                        name="estimated_cost"
                        onChange={(e) => {
                          setEstimatedCost(e.target.value);
                        }}
                        value={estimatedCost}
                        className="green-focus"
                      />
                    </InputGroup>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                    style={{ marginTop: "auto", marginBottom: "auto" }}
                  >
                    Comma Seperated Tags
                  </Form.Label>
                  <Col>
                    <TagAutocomplete
                      tags={(tag) => {
                        var dataArr = tag.map((item) => {
                          return [item.name, item];
                        });
                        var maparr = new Map(dataArr);
                        var result = [...maparr.values()];
                        setTag(result);
                      }}
                      tag={tag}
                    />
                    <Form.Text className="text-muted" muted>
                      Maximum 3 tags allowed
                    </Form.Text>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Contact Info
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      name="project_contact"
                      onChange={(e) => setProjectContact(e.target.value)}
                      onBlur={handleBlur}
                      value={projectContact}
                      className="green-focus"
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    External URL
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      placeholder="Enter the website link if you have any"
                      name="external_url"
                      onChange={(e) => setExternalUrl(e.target.value)}
                      value={externalUrl}
                      className="green-focus"
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom"></Form.Label>
                  <Col>
                    <button
                      type="submit"
                      className="ph-btn"
                      style={{ fontSize: "14px", padding: "6px" }}
                    >
                      Edit Project
                    </button>
                    {/* <CustomButton content="Create Project" /> */}
                  </Col>
                </Form.Row>
              </Form.Group>
            </form>
          </Card>
        </Card>
      </div>
    </>
  );
};
const mapStateToProps = function (state) {
  return {
    user: state.phAuth.user,
  };
};

export default connect(mapStateToProps)(EditProject);
// export default CreateProject;
