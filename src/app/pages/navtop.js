import React from "react";
import NotificationsIcon from "@material-ui/icons/Notifications";
import PersonIcon from "@material-ui/icons/Person";
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
import * as ReactBootstrap from "react-bootstrap";

class NavTop extends React.Component {
  render() {
    return (
      <div>
        <ReactBootstrap.Navbar style={{ width: "100%" }} bg="light" expand="lg">
          <ReactBootstrap.Navbar.Brand
            href="#home"
            style={{ marginRight: "2vw" }}
          >
            ProjectHeena
          </ReactBootstrap.Navbar.Brand>
          <ReactBootstrap.Navbar.Toggle aria-controls="basic-navbar-nav" />
          <ReactBootstrap.Navbar.Collapse id="basic-navbar-nav">
            <ReactBootstrap.Nav className="mr-auto">
              <ReactBootstrap.NavDropdown
                title="Volunteer"
                style={{ marginRight: "2vw" }}
                id="basic-nav-dropdown"
              >
                <ReactBootstrap.NavDropdown.Item href="#action/3.1">
                  My Tasks
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Item href="#action/3.2">
                  Search Tasks
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Item href="#action/3.3">
                  Search Volunteers
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Item href="#action/3.3">
                  Create Task
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Item href="#action/3.3">
                  Create Karmalog
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Item href="#action/3.3">
                  Volunteering Certificates
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Divider />
                <ReactBootstrap.NavDropdown.Item href="#action/3.4">
                  About Volunteering
                </ReactBootstrap.NavDropdown.Item>
              </ReactBootstrap.NavDropdown>

              <ReactBootstrap.NavDropdown
                title="Donate"
                style={{ marginRight: "2vw" }}
                id="basic-nav-dropdown"
              >
                <ReactBootstrap.NavDropdown.Item href="#action/3.1">
                  Search Donation{" "}
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Item href="#action/3.2">
                  Donation Reports
                </ReactBootstrap.NavDropdown.Item>

                <ReactBootstrap.NavDropdown.Divider />
                <ReactBootstrap.NavDropdown.Item href="#action/3.4">
                  About Donations
                </ReactBootstrap.NavDropdown.Item>
              </ReactBootstrap.NavDropdown>

              <ReactBootstrap.NavDropdown
                title="Advocate"
                style={{ marginRight: "2vw" }}
                id="basic-nav-dropdown"
              >
                <ReactBootstrap.NavDropdown.Item href="#action/3.1">
                  Amplify
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Item href="#action/3.2">
                  Write Blog
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Item href="#action/3.3">
                  Blog
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Divider />
                <ReactBootstrap.NavDropdown.Item href="#action/3.4">
                  About Advocacy
                </ReactBootstrap.NavDropdown.Item>
              </ReactBootstrap.NavDropdown>

              <ReactBootstrap.NavDropdown
                title="CSR Projects"
                style={{ marginRight: "2vw" }}
                id="basic-nav-dropdown"
              >
                <ReactBootstrap.NavDropdown.Item href="#action/3.1">
                  Search Projects{" "}
                </ReactBootstrap.NavDropdown.Item>
                <ReactBootstrap.NavDropdown.Item href="#action/3.2">
                  My Projects
                </ReactBootstrap.NavDropdown.Item>

                <ReactBootstrap.NavDropdown.Divider />
                <ReactBootstrap.NavDropdown.Item href="#action/3.4">
                  About Collaborations
                </ReactBootstrap.NavDropdown.Item>
              </ReactBootstrap.NavDropdown>
            </ReactBootstrap.Nav>
          </ReactBootstrap.Navbar.Collapse>
        </ReactBootstrap.Navbar>
      </div>
    );
  }
}

export default NavTop;
