import React, { useState } from 'react'
import { Videos } from "../../config/Videos"
import { Row, Col } from "react-bootstrap"
import moment from "moment"
export default function Homepage() {
    const [showExcerpt, setShowExcerpt] = useState(false)

    return (
        <>
            <div>
                <video autoPlay muted loop video width="100%" height="100%">
                    <source src={Videos.intro} type="video/mp4"></source>
                </video>
            </div>
            <div style={{ color: "red", width: "100%", backgroundColor: "black", opacity: 0.9, textAlign: "center", padding: "20px 0", fontSize: 28, position: "relative", bottom: "120px" }}>
                A Glenmark Joy of Giving Initiative
            </div>
            <div style={{ backgroundColor: "#F1F1F1", textAlign: "center", position: "relative", bottom: "40px" }}>
                <h1 style={{ color: "black", fontWeight: 600 }}>YES! YOU CAN MAKE A DIFFERENCE</h1>
                <h5 style={{ padding: "5px 80px", fontWeight: 600, color: "gray" }}>As part of Glenmark's principles of giving! We bring you the opportunities beyond daily office work, which can impact hundreds of lives. Let's join hands and contribute back to the society</h5>
                <button className="btn" style={{ borderRadius: 8, fontSize: 18, fontWeight: 600, padding: "10px 20px", backgroundColor: "#f00", color: "white", borderColor: "#f00" }}>Volunteer Now</button>
            </div>
            <section style={{ background: "url(https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/backgroungimage.png) center center no-repeat", backgroundSize: "cover", paddingBottom: "50px" }}>
                <div style={{ padding: "20px 0" }}>
                    <Row className="no-gutters">
                        <Col style={{ textAlign: "center", padding: "30px 0" }}>
                            <h1 style={{ color: "black", fontWeight: 600 }}>HOW CAN YOU HELP</h1>
                        </Col>
                    </Row>
                    <Row className="no-gutters">
                        <Col md={4} xs={12} md={{ span: 4, offset: 2 }}>
                            <div style={{ background: "white", width: "95%", height: "200px", padding: "20px 0 10px 0", borderRadius: "20px", margin: "15px 10px" }}>
                                <ul>
                                    <h2 style={{ color: "black" }}>EDUCATION & TRAINING</h2>
                                    <li style={{
                                        padding: 0, listStyleType: "disc", color: "black", fontSize: "15px"
                                    }}><b>Teach Maths, Science, English in Municipal Schools, Orphanages</b></li>
                                    <li style={{
                                        padding: 0, listStyleType: "disc", color: "black", fontSize: "15px"
                                    }}><b>Tutor Underprivileged Children</b></li>
                                    <li style={{
                                        padding: 0, listStyleType: "disc", color: "black", fontSize: "15px"
                                    }}><b>Teach Soft Skills to Women's Self Help Group</b></li>
                                    <li style={{
                                        padding: 0, listStyleType: "disc", color: "black", fontSize: "15px"
                                    }}><b>Financial Literacy Training</b></li>
                                </ul>
                            </div>
                        </Col>
                        <Col md={4} xs={12} md={{ span: 4, offset: 0 }}>
                            <div style={{ background: "white", width: "95%", height: "200px", padding: "20px 0 10px 0", borderRadius: "20px", margin: "15px 10px" }}>
                                <ul>
                                    <h2 style={{ color: "black" }}>Use Your Creative Skills</h2>
                                    <li style={{
                                        padding: 0, listStyleType: "disc", color: "black", fontSize: "15px"
                                    }}><b>Graphics Designing</b></li>
                                    <li style={{
                                        padding: 0, listStyleType: "disc", color: "black", fontSize: "15px"
                                    }}><b>Make Annual Report for an NGO</b></li>
                                    <li style={{
                                        padding: 0, listStyleType: "disc", color: "black", fontSize: "15px"
                                    }}><b>Arts, Craft, Music and Story-telling</b></li>
                                    <li style={{
                                        padding: 0, listStyleType: "disc", color: "black", fontSize: "15px"
                                    }}><b>Designing Newsletter for an NGO</b></li>
                                    <li style={{
                                        padding: 0, listStyleType: "disc", color: "black", fontSize: "15px"
                                    }}><b>Manage Social Media for an NGO</b></li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            <section style={{ backgroundColor: "#F1F1F1", textAlign: "center", position: "relative", bottom: "40px" }}>
                <Row className="no-gutters pt-5" >
                    <Col md={12} lg={4} sm={12} xs={12} className="text-center" style={{ padding: "15px" }}>
                        <div style={{ background: "#fff", borderRadius: "20px", height: "300px" }}>
                            {/* width: "100%", height: "310px", margin: "20px", padding: "10px" */}
                            <div>
                                <img style={{ width: "200px", margin: "0 auto" }} class="img-responsive" src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/head.png" />
                            </div>
                            <div>
                                <h5>WHY VOLUNTEER</h5>
                                <p style={{ fontSize: "14.5px" }}>
                                    Volunteering is the best form of enriching lives by contributing few hours.
                                    Lets do our small bit and together make a difference we are all proud of.
                                </p>
                                <br />
                                <a href="https://glenmarkvolunteers.com/join" style={{ padding: "10px 20px", backgroundColor: "#ED5565", color: "white", borderColor: "#ED5565", position: "absolute", left: "35%", bottom: "15px", width: "130px", }} >JOIN HERE</a>
                            </div>
                        </div>
                    </Col>
                    <Col md={4} xs={12} className="text-center" style={{ padding: "15px" }}>
                        <div style={{ background: "#fff", borderRadius: "20px", height: "300px" }}>
                            <div>
                                <img style={{ width: "200px", margin: "0 auto" }} class="img-responsive" src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/shakehand.png" />
                            </div>
                            <div>
                                <h5>VOLUNTEERING TO BUILD A BETTER TOMMOROW</h5>
                                <br />
                                <a href="https://glenmarkvolunteers.com/join" style={{
                                    padding: "10px 20px", backgroundColor: "#ED5565", color: "white", borderColor: "#ED5565", width: "130px", position: "absolute", bottom: "15px", left: "35%"
                                }} >KNOW MORE</a>
                            </div>
                        </div>
                    </Col>
                    <Col md={4} xs={12} className="text-center" style={{ padding: "15px" }}>
                        <div style={{ background: "#fff", borderRadius: "20px", height: "300px" }}>
                            <div>
                                <img style={{ width: "200px", margin: "0 auto" }} class="img-responsive" src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/hand.png" />
                            </div>
                            <div>
                                <h5>OPPORTUNITIES TO MAKE IMPACT</h5>
                                <p style={{ fontSize: "14.5px" }}>
                                    Based on your skills and preferences we give custom volunteering opportunities.
                                </p>
                                <br />
                                <a href="https://glenmarkvolunteers.com/join" style={{ padding: "10px 20px", backgroundColor: "#ED5565", color: "white", borderColor: "#ED5565", position: "absolute", left: "35%", bottom: "15px", width: "130px", }} >EXPLORE</a>
                            </div>
                        </div>
                    </Col>
                </Row>
            </section>
            <section style={{ background: "#facdbd", padding: "40px 0 20px 0" }}>
                <div className="container">
                    <Row className="no-gutters">
                        <Col md={12} className="text-center text-capitalize">
                            <h1 style={{ color: "black", fontWeight: "bold" }}>OUR CSR FOCUS</h1>
                        </Col>
                    </Row>
                    <Row className="no-gutters">
                        <Col md={12} className="text-center text-capitalize">
                            <h5 style={{ color: "black", fontWeight: "bold" }}>WE REALIZE OUR VISION BY WORKING PASSIONATELY IN THE FOLLOWING SECTORS</h5>
                        </Col>
                    </Row>
                    <Row className="no-gutters py-4">
                        <Col md={10} className="centered-column">
                            <Row className="no-gutters">
                                <Col md={1}>
                                    <img style={{
                                        display: "block",
                                        maxWidth: "80px",
                                        borderRadius: "50%",
                                        marginLeft: "auto",
                                        marginRight: "auto",
                                    }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home/pillar/child.jpg"></img>
                                </Col>
                                <Col md={11}>
                                    <Row className="no-gutters">
                                        <h3 style={{ padding: "0 10px", color: "black", fontWeight: 600 }}>CHILD HEALTH</h3>
                                    </Row>
                                    <Row className="no-gutters">
                                        <p style={{ padding: "0 10px", color: "black", fontSize: 14.5, fontWeight: 500 }}>
                                            Themed around ‘Healthier Children, Healthier World’, Glenmark Foundation is actively works towards improving maternal and child health.
                                            Glenmark Foundation aims to {" "}
                                            {showExcerpt ?
                                                <span>encourage a positive health seeking behaviour among pregnant women and mothers with infants, and caregivers. Along with its NGO partners the foundation has undertaken several community programs focused towards reducing infant and child mortality among the vulnerable population groups. The various interventions in child health are spread across Madhya Pradesh, Rajasthan, Maharashtra, Himachal Pradesh, Sikkim, Gujarat and Kenya.
                                                    <span style={{ color: "red", cursor: "pointer" }} onClick={() => setShowExcerpt(false)}>{" "}view less</span>
                                                </span>
                                                :
                                                <span style={{ color: "red", cursor: "pointer" }} onClick={() => setShowExcerpt(true)}>...view more</span>
                                            }
                                        </p>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="no-gutters py-3">
                        <Col md={6}>
                            <Row className="no-gutters">
                                <Col md={1}>
                                    <img style={{
                                        display: "block",
                                        maxWidth: "80px",
                                        borderRadius: "50%",
                                        marginLeft: "auto",
                                        marginRight: "auto",
                                    }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home/pillar/volunteering.png"></img>
                                </Col>
                                <Col md={11}>
                                    <Row className="no-gutters">
                                        <h3 style={{ padding: "0 35px", color: "black", fontWeight: 600 }}>EMPLOYEE VOLUNTEERING / JOY OF GIVING</h3>
                                    </Row>
                                    <Row className="no-gutters">
                                        <p style={{ padding: "0 35px", color: "black", fontSize: 14.5, fontWeight: 500 }}>
                                            Employees of Glenmark across the globe have been consistently volunteering through our CSR initiatives where they give their time and efforts to contribute to communities around them.
                                        </p>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={6}>
                            <Row className="no-gutters">
                                <Col md={1}>
                                    <img style={{
                                        display: "block",
                                        maxWidth: "80px",
                                        borderRadius: "50%",
                                        marginLeft: "auto",
                                        marginRight: "auto",
                                    }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home/pillar/livelihood.png"></img>
                                </Col>
                                <Col md={11}>
                                    <Row className="no-gutters">
                                        <h3 style={{ padding: "0 35px", color: "black", fontWeight: 600 }}>SUSTAINABLE LIVELIHOODS</h3>
                                    </Row>
                                    <Row className="no-gutters">
                                        <p style={{ padding: "0 35px", color: "black", fontSize: 14.5, fontWeight: 500 }}>
                                            In association with Jaipur Foot, we have been able to rehabilitate over 15,000 differently-abled individuals by providing artificial limbs, thus giving them the opportunity to lead a productive life.
                                        </p>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="no-gutters py-3">
                        <Col md={6}>
                            <Row className="no-gutters">
                                <Col md={1}>
                                    <img style={{
                                        display: "block",
                                        maxWidth: "80px",
                                        borderRadius: "50%",
                                        marginLeft: "auto",
                                        marginRight: "auto",
                                    }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home/pillar/aquatic.png"></img>
                                </Col>
                                <Col md={11}>
                                    <Row className="no-gutters">
                                        <h3 style={{ padding: "0 35px", color: "black", fontWeight: 600 }}>PROMOTION OF SPORTS IN INDIA</h3>
                                    </Row>
                                    <Row className="no-gutters">
                                        <p style={{ padding: "0 35px", color: "black", fontSize: 14.5, fontWeight: 500 }}>
                                            Employees of Glenmark across the globe have been consistently volunteering through our CSR initiatives where they give their time and efforts to contribute to communities around them.
                                        </p>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={6}>
                            <Row className="no-gutters">
                                <Col md={1}>
                                    <img style={{
                                        display: "block",
                                        maxWidth: "80px",
                                        borderRadius: "50%",
                                        marginLeft: "auto",
                                        marginRight: "auto",
                                    }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home/pillar/health.png"></img>
                                </Col>
                                <Col md={11}>
                                    <Row className="no-gutters">
                                        <h3 style={{ padding: "0 35px", color: "black", fontWeight: 600 }}>ACCESS TO HEALTH CARE</h3>
                                    </Row>
                                    <Row className="no-gutters">
                                        <p style={{ padding: "0 35px", color: "black", fontSize: 14.5, fontWeight: 500 }}>
                                            We are committed to donating medicines to the less privileged people who are suffering from life threatening and other diseases. We also provide aid in the event of natural disasters.
                                        </p>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </section >
            <section style={{ padding: "40px 0", background: "white", }}>
                <div className="container">
                    <Row className="no-gutters">
                        <Col md={12} className="text-center text-capitalize">
                            <h1 style={{ color: "black", fontWeight: "bold" }}>CSR IMPACT</h1>
                        </Col>
                        <Col md={12}>
                            <Row className="no-gutters">
                                <Col md={3} className="font-weight-bold text-center text-capitalize">
                                    <img src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/lives-touched.png"></img>
                                    <h2 className="font-weight-bold">18,60,000+</h2>
                                    <h6 className="font-weight-bold">LIVES TOUCHED</h6>
                                </Col>
                                <Col md={3} className="font-weight-bold text-center text-capitalize">
                                    <img src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/mal-nourished.png"></img>
                                    <h2 className="font-weight-bold">38,000+</h2>
                                    <h6 className="font-weight-bold">MALNOURISHED CHILDREN REACHED</h6>
                                </Col>
                                <Col md={3} className="font-weight-bold text-center text-capitalize">
                                    <img src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/child-benefit.png"></img>
                                    <h2 className="font-weight-bold">2,85,000+</h2>
                                    <h6 className="font-weight-bold">CHILDREN BENEFITTED</h6>
                                </Col>
                                <Col md={3} className="font-weight-bold text-center text-capitalize">
                                    <img src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/pregnant-lactating.png"></img>
                                    <h2 className="font-weight-bold">1,84,000+</h2>
                                    <h6 className="font-weight-bold">PREGNANT AND LACTATING WOMEN BENEFITTED</h6>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </section>
            <section style={{ padding: "40px 0 20px 0" }}>
                <div className="container">
                    <Row className="no-gutters">
                        <Col md={12} className="text-center text-capitalize">
                            <h1 style={{ color: "black", fontWeight: "bold", padding: "10px 0 30px 0" }}>OUR JOY OF GIVING IMPACT OVER THE YEARS</h1>
                        </Col>
                        <Col md={12}>
                            <Row className="no-gutters">
                                <Col md={3} className="p-3 font-weight-bold text-center text-capitalize">
                                    <div style={{ background: "white", padding: "10px 0", borderRadius: 10 }}>
                                        <img style={{ width: 150 }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/people.png"></img>
                                        <h2 className="font-weight-bold">6100+</h2>
                                        <h6 className="font-weight-bold">EMPLOYEES VOLLUNTEERED</h6>
                                    </div>
                                </Col>
                                <Col md={3} className="p-3 font-weight-bold text-center text-capitalize">
                                    <div style={{ background: "white", padding: "10px 0", borderRadius: 10 }}>
                                        <img style={{ width: 150 }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/clock.png"></img>
                                        <h2 className="font-weight-bold">40000+</h2>
                                        <h6 className="font-weight-bold">VOLUNTEERING HOURS</h6>
                                    </div>
                                </Col>
                                <Col md={3} className="p-3 font-weight-bold text-center text-capitalize">
                                    <div style={{ background: "white", padding: "10px 0", borderRadius: 10 }}>
                                        <img style={{ width: 150 }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/location.png"></img>
                                        <h2 className="font-weight-bold">47+</h2>
                                        <h6 className="font-weight-bold">LOCATION</h6>
                                    </div>
                                </Col>
                                <Col md={3} className="p-3 font-weight-bold text-center text-capitalize">
                                    <div style={{ background: "white", padding: "10px 0", borderRadius: 10 }}>
                                        <img style={{ width: 150 }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/country.png"></img>
                                        <h2 className="font-weight-bold">31+</h2>
                                        <h6 className="font-weight-bold">COUNTRIES</h6>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </section>
            <section style={{ padding: "40px 0 20px 0", background: "white" }}>
                <div className="container">
                    <Row className="no-gutters">
                        <Col md={12} className="text-center text-capitalize">
                            <h1 style={{ color: "gray", fontWeight: "bold", padding: "20px 0" }}>AWARDS & ACCOLADES</h1>
                        </Col>
                        <Col md={12}>
                        </Col>
                    </Row>
                </div>
            </section>
        </>
    )
}
// https://glenmarkvolunteers.com/assets/img/team/glenmark/home2/backgroungimage.png