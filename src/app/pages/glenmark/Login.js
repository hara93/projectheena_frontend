import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import { Images } from "../../config/Images";

import NGOlogin from "./../NGOlogin";
import CorporateLogin from "./../CorporateLogin";

import UserLogin from "./UserLogin"
const SignUp = () => {
    const [key, setKey] = useState("user");
    var icon1 = (
        <>
            <img src={Images.userlogin} alt="" height="35px"></img>
            <p
                style={{
                    margin: "0",
                    padding: "0",
                    fontWeight: "600",
                    fontSize: "16px",
                    color: "#686b6d",
                }}
            >
                User
            </p>
        </>
    );
    var icon2 = (
        <>
            <img src={Images.team} alt="" height="35px"></img>
            <p
                style={{
                    margin: "0",
                    padding: "0",
                    fontWeight: "600",
                    fontSize: "16px",
                    color: "#686b6d",
                }}
            >
                Admin
            </p>
        </>
    );

    return (
        <>
            <div style={{ maxWidth: "800px", paddingTop: 180, margin: "0 auto" }}>
                <Tabs
                    id="controlled-tab-example"
                    activeKey={key}
                    onSelect={(k) => setKey(k)}
                    style={{ display: "flex", justifyContent: "center" }}
                >
                    <Tab eventKey="user" title={icon1} style={{ opacity: "100%" }}>
                        <UserLogin />
                    </Tab>
                    <Tab eventKey="admin" title={icon2} style={{ opacity: "100%" }}>
                        <CorporateLogin />
                    </Tab>
                </Tabs>

                <p style={{ margin: "20px 0 0 0", textAlign: "center" }}>
                    This portal best works on Google Chrome.
                    <img src="http://projectheena.in/assets/img/chrome.jpg" alt="chrome" width="30"></img>
                </p>
            </div>
        </>
    );
};

export default SignUp;
