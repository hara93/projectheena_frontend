import React, { Suspense, lazy } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";
// import HomepageGlenmark from "../glenmark/Homepage";
import Homepage from "../glenmark/Homepage";
import Login from "../glenmark/Login";

export default function GlenmarkRoutes() {
    return (
        <Suspense fallback={<LayoutSplashScreen />}>
            <Switch>
                {
                    <Redirect exact from="/glenmark" to="/glenmark/home" />
                }
                <ContentRoute path="/glenmark/home" component={Homepage} />
                <ContentRoute path="/glenmark/join" component={Login} />

                <Redirect to="error/error-v1" />
            </Switch>
        </Suspense>
    );
}
