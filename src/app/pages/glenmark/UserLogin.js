import React from "react";
import { Row, Col } from "react-bootstrap";
import { Card } from "@material-ui/core";
import { useFormik } from "formik";
import { string, object } from "yup";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import axios from "./../../config/axios";
import { Images } from "./../../config/Images";
import Input from "./../../components/form/Input";
import { setNgoData } from "../../../redux/reducers/auth/actionCreator";

import { ToastContainer, toast } from 'react-toastify';

export default function UserLogin() {
    const history = useHistory();
    const dispatch = useDispatch();

    const defaultValues = {
        username_email: "",
        password: "",
    };

    const validationSchema = object().shape({
        username_email: string().required("Username or email is required"),
        password: string().required("Password is required"),
    });

    const onSubmit = (values) => {
        console.log("values", values)
        try {
            axios.post("ngo/login", { ...values })
                .then(({ data }) => {
                    console.info(data);
                    dispatch(setNgoData(data));
                    localStorage.setItem('token', data.token)
                    history.push("/ngo/" + data.ngo_username);
                })
                .catch(e => {
                    // console.log("ERROR", e)
                    toast.error("Login unsuccessful")
                })
        } catch (e) {
            alert(e.stack)
        }
    };

    const {
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        touched,
        initialValues,
    } = useFormik({
        initialValues: defaultValues,
        validationSchema,
        onSubmit,
    });

    return (
        <>
            <Row>
                <ToastContainer />
                <Card className="p-5">
                    <Row>
                        <Col xs={1} md={1} lg={1} style={{ padding: "0", margin: "0" }}>
                            <img
                                src="https://glenmarkvolunteers.com/assets/img/login/malefemale.png"
                                style={{ maxWidth: "200%" }}
                                alt=""
                            />
                        </Col>
                        <Col xs={11} md={11} lg={11}>
                            <div style={{ display: "flex", flexDirection: "column" }}>
                                <h2>Login as a Corporate Volunteer</h2>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={12} lg={6} className="col-divider-line">
                            <p>
                                Dont have a Volunteer account? Please click the below button to get one today!
                            </p>
                            <a href="/ngo-registration" style={{ background: "grey", }} className="mail">
                                {" "}
                                <i
                                    class="fas fa-envelope"
                                    style={{ color: "white", marginRight: "2%" }}
                                ></i>
                                Sign up with email
                            </a>
                        </Col>
                        <Col xs={12} md={12} lg={6}>
                            <form id="login-form" onSubmit={handleSubmit}>
                                <div>
                                    <Input
                                        type="text"
                                        name="username_email"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.username_email}
                                        value={values.username_email}
                                        touched={touched.username_email}
                                        aria-required="true"
                                        placeholder="username/email"
                                    ></Input>
                                </div>
                                <div>
                                    <Input
                                        type="password"
                                        name="password"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={errors.password}
                                        value={values.password}
                                        touched={touched.password}
                                        aria-required="true"
                                        placeholder="password"
                                    ></Input>
                                </div>
                                <div>
                                    <button type="submit" className="login">
                                        Login
                                    </button>
                                </div>

                                <div style={{ marginTop: "2vh" }}>
                                    <a
                                        href="#"
                                        style={{
                                            float: "left",
                                            color: "#4CAF50",
                                            fontWeight: "540",
                                        }}
                                    >
                                        Forgot Password?
                                    </a>
                                    <a
                                        href="#"
                                        style={{
                                            float: "right",
                                            color: "#4CAF50",
                                            fontWeight: "540",
                                        }}
                                    >
                                        Resend Confirmation email
                                    </a>
                                </div>
                            </form>
                        </Col>
                    </Row>
                </Card>
            </Row>
        </>
    );
}
