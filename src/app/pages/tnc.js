import React from "react";
import "../tnc.css";
import { Card } from "react-bootstrap";

class TermsandConditions extends React.Component {
  constructor() {
    super();
    this.state = {
      clickedTerms: true,
      clickedPrivacy: false,
      clickedCancellation: false,
    };
  }

  handleClick = () => {
    this.setState({
      clickedTerms: true,
      clickedPrivacy: false,

      clickedCancellation: false,
    });
  };
  handlePrivacy = () => {
    this.setState({
      clickedPrivacy: true,
      clickedTerms: false,
      clickedCancellation: false,
    });
  };

  handleCancellation = () => {
    this.setState({
      clickedCancellation: true,
      clickedTerms: false,
      clickedPrivacy: false,
    });
  };

  changeColour = () => {
    let initial = document.getElementById("tnc");
    initial.style.color = "#525252";
  };

  changeColour1 = () => {
    let initial1 = document.getElementById("privacy");
    initial1.style.color = "#525252";
  };

  changeColour2 = () => {
    let initial2 = document.getElementById("cancel");
    initial2.style.color = "#525252";
  };

  render() {
    return (
      <>
        {/* <img
          style={{
            width: "130%",
            marginLeft: "-165px",
            marginTop: "-25px",
            backgroundSize: "103%",
            backgroundAttachment: "fixed",
            backgroundPosition: "center left",
            height: "100%",
            maxHeight: "400px",
            backgroundRepeat: "no-repeat",
          }}
          src="/media/logos/superhero-kid.jpeg"
          alt=""
        /> */}
        <div className="banner">
          <div className="banner-img">
            <div id="parallex-bg" style={{ backgroundImage: "url('http://projectheena.in/assets/img/static/superhero-kid.jpg') " }}></div>
          </div>
        </div>
        <div className="container">
          <Card style={{ margin: "0 auto", padding: "0 15px" }}>
            <div style={{ marginTop: "30px", textAlign: "center" }}>
              <h3 className="static-title">Our Terms & Conditions</h3>
            </div>

            <div style={{ paddingBottom: "5px" }}>
              <ul className="donation-detail-tabs">
                <li>
                  <a href="#terms" className="active" onClick={this.handleClick}>
                    <i className="fa fa-bullhorn active-focus">
                      {" "}
                      TERMS & CONDITIONS
                    </i>
                  </a>
                </li>

                <li>
                  <a
                    href="#terms"
                    className="active"
                    onClick={this.handlePrivacy}
                  >
                    <i className="fa fa-bullhorn active-focus"> PRIVACY POLICY</i>
                  </a>
                </li>
                <li>
                  <a
                    href="#terms"
                    className="active"
                    onClick={this.handleCancellation}
                  >
                    <i className="fa fa-bullhorn active-focus">
                      {" "}
                      CANCELLATION & REFUND POLICY
                    </i>
                  </a>
                </li>
              </ul>
            </div>
            {this.state.clickedTerms === true ? (
              <div style={{ padding: "20px 0" }}>
                <p className="font-bold">
                  PLEASE READ THESE TERMS CAREFULLY BEFORE USING THIS SERVICE
                </p>
                <h2 className="heading">1. Introduction</h2>
                <div>
                  <ol>
                    <li>
                      Your use of Coense Solutions Pvt. Ltd. (henceforth Coense)
                      Product, Software, Online Service and ProjectHeena.
                      (referred to as the offering in this document and excluding
                      any offering provided to you by Coense under a separate
                      written agreement) is subject to these terms, without
                      prejudice to any additional terms which may be part of an
                      agreement specific to the service you wish to avail of. Your
                      agreement with Coense will always include, at a minimum, the
                      terms and conditions set out in this document.
                      ProjectHeena.com does not sell its own products or services
                      directly but merely acts as a mediatior between other
                      parties and service – providers.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  2. Accepting the Terms
                </h2>
                <div>
                  <ol>
                    <li>
                      Coense shall interact with customers / clients through
                      E-mail, Form on our website and Phone call, Skype or similar
                      service. By clicking a button or checkbox on a website and
                      by replying to our e-mails you accept and agree to the
                      terms, where this option is made available to you by Coense
                      in the user interface for any offering.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      In order to use the offering, you must first agree to the
                      terms. You may not use the offering if you do not accept the
                      terms.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  3. Provision of the offering by Coense
                </h2>
                <div>
                  <ol>
                    <li>
                      Coense will attempt to continuously innovate in order to
                      provide the best possible offering for its users. You
                      acknowledge and agree that the form and nature of the
                      offering which Coense provides may change from time to time.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You acknowledge and agree that Coense may stop (permanently
                      or temporarily) providing the offering (or any features
                      within the offering) to you or to users generally at
                      Coense's sole discretion.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You acknowledge and agree that if Coense disables access to
                      your account or login route, you may be prevented from
                      accessing the offering, your account details or any files or
                      other content which is contained in your account.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You acknowledge and agree that while Coense may not
                      currently limit your use of the offering in any way, it may
                      do so if that use hinders with the ability of Coense to
                      carry on its operations or the ability of other customers to
                      use the offering.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      The supply of the offering will be subject to your making
                      payments, if any, at the regular intervals as stated in the
                      contract for the service. In the event that your account is
                      terminated or access is disabled, or the service is
                      permanently or temporarily stopped, Coense's liability shall
                      extend only to forfeiting the outstanding amount due for the
                      current billing cycle. Coense be made liable or called on to
                      repay any amount paid during any previous billing cycle.
                    </li>

                    <li style={{ marginTop: "-4%" }}>
                      Coense shall not be responsible for any loss or damage
                      caused by a modification of the features, limitation of use
                      of the offering or the discontinuation altogether thereof.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  4. Use of the offering by you
                </h2>
                <div>
                  <ol>
                    <li>
                      In order to access certain offering, you may be required to
                      provide information about yourself as part of the
                      registration process for the service, or for your continued
                      use of the offering. You agree that any registration
                      information you give to Coense will always be accurate,
                      correct and up to date. Coense can share customer/ client
                      information with Employees and agents, any company which you
                      acquire / acquires you, to the Government or law enforcement
                      agencies (if officially requested or required by Order,
                      Notification, Statute or Court Order) and with anyone else,
                      with the consent of the customer / client..
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You agree to use the offering only for your personal,
                      non-commercial use and for purposes that are permitted by
                      (a) the terms and (b) any applicable law, regulation or
                      guidelines.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You agree not to access (or attempt to access) any of the
                      offering by any means other than through the interface that
                      is provided by Coense, unless you have been specifically
                      allowed to do so in a separate agreement with Coense You
                      specifically agree not to access (or attempt to access) any
                      of the offering through any automated means (including
                      through the use of scripts or web crawlers).
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You agree that you will not engage in any activity that
                      interferes with or disrupts the offering (or the servers and
                      networks which are connected to the offering).
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      Unless you have been specifically permitted to do so in a
                      separate agreement with Coense, you agree that you will not
                      reproduce, duplicate, copy, sell, trade or resell the
                      offering for any purpose.
                    </li>

                    <li style={{ marginTop: "-4%" }}>
                      You agree that you are solely responsible for (and that
                      Coense has no responsibility to you or to any third party
                      for) any breach of your obligations under the Terms and for
                      the consequences (including any loss or damage which Coense
                      may suffer) of any such breach.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  5. Your passwords and account security
                </h2>
                <div>
                  <ol>
                    <li>
                      You agree and understand that you are responsible to Coense
                      and to third parties for maintaining the confidentiality of
                      passwords associated with any account you use to access the
                      offering. You will be solely responsible for all activities
                      that occur under your account.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      Coense will take due care to ensure the confidentiality of
                      your credentials. You agree and acknowledge that in the very
                      unlikely event of your credentials becoming known to a third
                      party on account of an intrusion into Coense database,
                      Coense shall not be made liable for the resulting damages.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      If you become aware of any unauthorized use of your password
                      or of your account, you agree to notify the concerned
                      official at Coense immediately.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  6. Content in the offering
                </h2>
                <div>
                  <ol>
                    <li>
                      You understand that all information (such as data files,
                      written text, computer software, or images) which you may
                      have access to as part of, or through your use of, the
                      offering are the sole responsibility of the person from
                      which such content originated.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      Furthermore, Coense is not in any manner responsible for
                      assuring the veracity of the materials provided by it’s
                      members and entities. Although, Coense conducts a rough
                      background check it will not in any way be held responsible
                      for the information provided on it’s website. By agreeing to
                      the Terms and Services provided herein, you hereby exempt
                      Coense from any such liability arising from your perusal of
                      such information. Coense will also not be held responsible
                      for any discrepancy that arises between you and it’s other
                      members.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You should be aware that content presented to you as part of
                      the offering, including but not limited to advertisements
                      and promotional material of Coense or other companies, is
                      protected by intellectual property rights which are owned by
                      Coense, or the sponsors or advertisers who provide that
                      content to Coense (or by other persons or companies on their
                      behalf). You may not modify, rent, lease, loan, sell,
                      distribute, copy or create derivative work based on this
                      content (either in whole or in part) unless you have been
                      specifically told that you may do so by Coense or by the
                      owners of that content, in a separate agreement. Any content
                      being disseminated using Coense's sales network or the
                      product, service or platform may be pre-screened, reviewed,
                      flagged, filtered, modified or simply refused or removed.
                      Any spam or pornographic material and / or any illegal
                      content will be immediately deleted and we reserve the right
                      to take appropriate legal action.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You agree that you are solely responsible for (and that
                      Coense has no responsibility to you or to any third party
                      for) any content that you create, transmit or display while
                      using the offering or for the consequences of your actions
                      (including any loss or damage which Coense may suffer) by
                      doing so.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You understand that by using the offering you may be exposed
                      to content of other users that you may find offensive,
                      indecent or objectionable and that, in this respect, you use
                      the offering at your own risk. Coense shall not be made
                      responsible for any repugnant content circulated on its
                      offering by other users.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      On noticing any such content, it is your duty to bring it to
                      the attention of Coense officials immediately, who will take
                      due care to delete it from the offering.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  7. Intellectual Property rights
                </h2>
                <div>
                  <ol>
                    <li>
                      You acknowledge and agree that Coense (or Coense's
                      licensors) owns all legal right, title and interest in and
                      to the offering, including any intellectual property rights
                      which subsist in the offering (whether those rights happen
                      to be registered or not, and wherever in the world those
                      rights may exist). You further acknowledge that the offering
                      may contain information which is designated confidential by
                      Coense and that you shall not disclose such information
                      without Coense's prior written consent.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      Unless Coense has otherwise agreed in writing, nothing in
                      the terms gives you a right to use any of Coense's trade
                      names, trademarks, service marks, logos, domain names, and
                      other distinctive brand features. You additionally agree
                      that in using the offering, you will not use any trade mark,
                      service mark, trade name, logo of any company or
                      organization in a way that is likely or intended to cause
                      confusion about the owner or authorized user of such marks,
                      names or logos. Corporates are entitled to use
                      ProjectHeena’s logo for promotion of their Corporate Social
                      Responsibilities (CSR). However, prior to doing so they must
                      in no way represent to the viewers of such marketing
                      material that they are in no manner owner of such material
                      as is distributed.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      Coense acknowledges and agrees that it obtains no right,
                      title or interest from you (or your licensors) under these
                      terms in or to any content that you submit, post, transmit
                      or display on, or through, the offering, including any
                      intellectual property rights which subsist in that content
                      (whether those rights happen to be registered or not, and
                      wherever in the world those rights may exist). Unless you
                      have agreed otherwise in writing with Coense, you agree that
                      you are responsible for protecting and enforcing those
                      rights and that Coense has no obligation to do so on your
                      behalf.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      Coense reserves its right to use any of your Company logos
                      or any other Company owned graphic symbol, logo, or icon on
                      or in connection with web sites, products, packaging,
                      manuals, promotional/advertising materials, or for any other
                      purpose in pursuance to this agreement. By entering into
                      this agreement with Coense, you hereby vest with Coense your
                      rights over such trademarks and logos for purely
                      advertisement and show-casing purposes on the
                      ProjectHeena.com
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  8. License from Coense
                </h2>
                <div>
                  <ol>
                    <li>
                      Coense gives you a Personal Non-Exclusive license to use the
                      software provided to you by Coense as part of the offering
                      as provided to you by Coense. This license is for the sole
                      purpose of enabling you to use and enjoy the benefit of the
                      offering as provided by Coense, in the manner permitted by
                      these terms.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You are not entitled to commercially exploit, either
                      directly or by sale, or transfer commercially or profit from
                      the offering and products without prior written express
                      agreement from Coense.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You may not (and you may not permit anyone else to) copy,
                      modify, create a derivative work of, reverse engineer,
                      decompile or otherwise attempt to extract the source code of
                      the software or any part thereof.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      Unless Coense has given you specific written permission to
                      do so, you may not assign (or grant a sub-license of) your
                      rights to use the service or otherwise transfer any part of
                      your rights to use the service.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  9. Ending your relationship with Coense
                </h2>
                <div>
                  <ol>
                    <li>
                      The Terms will continue to apply until terminated by either
                      you or Coense as set out below.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      If you wish to terminate your agreement with Coense then you
                      will have to formally withdraw your membership by means of
                      sending a physical mail to the premises of the Coense
                      office. Withdrawal can’t be done by the means of a mere
                      email. Furthermore, removal of the user profile and
                      information from Coense’s website will be done in a
                      reasonable time period as stipulated by Coense.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      Coense may at any time, terminate its legal agreement with
                      you if:
                    </li>

                    <ol>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        you have breached any provision of the terms (or have
                        acted in manner which clearly shows that you do not intend
                        to, or are unable to comply with the provisions of the
                        terms); or
                      </li>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        Coense is required to do so by law (for example, where the
                        provision of the offering to you is, or becomes, unlawful)
                        Furthermore, Coense reserves its rights to remove any
                        person from the website on any grounds that it finds to be
                        justifiable.
                      </li>
                    </ol>

                    <li>
                      When these terms come to an end, all of the legal rights,
                      obligations and liabilities that you and Coense are subject
                      to in relation to the obligation to maintain confidentiality
                      or such other legal rights, obligations and liabilities
                      which are expressed to continue indefinitely, shall be
                      unaffected by this cessation.
                    </li>

                    <li>
                      In case a corporate entity decides to terminate its
                      agreement with ProjectHeena.com or any other brand, Coense
                      reserves its rights to completely terminate access by such
                      entity to its services. Further, Coense also reserves it’s
                      rights to have a public log of previously available data
                      about the entity for public reference. Coense can make any
                      required changes as per its discretion that it exercises as
                      and when the need to apply such discretion arises. In
                      absence of any inclination on the corporate entity to
                      disclose that it is no longer associated with the services
                      that Coense provides, then Coense reserves it’s rights to
                      take measures and inform user base of your termination from
                      the services. Furthermore, the members of the corporate
                      entity can still continue to be part of the ProjectHeena.com
                      franchise regardless of such termination. However, upon such
                      termination of such corporate entity, the hours credited to
                      each member will not be accredited to such corporate entity.
                    </li>
                  </ol>
                </div>
                <hr
                  style={{
                    marginTop: "-4%",
                    marginBottom: "20px",
                    border: "0",
                    borderTop: "1px solid #eee",
                  }}
                />

                <p style={{ marginTop: "10vh" }}>
                  In case the business entity decides
                </p>

                <h2 className="heading" style={{ marginTop: "1vh" }}>
                  10. EXCLUSION OF WARRANTIES
                </h2>
                <div>
                  <ol>
                    <li>
                      YOU EXPRESSLY UNDERSTAND AND AGREE THAT YOUR USE OF THE
                      OFFERING IS AT YOUR SOLE RISK AND THAT THE OFFERING ARE
                      PROVIDED AS IS AND AS AVAILABLE.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      IN PARTICULAR, COENSE DOES NOT REPRESENT OR WARRANT TO YOU
                      THAT:
                    </li>
                    <ol>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        YOUR USE OF THE OFFERING WILL MEET YOUR REQUIREMENTS,
                      </li>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        YOUR USE OF THE OFFERING WILL BE UNINTERRUPTED, TIMELY,
                        SECURE OR FREE FROM ERROR, INCLUDING SPECIFICALLY FROM
                        SERVER DOWNTIME,{" "}
                      </li>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        ANY INFORMATION OBTAINED BY YOU AS A RESULT OF YOUR USE OF
                        THE OFFERING WILL BE ACCURATE OR RELIABLE, AND{" "}
                      </li>
                    </ol>

                    <li style={{ marginTop: "-4%" }}>
                      ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE
                      USE OF THE OFFERING IS DONE AT YOUR OWN DISCRETION AND RISK
                      AND COENSE WILL NOT BE RESPONSIBLE FOR ANY DAMAGE TO YOUR
                      COMPUTER SYSTEM OR OTHER DEVICE OR LOSS OF DATA THAT RESULTS
                      FROM THE DOWNLOAD OF ANY SUCH MATERIAL.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED
                      BY YOU FROM COENSE OR THROUGH OR FROM THE OFFERING SHALL
                      CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THE TERMS.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      COENSE FURTHER EXPRESSLY DISCLAIMS ALL WARRANTIES AND
                      CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED,
                      INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES AND
                      CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
                      PURPOSE AND NON-INFRINGEMENT.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "1vh" }}>
                  11. LIMITATION OF LIABILITY
                </h2>
                <div>
                  <ol>
                    <li>
                      SUBJECT TO OVERALL PROVISION IN CLAUSE 11 ABOVE, YOU
                      EXPRESSLY UNDERSTAND AND AGREE THAT COENSE SHALL NOT BE
                      LIABLE TO YOU FOR:
                    </li>
                    <ol>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL
                        OR EXEMPLARY DAMAGES WHICH MAY BE INCURRED BY YOU, HOWEVER
                        CAUSED AND UNDER ANY THEORY OF LIABILITY. THIS SHALL
                        INCLUDE, BUT NOT BE LIMITED TO, ANY LOSS OF PROFIT
                        (WHETHER INCURRED DIRECTLY OR INDIRECTLY), ANY LOSS OF
                        GOODWILL OR BUSINESS REPUTATION, ANY LOSS OF DATA
                        SUFFERED, COST OF PROCUREMENT OF SUBSTITUTE GOODS OR
                        OFFERING, OR OTHER INTANGIBLE LOSS;
                      </li>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        ANY LOSS OR DAMAGE WHICH MAY BE INCURRED BY YOU, INCLUDING
                        BUT NOT LIMITED TO LOSS OR DAMAGE AS A RESULT OF:{" "}
                      </li>
                      <ol>
                        <li
                          style={{
                            listStyleType: "upper-roman",
                            marginTop: "-4%",
                          }}
                        >
                          ANY CHANGES WHICH COENSE MAY MAKE TO THE OFFERING, OR
                          FOR ANY PERMANENT OR TEMPORARY CESSATION IN THE
                          PROVISION OF THE OFFERING (OR ANY FEATURES WITHIN THE
                          OFFERING);
                        </li>
                        <li
                          style={{
                            listStyleType: "upper-roman",
                            marginTop: "-4%",
                          }}
                        >
                          THE DELETION OF, CORRUPTION OF, OR FAILURE TO STORE, ANY
                          CONTENT AND OTHER COMMUNICATIONS DATA MAINTAINED OR
                          TRANSMITTED BY OR THROUGH YOUR USE OF THE OFFERING;{" "}
                        </li>
                        <li
                          style={{
                            listStyleType: "upper-roman",
                            marginTop: "-4%",
                          }}
                        >
                          ANY INFORMATION OBTAINED BY YOU AS A RESULT OF YOUR USE
                          OF THE OFFERING WILL BE ACCURATE OR RELIABLE, AND{" "}
                        </li>
                      </ol>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        ANY INFORMATION OBTAINED BY YOU AS A RESULT OF YOUR USE OF
                        THE OFFERING WILL BE ACCURATE OR RELIABLE, AND{" "}
                      </li>
                    </ol>

                    <li style={{ marginTop: "-4%" }}>
                      IN PARTICULAR, COENSE DOES NOT REPRESENT OR WARRANT TO YOU
                      THAT:
                    </li>
                    <ol>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        YOUR USE OF THE OFFERING WILL MEET YOUR REQUIREMENTS,
                      </li>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        YOUR USE OF THE OFFERING WILL BE UNINTERRUPTED, TIMELY,
                        SECURE OR FREE FROM ERROR, INCLUDING SPECIFICALLY FROM
                        SERVER DOWNTIME,{" "}
                      </li>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        YOUR FAILURE TO PROVIDE COENSE WITH ACCURATE ACCOUNT
                        INFORMATION;{" "}
                      </li>
                      <li
                        style={{ listStyleType: "upper-alpha", marginTop: "-4%" }}
                      >
                        YOUR FAILURE TO KEEP YOUR PASSWORD OR ACCOUNT DETAILS
                        SECURE AND CONFIDENTIAL;{" "}
                      </li>
                    </ol>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  12. Other content to which references are made, links are
                  provided, etc.
                </h2>
                <div>
                  <ol>
                    <li>
                      The offering may include hyperlinks to other web sites or
                      content or resources. Coense may have no control over any
                      web sites or resources which are provided by companies or
                      persons other than Coense.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You acknowledge and agree that Coense is not responsible for
                      the availability of any such external sites or resources,
                      and does not endorse any advertising, products or other
                      materials on or available from such web sites or resources.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You acknowledge and agree that Coense is not liable for any
                      loss or damage which may be incurred by you as a result of
                      the availability of those external sites or resources, or as
                      a result of any reliance placed by you on the completeness,
                      accuracy or existence of any advertising, products or other
                      materials on, or available from, such web sites or
                      resources.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  13. Changes to the Terms
                </h2>
                <div>
                  <ol>
                    <li>
                      Coense may make changes to the terms from time to time. The
                      same will be communicated to you, on ProjectHeena.com. When
                      these changes are made, Coense shall make the amended copy
                      of the terms available in an accessible location.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You understand and agree that if you use the offering after
                      the date on which the terms have changed, or after the date
                      by which you have to respond as per these terms has passed,
                      Coense will treat your use as acceptance of the amended
                      terms.
                    </li>
                  </ol>
                </div>

                <h2 className="heading" style={{ marginTop: "-4vh" }}>
                  14. General legal terms
                </h2>
                <div>
                  <ol>
                    <li>
                      Sometimes when you use the offering, you may (as a result
                      of, or through your use of the offering) use a service or
                      download a piece of software, or purchase goods, which are
                      provided by another person or company. Your use of these
                      other offering, software or goods may be subject to separate
                      terms between you and the company or person concerned.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      You agree that if Coense does not exercise or enforce any
                      legal right or remedy which is contained in the terms (or
                      which Coense has the benefit of under any applicable law),
                      this will not be taken to be a formal waiver of Coense's
                      rights and that those rights or remedies will still be
                      available to Coense.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      If any court of law, having the jurisdiction to decide on
                      this matter, rules that any provision of these terms is
                      invalid, then that provision will be removed from the terms
                      without affecting the rest of the terms. The remaining
                      provisions of the terms will continue to be valid and
                      enforceable.
                    </li>
                    <li style={{ marginTop: "-4%" }}>
                      The terms, and your relationship with Coense under the
                      terms, shall be governed by the laws of India and
                      specifically the Courts in the City of Chennai shall have
                      jurisdiction without reference to conflict of laws
                      principles. You and Coense agree that any dispute will be
                      referred to arbitration, with the arbitral panel consisting
                      of three arbitrators, one arbitrator being chosen by each
                      party and a third being jointly appointed by the two
                      arbitrators so chosen.
                    </li>
                  </ol>
                </div>
              </div>
            ) : null}

            {this.state.clickedPrivacy === true ? (
              <div style={{ margin: "20px 0" }}>
                <p style={{ padding: "0" }}>Dear do gooder,</p>
                <p style={{ padding: "0" }}>
                  We appreciate your time and partnership in making a significant
                  contribution back to society via technology.
                </p>
                <p style={{ padding: "0" }}>
                  Our vision is to build tools of collaboration for change makers
                  and scale their work in the most convenient way possible. While
                  using this application certain data is shared for collaboration,
                  do note we take your privacy very seriously and do not access or
                  share your critical information unless required for the activity
                  you opt in for or basis your permission.
                </p>
                <p style={{ padding: "0" }}>
                  We view protection of your privacy as a very important
                  principle. We understand clearly that You and Your Personal
                  Information is one of our most important assets. We store and
                  process Your Information including any sensitive financial
                  information collected (as defined under the Information
                  Technology Act, 2000), if any, on computers that may be
                  protected by physical as well as reasonable technological
                  security measures and procedures in accordance with Information
                  Technology Act 2000 and Rules there under. If you object to our
                  current Privacy Policy and your information being transferred or
                  used in this way please do not use Website.
                </p>
                <p style={{ padding: "0" }}>
                  In case of any concern please feel free to reach out to us on
                  team@projectheena.com. We will remediate any issues on top
                  priority and assure you of great experience.
                </p>
                <p style={{ padding: "0" }}>Thanks</p>
              </div>
            ) : null}

            {this.state.clickedCancellation === true ? (
              <div style={{ margin: "20px 0" }}>
                <p style={{ padding: "0" }}>
                  We appreciate your time and trust in making your part of the
                  contribution on our platform.
                </p>
                <p style={{ padding: "0" }}>
                  To ensure completely transparent transactions and build rock
                  solid trust we will provide you in advance all the relevant
                  details about the organization receiving funds from you.
                </p>
                <p style={{ padding: "0" }}>
                  We will also regularly follow with the receiver and provide
                  relevant updates to you if any.
                </p>
                <p style={{ padding: "0" }}>
                  However do note cancelling or refunding the contribution won't
                  be possible from our end as the contribution goes directly for
                  the relevant project /beneficiary. We suggest you to note all
                  the details before making a payment and incase of any doubts
                  reach out to the said organization or directly to us at
                  team@projectheena.com
                </p>
              </div>
            ) : null}
          </Card>
        </div>


      </>
    );
  }
}

export default TermsandConditions;
