/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import "../../index.scss";

import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import {
    Col,

    FormControl,
    Card,
} from "react-bootstrap";
import CustomButton from "../components/CustomButton";
import "bootstrap/dist/css/bootstrap.min.css";
import TextEditor from "../components/textEditor";
import DatePicker from "react-datepicker";
// import PlacesAutoComplete from "../components/PlacesAutoComplete";
// import CheckboxTable from "../components/checkboxTable";

const EditMilestone = () => {
    const [startDate, setStartDate] = useState();
    // eslint-disable-next-line no-unused-vars
    const [endDate, setEndDate] = useState(new Date());
    // const [count, setCount] = useState(0);
    // const [countAwards, setCountAward] = useState(0);
    // var maxCount = 256;
    // var maxCountAwards = 1000;

    return (
        <>
            <div className="container">
                <div style={{ marginBottom: "30px" }}>
                    <h2 className="font-bold">Edit Milestone</h2>

                </div>
                <Card style={{ padding: "30px " }}>
                    <Form>
                        <Form.Group>
                            <Form.Row>
                                <Form.Label
                                    column="lg"
                                    lg={2}
                                    className="form-label-custom p-r-15"
                                >
                                    Name
                                </Form.Label>
                                <Col className="form-input-align-center">
                                    <Form.Control size="lg" type="text" className="green-focus" />
                                </Col>
                            </Form.Row>
                            <hr className="form-horizontal-line" />

                            <Form.Row>
                                <Form.Label
                                    column="lg"
                                    lg={2}
                                    className="form-label-custom p-r-15"
                                >
                                    Description
                                </Form.Label>
                                <Col>
                                    <TextEditor />
                                </Col>
                            </Form.Row>
                            <hr className="form-horizontal-line" />
                            <Form.Row>
                                <Form.Label
                                    column="lg"
                                    lg={2}
                                    className="form-label-custom p-r-15"
                                >
                                    Actual start date
                                </Form.Label>
                                <Col>
                                    <InputGroup>
                                        <DatePicker
                                            selected={startDate}
                                            selectsStart
                                            startDate={startDate}
                                            endDate={endDate}
                                            onChange={(date) => setStartDate(date)}
                                            className="green-focus"
                                        />
                                        <InputGroup.Append>
                                            {/* <i class="fas fa-calendar-alt"></i> */}
                                        </InputGroup.Append>
                                    </InputGroup>
                                </Col>
                            </Form.Row>
                            <hr className="form-horizontal-line" />
                            <Form.Row>
                                <Form.Label
                                    column="lg"
                                    lg={2}
                                    className="form-label-custom p-r-15"
                                >
                                    Estimated date
                                </Form.Label>
                                <Col>
                                    <InputGroup>
                                        <DatePicker
                                            selected={startDate}
                                            selectsStart
                                            startDate={startDate}
                                            endDate={endDate}
                                            onChange={(date) => setStartDate(date)}
                                            className="green-focus"
                                        />
                                        <InputGroup.Append>
                                            {/* <i class="fas fa-calendar-alt"></i> */}
                                        </InputGroup.Append>
                                    </InputGroup>
                                </Col>
                            </Form.Row>
                            <hr className="form-horizontal-line" />
                            <Form.Row>
                                <Form.Label
                                    column="lg"
                                    lg={2}
                                    className="form-label-custom p-r-15"
                                >
                                    Estimated Cost
                                </Form.Label>
                                <Col>
                                    <InputGroup>
                                        <InputGroup.Prepend>
                                            <InputGroup.Text
                                                style={{ marginRight: "0", background: "transparent" }}
                                                size="lg"
                                                className="green-focus"
                                            >
                                                <i class="fas fa-rupee-sign"></i>
                                            </InputGroup.Text>
                                        </InputGroup.Prepend>

                                        <FormControl type="number" size="lg" className="green-focus" />
                                    </InputGroup>
                                </Col>
                            </Form.Row>
                            <hr className="form-horizontal-line" />
                            <Form.Row>
                                <Form.Label
                                    column="lg"
                                    lg={2}
                                    className="form-label-custom p-r-15"
                                >
                                    Person/Partner Responsible
                                </Form.Label>
                                <Col className="form-input-align-center">
                                    <Form.Control size="lg" type="text" className="green-focus" />
                                </Col>
                            </Form.Row>
                            <hr className="form-horizontal-line" />
                            <div style={{ marginLeft: '17%' }}>
                                <CustomButton content={`Edit Milestone`} />
                                <button
                                    className="p-2"
                                    style={{
                                        backgroundColor: "#c2c2c2",
                                        border: "1px solid #c2c2c2",
                                        color: "white",
                                        marginLeft: '10px'
                                    }}
                                >
                                    Cancel
                                </button>
                            </div>













                        </Form.Group>
                    </Form>
                </Card>
            </div>

        </>
    );
};

export default EditMilestone;
