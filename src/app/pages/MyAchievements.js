import React from 'react'
import { Table } from "react-bootstrap"
import { Link } from 'react-router-dom'
const MyAchievements = () => {
    const achievements = [
        {
            image: "http://projectheena.in/uploads/projects/20138866546018/thb/default.jpg",
            name: "Volunteer at the Standard Chartered Mumbai Marathon",
            hours_credited: 6,
            testimonial: "Thank- you so much for helping LIFE Trust make the cheering zone lively and fun. We really appreciate your time, effort and enthusiasm. You have been part of our mission of changing lives, changing destines. Together in Service, Team Life Trust- Tasnim Motorwala.",
            date: "12th, Dec 2021",
        },
        {
            image: "http://projectheena.in/uploads/projects/20138866546018/thb/default.jpg",
            name: "Volunteer at the Standard Chartered Mumbai Marathon",
            hours_credited: 6,
            testimonial: "Thank- you so much for helping LIFE Trust make the cheering zone lively and fun. We really appreciate your time, effort and enthusiasm. You have been part of our mission of changing lives, changing destines. Together in Service, Team Life Trust- Tasnim Motorwala.",
            date: "12th, Dec 2021",
        },
        {
            image: "http://projectheena.in/uploads/projects/20138866546018/thb/default.jpg",
            name: "Volunteer at the Standard Chartered Mumbai Marathon",
            hours_credited: 6,
            testimonial: "Thank- you so much for helping LIFE Trust make the cheering zone lively and fun. We really appreciate your time, effort and enthusiasm. You have been part of our mission of changing lives, changing destines. Together in Service, Team Life Trust- Tasnim Motorwala.",
            date: "12th, Dec 2021",
        }
    ]
    return (
        <div className="container">
            <h2 className="font-bold" style={{ padding: "0", margin: "20px 0 10px 0" }}>
                Your Achievements
            </h2>
            <div className="box-ibox-title">
                <Table responsive="xl" className='mb-5'>
                    <thead>
                        <tr>
                            <td className='title-gray'>Name</td>
                            <td className='title-gray'>Hours Credited</td>
                            <td className='title-gray'>Testimonial</td>
                            <td className='title-gray'> Date</td>
                        </tr>
                    </thead>
                    <tbody>
                        {achievements.map((x, index) => (
                            <tr>
                                <td className='title-gray'>
                                    <div className='d-flex'>
                                        <img src={x.image} alt="" width={70} className='px-2'></img>
                                        <span><Link to="#" className="basecolor">{x.name}</Link></span>
                                    </div>
                                </td>
                                <td className='title-gray'> {x.hours_credited}</td>
                                <td className='title-gray'> {x.testimonial}</td>
                                <td className='title-gray'>{x.date}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default MyAchievements
