import React, { useState } from "react";
import {
  Tab,
  Tabs,
  Nav,
  Row,
  Col,
  InputGroup,
  FormControl, Card
} from "react-bootstrap";
import FormCard from "../components/FormCard";
import Question from "../components/Question";
import SurveyGeoTag from "./SurveyGeoTag";
import SurveyImage from "./SurveyImage";
import SurveyLikert from "./SurveyLikert";
import SurveyNoRepeat from "./SurveyNoRepeat";
import SurveyPhone from "./SurveyPhone";
import SurveyScale from "./SurveyScale";
import SurveyVideo from "./SurveyVideo";
import GroupNumber from "./SurveyGroupNumber";
import NoRepeat from './SurveyNoRepeat';
import GroupChoice from "./SurveyGroupChoice";


// import {Link} from 'react-scroll';

const MediaContact = () => {
  const [chosen, setChosen] = useState("Details");

  return (
    <>
      <Tab.Container id="left-tabs-example" defaultActiveKey="first">
        <Row style={{ marginTop: "7%" }}>
          <Col sm={3}>
            <Nav variant="pills" className="flex-column">
              <Nav.Item>
                <Nav.Link eventKey="first">Image</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="second">Image and Geo Tag </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="third" >Phone</Nav.Link>
              </Nav.Item>

              <Nav.Item>
                <Nav.Link eventKey="fourth" >Email</Nav.Link>

              </Nav.Item>

              <Nav.Item>
                <Nav.Link eventKey="fifth" >Audio</Nav.Link>

              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="sixth" >Video</Nav.Link>

              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="seventh" >File Upload</Nav.Link>
              </Nav.Item>

              <Nav.Item>
                <Nav.Link eventKey="eighth" >Likert</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="ninth" >Scale</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="tenth" >Rating</Nav.Link>
              </Nav.Item>

              <Nav.Item>
                <Nav.Link eventKey="eleventh" >Grop (No Repeat)</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="twelfth" >Group (Number)</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="thirteenth" >Group (Choice)</Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>
          <Col sm={9}>
            <Tab.Content style={{ backgroundColor: "#f0f0f0", height: "100%" }}>
              <Tab.Pane eventKey="first" style={{ opacity: "100" }}>
                <SurveyImage />
              </Tab.Pane>
              <Tab.Pane eventKey="second" style={{ opacity: "100" }}>
                <SurveyGeoTag />
              </Tab.Pane>
              <Tab.Pane eventKey="third" style={{ opacity: "100" }}>
                <SurveyPhone />
              </Tab.Pane>
              <Tab.Pane eventKey="fourth" style={{ opacity: "100" }}>
                <Card className="p-5">
                  <Question />
                </Card>

              </Tab.Pane>
              <Tab.Pane eventKey="fifth" style={{ opacity: "100" }}>
                <Card className="p-5">
                  <Question />
                </Card>
              </Tab.Pane>
              <Tab.Pane eventKey="sixth" style={{ opacity: "100" }}>
                <SurveyVideo />
              </Tab.Pane>
              <Tab.Pane eventKey="seventh" style={{ opacity: "100" }}>
                <Card className="p-5">
                  <Question />
                </Card>
              </Tab.Pane>
              <Tab.Pane eventKey="eighth" style={{ opacity: "100" }}>
                <SurveyLikert />
              </Tab.Pane>
              <Tab.Pane eventKey="ninth" style={{ opacity: "100" }}>
                <SurveyScale />
              </Tab.Pane>
              <Tab.Pane eventKey="tenth" style={{ opacity: "100" }}>
                <Card className="p-5">
                  <Question />
                </Card>
              </Tab.Pane>
              <Tab.Pane eventKey="eleventh" style={{ opacity: "100" }}>
                <NoRepeat />
              </Tab.Pane>
              <Tab.Pane eventKey="twelfth" style={{ opacity: "100" }}>
                <GroupNumber />

              </Tab.Pane>
              <Tab.Pane eventKey="thirteenth" style={{ opacity: "100" }}>
                <GroupChoice />
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    </>
  );
};
export default MediaContact;
