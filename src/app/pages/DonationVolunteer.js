import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';

class DonationVolunteer extends React.Component {
    render() {
        return (
            <>
                <Card style={{ marginBottom: '5%' }}>
                    <Row className="p-5">
                        <Col md={3} >
                            <p style={{ textAlign: 'center' }}>Total Tasks</p>
                            <h1 style={{ textAlign: 'center' }}>31</h1>
                        </Col>
                        <Col md={3}>
                            <p style={{ textAlign: 'center' }}>Volunteers Engaged</p>
                            <h1 style={{ textAlign: 'center' }}>31</h1>
                        </Col>
                        <Col md={3}>
                            <p style={{ textAlign: 'center' }}>Hours Contributed</p>
                            <h1 style={{ textAlign: 'center' }}>308</h1>
                        </Col>
                        <Col md={3}>
                            <p style={{ textAlign: 'center' }}>Possible Engagement</p>
                            <h1 style={{ textAlign: 'center' }}>572887</h1>
                        </Col>
                    </Row>
                </Card>

                <Card>
                    <h5 style={{ marginTop: '2%', marginLeft: '2%' }}>Top Volunteers</h5>
                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                    <Row className="p-5">
                        <Col md={3} >
                            <img src={Images.user} style={{ borderRadius: '50%', width: '70%' }}></img>
                            <h6 style={{ marginLeft: '24%', marginTop: '8%' }}>Test</h6>
                        </Col>
                        <Col md={3}>
                            <img src={Images.user} style={{ borderRadius: '50%', width: '70%' }}></img>
                            <h6 style={{ marginLeft: '24%', marginTop: '8%' }}>Test</h6>

                        </Col>
                        <Col md={3}>
                            <img src={Images.user} style={{ borderRadius: '50%', width: '70%' }}></img>
                            <h6 style={{ marginLeft: '24%', marginTop: '8%' }}>Test</h6>
                        </Col>
                        <Col md={3}>
                            <img src={Images.user} style={{ borderRadius: '50%', width: '70%' }}></img>
                            <h6 style={{ marginLeft: '24%', marginTop: '8%' }}>Test</h6>
                        </Col>
                    </Row>
                </Card>


                <Card style={{ marginTop: '5%' }}>
                    <h5 style={{ marginTop: '2%', marginLeft: '2%' }}>Engagement Stats</h5>
                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                    <Row className="px-5">
                        <Col md={2}>
                            <img src='http://projectheena.in/assets/img/team-dashboard/team-engagement.png'></img>
                        </Col>
                        <Col className="pl-0" md={10}>
                            <p>This section highlights all the engagement statistics available on the platform. Certain activities are time bound, some are continuous and some might be initiated by employees themselves</p>
                        </Col>
                    </Row>
                    <Row className="px-5">
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Sr. No</th>
                                    <th>Heading</th>
                                    <th>Count</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Total Open Tasks</td>
                                    <td>33</td>

                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Total Open Tasks</td>
                                    <td>33</td>

                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Total Open Tasks</td>
                                    <td>33</td>

                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Total Open Tasks</td>
                                    <td>33</td>

                                </tr>

                            </tbody>
                        </Table>

                    </Row>

                </Card>

            </>
        )
    }
}
export default DonationVolunteer;