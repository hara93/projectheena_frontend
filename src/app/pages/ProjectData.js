import React from 'react';
import { Col, Row, Table } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import CustomButtonOutline from '../components/CustomButtonOutline';
import * as ReactBootstrap from 'react-bootstrap';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
// import {
//     FaTv
// } from "react-icons/fa";

class ProjectData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            isAddOpen: false,
            plus: false,
            add: [{}]
        }


    }
    handleAdd = () => {

        this.setState({ add: [...this.state.add, { id: '1' }] })

    }
    handleRemove = (e, index) => {

        const newVal = [...this.state.add]
        newVal.splice(index, 1)
        this.setState({ add: newVal })

    }
    openModal = () => this.setState({ isOpen: true });
    closeModal = () => this.setState({ isOpen: false, isAddOpen: false });
    addModal = () => {
        this.setState({ isAddOpen: true })
    }



    render() {
        return (
            <>
                <Row className="no-gutters">
                    <Col md={12}>
                        <Card className="p-4">
                            <div className="item">
                                <h3 className="csr_heading"><i class="fa fa-tv"></i> Project Critical Data</h3>
                            </div>
                            {/* <h3 style={{ marginBottom:'2vh'}}> <FaTv color="#676a6c"/> Project Critical Data</h3> */}

                            <p>Project Meta Data</p>


                            <Table bordered >
                                <thead>
                                    <tr>

                                        <th> Name</th>
                                        <th>Value</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Project Code</td>
                                        <td>EPDSS0102

                                        </td>


                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <Row>
                                                <Col md={7}>
                                                </Col>
                                                <Col md={5}>
                                                    <div onClick={this.openModal}>

                                                        <CustomButtonOutline content={`Add/Edit Metadata`} />

                                                    </div>
                                                </Col>
                                            </Row>
                                        </td>
                                    </tr>

                                </tbody>
                            </Table>

                            <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                            <div className="item">
                                <h3 className="csr_heading"><i class="fa fa-tv"></i> Project Location Data</h3>
                            </div>


                            <h3 style={{ marginTop: '15vh', marginBottom: '15vh', marginLeft: '23%', color: '#c4c4c4' }}>No Locations Have been allocated by teams yet</h3>

                            <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />

                            <div className="item">
                                <h3 className="csr_heading"><i class="fa fa-tv"></i> Project Beneficiary</h3>
                            </div>


                            <Row>
                                <Col md={11}>
                                    <div className="item">
                                        <h3 className="csr_heading">Target and Actual Beneficiary</h3>
                                    </div>
                                </Col>
                                <Col md={1}>
                                    <div onClick={this.addModal}>
                                        <CustomButtonOutline content={`Add`} />

                                    </div>
                                </Col>
                            </Row>


                            <ReactBootstrap.Modal show={this.state.isOpen} onHide={this.closeModal} style={{ opacity: '1' }}>
                                <ReactBootstrap.Modal.Header closeButton>
                                    <ReactBootstrap.Modal.Title>Write Review</ReactBootstrap.Modal.Title>
                                </ReactBootstrap.Modal.Header>
                                <ReactBootstrap.Modal.Body>
                                    <Table bordered >
                                        <thead>
                                            <tr>

                                                <th> Name</th>
                                                <th>Value</th>
                                                <th></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.add.map(
                                                    (item, index) => (
                                                        <tr>
                                                            <td><input type="text" placeholder="Enter your meta key"></input></td>
                                                            <td><input type="text" placeholder="Enter your meta value"></input></td>
                                                            <td> <button onClick={this.handleAdd} style={{ backgroundColor: 'white', color: '#4caf50', borderRadius: '50%', border: '1px solid #4caf50' }} ><AddIcon /></button>
                                                                <button onClick={(e, index) => { this.handleRemove(e, index) }} style={{ backgroundColor: 'white', color: 'red', borderRadius: '50%', border: '1px solid red' }}><RemoveIcon /></button></td>


                                                        </tr>
                                                    )
                                                )
                                            }



                                        </tbody>
                                    </Table>


                                </ReactBootstrap.Modal.Body>
                                <ReactBootstrap.Modal.Footer>
                                    <div onClick={this.closeModal}>
                                        {/* <ReactBootstrap.Button variant="secondary">Close</ReactBootstrap.Button> */}
                                        <button className="p-2" onClick={this.closeModal} style={{ backgroundColor: '#c2c2c2', border: '1px solid #c2c2c2', color: 'white' }}>Close</button>
                                    </div>
                                    {/* <ReactBootstrap.Button variant="primary">Add meta data</ReactBootstrap.Button> */}
                                    <button className="p-2" onClick={this.closeModal} style={{ backgroundColor: '#4caf4f', border: '1px solid #4caf4f', color: 'white' }}>Add Meta Data</button>

                                </ReactBootstrap.Modal.Footer>
                            </ReactBootstrap.Modal>




                            <ReactBootstrap.Modal show={this.state.isAddOpen} onHide={this.closeModal} style={{ opacity: '1' }}>
                                <ReactBootstrap.Modal.Header closeButton>
                                    <ReactBootstrap.Modal.Title>Collaborate</ReactBootstrap.Modal.Title>
                                </ReactBootstrap.Modal.Header>
                                <ReactBootstrap.Modal.Body>
                                    <p>Only Teams can collaborate with projects.</p>

                                </ReactBootstrap.Modal.Body>
                                <ReactBootstrap.Modal.Footer>
                                    <div onClick={this.closeModal}>
                                        {/* <ReactBootstrap.Button variant="secondary">Close</ReactBootstrap.Button> */}
                                        <button className="p-2" onClick={this.closeModal} style={{ backgroundColor: '#c2c2c2', border: '1px solid #c2c2c2', color: 'white' }}>Close</button>
                                    </div>
                                    {/* <ReactBootstrap.Button variant="primary">Add meta data</ReactBootstrap.Button> */}
                                    <button className="p-2" onClick={this.closeModal} style={{ backgroundColor: '#4caf4f', border: '1px solid #4caf4f', color: 'white' }}>Add Meta Data</button>

                                </ReactBootstrap.Modal.Footer>
                            </ReactBootstrap.Modal>









                        </Card>
                    </Col>

                </Row>
            </>
        )
    }
}

export default ProjectData;