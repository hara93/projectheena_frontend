/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import "../../index.scss";
import DatePicker from "react-datepicker";
// import CustomButton from "../components/CustomButton";
import Card from "react-bootstrap/Card";
import { Form, Col, InputGroup, FormControl } from "react-bootstrap";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
// import TextEditor from "../components/textEditor";
// import TagsInput from "../components/TagInput";
import CheckboxTable from "../components/checkboxTable";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useFormik } from "formik";
import { string, object, number } from "yup";
// import Input from "../components/form/Input";
import axios from "../config/axios";
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/froala_editor.pkgd.min.css";
import FroalaEditorComponent from "react-froala-wysiwyg";
import "froala-editor/js/plugins.pkgd.min.js";
// import ReactTags from "react-tag-autocomplete";
import TagAutocomplete from "../components/tagAutocomplete";
import { connect } from "react-redux";

const CreateProject = (props) => {
  // console.log("props", props.user._id);
  console.log("props", props.location);
  const history = useHistory();
  const dispatch = useDispatch();
  const defaultValues = {
    project_name: "",
    project_duration: "",
    estimated_cost: 0,
    project_contact: "",
    external_url: "",
    project_excerpts: "",
  };
  const validationSchema = object().shape({
    project_name: string().required("Please provide project name"),
    project_duration: number().required("Please provide project duration"),
    estimated_cost: number().required("Please provide estimated cost"),
    project_contact: string()
      .required("Please provide contact details")
      .min(5, "Contact details must be atleast 5 characters long"),
    external_url: string().url("Please enter a valid url"),
    project_excerpts: string(),
  });

  const onSubmit = (values) => {
    values.project_location = location;
    values.project_slug = slug;
    values.project_excerpts = excerpt;
    values.project_desc = desc;
    values.project_start_d = startDate;
    values.project_end_d = endDate;
    // values.tags = tag;
    values.causes_supported = cause.join(",");
    values.project_lat = lat;
    values.project_lng = lng;
    values.project_creator_id = props.user._id;
    values.project_creator_type = 1;
    // axios.post("/project/submitNewProjectData", { ...values }).then(({ data }) => {
    //   console.info(data);
    //   // dispatch(setUserData(data));
    //   history.push("/csr");
    // });
    console.log("values", values);
  };
  const {
    handleChange,
    handleBlur,
    handleSubmit,
    values,
    errors,
    touched,
    initialValues,
  } = useFormik({
    initialValues: defaultValues,
    validationSchema,
    onSubmit,
  });
  const [count, setCount] = useState(0);
  var maxCount = 256;

  var temp_name = values.project_name.split(" ");
  var slug = temp_name.join("-");
  const [startDate, setStartDate] = useState();

  const [endDate, setEndDate] = useState();
  const [location, setLocation] = useState("");
  const [lat, setLat] = useState();
  const [lng, setLng] = useState();
  const [excerpt, setExcerpt] = useState("");
  const [cause, setCause] = useState([]);
  console.log("cause list ", cause);
  const handleExcerpt = (e) => {
    setExcerpt(e.target.value);
  };
  const [desc, setDesc] = useState("");
  const max = 5000;
  const handleModelChange = (e, editor) => {
    const char = e;
    var plainText = char.replace(/<[^>]+>/g, "");

    setDesc(char);
  };

  const config = {
    charCounterMax: 2000,
  };
  const error = {};
  error.name = errors.project_name ? errors.project_name : "no error";
  error.duration = errors.project_duration ? errors.project_duration : "no error";
  error.estimated_cost = errors.estimated_cost ? errors.estimated_cost : "no error";
  error.project_contact = errors.project_contact ? errors.project_contact : "no error";
  error.external_url = errors.external_url ? errors.external_url : "no error";
  error.project_excerpts = errors.project_excerpts ? errors.project_excerpts : "no error";

  const [tag, setTag] = useState([]);
  const tag_array = tag.map((ele) => ele["name"]);
  var temp = [];
  useEffect(() => {
    // console.log("start date", startDate);
    if (tag.length > 3) {
      for (var i = 0; i < 3; i++) {
        temp.push(tag[i]);
      }
      console.log("if");
      tag.splice(0, tag.length);
      setTag(temp);
    }
    // console.log("temp", temp);
    // console.log("tag", tag);
    // console.log(tag_array);
    // console.log("errors", error);
    // console.log("create project", cause);
  });

  return (
    <>
      <div className="container">
        <div>
          <h2 className="font-bold">Create Project</h2>
          <p>
            Projects are an avenue to collaborate with Corporate organizations. Do note approval is
            solely on our discretion.
          </p>
          <p>Please provide as detailed information as possible.</p>
        </div>
        <Card>
          <Card style={{ padding: "30px" }}>
            <form onSubmit={handleSubmit}>
              <Form.Group>
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Project Name
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      name="project_name"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={errors.project_name}
                      touched={touched.project_name}
                      value={values.project_name}
                      placeholder="Enter Project Name"
                      className="green-focus"
                    />
                    {error.project_name && (
                      <span className="text-danger">{error.project_name}</span>
                    )}
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Project Duration
                  </Form.Label>
                  <Col>
                    <InputGroup>
                      <FormControl
                        size="lg"
                        type="number"
                        name="project_duration"
                        placeholder="Number of Days"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.project_duration}
                        className="green-focus"
                      />
                      <InputGroup.Append>
                        <InputGroup.Text
                          style={{ background: "transparent", border: "1px solid #e5e6e7" }}
                        >
                          Days
                        </InputGroup.Text>
                      </InputGroup.Append>
                    </InputGroup>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom  p-r-15">
                    Location
                  </Form.Label>
                  <Col>
                    <PlacesAutoComplete
                      location={(location) => {
                        setLocation(location);
                      }}
                      lat={(lat) => {
                        setLat(lat);
                      }}
                      lng={(lng) => setLng(lng)}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom  p-r-15">
                    Cause Supported
                  </Form.Label>
                  <Col style={{ marginTop: "-10px" }}>
                    <CheckboxTable
                      cause={(causes) => {
                        setCause(causes);
                        console.log(causes, "table");
                      }}
                      alreadyChecked={cause}
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom  p-r-15">
                    Project Excerpts
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      name="project_excerpts"
                      as="textarea"
                      maxLength="256"
                      minLength="50"
                      rows="5"
                      placeholder="Your Project info in maximum 256 Characters"
                      onChange={(e) => {
                        setCount(e.target.value.length);
                        handleExcerpt(e);
                      }}
                      onBlur={handleBlur}
                      className="green-focus"
                      style={{ borderRadius: "0" }}
                    />
                    <span className="help-block">
                      Min: 50 chars | Max: 256 chars | Remaining Chars: {maxCount - count}
                    </span>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Project Description
                  </Form.Label>
                  <Col>
                    {/* <TextEditor /> */}
                    <div style={{ maxWidth: "100%" }}>
                      <FroalaEditorComponent
                        tag="textarea"
                        config={config}
                        onModelChange={handleModelChange}
                      />
                    </div>
                    <span className="help-block">
                      Min : 256 | Max : 2000 | Remaining : {max - desc.length}
                    </span>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Project Image
                  </Form.Label>
                  <Col className="form-input-align-center ">
                    <Form.File className="position-relative" name="file" />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Select Start And End Date
                  </Form.Label>
                  <Col lg={5} className="form-input-align-center">
                    {" "}
                    <DatePicker
                      placeholderText="Start Date"
                      selected={startDate}
                      dateFormat="dd-MM-yyyy"
                      selectsStart
                      startDate={startDate}
                      endDate={endDate}
                      onChange={(date) => setStartDate(date.getTime())}
                      popperPlacement="top-end"
                      style={{ color: "yellow" }}
                      required
                      className="green-focus"
                    />
                  </Col>
                  <Col lg={5} className="form-input-align-center">
                    {" "}
                    <DatePicker
                      placeholderText="End Date"
                      selected={endDate}
                      dateFormat="dd-MM-yyyy"
                      selectsEnd
                      startDate={startDate}
                      endDate={endDate}
                      minDate={startDate}
                      popperPlacement="top-end"
                      onChange={(date) => setEndDate(date.getTime())}
                      required
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Estimated Cost
                  </Form.Label>
                  <Col>
                    <InputGroup style={{ zIndex: 0 }}>
                      <InputGroup.Prepend>
                        <InputGroup.Text
                          id="basic-addon1"
                          style={{
                            margin: "0",
                            background: "transparent",
                            border: "1px solid #e5e6e7",
                          }}
                        >
                          <i className="fa fa-rupee"></i>
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        type="number"
                        size="lg"
                        name="estimated_cost"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.estimated_cost}
                        className="green-focus"
                      />
                    </InputGroup>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label
                    column="lg"
                    lg={2}
                    className="form-label-custom p-r-15"
                    style={{ marginTop: "auto", marginBottom: "auto" }}
                  >
                    Comma Seperated Tags
                  </Form.Label>
                  <Col>
                    {/* <TagsInput tags={[]} /> */}
                    {/* <ReactTags
                    // ref={this.reactTags}
                    tags={tags}
                    suggestions={suggestions}
                    onDelete={(i) => onDelete(i)}
                    onAddition={(tag) => onAddition(tag)}
                  /> */}
                    <TagAutocomplete
                      tags={(tag) => {
                        var dataArr = tag.map((item) => {
                          return [item.name, item];
                        });
                        var maparr = new Map(dataArr);
                        var result = [...maparr.values()];
                        setTag(result);
                      }}
                      tag={tag}
                    />
                    <Form.Text className="text-muted" muted>
                      Maximum 3 tags allowed
                    </Form.Text>
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    Contact Info
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      name="project_contact"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.project_contact}
                      className="green-focus"
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                    External URL
                  </Form.Label>
                  <Col>
                    <Form.Control
                      size="lg"
                      type="text"
                      placeholder="Enter the website link if you have any"
                      name="external_url"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.external_url}
                      className="green-focus"
                    />
                  </Col>
                </Form.Row>
                <hr className="form-horizontal-line" />
                <Form.Row>
                  <Form.Label column="lg" lg={2} className="form-label-custom"></Form.Label>
                  <Col>
                    <button
                      type="submit"
                      className="ph-btn"
                      style={{ fontSize: "14px", padding: "6px" }}
                    >
                      Create Project
                    </button>
                    {/* <CustomButton content="Create Project" /> */}
                  </Col>
                </Form.Row>
              </Form.Group>
            </form>
          </Card>
        </Card>
      </div>
    </>
  );
};
const mapStateToProps = function (state) {
  return {
    user: state.phAuth.user,
  };
};

export default connect(mapStateToProps)(CreateProject);
// export default CreateProject;
