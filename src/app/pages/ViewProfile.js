import React from "react";
import { Row, Col, Modal } from "react-bootstrap";
import { Card } from "@material-ui/core";
import { connect } from "react-redux";
import { Images } from "../config/Images";
import "./MyPage.css";
import ViewDetail from "../components/ViewDetails";
import Initiatives from "../components/Initiatives";
import CustomButton from "../components/CustomButton";
import TweetEmbed from "react-tweet-embed";
import { TwitterTimelineEmbed } from "react-twitter-embed";
import axios from "../config/axios";
import { ToastContainer, toast } from 'react-toastify';

const InitiativeDetail = [
  // {
  //   name: "Clean India Campaign",
  //   content:
  //     "`Swacch Bharat Abhiyaan pet project of PM Narendra Modi to make India clean. Volunteers are required to spread the word about Clean India Campaign on their social network like Facebook, Twitter etc.Be as creative as they can to spread the wordThey can organize ...`",
  //   image: "",
  //   duration: "10 hours",
  //   people: "999",
  //   apply: "infi",
  // },
  // {
  //   name: "Clean India Campaign",
  //   content:
  //     "`Swacch Bharat Abhiyaan pet project of PM Narendra Modi to make India clean. Volunteers are required to spread the word about Clean India Campaign on their social network like Facebook, Twitter etc.Be as creative as they can to spread the wordThey can organize ...`",
  //   image: "",
  //   duration: "10 hours",
  //   people: "999",
  //   apply: "infi",
  // },
  // {
  //   name: "Clean India Campaign",
  //   content:
  //     "`Swacch Bharat Abhiyaan pet project of PM Narendra Modi to make India clean. Volunteers are required to spread the word about Clean India Campaign on their social network like Facebook, Twitter etc.Be as creative as they can to spread the wordThey can organize ...`",
  //   image: "",
  //   duration: "10 hours",
  //   people: "999",
  //   apply: "infi",
  // },
  // {
  //   name: "Clean India Campaign",
  //   content:
  //     "`Swacch Bharat Abhiyaan pet project of PM Narendra Modi to make India clean. Volunteers are required to spread the word about Clean India Campaign on their social network like Facebook, Twitter etc.Be as creative as they can to spread the wordThey can organize ...`",
  //   image: "",
  //   duration: "10 hours",
  //   people: "999",
  //   apply: "infi",
  // },
  // {
  //   name: "Clean India Campaign",
  //   content:
  //     "`Swacch Bharat Abhiyaan pet project of PM Narendra Modi to make India clean. Volunteers are required to spread the word about Clean India Campaign on their social network like Facebook, Twitter etc.Be as creative as they can to spread the wordThey can organize ...`",
  //   image: "",
  //   duration: "10 hours",
  //   people: "999",
  //   apply: "infi",
  // },
];

class ViewProfile extends React.Component {
  constructor() {
    super();
    this.state = {
      clickedDetails: true,
      clickedInitiatives: false,
      tweet: true,
      viewDetails: "view-button-present",
      viewInitiatives: "view-button-other",
      iColorDetail: "white",
      iColorInitiative: "#4caf4f",
      cover: "",
      logo: "",
      show: false
    };
  }

  componentDidMount() {
    // toast.success("Welcome to ProjectHeena")
    console.log("process", process.env)
    console.group("componentDidMount");
    var slug = this.props.location.pathname.split("/")[2]
    console.info(this.props.ngo);
    console.groupEnd("componentDidMount");
    var path_for_images = "http://localhost:5000/uploads/images/ngo/"
    var default_image = 'http://localhost:3000/' + Images.Social_work
    // var logo = this.props.ngo.logo == "" ? default_image : path_for_images + this.props.ngo.logo
    // var cover = this.props.ngo.cover == "" ? default_image : path_for_images + this.props.ngo.cover
    // console.log(logo,)
    // var cover = path_for_images + this.props.ngo.cover
    // this.setState({ ...this.state, logo })
    // this.setState({ ...this.state, cover })

    // if (!this.props.user) {
    //   this.props.history.push({
    //     pathname: "/signup",
    //   });
    // }
  }

  handleDetails = () => {
    this.setState({
      clickedDetails: true,
      clickedInitiatives: false,
      viewInitiatives: "view-button-other",
      viewDetails: "view-button-present",
      iColorDetail: "white",
      iColorInitiative: "#4caf4f",
    });
  };
  handleInitiatives = () => {
    this.setState({
      clickedInitiatives: true,
      clickedDetails: false,
      tweet: false,
      viewInitiatives: "view-button-present",
      viewDetails: "view-button-other",
      iColorInitiative: "white",
      iColorDetail: "#4caf4f",
    });
  };

  handleClose = () => {
    this.setState({ ...this.state, show: false })
  }
  handleShow = () => {
    this.setState({ ...this.state, show: true })
  }

  render() {
    return (
      <div className="container">
        <ToastContainer />
        <Row style={{ marginTop: "5%" }}>
          <Col xs={1}></Col>
          <Col xs={3}>
            <img style={{ width: "75%" }} src={this.state.logo} alt=""></img>
          </Col>
          <Col xs={6} style={{ marginLeft: '-2.5%', marginTop: '4.5%' }}>
            <h1 className="font-weight-bold ">{`${this.props.ngo?.ngo_name}`}</h1>
            {/* <h1 className="font-weight-bold ">Volunteer</h1> */}
            <h4>Mumbai, Maharashtra, India</h4>
          </Col>
        </Row>
        <Card className="ngo_profile_card">
          <Row>
            <div style={{ marginLeft: "16%", marginTop: "-3%" }}>
              <CustomButton content={`Follow`} onClick={this.handleShow} type="submit" />
            </div>
          </Row>

          <Row className="p-5">
            {this.state.clickedDetails == true ? (
              <Col md={8}>
                <ViewDetail />
              </Col>
            ) : (
              <Col md={8}>
                <Initiatives init={InitiativeDetail} handleInitiatives={(e) => {
                  console.log('SETSTATE', e)
                  // this.setState({
                  //   clickedInitiatives: true,
                  //   clickedDetails: false,
                  //   tweet: false,
                  //   viewInitiatives: "view-button-present",
                  //   viewDetails: "view-button-other",
                  //   iColorInitiative: "white",
                  //   iColorDetail: "#4caf4f",
                  // })
                }} />
              </Col>
            )}

            <Col md={4}>
              <div class="m-b-xs view-buttons" style={{ marginBottom: "10%" }}>
                <button
                  onClick={this.handleDetails}
                  class={this.state.viewDetails}
                  style={{ marginRight: "10px" }}
                >
                  <i class="fa fa-newspaper-o" style={{ color: this.state.iColorDetail }}></i>{" "}
                  Details
                </button>
                <button onClick={this.handleInitiatives} class={this.state.viewInitiatives}>
                  <i class="fa fa-tasks" style={{ color: this.state.iColorInitiative }}></i>{" "}
                  Initiatives
                </button>
              </div>
              <div>
                <h3 className="font-weight-bold" style={{ marginLeft: "6%", fontSize: "20px" }}>
                  {/* Our Few Top Volunteers */}
                </h3>
                {/* <img src={Images.user_logo} style={{ width: '25%', borderRadius: '50%', marginLeft: '10%', marginRight: '2%' }}></img>
                <img src={Images.user_logo} style={{ width: '25%', borderRadius: '50%', marginLeft: '2%', marginRight: '2%' }}></img>
                <img src={Images.user_logo} style={{ width: '25%', borderRadius: '50%', marginLeft: '2%', marginRight: '2%' }}></img> */}

                {/* <div class="container1" style={{ display: "inline-block" }}>
                  <img
                    src={Images.user_logo}
                    alt="Avatar"
                    class="image1"
                    style={{
                      borderRadius: "50%",
                      width: "70%",
                      marginLeft: "3%",
                    }}
                  />
                  <div class="overlay1" style={{ display: "inline-block" }}>
                    <div class="text1" style={{ display: "inline-block" }}>
                      Daksh Khatri
                    </div>
                  </div>
                </div> */}
                {/* 
                <div class="container1" style={{ display: "inline-block" }}>
                  <img
                    src={Images.user_logo}
                    alt="Avatar"
                    class="image1"
                    style={{
                      borderRadius: "50%",
                      width: "70%",
                      marginLeft: "3%",
                    }}
                  />
                  <div class="overlay1" style={{ display: "inline-block" }}>
                    <div class="text1" style={{ display: "inline-block" }}>
                      Pravin Krishna
                    </div>
                  </div>
                </div> */}

                {/* <div class="container1" style={{ display: "inline-block" }}>
                  <img
                    src={Images.user_logo}
                    alt="Avatar"
                    class="image1"
                    style={{
                      borderRadius: "50%",
                      width: "70%",
                      marginLeft: "3%",
                    }}
                  />
                  <div class="overlay1" style={{ display: "inline-block" }}>
                    <div class="text1" style={{ display: "inline-block" }}>
                      Tejas Thakare
                    </div>
                  </div>
                </div> */}
              </div>
              <Row className="px-5 py-4">
                <h3 className="font-weight-bold" style={{ fontSize: "20px" }}>
                  Latest Tasks
                </h3>
                {/* <ul style={{ padding: 0 }}>
                  <li style={{ listStyleType: "none", marginBottom: "-4%" }}>
                    <i class="fa fa-tasks"></i>{" "}
                    <p style={{ display: "inline-block" }}>Clean India Campaign</p>
                  </li>
                  <li style={{ listStyleType: "none", marginBottom: "-4%" }}>
                    <i class="fa fa-tasks"></i>{" "}
                    <p style={{ display: "inline-block" }}>Testing Create Task Form </p>
                  </li>
                  <li style={{ listStyleType: "none", marginBottom: "-4%" }}>
                    <i class="fa fa-tasks"></i>{" "}
                    <p style={{ display: "inline-block" }}>Testing Create Task Form </p>
                  </li>
                </ul>*/}
                <p>View All Tasks by Social Work</p>
                {/* <a style={{ color: "#4CAF50", fontSize: "20px" }}>View All Tasks by Social Work</a> */}
              </Row>

              <Row className="px-5 py-4">
                <h3 className="font-weight-bold" style={{ fontSize: "20px" }}>
                  Corporate CSR Partners
                </h3>
                <br></br>
                <p>
                  No CSR partners added yet
                </p>
                {/* <ul style={{ marginLeft: "-18%" }}>
                  <li style={{ listStyleType: "none", marginBottom: "-4%" }}>
                    <i class="fa fa-tasks"></i>{" "}
                    <p style={{ display: "inline-block" }}>Jindal Steel and Power</p>
                  </li>
                  <li style={{ listStyleType: "none", marginBottom: "-4%" }}>
                    <i class="fa fa-tasks"></i>{" "}
                    <p style={{ display: "inline-block" }}>SBI Life Insurance Co Ltd</p>
                  </li>
                  <li style={{ listStyleType: "none", marginBottom: "-4%" }}>
                    <i class="fa fa-tasks"></i>{" "}
                    <p style={{ display: "inline-block" }}>Tata Capital</p>
                  </li>
                </ul> */}
              </Row>
              {this.state.tweet == true ? (
                <div>
                  <TwitterTimelineEmbed
                    sourceType="profile"
                    screenName="ProjectHeena"
                    options={{ height: 600 }}
                  />

                  <TweetEmbed id="1288825214835580928" />
                </div>
              ) : null}
            </Col>
          </Row>
        </Card>
        <Modal show={this.state.show} onHide={this.handleClose} style={{ opacity: '1' }} size="lg">
          <Modal.Header closeButton>
            <Modal.Title>Follow {`${this.props.user?.first_name} ${this.props.user?.last_name}`}</Modal.Title>
          </Modal.Header>
          <Modal.Body>You cannot follow yourself.</Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    user: state.phAuth.user,
    ngo: state.phAuth.ngo,
  };
};

export default connect(mapStateToProps)(ViewProfile);
