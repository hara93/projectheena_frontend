import React, { Component } from "react";
import { Button, Card, Form, Input, message, Select } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import CKEditor from "@ckeditor/ckeditor5-react";
// import CKEditor from 'ckeditor4-react';
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { article_add } from "../../appRedux/actions/Article";
import { category_list } from "../../appRedux/actions/Category";
import PropTypes from "prop-types";
import HttpService from "../../services/httpService";

const { Option } = Select;

const FormItem = Form.Item;

class SamplePage extends Component {
  httpService;
  constructor(props) {
    super(props);
    this.state = {
      article_body: "",
      subCatArray: [],
      is_sub_cat: false,
    };
    this.httpService = new HttpService();
    this.handleChange = this.handleChange.bind(this);
    this.onEditorChange = this.onEditorChange.bind(this);
  }
  componentDidMount() {
    this.props.category_list();
  }
  handleSubmit = (e) => {
    console.log("this.state.article_body", this.state.article_body);
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      //console.log(values);
      if (!err) {
        if (this.state.article_body === "") {
          message.error("Article body missing");
        } else {
          console.log("Received values of form: ", values);
          // console.log('localStorage.get("user_id")', localStorage.get("user_id"))
          var dataValues = {
            title: values.title,
            body: this.state.article_body,
            created_by: localStorage.getItem("user_id"),
            category_id: values.category_id,
            subcategory_id: values.subcategory_id,
          };
          // console.log("datavalues", dataValues)
          this.props.article_add(dataValues);
          // this.props.history.push('/article');
          // window.location.reload(true);
        }
      }
    });
  };
  componentDidUpdate(prevProps, prevState) {
    if (this.props.success) {
      this.props.history.push("/article");
    }
  }
  onEditorChange(evt) {
    this.setState({
      article_body: evt.editor.getData(),
    });
  }
  handleChange(changeEvent) {
    this.setState({
      data: changeEvent.target.value,
    });
  }
  handleCategory = (catID) => {
    this.httpService
      .post("category_controller/all_list", { category_id: catID, type: 2 })
      .then((res) => {
        this.setState({
          subCatArray: res.data,
          is_sub_cat: true,
        });
      })
      .catch((err) => {
        console.log("err-result", err);
      });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    // const { autoCompleteResult } = this.state;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    return (
      <div className="ant-row">
        <div className="ant-col ant-col-sm-24 ant-col-md-24 ant-col-lg-24 ant-col-xl-24">
          <Card className="gx-card" title="Create Article">
            <Form onSubmit={this.handleSubmit}>
              <FormItem {...formItemLayout} label={<span>Title &nbsp;</span>}>
                {getFieldDecorator("title", {
                  rules: [
                    {
                      required: true,
                      message: "Please enter title",
                      whitespace: true,
                    },
                  ],
                })(<Input placeholder="Article Title" />)}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label={<span>Category &nbsp;</span>}
              >
                {getFieldDecorator("category_id", {
                  rules: [
                    { required: true, message: "Please select category" },
                  ],
                })(
                  <Select
                    placeholder="Select category"
                    optionLabelProp="label"
                    showSearch
                    onChange={this.handleCategory}
                  >
                    {this.props.categories
                      ? this.props.categories.map((cat) => (
                          <Option value={cat.id} label={cat.category_name}>
                            {cat.category_name}
                          </Option>
                        ))
                      : null}
                  </Select>
                )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label={<span>Sub Category &nbsp;</span>}
              >
                {getFieldDecorator("subcategory_id", {
                  rules: [
                    { required: true, message: "Please select sub category" },
                  ],
                })(
                  <Select
                    placeholder="Select sub category"
                    optionLabelProp="label"
                    showSearch
                  >
                    {this.state.is_sub_cat
                      ? this.state.subCatArray
                        ? this.state.subCatArray.map((subcat) => (
                            <Option
                              value={subcat.id}
                              label={subcat.category_name}
                            >
                              {subcat.category_name}
                            </Option>
                          ))
                        : null
                      : null}
                  </Select>
                )}
              </FormItem>
              <FormItem {...formItemLayout} label={<span>Body&nbsp;</span>}>
                {getFieldDecorator("body", {
                  // rules: [{''}],
                })(
                  // <TextArea></TextArea>
                  <CKEditor
                    editor={ClassicEditor}
                    toolbar={"imageUpload"}
                    config={{
                      ckfinder: {
                        // uploadUrl: 'http://localhost:8001/article_controller/image',
                        uploadUrl:
                          "http://218.199.212.15:8001/article_controller/image",
                        options: {
                          resourceType: "Images",
                        },
                      },
                    }}
                    onChange={(event, editor) => {
                      const article_body = editor.getData();
                      this.setState({
                        article_body,
                      });
                    }}
                  />
                  // <Fragment>
                  //     <CKEditor
                  //         data={this.state.article_body}
                  //         config={ {
                  //             // filebrowserImageBrowseUrl : 'http://localhost:8001/article_controller/image',
                  //             filebrowserImageUploadUrl : 'http://localhost:8001/article_controller/image',
                  //             filebrowserWindowWidth: '640',
                  //             filebrowserWindowHeight: '480'
                  //         } }
                  //         onChange={this.onEditorChange} />
                  //         <label>
                  //             {/* Change value: */}
                  //             {/* <textarea defaultValue={this.state.data} onChange={this.handleChange} /> */}
                  //         </label>
                  //         <EditorPreview article_body={this.state.article_body}
                  //     />
                  // </Fragment>
                )}
              </FormItem>
              <FormItem {...tailFormItemLayout}>
                <Link to={"/article"} className="btn">
                  Cancel
                </Link>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </FormItem>
            </Form>
          </Card>
        </div>
      </div>
    );
  }
}
class EditorPreview extends Component {
  render() {
    return (
      <div className="editor-preview">
        {/* <h2>Rendered content</h2> */}
        {/* <div dangerouslySetInnerHTML={ { __html: this.props.data } }></div> */}
      </div>
    );
  }
}

// EditorPreview.defaultProps = {
//     data: ''
// };

EditorPreview.propTypes = {
  data: PropTypes.string,
};
const RegistrationForm = Form.create()(SamplePage);
const mapStateToProps = ({ articleList, categoryList }) => {
  const { success } = articleList;
  const { categories } = categoryList;
  return { success, categories };
};

export default connect(mapStateToProps, {
  article_add,
  category_list,
})(RegistrationForm);
