/* eslint-disable no-restricted-imports */
import React from "react";
import { Col, Row, Form, Modal, Card, Button, Tabs, Tab } from "react-bootstrap";
// import { Card } from "@material-ui/core";
// import Tooltip from "@material-ui/core/Tooltip";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import { Images } from "../config/Images";
// import { supportsInlineSVG } from "react-inlinesvg/lib/helpers";
import CustomButtonOutline from "../components/CustomButtonOutline";
// import ManageTask from "./ManageTask";
import UpdateTask from "./UpdateTask";
import TaskGallery from "./TaskGallery";
// import TaskVolunteer from "./TaskVolunteer";
import AttendanceDetail from "./AttendanceDetail";
import AttendanceDetailUser from "./AttendanceDetailUser";
import * as ReactBootstrap from "react-bootstrap";
// import CustomButton from "../components/CustomButton";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import { connect } from "react-redux";
import axios from "../config/axios";
import TurndownService from "turndown";
import ReactMarkdown from "react-markdown";
import moment from "moment";
// import ReactTooltip from "react-tooltip";

//http://localhost:5000/uploads/images/task/task-image-1629186822035.jpeg
class TaskDetailsUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      details: true,

      update: false,
      image: false,
      creatorId: "",
      attendance: false,
      isOpen: false,
      plus: false,
      users: false,
      show: false,
      temp_id: this.props.location.pathname.split("/"),
      tags: [],
      task_data: {},
      desc: "",
      task_creator: {},
      noOfUpdates: 0,
      focusCSS: { color: "#525252" },
      volunteersList: [],
      helpReason: "",
      joining: "",
      shareContact: 0,
      shareEmail: 0,
      tranxStatus: {},
      task_status: 1,
      unvolunteerShow: false,
      unvolReason: "",
      task_skills: [],
      selfEval: false,
    };
    // this.details = React.createRef();

    this.htmlToMarkdown = this.htmlToMarkdown.bind(this);
  }

  componentDidMount() {
    // this.details.current.focus();
    console.log("props ==============> ", this.props)
    var task_id = this.state.temp_id[this.state.temp_id.length - 1];

    axios
      .get(`/task/details/${task_id}`)
      .then((response) => {
        console.log("data", response);

        // if (this.props.user._id === response.data.task_creator_id) {
        //   console.log("isCreator");
        // } else {
        //   this.props.history.push("/task-detail-user");
        // }
        var s = [];
        response.data.task_skill_mapping.forEach((ele) => s.push(ele.skill_id));
        console.log("this", this.props.user._id);
        var markdown = this.htmlToMarkdown(response.data.task_description);
        this.setState({
          task_data: response.data,
          creatorId: response.data.task_creator_id,
          task_status: response.data.task_status,
          task_skills: s,
        });
        localStorage.setItem("task_data_id", response.data._id);
        this.setState({ desc: markdown });
        axios
          .post(`/tag/getTaskTags`, { task_id: response.data.task_id })
          .then((response) => {
            this.setState({ tags: response.data });
          })
          .catch((err) => console.log("err", err));
        axios
          .post("/user/retrieve", { user_id: response.data.task_creator_id })
          .then((response) => {
            this.setState({ task_creator: response.data });
          })
          .catch((err) => console.log("err", err));
      })
      .catch((err) => console.log("err", err));
    axios
      .get(`/taskTransaction/taskVolunteersList/${task_id}`)
      .then((res) => {
        this.setState({ volunteersList: res.data });
        console.log("taskVolunteersList", res);
      })
      .catch((err) => console.log("err", err));
    axios
      .post(`/taskTransaction/checkTransactionStatus`, {
        taskId: task_id,
        userId: this.props.user._id,
      })
      .then((res) => {
        this.setState({ tranxStatus: res });
        console.log("status check", res);
      })
      .catch((err) => console.log("err", err));
  }
  componentDidUpdate() {
    // console.log("desc", this.state.tranxStatus);
    // console.log("task status", this.state.task_status);
    // console.log("tags", this.state.joining);
    // console.log("userdata", this.state.helpReason);
  }
  htmlToMarkdown = (data) => {
    var turndown = new TurndownService();
    var desc = turndown.turndown(data);

    return desc;
  };
  handleDetails = () => {
    this.setState({
      details: true,
      manageTask: false,
      update: false,
      image: false,
      volunteer: false,
      attendance: false,
    });
  };

  handleUpdate = () => {
    this.setState({
      manageTask: false,
      details: false,
      update: true,
      image: false,
      volunteer: false,
      attendance: false,
    });
  };
  handleManage = () => {
    this.setState({
      manageTask: true,
      details: false,
      update: false,
      image: false,
      volunteer: false,
      attendance: false,
    });
  };
  handleGallery = () => {
    this.setState({
      manageTask: false,
      details: false,
      update: false,
      image: true,
      volunteer: false,
      attendance: false,
    });
  };
  handleVolunteer = () => {
    this.setState({
      manageTask: false,
      details: false,
      update: false,
      image: false,
      volunteer: true,
      attendance: false,
    });
  };
  handleAttendance = () => {
    this.setState({
      manageTask: false,
      details: false,
      update: false,
      image: false,
      volunteer: false,
      attendance: true,
    });
  };

  handlePlus = () => {
    this.setState({ plus: true });
  };

  handleMinus = () => {
    this.setState({ plus: false });
  };
  handleClick = () => {
    this.setState({ users: !this.state.users });
  };

  openModal = () => this.setState({ isOpen: true });
  closeModal = () => this.setState({ isOpen: false });

  handleClose = () =>
    this.setState({ show: false, joining: "", shareEmail: 0, shareContact: 0, helpReason: "" });
  handleShow = () => this.setState({ show: true });
  handleShareContact = (e) => {
    e.target.checked ? this.setState({ shareContact: 1 }) : this.setState({ shareContact: 0 });
  };
  handleEmailContact = (e) => {
    e.target.checked ? this.setState({ shareEmail: 1 }) : this.setState({ shareEmail: 0 });
  };
  closeUnvolunteerShow = () => this.setState({ unvolunteerShow: false });
  closeSelfEval = () => this.setState({ selfEval: false });
  setKey = (k) => this.setState({ key: k });
  unvolunteerSelf = () => {
    var data = {};
    data.task_id = this.state.task_data._id;
    data.user_id = this.props.user._id;
    data.reason = this.state.unvolReason;
    console.log("values", data);
    axios
      .post("/taskTransaction/unvolunteer", data)
      .then((res) => console.log("unvolunteer", res))
      .catch((err) => console.log("unvol err", err));
    this.setState({ unvolReason: "" });
    window.location.reload();
  };
  handleOfferHelp = () => {
    var data = {};
    data.task_id = this.state.temp_id[this.state.temp_id.length - 1];
    data.task_user_id = this.props.user._id;
    data.task_user_shared_email = this.state.shareEmail;
    data.task_user_shared_contact_number = this.state.shareContact;
    data.task_user_type = "0";
    data.task_comment = this.state.helpReason;
    if (this.state.joining === "Yes") {
      data.joining = 1;
    } else if (this.state.joining === "No") {
      data.joining = 0;
    }
    console.log("values", data);

    axios
      .post("/taskTransaction/offer_help", data)
      .then((res) => {
        console.log("res", res);
        window.location.reload();
      })
      .catch((err) => console.log("err", err));
    this.setState({ show: false, joining: "", shareEmail: 0, shareContact: 0, helpReason: "" });
  };
  render() {
    return (
      <div className="container" style={{ padding: "0" }}>
        <div style={{ margin: "25px 0", textAlign: "center" }}>
          <h1> {this.state.task_data.task_name}</h1>

          {/* <h2>By <p style={{ color: '#4caf4f', display: 'inline-block', fontSize: '23px', fontWeight: '600' }}>Social Work</p></h2> */}
          <div style={{ marginTop: 8.5, marginBottom: 25, display: "inline-block" }}>
            <h4 className="font-bold">
              By <a className="ph-link">{this.props.user.first_name}</a>
            </h4>
          </div>
        </div>

        <ul className="ph-nav-bar tabs" style={{ marginTop: "-2%" }}>
          <li>
            <a href="#terms" className="active" onClick={this.handleDetails} ref={this.details}>
              <div>
                <i
                  className="fa fa-newspaper-o active-focus  p-r-5"
                  style={this.state.details ? this.state.focusCSS : {}}
                ></i>
                <span
                  className="active-focus"
                  style={this.state.details ? this.state.focusCSS : {}}
                >
                  DETAILS
                </span>
              </div>
            </a>
          </li>

          <li>
            <a href="#terms" className="active" onClick={this.handleUpdate}>
              <div>
                <i
                  className="fa fa-bullhorn active-focus p-r-5"
                  style={this.state.update ? this.state.focusCSS : {}}
                ></i>
                <span className="active-focus" style={this.state.update ? this.state.focusCSS : {}}>
                  UPDATES & UPLOADS{" "}
                  {this.state.noOfUpdates > 0 ? `(${this.state.noOfUpdates})` : null}
                </span>
              </div>
            </a>
          </li>
          <li>
            <a href="#terms" className="active" onClick={this.handleGallery}>
              <div>
                <i
                  className="fa fa-image active-focus p-r-5"
                  style={this.state.image ? this.state.focusCSS : {}}
                ></i>
                <span className="active-focus" style={this.state.image ? this.state.focusCSS : {}}>
                  GALLERY
                </span>
              </div>
            </a>
          </li>

          <li>
            <a href="#terms" className="active" onClick={this.handleAttendance}>
              <div>
                <i
                  className="fa fa-calendar active-focus p-r-5"
                  style={this.state.attendance ? this.state.focusCSS : {}}
                ></i>
                <span
                  className="active-focus"
                  style={this.state.attendance ? this.state.focusCSS : {}}
                >
                  ATTENDANCE
                </span>
              </div>
            </a>
          </li>
        </ul>

        <Card style={{ padding: "15px 0" }}>
          <Row className="m-0">
            <Col md={8}>
              {this.state.details == true ? (
                <>
                  <div style={{ marginBottom: "30px" }}>
                    <img
                      className="p-1"
                      // src={`http://localhost:5000/uploads/images/task/${this.state.task_data.image}`}
                      src="http://projectheena.in/uploads/projects/24163180116341/images/default.jpg"

                      alt="task-profile"
                      style={{ width: "100%" }}
                    ></img>
                  </div>

                  {this.state.desc ? (
                    <div style={{ color: "gray", padding: "0 5px", marginBottom: "25px" }}>
                      <ReactMarkdown children={this.state.desc} />
                    </div>
                  ) : null}

                  <div style={{ padding: "0 5px" }}>
                    <div>
                      <h3 style={{ fontSize: "22px", fontWeight: "600" }}>
                        Share with your Friends
                      </h3>
                    </div>

                    <FacebookIcon style={{ fontSize: 35, marginRight: "2vw", color: "#3c5997" }} />
                    <TwitterIcon style={{ fontSize: 35, marginRight: "2vw", color: "#04abed" }} />
                    <LinkedInIcon style={{ fontSize: 35, marginRight: "2vw", color: "#027bb6" }} />
                    <GroupAddIcon style={{ fontSize: 35, marginRight: "2vw", color: "#4caf4f" }} />
                  </div>

                  <ReactBootstrap.Modal
                    show={this.state.isOpen}
                    onHide={this.closeModal}
                    style={{ opacity: "1" }}
                    className="p-5"
                  >
                    <ReactBootstrap.Modal.Header closeButton>
                      <ReactBootstrap.Modal.Title>
                        Enter Recommended User's Email
                      </ReactBootstrap.Modal.Title>
                    </ReactBootstrap.Modal.Header>
                    <ReactBootstrap.Modal.Body className="p-5">
                      <Form>
                        <Form.Group as={Row} controlId="formPlaintextEmail">
                          <Form.Label column sm="2">
                            User Email
                          </Form.Label>
                          <Col sm="7">
                            <Form.Control type="password" placeholder="Email" />
                          </Col>
                          <Col sm="3">
                            <button
                              style={{ border: "1px solid #e7eaec", backgroundColor: "white" }}
                              onClick={this.handlePlus}
                            >
                              <i className="fa fa-plus"></i>
                            </button>
                            <button
                              style={{ border: "1px solid #e7eaec", backgroundColor: "white" }}
                              onClick={this.handleMinus}
                            >
                              <i className="fa fa-minus"></i>
                            </button>
                          </Col>
                        </Form.Group>
                      </Form>

                      {this.state.plus == true ? (
                        <Form>
                          <Form.Group as={Row} controlId="formPlaintextEmail">
                            <Form.Label column sm="2"></Form.Label>
                            <Col sm="7">
                              <Form.Control type="password" placeholder="Email" />
                            </Col>
                            <Col sm="3">
                              <button
                                style={{ border: "1px solid #e7eaec", backgroundColor: "white" }}
                                onClick={this.handlePlus}
                              >
                                <i className="fa fa-plus"></i>
                              </button>
                              <button
                                style={{ border: "1px solid #e7eaec", backgroundColor: "white" }}
                                onClick={this.handleMinus}
                              >
                                <i className="fa fa-minus"></i>
                              </button>
                            </Col>
                          </Form.Group>
                        </Form>
                      ) : null}
                    </ReactBootstrap.Modal.Body>
                    <ReactBootstrap.Modal.Footer>
                      {/* <ReactBootstrap.Button variant="secondary">Close</ReactBootstrap.Button> */}
                      <button
                        className="p-2"
                        onClick={this.closeModal}
                        style={{
                          backgroundColor: "#c2c2c2",
                          border: "1px solid #c2c2c2",
                          color: "white",
                        }}
                      >
                        Close
                      </button>

                      {/* <ReactBootstrap.Button variant="primary">Add meta data</ReactBootstrap.Button> */}
                      <button
                        className="p-2"
                        onClick={this.closeModal}
                        style={{
                          backgroundColor: "#4caf4f",
                          border: "1px solid #4caf4f",
                          color: "white",
                        }}
                      >
                        Submit
                      </button>
                    </ReactBootstrap.Modal.Footer>
                  </ReactBootstrap.Modal>
                </>
              ) : null}

              {this.state.update == true ? (
                <>
                  <UpdateTask
                    data={this.state.task_data}
                    updates={(no) => {
                      this.setState({ noOfUpdates: no.length });
                    }}
                  />
                </>
              ) : null}
              {this.state.image == true ? (
                <>
                  <TaskGallery data={this.state.task_data} />
                </>
              ) : null}

              {this.state.attendance == true ? (
                <>
                  {/* <AttendanceDetail status={this.state.tranxStatus} /> */}
                  <AttendanceDetailUser status={this.state.tranxStatus} />
                </>
              ) : null}
            </Col>

            <Col md={4}>
              {this.state.tranxStatus.status === 11 ? (
                <>
                  {" "}
                  <button
                    className="p-2  search-volunteer-btn"
                    disabled
                    style={{ background: "gray", borderColor: "gray", whiteSpace: "nowrap" }}
                  >
                    Unvolunteered from the task
                  </button>
                </>
              ) : (
                <>
                  {this.state.task_status === 4 || this.state.task_status === 5 ? null : this.state
                    .tranxStatus.status === 4 || this.state.tranxStatus.status === 1 ? (
                    this.state.tranxStatus.status === 4 ? (
                      <button
                        className="p-2  search-volunteer-btn"
                        disabled
                        style={{ background: "gray", borderColor: "gray", whiteSpace: "nowrap" }}
                      >
                        You have Already offered
                      </button>
                    ) : (
                      <button
                        className="p-2  search-volunteer-btn"
                        disabled
                        style={{ background: "gray", borderColor: "gray", whiteSpace: "nowrap" }}
                      >
                        Creator has seeked help from you
                      </button>
                    )
                  ) : this.state.tranxStatus.status === 2 || this.state.tranxStatus.status === 5 ? (
                    <div>
                      <button
                        className="p-2  search-volunteer-btn"
                        onClick={() => this.setState({ selfEval: true })}
                      >
                        {" "}
                        Evaluate Yourself
                      </button>
                      <div>
                        <span style={{ display: "flex", justifyContent: "flex-end" }}>
                          Too busy?{" "}
                          <span
                            className="pointer base-color"
                            style={{ padding: "0 5px" }}
                            onClick={() => this.setState({ unvolunteerShow: true })}
                          >
                            Click here
                          </span>{" "}
                          to unvolunteer.
                        </span>
                        <Modal
                          show={this.state.unvolunteerShow}
                          onHide={this.closeUnvolunteerShow}
                          style={{ opacity: "1" }}
                          size="md"
                        >
                          <div className="modal-header-custom">
                            <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                              Unvolunteer from {`"${this.state.task_data.task_name}"`}
                            </h4>
                            <Button
                              variant="secondary"
                              style={{ height: "30px" }}
                              onClick={this.closeUnvolunteerShow}
                            >
                              <i
                                class="fas fa-times"
                                style={{ paddingRight: "0", color: "white" }}
                              ></i>
                            </Button>
                          </div>

                          <Modal.Body>
                            <div style={{ padding: "20px 10px" }}>
                              <Form.Group controlId="exampleForm.ControlTextarea1">
                                <Form.Control
                                  as="textarea"
                                  rows={5}
                                  placeholder="Reason why you wish to unvolunteer from this task."
                                  className="green-focus"
                                  value={this.state.unvolReason}
                                  onChange={(e) => this.setState({ unvolReason: e.target.value })}
                                />
                                <span className="help-block">
                                  Safe Characters: {500 - this.state.unvolReason.length}
                                </span>
                                <span>
                                  <b>Note</b> - this is one time activity. Once unvolunteered you
                                  will never be able to volunteer on this task.
                                </span>
                              </Form.Group>
                            </div>
                          </Modal.Body>
                          <Modal.Footer>
                            <div>
                              <Button variant="secondary" onClick={this.closeUnvolunteerShow}>Close</Button>
                              <span className="mx-2"></span>
                              <button className="ph-btn" style={{ borderRadius: 5 }} onClick={() => this.unvolunteerSelf()}>
                                Unvolunteer
                              </button>
                            </div>
                          </Modal.Footer>
                        </Modal>
                        <Modal
                          show={this.state.selfEval}
                          onHide={this.closeSelfEval}
                          animation={false}
                        >
                          {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
                          <div className="modal-header-custom">
                            <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                              Evaluate "{`your self`}"
                            </h4>
                            <Button
                              variant="secondary"
                              style={{ height: "30px" }}
                              onClick={this.closeSelfEval}
                            >
                              <i
                                class="fas fa-times"
                                style={{ paddingRight: "0", color: "white" }}
                              ></i>
                            </Button>
                          </div>
                          <Modal.Body>
                            <div>
                              <Tabs
                                id="controlled-tab-example"
                                activeKey={this.state.key}
                                onSelect={(k) => this.setKey(k)}
                              >
                                <Tab
                                  eventKey="Appreciate"
                                  title="Appreciate"
                                  style={{ opacity: "100" }}
                                >
                                  <div style={{ padding: "30px 10px" }}>
                                    <h3 className="font-bold" style={{ fontSize: "20px" }}>
                                      Worked For {0} Hours
                                    </h3>
                                    <div></div>
                                    <Form>
                                      <div style={{ display: "flex", alignItems: "center" }}>
                                        {/* <PrettoSlider
                                        valueLabelDisplay="auto"
                                        aria-label="pretto slider"
                                        defaultValue={0}
                                        max={50}
                                        step={1}
                                        // value={hours}
                                        // onChange={handleHourChange}
                                        style={{ maxWidth: "220px", margin: "20px 0" }}
                                      /> */}
                                        {/* <Rating
                                        name="simple-controlled"
                                        // value={rate}
                                        // onChange={(event, newValue) => {
                                        //   setRate(newValue);
                                        // }}
                                        style={{ margin: "0 10px", fontSize: "25px" }}
                                      /> */}
                                      </div>

                                      <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Control
                                          as="textarea"
                                          rows={5}
                                          placeholder="Write a reason for defaulting this user on this task."
                                          className="green-focus"
                                        // value={appreciate}
                                        // onChange={(e) => setAppreciate(e.target.value)}
                                        />
                                        <span className="help-block">
                                          Max: 500 chars | Remaining Chars: {500}
                                        </span>
                                      </Form.Group>
                                      <div>
                                        <button
                                          type="button"
                                          className="ph-btn"
                                        // onClick={() => appreciateUser()}
                                        >
                                          Appreciate
                                        </button>
                                      </div>
                                    </Form>
                                  </div>
                                </Tab>
                                <Tab eventKey="profile" title="Default" style={{ opacity: "100" }}>
                                  <div style={{ padding: "30px 10px" }}>
                                    <h3 className="font-bold" style={{ fontSize: "20px" }}>
                                      Reason for Defaulting
                                    </h3>
                                    <Form>
                                      <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Control
                                          as="textarea"
                                          rows={5}
                                          placeholder="Write a reason for defaulting this user on this task."
                                          className="green-focus"
                                        // value={defaultReason}
                                        // onChange={(e) => setDefaultReason(e.target.value)}
                                        />
                                        <span className="help-block">
                                          Max: 500 chars | Remaining Chars: {500}
                                        </span>
                                      </Form.Group>
                                      <div>
                                        <button
                                          type="button"
                                          className="ph-btn"
                                        // onClick={() => defaultUser()}
                                        >
                                          Default
                                        </button>
                                        {/* <CustomButton content="Default" />{" "} */}
                                      </div>
                                    </Form>
                                  </div>
                                </Tab>
                                <Tab
                                  eventKey="contact"
                                  title="Unutilized"
                                  style={{ opacity: "100" }}
                                >
                                  <div style={{ padding: "30px 10px" }}>
                                    <h3 className="font-bold" style={{ fontSize: "20px" }}>
                                      Reason for Unutilizing
                                    </h3>
                                    <Form>
                                      <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Control
                                          as="textarea"
                                          rows={5}
                                          placeholder="Write a reason for defaulting this user on this task."
                                          className="green-focus"
                                        // value={unutilize}
                                        // onChange={(e) => setUnutilize(e.target.value)}
                                        />
                                        <span className="help-block">
                                          Max: 500 chars | Remaining Chars: {500}
                                        </span>
                                      </Form.Group>
                                      <div>
                                        <button
                                          type="button"
                                          className="ph-btn"
                                        // onClick={() => unutilizeUser()}
                                        >
                                          Unutilize
                                        </button>
                                      </div>
                                    </Form>
                                  </div>
                                </Tab>
                              </Tabs>
                            </div>
                          </Modal.Body>
                        </Modal>
                      </div>
                    </div>
                  ) : (
                    <button className="p-2  search-volunteer-btn" onClick={() => this.handleShow()}>
                      {" "}
                      Offer Help
                    </button>
                  )}
                </>
              )}

              <div style={{ margin: "25px 0" }}>
                <div>
                  <h3 style={{ fontSize: "16px", fontWeight: "600" }}>Task Statistics</h3>
                </div>
                <div>
                  <ul className="donation-stats">
                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">Status</div>
                      <div className="tc-task">
                        {this.state.task_status === 4 || this.state.task_status === 5 ? (
                          this.state.task_status === 4 ? (
                            <span className="label-task" style={{ background: "gray" }}>
                              Task is closed
                            </span>
                          ) : (
                            <span className="label-task" style={{ backgroundColor: "gray" }}>
                              Deleted
                            </span>
                          )
                        ) : (
                          <span className="label-task">Task is open</span>
                        )}
                      </div>
                    </li>
                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">Duration (Hours)</div>
                      <div className="tc-task">
                        {" "}
                        Min:{this.state.task_data.task_duration_hours_min} &nbsp;|&nbsp; Max:
                        {this.state.task_data.task_duration_hours}
                      </div>
                    </li>
                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">Location</div>
                      <div className="tc-task"> {this.state.task_data.task_location}</div>
                    </li>
                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">People required</div>
                      <div className="tc-task">
                        {" "}
                        Min:{this.state.task_data.people_required_min} &nbsp;|&nbsp; Max:
                        {this.state.task_data.people_required}
                      </div>
                    </li>
                    {this.state.task_data.is_time_bound === 0 ? (
                      <li
                        style={{
                          display: "table-row",
                          listStyleType: "none",
                        }}
                      >
                        <div className="th-task">
                          Task timeline <br />
                          <br />
                          Flexible
                        </div>
                        <div className="tc-task">
                          This is continuous task, without any start or end date. You can join right
                          away and start doing good.
                        </div>
                      </li>
                    ) : (
                      <>
                        <li
                          style={{
                            display: "table-row",
                            listStyleType: "none",
                          }}
                        >
                          <div className="th-task">Application Deadline</div>
                          <div className="tc-task">
                            {moment(this.state.task_data.task_last_application).format(
                              "Do MMM YYYY"
                            )}
                          </div>
                        </li>
                        <li
                          style={{
                            display: "table-row",
                            listStyleType: "none",
                          }}
                        >
                          <div className="th-task">Starting From</div>
                          <div className="tc-task">
                            {moment(this.state.task_data.task_start_time).format("Do MMM YYYY LT")}
                          </div>
                        </li>
                        <li
                          style={{
                            display: "table-row",
                            listStyleType: "none",
                          }}
                        >
                          <div className="th-task">Ending On</div>
                          <div className="tc-task">
                            {moment(this.state.task_data.task_end_time).format("Do MMM YYYY LT")}
                          </div>
                        </li>
                      </>
                    )}

                    <li
                      style={{
                        display: "table-row",
                        listStyleType: "none",
                      }}
                    >
                      <div className="th-task">External Links</div>
                      <div className="tc-task">
                        <a className="ph-link" href={`${this.state.task_data.vision_page}`}>
                          {this.state.task_data.vision_page}
                        </a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div style={{ margin: "25px 0" }}>
                <div style={{ marginBottom: "10px" }}>
                  <h3 style={{ fontSize: "16px", fontWeight: "600", margin: "0" }}>
                    Skills Needed
                  </h3>
                </div>
                {/* <div style={{ display: "contents" }}>
                  {this.props.skillsList.skills
                    .filter((fil) => this.state.task_skills.includes(fil._id))
                    .map((ele) => (
                      <a className="p-2 task-tags-skill" href="skill#">
                        {ele.skill_name}
                      </a>
                    ))}
                </div> */}
                <div style={{ display: "contents" }}>
                  {this.state.task_skills
                    .map((ele) => (
                      <a className="p-2 task-tags-skill" href="skill#">
                        {ele}
                      </a>
                    ))}
                </div>
              </div>
              <div style={{ margin: "25px 0" }}>
                <div style={{ marginBottom: "10px" }}>
                  <h3 style={{ fontSize: "16px", fontWeight: "600", margin: "0" }}>Tags</h3>
                </div>

                <div style={{ display: "contents" }}>
                  {this.state.tags
                    ? this.state.tags.map((ele) => (
                      <a className="p-2 task-tags-skill" href="#">
                        {ele.name}
                      </a>
                    ))
                    : null}
                </div>
              </div>

              <div className="task-author-content">
                <div className="image-holder">
                  <img
                    height="75px"
                    style={{ borderRadius: "50%" }}
                    src={Images.Social_work}
                    alt=""
                  />
                </div>
                <div className="content-holder-task">
                  <h5
                    style={{
                      fontSize: "1.6rem",
                      fontWeight: "600px",
                    }}
                  >
                    {`${this.state.task_creator.first_name} ${this.state.task_creator.last_name} `}
                  </h5>
                  <p style={{ color: "gray" }}>
                    This is a ProjectHeena Operations account that can be tagged as the partner
                    organization where work was done, but there was no NGO involved. Several social
                    initiative executed by ProjectHeena team/volunteers will come here.
                  </p>
                  <CustomButtonOutline content={`View Creators Profile`} />
                </div>
              </div>

              <div style={{ margin: "25px 0" }}>
                <div style={{ marginBottom: "10px" }}>
                  <h3 style={{ fontSize: "16px", fontWeight: "600", margin: "0" }}>
                    Task Volunteers
                  </h3>
                </div>
                <div>
                  {this.state.volunteersList.map((ele, idx) => (
                    <>
                      <div class="container1" style={{ display: "inline-block" }}>
                        <img
                          src={Images.user_logo}
                          alt="Avatar"
                          class="image1"
                          style={{
                            borderRadius: "50%",
                            width: "70%",
                            marginLeft: "3%",
                          }}
                        />
                        <div class="overlay1" style={{ display: "inline-block" }}>
                          <div class="text1" style={{ display: "inline-block" }}>
                            {`${ele.first_name} ${ele.last_name}`}
                          </div>
                        </div>
                      </div>
                    </>
                  ))}
                </div>
              </div>
              {/* {this.state.users == true ? (
                <>
                  <div style={{ marginTop: "3%" }}>
                    <div class="container1" style={{ display: "inline-block" }}>
                      <img
                        src={Images.user_logo}
                        alt="Avatar"
                        class="image1"
                        style={{
                          borderRadius: "50%",
                          width: "70%",
                          marginLeft: "3%",
                        }}
                      />
                      <div class="overlay1" style={{ display: "inline-block" }}>
                        <div class="text1" style={{ display: "inline-block" }}>
                          Daksh Khatri
                        </div>
                      </div>
                    </div>

                    <div class="container1" style={{ display: "inline-block" }}>
                      <img
                        src={Images.user_logo}
                        alt="Avatar"
                        class="image1"
                        style={{
                          borderRadius: "50%",
                          width: "70%",
                          marginLeft: "3%",
                        }}
                      />
                      <div class="overlay1" style={{ display: "inline-block" }}>
                        <div class="text1" style={{ display: "inline-block" }}>
                          Reethika Renganathan{" "}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div style={{ marginTop: "3%" }}>
                    <a onClick={this.handleClick} style={{ paddingLeft: "29%", color: "#4caf4f" }}>
                      <i class="fas fa-arrow-up" style={{ color: "#4caf4f" }}></i> Show less{" "}
                    </a>
                  </div>
                </>
              ) : (
                <div style={{ marginTop: "3%" }}>
                  <a onClick={this.handleClick} style={{ paddingLeft: "29%", color: "#4caf4f" }}>
                    <i class="fas fa-arrow-down" style={{ color: "#4caf4f" }}></i> Show more{" "}
                  </a>
                </div>
              )} */}

              <Modal
                show={this.state.show}
                onHide={this.handleClose}
                style={{ opacity: "1" }}
                size="md"
              >
                <div className="modal-header-custom">
                  <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                    Offer Help on {`"${this.state.task_data.task_name}"`}
                  </h4>
                  <Button variant="secondary" style={{ height: "30px" }} onClick={this.handleClose}>
                    <i class="fas fa-times" style={{ paddingRight: "0", color: "white" }}></i>
                  </Button>
                </div>

                <Modal.Body>
                  <div style={{ padding: "20px 10px" }}>
                    <Form>
                      <Form.Group>
                        <Form.Control
                          as="textarea"
                          // rows={}
                          placeholder="Reason why you wish to help for this task"
                          className="green-focus"
                          value={this.state.helpReason}
                          onChange={(e) => this.setState({ helpReason: e.target.value })}
                        />
                        <span className="help-block">
                          Max: 500 chars | Remaining Chars: {500 - this.state.helpReason.length}
                        </span>
                      </Form.Group>
                      <hr className="form-horizontal-line" />
                      <Form.Group>
                        <Form.Row>
                          <Form.Label column="lg">
                            Will any of your family members be joining you?
                          </Form.Label>
                        </Form.Row>
                        <RadioGroup>
                          <div style={{ display: "flex" }}>
                            <FormControlLabel
                              id={1}
                              control={<Radio color="primary" />}
                              label="Yes"
                              value={"Yes"}
                              checked={this.state.joining === "Yes" ? 1 : 0}
                              onChange={(e) => {
                                this.setState({ joining: e.target.value });
                              }}
                            />
                            <FormControlLabel
                              id={2}
                              control={<Radio color="primary" />}
                              label="No"
                              value={"No"}
                              checked={this.state.joining === "No" ? 1 : 0}
                              onChange={(e) => {
                                this.setState({ joining: e.target.value });
                              }}
                            />
                          </div>
                        </RadioGroup>
                      </Form.Group>
                      <hr className="form-horizontal-line" />
                      <Form.Group>
                        <FormControlLabel
                          // id={1}
                          control={<Checkbox color="primary" />}
                          label={"Share Contact Number"}
                          value={this.state.shareContact}
                          // checked={causessupported.includes(element._id)}
                          onChange={(e) => this.handleShareContact(e)}
                        />
                        <FormControlLabel
                          // id={1}
                          control={<Checkbox color="primary" />}
                          label={"Share Email Address"}
                          value={this.state.shareEmail}
                          // checked={causessupported.includes(element._id)}
                          onChange={(e) => this.handleEmailContact(e)}
                        />
                      </Form.Group>
                      <hr className="form-horizontal-line" />
                      <Form.Group
                        as={Row}
                        controlId="formPlaintextPassword"
                        style={{ marginLeft: "10px" }}
                      >
                        <Form.Label column sm="4">
                          Offer help as:
                        </Form.Label>
                        <Col sm="6">
                          <Form.Control as="select">
                            <option>Individual</option>
                          </Form.Control>
                        </Col>
                      </Form.Group>

                      <hr className="form-horizontal-line" />
                      {/* onClick={() => handleClose()} */}
                      <Form.Group>
                        <button
                          className="ph-btn"
                          type="button"
                          onClick={() => this.handleOfferHelp()}
                        >
                          Save Changes
                        </button>
                      </Form.Group>
                    </Form>
                  </div>
                </Modal.Body>
              </Modal>
            </Col>
          </Row>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  console.log("state", state);
  return {
    user: state.phAuth.user,
    skillsList: state.common.skillsList,
  };
};
export default connect(mapStateToProps)(TaskDetailsUser);

// export default connect(mapStateToProps)(TaskDetails);
