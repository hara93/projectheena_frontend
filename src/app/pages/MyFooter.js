import React from "react";
import "../MyFooter.css";
import { Row, Col, Card } from "react-bootstrap";
// import { Card } from "@material-ui/core";
// import { Images } from "../config/Images";
import { Link } from "react-router-dom"
import { FaThumbsUp, FaTwitter } from "react-icons/fa";

class MyFooter extends React.Component {
  render() {
    return (
      <>
        <Card className="footer">
          <div className="footer-container">
            <Row style={{ marginBottom: "20px" }}>
              <Col md={4} style={{ padding: "0 10px" }}>
                <div className="headline">
                  <h5 className="footer-title"> Know More</h5>
                </div>
                <div className="footer-container" style={{ padding: "0 15px" }}>
                  <Row>
                    <Col xs={6}>
                      <ul className="ul-none">
                        <li>
                          <i className="fas fa-angle-double-right footer-bullet"></i>
                          <Link to="/aboutus" className="footer-links">
                            About Us
                          </Link>
                        </li>
                        <li>
                          <i className="fas fa-angle-double-right footer-bullet"></i>
                          <Link to="/contactus" className="footer-links">
                            Contact Us
                          </Link>
                        </li>
                        <li>
                          <i className="fas fa-angle-double-right footer-bullet"></i>
                          <Link to="/howitworks" className="footer-links">
                            How it works
                          </Link>
                        </li>
                        <li>
                          <i className="fas fa-angle-double-right footer-bullet"></i>
                          <Link to="/" className="footer-links">
                            Blog
                          </Link>
                        </li>
                      </ul>
                    </Col>
                    <Col xs={6}>
                      <ul className="ul-none">
                        <li>
                          <i className="fas fa-angle-double-right footer-bullet"></i>
                          <Link to="/ngolist" className="footer-links">
                            All NGOs
                          </Link>
                        </li>
                        <li>
                          <i className="fas fa-angle-double-right footer-bullet"></i>
                          <Link to="/terms-and-conditions" className="footer-links">
                            Terms & Conditions
                          </Link>
                        </li>
                        <li>
                          <i className="fas fa-angle-double-right footer-bullet"></i>
                          <Link to="/faq" className="footer-links">
                            FAQs
                          </Link>
                        </li>
                        <li>
                          <i className="fas fa-angle-double-right footer-bullet"></i>
                          <Link to="/secret" className="footer-links">
                            Secret
                          </Link>
                        </li>
                      </ul>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col md={4} style={{ padding: "0 10px" }}>
                <div className="headline">
                  <h5 className="footer-title">Lets Get Social</h5>
                </div>
                <p style={{ paddingTop: "0", color: "white" }}>
                  ProjectHeena needs your support. Please Share it and help it grow
                </p>

                <div style={{ margin: "5px 0" }}>
                  <i className="fab fa-facebook-f footer-facebook"></i>
                  <i className="fab fa-twitter footer-twitter"></i>
                  <i className="fab fa-google-plus-g footer-google"></i>
                  <i className="fab fa-youtube footer-youtube"></i>
                </div>

                <Row>
                  <Col md={4}>
                    <button
                      style={{
                        backgroundColor: " #1877f2",
                        color: "white",
                        border: "0px",
                      }}
                    >
                      <FaThumbsUp color="#FFFFFF" />
                      Like 1.6K
                    </button>
                  </Col>
                  <Col md={4}>
                    <button
                      style={{
                        backgroundColor: " #00acee",
                        color: "white",
                        border: "0px",
                      }}
                    >
                      <FaTwitter color="#FFFFFF" />
                      Follow
                    </button>
                  </Col>
                </Row>
              </Col>
              <Col md={4} style={{ padding: "0 10px" }}>
                <div className="headline">
                  <h5 className="footer-title">About ProjectHeena</h5>
                </div>
                <p style={{ paddingTop: "0", color: "white" }}>
                  ProjectHeena is a small experiment where we intend to add value to the world with
                  whatever we have today. It provides a platform to connect experts who can help
                  people across the globe leap forward with their help.
                </p>
                <small style={{ marginLeft: "16vw", color: "white" }}>
                  {" "}
                  &#169; 2021 ProjectHeena
                </small>
              </Col>
            </Row>
            <Row md={12}>
              <Col md={4}>
                <div className="headline">
                  <h5 className="footer-title">Connect</h5>
                </div>
                <p style={{ paddingBottom: "1vh", color: "white" }}>
                  For any help, call +91 8080 44 88 48
                </p>
              </Col>
              <Col md={4}>
                <div style={{ marginTop: "40px", marginBottom: "10px" }}>
                  <img src="http://projectheena.in/assets/img/footer/payment.png" alt="" />
                </div>
              </Col>
            </Row>
          </div>
        </Card>
      </>
    );
  }
}

export default MyFooter;
