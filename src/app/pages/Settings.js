/* eslint-disable no-restricted-imports */
import React from "react";
import { Row, Col, Card } from "react-bootstrap";
// import * as ReactBootstrap from "react-bootstrap";
// import { Card } from "@material-ui/core";
// import { Images } from "../config/Images";
import { Form } from "react-bootstrap";
import CustomButtonOutline from "../components/CustomButtonOutline";
// import TagsInput from "../components/TagInput";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

class Settings extends React.Component {
  render() {
    return (
      <div className="container">
        <div style={{ marginBottom: "30px" }}>
          <h2 className="font-bold">Settings</h2>
          <p>
            Please use the below forms to change your settings respectively.
          </p>
        </div>

        <Row>
          <Col lg={6} style={{ marginBottom: "25px" }}>
            <Card>
              <div
                className="box-ibox-title"
                style={{ borderBottom: "1px solid rgb(231 231 231)" }}
              >
                <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                  Change Password
                </h5>
              </div>
              <div
                style={{
                  padding: "15px 20px 20px 20px",
                  border: "1px solid rgb(231 231 231)",
                }}
              >
                <Form>
                  <Form.Group>
                    <Form.Row>
                      <Form.Label
                        column="lg"
                        lg={2}
                        className="form-label-custom p-r-15"
                      >
                        Current Password
                      </Form.Label>
                      <Col className="form-input-align-center">
                        <Form.Control type="text" className="green-focus" />
                      </Col>
                    </Form.Row>
                  </Form.Group>
                  <hr
                    className="form-horizontal-line"
                    style={{ margin: "20px 0" }}
                  />
                  <Form.Group>
                    <Form.Row>
                      <Form.Label
                        column="lg"
                        lg={2}
                        className="form-label-custom p-r-15"
                      >
                        New Password
                      </Form.Label>
                      <Col className="form-input-align-center">
                        <Form.Control type="password" className="green-focus" />
                      </Col>
                    </Form.Row>
                  </Form.Group>
                  <hr
                    className="form-horizontal-line"
                    style={{ margin: "20px 0" }}
                  />
                  <Form.Group>
                    <Form.Row>
                      <Form.Label
                        column="lg"
                        lg={2}
                        className="form-label-custom p-r-15"
                      >
                        Confirm Password
                      </Form.Label>
                      <Col className="form-input-align-center">
                        <Form.Control type="password" className="green-focus" />
                      </Col>
                    </Form.Row>
                  </Form.Group>
                  <hr
                    className="form-horizontal-line"
                    style={{ margin: "20px 0" }}
                  />
                  <Form.Group>
                    <Form.Row>
                      <Form.Label
                        column="lg"
                        lg={2}
                        className="form-invite-friends p-r-15"
                      ></Form.Label>
                      <Col>
                        <CustomButtonOutline content={`Change Password`} />
                      </Col>
                    </Form.Row>
                  </Form.Group>
                </Form>
              </div>
            </Card>
          </Col>
          <Col lg={6}>
            <Card style={{ marginBottom: "25px" }}>
              <div
                className="box-ibox-title"
                style={{ borderBottom: "1px solid rgb(231 231 231)" }}
              >
                <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                  SMS Preferene
                </h5>
              </div>
              <div
                style={{
                  padding: "15px 20px 20px 20px",
                  border: "1px solid rgb(231 231 231)",
                }}
              >
                <FormControlLabel
                  control={<Checkbox color="primary" />}
                  label=" Receive Occasional ProjectHeena SMS. (We dont spam)"
                  value=" Receive SMS"
                />

                <CustomButtonOutline content={`Change SMS Setting`} />
              </div>
            </Card>
            <Card style={{ marginBottom: "25px" }}>
              <div
                className="box-ibox-title"
                style={{ borderBottom: "1px solid rgb(231 231 231)" }}
              >
                <h5 style={{ fontSize: "14px", fontWeight: "600" }}>
                  NGO Admin
                </h5>
              </div>
              <div
                style={{
                  padding: "15px 20px 20px 20px",
                  border: "1px solid rgb(231 231 231)",
                }}
              >
                <div style={{ marginBottom: "15px" }}>
                  <Form.Control type="text" className="green-focus" />
                </div>
                <CustomButtonOutline content={`Change Admin`} />
              </div>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Settings;
