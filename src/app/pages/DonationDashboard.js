import React,{useState} from 'react';
import {Row,Col, Tab, Tabs, Card} from 'react-bootstrap';
import {Images} from '../config/Images';
import OpenTasks from '../components/OpenTasks';
import DonationVolunteer from './DonationVolunteer';
import Donate from './Donate';
import DonationCollaborate from './DonationCollaborate';


const DonationDashboard = () =>{
    const [key, setKey] = useState('volunteer');
    var icon = <>
                <img height="40px"src={Images.volunteer} style={{display:'block'}}></img>
                <p>Volunteer</p>
               </>
    var icon1 = <>
    <img height="40px"src={Images.donation}></img>
    <p >Donate</p>
   </>;
    var icon2 =<>
    <img height="40px"src={Images.collaboration}></img>
    <p >Collaborate</p>
   </>;
    var icon3 = <>
    <img height="40px"src={Images.advocate}></img>
    <p >Advocate</p>
   </>

    return(
        <>

         <h1 style={{ marginTop:'3vh', marginBottom:'2vh'}}>Dashboard</h1>
         <Row>
                <Col md={9}>
                <Tabs
        id="controlled-tab-example"
        activeKey={key}
        onSelect={(k) => setKey(k)}
        style={{justifyContent:'center', borderBottom:'transparent', backgroundColor:'white', display:'flex', flexDirection:'row', marginBottom:'5%', width:'fit-content',marginLeft:'16%',marginTop: '3%'}}
      >
        <Tab eventKey="volunteer" title={icon} style={{opacity:'100'}}>
        <div style={{backgroundColor:'transparent'}}>
        <DonationVolunteer/>
        </div>
       
        </Tab>
        <Tab eventKey="donate" title={icon1} style={{opacity:'100', backgroundColor:'white'}}>
        <Donate/>
        </Tab>
        <Tab eventKey="collaborate" title={icon2} style={{opacity:'100', backgroundColor:'white'}}>
        <DonationCollaborate/>
        </Tab>
        <Tab eventKey="advocate" title={icon3} style={{opacity:'100', backgroundColor:'white'}}>
        <Card className="p-5">
                <h3 style={{marginLeft:'38%', color:'#c4c4c4'}}>Coming Soon</h3>

            </Card>
        </Tab>
      </Tabs>
                </Col>
                <Col md={3}>
                    <OpenTasks/>
                </Col>

         </Row>
           
        </>
    )
}


export default DonationDashboard