import React, { useState, useEffect } from "react";
import "../../index.scss";
import { Table, Card } from "react-bootstrap";
import CustomButton from "../components/CustomButton";
// import { FaEye } from "react-icons/fa";
import axios from "../config/axios";
import { connect } from "react-redux";

const GetProjects = (props) => {
  const getProjectList = () => {
    axios.post("/project/list", { project_creator_id: props.user._id }).then((response) => {
      const data = response.data;
      console.log("response", data);
      setProjectList(data);
    });
  };
  const [projectList, setProjectList] = useState([]);
  console.log("list", projectList);
  useEffect(() => getProjectList(), []);

  // const projectList = [
  //   {
  //     name: "ST Youth Skill Trainging Program 1",
  //     budget: "₹75,00,000",
  //     status: "Open",
  //     health: "Poor",
  //   },
  //   {
  //     name: "ST Youth Skill Trainging Program 1",
  //     budget: "₹75,00,000",
  //     status: "Open",
  //     health: "Poor",
  //   },
  //   {
  //     name: "ST Youth Skill Trainging Program 1",
  //     budget: "₹75,00,000",
  //     status: "Open",
  //     health: "Poor",
  //   },
  //   {
  //     name: "ST Youth Skill Trainging Program 1",
  //     budget: "₹75,00,000",
  //     status: "Open",
  //     health: "Poor",
  //   },
  //   {
  //     name: "ST Youth Skill Trainging Program 1",
  //     budget: "₹75,00,000",
  //     status: "Open",
  //     health: "Poor",
  //   },
  //   {
  //     name: "ST Youth Skill Trainging Program 1",
  //     budget: "₹75,00,000",
  //     status: "Open",
  //     health: "Poor",
  //   },
  //   {
  //     name: "ST Youth Skill Trainging Program 1",
  //     budget: "₹75,00,000",
  //     status: "Open",
  //     health: "Poor",
  //   },
  // ];
  return (
    <div className="container">
      <Card style={{ border: "0" }}>
        <div
          className="project-title"
          style={{
            borderColor: "#e7eaec",
            borderStyle: "solid solid none",
            borderWidth: "1px",
          }}
        >
          <h5 style={{ fontWeight: "600", fontSize: "14px" }}>Project Details</h5>{" "}
        </div>
        <div
          className="ibox-content"
          style={{
            borderColor: "#e7eaec",
            borderStyle: "solid solid none",
            borderWidth: "1px",
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingTop: "15px",
            }}
          >
            <a href="/createProjects">
              <CustomButton content="Create Project" />
            </a>
          </div>

          <div className="beneficiaries-table">
            <Table striped hover size="sm">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Budget</th>
                  <th>Status</th>
                  <th>Health</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {
                  projectList || projectList.length > 0 ?
                    projectList.map((ele, index) => (
                      <tr>
                        <td style={{ padding: "8px", fontSize: "13px", color: "gray" }}>
                          <a href={`/editProject/${ele.project_slug}`} style={{ color: "#46a44b" }}>
                            {ele.project_name}
                          </a>
                        </td>
                        <td style={{ padding: "8px", fontSize: "13px", color: "gray" }}>
                          ₹{ele.estimated_cost}
                        </td>
                        <td style={{ padding: "8px", fontSize: "13px", color: "gray" }}>
                          {ele.project_status}
                        </td>
                        <td style={{ padding: "8px", fontSize: "13px", color: "gray" }}>
                          <spam>Poor</spam>
                        </td>
                        <td style={{ padding: "8px", fontSize: "13px", color: "gray" }}>
                          {" "}
                          <a href="#">
                            <i class="far fa-eye" style={{ color: "#4caf4f ", fontSize: "15px" }}></i>
                          </a>
                        </td>
                      </tr>
                    )) : null}
              </tbody>
            </Table>
            <a href="#" style={{ color: "#46a44b" }}>
              Click here
            </a>{" "}
            to have a look at your project timeline
          </div>
        </div>
      </Card>
    </div>
  );
};
const mapStateToProps = function (state) {
  return {
    user: state.phAuth.user,
  };
};

export default connect(mapStateToProps)(GetProjects);
// export default GetProjects;
