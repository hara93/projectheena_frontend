import React from 'react'
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom"
const DonationReports = () => {
    const donationReport = [
        {
            name: "Setting blood donation drive in Akola",
            amount: 400,
            type: "Public",
            date: '28th Sep, 2015'
        },
        {
            name: "Setting blood donation drive in Akola",
            amount: 400,
            type: "Public",
            date: '28th Sep, 2015'
        },
        {
            name: "Setting blood donation drive in Akola",
            amount: 400,
            type: "Public",
            date: '28th Sep, 2015'
        }
    ]
    return (
        <div className='container'>
            <h2 style={{paddingBottom:"30px"}}>Donation Reports</h2>
            <div className="box-ibox-title p-4">
                <Table responsive="xl" striped className='mb-2'>
                    <thead>
                        <tr>
                            <td className='title-gray'>Name</td>
                            <td className='title-gray'>Amount</td>
                            <td className='title-gray'>Donation Type</td>
                            <td className='title-gray'>Date</td>
                        </tr>
                    </thead>
                    <tbody>
                        {donationReport.map((x, index) => (
                            <tr>
                                <td className='title-gray'>
                                    <Link to="#" className="basecolor">{x.name}</Link>
                                </td>
                                <td className='title-gray'> {`₹ ${x.amount}`}</td>
                                <td className='title-gray'> {x.type}</td>
                                <td className='title-gray'>{x.date}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default DonationReports
