import React, { useState } from "react";
import "../../index.scss";
import "../CorporateSolutions.css"
// import "D:/Daksh/coense projects/demo1/src/index.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Row,
  Col,
  Button,
  Modal,
  Form,
} from "react-bootstrap";
import CustomButton from "../components/CustomButton";
import { Images } from "../config/Images";
import { Link } from "react-router-dom"

const CorporateSolutions = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div style={{ backgroundColor: "white", padding: 0, margin: 0 }}>
        <Modal show={show} onHide={handleClose} animation={false} size="md">
          <div className="modal-header-custom">
            <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
              Feel free to contact us
            </h4>
            <Button variant="secondary" style={{ height: "30px", background: "transparent", border: "1px solid transparent" }} onClick={handleClose}>
              <i class="fas fa-times" style={{ paddingRight: "0", color: "#cacaca" }}></i>
            </Button>
          </div>
          {/* <div
            style={{
              height: "60px",
              borderBottom: "1px solid #cccccc",
              padding: "15px 10px 5px 10px",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <h4>Feel free to contact us</h4>
            <Button
              variant="secondary"
              style={{ height: "30px" }}
              onClick={handleClose}
            >
              <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
            </Button>
          </div> */}
          <Modal.Body>
            <div style={{ minHeight: "150px", padding: "20px" }}>
              <Form>
                <Form.Group>
                  <Form.Row>
                    <Form.Label column="lg" lg={2} className="form-label-custom">
                      Name
                    </Form.Label>
                    <Col>
                      <Form.Control
                        size="lg"
                        type="text"
                        placeholder="Enter your name"
                      />
                    </Col>
                  </Form.Row>
                  <hr />

                  <Form.Row>
                    <Form.Label column="lg" lg={2} className="form-label-custom">
                      Email
                    </Form.Label>
                    <Col>
                      <Form.Control
                        size="lg"
                        type="email"
                        placeholder="Enter your email"
                      />
                    </Col>
                  </Form.Row>
                  <hr />
                  <Form.Row>
                    <Form.Label column="lg" lg={2} className="form-label-custom">
                      Subject
                    </Form.Label>
                    <Col>
                      <Form.Control as="select" size="lg" custom>
                        <option></option>
                        <option>Enquiry</option>
                        <option>Praise/Testimonial</option>
                        <option>Suggestion/Feedback</option>
                        <option>Report a bug/Error</option>
                        <option>Other</option>
                      </Form.Control>
                    </Col>
                  </Form.Row>
                  <hr />
                  <Form.Row>
                    <Form.Label column="lg" lg={2} className="form-label-custom">
                      I am
                    </Form.Label>
                    <Col>
                      <Form.Control as="select" size="lg" custom>
                        <option></option>
                        <option>Volunteer</option>
                        <option>Corporate Volunteer</option>
                        <option>NGO</option>
                        <option>CSR Professional</option>
                        <option>Other</option>
                      </Form.Control>
                    </Col>
                  </Form.Row>
                  <hr />
                  <Form.Row>
                    <Form.Label column="lg" lg={2} className="form-label-custom">
                      Message
                    </Form.Label>
                    <Col>
                      <Form.Control
                        as="textarea"
                        size="lg"
                        placeholder="Your Message"
                      />
                    </Col>
                  </Form.Row>
                </Form.Group>
              </Form>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <CustomButton content="Send feedback" />
          </Modal.Footer>
        </Modal>

        <div style={{ height: "600px", padding: 0, margin: 0 }} className="csr-background-image" >
          <div className="text-center csr-bgimg-child">
            <h3 className="text-white" style={{ marginBottom: "15px" }}>
              One platform to manage and scale your Corporate Social
              Responsibility Engagement.
            </h3>
            <div onClick={handleShow}>
              <CustomButton content="Connect with us" />
            </div>
          </div>
        </div>
        <div>

          <div className="container">
            <Row className="csr-row">
              <div style={{ textAlign: "center" }}>
                <h2>Complete CSR Engagement Platform</h2>
                <p>
                  ProjectHeena provides a complete platform to ensure CSR
                  engagement, scale, transparency and trust. We work closely with
                  stakeholders to understand the real need on ground and develop
                  technology solutions to streamline and scale CSR impact! Following
                  modules are available to corporates.
                </p>
              </div>
            </Row>
            <Row
              style={{
                display: "flex",
                justifyContent: "center",
                justifyItems: "stretch",
                flexGrow: "1",
              }}
            >
              <div style={{ flexGrow: "1" }}>
                <a href="#1">
                  {/* <CustomButtonOutline
                content="CSR Project Management"
               
              /> */}
                  <button className="csr-btn">CSR Project Management</button>
                </a>
              </div>

              <div style={{ flexGrow: "1" }}>
                <a href="#2">
                  {/* <CustomButtonOutline content="Employee Management" /> */}
                  <button className="csr-btn">Employee Management</button>
                </a>
              </div>
              <div style={{ flexGrow: "1" }}>
                <a href="#3">
                  {/* <CustomButtonOutline content="Corporate Philanthropy" /> */}
                  <button className="csr-btn">Corporate Philanthropy</button>
                </a>
              </div>
              <div style={{ flexGrow: "1" }}>
                <a href="#4">
                  {/* <CustomButtonOutline content="Advocacy & Brand Building" /> */}
                  <button className="csr-btn">Advocacy & Brand Building</button>
                </a>
              </div>
            </Row>




            <Row className="csr-row" id="1">
              <Col lg={5}>
                <img
                  src={Images.pm}
                  alt=""
                  height="300"
                  width="350"
                  className="img-center"
                />
              </Col>
              <Col lg={7}>
                <h3 className="corporateSolutions-headings fw-bold">
                  CSR Project Monitoring & Evaluation
                  <sup>(New)</sup>
                </h3>
                <p>
                  While the new state mandates would shape up CSR projects with
                  clear outcomes and specific causes; as an organization you would
                  look for scale and more strategic interventions where your value
                  add is differential and impactful.
                </p>
                <p>
                  <i>
                    The challenge is too many projects with several non profits
                    across multiple geographies. We simplify this with the one of
                    its kind CSR Project Management System, made from ground up with
                    a goal of collaboration.
                  </i>
                </p>
                <p>
                  Whether its about data capturing, auditing, collaboration or
                  monitoring and evaluation we simplify the process & meet every
                  demand of CSR Project Management.
                </p>
                <button className="contact-us-btn" onClick={handleShow}>
                  Contact Us
                </button>{" "}
              </Col>
            </Row>
            <Row className="csr-row" id="2">
              <Col
                lg={{ order: "first" }}
                md={{ order: 7 }}
                xs={{ order: 7 }}
                style={{ paddingRight: "40px" }}
              >
                <h3 className="corporateSolutions-headings fw-bold">Employee Engagement</h3>
                <p>
                  What if you could drive your work force to be a force of good! We
                  help you drive Employee engagement via Corporate Social
                  Responsibility in a serious yet fun and competitive manner.
                  Connecting right people skills with right causes, managing the
                  same and getting them accredited for their work can really boost
                  employee engagement and at the same time build your social brand.
                </p>
                <p>
                  We ensure employees learn and participate in your CSR initiatives
                  and feel part of a larger social good. While we increase your
                  employee engagement, we also bring your operational cost of the
                  same down by automating the whole program.
                </p>
                <button className="contact-us-btn" onClick={handleShow}>
                  Contact Us
                </button>{" "}
              </Col>
              <Col lg={5}>
                <img src={Images.volunteer1} alt="" className="img-center" />
              </Col>
            </Row>
            <Row className="csr-row" id="3">
              <Col lg={5}>
                <img
                  src={Images.donation}
                  alt=""
                  height="300"
                  width="350"
                  className="img-center"
                />
              </Col>
              <Col lg={7}>
                <h3 className="corporateSolutions-headings fw-bold">Corporate Philanthropy</h3>
                <p>
                  Our "Responsible Donations" module help bring the trust and
                  transparency back to corporate philanthropy. Donors can contribute
                  as per their capacities and be updated about how every single cent
                  is being utilised. Our donation drives don't end once the money is
                  raised, rather the main work starts when the money reaches the
                  change makers and projects begin.
                </p>
                <p>
                  Corporates can implant the joy of giving in their employee via
                  various initiatives that are campaign based or work around
                  employee payroll giving. Not mere donations but responsible
                  donations is the key!
                </p>
                <button className="contact-us-btn" onClick={handleShow}>
                  Contact Us
                </button>{" "}
              </Col>
            </Row>
            <Row className="csr-row" id="4">
              <Col lg={{ order: "first" }} md={{ order: 7 }} xs={{ order: 7 }}>
                <h3 className="corporateSolutions-headings fw-bold">Advocacy & Brand Building</h3>
                <p>
                  When all of us join hands we develop a network of impact. Do
                  gooders are never alone they leverage a huge network to reach out
                  and advocate about the causes they want to promote.
                </p>
                <p>
                  The Advocacy module on ProjectHeena helps people share their
                  success stories and inspire others to join. Employees can share
                  their experiences, ideas for change and views in a blog pattern.
                  This also helps nonprofits and foundations reach out to audience
                  beyond their circle of impact and update them about social
                  achievements. Team members with paucity of time, can share updates
                  in their networks and be cause advocates.
                </p>
                <p>
                  In coming days real social advocacy will become the single
                  marketing asset for power initiatives
                </p>
                <button className="contact-us-btn" onClick={handleShow}>
                  Contact Us
                </button>{" "}
              </Col>
              <Col lg={5}>
                <img src={Images.advocacy} alt="" className="img-center" />
              </Col>
            </Row>
            <Row className="csr-row">
              <Col lg={5}>
                <img
                  src={Images.magic}
                  alt=""
                  height="300"
                  width="350"
                  className="img-center"
                />
              </Col>
              <Col lg={7}>
                <h3 className="corporateSolutions-headings fw-bold">A bit of Magic</h3>
                <blockquote style={{ borderLeft: "3px solid gray" }}>
                  There is a real magic in enthusiasm. It spells the difference
                  between mediocrity and accomplishment.
                  <small>Norman Vincent Peale</small>
                </blockquote>
                <p>
                  We dont believe in cookie cutter solutions, hence you will always
                  find us evolving the platform to the satisfy changing demands and
                  being at the helm of change. This means while we understand the
                  base requirements of scaling CSR engagement and devise solutions
                  for the same; we also strive to develop custom solutions and
                  always are open to solving your challenges. Whether its social
                  campaigns or an innovative social project you would want to
                  execute. Reach out to us anytime and let our enthusiasm work out
                  some magic for you.
                </p>
                <button className="contact-us-btn" onClick={handleShow}>
                  Contact Us
                </button>{" "}
              </Col>
            </Row>
            <Row className="csr-row">
              <div style={{ width: "100%", textAlign: "center" }}>
                <h2>Why the ProjectHeena platform is right for your CSR team</h2>
              </div>
            </Row>
            <Row className="no-gutters">
              <Col md={1}></Col>
              <Col md={5}>
                <div style={{ paddingLeft: '25px', paddingRight: '25px' }}>
                  <div style={{ margin: "auto", paddingBottom: 0 }}>
                    <img src={Images.collaboration} className="img-center" />
                  </div>
                  <p style={{ fontSize: 13 }}>
                    While in house built applications are difficult to manage, own and
                    evolve; the primary reason that you need ProjectHeena is the
                    collaboration that it brings to one signle platform.
                  </p>
                  <p >
                    <i>
                      <strong style={{ fontSize: 13, color: 'grey' }}>
                        What can be achieved when all your nonprofits / NGO partners,
                        your employees, auditors and stakeholders collaborate one one
                        single platform to do good?
                      </strong>
                    </i>
                  </p>
                  <p style={{ fontSize: 13 }}>
                    This is a whole new way of solving CSR problems right from its
                    core. And apart from that our Software as a Service approach is
                  </p>
                </div>
              </Col>
              <Col md={5}>
                <div style={{ paddingLeft: '25px', paddingRight: '25px' }}>
                  <Row className="pb-3">
                    <Col md={2}>
                      <img
                        style={{ height: "65%", paddingTop: "10px" }}
                        src={Images.shield}
                        alt=""
                      />
                    </Col>
                    <Col>
                      <h4 className="corporateSolutions-sub-headings fw-bold">Easy to use & Secure</h4>
                      <p>
                        Made like a social network the platform is intuitive,
                        rewarding and sticky. While using least of data and secured by
                        SSL.
                      </p>
                    </Col>
                  </Row>
                  <Row className="pb-3">
                    <Col md={2} style={{ paddingTop: "10px" }}>
                      <img style={{ height: "65%" }} src={Images.electrical} alt="" />
                    </Col>
                    <Col>
                      <h4 className="corporateSolutions-sub-headings fw-bold">Plug & Play</h4>
                      <p>
                        You dont need to procure licenses, install or manage products.
                        We manage everything so that you concentrate on IMPACT
                      </p>
                    </Col>
                  </Row>
                  <Row className="pb-3">
                    <Col md={2}>
                      <img
                        style={{ height: "65%", paddingTop: "10px" }}
                        src={Images.engineering}
                        alt=""
                      />
                    </Col>
                    <Col>
                      <h4 className="corporateSolutions-sub-headings fw-bold">Ever Evolving</h4>
                      <p>
                        We devise the best ways of managing CSR initiatives. Our SaaS
                        approach gives you all the updates without any additional cost
                        or AMC. All included, no strings attached.
                      </p>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col md={1}></Col>
            </Row>
          </div>


          {/* <Card style={{ backgroundColor: "#556273", textAlign: "center" }}> */}
          <div style={{ marginTop: 50, padding: "30px 15px 30px 15px", backgroundColor: "#556273", textAlign: "center" }}>
            <h1 className="text-white">
              <strong>Need help?</strong> Call our CSR Solutions expert at (+91) 8080-448-848.
              <br />
              <i class="far fa-envelope" style={{ fontSize: "30px" }}></i> Mail -{" "}
              <Link to="mailto:team@projectheena.com" className="csr-maillink">
                team@projectheena.com
              </Link>
            </h1>
          </div>
          {/* </Card> */}
        </div>
      </div>
    </>
  );
};
export default CorporateSolutions;
