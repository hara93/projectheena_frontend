import React from 'react';
import HorizontalTabNav from './HorizontalTabNav';
import TabContent from './TabContent';
import { Row, Col } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';
import OpenTasks from '../components/OpenTasks';
import DonationVolunteer from './DonationVolunteer';
import HorizontalTabContent from './HorizonatalTabContent';
import Donate from './Donate';
import DonationCollaborate from './DonationCollaborate';

class MyDonation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: 'Volunteer'
    }
  }

  setSelected = (tab) => {
    this.setState({ selected: tab });
  }

  render() {
    return (
      <>
        <div className="container-fluid">
          <h2 className="font-bold">
            <span id="page-title">Dashboard</span></h2>
          <Row className="p-6">
            <Col md={9}>

              <HorizontalTabNav tabs={[{ name: 'Volunteer', img: 'http://projectheena.in/assets/img/dashboard/volunteer.png' }, { name: 'Donate', img: 'http://projectheena.in/assets/img/dashboard/donate.png' }, { name: 'Collaborate', img: 'http://projectheena.in/assets/img/dashboard/collaborate.png' }, { name: 'Advocate', img: 'http://projectheena.in/assets/img/dashboard/advocate.png' }]} selected={this.state.selected} setSelected={this.setSelected}>
                <HorizontalTabContent isSelected={this.state.selected === 'Volunteer'}>
                  <DonationVolunteer />
                </HorizontalTabContent>
                <HorizontalTabContent isSelected={this.state.selected === 'Donate'}>
                  <Donate />
                </HorizontalTabContent>

                <HorizontalTabContent isSelected={this.state.selected === 'Collaborate'}>
                  <DonationCollaborate />
                </HorizontalTabContent>

                <HorizontalTabContent isSelected={this.state.selected === 'Advocate'}>
                  <Card className="p-5">
                    <h3 style={{ marginLeft: '38%', color: '#c4c4c4' }}>Coming Soon</h3>

                  </Card>
                </HorizontalTabContent>
              </HorizontalTabNav>

            </Col>

            <Col md={3}>
              <OpenTasks />

            </Col>
          </Row>
        </div>

      </>
    )
  }
}

export default MyDonation