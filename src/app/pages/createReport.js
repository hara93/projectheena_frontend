/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Tab, Tabs, Col, Button, Card, Modal, Form } from "react-bootstrap";
import CustomButton from "../components/CustomButton";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const CreateReport = () => {
  const [show, setShow] = useState(false);
  const [key, setKey] = useState("home");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <Modal show={show} onHide={handleClose} animation={false} centered>
        <div
          style={{
            height: "60px",
            borderBottom: "1px solid #cccccc",
            padding: "15px 10px 5px 10px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h4>Select Skills</h4>
          <Button
            variant="secondary"
            style={{ height: "30px" }}
            onClick={handleClose}
          >
            <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ borderBottom: "1px solid #cccccc" }}>
            <Tabs
              id="controlled-tab-example"
              activeKey={key}
              onSelect={(k) => setKey(k)}
            >
              <Tab
                eventKey="home"
                title="Follow-up of Migrated children"
                style={{ opacity: "100" }}
              >
                <div style={{ minHeight: "100px", display: "grid" }}>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Tracking & Attendance Status.."
                    value="Tracking & Attendance Status.."
                  />
                </div>
              </Tab>
              <Tab
                eventKey="Follow-up of Migrated children"
                title="Follow-up of Migrated children"
                style={{ opacity: "100" }}
              >
                <div style={{ minHeight: "100px", display: "grid" }}>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Child's Status"
                    value="Child's Status"
                  />
                </div>
              </Tab>
              <Tab
                eventKey="School transport"
                title="School transport"
                style={{ opacity: "100" }}
              >
                <div style={{ minHeight: "100px", display: "grid" }}>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Type of transport to school"
                    value="Type of transport to school"
                  />
                </div>
              </Tab>
              <Tab
                eventKey="School Enrollment"
                title="School Enrollment"
                style={{ opacity: "100" }}
              >
                <div style={{ minHeight: "100px", display: "grid" }}>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Child Enrolled in std"
                    value="Child Enrolled in std"
                  />
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Enrolled by?(type)"
                    value="Enrolled by?(type)"
                  />
                </div>
              </Tab>
              <Tab
                eventKey="Survey of wards in PCMC area"
                title="Survey of wards in PCMC area"
                style={{ opacity: "100" }}
              >
                <div style={{ minHeight: "100px", display: "grid" }}>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Ward..."
                    value="Ward..."
                  />
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Site type..."
                    value="Site type..."
                  />
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Have children?..."
                    value="Have children?..."
                  />
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Children go to school?..."
                    value="Children go to school?..."
                  />
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Other NGOs working?..."
                    value="Other NGOs working?..."
                  />
                </div>
              </Tab>
              <Tab
                eventKey="Survey of sites in PCMC area"
                title="Survey of sites in PCMC area"
                style={{ opacity: "100" }}
              >
                <div style={{ minHeight: "100px", display: "grid" }}>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Ward number..."
                    value="Ward number..."
                  />
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Ward name..."
                    value="Ward name..."
                  />
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Type of site..."
                    value="Type of site..."
                  />
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Child's Age..."
                    value="Child's Age..."
                  />
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label="Already Enrolled in School?..."
                    value="Already Enrolled in School?..."
                  />
                </div>
              </Tab>
            </Tabs>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Select Questions" />
        </Modal.Footer>
      </Modal>

      <div style={{ marginBottom: "30px" }}>
        <h2 className="font-bold">Create Report</h2>
        <p>
          for{" "}
          <a href="" className="basecolor">
            Every Child Counts _ A Citizens Campaign
          </a>
        </p>
      </div>

      <Card style={{ padding: "30px" }}>
        <Form>
          <Form.Group>
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Report Name
              </Form.Label>
              <Col>
                <Form.Control
                  size="lg"
                  type="text"
                  placeholder="Enter Name"
                  className="green-focus"
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Description
              </Form.Label>
              <Col>
                <Form.Control size="lg" as="textarea" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Receiver Name
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="text" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Receiver Email
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="email" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Mail Frequency
              </Form.Label>
              <Col>
                <Form.Control as="select" size="lg" custom>
                  <option>Daily</option>
                  <option>Weekly</option>
                  <option>Monthly</option>
                  <option>Quarterly</option>
                </Form.Control>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Questions
              </Form.Label>
              <Col>
                <div onClick={handleShow}>
                  <button className="report-questions-btn" type="button">
                    Questions
                  </button>
                  <Form.Text className="text-muted" muted>
                    0 questions selected
                  </Form.Text>
                </div>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Add to dashboard
              </Form.Label>
              <Col>
                <Form.Control as="select" size="lg" custom>
                  <option>No</option>
                  <option>Yes</option>
                </Form.Control>
                <Form.Text className="text-muted" muted>
                  Select yes if you want your reports to be shown on project
                  dashboard
                </Form.Text>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2}></Form.Label>
              <Col>
                <CustomButton content="Create Report" />
              </Col>
            </Form.Row>
          </Form.Group>
        </Form>
      </Card>
    </>
  );
};
export default CreateReport;
