/* eslint-disable no-restricted-imports */
import React from "react";
import { Row, Col, Card, FormControl } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import PlacesAutoComplete from "../components/PlacesAutoComplete";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import CustomButton from "../components/CustomButton";
import CustomButtonOutline from "../components/CustomButtonOutline";

const SearchBeneficiary = () => {
  return (
    <>
      <div style={{ padding: "30px 0" }}>
        <h1 style={{ fontSize: "30px" }}>
          Search: 1 Beneficiaries found that matches your query
        </h1>
      </div>
      <div>
        <Row>
          <Col md={3}>
            <Card style={{ padding: "15px 15px 0 15px" }}>
              <Form>
                <Form.Group>
                  <Form.Row>
                    <Form.Label column="lg" className="search-ben-form">
                      Type
                    </Form.Label>

                    <Form.Control as="select" custom>
                      <option></option>
                      <option>Person</option>
                      <option>abstract</option>
                    </Form.Control>
                  </Form.Row>
                </Form.Group>
                <Form.Group>
                  <Form.Row>
                    <Form.Label column="lg" className="search-ben-form">
                      Name
                    </Form.Label>

                    <Form.Control type="text" className="green-focus" />
                  </Form.Row>
                </Form.Group>
                <Form.Group>
                  <Form.Row>
                    <Form.Label column="lg" className="search-ben-form">
                      Age
                    </Form.Label>

                    <InputGroup>
                      <FormControl
                        type="number"
                        className="green-focus"
                        placeholder="Enter Age"
                      />
                      <InputGroup.Append>
                        <InputGroup.Text id="basic-addon2">
                          Years
                        </InputGroup.Text>
                      </InputGroup.Append>
                    </InputGroup>
                  </Form.Row>
                </Form.Group>
                <Form.Group>
                  <Form.Row>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        width: "100%",
                      }}
                    >
                      <Form.Label
                        column="lg"
                        className="search-ben-form"
                        style={{ paddingLeft: "5px" }}
                      >
                        Location
                      </Form.Label>
                      <PlacesAutoComplete />
                    </div>
                  </Form.Row>
                </Form.Group>
                <Form.Group>
                  <Form.Row
                    style={{ justifyContent: "center", marginBottom: "15px" }}
                  >
                    <CustomButtonOutline content="Select Beneficiary" />
                  </Form.Row>
                </Form.Group>
              </Form>
            </Card>
          </Col>
          <Col>
            <Row>
              <Col md={6}>
                <Card
                  style={{
                    padding: "10px",
                    marginBottom: "20px",
                    minHeight: "300px",
                  }}
                >
                  <h4 style={{ fontSize: "14px", fontWeight: "600" }}>
                    <a href="" className="basecolor">
                      Raj
                    </a>{" "}
                  </h4>
                  <ul className="search-ben-ul">
                    <li>
                      <i
                        class="fas fa-map-marker-alt"
                        style={{ color: "inherit" }}
                      ></i>
                      <span> Pune,Maharashtra,India</span>
                    </li>
                    <li>
                      <i class="fas fa-user" style={{ color: "inherit" }}></i>
                      <span> Age :</span>
                    </li>
                  </ul>
                  <div>
                    {" "}
                    <FormControlLabel
                      control={<Checkbox color="primary" />}
                      label="Select"
                      value="Select"
                    />
                  </div>
                </Card>
              </Col>
              <Col md={6}>
                <Card
                  style={{
                    padding: "10px",
                    marginBottom: "20px",
                    minHeight: "300px",
                  }}
                >
                  <h4 style={{ fontSize: "14px", fontWeight: "600" }}>
                    <a href="" className="basecolor">
                      Raj
                    </a>{" "}
                  </h4>
                  <ul className="search-ben-ul">
                    <li>
                      <i
                        class="fas fa-map-marker-alt"
                        style={{ color: "inherit" }}
                      ></i>
                      <span> Pune,Maharashtra,India</span>
                    </li>
                    <li>
                      <i class="fas fa-user" style={{ color: "inherit" }}></i>
                      <span> Age :</span>
                    </li>
                  </ul>
                  <div>
                    {" "}
                    <FormControlLabel
                      control={<Checkbox color="primary" />}
                      label="Select"
                      value="Select"
                    />
                  </div>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <CustomButton content="Map Beneficiary" />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </>
  );
};
export default SearchBeneficiary;
