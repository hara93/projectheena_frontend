import React from 'react';

import * as ReactBootstrap from 'react-bootstrap';
import WorkIcon from '@material-ui/icons/Work';
import LocationOnIcon from '@material-ui/icons/LocationOn';


class PeoplePage extends React.Component {


  render() {
    return (
      <>
        <h1 style={{ color: 'gray', paddingLeft: '3vw', marginTop: '3vh', marginBottom: '3vh' }}>Search: 2990 Volunteers found that matches your query</h1>

        <div className="container">
          <div className="row">
            <div className="col-md-3">

            </div>
            <div className="col-md-6">
              <div className="tabs">

                <div className="btn-group btn-group-justified">
                  <a href="/people" className="btn btn-white btn-sm active"> People </a>
                  <a href="/search" className="btn btn-white btn-sm "> Tasks</a>
                  <a href="/project" className="btn btn-white btn-sm "> Project </a>
                  <a href="/donation" className="btn btn-white btn-sm "> Donation </a>

                </div>
              </div>
            </div>

          </div>

        </div>
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-lg-3" style={{ height: '40%', marginTop: '4vh' }}>
              <div class="filters">


                <div className="filter_heading" style={{ marginTop: '2vh', marginBottom: '1vh' }}>Would Like to help on</div>
                <select className="cause">
                  <option >All Causes</option>
                  <option>Animal Welfare</option>
                  <option>Youth and Children </option>
                  <option>Women </option>

                </select>

                <div className="filter_heading" style={{ marginTop: '2vh', marginBottom: '1vh' }}>Available on</div>
                <select className="cause">
                  <option >All Days</option>
                  <option>Monday</option>
                  <option>Tuesday </option>
                  <option>Wednesday </option>

                </select>





                <div className="filter_heading" style={{ marginTop: '1vh', display: 'block' }}>Lives in</div>
                <input className="tags" type="" placeholder="Enter your City" />


                <div className="filter_heading" style={{ display: 'block' }}>Which requires following skills</div>
                <div><input className="skill" type="text" /> </div>
                <button className="clear" >x</button>




                <div>
                  <button type="submit" className="task">Search Volunteers</button>
                </div>



              </div>

            </div>

            <div className="col-sm-12 col-lg-8">
              <div className="row">
                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-4">
                        <img
                          style={{ width: '60%', height: '50%', borderRadius: '50%', margin: '2vh 0vw 2vh 2vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" />

                      </div>
                      <div className="col-8">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 0vw', width: '100%' }}>ABHISHEK DWIVEDI</h5>
                        <div style={{ marginBottom: '1vh', marginTop: '1vh' }}><LocationOnIcon />Delhi, India</div>
                        <div style={{ marginBottom: '1vh', marginRight: '1vw' }}><WorkIcon />Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising</div>
                      </div>

                    </div>






                  </div>

                </div>
                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-4">
                        <img
                          style={{ width: '60%', height: '50%', borderRadius: '50%', margin: '2vh 0vw 2vh 2vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" />

                      </div>
                      <div className="col-8">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 0vw', width: '100%' }}>Rishabh Negi</h5>
                        <div style={{ marginBottom: '1vh', marginTop: '1vh' }}><LocationOnIcon />Delhi, India</div>
                        <div style={{ marginBottom: '1vh', marginRight: '1vw' }}><WorkIcon />Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising</div>
                      </div>

                    </div>






                  </div>

                </div>

                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-4">
                        <img
                          style={{ width: '60%', height: '50%', borderRadius: '50%', margin: '2vh 0vw 2vh 2vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" />

                      </div>
                      <div className="col-8">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 0vw', width: '100%' }}>Aman Singh</h5>
                        <div style={{ marginBottom: '1vh', marginTop: '1vh' }}><LocationOnIcon />Delhi, India</div>
                        <div style={{ marginBottom: '1vh', marginRight: '1vw' }}><WorkIcon />Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising</div>
                      </div>

                    </div>






                  </div>

                </div>

                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-4">
                        <img
                          style={{ width: '60%', height: '50%', borderRadius: '50%', margin: '2vh 0vw 2vh 2vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" />

                      </div>
                      <div className="col-8">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 0vw', width: '100%' }}>Simran Malhotra</h5>
                        <div style={{ marginBottom: '1vh', marginTop: '1vh' }}><LocationOnIcon />Delhi, India</div>
                        <div style={{ marginBottom: '1vh', marginRight: '1vw' }}><WorkIcon />Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising</div>
                      </div>

                    </div>






                  </div>

                </div>
                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-4">
                        <img
                          style={{ width: '60%', height: '50%', borderRadius: '50%', margin: '2vh 0vw 2vh 2vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" />

                      </div>
                      <div className="col-8">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 10w', width: '100%' }}>Raj Avasthi</h5>
                        <div style={{ marginBottom: '1vh', marginTop: '1vh' }}><LocationOnIcon />Delhi, India</div>
                        <div style={{ marginBottom: '1vh', marginRight: '1vw' }}><WorkIcon />Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising</div>
                      </div>

                    </div>






                  </div>

                </div>

                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-4">
                        <img
                          style={{ width: '60%', height: '50%', borderRadius: '50%', margin: '2vh 0vw 2vh 2vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" />

                      </div>
                      <div className="col-8">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 0vw', width: '100%' }}>Jia Shukla</h5>
                        <div style={{ marginBottom: '1vh', marginTop: '1vh' }}><LocationOnIcon />Delhi, India</div>
                        <div style={{ marginBottom: '1vh', marginRight: '1vw' }}><WorkIcon />Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising</div>
                      </div>

                    </div>






                  </div>

                </div>

                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-4">
                        <img
                          style={{ width: '60%', height: '50%', borderRadius: '50%', margin: '2vh 0vw 2vh 2vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" />

                      </div>
                      <div className="col-8">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 0vw', width: '100%' }}>Shanaya Kapoor</h5>
                        <div style={{ marginBottom: '1vh', marginTop: '1vh' }}><LocationOnIcon />Delhi, India</div>
                        <div style={{ marginBottom: '1vh', marginRight: '1vw' }}><WorkIcon />Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising</div>
                      </div>

                    </div>






                  </div>

                </div>

                <div className="col-sm-12 col-lg-6" >
                  <div className="task_card">
                    <div className="row">
                      <div className="col-4">
                        <img
                          style={{ width: '60%', height: '50%', borderRadius: '50%', margin: '2vh 0vw 2vh 2vw' }} src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2" />

                      </div>
                      <div className="col-8">
                        <h5 style={{ color: '#4CAF50', padding: '1vh 1vw 1vh 0vw', width: '100%' }}>Aditi Sachdeva</h5>
                        <div style={{ marginBottom: '1vh', marginTop: '1vh' }}><LocationOnIcon />Delhi, India</div>
                        <div style={{ marginBottom: '1vh', marginRight: '1vw' }}><WorkIcon />Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising</div>
                      </div>

                    </div>






                  </div>

                </div>


              </div>
            </div>











          </div>

        </div>

        <ReactBootstrap.Pagination style={{ marginLeft: '40%' }}>

          <ReactBootstrap.Pagination.Prev />
          <ReactBootstrap.Pagination.Item>{1}</ReactBootstrap.Pagination.Item>


          <ReactBootstrap.Pagination.Item>{2}</ReactBootstrap.Pagination.Item>
          <ReactBootstrap.Pagination.Item>{3}</ReactBootstrap.Pagination.Item>
          <ReactBootstrap.Pagination.Item active>{4}</ReactBootstrap.Pagination.Item>

          <ReactBootstrap.Pagination.Ellipsis />
          <ReactBootstrap.Pagination.Item>{20}</ReactBootstrap.Pagination.Item>

          <ReactBootstrap.Pagination.Next />

        </ReactBootstrap.Pagination>

      </>
    )
  }
}

export default PeoplePage;