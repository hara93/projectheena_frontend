import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import { Images } from "../config/Images";

import NGOlogin from "./NGOlogin";
import UserLogin from "./UserLogin";
import CorporateLogin from "./CorporateLogin";

const SignUp = () => {
  const [key, setKey] = useState("profile");
  var icon = (
    <>
      <img src={Images.ngo} alt="" height="30px" style={{ paddingRight: '7px' }}></img>
      <span
        style={{
          fontSize: "1.5rem",
          color: "#686b6d",
        }}
      >
        NGO / Non-Profit
      </span>
    </>
  );
  var icon1 = (
    <div >
      <img src={Images.userlogin} alt="" height="30px" style={{ paddingRight: '7px' }}></img>
      <span
        style={{
          fontSize: "1.5rem",
          color: "#686b6d",
        }}
      >
        User
      </span>
    </div>
  );
  var icon2 = (
    <>
      <img src={Images.team} alt="" height="30px" style={{ paddingRight: '7px' }}></img>
      <span
        style={{
          fontSize: "1.5rem",
          color: "#686b6d",
        }}
      >
        Corporate
      </span>
    </>
  );

  return (
    <>
      <div style={{ maxWidth: "970px", margin: "4rem auto" }}>
        <Tabs
          tabClassName="custom-signup-link"
          id="controlled-tab-example"
          activeKey={key}
          onSelect={(k) => setKey(k)}
          style={{ display: "flex", justifyContent: "center", border: '1px solid transparent' }}
        >
          <Tab eventKey="ngo" title={icon} style={{ opacity: "100%" }} tabClassName="custom-signup-link">
            <NGOlogin />
          </Tab>
          <Tab eventKey="profile" title={icon1} style={{ opacity: "100%", border: '1px solid transparent' }} tabClassName="custom-signup-link">
            <UserLogin />
          </Tab>
          <Tab eventKey="contact" title={icon2} style={{ opacity: "100%" }} tabClassName="custom-signup-link">
            <CorporateLogin />
          </Tab>
        </Tabs>

        <p style={{ margin: "20px 0 0 0", textAlign: "center" }}>
          This portal best works on Google Chrome.
          <img src="http://projectheena.in/assets/img/chrome.jpg" alt="chrome" width="30"></img>
        </p>
      </div>
    </>
  );
};

export default SignUp;
