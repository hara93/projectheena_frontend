import React, { useState } from "react";
import {
  Card,
  Col,
  Row,
  Modal,
  Button,
  Form,
  Container,
} from "react-bootstrap";
import Question from "../components/Question";
import CallSplitIcon from "@material-ui/icons/CallSplit";
import CustomButton from "../components/CustomButton";
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
import SurveyImage from "./SurveyImage";
import SurveyVideo from "./SurveyVideo";
import SurveyGeoTag from "./SurveyGeoTag";
import QuestionType from "../components/SurveyQuestionType";
import SurveyLikert from "./SurveyLikert";
import SurveyPhone from "./SurveyPhone";
import SingleChoice from "../components/SingleChoice";
import MultipleChoice from "../components/multipleChoice";
import Text from "../components/text";
import Number from "../components/number";
import Location from "../components/location";
import Date from "../components/date";
import Time from "../components/time";
import Note from "../components/note";
import Signature from "../components/signature";
import SectionBreak from "../components/sectionBreak";
import AreaOnMap from "../components/areaOnMap";
import DistanceOnMap from "../components/distanceOnMap";
import SurveyScale from "./SurveyScale";

const GroupNumber = () => {
  const [show, setShow] = useState(false);
  const [click, setClick] = useState(false);

  const [image, setImage] = useState(false);
  const [video, setVideo] = useState(false);
  const [geotag, setGeotag] = useState(false);

  const [phone, setPhone] = useState(false);
  const [email, setEmail] = useState(false);
  const [audio, setAudio] = useState(false);
  const [file, setFile] = useState(false);

  const [single, setSingle] = useState(false);
  const [multiple, setMultiple] = useState(false);
  const [text, setText] = useState(false);
  const [number, setNumber] = useState(false);
  const [location, setLocation] = useState(false);
  const [date, setDate] = useState(false);
  const [time, setTime] = useState(false);
  const [note, setNote] = useState(false);
  const [sign, setSign] = useState(false);
  const [sectionbreak, setSection] = useState(false);
  const [area, setArea] = useState(false);
  const [distance, setDistance] = useState(false);

  const [likert, setLikert] = useState(false);
  const [scale, setScale] = useState(false);
  const [rating, setRating] = useState(false);

  const [outer, setOuter] = useState(true);
  const [select, setSelect] = useState(false);
  const [bars, setBars] = useState([]);
  const [question, setQuestion] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleClick = () => {
    setClick(true);
    setOuter(true);
  };
  const handleCloseClick = () => {
    setClick(false);
    setImage(false);
    setVideo(false);
    setGeotag(false);
    setLikert(false);
    setPhone(false);
    setEmail(false);
    setAudio(false);
    setFile(false);
    setSingle(false);
    setMultiple(false);
    setText(false);
    setLocation(false);
    setNumber(false);
    setDate(false);
    setTime(false);
    setNote(false);
    setSign(false);
    setSection(false);
    setArea(false);
    setDistance(false);
    setLikert(false);
    setRating(false);
    setScale(false);
  };

  const handleButtonClicks = (val) => {
    if (val === 1) {
      setImage(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 1", value: "Image", number: "2.1" },
      ]);
    } else if (val === 2) {
      setGeotag(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 2", value: "GeoTag", number: "2.2" },
      ]);
    } else if (val === 3) {
      setPhone(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 3", value: "Phone", number: "2.5" },
      ]);
    } else if (val === 4) {
      setEmail(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 4", value: "Email", number: "2.6" },
      ]);
    } else if (val === 5) {
      setAudio(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 5", value: "Audio", number: "2.7" },
      ]);
    } else if (val === 6) {
      setVideo(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 6", value: "Video", number: "2.2" },
      ]);
    } else if (val === 7) {
      setFile(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 7", value: "File", number: "2.8" },
      ]);
    } else if (val === 8) {
      setSingle(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 8", value: "Single", number: "2.9" },
      ]);
    } else if (val === 9) {
      setMultiple(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Multiple", number: "2.10" },
      ]);
    } else if (val === 10) {
      setText(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Text", number: "2.10" },
      ]);
    } else if (val === 11) {
      setNumber(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Number", number: "2.10" },
      ]);
    } else if (val === 12) {
      setLocation(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Location", number: "2.10" },
      ]);
    } else if (val === 13) {
      setDate(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Date", number: "2.10" },
      ]);
    } else if (val === 14) {
      setTime(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Time", number: "2.10" },
      ]);
    } else if (val === 15) {
      setNote(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Note", number: "2.10" },
      ]);
    } else if (val === 16) {
      setSign(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Sign", number: "2.10" },
      ]);
    } else if (val === 17) {
      setSection(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Section", number: "2.10" },
      ]);
    } else if (val === 18) {
      setArea(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Area on map", number: "2.10" },
      ]);
    } else if (val === 19) {
      setDistance(true);
      setOuter(false);
      setBars([
        ...bars,
        {
          name: "This is my question 9",
          value: "Distance on map",
          number: "2.10",
        },
      ]);
    } else if (val === 20) {
      setLikert(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Likert", number: "2.10" },
      ]);
    } else if (val === 21) {
      setScale(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Scale", number: "2.10" },
      ]);
    } else if (val === 22) {
      setRating(true);
      setOuter(false);
      setBars([
        ...bars,
        { name: "This is my question 9", value: "Rating", number: "2.10" },
      ]);
    }
  };
  const handleSave = () => {
    setSelect(true);
    setShow(false);
  };
  const handleSelect = (e) => {
    setQuestion(e.target.value);
  };

  return (
    <>
      <Card className="p-4">
        <Question />
        <Row className="p-4" style={{ marginTop: "3%" }}>
          <Col md={6}>
            <span
              className="p-2"
              style={{
                backgroundColor: "#f4f5f5",
                color: "#2d3e4e",
                borderRadius: "6px",
              }}
            >
              1
            </span>
            <span
              style={{
                fontWeight: "600",
                color: "#52504f",
                marginLeft: "0.5rem",
              }}
            >
              Repeat Group{" "}
            </span>
            <p style={{ marginTop: "3%" }}>
              {" "}
              On the basis of answer of a number of question
            </p>
          </Col>
        </Row>
        {select == true ? (
          <Row>
            <Col md={10}>
              <p
                className="p-3"
                style={{ width: "100%", border: "1px solid #f0f0f0" }}
              >
                {question}
              </p>
            </Col>
            <Col md={2}>
              <a onClick={handleShow}>
                <i class="fas fa-edit fa-2x"></i> Change
              </a>
            </Col>
          </Row>
        ) : (
          <div
            onClick={handleShow}
            style={{ marginLeft: "5%", marginBottom: "5%" }}
          >
            <CustomButton content={`Select a Number Question  `} />
          </div>
        )}

        <Row className="p-4">
          <Col md={6}>
            <span
              className="p-2"
              style={{
                backgroundColor: "#f4f5f5",
                color: "#2d3e4e",
                borderRadius: "6px",
              }}
            >
              2
            </span>
            <span
              style={{
                fontWeight: "600",
                color: "#52504f",
                marginLeft: "0.5rem",
              }}
            >
              Add Child-Question(s){" "}
            </span>
          </Col>
          <Col md={6}>
            <button
              className="p-3"
              style={{
                backgroundColor: "#f8f9fa",
                border: "1px solid #e6e6e6",
                width: "90%",
              }}
            >
              <CallSplitIcon />
              Rules
            </button>
          </Col>
        </Row>

        <div className="p-5">
          <QuestionType bar={bars} />
        </div>

        <button
          className="p-2 add_ques"
          onClick={handleClick}
          style={{
            background: "transparent",
            color: "#4caf4f",
            fontWeight: "600",
            border: "none",
            marginTop: "5%",
          }}
        >
          <h4>
            {" "}
            <i
              className="fas fa-plus-circle add-ques"
              style={{ color: "#4caf4f" }}
            ></i>
            Add New Child Question...
          </h4>
        </button>

        <Modal
          show={show}
          onHide={handleClose}
          style={{ opacity: "100" }}
          size="lg"
          centered
        >
          <Modal.Header closeButton></Modal.Header>
          <Modal.Body>
            <h4>
              <QuestionAnswerIcon /> Select a Number Question{" "}
            </h4>

            <p style={{ marginTop: "3%", marginBottom: "3%" }}>
              This group will be repeated based on answer of a number question.
            </p>

            <Form>
              <Form.Group controlId="exampleForm.SelectCustom" onSelect>
                <Form.Label>Select a Question</Form.Label>
                <Form.Control as="select" custom onChange={handleSelect}>
                  <option>What's your name?</option>
                  <option>What's your name?</option>
                  <option>What's your age?</option>
                  <option>What's your qualification</option>
                  <option>What's your height?</option>
                </Form.Control>
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <button
              className="p-2"
              onClick={handleClose}
              style={{
                backgroundColor: "#c2c2c2",
                border: "1px solid #c2c2c2",
                color: "white",
              }}
            >
              Cancel
            </button>

            <button
              className="p-2"
              onClick={handleSave}
              style={{
                backgroundColor: "#4caf4f",
                border: "1px solid #4caf4f",
                color: "white",
              }}
            >
              Save
            </button>
          </Modal.Footer>
        </Modal>

        <Modal
          show={click}
          onHide={handleClick}
          style={{ opacity: "100" }}
          size="lg"
          centered
        >
          <Modal.Body>
            {outer == false ? null : (
              <>
                <Container className="p-5">
                  <Row style={{ margin: "0", padding: "0" }}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "100%",
                      }}
                    >
                      <div style={{ display: "flex" }}>
                        <h5 style={{ marginTop: "auto", marginBottom: "auto" }}>
                          Popular Question Types
                        </h5>
                        <div
                          style={{
                            backgroundColor: "black",
                            color: "white",
                            fontWeight: "bold",
                            borderRadius: "4px",
                            textAlign: "center",
                            height: "20px",
                            width: "20px",
                            marginTop: "auto",
                            marginBottom: "auto",
                          }}
                        >
                          12
                        </div>
                      </div>
                      <div>
                        <Button variant="light">
                          <i class="fas fa-times"></i>
                        </Button>
                      </div>
                    </div>
                  </Row>
                  <Row
                    style={{ margin: "0 0 15px 0", padding: "0" }}
                    className="p-4"
                  >
                    <Row style={{ margin: "0", width: "100%" }}>
                      <Col style={{ margin: "0", padding: "5px " }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(8);
                          }}
                        >
                          Single Choice
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(9);
                          }}
                        >
                          Multiple Choice
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(10);
                          }}
                        >
                          Text
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(11);
                          }}
                        >
                          Number
                        </Button>
                      </Col>
                    </Row>
                    <Row style={{ margin: "0", width: "100%" }}>
                      <Col style={{ margin: "0", padding: "5px " }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "4px",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(12);
                          }}
                        >
                          Location
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(13);
                          }}
                        >
                          Date
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(14);
                          }}
                        >
                          Time
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(15);
                          }}
                        >
                          Note
                        </Button>
                      </Col>
                    </Row>
                    <Row style={{ margin: "0", width: "100%" }}>
                      <Col style={{ margin: "0", padding: "5px " }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "4px",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(16);
                          }}
                        >
                          Signature
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(17);
                          }}
                        >
                          Section Break
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(18);
                          }}
                        >
                          Area on Map
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(19);
                          }}
                        >
                          Distance on Map
                        </Button>
                      </Col>
                    </Row>
                  </Row>

                  <Row style={{ margin: "0", padding: "0" }} className="p-4">
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "100%",
                      }}
                    >
                      <div style={{ display: "flex" }}>
                        <h5
                          style={{
                            marginTop: "auto",
                            marginBottom: "auto",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                        >
                          Media And Contacts
                        </h5>
                        <div
                          style={{
                            backgroundColor: "black",
                            color: "white",
                            fontWeight: "bold",
                            borderRadius: "4px",
                            textAlign: "center",
                            height: "20px",
                            width: "20px",
                            marginTop: "auto",
                            marginBottom: "auto",
                          }}
                        >
                          7
                        </div>
                      </div>
                    </div>
                  </Row>
                  <Row
                    style={{ margin: "0 0 15px 0", padding: "0" }}
                    className="p-4"
                  >
                    <Row style={{ margin: "0", width: "100%" }}>
                      <Col style={{ margin: "0", padding: "5px " }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "4px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(1);
                          }}
                        >
                          Image
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(2);
                          }}
                        >
                          Image Geo Tag
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(3);
                          }}
                        >
                          Phone
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(4);
                          }}
                        >
                          Email
                        </Button>
                      </Col>
                    </Row>
                    <Row style={{ margin: "0", width: "100%" }}>
                      <Col style={{ margin: "0", padding: "5px " }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "4px",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(5);
                          }}
                        >
                          Audio
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(6);
                          }}
                        >
                          Video
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(7);
                          }}
                        >
                          File Upload
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}></Col>
                    </Row>
                  </Row>
                  <Row style={{ margin: "0", padding: "0" }} className="p-4">
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "100%",
                      }}
                    >
                      <div style={{ display: "flex" }}>
                        <h5 style={{ marginTop: "auto", marginBottom: "auto" }}>
                          Feedback
                        </h5>
                        <div
                          style={{
                            backgroundColor: "black",
                            color: "white",
                            fontWeight: "bold",
                            borderRadius: "4px",
                            textAlign: "center",
                            height: "20px",
                            width: "20px",
                            marginTop: "auto",
                            marginBottom: "auto",
                          }}
                        >
                          3
                        </div>
                      </div>
                    </div>
                  </Row>
                  <Row
                    style={{ margin: "0 0 15px 0", padding: "0" }}
                    className="p-4"
                  >
                    <Row style={{ margin: "0", width: "100%" }}>
                      <Col style={{ margin: "0", padding: "5px " }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "4px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(20);
                          }}
                        >
                          Likert Scale
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(21);
                          }}
                        >
                          Scale
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}>
                        <Button
                          variant="light"
                          style={{
                            width: "100%",
                            borderRadius: "5px",
                            textAlign: "left",
                          }}
                          onClick={() => {
                            handleButtonClicks(22);
                          }}
                        >
                          Rating
                        </Button>
                      </Col>
                      <Col style={{ margin: "0", padding: "5px" }}></Col>
                    </Row>
                  </Row>
                </Container>
              </>
            )}
            {image === true ? <SurveyImage /> : null}
            {video === true ? <SurveyVideo /> : null}
            {geotag === true ? <SurveyGeoTag /> : null}
            {likert === true ? <SurveyLikert /> : null}
            {phone === true ? <SurveyPhone /> : null}
            {email === true ? <Question /> : null}
            {audio === true ? <Question /> : null}
            {file === true ? <Question /> : null}
            {single === true ? <SingleChoice /> : null}
            {multiple === true ? <MultipleChoice /> : null}
            {text === true ? <Text /> : null}
            {number === true ? <Number /> : null}
            {location === true ? <Location /> : null}
            {date === true ? <Date /> : null}
            {time === true ? <Time /> : null}
            {note === true ? <Note /> : null}
            {sign === true ? <Signature /> : null}
            {sectionbreak === true ? <SectionBreak /> : null}
            {area === true ? <AreaOnMap /> : null}
            {distance === true ? <DistanceOnMap /> : null}
            {likert === true ? <SurveyLikert /> : null}
            {scale === true ? <SurveyScale /> : null}
            {rating === true ? <Question /> : null}

            <Modal.Footer>
              <Button variant="secondary" onClick={handleCloseClick}>
                Close
              </Button>
              <Button variant="primary" onClick={handleCloseClick}>
                Done
              </Button>
            </Modal.Footer>
          </Modal.Body>
        </Modal>
      </Card>
    </>
  );
};
export default GroupNumber;
