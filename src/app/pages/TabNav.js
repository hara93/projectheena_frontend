import React from "react";
import "../../index.scss";
import { Col, Row, ProgressBar } from "react-bootstrap";
import * as ReactBootstrap from "react-bootstrap";
import { Card } from "@material-ui/core";

class TabNav extends React.Component {
  render() {
    return (
      <>
        <Row className="no-gutters">
          <Col md={2}>
            <ul className="nav nav-tabs ">
              {this.props.tabs.map((tab) => {
                const active = tab === this.props.selected ? "active " : "";
                return (
                  <>
                    <li
                      style={{ backgroundColor: "white", width: "100%", paddingBottom: "1px" }}
                      className={"nav-item " + active + " vertical-tabs"}
                      key={tab}
                    >
                      <a
                        style={{ borderBottom: "1px solid #e7eaec", fontSize: "14px", textTransform: "capitalize", fontWeight: "lighter" }}
                        onClick={() => this.props.setSelected(tab)}
                      >
                        {/* {tab.icon} */}
                        {tab}
                      </a>
                    </li>
                  </>
                );
              })}
            </ul>
          </Col>
          <Col md={10} className="pl-5">{this.props.children}</Col>
        </Row>
      </>
    );
  }
}

export default TabNav;
