import React from "react";
import { Card } from "react-bootstrap";
import NotificationPost from "../components/NotificationPost";
import "../../index.scss";

const Notification = () => {
  return (
    <>
      <div style={{ padding: "30px 0" }}>
        <h2 style={{ fontSize: "24px" }}>Notifications</h2>
      </div>
      <Card>
        <NotificationPost
          icon={<i class="fas fa-times"></i>}
          date="04.05.2021"
          time="3.50pm"
          content="this is the content of the  notification"
          email="tincygeorge8080@sharklasers.com "
          number="8080448848"
        />
        <NotificationPost
          icon={<i class="fa fa-info" aria-hidden="true"></i>}
          date="04.05.2021"
          time="3.50pm"
          content="this is the content of the notification"
          email="tincygeorge8080@sharklasers.com"
          number="8080448848"
        />
        <NotificationPost
          icon={<i class="fa fa-info" aria-hidden="true"></i>}
          date="04.05.2021"
          time="3.50pm"
          content="this is the content of the notification"
          email="tincygeorge8080@sharklasers.com"
          number="8080448848"
        />
      </Card>
    </>
  );
};
export default Notification;
