/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import "../../index.scss";

import Form from "react-bootstrap/Form";
import {
  Dropdown,
  DropdownButton,
  FormControl,
  InputGroup,
} from "react-bootstrap";

import { Col, Table, Card } from "react-bootstrap";
import CustomButton from "../components/CustomButton";
import "bootstrap/dist/css/bootstrap.min.css";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
import Checkbox from "@material-ui/core/Checkbox";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { useSelector } from "react-redux"
import { useHistory } from "react-router";
import axios from "../config/axios";

const UpdateBeneficiary = (props) => {
  const ngo_id = useSelector((state) => state.phAuth.ngo._id)
  const beneficiary_id = props.location.state.beneficiary_id
  console.log("props", props.location.state.beneficiary_id)
  const [type, setType] = useState();
  const [BeneficiaryData, setBeneficiaryData] = useState({})
  const handleType = (e) => {
    setType(e.target.value);
  };
  const getBeneficiaryDetails = () => {
    axios.post("/beneficiary/retrieve", { beneficiary_id })
      .then(({ data, status }) => {
        setBeneficiaryData(data)
      })
      .catch(e => alert(e))
  }
  useEffect(() => {
    getBeneficiaryDetails()
  }, [])

  let personType;
  personType = (
    <>
      {" "}
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          DOB
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Blood Group
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Phone
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="number" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Mobile
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="number" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Caste
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Religin
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Aadhar Card
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Pancard
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Qualification
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Height
        </Form.Label>
        <Col>
          <Form.Control size="lg" type="text" className="green-focus" />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Weight
        </Form.Label>
        <Col>
          <InputGroup>
            <FormControl type="text" className="green-focus" />

            <DropdownButton
              as={InputGroup.Append}
              variant="outline-secondary"
              title="POUND"
              id="input-group-dropdown-2"
            >
              <Dropdown.Item href="#">KG</Dropdown.Item>
              <Dropdown.Item href="#">POUND</Dropdown.Item>
            </DropdownButton>
          </InputGroup>
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Gender
        </Form.Label>
        <Col className="form-input-align-center">
          <RadioGroup
            name="gender"
          // value={location}
          // onChange={(e) => handleChangeLocation(e)}
          >
            <div>
              <FormControlLabel
                value="Male"
                control={<Radio color="primary" />}
                label="Male"
              //  onChange={(e) => handleMileStone(e)}
              />
              <FormControlLabel
                value="Female"
                control={<Radio color="primary" />}
                label="Female"
              //   onChange={(e) => handleMileStone(e)}
              />
            </div>
          </RadioGroup>
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
    </>
  );
  return (
    <>
      <div style={{ marginBottom: "30px" }}>
        <h2 className="font-bold">Update Beneficiary</h2>
        <p>
          Use the following form to update beneficiary details, which can be
          later mapped to projects. Apart from individuals, beneficiaries can be
          abstract like villages, environment, etc.
        </p>
      </div>
      <Card style={{ padding: "30px " }}>
        <Form>
          <Form.Group>
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Name
              </Form.Label>
              <Col>
                <Form.Control
                  value={BeneficiaryData.name}
                  name="name"
                  type="text"
                  placeholder="Enter beneficiary name"
                  className="green-focus"
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Code
              </Form.Label>
              <Col>
                <Form.Control
                  value={BeneficiaryData.code}
                  name="code"
                  type="text"
                  placeholder="Enter beneficiary code"
                  className="green-focus"
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom  p-r-15"
              >
                Image
              </Form.Label>
              <Col>
                <Form.File className="position-relative" required name="file" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Beneficiary Type
              </Form.Label>
              <Col className="form-input-align-center">
                <RadioGroup
                  name="beneficiaryType"
                // value={location}
                // onChange={(e) => handleChangeLocation(e)}
                >
                  <div>
                    <FormControlLabel
                      value="Person"
                      control={<Radio color="primary" />}
                      label="Person"
                      onChange={(e) => handleType(e)}
                    />
                    <FormControlLabel
                      value="Abstract"
                      control={<Radio color="primary" />}
                      label="Abstract"
                      onChange={(e) => handleType(e)}
                    />
                  </div>
                </RadioGroup>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            {type === "Person" ? personType : null}

            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Location
              </Form.Label>
              <Col>
                <PlacesAutoComplete />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Address
              </Form.Label>
              <Col>
                <Form.Control
                  size="lg"
                  type="text"
                  as="textarea"
                  className="green-focus"
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Pincode
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="number" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Beneficiary Requirement
              </Form.Label>
              <Col className="form-input-align-center">
                <RadioGroup
                  name="beneficiaryRequirement"
                // value={location}
                // onChange={(e) => handleChangeLocation(e)}
                >
                  <div>
                    <FormControlLabel
                      value="One time"
                      control={<Radio color="primary" />}
                      label="One time"
                    //  onChange={(e) => handleMileStone(e)}
                    />
                    <FormControlLabel
                      value="Continuous"
                      control={<Radio color="primary" />}
                      label="Continuous"
                    //   onChange={(e) => handleMileStone(e)}
                    />
                  </div>
                </RadioGroup>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Backed by
              </Form.Label>
              <Col>
                <Table
                  responsive="lg"
                  style={{ marginTop: "-10px", marginBottom: "0" }}
                >
                  <tr>
                    <td className="table-data ">
                      <FormControlLabel
                        control={<Checkbox color="primary" />}
                        label="NGO"
                        value="NGO"
                      />
                    </td>
                    <td className="table-data ">
                      <FormControlLabel
                        control={<Checkbox color="primary" />}
                        label="Corporate"
                        value="Corporate"
                      />
                    </td>
                    <td className="table-data ">
                      <FormControlLabel
                        control={<Checkbox color="primary" />}
                        label="Government"
                        value="Government"
                      />
                    </td>
                  </tr>
                </Table>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2}></Form.Label>
              <Col>
                <CustomButton content="Update Beneficiary" />
              </Col>
            </Form.Row>
          </Form.Group>
        </Form>
      </Card>
    </>
  );
};
export default UpdateBeneficiary;
