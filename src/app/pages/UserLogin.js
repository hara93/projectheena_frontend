import React from "react";
import "../login.css";
// import '../../index.scss';
import { Row, Col } from "react-bootstrap";
import { useFormik } from "formik";
import { string, object } from "yup";
import { useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import axios from "../config/axios";
import { Images } from "../config/Images";
import Input from "../components/form/Input";
import { setUserData, setProfileDetails } from "../../redux/reducers/auth/actionCreator";


export default function UserLogin() {
  const history = useHistory();
  const dispatch = useDispatch();

  const defaultValues = {
    username_email: "",
    password: "",
  };

  const validationSchema = object().shape({
    username_email: string().required("Username or email is required"),
    password: string().required("Password is required"),
  });

  const onSubmit = (values) => {
    console.log("values", values);
    try {
      axios
        .post("user/login", { ...values })
        .then(({ data, profileDetails }) => {
          console.info("profileDetails", profileDetails);
          // return;
          dispatch(setUserData(data));
          dispatch(setProfileDetails(profileDetails));
          localStorage.setItem("token", data.token);
          localStorage.setItem("profileDeatils", JSON.stringify(profileDetails));
          history.push({ pathname: "/userprofile", state: { notMe: 0 } });
        })
        .catch((e) => {
          console.log("ERROR", e);
        });
    } catch (e) {
      alert(e.stack);
    }
  };

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    values,
    errors,
    touched,
  } = useFormik({
    initialValues: defaultValues,
    validationSchema,
    onSubmit,
  });

  return (
    <div className="login-container" style={{ maxWidth: '830px', margin: "0 auto" }}>
      <div className="d-flex flex-row">
        <div style={{ maxWidth: '80px' }}>
          <img src={Images.UserIcon} alt="" className="login-avatar" />
        </div>

        <div>
          <h2 className="fw-bold">Login as an Individual</h2>
          <p>
            Please signup / login using the below options. Email signups will be validated by
            sending mails (check spam folder if not received), Social Network are here for
            faster signups.
          </p>
        </div>
      </div>
      <Row>
        <Col md={6} className="col-divider-line">
          <Link to="https://projectheena.com/authorization/fb" className="fb my-3">
            <i className="fab fa-facebook-f social-icons" style={{ color: "white", marginRight: "2%" }}></i>
            <span>Sign up/ Login with Facebook</span>
          </Link>
          <Link to="https://api.linkedin.com/uas/oauth/authorize?oauth_token=" className="linkedin my-3">
            <i
              className="fab fa-linkedin-in social-icons"
              style={{ color: "white", marginRight: "2%" }}
            ></i>
            <span>Sign up/ Login with LinkedIn</span>
          </Link>
          <Link to="/mailSignUp" className="mail my-3">
            {" "}
            <i className="fas fa-envelope social-icons" style={{ color: "white", marginRight: "2%" }}></i>
            Sign up with Mail
          </Link>
        </Col>
        <Col md={6}>
          <form id="login-form" onSubmit={handleSubmit}>
            <div>
              <Input
                type="text"
                name="username_email"
                onChange={handleChange}
                onBlur={handleBlur}
                error={errors.username_email}
                value={values.username_email}
                touched={touched.username_email}
                aria-required="true"
                placeholder="Username / Email"
              ></Input>
            </div>
            <div>
              <Input
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                error={errors.password}
                value={values.password}
                touched={touched.password}
                aria-required="true"
                placeholder="Password"
              ></Input>
            </div>
            <div>
              <button type="submit" className="login">
                Login
              </button>
            </div>
            <div className="d-flex justify-content-between mt-3">
              <Link
                to="#"
                style={{ color: "#4CAF50" }}
              >
                Forgot Password?
              </Link>
              <Link
                to="#"
                style={{ color: "#4CAF50" }}
              >
                Resend Confirmation email
              </Link>
            </div>
          </form>
        </Col>
      </Row>
    </div>
  );
}
