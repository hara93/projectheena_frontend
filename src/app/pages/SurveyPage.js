import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Card, CardHeader } from '@material-ui/core';
import { CardBody } from 'reactstrap';
import SurveyCard from '../components/SurveyCard';
import CustomButtonOutline from '../components/CustomButtonOutline'
import CustomButton from '../components/CustomButton';
import SurveyForm from '../components/Form';
import { FaChartArea } from 'react-icons/fa';

const survey_details=[
    {
        heading:"Follow-up Of Migrated Children",
        status:"Open",
        published:"Published",
        q_no:"0",
        res_no:"0"
    },
    {
        heading:"Follow-up Of Enrolled Children",
        status:"Open",
        published:"Published",
        q_no:"1",
        res_no:"0"
    },
    {
        heading:"School Transport",
        status:"Open",
        published:"Published",
        q_no:"1",
        res_no:"0"
    },
    {
        heading:"School Enrollment",
        status:"Open",
        published:"Published",
        q_no:"1",
        res_no:"0"
    },
    

]

class SurveyPage extends React.Component{
    render(){
        return(
            <>
            <Row className="no-gutters">
                <Col md={12}>
                    <Card className="p-5">
                    <h3 style={{marginTop:'4vh', marginLeft:'2vw', marginBottom:'2vh'}}> <FaChartArea style={{marginRight:'3%'}}color="#676a6c"/>Survey</h3>
                        
                        <CardBody>You can create new surveys here, fill surveys, check responses / stats of existing ones(as per your access levels / authority).</CardBody>
                        <div style={{marginLeft: '39%',marginBottom: '4%',marginTop: '4%'}}>
                        <CustomButtonOutline content="Create New Survey!" onClick={<SurveyForm/>}/> 

                        </div>
                        

                        <SurveyCard survey_details={survey_details} />
                        
                    </Card>
                </Col>

            </Row>
            </>
        )
    }
}

export default SurveyPage;