/* eslint-disable no-restricted-imports */
import React from "react";
import { Form, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import CustomButton from "../components/CustomButton";

const CreateTaskOption = () => {
  return (
    <>
      <div>
        <h2>Create Task</h2>
        <p>
          It becomes easier for volunteers to hunt out right opportunities if
          you bifurcate the same in right buckets. Please choose the right
          category for your task. Also note that though we dont approve all the
          tasks manually we do audit a few tasks and ensure that you are playing
          it all cool.
        </p>
      </div>
      <Card style={{ padding: "30px" }}>
        <Form>
          <Form.Group>
            <Form.Row>
              <Form.Label column="lg" lg={2}>
                Create the task as
              </Form.Label>
              <Col style={{ paddingTop: "6px" }}>
                <div className="project-checkbox">
                  <label for="2">
                    <input
                      type="radio"
                      name="time bound"
                      checked="checked"
                      id="2"
                    ></input>
                    As social work
                  </label>
                </div>
              </Col>
            </Form.Row>
            <br />

            <Form.Row>
              <Form.Label column="lg" lg={2}></Form.Label>
              <Col></Col>
            </Form.Row>
          </Form.Group>
        </Form>
        <div>
          <a href="/createTask">
            <CustomButton content="Create Task" />
          </a>
        </div>
      </Card>
    </>
  );
};

export default CreateTaskOption;
