/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Modal,
} from "react-bootstrap";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";

import SurveyConditions from "../components/surveyConditions";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import SurveyQuestions from "../components/SurveyQuestions";

import Select from "react-select";
import { arrayIncludes } from "@material-ui/pickers/_helpers/utils";
import { indexOf } from "lodash";
const defaultAddForm = {
  id: 1,
  s1: "",
  s2: "",
  s3: "",
};
// const beforeAfter = [
//   { value: "before", label: "Before" },
//   { value: "after", label: "After" },
// ];

const SurveyMainPage = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [arrays, setArrays] = useState([]);
  const [idcount, setIdcount] = useState([0]);
  var [count, setCount] = useState(0);
  const [checkedBoxes, setCheckedBoxes] = useState([]);
  const noOfQuestions = arrays.length;
  const [deleteItems, setDeleteItems] = useState([]);

  const [deleteConfirm, setDeleteConfirm] = useState(true);
  const [moveConfirm, setMoveConfirm] = useState(true);
  const [copyConfirm, setCopyConfirm] = useState(true);

  const [beforeAfter, setBeforeAfter] = useState("after");
  const [moveValue, setMoveValue] = useState(0);
  const [copyValue, setCopyValue] = useState();

  const [showButton, setShowButton] = useState(true);
  const [selectedQuestion, setSelectedQuestion] = useState([]);
  const [qType, setQtype] = useState([]);
  const [content, setContent] = useState(true);
  const [number, setNumber] = useState(0);
  const [display, setDisplay] = useState(false);
  const [ruleQuestion, setRuleQuestion] = useState([]);
  const [conditions, setConditions] = useState([{ ...defaultAddForm }]);
  function handleCount(e, index) {
    console.log(e.target.value);
  }
  const handleCheckbox = (e, s) => {
    const checkedboxes = [...checkedBoxes];

    if (e.target.checked) {
      checkedboxes.push(s.id);

      console.log("pushed Checked boxes", checkedboxes);
    } else {
      const index = checkedboxes.findIndex((ch) => ch.id === s.id);
      checkedboxes.splice(index, 1);
      setCheckedBoxes(checkedboxes);
      console.log("unchecked boxes", checkedBoxes);
    }
  };

  const handleDelete = (index) => {
    const newArray = [...arrays];
    newArray.splice(index, 1);
    setArrays(newArray);
  };

  const [rules, setRules] = useState([]);
  const handleCloseRules = (index) => {
    const newRules = [...rules];
    newRules.splice(index, 1);
    setRules(newRules);
  };

  const lengthOfRules = rules.length === 0;
  useEffect(() => {
    console.log("chenged cond", conditions);
  }, [conditions]);
  const handleAddCondition = () => {
    if (conditions) {
      var nx = conditions[conditions.length - 1];
      var newInser = { ...defaultAddForm, id: nx.id + 1 };
      console.log("new Ins", newInser);
      var newIns = conditions.concat(newInser);
      console.log("newIns", newIns);
      setConditions(newIns);
    }
  };

  const options = [
    { value: "chocolate", label: "Chocolate" },
    { value: "strawberry", label: "Strawberry" },
    { value: "vanilla", label: "Vanilla" },
  ];

  const criterias = [
    {
      value: "is any of",
      label: "is any of",
    },
    {
      value: "is none of",
      label: "is none of",
    },
  ];
  const step2Qs = [
    {
      value: "what is your name ?",
      label: "What is your name ?",
      type: "single choice",
    },
    {
      value: "what is your age ?",
      label: "What is your age?",
      type: "multiple choice",
    },
  ];

  // const [condition, setCondition] = useState("");
  const [criteriaType, setCriteriaType] = useState(false);
  const showCriteriaType = (e) => {
    const opt = e.target.value;
    opt !== "" ? setCriteriaType(true) : setCriteriaType(false);
    // setCondition(e.target.value);
    // condition.length === "" ? setCriteriaType(false) : setCriteriaType(true);
  };

  // const [criteria1, setCriteria1] = useState("");
  const [choices, setChoices] = useState(false);
  const handleChoices = (e) => {
    // setCriteria1(e.label);
    // criteria1 !== "" ? setChoices(true) : setChoices(false);
    const opt = e.value;
    opt !== "" ? setChoices(true) : setChoices(false);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  // const conditionList = conditions.map((ele, index) => (
  //   <SurveyConditions
  //     key={ele}
  //     index={index}
  //     // conditions={conditions}
  //   />
  const conditionList = conditions.map((ele, index) => (
    <SurveyConditions
      key={ele.id}
      ele={ele}
      handleChange={(id, name, val) => {
        console.log("change", id, name, val);
        var i = conditions.findIndex((x) => x.id === id);
        console.log("index", i);
        if (i > -1 && name && val) {
          let cond = conditions;
          cond[i][name] = val;
          console.log("updated cond", cond);
          setConditions(cond);
        }
      }}
      // conditions={conditions}
    />
  ));
  const handleRemove = (id) => {
    console.log("id of the component :", id);
    const newArray = [...arrays];
    newArray.pop();
    setArrays(newArray);
  };
  const onChange = (e, index) => {
    //console.log(e.target.checked, index);
    e.target.checked ? setCount(count + 1) : setCount(count - 1);
    const newArray = [...arrays];
    newArray[index] = { ...newArray[index], checked: e.target.checked };
    const list = [...deleteItems];

    if (e.target.checked) {
      list.push(index);
    } else {
      list.splice(index, 1);
    }

    setDeleteItems(list);
    console.log("delete items arr", deleteItems, list.sort());

    setArrays(newArray);
  };
  const handleDeleteMultiple = () => {
    var valuesArr = [...arrays];
    var removeValFromIndex = [...deleteItems.sort()];

    for (var i = removeValFromIndex.length - 1; i >= 0; i--)
      valuesArr.splice(removeValFromIndex[i], 1);
    setCount(0);
    setDeleteItems([]);
    setArrays(valuesArr);
  };

  const handleMove = (index) => {
    function array_move(arr, old_index, new_index) {
      if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
          arr.push(undefined);
        }
      }
      arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
      return arr; // for testing
    }
    var newIndex = moveValue;
    const moveTo = beforeAfter;
    if (moveTo === "after") {
      newIndex += 1;
    } else if (moveTo === "before") {
      newIndex -= 1;
    }
    if (newIndex <= 0) {
      newIndex = 0;
    }
    if (newIndex >= arrays.length - 1) {
      newIndex = arrays.length - 1;
    }
    const newarr = [...arrays];
    const moved = array_move(newarr, index, newIndex);
    setArrays(moved);
    // console.log("old index,new index ", index, newIndex);
    // console.log(moveTo, "value of BandF");
    // console.log(moved, "moved array");
  };

  useEffect(() => {
    console.log("questiontype:", arrays);
  });

  return (
    <>
      <Container style={{ padding: "30px", backgroundColor: "white" }}>
        <Row style={{ margin: " 0" }}>
          <Col xs={9} style={{ margin: "10", padding: "0" }}>
            <div
              style={{
                display: "flex",
                border: "1px solid #cccccc",
                alignItems: "center",
                width: "100%",

                backgroundColor: "white",
              }}
            >
              <Form.Control
                type="text"
                placeholder={`Search ${noOfQuestions} ${
                  noOfQuestions === 1 ? "question" : "questions"
                }  `}
                style={{ border: "0" }}
              />
              <i class="fas fa-search" style={{ marginRight: "10px" }}></i>
            </div>
          </Col>
          <Col style={{ margin: "0px 0px 0 10px", padding: "0" }}>
            <div style={{ width: "100%" }}>
              {count > 0 ? (
                <button
                  type="button"
                  className="delete-questions-btn"
                  // onClick={() => {
                  //   handleDeleteMultiple();
                  // }}
                  onClick={() => handleShow()}
                >
                  <i
                    class="fas fa-trash-alt"
                    style={{ marginRight: "5px", color: "white" }}
                  ></i>
                  Delete {count} {count === 1 ? "question" : "questions"}
                </button>
              ) : (
                <button type="button" className="publish-form-btn">
                  <i
                    class="fas fa-cloud-upload-alt"
                    style={{ marginRight: "5px", color: "white" }}
                  ></i>{" "}
                  Publish Form
                </button>
              )}
            </div>
          </Col>
        </Row>
        <Row style={{ margin: "10px 0 0 0 " }}>
          <AppBar position="static" style={{ zIndex: "0" }}>
            <Tabs
              value={value}
              onChange={handleChange}
              variant="fullWidth"
              style={{ background: "#4caf4f" }}
            >
              <Tab
                label="Questions"
                {...a11yProps(0)}
                style={{
                  width: "100%",
                  flexGrow: "1",
                  backgroundColor: "#9af19a",
                  color: "green",
                }}
              />
              <Tab
                label="Rules"
                {...a11yProps(1)}
                style={{
                  width: "100%",
                  flexGrow: "1",
                  backgroundColor: "#9af19a",
                  color: "green",
                }}
              />
            </Tabs>
          </AppBar>
          <TabPanel
            value={value}
            index={0}
            style={{
              width: "100%",
              margin: "0",
              padding: "0",
            }}
          >
            {" "}
            <div>
              {/* <SurveyQuestions /> */}
              {/* <QuestionList dummy={dummy} /> */}
              {/* <AccordionBuilder title={name} /> */}
            </div>
            {/* {display ? (
              questionTab
            ) : (
              <div style={{ display: "flex" }}>
                <button
                  className="p-2 survey-btn"
                  style={{
                    margin: "10px auto 10px auto",
                  }}
                  onClick={() => {
                    setDisplay(true);
                  }}
                >
                  <i
                    className="fas fa-plus-circle add-ques"
                    style={{ color: "#4caf4f" }}
                  ></i>
                  Add new Question
                </button>
              </div>
            )} */}
            {arrays.map((ele, index) => (
              <>
                <div
                  style={{
                    display: "flex",
                    position: "relative",
                  }}
                  key={index}
                  id={index}
                >
                  <div style={{ minWidth: "40px" }}>
                    {ele && ele.qType ? (
                      <div style={{ paddingTop: "5px" }}>
                        <Checkbox
                          id={ele.id}
                          color="primary"
                          value={ele.id}
                          checked={ele.checked}
                          onChange={(e) => onChange(e, index)}
                        />
                      </div>
                    ) : null}
                  </div>
                  <div className="survey-qns-index">
                    {index + 1}
                    {arrays.length > 1 ? (
                      <div className="survey-addqns-inbetween">
                        <button
                          type="button"
                          className="addqns-inbetween"
                          onClick={() => {
                            var idc = idcount.length - 1;
                            setIdcount([...idcount, idc + 1]);
                            var lastIndex = idcount[idc];

                            var list = [...arrays];
                            list.splice(index + 1, 0, { id: lastIndex + 1 });

                            setContent(lastIndex + 1);
                            setDeleteConfirm(lastIndex + 1);
                            setCopyConfirm(lastIndex + 1);
                            setMoveConfirm(lastIndex + 1);

                            setArrays(list);
                          }}
                        >
                          <i class="fas fa-plus basecolor"></i>
                        </button>
                      </div>
                    ) : null}
                  </div>
                  <div className="survey-content-box">
                    {" "}
                    {content == ele.id ? (
                      <div
                        style={{
                          width: "100%",
                          paddingTop: "3px",
                        }}
                      >
                        <SurveyQuestions
                          key={ele.id}
                          ele={ele}
                          setData={(data) => {
                            console.log("setData clicked", data);
                          }}
                          setQtype={(obj) => {
                            console.log("obj", obj);
                            var list = [...arrays];
                            list.filter((ele) => {
                              if (ele.id == obj.id) {
                                ele.qType = obj.qType;
                                ele.selectedQuestion = obj.selectedQuestion;
                                ele.checked = obj.checked;
                              }
                            });
                            console.log("updated Arr", list);
                            setSelectedQuestion(obj.selectedQuestion);
                            setArrays(list);
                          }}
                          getButtonState={(state) => setShowButton(state)}
                          getIndex={(id) => handleRemove(id)}
                          qtype={qType}
                        />
                        {ele &&
                        ele.qType &&
                        deleteConfirm == ele.id &&
                        moveConfirm == ele.id &&
                        copyConfirm == ele.id ? (
                          <div
                            style={{
                              borderTop: "1px solid #cccccc",
                              padding: "10px",
                            }}
                          >
                            <button
                              id={ele.id}
                              className="survey-delete-btn"
                              type="button"
                              // onClick={() => handleDelete(index)}
                              onClick={() =>
                                setDeleteConfirm(
                                  deleteConfirm == ele.id ? null : ele.id
                                )
                              }
                            >
                              <i
                                class="far fa-trash-alt"
                                style={{ color: "#F64A4A" }}
                              ></i>{" "}
                              Delete
                            </button>
                            <button
                              id={ele.id}
                              className="survey-move-btn"
                              type="button"
                              onClick={() =>
                                setMoveConfirm(
                                  moveConfirm == ele.id ? null : ele.id
                                )
                              }
                            >
                              <i
                                class="fas fa-arrows-alt-v"
                                style={{ color: "inherit" }}
                              ></i>{" "}
                              Move
                            </button>
                            <button
                              id={ele.id}
                              className="survey-copy-btn"
                              type="button"
                              onClick={() =>
                                setCopyConfirm(
                                  copyConfirm == ele.id ? null : ele.id
                                )
                              }
                            >
                              <i
                                class="far fa-copy"
                                style={{ color: "inherit" }}
                              ></i>{" "}
                              Copy
                            </button>
                          </div>
                        ) : null}
                        {!(deleteConfirm == ele.id) ? (
                          <div>
                            <div className="survey-content-footer">
                              <div>
                                <span>
                                  {" "}
                                  <i
                                    class="fas fa-trash-alt "
                                    style={{ color: "#f44455" }}
                                  ></i>{" "}
                                  Are you sure you want to delete this question?
                                </span>
                              </div>
                              <div>
                                <button
                                  type="button"
                                  className="confirm-cancel-btn"
                                  onClick={() => {
                                    setDeleteConfirm(ele.id);
                                  }}
                                >
                                  <i class="fas fa-times"></i> Cancel
                                </button>
                                <button
                                  type="button"
                                  className="confirm-delete-btn"
                                  onClick={() => handleDelete(index)}
                                >
                                  <i
                                    class="fas fa-check"
                                    style={{ color: "#f44455" }}
                                  ></i>{" "}
                                  Delete
                                </button>
                              </div>
                            </div>
                          </div>
                        ) : null}

                        {!(moveConfirm == ele.id) ? (
                          <div>
                            <div className="survey-content-footer">
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  flexGrow: "1",
                                }}
                              >
                                <Form.Control
                                  as="select"
                                  custom
                                  value={beforeAfter}
                                  style={{ width: "100px" }}
                                  onChange={(e) => {
                                    setBeforeAfter(e.target.value);
                                  }}
                                >
                                  <option value="after">After</option>
                                  <option value="before">Before</option>
                                </Form.Control>
                                <div
                                  style={{ width: "100%", margin: "0 20px" }}
                                >
                                  <Form.Control
                                    as="select"
                                    custom
                                    value={moveValue}
                                    onChange={(e) => {
                                      setMoveValue(parseInt(e.target.value));
                                    }}
                                  >
                                    {arrays.map((ele, idx) => (
                                      <option value={idx}>
                                        {/* <div
                                          style={{
                                            width: "100%",
                                            display: "flex",
                                          }}
                                        >
                                          {" "}
                                        </div> */}
                                        {idx + 1} {"               "}
                                        {ele.selectedQuestion}
                                      </option>
                                    ))}

                                    {/* {arrays
                                      .filter((ele, idx) => ele.id - 1 === idx)
                                      .map((filteredArr) => (
                                        <option>
                                          {filteredArr.id}{" "}
                                          {filteredArr.selectedQuestion}
                                        </option>
                                      ))} */}
                                  </Form.Control>
                                </div>
                              </div>

                              <div style={{ minWidth: "157px" }}>
                                <button
                                  type="button"
                                  className="confirm-cancel-btn"
                                  onClick={() => {
                                    setMoveConfirm(ele.id);
                                  }}
                                >
                                  <i class="fas fa-times"></i> Cancel
                                </button>
                                <button
                                  type="button"
                                  className="confirm-delete-btn"
                                  onClick={() => {
                                    handleMove(index);
                                    setContent(null);
                                  }}
                                >
                                  <i
                                    class="fas fa-check"
                                    style={{ color: "inherit" }}
                                  ></i>{" "}
                                  Move
                                </button>
                              </div>
                            </div>
                          </div>
                        ) : null}
                        {!(copyConfirm == ele.id) ? (
                          <div>
                            <div className="survey-content-footer">
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  flexGrow: "1",
                                }}
                              >
                                <Form.Control
                                  as="select"
                                  custom
                                  value={copyValue}
                                  style={{ width: "100px" }}
                                  onChange={(e) => {
                                    setCopyValue(e.target.value);
                                  }}
                                >
                                  <option>After</option>
                                  <option>Before</option>
                                </Form.Control>
                                <div
                                  style={{ width: "100%", margin: "0 20px" }}
                                >
                                  <Form.Control
                                    as="select"
                                    custom
                                    value={copyValue}
                                    onChange={(e) => {
                                      setCopyValue(e.target.value);
                                    }}
                                  >
                                    {arrays.map((ele, idx) => (
                                      <option>
                                        {/* <div
                                        style={{
                                          width: "100%",
                                          display: "flex",
                                        }}
                                      >  </div> */}
                                        {idx + 1} title - keyword{" "}
                                        {ele.selectedQuestion}
                                      </option>
                                    ))}
                                  </Form.Control>
                                </div>
                              </div>

                              <div style={{ minWidth: "157px" }}>
                                <button
                                  type="button"
                                  className="confirm-cancel-btn"
                                  onClick={() => {
                                    setCopyConfirm(ele.id);
                                  }}
                                >
                                  <i class="fas fa-times"></i> Cancel
                                </button>
                                <button
                                  type="button"
                                  className="confirm-delete-btn"
                                >
                                  <i
                                    class="fas fa-check"
                                    style={{ color: "inherit" }}
                                  ></i>{" "}
                                  Copy
                                </button>
                              </div>
                            </div>
                          </div>
                        ) : null}
                      </div>
                    ) : (
                      <div
                        style={{
                          fontSize: "16px",
                          display: "flex",
                          alignItems: "center",
                          width: "100%",
                        }}
                      >
                        <p
                          style={{
                            padding: "0",
                            margin: "0",
                          }}
                        >
                          No Title <span style={{ color: "#ff0000" }}>*</span>
                        </p>
                      </div>
                    )}
                    {ele && ele.qType ? (
                      <div className="survey-content-child">
                        <span className="survey-selected-qns">
                          {" "}
                          {ele.selectedQuestion}
                        </span>

                        <button
                          type="button"
                          style={{
                            background: "transparent",
                            border: "0",
                            display: "flex",
                          }}
                          onClick={() => {
                            setContent(content == ele.id ? null : ele.id);
                            setDeleteConfirm(ele.id);
                            setMoveConfirm(ele.id);
                            setCopyConfirm(ele.id);
                          }}
                        >
                          {content == ele.id ? (
                            <i
                              class="fas fa-angle-up"
                              style={{ color: "gray" }}
                            ></i>
                          ) : (
                            <i
                              class="fas fa-angle-down"
                              style={{ color: "gray" }}
                            ></i>
                          )}
                        </button>
                      </div>
                    ) : null}
                  </div>
                </div>
              </>
            ))}
            {showButton && count === 0 ? (
              <div style={{ display: "flex" }}>
                <button
                  className="p-2 survey-btn"
                  style={{
                    margin: "10px auto 10px auto",
                  }}
                  onClick={() => {
                    setDisplay(true);
                    var idc = idcount.length - 1;
                    setIdcount([...idcount, idc + 1]);
                    var lastIndex = idcount[idc];
                    console.log("index of this list", lastIndex);

                    var ndx = arrays.length;
                    var prevArr;
                    if (arrays.length > 0) {
                      prevArr = arrays[arrays.length - 1];
                    }
                    if (prevArr) {
                      setArrays([...arrays, { id: lastIndex + 1 }]);
                      setContent(lastIndex + 1);
                      setDeleteConfirm(lastIndex + 1);
                      setCopyConfirm(lastIndex + 1);
                      setMoveConfirm(lastIndex + 1);
                    } else {
                      setArrays([...arrays, { id: 1 }]);
                      setContent(1);
                      setDeleteConfirm(1);
                      setCopyConfirm(1);
                      setMoveConfirm(1);
                    }
                    console.log(
                      "arrays & content & length",
                      arrays,
                      content,
                      ndx
                    );
                    setShowButton(!showButton);
                  }}
                >
                  <i
                    className="fas fa-plus-circle add-ques"
                    style={{ color: "#4caf4f" }}
                  ></i>
                  Add new Question
                </button>
              </div>
            ) : null}
          </TabPanel>
          <TabPanel
            value={value}
            index={1}
            style={{
              width: "100%",
              margin: "0",
              padding: "0",
            }}
          >
            {rules.map((item, index) => (
              <Card style={{ padding: "20px" }}>
                <Row
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      width: "100%",
                      padding: "0 15px",
                    }}
                  >
                    <div style={{ display: "flex" }}>
                      <h3 style={{ fontSize: "24px ", fontWeight: "600" }}>
                        Rule #{index + 1}
                      </h3>
                    </div>
                    <div>
                      <Button
                        variant="light"
                        onClick={() => {
                          handleCloseRules(index);
                          setConditions([1]);
                          setRuleQuestion([]);
                        }}
                      >
                        <i class="fas fa-times"></i>
                      </Button>
                    </div>
                  </div>
                </Row>
                <Row
                  style={{
                    margin: "0",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div>
                    <p>Step 1</p>
                  </div>

                  <div style={{ display: " flex ", alignItems: "center" }}>
                    if
                    <Form.Control
                      as="select"
                      style={{
                        width: "fit-content",
                        margin: "0px 5px 0 5px",
                      }}
                    >
                      <option>All</option>
                      <option>Any</option>
                    </Form.Control>
                    of the folling conditions are met
                  </div>
                </Row>

                {/* <Row style={{ margin: "20px 0 20px 0" }}>{conditionList}</Row> */}
                <Row style={{ margin: "20px 0 20px 0" }}>
                  {conditions.map((ele, index) => {
                    console.log("ele", ele);
                    return (
                      <SurveyConditions
                        key={ele.id}
                        ele={ele}
                        handleChange={(id, name, val) => {
                          console.log("change", id, name, val);
                          var i = conditions.findIndex((x) => x.id === id);
                          console.log("index", i);
                          if (i > -1 && name && val) {
                            let cond = [...conditions];
                            cond[i][name] = val;
                            console.log("updated cond", cond);
                            setConditions(cond);
                          }
                        }}
                        // conditions={conditions}
                      />
                    );
                  })}
                </Row>
                <Row style={{ margin: "0", marginBottom: "30px" }}>
                  <button
                    className="p-2 survey-btn"
                    style={{ marginTop: "10px", marginBottom: "10px" }}
                    onClick={handleAddCondition}
                  >
                    <i
                      className="fas fa-plus-circle add-ques"
                      style={{ color: "#4caf4f" }}
                    ></i>
                    Add Condition
                  </button>
                </Row>

                <Row
                  style={{
                    margin: "0",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div>
                    <p>Step 2</p>
                  </div>
                  <div>
                    <p>
                      Then <b>show</b> the following questions
                    </p>
                  </div>
                  <div>
                    {ruleQuestion.map((item, index) => (
                      <Card
                        style={{
                          padding: "10px",
                          width: "100%",
                          border: "0",
                          borderRadius: "0",
                          backgroundColor: "#e3e3e3",
                        }}
                      >
                        <Row>
                          <Col xs={10}>
                            <div
                              style={{
                                display: "flex",
                                width: "100%",
                                justifyContent: "space-between",
                              }}
                            >
                              <div style={{ marginRight: "10px" }}>
                                Q{index + 1}
                              </div>
                              <div>{item.title}</div>
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "flex-end",
                                }}
                              >
                                {item.type}
                              </div>
                            </div>
                          </Col>
                          <Col>
                            <div>
                              <button
                                style={{
                                  backgroundColor: "#ff000000",
                                  border: "0",
                                }}
                              >
                                <i
                                  class="fas fa-ban"
                                  style={{ color: "red", fontSize: "20px" }}
                                ></i>
                              </button>
                            </div>
                          </Col>
                        </Row>
                      </Card>
                    ))}
                  </div>

                  <Select
                    options={step2Qs}
                    placeholder="Add a question"
                    onChange={(e) => {
                      setRuleQuestion([
                        ...ruleQuestion,
                        {
                          id: "1",
                          title: e.value,
                          type: e.type,
                        },
                      ]);
                    }}
                  />
                </Row>
                <Row
                  style={{
                    margin: "20px 0 0 0",
                    padding: "20px 0 0 0",
                    borderTop: "1px solid #e0e0e0",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      width: "100%",
                    }}
                  >
                    <div style={{ display: "flex" }}>
                      <Button
                        variant="light"
                        className="survey-questions-btn "
                        style={{ margin: "5px", textAlign: "center" }}
                        onClick={() => {
                          handleCloseRules(item.id);
                          setConditions([1]);
                          setRuleQuestion([]);
                        }}
                      >
                        {" "}
                        Delete
                      </Button>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "150px",
                      }}
                    >
                      <Button
                        variant="light"
                        className="survey-questions-btn "
                        onClick={() => {
                          handleCloseRules(item.id);
                          setConditions([1]);
                          setRuleQuestion([]);
                        }}
                        style={{ margin: "5px", textAlign: "center" }}
                      >
                        {" "}
                        Cancel
                      </Button>
                      <Button
                        variant="light"
                        className="survey-questions-btn"
                        style={{ margin: "5px", textAlign: "center" }}
                      >
                        {" "}
                        Save
                      </Button>
                    </div>
                  </div>
                </Row>
              </Card>
            ))}
            {lengthOfRules ? (
              <div style={{ display: "flex" }}>
                <button
                  className="p-2 survey-btn"
                  style={{
                    margin: "10px auto 10px auto",
                  }}
                  onClick={() => {
                    // setDisplayRule(true);
                    setRules([...rules, { id: "" }]);
                  }}
                >
                  <i
                    className="fas fa-plus-circle add-ques"
                    style={{ color: "#4caf4f" }}
                  ></i>
                  Add new Rule
                </button>
              </div>
            ) : null}
          </TabPanel>
        </Row>
      </Container>

      <Modal
        show={show}
        onHide={handleClose}
        animation={false}
        size="xs"
        centered
      >
        <div
          style={{
            height: "60px",
            borderBottom: "1px solid #cccccc",
            padding: "15px 10px 5px 10px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h4 style={{ fontSize: "26px", fontWeight: "600" }}>
            {" "}
            Delete {count} {count === 1 ? "Question" : "Questions"}
          </h4>
          <Button
            variant="secondary"
            style={{ height: "30px" }}
            onClick={handleClose}
          >
            <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ margin: "15px 0" }}>
            <span>
              Are you sure you want to delete {count}{" "}
              {count === 1 ? "question" : "questions"} ?
            </span>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <div>
            <button onClick={handleClose} className="survey-modal-cancelbtn">
              Cancel
            </button>
            <button
              onClick={() => {
                handleDeleteMultiple();
                handleClose();
              }}
              className="survey-modal-delbtn"
            >
              Delete
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default SurveyMainPage;
