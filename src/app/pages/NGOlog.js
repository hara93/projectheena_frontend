/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import { Table } from "react-bootstrap";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";
import { makeStyles } from "@material-ui/core/styles";

const NGOlog = () => {
  const ngoLogList = [
    {
      name: "Testing Karma Log Form",
      status: "pending for approval",
    },
    {
      name: "Testing Karma Log Form",
      status: "pending for approval",
    },
    {
      name: "Testing Karma Log Form",
      status: "pending for approval",
    },
    {
      name: "Testing Karma Log Form",
      status: "pending for approval",
    },
    {
      name: "Testing Karma Log Form",
      status: "pending for approval",
    },
    {
      name: "Testing Karma Log Form",
      status: "pending for approval",
    },
    {
      name: "Testing Karma Log Form",
      status: "pending for approval",
    },

  ];
  const useStylesBootstrap = makeStyles((theme) => ({
    arrow: {
      color: theme.palette.common.black,
    },
    tooltip: {
      backgroundColor: theme.palette.common.black,
      fontSize: "12px",
      width: "130px",
    },
  }));
  function BootstrapTooltip(props) {
    const classes = useStylesBootstrap();

    return <Tooltip arrow classes={classes} {...props} />;
  }
  const [ngo, setNgo] = useState(ngoLogList)
  const [currentPage, setCurrentPage] = useState(1)
  const [ngoPerPage] = useState(4)



  const indexOfLastNgo = currentPage * ngoPerPage
  const indexOfFirstNgo = indexOfLastNgo - ngoPerPage;
  const currentNgo = ngo.slice(indexOfFirstNgo, indexOfLastNgo)





  const pageNumber = [];
  const totalNgo = ngo.length

  for (let i = 1; i <= Math.ceil(totalNgo / ngoPerPage); i++) {
    pageNumber.push(i)
  }

  return (
    <>
      <Card>
        <h3
          className="ibox-title"
          style={{ fontSize: "16px", fontWeight: "600" }}
        >
          My Volunteering Log
        </h3>
        <div
          className="ibox-content"
          style={{
            borderStyle: "solid solid none",
            borderWidth: "1px 0",
            borderColor: "#e7eaec",
          }}
        >
          <Table responsive="xl" striped>
            <thead>
              <tr>
                <td>Name</td>
                <td>Status</td>
                <td></td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {currentNgo.map((ele, index) => (
                <tr>
                  <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                    <a style={{ color: "#4caf4f" }} href="#">
                      {ele.name}
                    </a>
                  </td>
                  <td style={{ paddingBottom: "5", fontSize: "11px" }}>
                    <span
                      style={{
                        backgroundColor: "#4caf4f",
                        color: "white",
                        padding: "5px",
                      }}
                    >
                      {ele.status}
                    </span>
                  </td>
                  <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                    <a href="#">
                      <i style={{ color: "#4caf4f" }} class="fas fa-eye"></i>
                    </a>
                  </td>
                  <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                    <BootstrapTooltip
                      TransitionComponent={Fade}
                      TransitionProps={{ timeout: 300 }}
                      placement="right"
                      title={`Edit "${ele.name}"`}
                    >
                      <a href="/editTask">
                        <i
                          style={{ color: "#4caf4f" }}
                          class="fas fa-pencil-alt"
                        ></i>
                      </a>
                    </BootstrapTooltip>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </Card>
      <nav>
        <ul className="pagination">

          {
            pageNumber.map(
              (item) => (
                <li key={item} className="page-item">
                  <button onClick={() => setCurrentPage(item)} className="page-link">
                    {item}
                  </button>
                </li>
              )
            )
          }

        </ul>

      </nav>
    </>
  );
};
export default NGOlog;
