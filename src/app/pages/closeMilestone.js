/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import "../../index.scss";

import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { Col, Card } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import DatePicker from "react-datepicker";
import CustomButtonOutline from "../components/CustomButtonOutline";

const CloseMilestone = () => {
    const [startDate, setStartDate] = useState();
    // eslint-disable-next-line no-unused-vars
    const [endDate, setEndDate] = useState(new Date());
    const [count, setCount] = useState(0);
    // const [countAwards, setCountAward] = useState(0);
    var maxCount = 256;
    // var maxCountAwards = 1000;

    return (
        <div className="container">
            <div style={{ marginBottom: "30px" }}>
                <h2 className="font-bold">Close Milestone</h2>

            </div>
            <Card style={{ padding: "30px " }}>
                <Form>
                    <Form.Group>
                        <Form.Row>
                            <Form.Label
                                column="lg"
                                lg={2}
                                className="form-label-custom p-r-15"
                            >
                                Name
                            </Form.Label>
                            <Col className="form-input-align-center">
                                <Form.Control size="lg" type="text" className="green-focus" />
                            </Col>
                        </Form.Row>
                        <hr className="form-horizontal-line" />
                        <Form.Row>
                            <Form.Label
                                column="lg"
                                lg={2}
                                className="form-label-custom p-r-15"
                            >
                                Budget
                            </Form.Label>
                            <Col className="form-input-align-center">
                                <Form.Control size="lg" type="number" className="green-focus" />
                            </Col>
                        </Form.Row>
                        <hr className="form-horizontal-line" />
                        <Form.Row>
                            <Form.Label
                                column="lg"
                                lg={2}
                                className="form-label-custom p-r-15"
                            >
                                Actual close date
                            </Form.Label>
                            <Col>
                                <InputGroup>
                                    <DatePicker
                                        selected={startDate}
                                        selectsStart
                                        startDate={startDate}
                                        endDate={endDate}
                                        onChange={(date) => setStartDate(date)}
                                        className="green-focus"
                                    />
                                    <InputGroup.Append>
                                        {/* <i class="fas fa-calendar-alt"></i> */}
                                    </InputGroup.Append>
                                </InputGroup>
                            </Col>
                        </Form.Row>
                        <hr className="form-horizontal-line" />
                        <Form.Row>
                            <Form.Label
                                column="lg"
                                lg={2}
                                className="form-label-custom p-r-15"
                            >
                                Actual Expense
                            </Form.Label>
                            <Col className="form-input-align-center">
                                <Form.Control size="lg" type="number" className="green-focus" />
                            </Col>
                        </Form.Row>
                        <hr className="form-horizontal-line" />
                        <Form.Row>
                            <Form.Label
                                column="lg"
                                lg={2}
                                className="form-label-custom p-r-15"
                            >
                                Reason for closing milestone
                            </Form.Label>
                            <Col>
                                <Form.Control
                                    size="lg"
                                    type="text"
                                    as="textarea"
                                    rows="5"
                                    maxLength="200"
                                    minLength="5"
                                    onChange={(e) => setCount(e.target.value.length)}
                                    style={{ borderRadius: "0" }}
                                    className="green-focus"
                                />
                                <span className="help-block">
                                    {" "}
                                    Min: 5 chars | Max: 200 chars | Remaining Chars:{" "}
                                    {maxCount - count}
                                </span>
                            </Col>
                        </Form.Row>
                        <hr className="form-horizontal-line" />
                        <Form.Row>
                            <Form.Label
                                column="lg"
                                lg={2}
                                className="form-label-custom p-r-15"
                            >
                                Milestone Checklist Achieved?
                            </Form.Label>
                            <Col >
                                <Form.Group className="mb-3" controlId="formBasicCheckbox" style={{ display: 'inline-block', marginRight: '20px' }}>
                                    <Form.Check type="radio" label="Yes" />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formBasicCheckbox" style={{ display: 'inline-block' }}>
                                    <Form.Check type="radio" label="No" />
                                </Form.Group>
                            </Col>
                        </Form.Row>
                        <hr className="form-horizontal-line" />
                        <Form.Row>
                            <Form.Label
                                column="lg"
                                lg={2}
                                className="form-label-custom p-r-15"
                            >
                                Milestone Health
                            </Form.Label>
                            <Col>
                                <Form.Control as="select" size="lg" custom>
                                    <option>Good</option>
                                    <option>Average</option>
                                    <option>Bad</option>
                                </Form.Control>
                            </Col>
                        </Form.Row>
                        <hr className="form-horizontal-line" />
                        <div style={{ marginLeft: '17%' }}>
                            <CustomButtonOutline content={`Set actual start date`} />
                            <p style={{ color: 'indianred' }}>Milestone cannot be set as achieved as its actual start date has not been set yet. Click above button to set actual date.</p>
                        </div>
                    </Form.Group>
                </Form>
            </Card>
        </div>
    );
};

export default CloseMilestone;
