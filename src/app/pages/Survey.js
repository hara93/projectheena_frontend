/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import "../../index.scss";

import Form from "react-bootstrap/Form";
import { Col, Card, Button } from "react-bootstrap";
import CustomButton from "../components/CustomButton";
import "bootstrap/dist/css/bootstrap.min.css";
import Modal from "react-bootstrap/Modal";
import DatePicker from "react-datepicker";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const Survey = () => {
  const [count, setCount] = useState(0);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [startDate, setStartDate] = useState();

  var maxCount = 256;

  const [freq, setFreq] = useState("");

  let ComponentToShow;
  if (freq === "Weekly") {
    ComponentToShow = (
      <>
        <Form.Control as="select" size="lg" custom>
          <option></option>
          <option>Sunday</option>
          <option>Monday</option>
          <option>Tuesday</option>
          <option>Wednesday</option>
          <option>Thursday</option>
          <option>Friday</option>
          <option>Saturday</option>
        </Form.Control>
        <span className="help-block">Select Day</span>
      </>
    );
  } else if (freq === "Monthly" || freq === "Quarterly" || freq === "Yearly") {
    ComponentToShow = (
      <>
        <DatePicker
          placeholderText="Select Date"
          selected={startDate}
          dateFormat="dd-MM-yyyy"
          selectsStart
          startDate={startDate}
          onChange={(date) => setStartDate(date)}
        />
        <span className="help-block">
          Please select the start date. A reminder email will be sent on this
          date basis frequency selected.
        </span>
      </>
    );
  }

  return (
    <>
      <Modal show={show} onHide={handleClose} animation={false} size="lg">
        {/* <Modal.Header closeButton>
          <Modal.Title>title of this modal</Modal.Title>
        </Modal.Header> */}

        <div
          style={{
            height: "60px",
            borderBottom: "1px solid #cccccc",
            padding: "15px 10px 5px 10px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h4>Select Milestones</h4>
          <Button
            variant="secondary"
            style={{ height: "30px" }}
            onClick={handleClose}
          >
            <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div>
            <Form>
              <Form.Group>
                <div style={{ padding: "15px" }}>
                  <Form.Check
                    id="fill once"
                    type="checkbox"
                    name="fillonce"
                    label="FY 2016-17 - Phase I - Advocacy, Mobilization and Survey"
                    className="bootstrap-checkbox"
                    style={{ padding: "10px" }}
                  />
                  <Form.Check
                    id="fill once"
                    type="checkbox"
                    name="fillonce"
                    label="FY 2016-17 - Phase II - Enrollment, Transport and Follow-up"
                    className="bootstrap-checkbox"
                    style={{ padding: "10px" }}
                  />
                  <Form.Check
                    id="fill once"
                    type="checkbox"
                    name="fillonce"
                    label="FY 2016-17 - Phase IV - Community Mobilization, Interventions and Monitoring"
                    className="bootstrap-checkbox"
                    style={{ padding: "10px" }}
                  />
                  <Form.Check
                    id="fill once"
                    type="checkbox"
                    name="fillonce"
                    label="FY 2016-17 - Phase II - Enrollment, Transport and Follow-up"
                    className="bootstrap-checkbox"
                    style={{ padding: "10px" }}
                  />{" "}
                  <Form.Check
                    id="fill once"
                    type="checkbox"
                    name="fillonce"
                    label="FY 2017-18 - Phase I - Advocacy, Mobilization and Survey"
                    className="bootstrap-checkbox"
                    style={{ padding: "10px" }}
                  />
                  <Form.Check
                    id="fill once"
                    type="checkbox"
                    name="fillonce"
                    label=" FY 2017-18 - Phase II - Enrollment, Transport and Follow-up"
                    className="bootstrap-checkbox"
                    style={{ padding: "10px" }}
                  />{" "}
                  <Form.Check
                    id="fill once"
                    type="checkbox"
                    name="fillonce"
                    label="FY 2017-18 - Phase III - Identify, Re-Survey and Enrollment"
                    className="bootstrap-checkbox"
                    style={{ padding: "10px" }}
                  />
                  <Form.Check
                    id="fill once"
                    type="checkbox"
                    name="fillonce"
                    label="FY 2017-18 - Phase IV - Community Mobilization, Interventions and Monitoring"
                    className="bootstrap-checkbox"
                    style={{ padding: "10px" }}
                  />
                </div>
              </Form.Group>
            </Form>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Select Milestones" />
        </Modal.Footer>
      </Modal>
      <div style={{ marginBottom: "30px" }}>
        <h2 className="font-bold">Create Survey</h2>
        <p>
          Following form will help you create / edit surveys. Please make sure
          to capture all the questions AND do ask for respondent details (Name,
          contact, etc) if required
        </p>
        <p>You are creating survey for ""</p>
      </div>
      <Card style={{ padding: "30px " }}>
        <Form>
          <Form.Group>
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Survey Name
              </Form.Label>
              <Col>
                <Form.Control
                  size="lg"
                  type="text"
                  placeholder="Enter survey name"
                  className="green-focus"
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Survey Description
              </Form.Label>
              <Col>
                <Form.Control
                  size="lg"
                  type="text"
                  as="textarea"
                  rows="5"
                  maxLength="256"
                  placeholder="Survey Description"
                  onChange={(e) => setCount(e.target.value.length)}
                  className="green-focus"
                />
                <span className="help-block">
                  {" "}
                  Max: 256 chars | Remaining Chars: {maxCount - count}
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Opening Message
              </Form.Label>
              <Col>
                <Form.Control
                  size="lg"
                  type="text"
                  as="textarea"
                  rows="5"
                  maxLength="256"
                  placeholder=" Opening Message"
                  onChange={(e) => setCount(e.target.value.length)}
                  className="green-focus"
                />
                <span className="help-block">
                  This is displayed at the start of the survey. Leave it blank
                  to have no opening message.
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Closing Message
              </Form.Label>
              <Col>
                <Form.Control
                  size="lg"
                  type="text"
                  as="textarea"
                  rows="5"
                  maxLength="256"
                  placeholder=" Closing Message"
                  onChange={(e) => setCount(e.target.value.length)}
                  className="green-focus"
                />
                <span className="help-block">
                  This is displayed when the user completes the survey. Leave it
                  blank to have the default message.
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Limit
              </Form.Label>
              <Col>
                <Form.Control
                  size="lg"
                  type="number"
                  placeholder="Survey Limit"
                  className="green-focus"
                />
                <span className="help-block">
                  The amount of users you would like to limit to taking this
                  survey. Set to 0 to allow for unlimited.
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Status
              </Form.Label>
              <Col>
                <Form.Control as="select" size="lg" custom>
                  <option>Inactive</option>
                  <option>Active</option>
                  <option>Old</option>
                </Form.Control>
                <span className="help-block">
                  It's a good idea to put your new surveys as inactive until you
                  have finished creating the questionaire.
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Milestones
              </Form.Label>
              <Col>
                <div onClick={handleShow}>
                  <button className="report-questions-btn" type="button">
                    Select Milestones
                  </button>
                </div>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Fill once
              </Form.Label>
              <Col>
                <FormControlLabel
                  control={<Checkbox color="primary" />}
                  value="Fill once"
                />
                <span className="help-block">
                  Checking this box will make it so a user can only submit
                  answers to the Survey once.
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Registered Users Only
              </Form.Label>
              <Col>
                <FormControlLabel
                  control={<Checkbox color="primary" />}
                  value="Registered users only"
                />
                <span className="help-block">
                  Only users who are logged in and registered will be able to
                  use this survey when this option is checked.
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Private Survey
              </Form.Label>
              <Col>
                <FormControlLabel
                  control={<Checkbox color="primary" />}
                  value="Private"
                />
                <span className="help-block">
                  Private Surveys can only be access via their URL. A special
                  token is generated for the URL to make it impossible to guess.
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Has Beneficiary
              </Form.Label>
              <Col>
                <FormControlLabel
                  control={<Checkbox color="primary" />}
                  value="Has beneficiary"
                />
                <span className="help-block">
                  If selected, each survey response will be mapped to project
                  beneficiaries
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Go Back URL
              </Form.Label>
              <Col>
                <Form.Control
                  size="lg"
                  type="text"
                  placeholder="Enter survey name"
                  className="green-focus"
                />
                <span className="help-block">
                  When the user finishes the survey, enter the URL that the Go
                  Back link will point to.
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Persons Responsible
              </Form.Label>
              <Col>
                <Form.Control
                  size="lg"
                  type="text"
                  placeholder="Enter survey name"
                  className="green-focus"
                />
                <span className="help-block">
                  If specified, only persons responsible will be able to fill
                  surveys.
                </span>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Survey Frequency
              </Form.Label>
              <Col lg={4}>
                <Form.Control
                  as="select"
                  size="lg"
                  custom
                  onClick={(e) => setFreq(e.target.value)}
                >
                  <option></option>
                  <option>Daily</option>
                  <option>Weekly</option>
                  <option>Monthly</option>
                  <option>Quarterly</option>
                  <option>Yearly</option>
                </Form.Control>
                <span className="help-block">
                  Specifies frequency in which survey owner has to fill surveys.
                </span>
              </Col>
              <Col lg={4}>{ComponentToShow}</Col>
            </Form.Row>
            <hr className="form-horizontal-line" />{" "}
          </Form.Group>
        </Form>
        <Form.Row>
          <Form.Label column="lg" lg={2}></Form.Label>
          <Col>
            <a href="/surveyMainPage">
              <CustomButton content="Create Survey" />
            </a>
          </Col>
        </Form.Row>
      </Card>
    </>
  );
};
export default Survey;
