import React from "react";
import TabNav from "./TabNav";
import TabContent from "./TabContent";
import "../../index.scss";
import { Images } from "../config/Images";
import CustomButton from "../components/CustomButton";

class Volunteer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: "Volunteering",
    };
  }

  setSelected = (tab) => {
    this.setState({ selected: tab });
  };

  render() {
    return (
      <div className="container">
        <h3 style={{ fontWeight: 600 }}>Help</h3>
        <TabNav
          tabs={["Volunteering", "Donation", "Advocacy", "Collaboration"]}
          selected={this.state.selected}
          setSelected={this.setSelected}
          style={{ margin: 0 }}
        >
          <TabContent isSelected={this.state.selected === "Volunteering"}>
            <div className="tab-details px-4">
              <h5
                style={{
                  marginTop: "2vh",
                  marginLeft: "2vw",
                  marginBottom: "2vh",
                }}
              >
                Welcome to the World of Volunteering
              </h5>
              <hr
                style={{
                  backgroundColor: "lightgray",
                  marginTop: "0px",
                  width: "100%",
                }}
              />
              <div className="container">
                <div className="row no-gutters">
                  <div className="col-1">
                    <img
                      src={Images.volunteer}
                      style={cssStyle.imgCss}
                      alt=""
                    />
                  </div>
                  <div className="col-11">
                    <p>
                      Congratulations, on your decision to take action and
                      Volunteer for Change. We all desire a better world and
                      have ideas that can bring transformation. However it takes
                      real action to make the impact the world needs. Ready to
                      be a 'Doer' ? Have any doubts on Volunteering? Let us
                      address these right away and welcome you to the world of
                      doing good.
                    </p>
                  </div>
                </div>
              </div>

              <div>
                <h6 style={{ margin: "2vh 0vw 1vh 2vw" }}>
                  What is Volunteering?
                </h6>
                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  Volunteering has always been synonymously used with the terms
                  Community Services or pro-bono help. While most of us have the
                  privilege to lead a good life, we know that a huge community
                  outside our four walls needs more help and care.
                </p>
              </div>
              <div>
                <blockquote style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  <p>
                    Volunteers do not necessarily have the time; they just have
                    the heart.
                  </p>
                  <small>Elizabeth Andrew</small>
                </blockquote>
              </div>
              <div>
                <h6 style={{ margin: "3vh 0vw 1vh 1vw", fontStyle: "bold" }}>
                  Why Volunteer? What are the benefits of Volunteering?
                </h6>
                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  While volunteering may seem a great contribution from your
                  side, it also is a great experience and brings gain to the
                  giver too! Surprised! Lets highlight why should one volunteer
                </p>
                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  When you Volunteer You Give
                </p>
                <ul>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    A better life to a needy person
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    A way for someone to be self sustainable or access a
                    resource
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    A better society that cleaner, greener and altruistic in
                    nature
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    A community that cherishes all its achievements and shares
                    all the challenges together
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    A better planet that grows together
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    And many more ....
                  </li>
                </ul>

                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  However when you Volunteer you also gain
                </p>
                <ul>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    A Chance to learn a new skill
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    Compassion and happiness of making a difference
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    More Confidence & self esteem in your work
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    Understanding of management skills
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    Understanding of life by meeting new people, thought and
                    cultures to broaden your horizons
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    To make a difference beyond what you are just supposed to do
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    And many more ....
                  </li>
                </ul>

                <h6 style={{ margin: "3vh 0vw 1vh 1vw", fontStyle: "bold" }}>
                  Can I Volunteer? What it takes?
                </h6>
                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  While you might think Volunteering as a huge task or
                  commitment, believe us you are far from reality. You can just
                  contribute a few hours, a small skill or a few days in the
                  whole year and still can make a huge impact. Here on this
                  portal we match your location, skills and time preferences
                  with the right causes and give you the opportunity to explore
                  several areas where you can contribute.
                </p>

                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>So here is the gist</p>
                <ul>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    This doesn't require a huge commitment (although even that
                    is most welcome). A few hours a month is good enough to make
                    an impact
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    This doesn't require you to Change the whole world in a day,
                    just starting with a small act of kindness is enough
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    This doesn't require a lot of hard work.
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    Remember if you are reading this sentence, a million kids
                    outside would love to learn to read the same from you
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    All it does require is an Open mind, Compassionate Heart and
                    Zeal to make a difference. We don't promise it will be all
                    easy and fun, but we promise it would be worth it!
                  </li>
                </ul>
                <div style={{ marginLeft: "42%", marginBottom: "3vh" }}>
                  <CustomButton content={"Join Today!"} />
                </div>
                <p
                  style={{
                    marginTop: "1vh",
                    marginLeft: "7vw",
                    marginBottom: "5vh",
                  }}
                >
                  Once logged in you can access more FAQs here. Also share this
                  with more volunteers & invite them to join you.
                </p>
              </div>
            </div>
          </TabContent>

          <TabContent isSelected={this.state.selected === "Donation"}>
            <div className="tab-details px-4">
              <h5
                style={{
                  marginTop: "2vh",
                  marginLeft: "2vw",
                  marginBottom: "2vh",
                }}
              >
                Donating Responsibly & Creating an Impact
              </h5>
              <hr
                style={{
                  backgroundColor: "lightgray",
                  marginTop: "0px",
                  width: "100%",
                }}
              />
              <div className="container">
                <div className="row no-gutters">
                  <div className="col-1">
                    <img
                      src={Images.donation}
                      style={cssStyle.imgCss}
                      alt=""
                    />
                  </div>
                  <div className="col-11">
                    <p>
                      {" "}
                      While donating something to the needy has been in our
                      nature from the very day we started our existence, however
                      from the same time we have doubts about how much, why and
                      to whom these donations to be made? Whether they make an
                      impact? Or are we supposed to leave everything on karma?
                    </p>
                  </div>
                </div>
              </div>
              <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                We all are aware that there is a huge world outside that is not
                as privileged as we are right now. We all know that sharing a
                small piece of what we have with the ‘have nots’ of the world
                will not make a significant difference to us but will have a
                great impact on their lives than why do we have such doubts?
              </p>

              <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                With the donations module we plan to simplify all the doubts and
                issues with regards to donating for a noble cause. We call it
                “Responsible Donations”
              </p>

              <div>
                <h6 style={{ margin: "3vh 0vw 1vh 1vw", fontStyle: "bold" }}>
                  Here is how we solve the problem:
                </h6>

                <ul>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    Whatever donations requests you see online are properly
                    vetted before they reach you. We check the need and the
                    impact that it will make on ground.
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    The work doesn’t complete ones you have completed the
                    donation, rather it starts from there. Every receiver is
                    supposed to update the utilization of funds in a transparent
                    manner with all the required evidences.
                  </li>
                  <li
                    style={{
                      padding: "0px 0px 0px 0px ",
                      listStyleType: "circle",
                    }}
                  >
                    We ask people to share progress and audit the evidences so
                    that more confidence is garnered and further donors can be
                    reached by sharing the updates with the world.
                  </li>
                </ul>

                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  How powerful will it be if even after a year you would have an
                  option to go back, open the donation page that you
                  participated in and track the progress the way you want?
                </p>
                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  What if there was an option to suggest the receiver and
                  provide him views on how the funds can be better utilized?
                </p>
                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  Will we be ready to share a small piece of what we have, when
                  we know that it’s being used in the right cause, in the right
                  manner and with complete transparency?
                </p>
                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  If your answers to any or all the above questions was a
                  resounding YES! Than like us, probably you too prefer making
                  impactful donations in a responsible way rather than not
                  taking action at all! We welcome you to check all the
                  available donations in the donation module online and
                  contribute to a larger good.
                </p>
                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  You have all the required details of the donations already
                  made available, and if for any reason you find that certain
                  data isn’t there we are always a call/mail away.
                </p>
                <p style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  Happy Donating (Responsibly)!
                </p>
              </div>
            </div>
          </TabContent>

          <TabContent isSelected={this.state.selected === "Advocacy"}>
            <div className="tab-details px-4">
              <h5
                style={{
                  marginTop: "2vh",
                  marginLeft: "2vw",
                  marginBottom: "2vh",
                }}
              >
                Let's Network & Advocate a Cause
              </h5>
              <hr
                style={{
                  backgroundColor: "lightgray",
                  marginTop: "0px",
                  width: "100%",
                }}
              />
              <div className="container">
                <div className="row no-gutters">
                  <div className="col-1">
                    <img
                      src={Images.advocate}
                      style={cssStyle.imgCss}
                      alt=""
                    />
                  </div>
                  <div className="col-11">
                    <p>
                      We live in the world that is well connected and by virtue
                      of being always online and active we are well networked
                      within our circles. While Change does require on-ground
                      voluntary action or monetary assistance , we have also
                      garnered a Network that can be leveraged to achieve social
                      good.
                    </p>
                  </div>
                </div>
              </div>

              <p style={{ margin: "2vh 1vw 2vh 1vw" }}>
                Advocacy module is all about providing your views on various
                social issues and suggesting solutions on how you would want the
                world to tackle it? And it is about garnering support and
                relying on the readers network to amplify the same amongst
                everybody.
              </p>
              <div>
                <blockquote style={{ margin: "2vh 0vw 1vh 1vw" }}>
                  <p>
                    We all are relatively connected to 300-500 people online via
                    several networks. What if we spread a right message to the
                    right audience and leverage this network to raise awareness
                    for a social cause?
                  </p>
                </blockquote>
              </div>

              <h6 style={{ margin: "3vh 0vw 1vh 1vw", fontStyle: "bold" }}>
                The Advocacy module helps you do the following
              </h6>

              <ul>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Share your opinion and views regarding various social issues
                </li>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Read through views of several other change makers
                </li>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Share the views you agree with within your network and help
                  nonprofits and foundations raise awareness
                </li>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Amplify the right messages and garner support online
                </li>
              </ul>

              <p style={{ margin: "2vh 1vw 2vh 1vw" }}>
                Like every business invests significantly on several marketing
                campaigns to make a mark in the customers mind, we cannot have
                the luxury of similar funds for a social cause. However if each
                one of us does that bit in creating, consuming and sharing the
                right content. then the impact made by Advocacy will be far
                greater.
              </p>

              <p style={{ margin: "2vh 1vw 2vh 1vw" }}>
                By the way we do vet the views that you see online on the portal
                and only the relevant ones are approved and whenever you share
                something online we manage the count of advocate internally. Who
                know you might have an impact just by sharing!
              </p>

              <p style={{ margin: "2vh 1vw 2vh 1vw" }}>
                Let the creation and sharing begin!
              </p>
            </div>
          </TabContent>

          <TabContent isSelected={this.state.selected === "Collaboration"}>
            <div className="tab-details px-4">
              <h5
                style={{
                  marginTop: "2vh",
                  marginLeft: "2vw",
                  marginBottom: "2vh",
                }}
              >
                Impactful CSR Projects & Collaboration
              </h5>
              <hr
                style={{
                  backgroundColor: "lightgray",
                  marginTop: "0px",
                  width: "100%",
                }}
              />
              <div className="container">
                <div className="row no-gutters">
                  <div className="col-1">
                    <img
                      src={Images.collaborate}
                      style={cssStyle.imgCss}
                      alt=""
                    />
                  </div>
                  <div className="col-11">
                    <p>
                      While we provide the easiest ways to make an impact and
                      change the world we realize that most of them are directed
                      towards individuals. Whether it’s Volunteering, Donations
                      or Advocacy all the modules rely on individuals coming
                      together and making an impact. How about teams coming
                      together for a bigger task?
                    </p>
                  </div>
                </div>
              </div>

              <p style={{ margin: "2vh 1vw 2vh 1vw" }}>
                Collaborate or the Collaboration module is meant for the CSR
                teams to work on larger projects for a longer time period and
                track the complete projects online, in turn bringing significant
                impact on ground.
              </p>

              <h6 style={{ margin: "3vh 0vw 1vh 1vw", fontStyle: "bold" }}>
                A simple example for all the modules can be this (Assuming
                education is the cause we are championing here):
              </h6>

              <ul>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Volunteering – Teaching kids math or how to draw during
                  weekends, max 3-4 times a month
                </li>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Donations – Donating for books Or 1 month of child’s education
                  online
                </li>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Advocacy – Creating and sharing unique solutions on how
                  illiteracy can be eradicated or sharing status updates with
                  your network about the progress so they can join too
                </li>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Collaborate – Creating or maintaining the school for a longer
                  period (6 months to couple of years). This might include
                  funding the school, managing several milestones, research
                  about the real needs, managing the beneficiaries of the
                  school, surveying them to understand needs and the impact made
                  etc.
                </li>
              </ul>

              <p style={{ margin: "2vh 1vw 2vh 1vw" }}>
                Collaboration module allows the CSR team to come work together,
                with NGOs/Foundations, Auditors, Volunteers or other social
                enterprises to manage large long running projects. Our teams and
                partners can start and manage a complete project online where
                all the details are captured and instead of random means of
                communication, all the communication can be captured online on
                one single platform.
              </p>

              <h6 style={{ margin: "3vh 0vw 1vh 1vw", fontStyle: "bold" }}>
                Most of the modules like the collaboration modules helps in the
                following ways:
              </h6>

              <ul>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Allow all collaborators/partners to come online on a single
                  platform
                </li>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Update and track the progress online
                </li>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Report the achievements and upload evidences like Photos,
                  reports etc. in the upload section
                </li>
                <li
                  style={{
                    padding: "0px 0px 0px 0px ",
                    listStyleType: "circle",
                  }}
                >
                  Archive every update so that it can be audited any given point
                  in time
                </li>
              </ul>

              <p style={{ margin: "2vh 1vw 2vh 1vw" }}>
                The collaborate module will set new benchmarks in transparency
                and help CSR team to track their CSR projects in a very
                convenient way possible. In case if you find that a certain
                section of society can be benefited by executing a long term
                project there, your suggestions are most welcome and we invite
                you to partner with us for our long term projects too.
              </p>

              <p style={{ margin: "2vh 1vw 2vh 1vw" }}>
                Log in to know more about how we can collaborate together to
                make a better world!
              </p>
            </div>
          </TabContent>
        </TabNav>
      </div>
    );
  }
}

const cssStyle = {
  imgCss: { width: "75px", height: "100%" }
}
export default Volunteer;
