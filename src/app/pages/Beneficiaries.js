/* eslint-disable no-restricted-imports */
import React from "react";
// eslint-disable-next-line no-restricted-imports

import { Table, Card } from "react-bootstrap";
import CustomButtonOutline from "../components/CustomButtonOutline";
import { makeStyles } from "@material-ui/core/styles";
import "../../index.scss";
import { FaUsers } from "react-icons/fa";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";

const deleteData = () => {
  //to delete the data
};

const BeneficiaryList = [
  { name: "Sunita Sule", type: "Person", gender: "Female" },
  { name: "Priya Chitale", type: "Person", gender: "Female" },
  { name: "Shweta Mahliskar", type: "Person", gender: "Female" },
  { name: "Aniket Kudalkar", type: "Person", gender: "Male" },
  { name: "Aniket Bangar", type: "Person", gender: "Male" },
  { name: "Rohit Kudalkar", type: "Person", gender: "Male" },
  { name: "Komal Kamble", type: "Person", gender: "Female" },
];

const Beneficiaries = () => {
  const useStylesBootstrap = makeStyles((theme) => ({
    arrow: {
      color: theme.palette.common.black,
    },
    tooltip: {
      backgroundColor: theme.palette.common.black,
      fontSize: "12px",
    },
  }));
  function BootstrapTooltip(props) {
    const classes = useStylesBootstrap();

    return <Tooltip arrow classes={classes} {...props} />;
  }
  return (
    <Card style={{ padding: "15px 0" }}>
      <div className="ibox-content">
        <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
          <FaUsers style={{ fontSize: "2rem" }} /> Project Beneficiaries
        </h3>

        <div className="beneficiaries-table">
          <Table striped hover size="sm">
            <thead>
              <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Gender</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {BeneficiaryList.map((ele, index) => (
                <tr>
                  <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                    {ele.name}
                  </td>
                  <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                    {ele.type}
                  </td>
                  <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                    {ele.gender}
                  </td>
                  <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                    <BootstrapTooltip
                      TransitionComponent={Fade}
                      TransitionProps={{ timeout: 300 }}
                      title="Remove Beneficiary"
                    >
                      <button className="btn-beneficiaries">
                        <i class="far fa-trash-alt beneficiary-trash"></i>
                      </button>
                    </BootstrapTooltip>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
          <div style={{ marginTop: "15px" }}>
            <a href="/searchBeneficiary">
              {" "}
              <CustomButtonOutline content="Add beneficiaries" />
            </a>
          </div>
        </div>
      </div>
    </Card>
  );
};
export default Beneficiaries;
