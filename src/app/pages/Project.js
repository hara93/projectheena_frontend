import React from "react";

import * as ReactBootstrap from "react-bootstrap";

class ProjectPage extends React.Component {
  render() {
    return (
      <>
        <h1
          style={{
            color: "gray",
            paddingLeft: "3vw",
            marginTop: "3vh",
            marginBottom: "3vh",
          }}
        >
          Search: 155 Projects found that matches your query
        </h1>

        <div className="container">
          <div className="row">
            <div className="col-md-3"></div>
            <div className="col-md-6">
              <div className="tabs">
                <div className="btn-group btn-group-justified">
                  <a href="/people" className="btn btn-white btn-sm active">
                    {" "}
                    People{" "}
                  </a>
                  <a href="/search" className="btn btn-white btn-sm ">
                    {" "}
                    Tasks
                  </a>
                  <a href="/project" className="btn btn-white btn-sm ">
                    {" "}
                    Project{" "}
                  </a>
                  <a href="/donation" className="btn btn-white btn-sm ">
                    {" "}
                    Donation{" "}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div
              className="col-sm-12 col-lg-3"
              style={{ height: "40%", marginTop: "4vh" }}
            >
              <div class="filters">
                <div className="filter_heading" style={{ marginTop: "1vh" }}>
                  Duration
                </div>
                <input
                  placeholder="Enter Duration"
                  className="contri"
                  type="number"
                  style={{ width: "100%", textAlign: "left" }}
                />

                <div
                  className="filter_heading"
                  style={{ marginTop: "2vh", marginBottom: "1vh" }}
                >
                  Causes
                </div>
                <select className="cause">
                  <option>All Causes</option>
                  <option>Animal Welfare</option>
                  <option>Youth and Children </option>
                  <option>Women </option>
                </select>

                <div
                  className="filter_heading"
                  style={{ marginTop: "1vh", display: "block" }}
                >
                  City
                </div>
                <input
                  className="tags"
                  type=""
                  placeholder="Enter a Location"
                />

                <div
                  className="filter_heading"
                  style={{ marginTop: "1vh", display: "block" }}
                >
                  Tag
                </div>
                <input className="tags" type="" />

                <div>
                  <button type="submit" className="task">
                    Search Tasks
                  </button>
                </div>
              </div>
            </div>

            <div className="col-sm-12 col-lg-8">
              <div className="row">
                <div className="col-sm-12 col-lg-6">
                  <div className="task_card">
                    <div className="row">
                      <div className="col-8">
                        <h5
                          style={{
                            color: "#4CAF50",
                            padding: "2vh 2vw 2vh 1vw",
                          }}
                        >
                          Clean your place
                        </h5>
                        <p style={{ padding: "1vh 0 0 1vw" }}>
                          he 1500s, when an unknown printer took a galley of
                          type and scrambled it to make a type specimen book. It
                          has survived not only five centuries , but also the
                          leap into electronic typesetting, remaining
                          essentially unchanged. It was popularised in the 1960s
                          with ...
                        </p>
                      </div>
                      <div className="col-4">
                        <img
                          style={{
                            width: "100%",
                            height: "40%",
                            padding: "1vh 1vw 1vh 1vw",
                          }}
                          src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12 col-lg-6">
                  <div className="task_card">
                    <div className="row">
                      <div className="col-8">
                        <h5
                          style={{
                            color: "#4CAF50",
                            padding: "2vh 2vw 2vh 1vw",
                          }}
                        >
                          Clean your place
                        </h5>
                        <p style={{ padding: "1vh 0 0 1vw" }}>
                          he 1500s, when an unknown printer took a galley of
                          type and scrambled it to make a type specimen book. It
                          has survived not only five centuries , but also the
                          leap into electronic typesetting, remaining
                          essentially unchanged. It was popularised in the 1960s
                          with ...
                        </p>
                      </div>
                      <div className="col-4">
                        <img
                          style={{
                            width: "100%",
                            height: "40%",
                            padding: "1vh 1vw 1vh 1vw",
                          }}
                          src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12 col-lg-6">
                  <div className="task_card">
                    <div className="row">
                      <div className="col-8">
                        <h5
                          style={{
                            color: "#4CAF50",
                            padding: "2vh 2vw 2vh 1vw",
                          }}
                        >
                          Clean your place
                        </h5>
                        <p style={{ padding: "1vh 0 0 1vw" }}>
                          he 1500s, when an unknown printer took a galley of
                          type and scrambled it to make a type specimen book. It
                          has survived not only five centuries , but also the
                          leap into electronic typesetting, remaining
                          essentially unchanged. It was popularised in the 1960s
                          with ...
                        </p>
                      </div>
                      <div className="col-4">
                        <img
                          style={{
                            width: "100%",
                            height: "40%",
                            padding: "1vh 1vw 1vh 1vw",
                          }}
                          src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12 col-lg-6">
                  <div className="task_card">
                    <div className="row">
                      <div className="col-8">
                        <h5
                          style={{
                            color: "#4CAF50",
                            padding: "2vh 2vw 2vh 1vw",
                          }}
                        >
                          Clean your place
                        </h5>
                        <p style={{ padding: "1vh 0 0 1vw" }}>
                          he 1500s, when an unknown printer took a galley of
                          type and scrambled it to make a type specimen book. It
                          has survived not only five centuries , but also the
                          leap into electronic typesetting, remaining
                          essentially unchanged. It was popularised in the 1960s
                          with ...
                        </p>
                      </div>
                      <div className="col-4">
                        <img
                          style={{
                            width: "100%",
                            height: "40%",
                            padding: "1vh 1vw 1vh 1vw",
                          }}
                          src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12 col-lg-6">
                  <div className="task_card">
                    <div className="row">
                      <div className="col-8">
                        <h5
                          style={{
                            color: "#4CAF50",
                            padding: "2vh 2vw 2vh 1vw",
                          }}
                        >
                          Clean your place
                        </h5>
                        <p style={{ padding: "1vh 0 0 1vw" }}>
                          he 1500s, when an unknown printer took a galley of
                          type and scrambled it to make a type specimen book. It
                          has survived not only five centuries , but also the
                          leap into electronic typesetting, remaining
                          essentially unchanged. It was popularised in the 1960s
                          with ...
                        </p>
                      </div>
                      <div className="col-4">
                        <img
                          style={{
                            width: "100%",
                            height: "40%",
                            padding: "1vh 1vw 1vh 1vw",
                          }}
                          src="https://qph.fs.quoracdn.net/main-qimg-dc0a72786b34882e7a38039bb77e3ce2"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <ReactBootstrap.Pagination style={{ marginLeft: "40%" }}>
          <ReactBootstrap.Pagination.Prev />
          <ReactBootstrap.Pagination.Item>{1}</ReactBootstrap.Pagination.Item>

          <ReactBootstrap.Pagination.Item>{2}</ReactBootstrap.Pagination.Item>
          <ReactBootstrap.Pagination.Item>{3}</ReactBootstrap.Pagination.Item>
          <ReactBootstrap.Pagination.Item active>
            {4}
          </ReactBootstrap.Pagination.Item>

          <ReactBootstrap.Pagination.Ellipsis />
          <ReactBootstrap.Pagination.Item>{20}</ReactBootstrap.Pagination.Item>

          <ReactBootstrap.Pagination.Next />
        </ReactBootstrap.Pagination>
      </>
    );
  }
}

export default ProjectPage;
