import React from "react";
import "../login.css";
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom"
import { Images } from "../config/Images";
import CustomButton from "../components/CustomButton";

class CorporateLogin extends React.Component {
  render() {
    return (
      <div className="login-container" style={{ maxWidth: '830px', margin: "0 auto" }}>


        <div className="d-flex flex-row">
          <div style={{ maxWidth: "80px" }}>
            <img
              src={Images.NGOSignup}
              alt=""
              className="login-avatar"
            />
          </div>
          <div>
            <h2 className="fw-bold">Corporate Team access</h2>
            <p>
              Corporate teams get a dedicated group on ProjectHeena to
              manage their social initiatives & CSR Projects. If you are a
              Corporate Volunteer or CSR Manager you would receive a
              separate URL to login and use your account.
            </p>
          </div>
        </div>

        <Col md={12} style={{ display: "flex", justifyContent: "center" }}>
          <div style={{ maxWidth: "660px" }}>
            <Row>
              <Col md={6}>
                <div style={{ display: "table-cell", padding: "0 20px" }}>
                  <img src={Images.Employee} alt="" />
                </div>

                <div style={{ display: "table-cell" }}>
                  <h5 style={{ fontSize: "14px" }}>Employee Engagement</h5>
                </div>
              </Col>

              <Col md={6}>
                <div style={{ display: "table-cell", padding: "0 20px" }}>
                  <img src={Images.Philanthropy} alt=""></img>
                </div>
                <div style={{ display: "table-cell" }}>
                  <h5 style={{ fontSize: "14px" }}>
                    Philanthropy Solutions
                  </h5>
                </div>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <div style={{ display: "table-cell", padding: "0 20px" }}>
                  <img src={Images.Monitoring} alt="" />
                </div>

                <div style={{ display: "table-cell" }}>
                  <h5 style={{ fontSize: "14px" }}>
                    CSR Project Monitoring
                  </h5>
                </div>
              </Col>

              <Col md={6}>
                <div style={{ display: "table-cell", padding: "0 20px" }}>
                  <img src={Images.Advocacy} alt=""></img>
                </div>
                <div style={{ display: "table-cell" }}>
                  <h5 style={{ fontSize: "14px", textAlign: "left" }}>
                    {" "}
                    Advocacy & Social Brand Building
                  </h5>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Row>
          <div style={{ margin: "30px auto 0 auto" }}>
            <Link to="/corporateSolutions">
              <CustomButton content={`Know More`} />
            </Link>
          </div>
        </Row>


      </div>
    );
  }
}

export default CorporateLogin;
