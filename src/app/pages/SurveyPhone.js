import React from "react";
import {
  Form,
  Card,
  Container,
  Row,
  Col,
  OverlayTrigger,
  Tooltip,
  Modal,
  Button,
} from "react-bootstrap";
import Question from "../components/Question";
import Example from "../components/Phone";

const SurveyPhone = () => {
  return (
    <Card className="p-5" style={{ color: "#9a9ba5" }}>
      <Question />

      <Form className="p-3">
       
        <Example/>

        <Form.Label style={{ marginTop: "3%", color: "#9a9ba5" }}>
          Settings
        </Form.Label>
        <div style={{ color: "#9a9ba5" }}>
          <input type="checkbox"></input>
          <label style={{ marginLeft: "2%" }}>
            Allow user to select an image from gallery
          </label>
        </div>

        <hr></hr>
        <i class="fas fa-trash"> Delete</i>
        <i class="fas fa-compress-alt" style={{ marginLeft: "2%" }}>
          Move
        </i>
        <i class="fas fa-copy" style={{ marginLeft: "2%" }}>
          {" "}
          Copy
        </i>
      </Form>
    </Card>
  );
};

export default SurveyPhone;
