import React from 'react';
import ClearIcon from '@material-ui/icons/Clear';
import EditIcon from '@material-ui/icons/Edit';
import { Col, Row, Table } from 'react-bootstrap';
import { Card, CardHeader } from '@material-ui/core';
import { Images } from '../config/Images';
import { CardBody } from 'reactstrap';
import * as reactstrap from 'react-bootstrap';
import CustomButton from '../components/CustomButton';
import Checkbox from '../components/CollabCheckbox';
import CustomButtonOutline from '../components/CustomButtonOutline';
import { FaUserFriends, FaUserSecret } from 'react-icons/fa';


const collab = [
    {
        sr_no: "1",
        module: "Dashboard",
        access: false
    },
    {
        sr_no: "2",
        module: "Critical Data",
        access: false
    },
    {
        sr_no: "3",
        module: "Milestones",
        access: false
    },
    {
        sr_no: "4",
        module: "Budget Tracker",
        access: false
    },
    {
        sr_no: "5",
        module: "Activity Tracker",
        access: false
    },
    {
        sr_no: "6",
        module: "Reports",
        access: false
    },
    {
        sr_no: "7",
        module: "Collaborators",
        access: false
    },
    {
        sr_no: "8",
        module: "Beneficiaries",
        access: false
    },
    {
        sr_no: "9",
        module: "Gallery",
        access: false
    },
    {
        sr_no: "10",
        module: "Comments",
        access: false
    },
    {
        sr_no: "11",
        module: "Reviews",
        access: false
    },
    {
        sr_no: "12",
        module: "Surveys",
        access: false
    },
    {
        sr_no: "13",
        module: "Social Maps",
        access: false
    },
]



// function onChangeCollaboratorType(event) {
//     setValue(event.target.value);
// }

class Collaborators extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            collaboratorType: "User",
            responsibility: "Collaborator"
        };
    }
    // const [value, setValue] = React.useState("User");

    onChangeCollaboratorType(event) {
        console.log(event.target.value);
        this.setState({ collaboratorType: event.target.value })
    }
    onChangeResponsibility(event) {
        this.setState({ responsibility: event.target.value })
    }
    render() {


        return (
            <>
                <Row className="no-gutters">
                    <Col xs={12}>
                        <Card className="p-4">
                            <div className="item">
                                <h3 className="csr_heading"><i class="fa fa-user-secret"></i> Collaborators</h3>
                            </div>

                            {/* <h3 style={{ marginBottom: '2vh' }}> <FaUserSecret color="#676a6c" /> Collaborators</h3> */}
                            <div style={{ padding: '4vh 3vw 4vh 1vw' }}>
                                <Row>
                                    <Col xs={4}>
                                        <Card>
                                            <div style={{ marginLeft: '80%', marginTop: '4%', marginBottom: '-8%' }}>
                                                <EditIcon /><ClearIcon />

                                            </div>

                                            <img src={Images.Social_work} style={{ margin: '3vh 0vw 2vh 29%', width: '45%', borderRadius: '50%' }} />
                                            <div className="text-center"><p style={{ color: '#4CAF50' }}>Social Work</p>
                                                <p>Admin</p>
                                            </div>

                                        </Card>

                                    </Col>

                                    <Col xs={4}>
                                        <Card>
                                            <div style={{ marginLeft: '80%', marginTop: '4%', marginBottom: '-8%' }}>
                                                <EditIcon /><ClearIcon />

                                            </div>

                                            <img src={Images.Social_work} style={{ margin: '3vh 0vw 2vh 29%', width: '45%', borderRadius: '50%' }} />
                                            <div className="text-center"><p style={{ color: '#4CAF50' }}>Social Work</p>
                                                <p>Admin</p>
                                            </div>

                                        </Card>

                                    </Col>

                                    <Col xs={4}>
                                        <Card>
                                            <div style={{ marginLeft: '80%', marginTop: '4%', marginBottom: '-8%' }}>
                                                <EditIcon /><ClearIcon />

                                            </div>

                                            <img src={Images.Social_work} style={{ margin: '3vh 0vw 2vh 29%', width: '45%', borderRadius: '50%' }} />
                                            <div className="text-center"><p style={{ color: '#4CAF50' }}>Social Work</p>
                                                <p>Admin</p></div>

                                        </Card>

                                    </Col>

                                </Row>

                            </div>

                            <div className="item">
                                <h3 className="csr_heading"><i class="fa fa-group"></i> Add Collaborators</h3>
                            </div>
                            {/* <FaUserFriends /> <h5 style={{ marginTop: '3vh', display: 'inline-block' }}> </h5> */}

                            {/* <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} /> */}

                            <reactstrap.Form.Group as={reactstrap.Row} controlId="formPlaintextEmail">

                                <reactstrap.Form.Label style={{ paddingLeft: '3vw', color: ' #676a6c', fontFamily: 'sans-serif' }}>
                                    Collaborator Type
                                </reactstrap.Form.Label>
                                <reactstrap.Col sm="10" style={{ paddingRight: '4vw' }}>
                                    <reactstrap.Form.Control as="select" onChange={this.onChangeCollaboratorType.bind(this)}>
                                        <option>User</option>
                                        <option>NGO</option>
                                        <option>Team</option>

                                    </reactstrap.Form.Control>

                                </reactstrap.Col>
                            </reactstrap.Form.Group>


                            <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                            <reactstrap.Form.Group as={reactstrap.Row} controlId="formPlaintextEmail" style={{ marginLeft: '2vw' }}>
                                <reactstrap.Form.Label style={{ paddingLeft: '3vw', paddingTop: '5vh', color: ' #676a6c', fontFamily: 'sans-serif' }}>
                                    {this.state.collaboratorType == 'Team' ? 'Add Team' : this.state.collaboratorType == 'NGO' ? 'Add NGO' : 'Add User'}
                                    {/* {value == 'User' ? 'Add User' : 'Add'} */}
                                </reactstrap.Form.Label>
                                <reactstrap.Col sm="10" style={{ paddingTop: '4vh', paddingRight: '4vw' }}>
                                    <reactstrap.Form.Control type="name" style={{ width: '106%' }} />
                                </reactstrap.Col>
                            </reactstrap.Form.Group>


                            <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />


                            <reactstrap.Form.Group as={reactstrap.Row} controlId="formPlaintextEmail">

                                <reactstrap.Form.Label style={{ paddingLeft: '3vw', paddingTop: '5vh', color: ' #676a6c', fontFamily: 'sans-serif', marginLeft: '1vw' }}>
                                    Responsibility
                                </reactstrap.Form.Label>
                                <reactstrap.Col sm="10" style={{ paddingTop: '4vh', paddingRight: '4vw' }}>
                                    <reactstrap.Form.Control as="select" onChange={this.onChangeResponsibility.bind(this)} >
                                        <option>Collaborator</option>
                                        <option>Admin</option>


                                    </reactstrap.Form.Control>

                                </reactstrap.Col>
                            </reactstrap.Form.Group>


                            {this.state.responsibility == "Collaborator" ?
                                <>
                                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%', marginBottom: '4%' }} />
                                    <span> Access</span>

                                    <Checkbox collab={collab} />
                                </>
                                : null
                            }
                            <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />
                            <reactstrap.Form.Group as={reactstrap.Row} controlId="formPlaintextEmail" style={{ marginLeft: '2vw' }}>
                                <reactstrap.Form.Label style={{ paddingLeft: '3vw', paddingTop: '5vh', color: ' #676a6c', fontFamily: 'sans-serif' }}>
                                    Role
                                </reactstrap.Form.Label>
                                <reactstrap.Col sm="10" style={{ paddingTop: '4vh', paddingRight: '4vw' }}>
                                    <reactstrap.Form.Control type="name" style={{ width: '100%' }} />
                                </reactstrap.Col>
                            </reactstrap.Form.Group>




                            <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%', marginBottom: '4%' }} />



                            <reactstrap.Form.Group as={reactstrap.Row} controlId="formPlaintextPassword">
                                <reactstrap.Form.Label column sm="2" className="collab_description">
                                    Description
                                </reactstrap.Form.Label>
                                <reactstrap.Col sm="10" style={{ paddingTop: '4vh', paddingRight: '4vw' }}>
                                    <reactstrap.Form.Group controlId="exampleForm.ControlTextarea1">

                                        <reactstrap.Form.Control as="textarea" rows={3} className="collab_textbox" />
                                    </reactstrap.Form.Group>

                                </reactstrap.Col>
                            </reactstrap.Form.Group>

                            <CustomButtonOutline content={`Add Collaborator`} />




                        </Card>
                    </Col>

                </Row>

            </>
        )
    }
}

export default Collaborators;





