import React from "react";
import "../../index.scss";

import BudgetAccordion from "../components/BudgetAccordion";

import { Card, Row, Col } from "react-bootstrap";
import NewUpdate from "../components/new_update_budget";
import { FaRupeeSign } from "react-icons/fa";

const tracker = [
  {
    heading: " Partner On-Boarding",
    item_list: [
      "Support classes at Communities",
      "Program Manager",
      "Field Associate / Community Worker",
      "Educational Material for Preparatory Camps",
      "School Transportation",
    ],
    description: [
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
    ],
    qty: ["4", "4", "4", "4", "4"],

    unit_price: ["1234", "1234", "1234", "1234", "1234"],

    category: ["general", "general", "general", "general", "general"],
    total_price: ["212344", "212344", "212344", "212344", "212344"],
  },
  {
    heading: " Selection Of Districts And Villages",
    item_list: [
      "Support classes at Communities",
      "Program Manager",
      "Field Associate / Community Worker",
      "Educational Material for Preparatory Camps",
      "School Transportation",
    ],
    description: [
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
    ],
    qty: ["4", "4", "4", "4", "4"],

    unit_price: ["1234", "1234", "1234", "1234", "1234"],

    category: ["general", "general", "general", "general", "general"],
    total_price: ["212344", "212344", "212344", "212344", "212344"],
  },
  {
    heading: "Need Assessment Of Village 1",
    item_list: [
      "Support classes at Communities",
      "Program Manager",
      "Field Associate / Community Worker",
      "Educational Material for Preparatory Camps",
      "School Transportation",
    ],
    description: [
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
      "4 Classes @ Rs. 4000 pm",
    ],
    qty: ["4", "4", "4", "4", "4"],

    unit_price: ["1234", "1234", "1234", "1234", "1234"],

    category: ["general", "general", "general", "general", "general"],
    total_price: ["212344", "212344", "212344", "212344", "212344"],
  },
];

class BudgetTracker extends React.Component {
  render() {
    return (

      <div className="ibox p-3">
        <div >
          <Row>
            <Col md={12}>
              <div className="item">
                <h3 className="csr_heading"><i class="fas fa-rupee-sign"></i> Budget Tracker</h3>
              </div>
              <div className="item">
                <BudgetAccordion tracker={tracker} />

              </div>
            </Col>
          </Row>

        </div>

      </div>
      // <Card>
      //    <h3 style={{marginTop:'4vh', marginLeft:'2vw', marginBottom:'2vh'}}> <FaRupeeSign style={{marginRight:'3%'}}color="#676a6c"/>Budget Tracker</h3>
      //   <BudgetAccordion tracker={tracker} />
      //   <NewUpdate />
      // </Card>
    );
  }
}

export default BudgetTracker;
