/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import "../../index.scss";
import { useFormik } from "formik";
import { string, object } from "yup";
import axios from "../config/axios";
import BInput from "../components/form/BInput";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { Col, Card, Button, Tabs, Tab } from "react-bootstrap";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
// import FileUpload from "../components/form/FileUpload";
import CustomButton from "../components/CustomButton";
import "bootstrap/dist/css/bootstrap.min.css";
// import TextEditor from "../components/textEditor";
// import TagsInput from "../components/TagInput";
import Modal from "react-bootstrap/Modal";
import ProfessionalTab from "../components/ProfessionalTab";
import CreativeTab from "../components/CreativeTab";
import SocialTab from "../components/SocialTab";
import RadiobuttonTable from "../components/radiobuttonTable";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
import DatePicker from "react-datepicker";
// import CheckboxTable from "../components/checkboxTable";
import { connect, useSelector, useDispatch } from "react-redux";
import TagAutocomplete from "../components/tagAutocomplete";
// import { ToastContainer, toast } from "react-toastify";
import { useHistory } from "react-router-dom";

//froala editor
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/froala_editor.pkgd.min.css";
import FroalaEditorComponent from "react-froala-wysiwyg";
import "froala-editor/js/plugins.pkgd.min.js";
import { getSkillsList } from "../../redux/reducers/common/actionCreator";

const CreateTask = () => {
  const history = useHistory();

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getSkillsList());
    // console.log(milestones);
  }, []);
  const [show, setShow] = useState(false);
  const [key, setKey] = useState("home");
  const [taskimage, setImage] = useState(null);
  // const [taskName, setTaskName] = useState("");
  // console.log(taskimage);
  const user = useSelector((state) => state.phAuth.user);
  const [tag, setTag] = useState([]);
  // console.log(tag);
  const skillList = useSelector((state) => (state.common && state.common.skillsList) || null);
  // console.log(skillList);
  const [radioValue, setRadioValue] = useState("");
  const handleClose = () => {
    setShow(false);
    setSkill([]);
  };
  const handleShow = () => setShow(true);

  const [skill, setSkill] = useState([]);
  // console.log("skill", skill);
  const [toggleState, setToggleState] = useState(false);
  const handleSkill = (e) => {
    if (e.target.checked) {
      setSkill([...skill, e.target.value]);
    } else {
      skill.splice(skill.indexOf(e.target.value), 1);
      setSkill([...skill]);
    }
  };
  const selectSkill = () => {
    setShow(false);
  };
  const [loc, setLoc] = useState("");
  const [location, setLocation] = useState("");
  const [lat, setLat] = useState();
  const [lng, setLng] = useState();
  const handleChangeLocation = (e) => {
    setLoc(e.target.value);
  };

  // const randomString = (len) => {
  //   len = len || 32;
  //   let timestamp = new Date().getTime();
  //   let $chars = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678";
  //   let maxPos = $chars.length;
  //   let randomStr = "";
  //   for (let i = 0; i < len; i++) {
  //     randomStr += $chars.charAt(Math.floor(Math.random() * maxPos));
  //   }
  //   return randomStr + timestamp;
  // };

  const defaultValues = {
    task_slug: "",
    // task_name: taskName,
    task_excerpt: "",
    task_description: "",
    task_last_application: "",
    task_start_time: "",
    task_end_time: "",
    task_duration_hours: "",
    task_duration_hours_min: "",
    task_location: "",
    meeting_link: "",
    task_lat: "",
    task_lng: "",
    task_country_id: "",
    task_call: "",
    task_tags: "",
    task_contact_details: "",
    spoc_mobile: "",
    spoc_email: "",
    people_required: "",
    people_required_min: "",
    vision_page: "",
    task_type: 1,
    // has_milestones: "",

    image: null,
  };

  const validationSchema = object().shape({
    task_name: string().required("Task Name is required"),
    task_duration_hours: string().required("Task Duration is required"),
    task_duration_hours_min: string().required("Task Minimum Duration is required"),
    task_location: string().required("Task Location is required"),
    // meeting_link: string().required('Task Meeting Link is required'),
    meeting_link: string().when("task_location", {
      is: "Online",
      then: string().required("Meeting Link is required"),
    }),
    // cause_supported: string().required("Cause is required"),

    // task_excerpt: string().required('Task Excerpt is required'),
    // task_description: string().required('Task Desc is required'),
    // task_last_application: string().required('Task Last Application Date is required'),
    // task_start_time: string().required('Task Start time is required'),
    // task_end_time: string().required('Task End time is required'),

    // task_lat: string().required('Task Lat is required'),
    // task_lng: string().required('Task Long is required'),
    // task_call: string().required('Call is required'),
    // task_tags: string().required('Tag is required'),
    // task_contact_details: string().required('Contact Detail is required'),
    // spoc_mobile: string().required('Mobile is required'),
    // spoc_email: string().required('Email is required'),
    // people_required: string().required('Volunteer Count is required'),
    // people_required_min: string().required('Min Volunteer Count is required'),
    // vision_page: string().required('Vision is required'),
  });

  const onSubmit = (values) => {
    const data = new FormData();

    const keys = Object.keys(values);
    keys.forEach((key, index) => {
      if (key !== "task_description") {
        data.append(`${key}`, `${values[key]}`);
      }
    });
    if (taskimage) {
      data.append("image", taskimage);
    }
    if (desc) {
      data.append("task_description", desc);
    }
    data.append("task_creator_id", user._id);
    data.append("task_creator_type", 1);
    data.append("cause_supported", radioValue);
    if (continuous === "No") {
      data.set("is_time_bound", 1);
      data.set("task_start_date", startDate);
      data.set("task_start_time", startTime);
      data.set("task_end_date", endDate);
      data.set("task_end_time", endTime);
      data.set("task_last_application", deadline);
    } else if (continuous === "Yes") {
      data.set("is_time_bound", 0);
    }

    if (milestone === "Yes") {
      data.append("has_milestones", 1);
    } else if (milestone === "No") {
      data.append("has_milestones", 0);
    }
    var temp = [];
    if (skill) {
      skillList.Professional.forEach((ele) => {
        if (skill.includes(ele.skill_name)) {
          temp.push(ele._id);
        }
      });
    }
    if (skill) {
      skillList.Social.forEach((ele) => {
        if (skill.includes(ele.skill_name)) {
          temp.push(ele._id);
        }
      });
    }
    if (skill) {
      skillList.Creative.forEach((ele) => {
        if (skill.includes(ele.skill_name)) {
          temp.push(ele._id);
        }
      });
    }
    var mile_date = [];
    var mile_name = [];
    if (milestones) {
      milestones.forEach((ele) => mile_date.push(ele.milestone_date));
      milestones.forEach((ele) => mile_name.push(ele.milestone_name));
    }

    if (loc === "Online") {
      data.set("task_location", "Online");
    } else {
      data.set("task_location", location);
      data.set("task_lat", lat);
      data.set("task_lng", lng);
      var task_country = "";
      var temp1 = location.split(",");
      task_country = temp1[temp1.length - 1];
      data.set("task_country", task_country);
    }

    if (tag) {
      var temp_tag = [];
      tag.forEach((ele) => temp_tag.push(ele.name));
      data.set("tags", temp_tag.join(","));
    }
    // var taskName = data.get("task_name");
    if (data.get("task_name")) {
      data.set("task_slug", values.task_name.replaceAll(" ", "-").toLowerCase());
    }
    data.set("skill", temp.join());
    data.set("milestone_date", mile_date.join());
    data.set("milestone_name", mile_name.join());
    console.log("task_name", data.get("task_name"));
    console.log("task_slug", data.get("task_slug"));
    console.log("task_duration_hours_min", data.get("task_duration_hours_min"));
    console.log("task_duration_hours", data.get("task_duration_hours"));
    console.log("cause_supported", data.get("cause_supported"));
    console.log("meeting_link", data.get("meeting_link"));
    console.log("task_description", data.get("task_description"));
    console.log("skill", data.get("skill"));
    console.log("image", data.get("image"));
    console.log("desc", desc);
    console.log("is_time_bound", data.get("is_time_bound"));
    console.log("has_milestones", data.get("has_milestones"));
    console.log("task_location", data.get("task_location"));
    console.log("tags", data.get("tags"));
    console.log("task_start_date", data.get("task_start_date"));
    console.log("task_start_time", data.get("task_start_time"));
    console.log("task_end_date", data.get("task_end_date"));
    console.log("task_end_time", data.get("task_end_time"));
    console.log("task_last_application", data.get("task_last_application"));
    axios
      .post("task/create", data)
      .then(({ data }) => {
        // var message = "Task Created";
        // toast.success(message, { onClose: () => history.push("/myTasks") });
        history.push("/myTasks");
        console.info("data", data);
      })
      .catch((err) => {
        alert(err);
      });
  };

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    values,
    errors,
    touched,
    isSubmitting,
    isValidating,
    setFieldValue,
  } = useFormik({
    initialValues: defaultValues,
    validationSchema,
    onSubmit,
  });
  // console.log("err", errors);
  useEffect(() => {
    const keys = Object.keys(errors);
    if (keys.length > 0 && isSubmitting && !isValidating) {
      const nameSelector = `[name="${keys[0]}"]`;
      const nameErrorElement = document.querySelector(nameSelector);

      const idSelector = `[id="${keys[0]}"]`;
      const idErrorElement = document.querySelector(idSelector);
      if (nameErrorElement) {
        nameErrorElement.focus();
      } else if (idErrorElement) {
        idErrorElement.scrollIntoView();
      }
    }
    if (skill.length > 4) {
      setToggleState(true);
    } else {
      setToggleState(false);
    }
    // console.log("skills", skill);
  }, [isSubmitting, isValidating, errors, skill]);

  const handleFileSelect = (fileData) => {
    // console.log("fileData", fileData.currentTarget.files[0])
    setFieldValue("image", fileData.currentTarget.files[0]);
    setImage(fileData.currentTarget.files[0]);
  };
  const [desc, setDesc] = useState("");

  // const max = 5000;
  const handleModelChange = (e, editor) => {
    const char = e;
    // var plainText = char.replace(/<[^>]+>/g, "");
    // console.log("char", char);
    setDesc(char);
  };

  const config = {
    charCounterMax: 2000,
  };
  // console.log(location);
  let locationComponent;
  if (loc === "Online")
    locationComponent = (
      <>
        <Form.Row>
          <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
            Meeting Link
          </Form.Label>
          <Col>
            <BInput
              type="text"
              placeholder="Meeting Link"
              name="meeting_link"
              onChange={handleChange}
              onBlur={handleBlur}
              error={errors.meeting_link}
              value={values.meeting_link}
              touched={touched.meeting_link}
              size="lg"
            />
            {/* <Form.Control
              size="lg"
              type="text"
              placeholder="Meeting Link"
              className="green-focus"
            /> */}
          </Col>
        </Form.Row>
        <hr className="form-horizontal-line" />
      </>
    );
  else
    locationComponent = (
      <>
        <Form.Row>
          <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
            Task location
          </Form.Label>
          <Col>
            <PlacesAutoComplete
              location={(location) => {
                setLocation(location);
              }}
              lat={(lat) => {
                setLat(lat);
              }}
              lng={(lng) => setLng(lng)}
            />
          </Col>
        </Form.Row>
        <hr className="form-horizontal-line" />
      </>
    );

  const startDateOnly = (date) => {
    const d = new Date(date);
    const day = d.getDate();
    const month = d.getMonth();
    const year = d.getFullYear();
    var temp = `${day} - ${month} - ${year}`;
    console.log("function", temp);
  };
  const startTimeOnly = (time) => {
    const d = new Date(time);
    const hours = d.getHours();
    const min = d.getMinutes();
    var temp = `${hours} - ${min} `;
    console.log("function", temp);
  };

  const endDateOnly = (date) => {
    const d = new Date(date);
    const day = d.getDate();
    const month = d.getMonth();
    const year = d.getFullYear();
    var temp = `${day} - ${month} - ${year}`;
    console.log("function", temp);
  };
  const endTimeOnly = (time) => {
    const d = new Date(time);
    const hours = d.getHours();
    const min = d.getMinutes();
    var temp = `${hours} - ${min} `;
    console.log("function", temp);
  };
  const [continuous, setContinuous] = useState("");
  const [deadline, setDeadline] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [startTime, setStartTime] = useState();
  const [endTime, setEndTime] = useState();

  const handleChangeContinuous = (e) => {
    setContinuous(e.target.value);
  };

  let continuousComponent = (
    <>
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Task Start And End Date
        </Form.Label>
        {/* <Col className="form-input-align-center"> </Col> */}
        <Col md={12} lg={3}>
          <DatePicker
            placeholderText="Start Date"
            selected={startDate}
            dateFormat="dd-MM-yyyy"
            selectsStart
            startDate={startDate}
            endDate={endDate}
            onChange={(date) => {
              setStartDate(date);
              startDateOnly(date);
            }}
          />
        </Col>
        <Col md={12} lg={2}>
          <DatePicker
            placeholderText="Start Time"
            selected={startTime}
            onChange={(time) => {
              setStartTime(time);
              startTimeOnly(time);
            }}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption="Time"
            dateFormat="h:mm aa"
          />
        </Col>
        <Col md={12} lg={3}>
          <DatePicker
            placeholderText="End Date"
            selected={endDate}
            dateFormat="dd-MM-yyyy "
            selectsEnd
            startDate={startDate}
            endDate={endDate}
            minDate={startDate}
            onChange={(date) => {
              setEndDate(date);
              endDateOnly(date);
            }}
          />
        </Col>
        <Col md={12} lg={2}>
          <DatePicker
            placeholderText="End Time"
            selected={endTime}
            onChange={(time) => {
              setEndTime(time);
              endTimeOnly(time);
            }}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption="Time"
            dateFormat="h:mm aa"
          />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Task Application Deadline
        </Form.Label>
        <Col style={{ display: "flex" }}>
          <DatePicker
            placeholder=""
            selected={deadline}
            dateFormat="dd-MM-yyyy"
            selectsStart
            startDate={deadline}
            onChange={(e) => {
              setDeadline(e);
            }}
          />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
    </>
  );
  const [milestone, setMilestone] = useState("");

  const [milestones, setMilestones] = useState([
    { milestone_date: "", milestone_name: "", task_type: 1 },
  ]);

  const handleMileStone = (e) => {
    setMilestone(e.target.value);
  };
  const handleMilestoneName = (e, idx) => {
    const values = [...milestones];
    if (e.target.name === `milestone_name[${idx}]`) {
      values[idx].milestone_name = e.target.value;
    } else if (e.target.name === `milestone_date[${idx}]`) {
      values[idx].milestone_date = e;
    }
    setMilestones(values);
  };
  const handleMilestoneDate = (date, name, index) => {
    const values = [...milestones];
    if (name === `milestone_date[${index}]`) {
      values[index].milestone_date = date;
    }
    setMilestones(values);
  };
  const addMilestone = () => {
    setMilestones([...milestones, { milestone_date: "", milestone_name: "" }]);
  };
  const deleteMilestone = (index) => {
    console.log("idx", index);
    const temp = [...milestones];
    temp.splice(index, 1);
    setMilestones(temp);
  };
  let milestoneComponent;
  milestoneComponent = (
    <>
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Milestones
        </Form.Label>
        <Col xs={10}>
          {milestones.map((milestone, index) => (
            <Form.Row style={{ marginBottom: "10px" }}>
              <Col xs={12} md={12} lg={5}>
                <DatePicker
                  name={`milestone_date[${index}]`}
                  placeholderText="Select Date"
                  selected={milestone.milestone_date}
                  dateFormat="dd-MM-yyyy"
                  selectsStart
                  startDate={milestone.milestone_date}
                  onChange={(date, e) => {
                    handleMilestoneDate(date, `milestone_date[${index}]`, index);
                  }}
                />
              </Col>
              <Col xs={12} md={12} lg={5}>
                {" "}
                <Form.Control
                  type="text"
                  name={`milestone_name[${index}]`}
                  size="lg"
                  className="green-focus"
                  value={milestone.milestone_name}
                  onChange={(e) => handleMilestoneName(e, index)}
                />
              </Col>
              <Col xs={12} md={12} lg={2}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    width: "40px",
                  }}
                >
                  <button
                    type="button"
                    style={{ border: "0", background: "transparent" }}
                    onClick={(e) => addMilestone(e, index)}
                  >
                    <i class="fas fa-plus-square" style={{ color: "#4CAF50" }}></i>
                  </button>
                  <button
                    type="button"
                    style={{ border: "0", background: "transparent" }}
                    onClick={() => deleteMilestone(index)}
                  >
                    <i class="fas fa-minus-square" style={{ color: "#4CAF50" }}></i>
                  </button>
                </div>
              </Col>
            </Form.Row>
          ))}
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
    </>
  );
  const [slugAvail, setSlugAvail] = useState(true);
  const getSlugAvailability = () => {
    axios
      .get(
        `/slugMaster/isSlugAvailable/${values.task_name ? values.task_name.replaceAll(" ", "-").toLowerCase() : null
        }`
      )
      .then((res) => setSlugAvail(res))
      .catch((err) => console.log("err", err));
    // console.log(isAvailable);
  };
  if (slugAvail === false) {
    errors.task_slug = "this name is already taken";
  }
  return (
    <>
      <Modal show={show} onHide={handleClose} animation={false} centered>
        {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
        <div className="modal-header-custom">
          <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
            Select Skills
          </h4>
          <Button variant="secondary" style={{ height: "30px" }} onClick={handleClose}>
            <i class="fas fa-times" style={{ paddingRight: "0", color: "white" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ borderBottom: "1px solid #cccccc" }}>
            <Tabs id="controlled-tab-example" activeKey={key} onSelect={(k) => setKey(k)}>
              <Tab eventKey="home" title="Professional" style={{ opacity: "100" }}>
                <ProfessionalTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
              <Tab eventKey="profile" title="Creative" style={{ opacity: "100" }}>
                <CreativeTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
              <Tab eventKey="contact" title="Social" style={{ opacity: "100" }}>
                <SocialTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
            </Tabs>
          </div>
          <div style={{ marginTop: "50px" }}>
            <p>Maximum skills allowed: 5. ({skill.length} Of 5 Skills selected)</p>
            <p>Select as many skills relevant, for better result</p>
            <p>Selected Skills -</p>
            {skill.map((ele) => (
              <div style={{ display: "flex", flexDirection: "column", width: "fit-content" }}>
                <p className="skill-tags">{ele}</p>
              </div>
            ))}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Select Skills" onClick={selectSkill} />
        </Modal.Footer>
      </Modal>
      <div className="container">
        {/* title part */}
        <div>
          <h2 className="font-bold">Create Task</h2>
          <p>
            You are creating this task as{" "}
            <sup>
              <i class="fas fa-quote-left"></i>
            </sup>{" "}
            Social Work{" "}
            <sup>
              <i class="fas fa-quote-right"></i>
            </sup>
          </p>
          <p>
            Tasks are an avenue to collaborate with as many volunteers as possible. You can
            discover, engage and accredit volunteers on task.
          </p>
          <p>Please provide as detailed information as possible.</p>
        </div>
        {/*Form starts  */}
        <Card style={{ padding: "30px " }}>
          <Form onSubmit={handleSubmit}>
            <Form.Group>
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Task Name
                </Form.Label>
                <Col>
                  <div>
                    {slugAvail ? null : (
                      <span className="text-danger">This name is already taken</span>
                    )}
                  </div>
                  <BInput
                    type="text"
                    name="task_name"
                    onChange={handleChange}
                    onKeyPress={getSlugAvailability()}
                    onBlur={handleBlur}
                    error={errors.task_name}
                    value={values.task_name}
                    touched={touched.task_name}
                    size="lg"
                  />
                  {/* <Form.Control size="lg" type="text" className="green-focus" /> */}
                  <span className="help-block m-b-none">
                    Task Url:{" "}
                    <i style={{ color: "#737373", fontSize: "13px" }}>
                      http://projectheena.in/task/
                      {values && values.task_name
                        ? values.task_name.replaceAll(" ", "-").toLowerCase()
                        : null}
                    </i>
                  </span>
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />

              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Task Duration
                </Form.Label>
                <Col md={12} lg={5} className="form-input-align-center">
                  <InputGroup>
                    <BInput
                      type="number"
                      placeholder="Minimum"
                      name="task_duration_hours_min"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      // error={errors.task_duration_hours_min}
                      value={values.task_duration_hours_min}
                      // touched={touched.task_duration_hours_min}
                      size="lg"
                    />
                    <InputGroup.Append>
                      <InputGroup.Text id="basic-addon2" style={{ background: "transparent" }}>
                        Hours per volunteer
                      </InputGroup.Text>
                    </InputGroup.Append>
                  </InputGroup>
                  {touched.task_duration_hours_min && errors.task_duration_hours_min && (
                    <div>
                      <span className="text-danger">{errors.task_duration_hours_min}</span>
                    </div>
                  )}
                </Col>
                <Col className="form-input-align-center">
                  <InputGroup>
                    <BInput
                      type="number"
                      placeholder="Maximum"
                      name="task_duration_hours"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      // error={errors.task_duration_hours}
                      value={values.task_duration_hours}
                      // touched={touched.task_duration_hours}
                      size="lg"
                    />
                    <InputGroup.Append>
                      <InputGroup.Text id="basic-addon2" style={{ background: "transparent" }}>
                        Hours per volunteer
                      </InputGroup.Text>
                    </InputGroup.Append>
                  </InputGroup>
                  {touched.task_duration_hours && errors.task_duration_hours && (
                    <div>
                      <span className="text-danger">{errors.task_duration_hours}</span>
                    </div>
                  )}
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />

              <Form.Row>
                <Form.Label
                  column="lg"
                  lg={2}
                  className="form-label-custom p-r-15"
                  style={{ padingTop: "10px" }}
                >
                  Location
                </Form.Label>
                <Col style={{ display: "flex" }}>
                  <RadioGroup aria-label="gender">
                    <div>
                      <FormControlLabel
                        // value="work from home"
                        value="Online"
                        name="task_location"
                        control={<Radio color="primary" />}
                        label="Work from Home"
                        onChange={(e) => {
                          handleChange(e);
                          handleChangeLocation(e);
                        }}
                      />
                      <FormControlLabel
                        value="Offline"
                        name="task_location"
                        // value="work from location"
                        control={<Radio color="primary" />}
                        label="Work from Location"
                        onChange={(e) => {
                          handleChange(e);
                          handleChangeLocation(e);
                        }}
                      />
                    </div>
                  </RadioGroup>
                  {touched.task_location && errors.task_location && (
                    <div>
                      <span className="text-danger">{errors.task_location}</span>
                    </div>
                  )}
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              {loc ? locationComponent : null}

              {/* <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Company to engage in task
                </Form.Label>
                <Col className="form-input-align-center">
                  <Form.Control size="lg" type="text" className="green-focus" />
                </Col>
              </Form.Row> */}
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom  p-r-15">
                  Cause Supported
                </Form.Label>
                <Col style={{ marginTop: "-10px" }}>
                  <RadiobuttonTable
                    name="cause_supported"
                    handleChange={(radio) => setRadioValue(radio)}
                    error={errors.cause_supported}
                    value={values.cause_supported}
                    touched={touched.cause_supported}
                    alreadyChecked={radioValue.split()}
                  />
                  {touched.cause_supported && errors.cause_supported && (
                    <div>
                      <span className="text-danger">{errors.cause_supported}</span>
                    </div>
                  )}
                </Col>
              </Form.Row>
              <hr className="form-hr" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Task Description
                </Form.Label>
                <Col>
                  <div id="task_description" />
                  <div style={{ maxWidth: "100%" }}>
                    <FroalaEditorComponent
                      tag="textarea"
                      config={config}
                      onModelChange={handleModelChange}
                    />
                  </div>
                  {/* <TextEditor /> */}
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label
                  column="lg"
                  lg={2}
                  className="form-label-custom p-r-15"
                  style={{ marginBottom: "0" }}
                >
                  Task Image
                </Form.Label>
                <Col className="form-input-align-center">
                  <Form.Group style={{ marginBottom: "0" }}>
                    {/* <FileUpload name="file" onFileSelect={handleFileSelect} /> */}
                    <input name="image" type="file" onChange={handleFileSelect} />
                  </Form.Group>
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Skills Required
                </Form.Label>
                <Col className="form-input-align-center">
                  <InputGroup>
                    <Form.Control
                      size="lg"
                      type="text"
                      onClick={handleShow}
                      id="skills"
                      className="green-focus"
                      value={skill}
                    />
                    <InputGroup.Append style={{ width: "45px" }}>
                      <Button
                        variant="secondary"
                        id=""
                        onClick={() => {
                          setSkill([]);
                        }}
                        style={{ width: "70px" }}
                      >
                        <i class="fas fa-times" style={{ color: "white" }}></i>
                      </Button>
                    </InputGroup.Append>
                  </InputGroup>
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Volunteers Required
                </Form.Label>
                <Col md={12} lg={5} className="form-input-align-center">
                  <InputGroup>
                    {/* <FormControl type="number" placeholder="Min" size="lg" className="green-focus" /> */}
                    <BInput
                      type="number"
                      placeholder="Min"
                      name="people_required_min"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.people_required_min}
                      size="lg"
                    />
                    <InputGroup.Append>
                      <InputGroup.Text id="basic-addon2" style={{ background: "transparent" }}>
                        Volunteer
                      </InputGroup.Text>
                    </InputGroup.Append>
                  </InputGroup>
                  {touched.people_required_min && errors.people_required_min && (
                    <div>
                      <span className="text-danger">{errors.people_required_min}</span>
                    </div>
                  )}
                </Col>
                <Col className="form-input-align-center">
                  <InputGroup>
                    {/* <FormControl type="number" placeholder="Max" size="lg" className="green-focus" /> */}
                    <BInput
                      type="number"
                      placeholder="Max"
                      name="people_required"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.people_required}
                      size="lg"
                    />
                    <InputGroup.Append>
                      <InputGroup.Text id="basic-addon2" style={{ background: "transparent" }}>
                        Volunteer
                      </InputGroup.Text>
                    </InputGroup.Append>
                  </InputGroup>
                  {touched.people_required && errors.people_required && (
                    <div>
                      <span className="text-danger">{errors.people_required}</span>
                    </div>
                  )}
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Is this a Continuous task
                </Form.Label>

                <Col className="form-input-align-center">
                  <RadioGroup
                    aria-label="gender"
                    name="continuous"
                  // value={location}
                  // onChange={(e) => handleChangeLocation(e)}
                  >
                    <div>
                      <FormControlLabel
                        value="Yes"
                        control={<Radio color="primary" />}
                        label="Yes"
                        onChange={(e) => handleChangeContinuous(e)}
                      />
                      <FormControlLabel
                        value="No"
                        control={<Radio color="primary" />}
                        label="No"
                        onChange={(e) => handleChangeContinuous(e)}
                      />
                    </div>
                  </RadioGroup>
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              {continuous === "No" ? continuousComponent : null}
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Is it Milestone based
                </Form.Label>
                <Col className="form-input-align-center">
                  <RadioGroup
                    aria-label="gender"
                    name="milestone"
                  // value={location}
                  // onChange={(e) => handleChangeLocation(e)}
                  >
                    <div>
                      <FormControlLabel
                        value="Yes"
                        control={<Radio color="primary" />}
                        label="Yes"
                        onChange={(e) => handleMileStone(e)}
                      />
                      <FormControlLabel
                        value="No"
                        control={<Radio color="primary" />}
                        label="No"
                        onChange={(e) => handleMileStone(e)}
                      />
                    </div>
                  </RadioGroup>
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              {milestone === "Yes" ? milestoneComponent : null}
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Spoc Name
                </Form.Label>
                <Col>
                  <BInput
                    type="text"
                    name="task_contact_details"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.task_contact_details}
                    value={values.task_contact_details}
                    touched={touched.task_contact_details}
                    size="lg"
                  />
                  {/* <Form.Control size="lg" type="text" className="green-focus" /> */}
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Spoc Mobile
                </Form.Label>
                <Col>
                  <BInput
                    type="text"
                    name="spoc_mobile"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.spoc_mobile}
                    error={errors.spoc_mobile}
                    touched={touched.spoc_mobile}
                    size="lg"
                  />
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Spoc Email
                </Form.Label>
                <Col>
                  <BInput
                    type="text"
                    name="spoc_email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.spoc_email}
                    error={errors.spoc_email}
                    touched={touched.spoc_email}
                    size="lg"
                  />
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  Comma Seperated Tags
                </Form.Label>
                <Col>
                  {/* <TagsInput tags={[]} /> */}
                  <TagAutocomplete
                    tags={(tag) => {
                      var dataArr = tag.map((item) => {
                        return [item.name, item];
                      });
                      var maparr = new Map(dataArr);
                      var result = [...maparr.values()];
                      setTag(result);
                    }}
                    tag={tag}
                  />
                  <Form.Text className="text-muted" muted>
                    Maximum 3 tags allowed
                  </Form.Text>
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                  External URL
                </Form.Label>
                <Col>
                  <BInput
                    type="text"
                    name="vision_page"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.vision_page}
                    error={errors.vision_page}
                    touched={touched.vision_page}
                    size="lg"
                  />
                </Col>
              </Form.Row>
              <hr className="form-horizontal-line" />
              <Form.Row>
                <Form.Label column="lg" lg={2} className="form-labels"></Form.Label>
                <Col>
                  <CustomButton type="submit" content="Create Task" />
                </Col>
              </Form.Row>
            </Form.Group>
          </Form>
        </Card>
      </div>
    </>
  );
};

const mapStateToProps = function (state) {
  return {
    user: state.phAuth.user,
  };
};

export default connect(mapStateToProps)(CreateTask);
