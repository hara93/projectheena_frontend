import React from "react";
import "../tnc.css";
import "../../index.scss";
import * as ReactBootstrap from "react-bootstrap";
import { Card, Row, Col } from "react-bootstrap";
import { Label } from "react-bootstrap";
// import {
//   useContext,
//   ContextAwareToggle,
//   useAccordionToggle,
//   useAccordionEventKey,
// } from "react";

// import {
//   FaFacebook,
//   FaTwitterSquare,
//   FaGooglePlus,
//   FaYoutube,
//   FaThumbsUp,
//   FaTwitter,
// } from "react-icons/fa";

class faq extends React.Component {
  render() {
    return (
      <>
        {/* <img
          style={{
            width: "130%",
            marginLeft: "-165px",
            marginTop: "-25px",
            backgroundSize: "103%",
            backgroundAttachment: "fixed",
            backgroundPosition: "center left",
            height: "100%",
            maxHeight: "400px",
            backgroundRepeat: "no-repeat",
          }}
          src="/media/logos/superhero-kid.jpeg"
          alt=""
        /> */}
        <div className="banner">
          <div className="banner-img">
            <div id="parallex-bg" style={{ backgroundImage: "url('http://projectheena.in/assets/img/static/superhero-kid.jpg') " }}></div>
          </div>
        </div>
        <div className="container">
          <Card style={{ margin: "0 auto", padding: "0 15px" }}>
            <div style={{ marginTop: "30px", textAlign: "center" }}>
              <h3 className="static-title">FAQs</h3>
            </div>

            <p>
              We believe Social media channels just rock! However we are cool
              which ever way you would like to connect. Just a small headsup in
              case if you feel there is an unwanted error bugging you or feature
              you want to request; use the on page Feedback buttons to intimate
              us. Else you can use the following channels.
            </p>

            <Row
              style={{
                display: "flex",
                justifyContent: "center",
                justifyItems: "stretch",
                flexGrow: "1",
                padding: "0 15px",
                marginBottom: "20px",
              }}
            >
              <div style={{ flexGrow: "1" }}>
                <a href="#1">
                  <button className="csr-btn"> Heena FAQ</button>
                </a>
              </div>

              <div style={{ flexGrow: "1" }}>
                <a href="#2">
                  <button className="csr-btn"> NGO FAQ</button>
                </a>
              </div>
              <div style={{ flexGrow: "1" }}>
                <a href="#3">
                  <button className="csr-btn"> User FAQ</button>
                </a>
              </div>
              <div style={{ flexGrow: "1" }}>
                <a href="#4">
                  <button className="csr-btn">Other</button>
                </a>
              </div>
            </Row>
            <div>
              <div>
                <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
                  Heena's FAQ
                </h3>
              </div>

              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ border: "0" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="0"
                      className="faq-qns"
                    >
                      What is ProjectHeena?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="0">
                    <ReactBootstrap.Card.Body
                      style={{
                        backgroundColor: "#f3f3f4",
                        border: "1px solid #f3f3f4",
                        width: "100%",
                      }}
                    >
                      ProjectHeena provides a platform where you can donate your
                      skills and time for the cause you want to support.
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="1"
                      className="faq-qns"
                    >
                      How can I get in touch with ProjectHeena?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="1">
                    <ReactBootstrap.Card.Body
                      style={{
                        backgroundColor: "#f3f3f4",
                        border: "1px solid #f3f3f4",
                        width: "100%",
                      }}
                    >
                      There is a icon on right side of screen where u can have a
                      chat with us. Most of the times, we will be online, But
                      incase if we aren't then you can click the Contact Form and
                      we will get back to you soon :)
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="0"
                      className="faq-qns"
                    >
                      Who is behind ProjectHeena?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="0">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      We are a small team working hard to provide you a great
                      platform for best utilizing your skills & time by helping
                      someone who needs it.
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="1"
                      className="faq-qns"
                      id="2"
                    >
                      Interested in Volunteering ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="1">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      If you are interested in helping someone by your skills or
                      time then you can search for tasks with the skills you got,
                      the time you are comfortable & if it interests you, then hit
                      the 'Offer Help' button to get in touch with the task
                      creator
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
            </div>

            <hr className="form-horizontal-line"></hr>

            <div>
              <div>
                <h3 style={{ fontSize: "16px", fontWeight: "600" }}>NGO's FAQ</h3>
              </div>

              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="0"
                      className="faq-qns"
                    >
                      How can we seek help on ProjectHeena ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="0">
                    <ReactBootstrap.Card.Body
                      style={{
                        backgroundColor: "#f3f3f4",
                        border: "1px solid #f3f3f4",
                        width: "100%",
                      }}
                    >
                      ProjectHeena provides a platform where you can donate your
                      skills and time for the cause you want to support.
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="1"
                      className="faq-qns"
                    >
                      Do we need to pay for the help we receive on ProjectHeena ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="1">
                    <ReactBootstrap.Card.Body
                      style={{
                        backgroundColor: "#f3f3f4",
                        border: "1px solid #f3f3f4",
                        width: "100%",
                      }}
                    >
                      There is a icon on right side of screen where u can have a
                      chat with us. Most of the times, we will be online, But
                      incase if we aren't then you can click the Contact Form and
                      we will get back to you soon :)
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>

              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="0"
                      className="faq-qns"
                    >
                      What is the maximum number of volunteers that we can get ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="0">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      We are a small team working hard to provide you a great
                      platform for best utilizing your skills & time by helping
                      someone who needs it.
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="1"
                      className="faq-qns"
                    >
                      How many volunteers will be provided by the ProjectHeena ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="1">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      If you are interested in helping someone by your skills or
                      time then you can search for tasks with the skills you got,
                      the time you are comfortable & if it interests you, then hit
                      the 'Offer Help' button to get in touch with the task
                      creator
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>

              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="0"
                      className="faq-qns"
                    >
                      What if the volunteers do not turn up ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="0">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      We are a small team working hard to provide you a great
                      platform for best utilizing your skills & time by helping
                      someone who needs it.
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="1"
                      className="faq-qns"
                    >
                      For how many tasks can we create ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="1">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      If you are interested in helping someone by your skills or
                      time then you can search for tasks with the skills you got,
                      the time you are comfortable & if it interests you, then hit
                      the 'Offer Help' button to get in touch with the task
                      creator
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>

              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="0"
                      className="faq-qns"
                    >
                      Do we need to pay any money for registration ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="0">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      We are a small team working hard to provide you a great
                      platform for best utilizing your skills & time by helping
                      someone who needs it.
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="1"
                      className="faq-qns"
                      id="3"
                    >
                      Are the volunteers verified ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="1">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      If you are interested in helping someone by your skills or
                      time then you can search for tasks with the skills you got,
                      the time you are comfortable & if it interests you, then hit
                      the 'Offer Help' button to get in touch with the task
                      creator
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
            </div>

            <hr className="form-horizontal-line"></hr>
            <div>
              <div>
                <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
                  User's FAQ
                </h3>
              </div>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="0"
                      className="faq-qns"
                    >
                      Do I get paid for helping someone ?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="0">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      We are a small team working hard to provide you a great
                      platform for best utilizing your skills & time by helping
                      someone who needs it.
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="1"
                      className="faq-qns"
                    >
                      What causes are available online?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="1">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      If you are interested in helping someone by your skills or
                      time then you can search for tasks with the skills you got,
                      the time you are comfortable & if it interests you, then hit
                      the 'Offer Help' button to get in touch with the task
                      creator
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>

              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="0"
                      className="faq-qns"
                    >
                      I want to support 'x' cause which is not listed now what?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="0">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      We are a small team working hard to provide you a great
                      platform for best utilizing your skills & time by helping
                      someone who needs it.
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="1"
                      className="faq-qns"
                      id="4"
                    >
                      I have offered my help to a particular task Now what?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="1">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      If you are interested in helping someone by your skills or
                      time then you can search for tasks with the skills you got,
                      the time you are comfortable & if it interests you, then hit
                      the 'Offer Help' button to get in touch with the task
                      creator
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
            </div>

            <hr className="form-horizontal-line"></hr>

            <div>
              <div>
                <h3 style={{ fontSize: "16px", fontWeight: "600" }}>Other FAQ</h3>
              </div>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="0"
                      className="faq-qns"
                    >
                      On Accepting help, Ask volunteer whether he/she is 100%
                      available
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="0">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      We are a small team working hard to provide you a great
                      platform for best utilizing your skills & time by helping
                      someone who needs it.
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
              <ReactBootstrap.Accordion>
                <ReactBootstrap.Card
                  style={{ borderColor: "transparent" }}
                  className="p-3 border-none"
                >
                  <ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Toggle
                      as={ReactBootstrap.FormLabel}
                      variant="link"
                      eventKey="1"
                      className="faq-qns"
                    >
                      Feature - Once applied shud I be able to roll back?
                    </ReactBootstrap.Accordion.Toggle>
                  </ReactBootstrap.Card.Header>
                  <ReactBootstrap.Accordion.Collapse eventKey="1">
                    <ReactBootstrap.Card.Body className="faq_ans">
                      If you are interested in helping someone by your skills or
                      time then you can search for tasks with the skills you got,
                      the time you are comfortable & if it interests you, then hit
                      the 'Offer Help' button to get in touch with the task
                      creator
                    </ReactBootstrap.Card.Body>
                  </ReactBootstrap.Accordion.Collapse>
                </ReactBootstrap.Card>
              </ReactBootstrap.Accordion>
            </div>
          </Card>
        </div>


      </>
    );
  }
}

export default faq;
