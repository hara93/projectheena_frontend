import React from "react";
import {
  Form,
  Card,
  Container,
  Row,
  Col,
  OverlayTrigger,
  Tooltip,
  Modal,
  Button,
} from "react-bootstrap";
import Question from "../components/Question";

const SurveyGeoTag = () => {
  return (
    <Card className="p-5" style={{ color: "#9a9ba5" }}>
      <Question />

      <Form className="p-3">
        <Row>
          <Col md={6}>
            <p>Image Accuracy</p>
            <div style={{ display: "block" }}>
              <input type="radio"></input> <label>Low</label>
            </div>
            <div style={{ display: "block", marginLeft: "-5%" }}>
              <input type="radio" style={{ marginLeft: "5%" }}></input>{" "}
              <label>Medium</label>
            </div>
            <div style={{ display: "block", marginLeft: "-5%" }}>
              <input type="radio" style={{ marginLeft: "5%" }}></input>{" "}
              <label>High</label>
            </div>
          </Col>
          <Col md={6}>
            <p>Location Accuracy</p>
            <div style={{ display: "block" }}>
              <input type="radio"></input> <label>Low</label>
              <OverlayTrigger
                placement="right"
                delay={{ show: 100, hide: 100 }}
                overlay={
                  <Tooltip>
                    A question keyword is an alias or second name for your
                    questions. It makes your data reports simpler to understand
                    and more analysis-friendly.
                  </Tooltip>
                }
              >
                <i
                  class="fas fa-info-circle"
                  style={{ marginLeft: "10px" }}
                ></i>
              </OverlayTrigger>
            </div>
            <div style={{ display: "block", marginLeft: "-5%" }}>
              <input type="radio" style={{ marginLeft: "5%" }}></input>{" "}
              <label>Medium</label>
              <OverlayTrigger
                placement="right"
                delay={{ show: 100, hide: 100 }}
                overlay={
                  <Tooltip>
                    A question keyword is an alias or second name for your
                    questions. It makes your data reports simpler to understand
                    and more analysis-friendly.
                  </Tooltip>
                }
              >
                <i
                  class="fas fa-info-circle"
                  style={{ marginLeft: "10px" }}
                ></i>
              </OverlayTrigger>
            </div>
            <div style={{ display: "block", marginLeft: "-5%" }}>
              <input type="radio" style={{ marginLeft: "5%" }}></input>{" "}
              <label>High</label>
              <OverlayTrigger
                placement="right"
                delay={{ show: 100, hide: 100 }}
                overlay={
                  <Tooltip>
                    A question keyword is an alias or second name for your
                    questions. It makes your data reports simpler to understand
                    and more analysis-friendly.
                  </Tooltip>
                }
              >
                <i
                  class="fas fa-info-circle"
                  style={{ marginLeft: "10px" }}
                ></i>
              </OverlayTrigger>
            </div>
          </Col>
        </Row>

        <Form.Label style={{ marginTop: "3%", color: "#9a9ba5" }}>
          Settings
        </Form.Label>
        <div style={{ color: "#9a9ba5" }}>
          <input type="checkbox"></input>
          <label style={{ marginLeft: "2%" }}>
            Allow user to select an image from gallery
          </label>
        </div>

        <hr></hr>
        <i class="fas fa-trash"> Delete</i>
        <i class="fas fa-compress-alt" style={{ marginLeft: "2%" }}>
          Move
        </i>
        <i class="fas fa-copy" style={{ marginLeft: "2%" }}>
          {" "}
          Copy
        </i>
      </Form>
    </Card>
  );
};

export default SurveyGeoTag;
