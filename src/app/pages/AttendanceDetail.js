/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import { Table, Button, Col } from "react-bootstrap";
import { Images } from "../config/Images";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import DatePicker from "react-datepicker";
import axios from "../config/axios";
import moment from "moment";
import { useSelector } from "react-redux";
const AttendanceDetail = (props) => {
  const user = useSelector((state) => state.phAuth.user);
  const task_id = localStorage.getItem("task_data_id");

  const data = [
    {
      user_profile: Images.userlogo,
      name: "Nikhil Kataria",
      date: "15th aug",
      hours: 5,
    },
    {
      user_profile: Images.userlogo,
      name: "Nikhil Kataria",
      date: "15th aug",
      hours: 5,
    },
    {
      user_profile: Images.userlogo,
      name: "Nikhil Kataria",
      date: "15th aug",
      hours: 5,
    },
    {
      user_profile: Images.userlogo,
      name: "Nikhil Kataria",
      date: "15th aug",
      hours: 5,
    },
  ];
  const [pendingAttendance, setPendingAttendance] = useState([]);
  const [approvedAttendance, setApprovedAttendance] = useState([]);
  console.log("approved", approvedAttendance);
  const getPendingAttendanceList = () => {
    axios
      .post("/taskTransaction/pendingAttendanceList", { taskId: task_id })
      .then((res) => {
        console.log("res", res);
        setPendingAttendance(res.data);
      })
      .catch((err) => console.log("err", err));
  };
  const getApprovedAttendanceList = () => {
    axios
      .post("/taskTransaction/approvedAttendanceList", { taskId: task_id })
      .then((res) => {
        console.log("res", res);
        setApprovedAttendance(res.data);
      })
      .catch((err) => console.log("err", err));
  };
  const approveTaskAttendance = (attendance_id) => {
    axios
      .post("/taskTransaction/approveAttendance", { taskId: task_id, attendanceId: attendance_id })
      .then((res) => {
        setPendingAttendance(res.pending);
        setApprovedAttendance(res.approved);
      })
      .catch((err) => console.log("err", err));
  };
  const deleteAttendance = (attendance_id) => {
    var proceed = window.confirm("Do you really want to disapprove the attendance?")
    if (proceed) {
      axios
        .post("/taskTransaction/deleteTaskAttendance", {
          taskId: task_id,
          attendanceId: attendance_id,
        })
        .then((res) => {
          setPendingAttendance(res.pending);
          setApprovedAttendance(res.approved);
        })
        .catch((err) => console.log("err", err));
    }
  };
  const updateTaskAttendance = () => {
    // var { taskId, attendanceId, hours, attendance_date } = req.body;
    axios
      .post("/taskTransaction/updateTaskAttendance", {
        taskId: task_id,
        attendanceId: attendance_id,
        hours: hours,
        attendance_date: startDate,
      })
      .then((res) => {
        setPendingAttendance(res.pending);
        setApprovedAttendance(res.approved);
      })
      .catch((err) => console.log("update attendance error ", err));
    setStartDate();
    setHours();
    setShow(false);
    setAttendance_id();
  };
  const [show, setShow] = useState(false);
  const handleClose = () => {
    setShow(false);
    setStartDate();
    setHours();
    setAttendance_id();
  };
  const handleShow = (idx, ele) => {
    setShow(idx);
    setStartDate(new Date(ele.attendance_date));
    setHours(ele.hours);
    setAttendance_id(ele._id);
  };
  const [attendance_id, setAttendance_id] = useState();
  const [startDate, setStartDate] = useState();
  const [hours, setHours] = useState();

  const [attendanceModal, setAttendanceModal] = useState(false);
  const [submitDate, setSubmitDate] = useState();
  const [submitHours, setSubmitHours] = useState();
  // console.log(submitDate, submitHours);
  const closeAttendanceModal = () => {
    setAttendanceModal(false);
    setSubmitHours();
    setSubmitDate();
  };
  const submitAttendance = () => {
    var data = {};
    data.user_id = user._id;
    data.hours = submitHours;
    data.attendance_date = submitDate;
    data.user_type = 0;
    data.task_id = task_id;
    console.log("values", data);
    axios
      .post("/taskTransaction/insertTaskAttendance", data)
      .then((res) => {
        console.log("res", res);
        setPendingAttendance(res.pending);
        // setApprovedAttendance(res.approved);
      })
      .catch((err) => console.log("err", err));
    setAttendanceModal(false);
    setSubmitHours();
    setSubmitDate();
  };
  useEffect(() => {
    getPendingAttendanceList();
    getApprovedAttendanceList();
  }, []);
  return (
    <>
      <div>
        {props.status.status === 5 || props.status.status === 2 ? (
          <>
            <div>
              <div style={{ marginBottom: "30px" }}>
                <h3 className="font-bold" style={{ fontSize: "20px" }}>
                  Attendance Details
                </h3>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "70px",
                marginBottom: "30px",
              }}
            >
              <button className="ph-btn" onClick={() => setAttendanceModal(true)}>
                Submit attendance
              </button>
            </div>
            <div>
              {pendingAttendance.length > 0 ? (
                <div>
                  <Table striped size="md" bordered style={{ color: "gray" }}>
                    <thead style={{ backgroundColor: "#0000000d" }}>
                      <tr>
                        <th>Sr.No</th>
                        <th>Attendance Date</th>
                        <th>Hours Contributed</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      {pendingAttendance.map((ele, idx) => (
                        <>
                          <tr>
                            <td>{idx + 1}</td>
                            <td>{moment(ele.attendance_date).format("Do MMM ")}</td>
                            <td>{ele.hours}</td>
                            <td>
                              <span>Approval Pending</span>
                            </td>
                          </tr>
                        </>
                      ))}
                    </tbody>
                  </Table>
                </div>
              ) : null}
            </div>
            <Modal show={attendanceModal} onHide={closeAttendanceModal} animation={false} centered>
              {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
              <div className="modal-header-custom">
                <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                  Submit Task Attendance
                </h4>
                <Button
                  variant="secondary"
                  style={{ height: "30px" }}
                  onClick={closeAttendanceModal}
                >
                  <i class="fas fa-times" style={{ paddingRight: "0", color: "white" }}></i>
                </Button>
              </div>
              <Modal.Body>
                <div style={{ padding: "20px 10px" }}>
                  <Form>
                    <Form.Group>
                      <Form.Row>
                        <Form.Label column="lg" lg={2} className="form-label-custom">
                          Attendance Date
                        </Form.Label>
                        <Col>
                          {" "}
                          <DatePicker
                            placeholderText="Start Date"
                            selected={submitDate}
                            dateFormat="dd-MM-yyyy"
                            selectsStart
                            startDate={new Date()}
                            endDate={new Date()}
                            onChange={(date) => {
                              setSubmitDate(date);
                            }}
                          />
                        </Col>
                      </Form.Row>
                    </Form.Group>
                    <Form.Group>
                      <Form.Row>
                        <Form.Label column="lg" lg={2} className="form-label-custom ">
                          Attendance Hours
                        </Form.Label>
                        <Col>
                          {" "}
                          <Form.Control
                            size="lg"
                            type="number"
                            name="project_contact"
                            onChange={(e) => setSubmitHours(e.target.value)}
                            value={submitHours}
                            className="green-focus"
                          />
                        </Col>
                      </Form.Row>
                    </Form.Group>
                  </Form>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={closeAttendanceModal}>
                  Close
                </Button>
                <button className="ph-btn" onClick={() => submitAttendance()}>
                  Submit Attendance
                </button>
                {/* <CustomButton content="Select Skills" onClick={selectSkill} /> */}
              </Modal.Footer>
            </Modal>
          </>
        ) : (
          <>
            <div>
              <h3 className="font-bold" style={{ fontSize: "20px" }}>
                Attendance Details
              </h3>
            </div>
            {pendingAttendance.length > 0 ? (
              <div>
                <Table striped size="md" bordered style={{ color: "gray", marginBottom: "30px" }}>
                  <thead style={{ backgroundColor: "#0000000d" }}>
                    <tr>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>Sr.No</th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>UserProfile</th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>Name</th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>
                        Attendance Date
                      </th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>Hours</th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {pendingAttendance.map((ele, idx) => (
                      <>
                        <tr>
                          <td>{idx + 1}</td>
                          <td>
                            <img
                              src={Images.userlogo}
                              alt="logo"
                              className="img-task-attendance"
                            ></img>
                          </td>
                          <td>{`${ele.first_name} ${ele.last_name}`}</td>
                          <td>{moment(ele.attendance_date).format("Do MMM ")}</td>
                          <td>{ele.hours}</td>
                          <td>
                            <div style={{ display: "flex" }}>
                              <i
                                class="fas fa-check attendance-action pointer"
                                onClick={() => approveTaskAttendance(ele._id)}
                              ></i>
                              <i
                                class="fas fa-times attendance-action pointer"
                                onClick={() => deleteAttendance(ele._id)}
                              ></i>
                              <i
                                class="fas fa-pencil-alt attendance-action pointer"
                                onClick={() => handleShow(idx, ele)}
                              ></i>
                            </div>
                          </td>
                        </tr>
                        <Modal show={show === idx} onHide={handleClose} animation={false} centered>
                          {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
                          <div className="modal-header-custom">
                            <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                              Feel free to contact Us
                            </h4>
                            <Button
                              variant="secondary"
                              style={{ height: "30px" }}
                              onClick={handleClose}
                            >
                              <i
                                class="fas fa-times"
                                style={{ paddingRight: "0", color: "white" }}
                              ></i>
                            </Button>
                          </div>
                          <Modal.Body>
                            <div style={{ padding: "20px 10px" }}>
                              <Form>
                                <Form.Group>
                                  <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom">
                                      Attendance Date
                                    </Form.Label>
                                    <Col>
                                      {" "}
                                      <DatePicker
                                        placeholderText="Start Date"
                                        selected={startDate}
                                        dateFormat="dd-MM-yyyy"
                                        selectsStart
                                        startDate={startDate}
                                        // endDate={endDate}
                                        onChange={(date) => {
                                          setStartDate(date);
                                        }}
                                      />
                                    </Col>
                                  </Form.Row>
                                </Form.Group>
                                <Form.Group>
                                  <Form.Row>
                                    <Form.Label column="lg" lg={2} className="form-label-custom ">
                                      Attendance Hours
                                    </Form.Label>
                                    <Col>
                                      {" "}
                                      <Form.Control
                                        size="lg"
                                        type="number"
                                        name="project_contact"
                                        onChange={(e) => setHours(e.target.value)}
                                        value={hours}
                                        className="green-focus"
                                      />
                                    </Col>
                                  </Form.Row>
                                </Form.Group>
                              </Form>
                            </div>
                          </Modal.Body>
                          <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                              Close
                            </Button>
                            <button className="ph-btn" onClick={() => updateTaskAttendance()}>
                              Edit Attendance
                            </button>
                            {/* <CustomButton content="Select Skills" onClick={selectSkill} /> */}
                          </Modal.Footer>
                        </Modal>
                      </>
                    ))}
                  </tbody>
                </Table>
              </div>
            ) : (
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "100px",
                }}
              >
                <span style={{ color: "#c4c4c4", fontSize: "2rem" }}>
                  No Task Attendance to approve or reject{" "}
                </span>
              </div>
            )}

            <div style={{ marginBottom: "30px" }}>
              <h3 className="font-bold" style={{ fontSize: "20px" }}>
                Previous Attendance
              </h3>
            </div>
            {approvedAttendance.length > 0 ? (
              <div>
                <Table striped size="md" bordered style={{ color: "gray" }}>
                  <thead style={{ backgroundColor: "#0000000d" }}>
                    <tr>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>Sr.No</th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>UserProfile</th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>Name</th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>
                        Attendance Date
                      </th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>Hours</th>
                      <th style={{ background: "#ddd", border: "1px solid #ccc" }}>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {approvedAttendance.map((ele, idx) => (
                      <tr>
                        <td>{idx + 1}</td>
                        <td>
                          <img
                            src={Images.userlogo}
                            alt="logo"
                            className="img-task-attendance"
                          ></img>
                        </td>
                        <td>{`${ele.first_name} ${ele.last_name}`}</td>
                        <td>{moment(ele.attendance_date).format("Do MMM ")}</td>
                        <td>{ele.hours}</td>
                        <td>Approved</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            ) : (
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "100px",
                }}
              >
                <span style={{ color: "#c4c4c4", fontSize: "2rem" }}>
                  No Task Attendance to approve or reject{" "}
                </span>
              </div>
            )}
          </>
        )}
        <div></div>
      </div>
    </>
  );
};
export default AttendanceDetail;
