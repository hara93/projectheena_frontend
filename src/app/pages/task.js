import React from 'react';
import '../task.css';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import ImageUploader from 'react-images-upload';

import { Row, Col } from 'react-bootstrap';
import * as ReactBootstrap from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';
import SimpleMap from './googlemaps';
import CustomButton from '../components/CustomButton';
import { subISOWeekYears } from 'date-fns';
import CustomButtonOutline from '../components/CustomButtonOutline'
import TaskGallery from './TaskGallery';
import AttendanceDetail from './AttendanceDetail';
import NewUpdatePost from '../components/new_update_post';
import UpdateTask from './UpdateTask';






class Task extends React.Component {
  constructor() {
    super();
    this.state = {
      taskOpen: true,
      details: true,
      updates: false,
      gallery: false,
      attendance: false,
      users: false,
      reply: false,
      star: false

    }
  }

  closeTask = () => {
    this.setState({ taskOpen: false })
  }
  handleDisplay = () => {
    this.setState({ details: true, updates: false, gallery: false, attendance: false })
  }
  handleGallery = () => {
    this.setState({ details: false, updates: false, gallery: true, attendance: false })
  }

  handleAttendance = () => {
    this.setState({ details: false, updates: false, gallery: false, attendance: true })
  }
  handleUpdate = () => {
    this.setState({ details: false, updates: true, gallery: false, attendance: false })
  }

  handleClick = () => {
    this.setState({ users: !this.state.users })
  }
  handleReply = () => {
    this.setState({ reply: !this.state.reply })
  }
  handleStar = () => {
    this.setState({ star: !this.state.star })
  }
  render() {
    return (
      <>
        {
          this.state.taskOpen == true ?
            <Card style={{ backgroundColor: '#dfefd8' }}>
              <Row>
                <Col md={10}>
                  <p style={{ paddingTop: '3vh', paddingLeft: '2vw', fontSize: '14px', fontWeight: '500', display: 'inline-block' }}>This task is currently open</p>
                </Col>
                <Col md={2}>
                  <a onClick={this.closeTask} style={{ paddingTop: '3vh', paddingLeft: '82%', fontSize: '14px', fontWeight: '500', display: 'inline-block', textAlign: 'right' }} >X</a>
                </Col>
              </Row>

            </Card> : null
        }

        {/* <div style={{backgroundColor:'#dff0d8',color:'#3c763d', height:'5%', width:'100%',margin:'0 0 0 0'}}>
                <p >This Task is currectly open</p>
               
            </div> */}

        <div className="container">
          <div style={{ margin: '25px 0', textAlign: 'center' }}>
            <h1 style={{
              fontFamily: 'sans-serif',
              wordSpacing: '7px', fontWeight: '500', fontSize: '30px', color: '#676a6c'
            }}>Clean your Place</h1>

            <h2>By <p style={{ color: '#4caf4f', display: 'inline-block', fontSize: '23px', fontWeight: '600' }}>Komal Kamble</p></h2>

          </div>

          <ul className="ph-nav-bar tabs" style={{ marginBottom: '-3%', width: '100%  ' }}>
            <li
              role="presentation"
              style={{
                display: "inline-block",
                marginRight: "3%",
                marginTop: "1vh",
              }}
            >
              <a
                href="#terms"
                className="active"
                role="tab"
                data-toggle="tab"
                onClick={this.handleDisplay}
                id="tnc"
                style={{ display: 'inline-block', color: '#a0a0a0' }}

              >

                {
                  this.state.details == true ?
                    <>
                      <i
                        className="fa fa-newspaper-o"
                        style={{ color: "black", fontSize: "15px", display: 'inline-block', marginLeft: '-17%', marginRight: '10%' }}

                      >

                      </i>
                      <div style={{ display: 'inline-block' }}>
                        <span style={{ color: 'black', fontWeight: '600' }}>DETAILS</span>
                      </div>

                    </>
                    :
                    <>
                      <i
                        className="fa fa-newspaper-o"
                        style={{ color: "#a0a0a0", fontSize: "15px", display: 'inline-block', marginLeft: '-17%', marginRight: '10%' }}

                      >

                      </i>
                      <span style={{ color: '#a0a0a0', fontWeight: '600' }}>DETAILS</span>
                    </>

                }


              </a>

            </li>

            <li
              role="presentation"
              style={{
                display: "inline-block",
                marginRight: "3%",
                marginTop: "1vh",
              }}
            >
              <a
                href="#terms"
                role="tab"
                data-toggle="tab"
                onClick={this.handleUpdate}
                id="privacy"
                style={{ display: 'inline-block', color: '#a0a0a0' }}
              >
                {
                  this.state.updates == true ?
                    <>
                      <i
                        className="fa fa-bullhorn"
                        style={{ color: "black", fontSize: "15px", display: 'inline-block', marginLeft: '-9%', marginRight: '5%' }}

                      >

                      </i>
                      <span style={{ color: 'black', fontWeight: '600' }}>UPDATES & UPLOADS</span>

                    </> :
                    <>
                      <i
                        className="fa fa-bullhorn"
                        style={{ color: "#a0a0a0", fontSize: "15px", display: 'inline-block', marginLeft: '-9%', marginRight: '5%' }}

                      >

                      </i>
                      <span style={{ color: "#a0a0a0", fontWeight: '600' }}>UPDATES & UPLOADS</span>

                    </>

                }

              </a>
            </li>
            <li
              role="presentation"
              style={{
                display: "inline-block",
                marginRight: "3%",
                marginTop: "1vh",
              }}
            >
              <a
                href="#terms"
                role="tab"
                data-toggle="tab"
                onClick={this.handleGallery}
                id="privacy"
                style={{ display: 'inline-block', color: '#a0a0a0' }}
              >
                {
                  this.state.gallery == true ?
                    <>
                      <i
                        className="fa fa-image"
                        style={{ color: "black", fontSize: "15px", display: 'inline-block', marginLeft: '-17%', marginRight: '10%' }}

                      >

                      </i>
                      <span style={{ color: 'black', fontWeight: '600' }}>GALLERY</span>
                    </>
                    :
                    <>
                      <i
                        className="fa fa-image"
                        style={{ color: "#a0a0a0", fontSize: "15px", display: 'inline-block', marginLeft: '-17%', marginRight: '10%' }}

                      >

                      </i>
                      <span style={{ color: '#a0a0a0', fontWeight: '600' }}>GALLERY</span>
                    </>
                }

              </a>

            </li>
            <li
              role="presentation"
              style={{
                display: "inline-block",
                marginRight: "3%",
                marginTop: "1vh",
              }}
            >
              <a
                href="#terms"
                role="tab"
                data-toggle="tab"
                onClick={this.handleAttendance}
                id="privacy"
                style={{ display: 'inline-block', color: '#a0a0a0' }}
              >
                {
                  this.state.attendance == true ?
                    <>
                      <i
                        className="fa fa-calendar"
                        style={{ color: "black", fontSize: "15px", display: 'inline-block', marginLeft: '-17%', marginRight: '10%' }}

                      >

                      </i>
                      <span style={{ color: 'black', fontWeight: '600' }}>ATTENDANCE</span>
                    </>
                    :
                    <>
                      <i
                        className="fa fa-calendar"
                        style={{ color: "#a0a0a0", fontSize: "15px", display: 'inline-block', marginLeft: '-17%', marginRight: '10%' }}

                      >

                      </i>
                      <span style={{ color: "#a0a0a0", fontWeight: '600' }}>ATTENDANCE</span>
                    </>
                }

              </a>

            </li>


          </ul>



          <Card >
            <Col md={8} >
              {this.state.details == true ?
                <>
                  <img style={{ width: '100%', marginTop: '2%' }} src="/media/logos/default.jpeg" />


                  <p style={{ color: '#676a6c', marginTop: '4vh', marginBottom: '2vh', marginLeft: '1%' }}>he 1500s, when an unknown printer took a galley of
                    type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Lethe 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only
                    five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s
                    with the release of Lethe 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was
                    popularised in the 1960s with the release of Lethe 1500s, when an unknown printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially
                    unchanged. It was popularised in the 1960s with the release of Lethe 1500s, when an unknown printer took a galley of type and
                    scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Let
                  </p>

                  <h4 style={{ fontSize: '2rem', marginTop: '4vh' }}>Share with your Friends</h4>
                  <FacebookIcon style={{ fontSize: 40, marginRight: '2vw', color: "#3c5997" }} />
                  <TwitterIcon style={{ fontSize: 40, marginRight: '2vw', color: '#04abed' }} />
                  <LinkedInIcon style={{ fontSize: 40, marginRight: '2vw', color: '#027bb6' }} />
                  <GroupAddIcon style={{ fontSize: 40, marginRight: '2vw', color: '#4caf4f' }} />


                </> : null}
              {
                this.state.updates == true ?
                  <>
                    <UpdateTask />


                  </> : null
              }
              {
                this.state.gallery == true ?
                  <>
                    <TaskGallery />

                  </> : null
              }
              {
                this.state.attendance == true ?
                  <>
                    <AttendanceDetail />
                  </> : null
              }
            </Col>
            <Col md={4}>

              <button className="p-2 position-sticky" style={{ width: '100%', marginTop: '4%', backgroundColor: '#4caf4f', border: '1px solid #4caf4f', color: 'white', fontWeight: '700', fontSize: '17px' }}> Offer Help</button>
              <h3 style={{ wordSpacing: '3px', padding: '2vh 0 1vh 1vw', marginTop: '5%' }}>Task Statistics</h3>
              <ReactBootstrap.Table >

                <tbody  >

                  <tr className="table_border" style={{ borderBottom: '1px solid gainsboro' }}>
                    <td style={{ width: '40%', fontSize: '13px', fontWeight: '600', color: '#676a6c' }}>Status</td>
                    <td ><span style={{ backgroundColor: '#4CAF50', color: 'white', padding: '0.5px', fontSize: '12px', fontWeight: '600' }}>Task is open</span></td>
                  </tr>

                  <tr className="table_border">
                    <td style={{ fontSize: '13px', fontWeight: '600', color: '#676a6c' }}>Duration Hours</td>
                    <td style={{ fontSize: '13px', fontWeight: '600', color: '#676a6c' }}>Min:4 &nbsp;|&nbsp; Max:33</td>
                  </tr>
                  <tr className="table_border">
                    <td style={{ fontSize: '13px', fontWeight: '600', color: '#676a6c' }}>Location</td>
                    <td style={{ fontSize: '13px', fontWeight: '600', color: '#676a6c' }}>Mumbai, Maharashtra, India.</td>
                  </tr>
                  <tr className="table_border">
                    <td style={{ fontSize: '13px', fontWeight: '600', color: '#676a6c' }}>People Required</td>
                    <td style={{ fontSize: '13px', fontWeight: '600', color: '#676a6c' }}>Min:1 &nbsp;|&nbsp; Max:40</td>
                  </tr>

                  <tr className="table_border">
                    <td style={{ fontSize: '13px', fontWeight: '600', color: '#676a6c' }}><p>Task Timeline</p>
                      <p style={{ fontWeight: '600', color: '#676a6c' }}>Flexible</p>
                    </td>
                    <td style={{ marginTop: '2%', fontSize: '13px', fontWeight: '600', color: '#676a6c' }}>This is continuous task, without any start or end date. You can join right away and start doing good.</td>
                  </tr>
                  <tr className="table_border" style={{ borderBottom: 'transparent' }}>
                    <td style={{ fontSize: '13px', fontWeight: '600', color: '#676a6c' }}><p>External Links</p>

                    </td>
                    <td style={{ fontSize: '13px', color: '#4caf4f', fontWeight: '600' }}>http://projectheena.com</td>
                  </tr>

                </tbody>
                <div style={{ float: 'right', marginLeft: '283%', marginRight: '-148%', marginTop: '11%', width: '50%' }} >
                  <p style={{ width: '100%' }}></p>




                </div>


              </ReactBootstrap.Table>


              <Row>
                <h3 style={{ wordSpacing: '3px', padding: '1vh 0 1vh 1vw', display: 'block' }}>Task Location</h3>




              </Row>
              <div style={{ height: '500px', width: '100%' }}>
                <SimpleMap />
              </div>

              <Row>
                <h3 style={{ wordSpacing: '3px', padding: '1vh 0 1vh 1vw' }}>Skills Needed</h3>
              </Row>
              <Row>
                <a className="p-2" style={{ backgroundColor: '#e7eaec', border: '1px solid #e7eaec', color: 'black', marginLeft: '3%', marginBottom: '3%', paddingRight: '5px', paddingLeft: '5px', fontSize: '12px' }} href="#" >Finance</a>

              </Row>

              <Row>
                <h3 style={{ wordSpacing: '3px', padding: '1vh 0 1vh 1vw' }}>Tags</h3>
              </Row>
              <Row>
                <p style={{ marginLeft: '1vw' }}>No Tags Supported</p>
              </Row>

              <Row>
                <Col xs={4}>
                  <img style={{ borderRadius: '50%', width: '100%', marginTop: '2vh' }} src="/media/logos/me.jpeg" />
                </Col>
                <Col xs={8}>
                  <h5 style={{ fontSize: '1.6rem', textAlign: 'left', marginTop: '3vh', marginLeft: '0px' }}>Komal Kamble</h5>
                  {/* <button style={{ backgroundColor: 'white', color: ' #4CAF50', border: '1px solid  #4CAF50', padding: '1vh 1vw 1vh 1vw' }}>View Creator's Profile</button> */}
                  <CustomButtonOutline content={`View Creator's Profile`} />
                </Col>

              </Row>


              <div>
                <h3 style={{ marginTop: "5vh", marginBottom: "2vh" }}>
                  Task Volunteers
                </h3>

                <div class="container1" style={{ display: 'inline-block' }}>
                  <img src={Images.user_logo} alt="Avatar" class="image1" style={{
                    borderRadius: "50%",
                    width: "70%",
                    marginLeft: '3%'
                  }} />
                  <div class="overlay1" style={{ display: 'inline-block' }}>
                    <div class="text1" style={{ display: 'inline-block' }}>Daksh Khatri</div>
                  </div>
                </div>

                <div class="container1" style={{ display: 'inline-block' }}>
                  <img src={Images.user_logo} alt="Avatar" class="image1" style={{
                    borderRadius: "50%",
                    width: "70%",
                    marginLeft: '3%'
                  }} />
                  <div class="overlay1" style={{ display: 'inline-block' }}>
                    <div class="text1" style={{ display: 'inline-block' }}>Reethika Renganathan </div>
                  </div>
                </div>

                <div class="container1" style={{ display: 'inline-block' }}>
                  <img src={Images.user_logo} alt="Avatar" class="image1" style={{
                    borderRadius: "50%",
                    width: "70%",
                    marginLeft: '3%'
                  }} />
                  <div class="overlay1" style={{ display: 'inline-block' }}>
                    <div class="text1" style={{ display: 'inline-block' }}>Tejas Thakare</div>
                  </div>
                </div>


              </div>
              {
                this.state.users == true ?
                  <>
                    <div style={{ marginTop: '3%' }}>

                      <div class="container1" style={{ display: 'inline-block' }}>
                        <img src={Images.user_logo} alt="Avatar" class="image1" style={{
                          borderRadius: "50%",
                          width: "70%",
                          marginLeft: '3%'
                        }} />
                        <div class="overlay1" style={{ display: 'inline-block' }}>
                          <div class="text1" style={{ display: 'inline-block' }}>Daksh Khatri</div>
                        </div>
                      </div>

                      <div class="container1" style={{ display: 'inline-block' }}>
                        <img src={Images.user_logo} alt="Avatar" class="image1" style={{
                          borderRadius: "50%",
                          width: "70%",
                          marginLeft: '3%'
                        }} />
                        <div class="overlay1" style={{ display: 'inline-block' }}>
                          <div class="text1" style={{ display: 'inline-block' }}>Reethika Renganathan </div>
                        </div>
                      </div>
                    </div>

                    <div style={{ marginTop: '3%', marginBottom: '10%' }}>
                      <a onClick={this.handleClick} style={{ paddingLeft: '29%', color: '#4caf4f' }}><i class="fas fa-arrow-up" style={{ color: '#4caf4f' }}></i> Show less </a>
                    </div>
                  </> : <div style={{ marginTop: '3%', marginBottom: '10%' }}>
                    <a onClick={this.handleClick} style={{ paddingLeft: '29%', color: '#4caf4f' }}><i class="fas fa-arrow-down" style={{ color: '#4caf4f' }}></i> Show more </a>
                  </div>
              }











            </Col>
          </Card>

        </div>






      </>
    )
  }
}

export default Task;