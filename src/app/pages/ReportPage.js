/* eslint-disable no-restricted-imports */
import React from "react";
import { Card, Table } from "react-bootstrap";
import { FaChartPie } from "react-icons/fa";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";
import { makeStyles } from "@material-ui/core/styles";

const Report = () => {
  const useStylesBootstrap = makeStyles((theme) => ({
    arrow: {
      color: theme.palette.common.black,
    },
    tooltip: {
      backgroundColor: theme.palette.common.black,
      fontSize: "12px",
    },
  }));
  function BootstrapTooltip(props) {
    const classes = useStylesBootstrap();

    return <Tooltip arrow classes={classes} {...props} />;
  }

  const reportNames = ["Survey Of Wards In PCMC Area", "Survey of NGO"];
  const handleConfirm = () => {
    const confirmBox = window.confirm(
      "do you really want to delete this report?"
    );
  };
  return (
    <>
      <Card style={{ padding: "15px 0" }}>
        <div className="ibox-content">
          <div style={{ marginBottom: "30px" }}>
            <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
              <FaChartPie /> Project Reports
            </h3>
          </div>

          <div style={{ widh: "100%" }}>
            <Table size="sm" striped hover>
              <thead>
                <tr>
                  <th>Report Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {reportNames.map((ele, index) => (
                  <tr>
                    <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                      {ele}
                    </td>
                    <td style={{ paddingBottom: "0", fontSize: "13px" }}>
                      <div>
                        <BootstrapTooltip
                          TransitionComponent={Fade}
                          TransitionProps={{ timeout: 300 }}
                          title={`Edit ${ele}`}
                        >
                          <a href="/editReport">
                            <button className="btn-beneficiaries">
                              <i class="fas fa-pencil-alt csr-report-icons"></i>
                            </button>
                          </a>
                        </BootstrapTooltip>
                        <BootstrapTooltip
                          TransitionComponent={Fade}
                          TransitionProps={{ timeout: 300 }}
                          title="Delete"
                        >
                          <button
                            className="btn-beneficiaries"
                            onClick={handleConfirm}
                          >
                            <i class="fas fa-trash-alt csr-report-icons"></i>
                          </button>
                        </BootstrapTooltip>
                        <BootstrapTooltip
                          TransitionComponent={Fade}
                          TransitionProps={{ timeout: 300 }}
                          title="View"
                        >
                          <button className="btn-beneficiaries">
                            <i class="far fa-eye csr-report-icons"></i>
                          </button>
                        </BootstrapTooltip>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      </Card>
    </>
  );
};

export default Report;
