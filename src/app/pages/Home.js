import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Col, Card } from "react-bootstrap";
// import CustomButton from "../components/CustomButton";
import { zoomOutUp } from "react-animations";
import Radium, { StyleRoot } from "radium";

import { Images } from "../config/Images";
import { Videos } from "../config/Videos";
import { Link } from "react-router-dom";

const styles = {
  bounce: {
    animation: "y 2s",
    animationName: Radium.keyframes(zoomOutUp, "zoomOutUp"),
  },
};

const Home = () => {
  const [action, setAction] = useState("give");
  const [display, setDisplay] = useState("none");
  const [type, setType] = useState("Volunteering");
  const [display1, setDisplay1] = useState("none");

  const handleAction = () => {
    setDisplay("block");
  };
  const handleNone = () => {
    setDisplay("none");
  };
  const handleGet = () => {
    setAction("get");
  };
  const handleGive = () => {
    setAction("give");
  };

  const handleTypeAction = () => {
    setDisplay1("block");
  };
  const handleTypeNone = () => {
    setDisplay1("none");
  };
  const handleVolunteer = () => {
    setType("Volunteering");
  };
  const handleDonation = () => {
    setType("Donation");
  };
  const handleCSR = () => {
    setType("CSR Teams");
  };

  const navitemsIfNotAuthorized = [
    {
      name: "Volunteer",
      link: "/help",
      icon: "http://projectheena.in/assets/img/menu/white/volunteer.png"
    },
    {
      name: "Donate",
      link: "/help",
      icon: "http://projectheena.in/assets/img/menu/white/donate.png"
    },
    {
      name: "Advocate",
      link: "/help",
      icon: "http://projectheena.in/assets/img/menu/white/advocate.png"
    },
    {
      name: "CSR Projects",
      link: "/help",
      icon: "http://projectheena.in/assets/img/menu/white/collaborate.png"
    },
  ]

  return (
    <>
      <div style={{ padding: 0, margin: 0, width: "100%" }}>
        <Card className="home-video-container">
          {" "}

          <video autoPlay muted loop className="home-video-bg">
            <source src={Videos.intro} type="video/mp4"></source>
          </video>
          <div className="home-video-content">
            <h1 className="home-video-title">CHANGING THE WORLD, MADE EASY™</h1>
            <h4 className="home-sub-title">EXPLORE SMALL OPPORTUNITIES TO BRING BIG CHANGES.</h4>
            <button className="csr_join_button">Join the Revolution</button>
          </div>
          {/* <Row style={{ position: "absolute", background: "transparent", width: "100%", padding: "20px 0" }} className="no-gutters">
            <Col md={2}>
              <Link to={"/home"}>
                <img src="http://projectheena.in/assets/img/white-phlogo.png" alt="" style={{ width: "175px" }} />
              </Link>
            </Col>
            <Col md={7} style={{ textAlign: "left", paddingLeft: 50 }}>
              {navitemsIfNotAuthorized.map((nav) =>
                <>
                  <a href={nav.link}>
                    <img src={nav.icon} style={cssStyle.navBarIcon} alt="" />
                    <span style={cssStyle.navBarFont}>{nav.name}</span>
                  </a>
                </>
              )}
            </Col>
            <Col md={3}>
              <a href="/">
                <span style={cssStyle.navBarFont}>SEARCH</span>
              </a>
              <a href="/signup">
                <span style={cssStyle.navBarFont}>LOGIN</span>
              </a>
            </Col>
          </Row> */}
          <div style={{ position: "relative", bottom: 0 }} className="banner-action nl-wrapper">
            <form id="nl-form" className="nl-form">
              Want to
              <div className="nl-field nl-dd"></div>
              <div
                onClick={handleNone}
                style={{
                  position: "fixed",
                  display: display,
                  width: "100%",
                  height: "100%",
                  top: "0",
                  left: "0",
                  right: 0,
                  bottom: 0,
                  backgroundColor: "rgba(0, 0, 0, 0.5)",
                  zIndex: "2",
                  cursor: "pointer",
                }}
              >
                <div class="text">
                  <ul className="get_give">
                    <li style={{ listStyleType: "none" }}>
                      <a style={{ color: "white" }} onClick={handleGet}>
                        Get
                      </a>
                    </li>
                    <li style={{ listStyleType: "none" }}>
                      <a style={{ color: "white" }} onClick={handleGive}>
                        Give
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <a onClick={handleAction} className="nl-field-toggle">
                {action}
              </a>
              help
              <span id="text-corrector" style={{ padding: "8px" }}>
                by
              </span>
              <div
                onClick={handleTypeNone}
                style={{
                  position: "fixed",
                  display: display1,
                  width: "100%",
                  height: "100%",
                  top: "0",
                  left: "0",
                  right: 0,
                  bottom: 0,
                  backgroundColor: "rgba(0, 0, 0, 0.5)",
                  zIndex: "2",
                  cursor: "pointer",
                }}
              >
                <div class="text">
                  <ul className="get_give">
                    <li style={{ listStyleType: "none" }}>
                      <a style={{ color: "white", fontSize: "14px" }} onClick={handleVolunteer}>
                        Volunteering
                      </a>
                    </li>
                    <li style={{ listStyleType: "none" }}>
                      <a style={{ color: "white" }} onClick={handleDonation}>
                        Donation
                      </a>
                    </li>
                    <li style={{ listStyleType: "none" }}>
                      <a style={{ color: "white" }} onClick={handleCSR}>
                        CSR Teams
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <a onClick={handleTypeAction} className="nl-field-toggle">
                {type}
              </a>
              <button className="nl-submit" type="submit" style={{ margin: "8px" }}>
                {" "}
                <i class="fas fa-arrow-right" style={{ color: "white" }}></i>Go
              </button>
              <div className="nl-overlay"></div>
            </form>
          </div>
        </Card>
      </div>

      <Container fluid>
        <Row
          style={{
            backgroundImage: "url(http://projectheena.in/assets/img/home/texture.jpg)",
            padding: "50px 0",
          }}
        >
          <div
            style={{
              marginTop: "30px",
              marginLeft: "auto",
              marginRight: "auto",
            }}
          >
            <h2 style={{ fontSize: "1.8em", fontWeight: "600" }}>BENEFITS OF PROJECTHEENA</h2>
          </div>
          <Col lg={12}>
            <Col style={{ marginTop: "30px" }}>
              <StyleRoot style={{ marginTop: "7%" }}>
                <div className="test" style={styles.bounce}>
                  <Row style={{ width: "100%", display: "inline-block" }}>
                    <Col sm={1}>
                      <img
                        height="100px"
                        src={Images.opportunities}
                        alt=""
                        className="img-center"
                      />
                    </Col>
                    <Col sm={4} className="benefits">
                      <h4>Best opportunities to do good made for you</h4>
                      <p>Do good the way you want; When, where & how you prefer</p>
                    </Col>
                    <Col sm={2}></Col>
                    <Col sm={1}>
                      <img height="100px" src={Images.ironman} alt="" className="img-center" />
                    </Col>
                    <Col sm={4} className="benefits">
                      <h4>Create a social brand & be a change maker</h4>
                      <p>Get a Karma profile to showcase your contribution</p>
                    </Col>
                  </Row>
                </div>
              </StyleRoot>
              <div className="superman"></div>
              <StyleRoot>
                <div className="test" style={styles.bounce}>
                  <Row className="row2">
                    <Col md={1}>
                      <img height="100px" src={Images.oneplat} alt="" className="img-center" />
                    </Col>
                    <Col md={4} className="benefits">
                      <h4>One platform for everything Do Good!</h4>
                      <p>Give what you can, Get what you want. All at one place.</p>
                    </Col>
                    <Col md={2}></Col>
                    <Col md={1}>
                      <img height="100px" src={Images.idea} alt="" className="img-center" />
                    </Col>
                    <Col md={4} className="benefits">
                      <h4>Showcase your work & inspires others</h4>
                      <p>
                        Follow Nonprofits, get updates on how your contribution made a difference
                      </p>
                    </Col>
                  </Row>
                </div>
              </StyleRoot>
            </Col>
          </Col>
        </Row>

        <Row className="app-stats">
          <Col lg={4} className="stat-column">
            <h3 className="stat-title"> REGISTERED VOLUNTEERS</h3>
            <h2 className="stat-data">2310</h2>
          </Col>
          <Col lg={4} className="stat-column">
            <h3 className="stat-title"> REGISTERED NON PROFITS</h3>
            <h2 className="stat-data">254</h2>
          </Col>
          <Col lg={4} className="stat-column">
            <h3 className="stat-title"> VOLUNTEERING HOURS CONTRIBUTED</h3>
            <h2 className="stat-data">5697</h2>
          </Col>
        </Row>

        {/* <Row style={{ backgroundColor: "white", padding: "10px" }}>
          <Col lg={7}>
            <p>One Simple solutions for</p>
            <h2>CORPORATE CSR TEAMS & SOCIAL CAMPAIGNS</h2>
            <p>
              We offer this powerful ProjectHeena platform to execute end to end
              CSR engagement online in an easy to manage way. Whether it be
              managing complete CSR projects or advocacy & amplification of
              social campaigns, we help utilize technology for social good.
            </p>

            <Col style={{ marginTop: "40px" }}>
              <Row style={{ marginBottom: "20px" }}>
                <Col md={1} className="home-solutions-img">
                  <img
                    height="40px"
                    src={Images.Employee}
                    alt=""
                    className="img-center"
                  />
                </Col>
                <Col className="home-solutions-title">
                  <h4>Employee Engagement</h4>
                </Col>
                <Col md={1} className="home-solutions-img">
                  <img
                    height="40px"
                    src={Images.Monitoring}
                    alt=""
                    className="img-center"
                  />
                </Col>
                <Col className="home-solutions-title">
                  <h4>CSR Project Monitoring</h4>
                </Col>
              </Row>
              <Row>
                <Col md={1} className="home-solutions-img">
                  <img
                    height="40px"
                    src={Images.Philanthropy}
                    alt=""
                    className="img-center"
                  />
                </Col>
                <Col className="home-solutions-title">
                  <h4>Philanthropy Solutions</h4>
                </Col>
                <Col md={1} className="home-solutions-img">
                  <img
                    height="40px"
                    src={Images.Advocacy}
                    alt=""
                    className="img-center"
                  />
                </Col>
                <Col className="home-solutions-title">
                  <h4>Advocacy & Social Brand Building</h4>
                </Col>
              </Row>
              <Row style={{ marginTop: "30px" }}>
                <div style={{ margin: "auto" }}>
                  <a href="/corporateSolutions">
                    <CustomButton content="Corporate Solutions" />
                  </a>
                </div>
              </Row>
            </Col>
          </Col>
          <Col lg={5}>
            <img
              src={Images.volunteers}
              alt=""
              height="300"
              width="400"
              className="img-center"
            />
          </Col>
        </Row> */}
        <div className="item app-corporate">
          <div className="container">
            <div className="row">
              <div className="col-md-6 content-box">
                <p className="title-intro">One Simple solutions for</p>
                <h3 className="item-title">Corporate CSR Teams &amp; Social Campaigns</h3>
                <p className="item-content">
                  We offer this powerful ProjectHeena platform to execute end to end CSR engagement
                  online in an easy to manage way. Whether it be managing complete CSR projects or
                  advocacy &amp; amplification of social campaigns, we help utilize technology for
                  social good.
                </p>

                <Row>
                  <Col md={6}>
                    <div style={{ display: "table-cell", padding: "0 20px" }}>
                      <img src={Images.Employee} alt="" style={{ width: "44px" }} />
                    </div>

                    <div style={{ display: "table-cell" }}>
                      <h5 style={{ fontSize: "14px" }}>Employee Engagement</h5>
                    </div>
                  </Col>

                  <Col md={6}>
                    <div style={{ display: "table-cell", padding: "0 20px" }}>
                      <img src={Images.Philanthropy} alt="" style={{ width: "44px" }}></img>
                    </div>
                    <div style={{ display: "table-cell" }}>
                      <h5 style={{ fontSize: "14px" }}>Philanthropy Solutions</h5>
                    </div>
                  </Col>
                </Row>

                <Row>
                  <Col md={6}>
                    <div style={{ display: "table-cell", padding: "0 20px" }}>
                      <img src={Images.Monitoring} alt="" style={{ width: "44px" }} />
                    </div>

                    <div style={{ display: "table-cell" }}>
                      <h5 style={{ fontSize: "14px" }}>CSR Project Monitoring</h5>
                    </div>
                  </Col>

                  <Col md={6}>
                    <div style={{ display: "table-cell", padding: "0 20px" }}>
                      <img src={Images.Advocacy} alt="" style={{ width: "44px" }}></img>
                    </div>
                    <div style={{ display: "table-cell" }}>
                      <h5 style={{ fontSize: "14px", textAlign: "left" }}>
                        {" "}
                        Advocacy & Social Brand Building
                      </h5>
                    </div>
                  </Col>
                </Row>

                <div className="text-center m-t-md">
                  <button className="csr_button">Corporate Solutions</button>
                </div>
              </div>
              <div className="col-md-6">
                <img
                  className="app-corporate-img"
                  src="http://projectheena.in/assets/img/home/volunteering.jpg "
                />
              </div>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};
export default Home;

const cssStyle = {
  navBarFont: { cursor: "pointer", padding: "5px 35px 5px 5px", fontSize: 14, color: "white", fontWeight: 600, textTransform: "uppercase" },
  navBarIcon: { maxHeight: "24px", verticalAlign: "middle" },
}