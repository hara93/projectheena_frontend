/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import { Form, Col, InputGroup } from "react-bootstrap";
import Card from "react-bootstrap/Card";
// import CustomButton from "../components/CustomButton";
import { useFormik } from "formik";
import { string, object } from "yup";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import Input from "../components/form/Input";
import axios from "../config/axios";
import { setUserData } from "../../redux/reducers/auth/actionCreator";
import Select from "react-select";
// import { Input } from "../../_metronic/_partials/controls/index";
import { useEffect } from 'react';

function onChange(value) {
  console.log("captcha value", value);
}
const country_code = [
  {
    value: "+91",
    label: "+91",
  },
  {
    value: "+92",
    label: "+92",
  },
  {
    value: "+93",
    label: " +93",
  },
];

const MailSignUp = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    console.log("else-not-showing")
  }, [])

  const defaultValues = {
    first_name: "",
    last_name: "",
    email_id: "",
    password: "",
    country_code: "",
  };

  const validationSchema = object().shape({
    first_name: string().required("First name required"),
    last_name: string().required("Last name required"),
    email_id: string().required(" Email Id is required"),
    password: string().required("Password is required"),
    user_contact_number: string().required("Contact number is required"),
    country_code: string(),
  });

  const onSubmit = (values) => {
    values.country_code = countryCode;
    console.log("values", values);
    axios.post("/user/signup", { ...values }).then(({ data }) => {
      console.info(data);
      dispatch(setUserData(data));
      localStorage.setItem("token", data.token);
      history.push("/signup");
    });
    console.log("values", values);
  };

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    values,
    errors,
    touched,
    initialValues,
  } = useFormik({
    initialValues: defaultValues,
    validationSchema,
    onSubmit,
  });
  const [countryCode, setCountryCode] = useState("+91");
  const setCode = (code) => {
    setCountryCode(code);
  };
  return (
    <>
      {/* width:66% aayega starting me */}
      <div
        style={{
          maxWidth: "800px",
          margin: "0 auto",

          padding: "0 20px",
        }}
      >
        <div
          style={{
            margin: "auto",
            backgroundColor: "#ffffff00",
            border: "0",
          }}
        >
          <div style={{ marginBottom: "30px" }}>
            <h2 className="font-bold">Welcome to ProjectHeena</h2>
            <p>
              Please use the below form to register using email id. Please check your spam folder if
              confirmation link is not received within next 15 minutes. For any issues click on the
              ProjectHeena logo near bottom of your scrollbar.
            </p>
          </div>
        </div>
        <Card style={{ padding: "15px 20px 20px 20px", margin: "auto" }}>
          <Form onSubmit={handleSubmit} style={{ marginTop: "15px" }}>
            <Form.Group>
              <Form.Row>
                <Col xs={12} md={12} lg={6} className="mail-signup-col-l">
                  <Form.Label className="mail-signup-label">First name</Form.Label>
                  <Input
                    type="text"
                    name="first_name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.first_name}
                    value={values.first_name}
                    aria-required="true"
                    className="m-0 green-focus"
                  ></Input>
                </Col>
                <Col className="xs">
                  <hr className="hr-mailsignup" />
                </Col>

                <Col xs={12} md={12} lg={6} className="mail-signup-col-r">
                  <Form.Label className="mail-signup-label">Last name</Form.Label>
                  <Input
                    type="text"
                    name="last_name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.last_name}
                    value={values.last_name}
                    aria-required="true"
                    className="m-0 green-focus"
                  ></Input>
                </Col>
              </Form.Row>
              <hr className="hr-mailsignup" />
              <Form.Row>
                <Col xs={12} md={12} lg={6} className="mail-signup-col-l">
                  <Form.Label className="mail-signup-label">Email ID</Form.Label>
                  <Input
                    type="text"
                    name="email_id"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.email_id}
                    value={values.email_id}
                    aria-required="true"
                    className="m-0 green-focus"
                  ></Input>
                </Col>
                <Col className="xs">
                  <hr className="hr-mailsignup" />
                </Col>
                <Col xs={12} md={12} lg={6} className="mail-signup-col-r">
                  <Form.Label className="mail-signup-label">Contact Number</Form.Label>
                  <InputGroup>
                    <div style={{ display: "flex", width: "100%" }}>
                      <div style={{ maxWidth: "77px", width: "100%" }}>
                        <Select
                          defaultValue={country_code[0]}
                          name="country_code"
                          options={country_code}
                          onChange={(e) => setCode(e.value)}
                          style={{ borderRadius: "0" }}
                        />
                      </div>
                      <div style={{ width: "100%" }}>
                        <Input
                          type="number"
                          name="user_contact_number"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          // error={errors.user_contact_number}
                          value={values.user_contact_number}
                          aria-required="true"
                          placeholder="Your Contact number"
                          className="m-0 green-focus"
                        ></Input>
                      </div>
                    </div>
                  </InputGroup>
                  <Form.Text className="text-muted" muted>
                    We do not spam
                  </Form.Text>
                </Col>
              </Form.Row>
              <hr className="hr-mailsignup" />
              <Form.Row>
                <Col xs={12} md={12} lg={6} className="mail-signup-col-l">
                  <Form.Label className="mail-signup-label">Password</Form.Label>
                  <Input
                    type="password"
                    name="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.password}
                    value={values.password}
                    aria-required="true"
                    className="m-0 green-focus"
                  ></Input>
                </Col>
                <Col>
                  <Form.Label></Form.Label>
                </Col>
              </Form.Row>
              <hr className="hr-mailsignup" />

              <Form.Row>
                <Col xs={12}>
                  <ReCAPTCHA
                    sitekey="6LcjScAaAAAAAJFEVrvGCWI-OTc9SeNuHWY5JFT3"
                    onChange={onChange}
                  />
                </Col>

                <Col></Col>
              </Form.Row>
              <hr className="hr-mailsignup" />
              <Form.Row>
                <Col>
                  {/* <CustomButton content="Create Account" /> */}
                  <button
                    type="submit"
                    className="ph-btn"
                    style={{ fontSize: "14px", padding: "6px" }}
                  >
                    Create Account
                  </button>
                </Col>
                <Col></Col>
              </Form.Row>
            </Form.Group>
          </Form>
        </Card>
      </div>
    </>
  );
};
export default MailSignUp;