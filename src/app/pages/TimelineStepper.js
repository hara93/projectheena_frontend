
import React from 'react';

const TimelineStepper = () => {
  return (
    <div className="container">
      <div class="page page-dashboard dashboard-timeline">
        <div class="row">
          <div class="col-md-12 m-b-md">
            <h2 class="font-bold">Project timeline</h2>
            <p>
              Following is a list of all projects created by you. More features coming soon!
            </p>
          </div>
        </div>


        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">

              <div class="ibox-content">
                <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">

                  <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon navy-bg">
                      <i class="fa fa-bullhorn fa-flip-horizontal" style={{ color: 'white', paddingTop: '24%' }} ></i>
                    </div>

                    <div class="vertical-timeline-content">
                      <h2>ST Youth Skill Trainging Program 1</h2>
                      <p>This project identified three important areas were the Adivasi youth need a strong support system to link them with the employment opportunities. First is to support them to continue the education up to their expectations and ability by establishing proper</p>
                      <button className="more-info-left">More Info</button>
                      {/* <a href="http://projectheena.in/project/st-youth-skill-trainging-program-1573635156" class="btn btn-sm btn-primary"> More info</a> */}
                      <span class="vertical-date-right">
                        <small>13<sup>th</sup> Nov, 2019</small>
                      </span>
                    </div>


                  </div>

                  <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon navy-bg">
                      <i class="fa fa-bullhorn " style={{ color: 'white', paddingTop: '24%' }} ></i>
                    </div>

                    <div class="vertical-timeline-content block">
                      <h2>Smokeless Chulhas 2</h2>
                      <p>Smokeless Chulhas will promote clean household energy that can in turn help to achieve environmental sustainability as well as improved air quality within the household. A report on how smokeless stoves in rural areas can keep homes warm without causing in</p>
                      {/* <a href="http://projectheena.in/project/smokeless-1516288430" class="btn btn-sm btn-primary"> More info</a> */}
                      <button className="more-info-right">More Info</button>
                      <span class="vertical-date-left">
                        <small>18<sup>th</sup> Jan, 2018</small>
                      </span>
                    </div>
                  </div>

                  <div class="vertical-timeline-block ">
                    <div class="vertical-timeline-icon navy-bg">
                      <i class="fa fa-bullhorn fa-flip-horizontal" style={{ color: 'white', paddingTop: '24%' }} ></i>
                    </div>

                    <div class="vertical-timeline-content block2">
                      <h2>ST Youth Skill Trainging Program 1</h2>
                      <p>This project identified three important areas were the Adivasi youth need a strong support system to link them with the employment opportunities. First is to support them to continue the education up to their expectations and ability by establishing proper</p>
                      <button className="more-info-left">More Info</button>
                      {/* <a href="http://projectheena.in/project/st-youth-skill-trainging-program-1573635156" class="btn btn-sm btn-primary"> More info</a> */}
                      <span class="vertical-date-right">
                        <small>13<sup>th</sup> Nov, 2019</small>
                      </span>
                    </div>
                  </div>

                  <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon navy-bg">
                      <i class="fa fa-bullhorn " style={{ color: 'white', paddingTop: '24%' }} ></i>
                    </div>

                    <div class="vertical-timeline-content block">
                      <h2>Smokeless Chulhas 2</h2>
                      <p>Smokeless Chulhas will promote clean household energy that can in turn help to achieve environmental sustainability as well as improved air quality within the household. A report on how smokeless stoves in rural areas can keep homes warm without causing in</p>
                      {/* <a href="http://projectheena.in/project/smokeless-1516288430" class="btn btn-sm btn-primary"> More info</a> */}
                      <button className="more-info-right">More Info</button>
                      <span class="vertical-date-left">
                        <small>18<sup>th</sup> Jan, 2018</small>
                      </span>
                    </div>
                  </div>

                  <div class="vertical-timeline-block ">
                    <div class="vertical-timeline-icon navy-bg">
                      <i class="fa fa-bullhorn fa-flip-horizontal" style={{ color: 'white', paddingTop: '24%' }} ></i>
                    </div>

                    <div class="vertical-timeline-content block2">
                      <h2>ST Youth Skill Trainging Program 1</h2>
                      <p>This project identified three important areas were the Adivasi youth need a strong support system to link them with the employment opportunities. First is to support them to continue the education up to their expectations and ability by establishing proper</p>
                      <button className="more-info-left">More Info</button>

                      <span class="vertical-date-right">
                        <small>13<sup>th</sup> Nov, 2019</small>
                      </span>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  )
}

export default TimelineStepper
