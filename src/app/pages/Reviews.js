/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import "../../index.scss";
import CustomButtonOutline from "../components/CustomButtonOutline";
import { FaQuoteLeft } from "react-icons/fa";
import Rating from "../components/Rating";
import { Modal, Form, Col, Button, Card, Row } from "react-bootstrap";
import CustomButton from "../components/CustomButton";

const Reviews = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [count, setCount] = useState(0);
  const maxCount = 300;

  return (
    <>
      <Modal show={show} onHide={handleClose} animation={false} size="lg">
        <div
          style={{
            height: "60px",
            borderBottom: "1px solid #cccccc",
            padding: "15px 10px 5px 10px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h4>Write Review</h4>
          <Button
            variant="secondary"
            style={{ height: "30px" }}
            onClick={handleClose}
          >
            <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ minHeight: "150px", padding: "20px" }}>
            <Form>
              <Form.Group>
                <Form.Row>
                  <Col>
                    <Form.Label
                      style={{ marginBottom: "5px", fontWeight: "700" }}
                    >
                      Collaborator Name
                    </Form.Label>
                    <Form.Control type="text" className="green-focus" />
                  </Col>
                </Form.Row>
              </Form.Group>
              <hr
                className="form-horizontal-line"
                style={{ margin: "20px 0" }}
              />

              {/* <Form.Row>
                  <Col>
                    <Form.Label
                      style={{ marginBottom: "5px", fontWeight: "700" }}
                    >
                      Rating
                    </Form.Label>
                    <Rating />
                    <span className="help-block">
                      You can rate collaborator with 1 being lowest and 5 as max
                    </span>
                  </Col>
                </Form.Row> */}

              <div className="spacing">
                <Form.Label style={{ marginBottom: "5px", fontWeight: "700" }}>
                  Rating
                </Form.Label>

                <Rating />
                <span className="help-block">
                  You can rate collaborator with 1 being lowest and 5 as max
                </span>
              </div>
              <hr
                className="form-horizontal-line"
                style={{ margin: "20px 0" }}
              />
              <Form.Group>
                <Form.Row>
                  <Col>
                    <Form.Label>Title</Form.Label>

                    <Form.Control
                      type="text"
                      placeholder="Write your review title"
                      className="green-focus"
                    />
                  </Col>
                </Form.Row>
                <hr
                  className="form-horizontal-line"
                  style={{ margin: "20px 0" }}
                />
                <Form.Row>
                  <Col>
                    <Form.Label>Review</Form.Label>
                    <Form.Control
                      size="lg"
                      type="text"
                      as="textarea"
                      maxLength="300"
                      rows="5"
                      placeholder="Your Review"
                      onChange={(e) => setCount(e.target.value.length)}
                      className="green-focus"
                      style={{ borderRadius: "0" }}
                    />
                    <span className="help-block">
                      Max: 300 chars | Remaining Chars: {maxCount - count}
                    </span>
                  </Col>
                </Form.Row>
              </Form.Group>
            </Form>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Submit review" />
        </Modal.Footer>
      </Modal>
      <Card style={{ padding: "14px 15px 7px" }}>
        <div className="ibox-content">
          <div className="review-title">
            <div>
              <h3
                style={{
                  fontSize: "16px",
                  fontWeight: "600",
                  marginBottom: "0",
                }}
              >
                <FaQuoteLeft style={{ fontSize: "1.5rem" }} /> Project Reviews
              </h3>
            </div>
            <div onClick={handleShow}>
              <CustomButtonOutline content="Write review" />
            </div>
          </div>
          <div>
            <h4 style={{ fontSize: "14px ", fontWeight: "600" }}>Reviews</h4>
            <div className="no-content">
              {/* <h3 className="no-content-msg" style={{ color: "#c4c4c4" }}>
                No Reviews to show
              </h3> */}
              <Row>
                <Col xs={2}>
                  <img
                    src="http://projectheena.in/uploads/users/16144766306040/me.jpg"
                    alt="logo"
                    className="img-circle img-center "
                  ></img>
                </Col>
                <Col>
                  <Card
                    style={{
                      padding: "1rem",
                      textAlign: "left",
                      backgroundColor: "#f5f5f5",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <h5 style={{ fontWeight: "600" }}>
                        <a href="" className="basecolor">
                          Title
                        </a>
                      </h5>
                      <button
                        type="button"
                        style={{ border: "0", background: "transparent" }}
                      >
                        <i
                          class="fas fa-pencil-alt  "
                          style={{ color: "inherit" }}
                        ></i>
                      </button>
                    </div>
                    <div>
                      <p style={{ margin: "1rem 0" }}>"Reviewd text"</p>
                    </div>
                    <ul
                      className="review-card-ul"
                      style={{ margin: "0", padding: "0" }}
                    >
                      <li style={{ listStyleType: "none" }}>Reviewed:</li>
                      <li style={{ listStyleType: "none" }}>Reviewer:</li>
                      <li style={{ listStyleType: "none" }}>Name:</li>
                      <li style={{ listStyleType: "none" }}>Date:</li>
                    </ul>
                  </Card>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </Card>
    </>
  );
};

export default Reviews;
