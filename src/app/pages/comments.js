import React from "react";
import NewUpdate from "../components/new_update";
import * as Reactstrap from "react-bootstrap";
import { Col, Row } from "react-bootstrap";
import { Card, CardHeader } from "@material-ui/core";
import { Images } from "../config/Images";
import { CardBody } from "reactstrap";
import CommentUpdate from "../components/CommentUpdate";
import { FaComments } from "react-icons/fa";

const update = [
  {
    heading: "Children studying in classroom",
    image: "Children",
    title: "Children_in_School",
    date: "Added: 02nd Jun, 2017",
    milestone: "FY 2016-17 - Phase IV - Community Mobilization, Interventions and Monitoring",
    reply: false,
  },

  {
    heading: "Children studying in classroom",
    image: "Children",
    title: "Children_in_School",
    date: "Added: 02nd Jun, 2017",
    milestone: "FY 2016-17 - Phase IV - Community Mobilization, Interventions and Monitoring",
    reply: false,
  },
  {
    heading: "Children studying in classroom",
    image: "Children",
    title: "Children_in_School",
    date: "Added: 02nd Jun, 2017",
    milestone: "FY 2016-17 - Phase IV - Community Mobilization, Interventions and Monitoring",
    reply: false,
  },
];

class Comments extends React.Component {
  render() {
    return (
      <>
        <Row className="no-gutters">
          <Col md={12}>
            <Card className="p-4">
              <div className="item">
                <h3 className="csr_heading">
                  <i class="fa fa-comments"></i> Comments
                </h3>
              </div>
              {/* <h3 style={{ marginBottom: '2vh' }}> <FaComments color="#676a6c" /> Comments</h3> */}
              <div style={{ padding: "4vh 3vw 4vh 1vw" }}>
                <Row>
                  <Col md={4}>
                    <Row>
                      <Col sm={6}>
                        <span className="collaborator_name">Collaborator Name</span>
                      </Col>
                      <Col sm={6}>
                        <select>
                          <option>All</option>
                          <option>Umang</option>
                          <option>Social Work</option>
                          <option>Glenmark</option>
                        </select>
                      </Col>
                    </Row>
                  </Col>

                  <Col md={4}>
                    <Row>
                      <Col sm={4}>
                        <span className="text-align-end">Date</span>
                      </Col>
                      <Col sm={8}>
                        <input type="date" />
                      </Col>
                    </Row>
                  </Col>

                  <Col md={4}>
                    <Row>
                      <Col sm={6}>
                        <span>Milestone Name</span>
                      </Col>
                      <Col sm={6}>
                        <select>
                          <option>All</option>
                          <option>Umang</option>
                          <option>Social Work</option>
                          <option>Glenmark</option>
                        </select>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>

              <Row>
                <Col md={12}>
                  <NewUpdate style={{ backgroundColor: "white" }} />
                  <h3
                    style={{
                      fontSize: "16px",
                      wordSpacing: "3px",
                      color: "#676a6c",
                      marginTop: "5px",
                    }}
                  >
                    15 updates shared
                  </h3>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <CommentUpdate update={update} />
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </>
    );
  }
}

export default Comments;
