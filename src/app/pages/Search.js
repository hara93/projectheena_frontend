import React, { useEffect, useRef, useState } from "react";
import { Row, Col } from "react-bootstrap";
import SearchPeople from "./SearchPeople";
import TaskSearchBox from "../components/TaskSearchBox";
import SearchTask from "./SearchTask";
import ProjectSearchBox from "../components/ProjectSearchBox";
import SearchProject from "./SearchProject";
import DonationSearchBox from "../components/DonationSearchBox";
import SearchDonation from "./SearchDonation";
import SearchBoxPeople from "../components/SearchBoxPeople";
import { Link } from "react-router-dom";

const Search = (props) => {
  const focus = useRef();
  const focusCSS = {
    borderBottom: "5px solid #4caf4f",
    paddingBottom: "1px",
  };
  const [state, setState] = useState("");
  useEffect(() => {
    console.log("hrrksjdbkaskfjbaksfhgbksfjbkasrufdvk")
    var current_url = window.location.href
    var url = new URL(current_url);
    var c = url.searchParams.get("tab");
    setState(c)
    focus.current.focus()
  }, [window.location.href]);
  const [noOfPeople, setNoOfPeople] = useState(0);
  const [noOfTask, setNoOfTask] = useState(0);
  const [noOfProject, setNoOfProject] = useState(0);
  const [noOfDonation, setNoOfDonation] = useState(0);

  return (
    <div className="container">
      <div style={{ margin: "0 0 30px 0" }}>
        {state === "people" ? (
          <h1 style={{ fontSize: "30px" }}>
            Search: {noOfPeople} Volunteers found that matches your query
          </h1>
        ) : null}
        {state === "task" ? (
          <h1 style={{ fontSize: "30px" }}>
            Search: {noOfTask} Tasks found that matches your query
          </h1>
        ) : null}
        {state === "project" ? (
          <h1 style={{ fontSize: "30px" }}>
            Search: {noOfProject} Projects found that matches your query
          </h1>
        ) : null}
        {state === "donation" ? (
          <h1 style={{ fontSize: "30px" }}>
            Search: {noOfDonation} Donations found that matches your query
          </h1>
        ) : null}
      </div>

      <div>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <ul className="search-tab">
            <li ref={focus} tabindex="-1" style={state === "people" ? focusCSS : {}}>
              <Link to={"/searchtab?tab=people"} style={{ color: "black" }}>
                <div onClick={() => setState("people")}>
                  <i
                    style={{
                      fontSize: "13px",
                      color: "#9c9d9e",
                      paddingBottom: "0px",
                      marginRight: "7px",
                    }}
                    class="fas fa-male"
                  ></i>
                  <span className="search-focus">People</span>
                </div>
              </Link>
            </li>
            <li tabindex="-1" style={state === "task" ? focusCSS : {}}>
              <Link to={"/searchtab?tab=task"} style={{ color: "black" }}>
                <div onClick={() => setState("task")}>
                  <i
                    style={{ fontSize: "13px", color: "#9c9d9e", marginRight: "7px" }}
                    class="fas fa-tasks"
                  ></i>
                  <span className="search-focus">Task</span>
                </div>
              </Link>
            </li>
            <li tabindex="-1" style={state === "project" ? focusCSS : {}}>
              <Link to={"/searchtab?tab=project"} style={{ color: "black" }}>
                <div onClick={() => setState("project")}>
                  <i
                    style={{ fontSize: "13px", color: "#9c9d9e", marginRight: "7px" }}
                    class="fas fa-hand-paper"
                  ></i>
                  <span className="search-focus">Project</span>
                </div>
              </Link>
            </li>
            <li tabindex="-1" style={state === "donation" ? focusCSS : {}}>
              <Link to={"/searchtab?tab=donation"} style={{ color: "black" }}>
                <div onClick={() => setState("donation")}>
                  <i
                    style={{ fontSize: "13px", color: "#9c9d9e", marginRight: "7px" }}
                    class="fas fa-rupee-sign"
                  ></i>
                  <span className="search-focus">Donation</span>
                </div>
              </Link>
            </li>
          </ul>
        </div>
        <div>
          {state === "people" ? (
            <Row style={{ background: "#f1f1f1" }}>
              <Col md={3} className="p-0">
                {/* <PeopleSearchBox /> */}
                <SearchBoxPeople />
              </Col>
              <Col md={9}>
                <SearchPeople count={(no) => setNoOfPeople(no)} />
              </Col>
            </Row>
          ) : null}
          {state === "task" ? (
            <Row style={{ background: "#f1f1f1" }}>
              <Col md={3} className="p-0">
                <TaskSearchBox />
              </Col>
              <Col md={9}>
                <SearchTask count={(no) => setNoOfTask(no)} />
              </Col>
            </Row>
          ) : null}
          {state === "project" ? (
            <Row style={{ background: "#f1f1f1" }}>
              <Col md={3} className="p-0">
                <ProjectSearchBox />
              </Col>
              <Col md={9}>
                <SearchProject count={(no) => setNoOfProject(no)} />
              </Col>
            </Row>
          ) : null}
          {state === "donation" ? (
            <Row style={{ background: "#f1f1f1" }}>
              <Col md={3} className="p-0">
                <DonationSearchBox count={(no) => setNoOfDonation(no)} />
              </Col>
              <Col md={9}>
                <SearchDonation />
              </Col>
            </Row>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Search;
