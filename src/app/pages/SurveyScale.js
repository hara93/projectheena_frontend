import React from "react";
import {
  Form,
  Card,
  Container,
  Row,
  Col,
  OverlayTrigger,
  Tooltip,
  Modal,
  Button,
} from "react-bootstrap";
import Question from "../components/Question";
import Example from "../components/Phone";

const SurveyScale = () => {
  return (
    <Card className="p-5">
    <Question />

    <Row style={{color:'#828490'}}>
        <Col md={2}>
        <p>Minimum</p>
       
        </Col>
        <Col md={1}>
           
        </Col>


        <Col md={2}>
        <p>Maximum</p>
      
        </Col>
        <Col md={2}>
        <p style={{display:'inline-block'}}>Step Size</p> <OverlayTrigger
          placement="right"
          delay={{ show: 100, hide: 100 }}
          overlay={
            <Tooltip>
              A question keyword is an alias or second name for your
              questions. It makes your data reports simpler to understand and
              more analysis-friendly.
            </Tooltip>
          }
        >
          <i class="fas fa-info-circle" style={{ marginLeft: "10px" }}></i>
        </OverlayTrigger>
       
        </Col>
    </Row>
    <Row style={{color:'#828490'}}>
        <Col md={2}>
        <input type="number" style={{width:'100%'}}></input>
        </Col>
        <Col md={1}>
        <p>to</p>
        </Col>
        <Col md={2}> 
        <input type="number" style={{width:'100%'}}></input>
        </Col>
        <Col md={2}>
        <input type="number" style={{width:'100%'}}></input>
        </Col>

    </Row>
  </Card>
  );
};

export default SurveyScale;
