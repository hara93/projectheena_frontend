// http://localhost:3000/aboutngo
import React, { Component } from 'react';
import SingleNGOLayout from '../components/SingleNGOLayout';

const aboutngo = {
    about_ngo: "Abhinav Samaj' is a non-profit registered Charitable Trust with Govt. of NCT of Delhi vide Registration No.3466 Dated 7th April 2010 working on issues affecting the urban and rural poor, with a special focus on Elederly/Old Age Peoples specially for destitute parents de-sheltered by their own children. Since this is a very common problem in current times and because Every elderly is a base-pillar of the society and that society can never be developed where there is no respect for the warriors of past i.e. OLD AGE PEOPLES and ABHINAV SAMAJ is committed for same and works towards making this possible-work for welfare and rehabilitation of such elderly/destitute parents.",
    ngo_name: "ABHINAV SAMAJ",
    ngo_place: "West Delhi",
    ngo_image: "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG"
}
class AboutNGO extends Component {
    render() {
        return (
            <div >
                <SingleNGOLayout aboutngo={aboutngo} />
            </div>
        )
    }
}
export default AboutNGO;