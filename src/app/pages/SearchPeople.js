/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import { Row, Col, Button } from "react-bootstrap";
import { Images } from "../config/Images";
// import PeopleCard from "../components/PeopleCard";
// import CustomButton from "../components/CustomButton";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import axios from "../config/axios";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { setOtherUser } from "../../redux/reducers/auth/actionCreator";
import { useFormik } from "formik";

const peopleData = [
  {
    img: "",
    name: "Avinash Tripathi",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Avinash Tripathi",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Avinash Tripathi",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Avinash ",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Avinash ",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Avinash ",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Suyash Tripathi",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Suyash Tripathi",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Suyash Tripathi",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Anurag Basu",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Anurag Basu",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
  {
    img: "",
    name: "Anurag Basu",
    place: "Gurgaon, Harayana",
    bio:
      "Research - R&D, Content Writing / Blogging / Copywriting, Consulting / Strategy, fundraising",
  },
];
const SearchPeople = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [people, setPeople] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [peoplePerPage] = useState(6);
  const [userId, setUserId] = useState();
  const user = useSelector((state) => state.phAuth.user);
  const [data, setData] = useState([]);
  const userList = useSelector((state) => state.common.usersList || []);
  console.log("redux", userList);
  const [usersList, setUsersList] = useState([]);
  const getTaskList = () => {
    axios.post("/task/getTaskByCreatorId", { creator_id: user._id }).then((response) => {
      const data = response.data;
      console.log("response", data);
      var temp = [];
      data.forEach((ele) => temp.push({ value: ele._id, label: ele.task_name }));
      setData(temp);
    });
  };
  const getUsersList = () => {
    axios.post("/user/list").then((response) => {
      setPeople(response.data);
      console.log("users List", response);
      props.count(response.data.length);
    });
  };
  const onSubmit = (values) => {
    var data = {};
    data.task_id = task;
    data.task_user_id = userId;
    data.task_user_shared_email = shareEmail;
    data.task_user_shared_contact_number = shareContact;
    data.task_user_type = 0;
    data.task_comment = helpReason;

    // console.log("values", data);

    axios
      .post("/taskTransaction/seek_help", data)
      .then((res) => {
        console.log("res", res);
        // window.location.reload();
      })
      .catch((err) => console.log("err", err));
    setShow(false);
    setTask();
    setUserId();
    setShareEmail(0);
    setShareContact(0);
    setHelpReason("");
  };
  const { handleSubmit, errors, handleChange, touched, values } = useFormik({
    initialValues: { update_message: "" },
    enableReinitialize: true,
    onSubmit,
    // validationSchema,
  });
  useEffect(() => {
    getTaskList();
    getUsersList();
  }, []);
  const indexOfLastPerson = currentPage * peoplePerPage;
  const indexOfFirstPerson = indexOfLastPerson - peoplePerPage;
  const currentPeople = people.slice(indexOfFirstPerson, indexOfLastPerson);

  const pageNumber = [];
  const totalPeople = people.length;

  for (let i = 1; i <= Math.ceil(totalPeople / peoplePerPage); i++) {
    pageNumber.push(i);
  }
  const [show, setShow] = useState(false);
  const handleClose = () => {
    setShow(false);
    setTask();
    setUserId();
    setShareEmail(0);
    setShareContact(0);
    setHelpReason("");
  };
  const handleShow = (idx) => setShow(idx);

  const [helpReason, setHelpReason] = useState("");
  const [shareContact, setShareContact] = useState(0);
  const [shareEmail, setShareEmail] = useState(0);
  const [task, setTask] = useState();
  // console.log("uid", shareContact, shareEmail);

  const handleUserClick = (user_id) => {
    axios
      .post("/user/retrieve", { user_id: user_id })
      .then(({ status, data }) => {
        if (status === 1) {
          dispatch(setOtherUser(data));
          console.log("data", data);
        } else {
          history.push("/searchTab");
        }
      })
      .catch((err) => console.log(err));
  };
  return (
    <>
      {/* <div>
        <PeopleCard people={currentPeople} />
      </div> */}
      <Row style={{ padding: "15px " }}>
        {currentPeople.map((ele, idx) => (
          <>
            <Col md={6}>
              <div className="result-card">
                <div className="result-basic">
                  <img className="img-circle" src={Images.user} alt="people"></img>
                </div>
                <div className="result-desc" style={{ minHeight: "" }}>
                  <h4 className="font-bold" style={{ fontSize: "14px" }}>
                    <Link
                      to={{
                        pathname: "/userprofile",
                        state: { notMe: 1 },
                      }}
                      onClick={() => handleUserClick(ele._id)}
                      className="ph-link"
                    >
                      {`${ele.first_name} ${ele.last_name}`}
                    </Link>
                    {/* <a href="/userprofile" className="ph-link">
                    </a> */}
                  </h4>
                  <div style={{ height: "110px" }}>
                    <div style={{ margin: "10px 0" }}>
                      <div style={{ display: "table-cell", width: "25px" }}>
                        <i
                          class="fas fa-map-marker-alt"
                          style={{ fontSize: "13px", color: "#616161", paddingRight: "8px" }}
                        ></i>
                      </div>
                      <div style={{ display: "table-cell" }}>
                        <span>{ele.user_location}</span>
                      </div>
                    </div>
                    <div>
                      <div style={{ display: "table-cell", width: "25px" }}>
                        <i
                          class="fas fa-tags"
                          style={{ fontSize: "13px", color: "#616161", paddingRight: "8px" }}
                        ></i>
                      </div>
                      <div style={{ display: "table-cell" }}>
                        <span>{ele.skills_mapped}</span>
                      </div>
                    </div>
                  </div>
                  <div style={{ display: "flex", justifyContent: "flex-end" }}>
                    <button
                      className="ph-btn"
                      onClick={() => {
                        setShow(idx);
                        setUserId(ele._id);
                      }}
                    >
                      Get Help
                    </button>
                  </div>
                </div>
              </div>
            </Col>
            <Modal show={show === idx} onHide={handleClose} animation={false} centered>
              {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
              <div className="modal-header-custom">
                <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
                  Seek Help from {ele.first_name} in one of these Tasks
                </h4>
                <Button variant="secondary" style={{ height: "30px" }} onClick={handleClose}>
                  <i class="fas fa-times" style={{ paddingRight: "0", color: "white" }}></i>
                </Button>
              </div>
              <Modal.Body>
                <div style={{ padding: "20px 10px" }}>
                  <Form onSubmit={handleSubmit}>
                    <Form.Group>
                      <Select
                        options={data}
                        className="green-focus"
                        onChange={(e) => setTask(e.value)}
                      />
                    </Form.Group>
                    <Form.Group>
                      <Form.Control
                        as="textarea"
                        rows={5}
                        placeholder="Mention how can this user help you"
                        className="green-focus"
                        value={helpReason}
                        onChange={(e) => setHelpReason(e.target.value)}
                      />
                      <span className="help-block">
                        Max: 500 chars | Remaining Chars: {500 - helpReason.length}
                      </span>
                    </Form.Group>
                    <hr className="form-horizontal-line" />
                    <Form.Group>
                      <FormControlLabel
                        // id={1}
                        control={<Checkbox color="primary" />}
                        label={"Share Contact Number"}
                        value={shareContact}
                        // checked={causessupported.includes(element._id)}
                        onChange={(e) => {
                          e.target.checked ? setShareContact(1) : setShareContact(0);
                        }}
                      />
                      <FormControlLabel
                        // id={1}
                        control={<Checkbox color="primary" />}
                        label={"Share Email Address"}
                        value={shareEmail}
                        // checked={causessupported.includes(element._id)}
                        onChange={(e) => {
                          e.target.checked ? setShareEmail(1) : setShareEmail(0);
                        }}
                      />
                    </Form.Group>
                    <hr className="form-horizontal-line" />
                    <Form.Group>
                      <button className="ph-btn" type="submit">
                        Save Changes
                      </button>
                    </Form.Group>
                  </Form>
                </div>
              </Modal.Body>
              {/* <Button variant="secondary" onClick={handleClose}>
                  Close
                </Button> */}
            </Modal>
          </>
        ))}
      </Row>

      <nav>
        <ul className="pagination">
          {pageNumber.map((item) => (
            <li key={item} className="page-item">
              <button onClick={() => setCurrentPage(item)} className="page-link">
                {item}
              </button>
            </li>
          ))}
        </ul>
      </nav>
    </>
  );
};

export default SearchPeople;
