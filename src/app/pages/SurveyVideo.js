import React from "react";
import {
  Form,
  Card,
  Container,
  Row,
  Col,
  OverlayTrigger,
  Tooltip,
  Modal,
  Button,
} from "react-bootstrap";
import Question from "../components/Question";
import Example from "../components/Phone";

const SurveyVideo = () => {
  return (
    <Card className="p-5">
    <Question />

    <Form className="p-3">
    <Form.Label style={{color:'#9a9ba5'}}>
           Audio Settings
      </Form.Label>
      <div style={{color:'#9a9ba5'}}>
        <input type="checkbox"></input><label style={{marginLeft:'2%'}}>Mute Audio</label>   
        <OverlayTrigger
          placement="right"
          delay={{ show: 100, hide: 100 }}
          overlay={
            <Tooltip>
              A question keyword is an alias or second name for your
              questions. It makes your data reports simpler to understand and
              more analysis-friendly.
            </Tooltip>
          }
        >
          <i class="fas fa-info-circle" style={{ marginLeft: "10px" }}></i>
        </OverlayTrigger>
      </div>
      <Form.Label style={{color:'#9a9ba5',marginTop:'3%'}}>
        Image Quality
        
      </Form.Label>
      <div style={{color:'#9a9ba5'}}>
        <input type="radio"></input> <label>480p</label>
         <input type="radio" style={{marginLeft:'5%'}}></input> <label>720p</label>
         <input type="radio" style={{marginLeft:'5%'}}></input> <label>1080p</label>  
      </div>

      <p style={{marginTop:'3%'}}>Note:</p>
      <ul>
          <li style={{listStyleType:'circle', marginBottom:'-4%'}}>You can record up to 60 seconds of video to answer this question.</li>
          <li style={{listStyleType:'circle', marginBottom:'-4%'}}>Increasing quality will reduce video duration.</li>
          <li style={{listStyleType:'circle'}}>Video question is available on Collect Android App V2.1.1 and above</li>
      </ul>

      
     
     <hr></hr>
     <i class="fas fa-trash"> Delete</i>
     <i class="fas fa-compress-alt" style={{marginLeft:'2%'}}>Move</i>
     <i class="fas fa-copy" style={{marginLeft:'2%'}}> Copy</i>
         
    </Form>
  </Card>
  );
};

export default SurveyVideo;
