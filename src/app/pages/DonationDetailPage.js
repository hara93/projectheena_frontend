/* eslint-disable eqeqeq */
/* eslint-disable no-restricted-imports */
/* eslint-disable no-mixed-operators */
import React, { useState, useRef } from 'react'
import { Link } from "react-router-dom"
import { Images } from "../config/Images";
import { Card, Row, Col, Form, OverlayTrigger, Tooltip, Table } from "react-bootstrap"
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { RangeStepInput } from "react-range-step-input";
import CustomButton from "../components/CustomButton";
import { useFormik } from "formik";
import { string, object } from "yup";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import moment from "moment";

const DonationDetailPage = () => {
    const [currentTab, setCurrentTab] = useState("details")
    const handleTab = (x) => {
        console.log(x)
        setCurrentTab(x)
    }
    const details = useRef()
    const focusCss = { color: "#525252" }
    const ulTableLi = { display: "table-row", listStyleType: "none", }
    const data = {
        title: "Donation without Excerpts",
        by: {
            name: "Trinayani",
            link: "#"
        },
        img: Images.Donation_detail,
        desc: " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculusmus.Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nullaconsequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a",
        statistics: [
            {
                title: "Creator",
                value: "TRINAYANI",
                link: "#"
            },
            {
                title: "Contact",
                value: "nikhil@projectheena.com",
            },
            {
                title: "Timebound Donation?",
                value: "No",
            },
            {
                title: "Donation Created On",
                value: "14th Feb 2016",
            },
            {
                title: "Ending On",
                value: "This is not time bounded donation",
            }
        ],
        details: [
            {
                title: "Amount to be raised",
                value: "₹100,000"

            },
            {
                title: "Number of Donors",
                value: "2"

            },
            {
                title: "Mininum Donation Required",
                value: "No Minimum Donation Required"

            },
            {
                title: "Team",
                value: "Glenmark",
                link: "#"
            }
        ],
        tags: ['tag1', 'tag2', 'tag3'],
        deliverables: ['80G Certificate', '35AC Certificate'],
        donersList: [
            {
                name: 'demo1',
                img: Images.userlogo
            },
            {
                name: 'demo2',
                img: Images.userlogo
            },
            {
                name: 'demo2',
                img: Images.userlogo
            }
        ]
    }
    const totalAmount = data.details.filter((x) => x.title == "Amount to be raised");
    const [donationAmount, setDonationAmount] = useState(1);
    const handleDonationAmount = (e) => setDonationAmount(e.target.value);
    const isAuthorized = 1;
    const [image, setImage] = useState("");
    const [reply, setReply] = useState();
    const [visibility, setVisibility] = useState("0");
    const [posts, setPosts] = useState([]);
    const [postFeed, setPostFeed] = useState([]);
    const [postNumber, setPostNumber] = useState(0);
    const handleFileSelect = (fileData) => {
        setImage(fileData.currentTarget.files[0]);
    };
    const validationSchema = object().shape({
        update_message: string().required("Post message is required"),
    });
    const onSubmit = (values) => {
        const data = new FormData();
        data.set("image", image);
        // data.set("update_for_id", props.data._id);
        data.set("update_for_type", 1);
        data.set("update_from_type", 1);
        // data.set("update_from_id", user._id);
        // data.set("update_from_name", user.first_name);
        // data.set("update_from_slug", user.first_name);
        data.set("update_message", values.update_message);
        data.set("update_visibility", visibility);
        // console.log("image", data.get("image"));
        // console.log("update_for_id", data.get("update_for_id"));
        // console.log("update_for_type", data.get("update_for_type"));
        // console.log("update_from_type", data.get("update_from_type"));
        // console.log("update_from_id", data.get("update_from_id"));
        // console.log("update_from_name", data.get("update_from_name"));
        // console.log("update_from_slug", data.get("update_from_slug"));
        // console.log("update_message", data.get("update_message"));
        // console.log("update_visibility", data.get("update_visibility"));
        // axios
        //   .post("/task/uploadComment", data)
        //   .then((res) => {
        //     console.log("res", res);
        //     window.location.reload();
        //   })
        //   .catch((err) => console.log("err", err));
    };
    const { handleSubmit, errors, handleChange, touched, values } = useFormik({
        initialValues: { update_message: "" },
        enableReinitialize: true,
        onSubmit,
        validationSchema,
    });
    const handleFav = (ele) => {
        // axios
        //   .post("/task/toggleFav", {
        //     is_favourite: ele.is_favourite,
        //     update_id: ele._id,
        //     task_id: task_id,
        //   })
        //   .then((res) => setPosts(res))
        //   .catch((err) => console.info("err", err));
    };
    const handleReplyChange = (e, idx) => {
        const values = [...postFeed];
        values[idx].reply = e.target.value;
        setPostFeed(values);
    };
    const handleReply = (ele, idx) => {
        var values = {};
        // values.update_for_id = task_id;
        values.update_for_type = 1;
        values.update_from_type = 1;
        // values.update_from_id = user._id;
        // values.update_from_name = user.first_name;
        // values.update_from_slug = user.first_name;
        values.update_message = postFeed[idx].reply;
        values.update_ref_id = ele._id;

        // axios
        //   .post("/task/taskUpdateReply", values)
        //   .then((res) => {
        //     setPosts(res);
        //   })
        //   .catch((err) => console.log("err", err));
        const arr = [...postFeed];
        arr.forEach((ele) => (ele.reply = ""));

        setReply(false);
    };
    const [donersList, setDonersList] = useState([
        {
            first_name: "daksh",
            last_name: "khatri",
            email_id: "daksh@gmail.com",
            user_contact_number: "9769976979"
        },
        {
            first_name: "daksh",
            last_name: "khatri",
            email_id: "daksh@gmail.com",
            user_contact_number: "9769976979"
        },
        {
            first_name: "daksh",
            last_name: "khatri",
            email_id: "daksh@gmail.com",
            user_contact_number: "9769976979"
        }
    ])
    const [transactionLog, setTransactionLog] = useState([
        {
            name: "umang",
            link: "#",
            amount: 10,
        }, {
            name: "umang",
            link: "#",
            amount: 10,
        }, {
            name: "umang",
            link: "#",
            amount: 10,
        }, {
            name: "umang",
            link: "#",
            amount: 10,
        }
    ])
    return (
        <div className='container'>
            <div className="donation-header">
                <h1 className="donation-title">{data.title}</h1>
                <div className="donation-creator">
                    By{" "}
                    <Link to={data.by.link} style={{ color: "#4CAF50" }}>{data.by.name}</Link>
                </div>
            </div>
            <div style={{ paddingBottom: "5px", paddingLeft: "15px" }}>
                <ul className="ph-nav-bar tabs" style={{ marginTop: "-2%" }}>
                    <li>
                        <Link href="#terms" className="active" onClick={() => handleTab("details")} ref={details}>
                            <div>
                                <i className="fa fa-newspaper-o active-focus  p-r-5" style={currentTab === 'details' && focusCss || {}} ></i>
                                <span className="active-focus" style={currentTab === 'details' && focusCss || {}} >
                                    DETAILS
                                </span>
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link href="#terms" className="active" onClick={() => handleTab("updates")} >
                            <div>
                                <i className="fa fa-newspaper-o active-focus  p-r-5" style={currentTab === 'updates' && focusCss || {}} ></i>
                                <span className="active-focus" style={currentTab === 'updates' && focusCss || {}} >
                                    UPDATES
                                </span>
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link href="#terms" className="active" onClick={() => handleTab("doners")} >
                            <div>
                                <i className="fa fa-newspaper-o active-focus  p-r-5" style={currentTab === 'doners' && focusCss || {}} ></i>
                                <span className="active-focus" style={currentTab === 'doners' && focusCss || {}} >
                                    DONERS
                                </span>
                            </div>
                        </Link>
                    </li>
                    {isAuthorized && <li>
                        <Link href="#terms" className="active" onClick={() => handleTab("reports")} >
                            <div>
                                <i className="fa fa-newspaper-o active-focus  p-r-5" style={currentTab === 'reports' && focusCss || {}} ></i>
                                <span className="active-focus" style={currentTab === 'reports' && focusCss || {}} >
                                    REPORTS
                                </span>
                            </div>
                        </Link>
                    </li> || null}

                </ul>
            </div>
            <Card style={{ paddingTop: "15px", borderRadius: "0", }}>
                <Row style={{ margin: "0" }}>
                    <Col md={8} style={{ padding: "0 15px" }}>
                        {currentTab === 'details' && <>
                            <div className="donation-img">
                                <img
                                    className="p-1"
                                    src={data.img}
                                    alt=""
                                    style={{ width: "100%" }}
                                ></img>
                            </div>
                            <div className="donation-desc">
                                {data.desc}
                            </div>
                            <div style={{ marginBottom: "25px" }}>
                                <div className="box-ibox-title">
                                    <h3 style={{ fontSize: "16px", fontWeight: "600" }}>Statistics</h3>
                                </div>
                                <div style={{ padding: "15px 20px 20px 20px", border: "1px solid #e7eaec" }} >
                                    <ul className="donation-stats">
                                        {data.statistics.map((x, index) => (
                                            <li key={index} style={ulTableLi} className='title-gray' >
                                                <div className="th">{x.title}</div>
                                                <div className="tc">{x.link && <Link to={x.link} className='basecolor'>{x.value} </Link> || x.value}  </div>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                        </>
                        }
                        {currentTab === 'updates' && <>
                            <div className="box-ibox-title">
                                <h3 style={{ fontSize: "16px", fontWeight: "600" }}>Post New Update</h3>
                            </div>
                            <div style={{ padding: "15px 20px 20px 20px", border: "1px solid #e7eaec" }} >
                                <Form onSubmit={handleSubmit}>
                                    <Form.Group>
                                        <Form.Row style={{ margin: "10px 0" }}>
                                            <Form.Control
                                                name="update_message"
                                                as="textarea"
                                                placeholder="Share any update or task status with fellow volunteers"
                                                className="green-focus"
                                                onChange={handleChange}
                                                error={errors.update_message}
                                                value={values.update_message}
                                                touched={touched.update_message}
                                            />
                                            {touched && errors.update_message && (
                                                <span className="text-danger" style={{ padding: "5px 0" }}>
                                                    {errors.update_message}
                                                </span>
                                            )}
                                        </Form.Row>
                                        <Form.Row style={{ margin: "10px 0" }}>
                                            <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                                                Attach File
                                            </Form.Label>
                                            <Col className="form-input-align-center">
                                                <Form.Group style={{ marginBottom: "0" }}>
                                                    <input name="image" type="file" onChange={handleFileSelect} />
                                                </Form.Group>
                                            </Col>
                                        </Form.Row>
                                        <Form.Row style={{ margin: "10px 0" }}>
                                            <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
                                                Visibility
                                            </Form.Label>
                                            <Col className='d-flex justify-content-between' style={{ alignItems: "normal" }} >
                                                <RadioGroup aria-label="gender" name="milestone">
                                                    <div>
                                                        <FormControlLabel
                                                            value="0"
                                                            control={<Radio color="primary" />}
                                                            label="Everyone"
                                                            onChange={(e) => setVisibility(e.target.value)}
                                                        />
                                                        <FormControlLabel
                                                            value="1"
                                                            control={<Radio color="primary" />}
                                                            label="Logged in"
                                                            onChange={(e) => setVisibility(e.target.value)}
                                                        />
                                                        <FormControlLabel
                                                            value="2"
                                                            control={<Radio color="primary" />}
                                                            label="Engaged Volunteers"
                                                            onChange={(e) => setVisibility(e.target.value)}
                                                        />
                                                    </div>
                                                </RadioGroup>
                                                <div style={{ fontSize: "11px", maxWidth: "160px", paddingTop: "10px" }}>
                                                    <span>
                                                        Some updates are public, some available to logged in users and others
                                                        available to the ones who have engaged on the activity
                                                    </span>
                                                </div>
                                            </Col>
                                        </Form.Row>
                                        <Form.Row>
                                            <Form.Label column="lg" lg={2}></Form.Label>
                                            <Col className="form-input-align-center">
                                                <button className="post-update-btn" type="submit">
                                                    {" "}
                                                    Post Update
                                                </button>
                                            </Col>
                                        </Form.Row>
                                    </Form.Group>
                                </Form>
                            </div>
                            <div>
                                {posts.length > 0
                                    ? posts
                                        .filter((filt) => filt.update_ref_id == undefined)
                                        .map((ele, idx) => (
                                            <Row style={{ margin: "20px 0" }}>
                                                <Col md={2}>
                                                    <div style={{ textAlign: "center" }}>
                                                        <div style={{ display: "flex", justifyContent: "center" }}>
                                                            {" "}
                                                            <img
                                                                height="48px"
                                                                width="48px"
                                                                className="img-circle"
                                                                src={Images.userlogo}
                                                                alt="himanshu chanda"
                                                            ></img>
                                                        </div>
                                                        <span style={{ whiteSpace: "nowrap", fontSize: "12px", fontWeight: "600" }}>
                                                            {ele.update_from_name}
                                                        </span>
                                                        <span style={{ whiteSpace: "nowrap", fontSize: "11px" }}>
                                                            {moment(ele.activity_date_time)
                                                                .startOf("hour")
                                                                .fromNow()}
                                                        </span>
                                                    </div>
                                                </Col>
                                                <Col>
                                                    <div>
                                                        <p>{ele.update_message}</p>
                                                    </div>
                                                    <div
                                                        style={{ display: "flex", justifyContent: "flex-end", color: "#4caf4f" }}
                                                    >
                                                        <div className="pointer" onClick={() => setReply(idx)}>
                                                            <i
                                                                class="fas fa-reply"
                                                                style={{ paddingRight: "5px", color: "#4caf4f", fontSize: "13px" }}
                                                            ></i>
                                                            Reply{" "}
                                                        </div>
                                                        {ele.is_favourite === 1 ? (
                                                            <div
                                                                className="isFav"
                                                                onClick={() => {
                                                                    handleFav(ele);
                                                                }}
                                                            >
                                                                <i class="fas fa-star base-color"></i>
                                                            </div>
                                                        ) : (
                                                            <div
                                                                className="isFav"
                                                                onClick={() => {
                                                                    handleFav(ele);
                                                                }}
                                                            >
                                                                <i class="far fa-star base-color"></i>
                                                            </div>
                                                        )}
                                                    </div>
                                                    {reply === idx ? (
                                                        <div style={{ margin: "10px 0" }}>
                                                            <Form.Control
                                                                type="text"
                                                                placeholder="Your reply here"
                                                                className="green-focus"
                                                                style={{ height: "50px", margin: "10px 0" }}
                                                                onChange={(e) => handleReplyChange(e, idx)}
                                                                value={postFeed[idx].reply}
                                                            />
                                                            <div style={{ display: "flex", justifyContent: "flex-end" }}>
                                                                <button className="reply-btn" onClick={() => handleReply(ele, idx)}>
                                                                    Reply
                                                                </button>
                                                            </div>
                                                        </div>
                                                    ) : null}
                                                    {ele.filename != null ? (
                                                        <div>
                                                            <div style={{ border: "1px solid #ddd", width: "fit-content" }}>
                                                                <div>
                                                                    <img
                                                                        src={`http://localhost:5000/uploads/images/task/${ele.filename}`}
                                                                        height="100px"
                                                                        width="200px"
                                                                        alt="post"
                                                                    ></img>
                                                                </div>
                                                                <div style={{ padding: "10px", backgroundColor: "#f8f8f8" }}>
                                                                    <span className="base-color">{ele.original_name}</span>
                                                                    <br />
                                                                    <br />
                                                                    <span>
                                                                        Added : {moment(ele.activity_date_time).format("Do MMM, YYYY")}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ) : null}
                                                    {posts
                                                        .filter((reply) => ele._id === reply.update_ref_id)
                                                        .map((ele, idx) => (
                                                            <div
                                                                style={{
                                                                    backgroundColor: "#f8f8f8",
                                                                    display: "flex",
                                                                    justifyContent: "space-between",
                                                                    border: "1px solid #ddd",
                                                                    padding: "10px",
                                                                    margin: "10px 0",
                                                                }}
                                                            >
                                                                <span>{ele.update_message}</span>

                                                                <div style={{ textAlign: "center", width: "130px" }}>
                                                                    <div style={{ display: "flex", justifyContent: "center" }}>
                                                                        {" "}
                                                                        <img
                                                                            height="48px"
                                                                            width="48px"
                                                                            className="img-circle"
                                                                            src={Images.user_logo}
                                                                            alt="himanshu chanda"
                                                                        ></img>
                                                                    </div>
                                                                    <span
                                                                        style={{
                                                                            whiteSpace: "nowrap",
                                                                            fontSize: "12px",
                                                                            fontWeight: "600",
                                                                        }}
                                                                    >
                                                                        {ele.update_from_name}
                                                                    </span>
                                                                    <br />
                                                                    <span style={{ whiteSpace: "nowrap", fontSize: "11px" }}>
                                                                        {moment(ele.activity_date_time)
                                                                            .startOf("hour")
                                                                            .fromNow()}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        ))}
                                                </Col>
                                            </Row>
                                        ))
                                    : null}
                            </div>
                        </>}
                        {currentTab === 'doners' && <>
                            <strong>
                                <p className="pl-1 pt-4" style={{ color: "#676A6C", fontSize: 20 }}>
                                    Doners Details
                                </p>
                            </strong>
                            <Row className="px-4">
                                <table className="custom-table table-bordered" bordered size="sm">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Volunteer Name</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {donersList.map((ele, idx) => (
                                            <tr>
                                                <td>{idx + 1}</td>
                                                <td>{`${ele.first_name} ${ele.last_name}`}</td>
                                                <td>{ele.email_id}</td>
                                                <td>{ele.user_contact_number}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </Row>
                            <p className="pl-1" style={{ color: "grey" }}>
                                <span className='basecolor'>Click here</span> to download doner details
                            </p>
                        </>}
                        {currentTab === 'reports' && isAuthorized && <>
                            <div style={{ marginBottom: "30px" }}>
                                <h3 className='fw-bold mb-2' style={{ fontSize: "20px" }} >
                                    Reports
                                </h3>
                                <div style={{ padding: "15px 20px 20px 20px", borderBottom: "1px solid #e7eaec" }} >
                                    <div className="no-content">
                                        <p style={{ color: "#c4c4c4", fontSize: "20px" }} className='p-4'>
                                            No reports about this Donation Drive has yet been made public.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div style={{ marginBottom: "30px" }}>
                                <h3 className='fw-bold mb-2' style={{ fontSize: "20px" }}>
                                    Transaction Log
                                </h3>
                                <Table striped hover size="sm">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Donated</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {transactionLog.map((x, index) => (
                                            <tr style={{ fontSize: "13px" }} className='title-gray'>
                                                <td> <Link to={x.link} className='basecolor'> {x.name} </Link> </td>
                                                <td>&#8377;{x.amount}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </div>
                        </>}
                    </Col>
                    <Col md={4}>
                        <div style={{ marginBottom: "30px" }}>
                            <h3 style={{ fontSize: "16px" }} className='fw-bold'>Details</h3>
                            <ul className="donation-stats">
                                {data.details.map((x, index) => (
                                    <li style={ulTableLi} key={index} className='title-gray'>
                                        <div className="th">{x.title}</div>
                                        <div className="tc"> {x.link && <Link to="" className='basecolor'>{x.value}</Link> || x.title == 'Amount raised' ? `${x.value}` : x.value}</div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div style={{ marginBottom: "30px" }}>
                            <h3 style={{ fontSize: "16px" }} className='fw-bold'>Tags</h3>
                            {data.tags && data.tags.length > 0 && data.tags.map((x, index) => (
                                <span className="p-2 task-tags-skill">
                                    {x}
                                </span>
                            )) || <p>No Tags Supported</p>
                            }
                        </div>
                        <div style={{ marginBottom: "30px" }}>
                            <h3 className='fw-bold mb-3' style={{ fontSize: "20px" }} >
                                Deliverables
                            </h3>
                            {data.deliverables.map((x, index) => (
                                <span key={index} className="deliverables-tags" >
                                    {x}
                                </span>
                            ))}
                        </div>
                        {isAuthorized && <div style={{ marginBottom: "30px" }}>
                            <h3 className='fw-bold mb-3' style={{ fontSize: "20px" }} >
                                Donation Box
                            </h3>
                            <Form>
                                <Form.Group>
                                    <Form.Row>
                                        <Form.Label column="lg">
                                            I wish to donate:{" "}
                                            <span>{donationAmount}</span>
                                        </Form.Label>
                                    </Form.Row>
                                    <Form.Control
                                        type="number"
                                        value={donationAmount}
                                        className="green-focus"
                                    />
                                    <div style={{ margin: "10px 0" }}>
                                        <RangeStepInput
                                            min={1}
                                            max={50000}
                                            value={donationAmount}
                                            step={1}
                                            onChange={(e) => handleDonationAmount(e)}
                                        />
                                    </div>
                                    <div>
                                        <FormControlLabel
                                            control={<Checkbox color="primary" />}
                                            label="Keep this donation as private"
                                            value="private"
                                            style={{ margin: "0" }}
                                        />
                                        <OverlayTrigger
                                            placement="right"
                                            delay={{ show: 100, hide: 100 }}
                                            overlay={
                                                <Tooltip>
                                                    If You select this option, Your contributon will not
                                                    be publicly visible on your KARMA profile. This
                                                    cannot be changed later (Not recommended
                                                </Tooltip>
                                            }
                                        >
                                            <i className="fas fa-info-circle title-gray mx-2" ></i>
                                        </OverlayTrigger>
                                    </div>
                                    <CustomButton content="Donate" />
                                    <hr className="form-horizontal-line" />
                                </Form.Group>
                            </Form>
                        </div> || <div style={{ marginBottom: "30px" }}>
                                <p>You need to be Logged in to Donate for this Cause</p>
                                <Link to="/signup" className='basecolor'> Login here </Link>
                            </div>}




                        <div style={{ marginBottom: "30px" }}>
                            <h3 className='mb-3 fw-bold' style={{ fontSize: "20px" }} >
                                Following people are donating
                            </h3>
                            <ul className='m-0 p-0 d-flex'>
                                {data.donersList.map((x, index) => (
                                    <li style={{ listStyleType: "none" }} key={index} className='p-1'>
                                        <Link to="" className="donor">
                                            <img
                                                src={x.img}
                                                style={{ border: "1px solid #c4c4c4" }}
                                                className='rounded-circle'
                                                alt=""
                                                height="75px"
                                            ></img>
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </Col>
                </Row>
            </Card>
        </div>
    )
}

export default DonationDetailPage
