/* eslint-disable no-restricted-imports */
import React from "react";
import { Row, Col, Table, Modal, Button } from "react-bootstrap";
import * as ReactBootstrap from "react-bootstrap";
import { Card } from "@material-ui/core";
import { Images } from "../config/Images";
// import './MyPage.css';

import ViewDetail from "../components/ViewDetails";
import Initiatives from "../components/Initiatives";
import CustomButton from "../components/CustomButton";
import CustomButtonOutline from "../components/CustomButtonOutline";
import VerticalLinearStepper from "../components/VerticalStepper";
import TweetEmbed from "react-tweet-embed";
import LinearDeterminate from "../components/ProgressBar";
import { VerticalTimeline, VerticalTimelineElement } from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import ProgressBar from "react-bootstrap/ProgressBar";
import axios from "../config/axios";
import { FaFacebook, FaTwitterSquare, FaLinkedin, FaPencilAlt } from "react-icons/fa";
import { connect } from "react-redux";

class UserProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: false,
      score: 0,
      taskcount: 0,
      show: false,
      show_for_email: false,
      show_for_seek_help: false,
      show_for_karma_points: false,
      MyUserProfile: this.props.user,
      notMe: this.props.location.state.notMe,
      user_id: "",
    };
  }
  componentDidMount() {
    // this.setState({ ...this.state, MyUserProfile: this.props.user });
    console.log("is me?", this.props.location.state);

    if (this.state.notMe) {
      //another account

      var user_id = this.props.otherUser._id;
    } else {
      var user_id = this.state.MyUserProfile._id;
    }

    axios.post("user/profilescore", { user_id }).then(({ data }) => {
      console.log("data", data);
      this.setState({ ...this.state, score: data.scorecount, taskcount: data.taskcount });
    });
  }
  handleClick = () => {
    this.setState({ ...this.state, users: !this.state.users });
  };
  handleClose = () => {
    this.setState({ ...this.state, show: false });
  };
  handleShow = () => {
    this.setState({ ...this.state, show: true });
  };
  handleModalForEmailShow = () => {
    this.setState({ ...this.state, show_for_email: true });
  };
  handleModalForEmailClose = () => {
    this.setState({ ...this.state, show_for_email: false });
  };
  handleModalForSeekHelpShow = () => {
    this.setState({ ...this.state, show_for_seek_help: true });
  };
  handleModalForSeekHelpClose = () => {
    this.setState({ ...this.state, show_for_seek_help: false });
  };
  handleModalForKarmaPointsShow = () => {
    this.setState({ ...this.state, show_for_karma_points: true });
  };
  handleModalForKarmaPointsClose = () => {
    this.setState({ ...this.state, show_for_karma_points: false });
  };
  handleRedirectToEdit = () => {
    console.log("clicked");
    this.props.history.push("/editProfileUser");
  };
  render() {
    const now = this.state.score;
    return (
      <>
        <div className="container">
          {/* <img className="user_profile_pic" src={Images.Social_work}></img> */}
          <img
            className="user_profile_pic"
            src={
              !this.props.user.image || this.props.user.image == ""
                ? Images.Social_work
                : `http://localhost:5000/uploads/images/user/${this.props.user.image}`
            }
          ></img>

          <h1 className="user_profile_name mb-3">
            {`${this.props.user?.first_name} ${this.props.user?.last_name}`}
            <FaPencilAlt
              onClick={this.handleRedirectToEdit}
              style={{ cursor: "pointer", color: "#4caf50", fontSize: "28px" }}
              className="ml-3 mb-1"
            />
          </h1>
          <div className="user_description" style={{ marginTop: 30 }}>
            <h4>Awesome Volunteer, Project Heena | Navi Mumbai, Maharashtra, India</h4>
          </div>
          <div className="follow_button" style={{ marginTop: 45 }}>
            <CustomButton content={`Follow`} onClick={this.handleShow} type="submit" />
          </div>
          <Card style={{ paddingTop: "10%", marginTop: "10%" }}>
            <Row></Row>
            <Row className="p-3">
              <Col md={7}>
                {!this.props.user?.details || this.props.user?.details == "" ? null : (
                  <div>
                    <h3>About Me</h3>
                    <p>{this.props.user?.details}</p>
                  </div>
                )}
                <h3 style={{ marginTop: "6%", color: "#676A6C", fontWeight: "600" }}>
                  Testimonials
                </h3>
                <p>
                  {this.props.user?.first_name + " " + this.props.user?.last_name} is yet to
                  Contribute on ProjectHeena. Why not seek help from him/her and ask to collaborate
                  for your task
                </p>
                {/* <div className="timeline">
                                    <div className="container container-right">
                                        <div className="image" style={{ backgroundImage: `url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWTN5jNTuXqxXWTOyY8s3wLzDG1d-qUe3q7Q&usqp=CAU")` }}></div>
                                        <div className="content">

                                            <h5 style={{ color: '#4caf4f', fontWeight: '600' }}>Volunteer at the Standard Chartered Mumbai Marathon</h5>
                                            <p>
                                                Et hinc magna voluptatum usu, cum veniam graece et. Ius ea scripserit temporibus, pri cu harum tacimates neglegentur. At adipisci incorrupte nam. Cu qui sumo appareat constituto.
                                            </p>
                                            <p>Donated 6 hours</p>
                                        </div>

                                    </div>
                                    <div className="container container-right" style={{ marginTop: '5%' }}>
                                        <div className="image" style={{ backgroundImage: `url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWTN5jNTuXqxXWTOyY8s3wLzDG1d-qUe3q7Q&usqp=CAU")` }}></div>
                                        <div className="content">

                                            <h5 style={{ color: '#4caf4f', fontWeight: '600' }}>Volunteer at the Standard Chartered Mumbai Marathon</h5>
                                            <p>
                                                Et hinc magna voluptatum usu, cum veniam graece et. Ius ea scripserit temporibus, pri cu harum tacimates neglegentur. At adipisci incorrupte nam. Cu qui sumo appareat constituto.
                                            </p>
                                            <p>Donated 6 hours</p>
                                        </div>

                                    </div>
                                    <div className="container container-right" style={{ marginTop: '5%' }}>
                                        <div className="image" style={{ backgroundImage: `url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWTN5jNTuXqxXWTOyY8s3wLzDG1d-qUe3q7Q&usqp=CAU")` }}></div>
                                        <div className="content">

                                            <h5 style={{ color: '#4caf4f', fontWeight: '600' }}>Volunteer at the Standard Chartered Mumbai Marathon</h5>
                                            <p>
                                                Et hinc magna voluptatum usu, cum veniam graece et. Ius ea scripserit temporibus, pri cu harum tacimates neglegentur. At adipisci incorrupte nam. Cu qui sumo appareat constituto.
                                            </p>
                                            <p>Donated 6 hours</p>
                                        </div>

                                    </div>
                                </div> */}
                <h3 style={{ marginTop: "6%", fontWeight: "600" }}>My Tasks</h3>
                {this.state.taskcount > 0 ? (
                  <div style={{ marginTop: "3%" }}>
                    <a
                      href="#"
                      style={{
                        textDecoration: "none",
                        color: "#4CAF50",
                        marginTop: "4vh",
                        marginLeft: "3vw",
                        paddingTop: "4vh",
                      }}
                    >
                      Campus Ambassador required for ProjectHeena
                    </a>
                    <hr
                      style={{
                        backgroundColor: "lightgray",
                        marginLeft: "3vw",
                      }}
                    />
                  </div>
                ) : (
                  <p>No tasks created yet</p>
                )}
              </Col>
              <Col md={5}>
                <div>
                  <h3 style={{ fontWeight: "600" }}>Profile Completion Score</h3>
                  <ProgressBar animated now={now} label={`${now}%`} variant="success" />
                  <h3 style={{ marginTop: "6vh", marginBottom: "4vh", fontWeight: "600" }}>
                    Details
                  </h3>
                  <table>
                    <tbody>
                      <tr>
                        <td className="pr-3">
                          <div
                            style={{
                              border: "1px solid #ebebeb",
                              borderTop: "none",
                              borderLeft: "none",
                              padding: 5,
                            }}
                          >
                            <i
                              className="fa fa-map-marker p-2 text-secondary"
                              style={{ fontSize: "12px" }}
                            ></i>
                          </div>
                        </td>
                        <td>
                          {!this.props.user?.current_location ||
                            this.props.user?.current_location == ""
                            ? "Not Shared"
                            : this.props.user?.current_location}
                        </td>
                      </tr>
                      <tr>
                        <td className="pr-3">
                          <div
                            style={{
                              border: "1px solid #ebebeb",
                              borderTop: "none",
                              borderLeft: "none",
                              padding: 5,
                            }}
                          >
                            <i
                              className="fa fa-venus-mars p-2 text-secondary"
                              style={{ fontSize: "12px" }}
                            ></i>
                          </div>
                        </td>
                        <td className="user_detail_cell">{this.props.user?.gender}</td>
                      </tr>
                      <tr>
                        <td className="pr-3">
                          <div
                            style={{
                              border: "1px solid #ebebeb",
                              borderTop: "none",
                              borderLeft: "none",
                              padding: 5,
                            }}
                          >
                            <i
                              className="fa fa-calendar p-2 text-secondary"
                              style={{ fontSize: "12px" }}
                            ></i>
                          </div>
                        </td>
                        <td className="user_detail_cell">
                          {!this.props.user?.dob || this.props.user?.dob == ""
                            ? "Not Shared"
                            : this.props.user?.dob}
                        </td>
                      </tr>
                      <tr>
                        <td className="pr-3">
                          <div
                            style={{
                              border: "1px solid #ebebeb",
                              borderTop: "none",
                              borderLeft: "none",
                              padding: 5,
                            }}
                          >
                            <i
                              className="fa fa-share p-2 text-secondary"
                              style={{ fontSize: "12px" }}
                            ></i>
                          </div>
                        </td>
                        <td className="text-secondary">
                          <strong>Available on</strong> : Weekends <strong>Time</strong> : Flexible
                        </td>
                      </tr>
                      <tr>
                        <td className="pr-3">
                          <div
                            style={{
                              border: "1px solid #ebebeb",
                              borderTop: "none",
                              borderLeft: "none",
                              padding: 5,
                            }}
                          >
                            <i
                              className="fa fa-chain p-2 text-secondary"
                              style={{ fontSize: "12px" }}
                            ></i>
                          </div>
                        </td>
                        <td className="user_detail_cell">
                          <a href="http://projectheena.com" style={{ color: "#4caf50" }}>
                            http://projectheena.com
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td className="pr-3">
                          <div
                            style={{
                              border: "1px solid #ebebeb",
                              borderTop: "none",
                              borderLeft: "none",
                              padding: 5,
                            }}
                          >
                            <i
                              class="fas fa-external-link-alt p-2 text-secondary"
                              style={{ fontSize: "12px" }}
                            ></i>
                          </div>
                        </td>
                        <td>
                          <tr>
                            <td>
                              <i
                                style={{
                                  border: "1px solid #ebebeb",
                                  borderTop: "none",
                                  borderLeft: "none",
                                  padding: 7,
                                }}
                                className="pr-3"
                              >
                                <FaFacebook
                                  color="#4caf50"
                                  background-color="white"
                                  style={{ fontSize: 12 }}
                                />
                              </i>
                              <a
                                style={{
                                  color: "#4caf50",
                                  paddingLeft: 5,
                                  paddingRight: 5,
                                }}
                                href={
                                  !this.props.user?.fb_link || this.props.user?.fb_link == ""
                                    ? ""
                                    : this.props.user?.fb_link
                                }
                              >
                                Facebook
                              </a>
                            </td>
                            <td>
                              <i
                                style={{
                                  border: "1px solid #ebebeb",
                                  borderTop: "none",
                                  borderLeft: "none",
                                  padding: 7,
                                }}
                                className="pr-3"
                              >
                                <FaTwitterSquare
                                  color="#4caf50"
                                  background-color="white"
                                  style={{ fontSize: 12 }}
                                />
                              </i>
                              <a
                                style={{
                                  color: "#4caf50",
                                  paddingLeft: 5,
                                  paddingRight: 5,
                                }}
                                href={
                                  !this.props.user?.twitter_handle ||
                                    this.props.user?.twitter_handle == ""
                                    ? ""
                                    : this.props.user?.twitter_handle
                                }
                              >
                                Twitter
                              </a>
                            </td>
                            <td>
                              <i
                                style={{
                                  border: "1px solid #ebebeb",
                                  borderTop: "none",
                                  borderLeft: "none",
                                  padding: 7,
                                }}
                                className="pr-3"
                              >
                                <FaLinkedin
                                  color="#4caf50"
                                  background-color="white"
                                  style={{ fontSize: 12 }}
                                />
                              </i>
                              <a
                                style={{
                                  color: "#4caf50",
                                  paddingLeft: 5,
                                  paddingRight: 5,
                                }}
                                href={
                                  !this.props.user?.linkedin_link ||
                                    this.props.user?.linkedin_link == ""
                                    ? ""
                                    : this.props.user?.linkedin_link
                                }
                              >
                                LinkedIn
                              </a>
                            </td>
                          </tr>
                        </td>
                      </tr>
                      <tr>
                        <td className="pr-3">
                          <div
                            style={{
                              border: "1px solid #ebebeb",
                              borderTop: "none",
                              borderLeft: "none",
                              padding: 5,
                            }}
                          >
                            <i
                              className="fa fa-envelope p-2 text-secondary"
                              style={{ fontSize: "12px" }}
                            ></i>
                          </div>
                        </td>
                        <td>
                          <a onClick={this.handleModalForEmailShow} style={{ color: "#4caf50" }}>
                            Contact User by sending an Email
                          </a>
                        </td>
                      </tr>
                      {/* <tr>
                                                <td className="user_detail_cell">
                                                    <i className="fa fa-share-alt p-2" style={{ borderBottom: '1px solid #ebebeb', borderRight: '1px solid #ebebeb' }}></i>
                                                </td>
                                                <td className="user_detail_cell">{(!this.props.user?.dob || this.props.user?.dob == '') ? 'Not Shared' : this.props.user?.dob}</td>
                                            </tr> */}
                    </tbody>
                  </table>
                </div>
                <div>
                  <h3 style={{ marginTop: "6vh", marginBottom: "2vh", fontWeight: "600" }}>
                    Achievements
                  </h3>
                  <table className="text-secondary" style={{ borderCollapse: "collapse" }}>
                    <tbody>
                      <tr>
                        <td style={{ width: "40%", borderBottom: "1px solid #dddddd", padding: "8px 0" }}>Hours Given</td>
                        <td style={{ borderBottom: "1px solid #dddddd", padding: "8px 0" }}>
                          {!this.props.user?.user_hours || this.props.user?.user_hours == ""
                            ?
                            <span>
                              {`${this.props.user?.first_name}` + ` is yet to Contribute Why not `}
                              <span onClick={this.handleModalForSeekHelpShow} style={{ color: "#4caf50", cursor: "pointer" }}>ask him/her to help you</span>
                            </span>
                            : `${this.props.user?.user_hours} Hours`}
                        </td>
                      </tr>
                      <tr>
                        <td style={{ width: "40%", borderBottom: "1px solid #dddddd", padding: "8px 0" }}>Tasks Created</td>
                        <td style={{ borderBottom: "1px solid #dddddd", padding: "8px 0" }}>{this.state.taskcount > 0 ? this.state.taskcount : "No Tasks Created So Far"}</td>
                      </tr>
                      <tr>
                        <td style={{ padding: "8px 0", width: "40%" }}>Karma Points</td>
                        <td
                          style={{ padding: "8px 0", cursor: "pointer" }}
                          onClick={this.handleModalForKarmaPointsShow}
                        >
                          0 <img style={{ width: "20%" }} alt="coins" src={Images.coins}></img>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                {/* <div>
                  <h3 style={{ marginTop: "5vh", marginBottom: "2vh", fontWeight: "600" }}>
                    Preferred Cause
                  </h3>
                  <div>
                    {this.props.user.user_causes && this.props.user.user_causes.length > 0
                      ? this.props.user.user_causes.map((element, index) => (
                          <button
                            className="p-1 mx-1"
                            style={{
                              backgroundColor: "#4caf4f",
                              color: "white",
                              border: "1px solid #4caf4f",
                              marginRight: "1%",
                              marginBottom: "1%",
                            }}
                          >
                            {element.user_cause_disc}
                          </button>
                        ))
                      : `No causes added yet!!`}
                    <br />
                  </div>
                </div> */}
                <div>
                  <button
                    onClick={this.handleModalForSeekHelpShow}
                    className="get_help"
                    style={{ marginTop: "5vh", marginBottom: "2vh" }}
                  >
                    Get Help From {`${this.props.user?.first_name} ${this.props.user?.last_name}`}
                  </button>
                </div>
                <div>
                  <h3 style={{ marginTop: "5vh", marginBottom: "2vh", fontWeight: "600" }}>
                    Skills
                  </h3>
                  {this.props.user.user_skills && this.props.user.user_skills.length > 0
                    ? this.props.user.user_skills.map((element, index) => (
                      <button
                        className="p-2 mx-1"
                        style={{
                          backgroundColor: "#ff8a64",
                          color: "white",
                          border: "1px solid #ff8a64",
                          marginRight: "1%",
                          marginBottom: "1%",
                        }}
                      >
                        {element.user_skill_desc}
                      </button>
                    ))
                    : `No Skills Added yet!! Why not contact ${this.props.user?.first_name} ${this.props.user?.last_name} by clicking Contact link above`}

                  <div>
                    <h3 style={{ marginTop: "5vh", marginBottom: "2vh", fontWeight: "600" }}>
                      Similar Users
                    </h3>
                    No Similar Users to show. You can Search here for users with specific skills,
                    location
                    {/* <div class="container1" style={{ display: 'inline-block' }}>
                                            <img src={Images.user_logo} alt="Avatar" class="image1" style={{
                                                borderRadius: "50%",
                                                width: "70%",
                                                marginLeft: '3%'
                                            }} />
                                            <div class="overlay1" style={{ display: 'inline-block' }}>
                                                <div class="text1" style={{ display: 'inline-block' }}>Daksh Khatri</div>
                                            </div>
                                        </div> */}
                  </div>
                  {/* {
                                        this.state.users == true ?
                                            <>
                                                <div style={{ marginTop: '3%' }}>
                                                    <a onClick={this.handleClick} style={{ paddingLeft: '29%', color: '#4caf4f' }}><i class="fas fa-arrow-up" style={{ color: '#4caf4f' }}></i> Show less </a>
                                                </div>
                                            </> : <div style={{ marginTop: '3%' }}>
                                                <a onClick={this.handleClick} style={{ paddingLeft: '29%', color: '#4caf4f' }}><i class="fas fa-arrow-down" style={{ color: '#4caf4f' }}></i> Show more </a>
                                            </div>
                                    } */}
                </div>
              </Col>
            </Row>
          </Card>
        </div>
        {/* <div>
          <h3 style={{ marginTop: "5vh", marginBottom: "2vh", fontWeight: "600" }}>
            Preferred Cause
          </h3>
          <div>
            {this.props.user.user_causes && this.props.user.user_causes.length > 0
              ? this.props.user.user_causes.map((element, index) => (
                  <button
                    className="p-1 mx-1"
                    style={{
                      backgroundColor: "#4caf4f",
                      color: "white",
                      border: "1px solid #4caf4f",
                      marginRight: "1%",
                      marginBottom: "1%",
                    }}
                  >
                    {element.user_cause_disc}
                  </button>
                ))
              : `No causes added yet!!`}
            <br />
          </div>
        </div> */}

        <Modal show={this.state.show} onHide={this.handleClose} style={{ opacity: "1" }} size="lg">
          <Modal.Header closeButton>
            <Modal.Title>
              Follow {`${this.props.user?.first_name} ${this.props.user?.last_name}`}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>You cannot follow yourself.</Modal.Body>
        </Modal>
        <Modal
          show={this.state.show_for_email}
          onHide={this.handleModalForEmailClose}
          style={{ opacity: "1" }}
          size="lg"
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Contact {`${this.props.user?.first_name} ${this.props.user?.last_name}`}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>You cannot send message to yourself.</Modal.Body>
        </Modal>
        <Modal
          show={this.state.show_for_seek_help}
          onHide={this.handleModalForSeekHelpClose}
          style={{ opacity: "1" }}
          size="lg"
        >
          <Modal.Header closeButton>
            <Modal.Title>Seek Help</Modal.Title>
          </Modal.Header>
          <Modal.Body>You cannot seek help from yourself.</Modal.Body>
        </Modal>
        <Modal
          show={this.state.show_for_karma_points}
          onHide={this.handleModalForKarmaPointsClose}
          style={{ opacity: "1" }}
          size="lg"
        >
          <Modal.Header closeButton>
            <Modal.Title>Karma Points Breakdown</Modal.Title>
          </Modal.Header>
          <Modal.Body>You cannot seek help from yourself.</Modal.Body>
          <Modal.Footer>
            <button
              style={{ backgroundColor: "WhiteSmoke", padding: 10, border: "none" }}
              onClick={this.handleModalForKarmaPointsClose}
            >
              Close
            </button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = function (state) {
  console.log(state);
  return {
    user: state.phAuth.user,
    otherUser: state.phAuth.otherUser,
  };
};

export default connect(mapStateToProps)(UserProfile);
