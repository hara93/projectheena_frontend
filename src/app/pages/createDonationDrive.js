/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import "../../index.scss";

import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import {
  Col,
  Table,
  Dropdown,
  DropdownButton,
  FormControl,
  Card,
} from "react-bootstrap";
import CustomButton from "../components/CustomButton";
import "bootstrap/dist/css/bootstrap.min.css";
import TextEditor from "../components/textEditor";
import TagsInput from "../components/TagInput";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import CheckboxTable from "../components/checkboxTable";
import DatePicker from "react-datepicker";

const CreateDonationDrive = () => {
  const today = new Date();
  const dd = today.getDate();
  const mm = today.getMonth();
  const yyyy = today.getFullYear();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState();
  const [timeBound, setTimeBound] = useState("");
  const handleTimeBound = (e) => {
    setTimeBound(e.target.value);
  };
  let donationDates;
  donationDates = (
    <>
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom">
          Donation Dates
        </Form.Label>
        <Col>
          {/* <DatePicker
            selected={startDate}
            dateFormat="dd-MM-yyyy"
            startDate={startDate}
            endDate={endDate}
          /> */}
          <Form.Control
            size="lg"
            type="text"
            value={`${dd}-${mm}-${yyyy}`}
          ></Form.Control>
        </Col>
        <Col>
          <DatePicker
            placeholderText="End Date"
            selected={endDate}
            dateFormat="dd-MM-yyyy "
            selectsEnd
            startDate={startDate}
            endDate={endDate}
            minDate={startDate}
            onChange={(date) => setEndDate(date)}
          />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
    </>
  );
  const [other, setOther] = useState(false);
  const handleOther = (e) => {
    setOther(!other);
  };
  let otherDeliverables;
  otherDeliverables = (
    <>
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Other deliverables
        </Form.Label>
        <Col>
          <Form.Control
            size="lg"
            type="text"
            placeholder="Add other deliverables comma seperated"
            className="green-focus"
          />
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
    </>
  );

  const [minDonation, setMinDonation] = useState();
  const minDonationAmt = (e) => {
    setMinDonation(e.target.value);
  };
  let minDonationYes;
  minDonationYes = (
    <>
      <Form.Row>
        <Form.Label column="lg" lg={2} className="form-label-custom p-r-15">
          Set Minimum Donation Amount from each donor
        </Form.Label>
        <Col>
          <InputGroup style={{ zIndex: 0 }}>
            <InputGroup.Prepend>
              <InputGroup.Text
                style={{ margin: "0", background: "transparent" }}
              >
                <i class="fa fa-rupee"></i>
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              type="number"
              size="lg"
              className="green-focus"
              placeholder="Maximum 5 Digits"
            />
          </InputGroup>
        </Col>
      </Form.Row>
      <hr className="form-horizontal-line" />
    </>
  );

  return (
    <>
      <div style={{ marginBottom: "30px" }}>
        <h2 className="font-bold">Create Donation</h2>
        <p>
          Currently, donation can only be created by approved NGOs. Please get
          in touch with us before filling the below form for easy approval.
        </p>
      </div>
      <Card style={{ padding: "30px " }}>
        <Form>
          <Form.Group>
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Donation Name
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="text" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Donation For
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="text" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Cause Supported
              </Form.Label>
              <Col>
                <div style={{ marginTop: "-10px" }}>
                  <CheckboxTable />
                </div>
              </Col>
            </Form.Row>
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Description
              </Form.Label>
              <Col>
                <TextEditor />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Donation Amount to be raised
              </Form.Label>
              <Col>
                <InputGroup className="mb-3">
                  <DropdownButton
                    as={InputGroup.Prepend}
                    variant="outline-secondary"
                    title="INR"
                    size="lg"
                    id="input-group-dropdown-1"
                  >
                    <Dropdown.Item href="#">AFN</Dropdown.Item>
                    <Dropdown.Item href="#">EUR</Dropdown.Item>
                    <Dropdown.Item href="#">ALL</Dropdown.Item>
                  </DropdownButton>
                  <FormControl size="lg" className="green-focus" />
                </InputGroup>

                <FormControlLabel
                  control={<Checkbox color="primary" />}
                  label="Stop Donation once the above amount is reached"
                  value="Stop Donation once the above amount is reached"
                />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Minimum donation amount?
              </Form.Label>

              <Col className="form-input-align-center">
                <RadioGroup
                  name="donationAmount"
                  // value={location}
                  // onChange={(e) => handleChangeLocation(e)}
                >
                  <div>
                    <FormControlLabel
                      value="Yes"
                      control={<Radio color="primary" />}
                      label="Yes"
                      onChange={(e) => minDonationAmt(e)}
                    />
                    <FormControlLabel
                      value="No"
                      control={<Radio color="primary" />}
                      label="No"
                      onChange={(e) => minDonationAmt(e)}
                    />
                  </div>
                </RadioGroup>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            {minDonation === "Yes" ? minDonationYes : null}
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Deliverables by you
              </Form.Label>
              <Col>
                <Table
                  responsive="lg"
                  borderless
                  style={{ marginTop: "-10px" }}
                >
                  <tbody>
                    <tr>
                      <td className="table-data">
                        <FormControlLabel
                          control={<Checkbox color="primary" />}
                          label="80G Certificate"
                        />
                      </td>
                      <td className="table-data">
                        <FormControlLabel
                          control={<Checkbox color="primary" />}
                          label="35AC Certificate"
                        />
                      </td>
                      <td className="table-data">
                        <FormControlLabel
                          control={<Checkbox color="primary" />}
                          label="Reports"
                        />
                      </td>
                    </tr>
                    <tr>
                      <td className="table-data">
                        <FormControlLabel
                          control={<Checkbox color="primary" />}
                          label="Photographs"
                        />
                      </td>
                      <td className="table-data">
                        <FormControlLabel
                          control={<Checkbox color="primary" />}
                          checked={other}
                          label="Other"
                          value="Other"
                          onChange={(e) => handleOther(e)}
                        />
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            {other ? otherDeliverables : null}
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Time Bound Donation?
              </Form.Label>
              <Col className="form-input-align-center">
                <RadioGroup
                  name="timBound"
                  // value={location}
                  // onChange={(e) => handleChangeLocation(e)}
                >
                  <div>
                    <FormControlLabel
                      value="Yes"
                      control={<Radio color="primary" />}
                      label="Yes"
                      onChange={(e) => handleTimeBound(e)}
                    />
                    <FormControlLabel
                      value="No"
                      control={<Radio color="primary" />}
                      label="No"
                      onChange={(e) => handleTimeBound(e)}
                    />
                  </div>
                </RadioGroup>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            {timeBound === "Yes" ? donationDates : null}

            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Image
              </Form.Label>
              <Col
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                }}
              >
                <Form.File required name="file" />
                <p className="help-block m-b-none">
                  {" "}
                  Optimum size would be 600px by 400px
                </p>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Comma Seperated tags
              </Form.Label>
              <Col>
                <TagsInput tags={[]} />
                <Form.Text className="text-muted" muted>
                  Maximum 3 tags allowed
                </Form.Text>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom p-r-15"
              >
                Contact Info
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="text" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label
                column="lg"
                lg={2}
                className="form-label-custom  p-r-15"
              >
                External URL
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="text" className="green-focus" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2}></Form.Label>
              <Col>
                <CustomButton content="Create dontaion" />
                <p className="help-block">
                  By creating a donation you agree to ProjectHeena's terms &
                  conditions. You also confirm to judiciously provide all the
                  relevant proof in a timely manner about the utilization of
                  these funds. Donation processing is completely under
                  ProjectHeena's sole discretion.
                </p>
              </Col>
            </Form.Row>
          </Form.Group>
        </Form>
      </Card>
    </>
  );
};

export default CreateDonationDrive;
