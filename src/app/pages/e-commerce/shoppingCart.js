/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import { Card, Row, Col } from "react-bootstrap";
import { Products } from "../../config/e-commerce/product-images";
import Form from "react-bootstrap/Form";

const Cart = () => {
  const [quantity, setQuantity] = useState(0);
  const [total_items, setTotal_items] = useState(0);
  var data1 = [
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      amt: 1299,
    },
    {
      image: Products.mouse1,
      title: "Logitech M190 Full Size Wireless Mouse",
      amt: 2500,
    },
    {
      image: Products.headphone1,
      title: "Sennheiser HD 458 BT Over Ear Wireless Headphones",
      amt: 780,
    },
  ];
  const [data, setData] = useState(data1);
  var [total_amount, setTotal_amount] = useState(0);
  data.forEach((e) => (total_amount = total_amount + e.amt));
  useEffect(() => {
    if (quantity < 0) {
      setQuantity(0);
    }
    setTotal_items(data.length);
  });
  var handleQuantity = (e) => {
    setQuantity(e.target.value);
  };
  const handleDelete = (idx) => {
    const temp = [...data];
    temp.splice(idx, 1);
    setData(temp);
  };
  //   total_amount = data.forEach((ele) => (total_amount += ele.amt));
  return (
    <>
      <div className="container">
        <h2 className="font-bold">SHOPPING CART</h2>
        <Row>
          <Col lg={8}>
            {data.length == 0 ? (
              <div className="no-items">
                <spam>No items in your cart</spam>
              </div>
            ) : (
              data.map((ele, idx) => (
                <>
                  {" "}
                  <Card className="cart-list">
                    <div style={{ width: "150px" }}>
                      <img
                        src={ele.image}
                        alt=""
                        height="100"
                        width="120"
                        style={{ width: "100%" }}
                      />
                    </div>
                    <span style={{ fontSize: "18px", padding: "0 10px", width: "55%" }}>
                      {" "}
                      {ele.title}
                    </span>

                    <span style={{ fontSize: "18px", padding: "0 10px" }}> ₹{ele.amt}</span>
                    <div style={{ fontSize: "18px", padding: "0 10px" }}>
                      <Form.Control
                        size="lg"
                        type="number"
                        className="green-focus"
                        onChange={(e) => handleQuantity(e)}
                        value={quantity}
                      />
                    </div>
                    <div style={{ padding: "0 10px" }}>
                      <button style={{ border: "0" }} onClick={() => handleDelete(idx)}>
                        <i class="fas fa-trash-alt" style={{ color: "black" }}></i>
                      </button>
                    </div>
                  </Card>{" "}
                </>
              ))
            )}
          </Col>
          <Col>
            <div className="checkout-tab">
              <div className="checkout-tab-head">
                <h3 className="font-bold">Subtotal ({total_items}) items</h3>
                <spam className="total-amount"> ₹{total_amount}</spam>
              </div>

              <div className="checkout-tab-body">
                <button className="add-to-cart-btn">Proceed to Checkout</button>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
};
export default Cart;
