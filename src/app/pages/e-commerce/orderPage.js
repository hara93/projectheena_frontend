/* eslint-disable no-restricted-imports */
import React from "react";
import { Card, Row, Col, Badge } from "react-bootstrap";
import Paper from "@material-ui/core/Paper";
import { Products } from "../../config/e-commerce/product-images";
import Table from "@material-ui/core/Table";

const OrdersPage = () => {
  var data = [
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      amount: 1299,
      quantity: 1,
    },
    {
      image: Products.mouse1,
      title: "Logitech M190 Full Size Wireless Mouse",
      amount: 2500,
      quantity: 2,
    },
    {
      image: Products.headphone1,
      title: "Sennheiser HD 458 BT Over Ear Wireless Headphones",
      amount: 780,
      quantity: 3,
    },
  ];
  var temp = [];
  data.forEach((ele) => temp.push(ele.amount * ele.quantity));
  var total = 0;
  temp.forEach((ele) => (total += ele));
  var discount = 50;
  var GST = total * 0.05;
  var gst_amount = parseInt(GST);
  var delivery = 100;
  var grand_total = total + gst_amount + delivery - discount;
  console.log("gt:", grand_total);
  return (
    <>
      <div className="container">
        <Paper style={{ padding: "15px", margin: "25px 0" }} elevation={3}>
          <Row>
            <Col lg={12}>
              <div className="order-header">
                <h2 className="font-bold">ORDER INFO</h2>
              </div>
            </Col>
            <Col lg={6} style={{ borderRight: "1px solid #ddd" }}>
              <div style={{ display: "flex" }}>
                <span className="order-info font-bold">Order ID :</span>
                <span className="order-info">123412321</span>
              </div>
              <div style={{ display: "flex" }}>
                <span className="order-info font-bold">Date :</span>
                <span className="order-info">12/07/2021</span>
              </div>
            </Col>
            <Col lg={6}>
              {" "}
              <div style={{ display: "flex" }}>
                <span className="order-info font-bold">Cause :</span>
                <span className="order-info">Development</span>
              </div>
              <div style={{ display: "flex" }}>
                <span className="order-info font-bold">Status</span>
                <span className="order-info">
                  <Badge variant="danger" className="badge1">
                    Not Delivered
                  </Badge>
                </span>
              </div>
            </Col>
          </Row>
        </Paper>
        <Paper style={{ padding: "15px" }} elevation={3}>
          <Row>
            <Col lg={12}>
              <div style={{ borderBottom: "0" }}>
                <div className="order-header">
                  <h2 className="font-bold">ORDERED ITEMS</h2>
                </div>
                <div>
                  <Table responsive="sm" className="m-0">
                    <thead style={{ background: "#ddd" }}>
                      <tr style={{ textAlign: "center" }}>
                        <th style={{ fontSize: "18px" }}>Image</th>
                        <th style={{ fontSize: "18px" }}>Title</th>
                        <th style={{ fontSize: "18px" }}>Quantity</th>
                        <th style={{ fontSize: "18px" }}>Price</th>
                        <th style={{ fontSize: "18px" }}>Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.map((ele, idx) => (
                        <>
                          <tr style={{ borderBottom: "1px solid #dedede" }}>
                            <td style={{ border: "0", background: "white" }}>
                              <div>
                                <img src={ele.image} alt="" height="65" width="100" />
                              </div>
                            </td>
                            <td style={{ border: "0", background: "white" }}>
                              <div>
                                <span
                                  style={{
                                    fontSize: "1.2rem",
                                    padding: "0 10px",
                                    width: "55%",
                                    color: "gray",
                                  }}
                                >
                                  {" "}
                                  {ele.title}
                                </span>
                              </div>
                            </td>
                            <td style={{ border: "0", background: "white" }}>
                              <span
                                style={{ fontSize: "1.2rem", padding: "0 10px", color: "gray" }}
                              >
                                {" "}
                                {ele.quantity}
                              </span>
                            </td>
                            <td style={{ border: "0", background: "white" }}>
                              <span
                                style={{ fontSize: "1.2rem", padding: "0 10px", color: "gray" }}
                              >
                                {" "}
                                {ele.amt}
                              </span>
                            </td>
                            <td style={{ border: "0", background: "white" }}>
                              <span
                                style={{ fontSize: "1.2rem", padding: "0 10px", color: "gray" }}
                              >
                                {" "}
                                {ele.amt * ele.quantity}
                              </span>
                            </td>
                          </tr>
                        </>
                      ))}
                    </tbody>
                  </Table>
                </div>
              </div>
            </Col>
          </Row>
        </Paper>
        <Paper style={{ padding: "15px", margin: "25px 0" }} elevation={3}>
          <Row>
            <Col lg={8}>
              <Paper style={{ padding: "10px" }} elevation={3}>
                <div className="order-header">
                  <h2 className="font-bold">SHIPPING ADDRESS</h2>
                </div>
                <div>
                  <span style={{ fontSize: "14px", color: "gray" }}>
                    1345 Parkview Drive
                    <br /> Anaheim
                    <br /> CA
                    <br /> full California
                    <br /> 92801
                  </span>
                </div>
              </Paper>
              <Paper style={{ padding: "10px", margin: "20px 0" }} elevation={3}>
                <div className="order-header">
                  <h2 className="font-bold">SELLERS ADDRESS</h2>
                </div>
                <div>
                  <span style={{ fontSize: "14px", color: "gray" }}>
                    1345 Parkview Drive
                    <br /> Anaheim
                    <br /> CA
                    <br /> full California
                    <br /> 92801
                  </span>
                </div>
              </Paper>
            </Col>
            <Col lg={4}>
              <Paper style={{ padding: "10px" }} elevation={3}>
                <h2 className="font-bold" style={{ textAlign: "center" }}>
                  ORDER SUMMARY
                </h2>
                <div className="order-summary">
                  <div className="summary-sections">
                    <span>Items</span>
                    <span>{total}</span>
                  </div>
                  <div className="summary-sections">
                    <span>Discount</span>
                    <span>-{discount}</span>
                  </div>
                  <div className="summary-sections">
                    <span>GST</span>
                    <span>₹{gst_amount}</span>
                  </div>
                  <div className="summary-sections">
                    <span>Delivery</span>
                    <span>₹{delivery}</span>
                  </div>
                  <div className="summary-sections">
                    <span style={{ fontWeight: "600", fontSize: "18px" }}>Total</span>
                    <span style={{ fontWeight: "600", fontSize: "18px" }}>₹{grand_total}</span>
                  </div>

                  <button className="proceed-to-checkout">Proceed to checkout</button>
                  <div style={{ borderBottom: "0" }}>
                    <a href="" className="ph-link">
                      Need Help?
                    </a>
                  </div>
                </div>
              </Paper>
            </Col>
          </Row>
        </Paper>
      </div>
    </>
  );
};
export default OrdersPage;
