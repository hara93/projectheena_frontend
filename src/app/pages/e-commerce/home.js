/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import { Card, Row, Col } from "react-bootstrap";
import Rating from "@material-ui/lab/Rating";
import Carousel from "react-bootstrap/Carousel";
import { Products } from "../../config/e-commerce/product-images";
import Paper from "@material-ui/core/Paper";
const Home = () => {
  const [value, setValue] = useState(2);
  var product_list = [
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      price: 1000,
    },
    {
      image: Products.mouse1,
      title: "Logitech M90 Wired USB Mouse,Black",
      price: 2500,
    },
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      price: 1000,
    },
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      price: 1000,
    },
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      price: 1000,
    },
  ];
  const cause_list = [
    {
      image: Products.physicallychallenged1,
      title: "Physically challenged childs",
    },
    {
      image: Products.physicallychallenged2,
      title: "Visually challenged childs",
    },
    {
      image: Products.skilldevelopment1,
      title: "Community Development",
    },
    {
      image: Products.physicallychallenged2,
      title: "Visually challenged childs",
    },
    {
      image: Products.skilldevelopment1,
      title: "Community Development",
    },
  ];

  return (
    <>
      <div className="container">
        <div style={{ height: "400px", margin: "15px 0" }}>
          <Carousel>
            <Carousel.Item interval={1000}>
              <img
                className="d-block w-100"
                src={Products.banner1}
                height="400px"
                alt="First slide"
              />
            </Carousel.Item>
            <Carousel.Item interval={500}>
              <img
                className="d-block w-100"
                src={Products.banner1}
                height="400px"
                alt="Second slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={Products.banner1}
                height="400px"
                alt="Third slide"
              />
            </Carousel.Item>
          </Carousel>
        </div>

        <Paper className="home-sections">
          <div className="category-heading">
            <h1>Popular Categories</h1>
          </div>
          <div className="popular-categories">
            <div style={{ textAlign: "center" }}>
              <div className="category-list">
                <i class="fas fa-tshirt category-icon"></i>
              </div>
              <span className="category-name">Clothing</span>
            </div>
            <div style={{ textAlign: "center" }}>
              <div className="category-list">
                <i class="fas  fa-laptop category-icon"></i>
              </div>
              <span className="category-name">Gadgets</span>
            </div>
            <div style={{ textAlign: "center" }}>
              <div className="category-list">
                <i class="fas fa-headphones category-icon"></i>
              </div>
              <span className="category-name">Accessories</span>
            </div>
            <div style={{ textAlign: "center" }}>
              <div className="category-list">
                <i class="fas fa-shopping-bag category-icon"></i>
              </div>
              <span className="category-name">Bags</span>
            </div>
            <div style={{ textAlign: "center" }}>
              <div className="category-list">
                <i class="fas fa-gifts category-icon"></i>
              </div>
              <span className="category-name">Combo</span>
            </div>
          </div>
        </Paper>

        <div className="home-latest-products">
          <div className="category-heading">
            <h1 className="category-title">Latest products</h1>
          </div>
          <Row style={{ margin: "10px 0" }}>
            {product_list.map((ele, idx) => (
              <>
                <Col xs={6} md={4} lg={3} style={{ padding: "0 5px", margin: "10px 0" }}>
                  <Card className="product-card">
                    <img src={ele.image} alt="camera1" height="175px"></img>
                    <h5 className="product-title">
                      <a href="" className="ph-link">
                        {ele.title}
                      </a>
                    </h5>
                    <div>
                      <p className="product-amount">₹{ele.price}</p>
                    </div>
                    <div className="product-body">
                      <Rating
                        name="simple-controlled"
                        value={value}
                        onChange={(event, newValue) => {
                          setValue(newValue);
                        }}
                        readOnly
                      />
                      <p className="product-review">0 Reviews</p>
                    </div>
                  </Card>
                </Col>
              </>
            ))}
          </Row>
        </div>
        <Paper className="product-sections">
          <div>
            <div className="category-heading">
              <h1>Shop for a Cause |</h1>
              <div>Below are some list of causes you can shop for</div>
            </div>
          </div>

          <Row style={{ margin: "10px 0" }}>
            {cause_list.map((ele, idx) => (
              <>
                <Col xs={6} md={4} lg={3} style={{ padding: "0 5px", margin: "10px 0" }}>
                  <Card className="cause-card">
                    <img src={ele.image} alt="physicallychallenged1" height="175px"></img>
                    <h5 className="cause-title">
                      {" "}
                      <a href="" className="ph-link">
                        {ele.title}
                      </a>
                    </h5>
                  </Card>
                </Col>
              </>
            ))}
          </Row>
        </Paper>
      </div>
    </>
  );
};
export default Home;
