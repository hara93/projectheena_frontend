/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import { Card, Row, Col } from "react-bootstrap";
import { Products } from "../../config/e-commerce/product-images";
import Rating from "@material-ui/lab/Rating";
import Form from "react-bootstrap/Form";
import Paper from "@material-ui/core/Paper";
import Carousel from "react-bootstrap/Carousel";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
const Product = () => {
  const [value, setValue] = useState(2);
  const [quantity, setQuantity] = useState(0);
  useEffect(() => {
    if (quantity < 0) {
      setQuantity(0);
    }
  });
  var handleQuantity = (e) => {
    setQuantity(e.target.value);
  };
  var data = {
    image: Products.camera1,
    title: "Nikon0 D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
    amount: 1299.0,
    desc:
      "The great companion for your photography needs, the advanced Nikon D3500 24 MP (18-55mm Lens)   DSLR Camera is not only feature-rich, but also ergonomically designed and    lightweight. The Nikon D3500, which is designed to be as flexible and intuitive as    possible, while still offering the imaging capabilities you expect from a DSLR\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). ",
  };
  var product_details = {
    price: 1299.0,
    is_available: 1,
  };
  const [key, setKey] = useState("Reviews");
  return (
    <>
      <div className="container">
        <Paper className="product-sections">
          <Row>
            <Col lg={5}>
              <div style={{ width: "100%" }}>
                <Carousel>
                  <Carousel.Item interval={1000}>
                    <img
                      className="d-block w-100 carousel"
                      src={Products.camera1}
                      height="400px"
                      alt="First slide"
                    />
                  </Carousel.Item>
                  <Carousel.Item interval={500}>
                    <img
                      className="d-block w-100 carousel"
                      src={Products.camera1}
                      height="400px"
                      alt="Second slide"
                    />
                  </Carousel.Item>
                  <Carousel.Item>
                    <img
                      className="d-block w-100 carousel"
                      src={Products.camera1}
                      height="400px"
                      alt="Third slide"
                    />
                  </Carousel.Item>
                </Carousel>
              </div>
            </Col>
            <Col lg={3} className="p-0">
              <div>
                <h3 className="p-title">{data.title}</h3>
                <span className="by">By Nikon</span>
              </div>
              <div className="product-body">
                <Rating
                  name="simple-controlled"
                  value={value}
                  onChange={(event, newValue) => {
                    setValue(newValue);
                  }}
                />
                <p className="product-review">0 Reviews</p>
              </div>
              <div className="section">
                <h5 className="font-bold">
                  Available - <span className="in-stock">In Stock</span>
                </h5>
                <h5 className="font-bold">
                  Available - <span className="not-in-stock">Out of Stock</span>
                </h5>
                <div className="m-v-10">
                  <div className="flex-align-center">
                    {" "}
                    <i class="fas fa-check base-color" style={{ marginRight: "5px" }}></i>{" "}
                    <p className="m-0-p-0">Study Certificate up to 30 days</p>
                  </div>
                  <div className="flex-align-center">
                    <i class="fas fa-check base-color " style={{ marginRight: "5px" }}></i>{" "}
                    <p className="m-0-p-0">Upto 5 user simultaneouslu</p>
                  </div>
                  <div className="flex-align-center">
                    <i class="fas fa-check base-color " style={{ marginRight: "5px" }}></i>{" "}
                    <p className="m-0-p-0">Has health certificate</p>
                  </div>
                </div>
              </div>
              <div className="section">
                <div className="flex-align-center">
                  {" "}
                  <i class="fas fa-dollar-sign" style={{ marginRight: "13px" }}></i>{" "}
                  <p className="m-0-p-0">100% Money Back</p>
                </div>
                <div className="flex-align-center">
                  {" "}
                  <i class="fas fa-truck" style={{ marginRight: "5px" }}></i>{" "}
                  <p className="m-0-p-0">Cash On Delivery available</p>
                </div>
                <div className="flex-align-center">
                  {" "}
                  <i class="fas fa-shield-virus" style={{ marginRight: "10px" }}></i>{" "}
                  <p className="m-0-p-0">Non-Contact Shipping</p>
                </div>
              </div>
            </Col>
            <Col lg={4}>
              {/* <div className="add-to-cart-cont">
                <ul className="add-to-cart">
                  <li>
                    <div className="th-cart">
                      <span style={{ fontSize: "18px" }}>Price:</span>
                    </div>
                    <div className="tc-cart">{product_details.price}</div>
                  </li>
                  <li>
                    <div className="th-cart">
                      <span style={{ fontSize: "18px" }}>Status:</span>
                    </div>
                    <div className="tc-cart">
                      {product_details.is_available == 1 ? "In Stock" : "Out of stock"}
                    </div>
                  </li>
                  <li
                    style={{
                      display: "table-row",
                      listStyleType: "none",
                    }}
                  >
                    <div className="th-cart">
                      <span style={{ fontSize: "18px" }}>Quantity:</span>
                    </div>
                    <div className="tc-cart">
                      {" "}
                      <Form.Control
                        size="lg"
                        type="number"
                        className="green-focus"
                        onChange={(e) => handleQuantity(e)}
                        value={quantity}
                      />
                    </div>
                  </li>
                </ul>
                <div style={{ width: "100%", display: "flex", flexGrow: "1", padding: "10px" }}>
                  {product_details.is_available == 1 ? (
                    <button className="add-to-cart-btn">Add to Cart</button>
                  ) : (
                    <button className="out-of-stock-btn" disabled>
                      Out of stock
                    </button>
                  )}
                </div>
              </div> */}

              <Paper className="product-info">
                <div className="out-of-stock">
                  <span>OUT OF STOCK</span>
                </div>
                <div className="available">
                  <span>AVAILABLE</span>
                </div>
                <div className="price">
                  <h2 className="font-bold">₹ {product_details.price}</h2>
                  <span className="discounted-price">SAVE 199</span>
                </div>
                <div>
                  <h4 className="font-bold">Quantity</h4>
                  <Form.Control
                    size="lg"
                    type="number"
                    className="green-focus"
                    onChange={(e) => handleQuantity(e)}
                    value={quantity}
                    style={{ background: "transparent", border: "1px solid #4caf4f" }}
                  />
                </div>
                <div>
                  {/* {product_details.is_available == 1 ? (
                    <>
                      <button className="add-to-cart-btn">Add to Cart</button>
                     
                    </>
                  ) : (
                    <button className="out-of-stock-btn" disabled>
                      Out of stock
                    </button>
                  )} */}
                  <button className="out-of-stock-btn" disabled>
                    Out of stock
                  </button>
                  <button className="add-to-cart-btn">Add to Cart</button>
                </div>
                <div>
                  <a href="" className="ph-link">
                    Add to Wishlist
                  </a>
                </div>
              </Paper>
            </Col>
          </Row>
        </Paper>
        <Paper className="product-sections">
          <h3 className="p-title">{data.title}</h3>
          <div className="p-desc">{data.desc}</div>
        </Paper>
        <Tabs activeKey={key} onSelect={(k) => setKey(k)} style={{ margin: "0" }}>
          <Tab eventKey="Reviews" title="Reviews" style={{ opacity: "1" }}>
            <Paper style={{ margin: "0" }} className="product-sections">
              <Row style={{ margin: "0", padding: "0" }}>
                <Col xs={8} className="p-0">
                  <div>
                    <Card className="comments">
                      <div>
                        <h5 className="font-bold">Unknown user</h5>
                      </div>
                      <div>
                        <Rating
                          name="simple-controlled"
                          value={value}
                          onChange={(event, newValue) => {
                            setValue(newValue);
                          }}
                        />
                      </div>
                      <div>
                        <p>This camera is awesome</p>
                      </div>
                    </Card>
                  </div>
                </Col>
                <Col xs={8} className="p-0">
                  <div>
                    <Card className="comments">
                      <div>
                        <h5 className="font-bold">Unknown user</h5>
                      </div>
                      <div>
                        <Rating
                          name="simple-controlled"
                          value={value}
                          onChange={(event, newValue) => {
                            setValue(newValue);
                          }}
                        />
                      </div>
                      <div>
                        <p>This camera is awesome</p>
                      </div>
                    </Card>
                  </div>
                </Col>
                <Col xs={8} className="p-0">
                  <div>
                    <Card className="comments">
                      <div>
                        <h5 className="font-bold">Unknown user</h5>
                      </div>
                      <div>
                        <Rating
                          name="simple-controlled"
                          value={value}
                          onChange={(event, newValue) => {
                            setValue(newValue);
                          }}
                        />
                      </div>
                      <div>
                        <p>This camera is awesome</p>
                      </div>
                    </Card>
                  </div>
                </Col>
              </Row>
            </Paper>
          </Tab>
          <Tab eventKey="profile" title="Write Review" style={{ opacity: "1" }}>
            <Paper style={{ margin: "0" }} className="product-sections">
              <Row className="m-0-p-0">
                <Col xs={12} className="p-0">
                  <h3 className="p-title">Write a Customer review</h3>
                </Col>

                <Col className="p-0">
                  <div className="please-signin">
                    <div>
                      <p>
                        Please{" "}
                        <a href="/signup" className="ph-link">
                          Sign in
                        </a>{" "}
                        to write a review
                      </p>
                    </div>
                  </div>
                </Col>
              </Row>
            </Paper>
          </Tab>
        </Tabs>
      </div>
    </>
  );
};
export default Product;
