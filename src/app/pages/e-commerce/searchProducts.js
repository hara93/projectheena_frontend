/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import { Card, Row, Col, Form } from "react-bootstrap";
import { Products } from "../../config/e-commerce/product-images";
import Rating from "@material-ui/lab/Rating";
import Paper from "@material-ui/core/Paper";

const SearchProducts = () => {
  const [value, setValue] = useState(2);

  var data = [
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      price: 1000,
    },
    {
      image: Products.mouse1,
      title: "Logitech M90 Wired USB Mouse,Black",
      price: 2500,
    },
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      price: 1000,
    },
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      price: 1000,
    },
    {
      image: Products.camera1,
      title: "Nikon D5300 DSLR Camera with 18-55 mm Lens Kit, Black",
      price: 1000,
    },
  ];

  return (
    <>
      <div className="container">
        <Row style={{ margin: "10px -20px" }}>
          <Col md={3}>
            <h2 className="font-bold">Search Products</h2>
          </Col>
          <Col style={{ padding: "5px" }}>
            <Paper style={{ padding: "10px", borderRadius: "10px" }} elevation={0}>
              <h3 className="font-bold">CLOTHING </h3>
              <span style={{ color: "gray" }}>
                {" "}
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse pretium tortor
                id tellus cursus, nec hendrerit quam tincidunt. Ut quis venenatis tellus, non
                molestie ex. Vivamus fermentum quam eu leo lacinia suscipit. Vivamus viverra diam in
                porttitor blandit. Praesent vulputate enim a tincidunt sodales{" "}
              </span>
            </Paper>
          </Col>
        </Row>
        <Row style={{ margin: "0 -20px" }}>
          <Col md={4} lg={3} style={{ marginBottom: "30px" }}>
            <Card className="filter-card">
              <div>
                <Form.Row className="section">
                  <Form.Label column="lg" className="m-0">
                    Category
                  </Form.Label>
                  <Col xs={12}>
                    <Form.Control as="select" size="lg" custom>
                      <option>All</option>
                      <option>Clothing</option>
                    </Form.Control>
                  </Col>
                </Form.Row>
              </div>

              <div>
                <Form.Row className="section">
                  <Form.Label column="lg" className="m-0">
                    Price range
                  </Form.Label>
                  <Col xs={12}>
                    <div style={{ display: "flex", flexDirection: "column" }}>
                      <a href="" className="price-range">
                        Under ₹500
                      </a>
                      <a href="" className="price-range">
                        ₹500 - ₹1500
                      </a>
                      <a href="" className="price-range">
                        ₹1500 - ₹2500
                      </a>
                      <a href="" className="price-range">
                        ₹2500 - ₹3500
                      </a>
                    </div>
                  </Col>
                </Form.Row>
              </div>
              <div>
                <Form.Row className="section">
                  <Form.Label column="lg" className="m-0">
                    Avg. Customer Review
                  </Form.Label>
                  <Col xs={12}>
                    <div style={{ display: "flex", flexDirection: "column" }}>
                      <a href="">
                        <Rating name="simple-controlled" value={4} readOnly />
                      </a>
                      <a href="" className="price-range">
                        <Rating name="simple-controlled" value={3} readOnly />
                      </a>
                      <a href="" className="price-range">
                        <Rating name="simple-controlled" value={2} readOnly />
                      </a>
                      <a href="" className="price-range">
                        <Rating name="simple-controlled" value={1} readOnly />
                      </a>
                    </div>
                  </Col>
                </Form.Row>
              </div>
              <div>
                <Form.Row className="section">
                  <Form.Label column="lg" className="m-0">
                    Avg. Customer Review
                  </Form.Label>
                  <Col xs={12}>
                    <div style={{ display: "flex", flexDirection: "column" }}>
                      <a href="" className="price-range">
                        10% off or more
                      </a>
                      <a href="" className="price-range">
                        20% off or more
                      </a>
                      <a href="" className="price-range">
                        30% off or more
                      </a>
                      <a href="" className="price-range">
                        40% off or more
                      </a>
                    </div>
                  </Col>
                </Form.Row>
              </div>
            </Card>
          </Col>
          <Col md={8} lg={9} style={{ maring: "0", padding: "0" }}>
            <div></div>
            <div className="search-card">
              <Row style={{ margin: "0" }}>
                {data.map((ele, idx) => (
                  <>
                    <Col xs={6} md={6} lg={4} style={{ padding: "0 10px ", margin: "0 0 10px 0" }}>
                      <Card className="product-card">
                        <img src={ele.image} alt="camera1" height="175px"></img>
                        <h5 className="product-title">{ele.title}</h5>
                        <div>
                          <p className="product-amount">₹{ele.price}</p>
                        </div>
                        <div>
                          <div className="product-body">
                            <Rating
                              name="simple-controlled"
                              value={value}
                              onChange={(event, newValue) => {
                                setValue(newValue);
                              }}
                            />
                            <p className="product-review">0 Reviews</p>
                          </div>
                          <div>
                            <button className="add-to-cart-btn">Add to Cart</button>
                          </div>
                        </div>
                      </Card>
                    </Col>
                  </>
                ))}
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
};
export default SearchProducts;
