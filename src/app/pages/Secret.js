import React from "react";
import "../tnc.css";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom"

const Secret = () => {
  return (
    <>
      <div className="banner">
        <div className="banner-img">
          <div id="parallex-bg" style={{ backgroundImage: "url('http://projectheena.in/assets/img/static/superhero-kid.jpg') " }}></div>
        </div>
      </div>
      <div className="container">
        <Card style={{ margin: "0 auto", padding: "0 15px" }}>
          <div style={{ marginTop: "30px", textAlign: "center" }}>
            <h3 className="static-title"> Why the name ProjectHeena</h3>
          </div>

          <p style={{ paddingTop: "0" }}>
            One of the questions that we frequently receive is why the name
            'ProjectHeena'. That sounds quirky right? But rest assured it was
            not decided by drawing a lottery in our office. The thought behind
            this name is deeper and hence requires a dedicated page :P
          </p>
          <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
            What should we name it?
          </h3>
          <p style={{ paddingTop: "0" }}>
            We are building this platform to help people collaborate for the
            cause they stand for. We think we need to reinvent the way
            volunteering is done in India and the world. The basic message that
            was to be communicated was "Even you will gain if you start giving".
            Simple examples like we learn more when we teach what we have
            learned. We get satisfaction no money can buy when we help someone
            achieve their goal. Its all about "Paying it Forward".
          </p>
          <p style={{ paddingTop: "0" }}>
            But the problem was names that we earlier decided like - Generoism,
            BeingHuman (yes we thought that first), lend a hand, etc., didn't
            communicate the whole message. The final name was decided, basis the
            thought that - we are on a mission to make people contribute for
            better future.
          </p>

          <blockquote>
            Heena means 'Mehendi' in India. Its an urdu word for a paste that
            when applied on palms leaves a nice red color stain. Heena is used
            during festivals and weddings in several countries including India.
          </blockquote>

          <p style={{ paddingTop: "0" }}>
            The name Heena is adopted post inspiration from the following lines
            taken from a poem by Indian Saint Raheem.
          </p>
          <img
            src="http://projectheena.in/assets/img/static/genesis/doha.png"
            className="img-center"
            style={{ maxWidth: "100%", height: "auto" }}
            alt=""
          />

          <p style={{ paddingTop: "0" }}>
            Heena colors not just the hands of the receiver but also the hands
            of the giver. Heena for us stands for Happiness, Help and Gratitude.
            It increases as we share it further. When we volunteer to help
            someone we don't just help the receiver; even ourselves get helped
            in some ways by the gratitude shown.
          </p>

          <p style={{ paddingTop: "0" }}>
            Like the logic? We would appreciate if you can join ProjectHeena and
            let us figure out the best tasks for you to volunteer for. Also if
            possible please share a word with your group by{" "}
            <Link to="#" style={{ color: "#4CAF50", outline: "none" }}>
              Clicking Here for FB{" "}
            </Link>
            or{" "}
            <Link to="#" style={{ color: "#4CAF50", outline: "none" }}>
              here for Twitter
            </Link>{" "}
            and sharing the color of Heena and happiness with others.
          </p>

          <p style={{ color: "#4CAF50", fontWeight: "600", padding: "0" }}>
            Remember - The most colored hands and the happiest hearts are the
            ones who gave Heena the most not the ones who tried to hold back!
          </p>
          <p style={{ color: "#4CAF50", fontWeight: "600", padding: "0" }}>
            {" "}
            Keep Rocking!
          </p>

          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column",
            }}
          >
            <button className="join" style={{ display: "block" }}>
              Join the Revolution
            </button>

            <div className="d-flex align-items-center my-3">
              <i class="fab fa-facebook-f footer-facebook basecolor p-0"  ></i>
              <Link to="" className="basecolor" style={{ padding: "2px 0 0 0" }}>Share</Link>
              <span style={{ color: "inherit", paddingLeft: "5px" }}>|</span>
              <i class="fab fa-twitter footer-twitter basecolor p-0"></i>
              <Link to="" className="basecolor" style={{ padding: "2px 0 0 0" }}>Tweet</Link>
            </div>
          </div>
        </Card>
      </div>
    </>
  );
}


export default Secret;
