import React, { useState } from "react";
import NGOCard from "../components/NGOCard";

const ngoData = [
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
  {
    ngo_icon:
      "http://projectheena.in/uploads/ngo/32141200479513/profileImage/images/abhinavsamaj.PNG",
    ngo_name: "ABHINAV SAMAJ",
    ngo_details:
      "For years 'Abhinav Samaj' has been working in the urban as well as rural areas of India and",
  },
];
const latestTasks = [
  { task_name: "Record Audiobooks - Panchatantra and Jataka Tales" },
  { task_name: "Record Audiobooks - Regional Fables" },
  { task_name: "Record Audiobooks - Textbooks" },
];
const NGOList = () => {
  const [ngo, setNgo] = useState(ngoData);
  const [currentPage, setCurrentPage] = useState(1);
  const [ngoPerPage] = useState(4);

  const indexOfLastNgo = currentPage * ngoPerPage;
  const indexOfFirstNgo = indexOfLastNgo - ngoPerPage;
  const currentNgo = ngo.slice(indexOfFirstNgo, indexOfLastNgo);

  const pageNumber = [];
  const totalNgo = ngo.length;

  for (let i = 1; i <= Math.ceil(totalNgo / ngoPerPage); i++) {
    pageNumber.push(i);
  }

  return (
    <div className="container">
      <div style={{ marginBottom: "20px" }}>
        <div>
          <h2 className="font-bold">NGOs Registered with us</h2>
          <p>
            Following is a list of all the NGOs registered with us. If you know any NGO who would
            benefit from our technology please ask them to create an account with us or contact on
            team@projectheena.com
          </p>
        </div>
      </div>
      <NGOCard ngolist={currentNgo} latestTasks={latestTasks} />
      <nav>
        <ul className="pagination">
          {pageNumber.map((item) => (
            <li key={item} className="page-item">
              <button onClick={() => setCurrentPage(item)} className="page-link">
                {item}
              </button>
            </li>
          ))}
        </ul>
      </nav>
    </div>
  );
};
export default NGOList;
