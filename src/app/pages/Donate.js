import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';

class Donate extends React.Component {
    render() {
        return (
            <>
                <Card style={{ marginBottom: '5%' }}>
                    <Row className="p-5">
                        <Col md={3} >
                            <p style={{ textAlign: 'center' }}>Donations</p>
                            <h1 style={{ textAlign: 'center' }}>31</h1>
                        </Col>
                        <Col md={3}>
                            <p style={{ textAlign: 'center' }}>Donors</p>
                            <h1 style={{ textAlign: 'center' }}>31</h1>
                        </Col>
                        <Col md={3}>
                            <p style={{ textAlign: 'center' }}>To be Raised</p>
                            <h1 style={{ textAlign: 'center' }}>2M</h1>
                        </Col>
                        <Col md={3}>
                            <p style={{ textAlign: 'center' }}>Raised</p>
                            <h1 style={{ textAlign: 'center' }}>0</h1>
                        </Col>
                    </Row>
                </Card>



                <Card style={{ marginTop: '5%' }}>

                    <h5 style={{ marginTop: '2%', marginLeft: '2%' }}>Donations</h5>
                    <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2%' }} />

                    <Row className="p-5">
                        <Table >
                            <thead>
                                <tr>
                                    <th style={{ width: '35%' }}>Name</th>
                                    <th>To be Raised</th>
                                    <th>Raised</th>
                                    <th>End Date</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><img src={Images.CSRDetail} style={{ width: '45%', display: 'inline-block' }}></img>
                                        <p style={{ display: 'inline-block', color: '#4CAF50', paddingLeft: '1vw' }}>AIM for Seva</p></td>
                                    <td>₹ 8,40,000</td>
                                    <td>₹ 0</td>
                                    <td>Continuous Donation</td>

                                </tr>

                                <tr>
                                    <td><img src={Images.CSRDetail} style={{ width: '45%', display: 'inline-block' }}></img>
                                        <p style={{ display: 'inline-block', color: '#4CAF50', paddingLeft: '1vw' }}>AIM for Seva</p></td>
                                    <td>₹ 8,40,000</td>
                                    <td>₹ 0</td>
                                    <td>Continuous Donation</td>

                                </tr>


                            </tbody>
                        </Table>

                    </Row>

                </Card>

            </>
        )
    }
}
export default Donate;