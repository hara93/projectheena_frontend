import React, { useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import PeopleCard from '../components/PeopleCard';
import SearchTaskCard from '../components/SearchTaskCard';
import SearchDonationCard from '../components/SearchDonationCard';

const donationData = [
    {
        img: "",
        name: "Clean Your place",
        description: "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
        requires: "10,000"
    },
    {
        img: "",
        name: "Laptop for visually challenged student",
        description: "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
        requires: "10,000"
    },
    {
        img: "",
        name: "Clean Your place",
        description: "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
        requires: "10,000"
    },
    {
        img: "",
        name: "Clean Your place",
        description: "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
        requires: "10,000",
        ends: "13th July 2016"
    },
    {
        img: "",
        name: "Clean Your place",
        description: "he 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with ...",
        requires: "10,000"
    },

]
const SearchDonation = () => {
    const [donation, setDonation] = useState(donationData)
    const [currentPage, setCurrentPage] = useState(1)
    const [donationPerPage] = useState(2)


    const indexOfLastDonation = currentPage * donationPerPage
    const indexOfFirstDonation = indexOfLastDonation - donationPerPage;
    const currentDonation = donation.slice(indexOfFirstDonation, indexOfLastDonation)

    const pageNumber = [];
    const totaldonation = donation.length

    for (let i = 1; i <= Math.ceil(totaldonation / donationPerPage); i++) {
        pageNumber.push(i)
    }



    return (
        <>
            <SearchDonationCard donation={currentDonation} />
            <nav>
                <ul className="pagination">

                    {
                        pageNumber.map(
                            (item) => (
                                <li key={item} className="page-item">
                                    <button onClick={() => setCurrentPage(item)} className="page-link">
                                        {item}
                                    </button>
                                </li>
                            )
                        )
                    }

                </ul>

            </nav>
        </>
    )

}

export default SearchDonation;