/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import { Col, Modal, Tabs, Tab, Button, Card } from "react-bootstrap";

import ProfessionalTab from "./ProfessionalTab";
import CreativeTab from "./CreativeTab";
import SocialTab from "./SocialTab";
import CustomButton from "./CustomButton";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TagAutocomplete from "../components/tagAutocomplete";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
import PropTypes from 'prop-types';
import { getAllTaskList } from '../../redux/reducers/common/actionCreator';

const TaskSearchBox = (props) => {
  const causeList = useSelector((state) => state.common.causeList);
  const skillList = useSelector((state) => (state.common && state.common.skillsList) || null);

  const dispatch = useDispatch();
  useEffect(() => {
    var temp = [];

    causeList.forEach((ele) => temp.push({ value: ele._id, label: ele.name }));
    setData(temp);
  }, []);

  const [helpOn, setHelpOn] = useState();

  const [hours, setHours] = useState();
  const [workLocation, setWorkLocation] = useState("");
  const [skill, setSkill] = useState([]);
  const [toggleState, setToggleState] = useState(false);
  const [key, setKey] = useState("home");
  const [tag, setTag] = useState([]);

  const [location, setLocation] = useState("");
  const [lat, setLat] = useState();
  const [lng, setLng] = useState();
  const [showCity, setShowCity] = useState(false);

  const handleSkill = (e) => {
    // console.log("eeeeeeee", e.target.value);
    if (e.target.checked) {
      setSkill([...skill, e.target.value]);
    } else {
      skill.splice(skill.indexOf(e.target.value), 1);
      setSkill([...skill]);
    }
  };
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => {
    setShow(false);
    setSkill([]);
  };
  const selectSkill = () => {
    setShow(false);
  };
  const searchTask = () => {
    var data = {};
    if (cause !== "" && cause.length > 0 && cause !== "60f0328f478a39331cdcea39") {
      data.cause_supported = cause;
    }
    if (hours !== 0 && hours !== undefined) {
      data.task_duration_hours_min = hours;
    }
    if (workLocation !== "0" && workLocation !== "" && workLocation !== "2") {
      data.task_location = workLocation;
    }
    if (workLocation === "2") {
      data.task_location = location;
    }
    var temp = [];
    if (skill) {
      skillList.Professional.forEach((ele) => {
        if (skill.includes(ele.skill_name)) {
          temp.push(ele._id);
        }
      });
    }
    if (skill) {
      skillList.Social.forEach((ele) => {
        if (skill.includes(ele.skill_name)) {
          temp.push(ele._id);
        }
      });
    }
    if (skill) {
      skillList.Creative.forEach((ele) => {
        if (skill.includes(ele.skill_name)) {
          temp.push(ele._id);
        }
      });
    }
    if (temp.length > 0) {
      data.task_skill_mapping = temp.join();
    }
    console.log("values", data);
    dispatch(getAllTaskList(data));
  };
  const [data, setData] = useState([]);
  const [cause, setCause] = useState("");

  return (
    <>
      <div style={{ padding: "15px 0" }}>
        <Card style={{ padding: "15px" }}>
          <Form>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Would like to Help on</Form.Label>
              </Form.Row>
              <Col className="p-0">
                <Select
                  options={data}
                  className="green-focus"
                  onChange={(e) => {
                    setCause(e.value)
                  }}
                />
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Hourly contribution of</Form.Label>
              </Form.Row>
              <Col className="p-0">
                <InputGroup>
                  <Form.Control
                    className="green-focus"
                    type="number"
                    value={hours}
                    onChange={(e) => setHours(e.target.value)}
                    style={{ zIndex: "0" }}
                  />
                  <InputGroup.Append>
                    <InputGroup.Text style={{ background: "transparent" }}>Hours</InputGroup.Text>
                  </InputGroup.Append>
                </InputGroup>
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Would prefer working from</Form.Label>
              </Form.Row>
              <Col className="p-0">
                <RadioGroup
                  name="work_location"
                  style={{ paddingLeft: "10px" }}
                // value={location}
                // onChange={(e) => handleChangeLocation(e)}
                >
                  <div>
                    <FormControlLabel
                      value="0"
                      control={
                        <Radio color="primary" className="p-0" style={{ paddingRight: "5px" }} />
                      }
                      label="Anywhere"
                      onClick={() => setShowCity(false)}
                      onChange={(e) => setWorkLocation(e.target.value)}
                    />
                    <FormControlLabel
                      value="Online"
                      control={
                        <Radio color="primary" className="p-0" style={{ paddingRight: "5px" }} />
                      }
                      onClick={() => setShowCity(false)}
                      label="Volunteer From Home"
                      onChange={(e) => setWorkLocation(e.target.value)}
                    />
                    <FormControlLabel
                      value="2"
                      control={
                        <Radio color="primary" className="p-0" style={{ paddingRight: "5px" }} />
                      }
                      label="From a city"
                      onClick={() => setShowCity(true)}
                      onChange={(e) => setWorkLocation(e.target.value)}
                    />
                  </div>
                </RadioGroup>
              </Col>
            </Form.Group>
            {
              workLocation === "2" ? (
                <Form.Group className="p-tb-5">
                  <Form.Row className="m-0">
                    <Form.Label className="font-bold">Lives in</Form.Label>
                  </Form.Row>
                  <Col className="p-0">
                    <PlacesAutoComplete
                      location={(location) => {
                        setLocation(location);
                      }}
                      lat={(lat) => {
                        setLat(lat);
                      }}
                      lng={(lng) => setLng(lng)}
                    />
                  </Col>
                </Form.Group>
              ) :
                null
            }
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Which requires following skills</Form.Label>
              </Form.Row>
              <Col className="form-input-align-center m-0-p-0">
                <InputGroup>
                  <Form.Control
                    type="text"
                    onClick={handleShow}
                    id="skills"
                    className="green-focus"
                    value={skill}
                    style={{ zIndex: 0 }}
                  />
                  <InputGroup.Append style={{ width: "45px" }}>
                    <Button
                      variant="secondary"
                      id=""
                      onClick={() => {
                        setSkill([]);
                      }}
                      style={{ width: "70px", zIndex: 0 }}
                    >
                      <i class="fas fa-times" style={{ color: "white" }}></i>
                    </Button>
                  </InputGroup.Append>
                </InputGroup>
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Tags</Form.Label>
              </Form.Row >
              <Col className="m-0-p-0">
                <TagAutocomplete
                  tags={(tag) => {
                    var dataArr = tag.map((item) => {
                      return [item.name, item];
                    });
                    var maparr = new Map(dataArr);
                    var result = [...maparr.values()];
                    setTag(result);
                  }}
                  tag={tag}
                />
              </Col>
            </Form.Group >
            <div className="center-content">
              <button type="button" onClick={() => searchTask()} className="ph-btn-outline">
                Search Task
              </button>
            </div>
          </Form >
        </Card >
      </div >
      <Modal show={show} onHide={handleClose} animation={false} centered>
        {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
        <div className="modal-header-custom">
          <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
            Select Skills
          </h4>
          <Button variant="secondary" style={{ height: "30px" }} onClick={handleClose}>
            <i class="fas fa-times" style={{ paddingRight: "0", color: "white" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ borderBottom: "1px solid #cccccc" }}>
            <Tabs id="controlled-tab-example" activeKey={key} onSelect={(k) => setKey(k)}>
              <Tab eventKey="home" title="Professional" style={{ opacity: "100" }}>
                <ProfessionalTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
              <Tab eventKey="profile" title="Creative" style={{ opacity: "100" }}>
                <CreativeTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
              <Tab eventKey="contact" title="Social" style={{ opacity: "100" }}>
                <SocialTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
            </Tabs>
          </div>
          <div style={{ marginTop: "50px" }}>
            <p>Maximum skills allowed: 5. ({skill.length} Of 5 Skills selected)</p>
            <p>Select as many skills relevant, for better result</p>
            <p>Selected Skills -</p>
            {skill.map((ele) => (
              <div style={{ display: "flex", flexDirection: "column", width: "fit-content" }}>
                <p className="skill-tags">{ele}</p>
              </div>
            ))}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Select Skills" onClick={selectSkill} />
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default TaskSearchBox
TaskSearchBox.propTypes = {
  taskListFilter: PropTypes.object,
};
