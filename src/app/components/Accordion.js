/* eslint-disable no-restricted-imports */
import React from "react";
import "../../index.scss";
import { Button } from "react-bootstrap";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { Container, Row, Col, Form, Card } from "react-bootstrap";

import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import Divider from "@material-ui/core/Divider";
import AccordionActions from "@material-ui/core/AccordionActions";

import { Typography } from "@material-ui/core";

function AccordionBuilder(props) {
  return (
    <>
      {props.title.map((ele, index) => (
        <Row>
          <Col
            xs={1}
            style={{
              margin: "0",
              padding: "0",
            }}
          >
            {" "}
            <Checkbox
              color="primary"
              style={{
                margin: "0",
                padding: "0",
                marginTop: "15px",
                marginLeft: "25px",
              }}
            />
          </Col>
          <Col
            xs={1}
            style={{
              margin: "0",
              padding: "0",
            }}
          >
            <div
              style={{
                margin: "0",
                padding: "0",
                marginTop: "15px",
                marginLeft: "25px",
              }}
            >
              {index + 1}
            </div>
          </Col>
          <Col
            xs={10}
            style={{
              margin: "0",
              padding: "0",
            }}
          >
            <Accordion>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-label="Expand"
                aria-controls="additional-actions1-content"
                id="additional-actions1-header"
              >
                {/* <FormControlLabel
                  aria-label="Acknowledge"
                  onClick={(event) => event.stopPropagation()}
                  onFocus={(event) => event.stopPropagation()}
                  control={<Checkbox />}
                  label={ele.names}
                  style={{ textAlign: "right" }}
                /> */}
                <Typography style={{ textAlign: "right" }}>
                  {ele.names}
                </Typography>
              </AccordionSummary>
              <AccordionDetails>{ele.component}</AccordionDetails> <Divider />
              <AccordionActions>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    width: "200px",
                  }}
                >
                  <Button variant="light" className="survey-questions-btn ">
                    {" "}
                    delete
                  </Button>
                  <Button variant="light" className="survey-questions-btn">
                    {" "}
                    Copy
                  </Button>
                </div>
              </AccordionActions>
            </Accordion>
          </Col>
        </Row>
      ))}
      {/* {props.title} */}
    </>
  );
}
export default AccordionBuilder;
