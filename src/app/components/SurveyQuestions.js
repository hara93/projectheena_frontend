import React, { useState, useEffect } from "react";

import { Container, Row, Col, Button } from "react-bootstrap";
import { MdTextFields } from "react-icons/md";
import { ImPageBreak } from "react-icons/im";
import { GrMapLocation } from "react-icons/gr";
const ComponentToShow = (props) => {
  const LazyComponent = React.lazy(() =>
    import(`../components/${props.qType}`)
  );
  // switch(props.qType){
  //   case 'SingleChoice':
  //     return <SingleChoice {...props} />;
  //   default : return null;
  // }
  return <LazyComponent />;
};
//const ComponentToShow = React.lazy(() => import(`../components/SingleChoice`));
const SurveyQuestions = (props) => {
  useEffect(() => {
    // Update the document title using the browser API
    console.log("questions type component", props.qType);
  });
  return (
    <>
      {/* <button
        onClick={() => {
          console.log("setData child", props.ele);
          props.setData({ ...props.ele, qType: "Single" });
        }}
      ></button> */}
      <Container>
        {!(props.ele && props.ele.qType) ? (
          <>
            <Row style={{ margin: "0", padding: "0" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                }}
              >
                <div style={{ display: "flex" }}>
                  <h5 style={{ marginTop: "auto", marginBottom: "auto" }}>
                    Popular Question Types
                  </h5>
                  <div className="survey-questions-title-no">12</div>
                </div>
                <div>
                  <Button
                    variant="light"
                    // onClick={() => {
                    //   setDisplay(false);
                    // }}
                    onClick={() => {
                      props.getIndex(props.ele.id);
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-times"></i>
                  </Button>
                </div>
              </div>
            </Row>
            <Row style={{ margin: "0 0 15px 0", padding: "0" }}>
              <Row style={{ margin: "0", width: "100%" }}>
                <Col style={{ margin: "0", padding: "5px " }}>
                  <Button
                    variant="light"
                    name="single choice"
                    className="survey-questions-btn"
                    onClick={(e) => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SingleChoice",
                        selectedQuestion: "SINGLE CHOICE",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="far fa-dot-circle"></i> Single Choice
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "multipleChoice",
                        selectedQuestion: "MULTIPLE CHOICE",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="far fa-check-circle"></i>Multiple Choice
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    name="SingleChoice"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "text",
                        selectedQuestion: "TEXT",
                        checked: false,
                      });

                      props.getButtonState(true);
                    }}
                  >
                    <MdTextFields style={{ fontSize: "18px" }} /> Text
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "number",
                        selectedQuestion: "NUMBER",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="far fa-clone"></i> Number
                  </Button>
                </Col>
              </Row>
              <Row style={{ margin: "0", width: "100%" }}>
                <Col style={{ margin: "0", padding: "5px " }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "location",
                        selectedQuestion: "LOCATION",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-map-marker-alt"></i>Location
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "date",
                        selectedQuestion: "DATE",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="far fa-calendar"></i> Date
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "time",
                        selectedQuestion: "TIME",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="far fa-clock"></i> Time
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "note",
                        selectedQuestion: "NOTE",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-stream"></i> Note
                  </Button>
                </Col>
              </Row>
              <Row style={{ margin: "0", width: "100%" }}>
                <Col style={{ margin: "0", padding: "5px " }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "signature",
                        selectedQuestion: "SIGNATURE",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-signature"></i> Signature
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "sectionBreak",
                        selectedQuestion: "SECTION BREAK",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <ImPageBreak style={{ fontSize: "18px" }} /> Section Break
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "areaOnMap",
                        selectedQuestion: "AREA ON MAP",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-map-marked-alt"></i> Area on Map
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "distanceOnMap",
                        selectedQuestion: "DISTANCE ON MAP",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-route"></i> Distance on Map
                  </Button>
                </Col>
              </Row>
            </Row>

            <Row style={{ margin: "0", padding: "0" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                }}
              >
                <div style={{ display: "flex" }}>
                  <h5
                    style={{
                      marginTop: "auto",
                      marginBottom: "auto",
                      borderRadius: "5px",
                      textAlign: "left",
                    }}
                  >
                    Media And Contacts
                  </h5>
                  <div className="survey-questions-title-no">7</div>
                </div>
              </div>
            </Row>
            <Row style={{ margin: "0 0 15px 0", padding: "0" }}>
              <Row style={{ margin: "0", width: "100%" }}>
                <Col style={{ margin: "0", padding: "5px " }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SurveyImage",
                        selectedQuestion: "IMAGE",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-image"></i>Image
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SurveyGeoTag",
                        selectedQuestion: "IMAGE GEO TAG",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <GrMapLocation style={{ fontSize: "18px" }} /> Image Geo Tag
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SurveyPhone",
                        selectedQuestion: "PHONE",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-phone-alt"></i> Phone
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "Question",
                        selectedQuestion: "EMAIL",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="far fa-envelope"></i> Email
                  </Button>
                </Col>
              </Row>
              <Row style={{ margin: "0", width: "100%" }}>
                <Col style={{ margin: "0", padding: "5px " }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "Question",
                        selectedQuestion: "AUDIO",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-microphone-alt"></i> Audio
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SurveyVideo",
                        selectedQuestion: "VIDEO",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-video"></i> Video
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "Question",
                        selectedQuestion: "FILE UPLOAD",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="far fa-file"></i> File Upload
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}></Col>
              </Row>
            </Row>
            <Row style={{ margin: "0", padding: "0" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                }}
              >
                <div style={{ display: "flex" }}>
                  <h5 style={{ marginTop: "auto", marginBottom: "auto" }}>
                    Feedback
                  </h5>
                  <div className="survey-questions-title-no">3</div>
                </div>
              </div>
            </Row>
            <Row style={{ margin: "0 0 15px 0", padding: "0" }}>
              <Row style={{ margin: "0", width: "100%" }}>
                <Col style={{ margin: "0", padding: "5px " }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SurveyLikert",
                        selectedQuestion: "LIKER SCALE",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="far fa-laugh"></i> Likert Scale
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SurveyScale",
                        selectedQuestion: "SCALE",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-balance-scale"></i>Scale
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "Question",
                        selectedQuestion: "RATING",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="far fa-star"></i> Rating
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}></Col>
              </Row>
            </Row>
            <Row style={{ margin: "0", padding: "0" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                }}
              >
                <div style={{ display: "flex" }}>
                  <h5
                    style={{
                      marginTop: "auto",
                      marginBottom: "auto",
                      borderRadius: "5px",
                      textAlign: "left",
                    }}
                  >
                    Advanced
                  </h5>
                  <div className="survey-questions-title-no">3</div>
                </div>
              </div>
            </Row>
            <Row style={{ margin: "0 0 15px 0", padding: "0" }}>
              <Row style={{ margin: "0", width: "100%" }}>
                <Col style={{ margin: "0", padding: "5px " }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SurveyNoRepeat",
                        selectedQuestion: "(NO REPEAT) GROUP",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-network-wired"></i> Group (No repeat)
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SurveyGroupNumber",
                        selectedQuestion: "(NUMBER) GROUP",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-network-wired"></i>Group (Number)
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}>
                  <Button
                    variant="light"
                    className="survey-questions-btn"
                    onClick={() => {
                      props.setQtype({
                        ...props.ele,
                        qType: "SurveyGroupChoice",
                        selectedQuestion: "(CHOICE) GROUP",
                        checked: false,
                      });
                      props.getButtonState(true);
                    }}
                  >
                    <i class="fas fa-network-wired"></i> Group (Choice)
                  </Button>
                </Col>
                <Col style={{ margin: "0", padding: "5px" }}></Col>
              </Row>
            </Row>
          </>
        ) : props.ele.qType ? (
          <ComponentToShow
            qType={props.ele.qType}
            setStateData={(data) => {
              console.log("Set Data parent", data);
            }}
          />
        ) : (
          <p>No Questiontype</p>
        )}
      </Container>
    </>
  );
};
export default SurveyQuestions;
