import axios from 'axios';

import React, { Component, useState } from 'react';

const FileInput = () => {
  const [inputList, setInputList] = useState([]);

  const onAddBtnClick = event => {
    console.log("Add Btn")
    setInputList(inputList.concat(<FileInput key={inputList.length} />));
  };

  return <div>
    <input type="file" onChange={this.onFileChange} />
    <button style={{ marginTop: '3vh', border: '1px solid #4CAF50', color: '#4CAF50', backgroundColor: 'white' }} onClick={this.onFileUpload}>
      Add File
    </button>
    <button style={{ marginTop: '3vh', marginLeft: '2%', border: '1px solid red', color: 'red', backgroundColor: 'white' }} onClick={this.onFileUpload}>
      Remove File
    </button>
  </div>;
};



class FileUpload extends Component {




  state = {

    // Initially, no file is selected
    selectedFile: null,
    add: [{}]
  };

  // On file select (from the pop up)
  onFileChange = event => {

    // Update the state
    this.setState({ selectedFile: event.target.files[0] });

  };

  // On file upload (click the upload button)
  onFileUpload = () => {

    // Create an object of formData
    const formData = new FormData();

    // Update the formData object
    formData.append(
      "myFile",
      this.state.selectedFile,
      this.state.selectedFile.name
    );

    // Details of the uploaded file
    console.log(this.state.selectedFile);

    // Request made to the backend api
    // Send formData object
    axios.post("api/uploadfile", formData);
  };

  // File content to be displayed after
  // file upload is complete
  fileData = () => {

    if (this.state.selectedFile) {

      return (
        <div>
          <>
          </>

        </div>
      );
    } else {
      return (
        <>
        </>
      );
    }
  };

  handleAdd = () => {
    this.setState({ add: [...this.state.add, { id: '1' }] })

  }
  handleRemove = (e, index) => {
    const newVal = [...this.state.add]
    newVal.splice(index, 1)
    this.setState({ add: newVal })

  }

  render() {

    return (
      <div>
        {
          this.state.add.map(
            (item, index) => (
              <div style={{ marginTop: '10px' }}>
                <input type="file" onChange={this.onFileChange} />
                {/* <button onClick={this.onFileUpload}>
            Upload!
          </button> */}
                <div style={{ marginTop: '20px' }}>
                  <button onClick={this.handleAdd} style={{ backgroundColor: 'white', border: '1px solid #4caf4f', color: '#4caf4f', marginRight: '10px' }}>Add File</button>
                  <button onClick={(e, index) => { this.handleRemove(e, index) }} style={{ backgroundColor: 'white', border: '1px solid #ed5564', color: '#ed5564' }}>Remove File</button>
                </div>


              </div>
            )
          )
        }


        {/* </div> */}

        {this.fileData()}
      </div >
    );
  }
}

export default FileUpload;