import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ProjectTimelineEven from './ProjectTimelineEven';

const even =[
    {
        name:"Smokeless Chulhas 2",
        description:"Smokeless Chulhas will promote clean household energy that can in turn help to achieve environmental sustainability as well as improved air quality within the household. A report on how smokeless stoves in rural areas can keep homes warm without causing in"
    },
    {
        name:"Smokeless Chulhas 2",
        description:"Smokeless Chulhas will promote clean household energy that can in turn help to achieve environmental sustainability as well as improved air quality within the household. A report on how smokeless stoves in rural areas can keep homes warm without causing in"
    }
]

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(4),
    marginRight: theme.spacing(2),
  },
  affected: {
    flex:1,
    alignItems:'right'
  },
  unaffected: {
    flex:1,
    alignItems:'left'
    
  },
}));

export default function RtlOptOut() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.affected}> right</div>
      <div className={classes.unaffected}>Unaffected</div>
    </div>
  );
}
