/* eslint-disable no-restricted-imports */
import React, { useEffect } from "react";
import "../../index.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import { useDispatch, useSelector } from "react-redux";
import { getSkillsList } from "../../redux/reducers/common/actionCreator";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
// const professional = [
//   {
//     name: "Accounting & Auditing",
//     value: 1,
//   },
//   {
//     name: "Finance",
//     value: 2,
//   },
//   {
//     name: "Marketting / Branding",
//     value: 3,
//   },
//   {
//     name: "Human Resources",
//     value: 4,
//   },
//   {
//     name: "Computer Software",
//     value: 5,
//   },
//   {
//     name: "Computer Hardware",
//     value: 6,
//   },
//   {
//     name: "Internet / web",
//     value: 7,
//   },
//   {
//     name: "Sales",
//     value: 8,
//   },
//   {
//     name: "Research - R&D",
//     value: 9,
//   },
//   {
//     name: "Legal & Compliance",
//     value: 10,
//   },
//   {
//     name: "Operations/Manufacturing/Logistics",
//     value: 11,
//   },
//   {
//     name: "Product Development",
//     value: 12,
//   },
//   {
//     name: "Recruitment",
//     value: 13,
//   },
//   {
//     name: "Event Planning/Management",
//     value: 14,
//   },
//   {
//     name: "Health Care/Pharma/Medical Services",
//     value: 15,
//   },
//   {
//     name: "Engineering",
//     value: 16,
//   },
//   {
//     name: "Testing/Trials",
//     value: 17,
//   },
//   {
//     name: "Electronics",
//     value: 18,
//   },
//   {
//     name: "Counselling",
//     value: 19,
//   },
//   {
//     name: "Sports",
//     value: 20,
//   },
//   {
//     name: "Translation/Transcription",
//     value: 21,
//   },
//   {
//     name: "Nutritionist",
//     value: 48,
//   },
//   {
//     name: "Reading/Narration",
//     value: 49,
//   },
// ];

const ProfessionalTab = ({ handleChange, toggleState, alreadyChecked }) => {
  const dispatch = useDispatch();
  const common = useSelector((state) => (state.common && state.common.skillsList) || null);
  useEffect(() => {
    dispatch(getSkillsList());
  }, []);

  console.log("props", common);
  return (
    <>
      <div class="table-wrapper-scroll-y my-custom-scrollbar">
        <table class="table table-bordered table-striped mb-0">
          <tbody>
            {common.Professional.map((ele, idx) => (
              <tr>
                <td>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label={ele.skill_name}
                    value={ele.skill_name}
                    disabled={toggleState}
                    checked={alreadyChecked.includes(ele.skill_name)}
                    onChange={(e) => handleChange(e)}
                    style={{ marginBottom: "0" }}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default ProfessionalTab;
