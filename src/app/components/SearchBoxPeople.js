/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import { Card, Col, Tabs, Tab, Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
import Modal from "react-bootstrap/Modal";
import ProfessionalTab from "../components/ProfessionalTab";
import CreativeTab from "../components/CreativeTab";
import SocialTab from "../components/SocialTab";
import CustomButton from "../components/CustomButton";
import InputGroup from "react-bootstrap/InputGroup";
import Select from "react-select";
import { useSelector, useDispatch } from "react-redux";
import { getAllUsersList } from "../../redux/reducers/common/actionCreator";
const SearchBoxPeople = () => {
  var dispatch = useDispatch();
  const [helpOn, setHelpOn] = useState();
  const [availableDay, setAvailableDay] = useState();
  const [location, setLocation] = useState("");
  const [lat, setLat] = useState();
  const [lng, setLng] = useState();
  const [skill, setSkill] = useState([]);
  const [toggleState, setToggleState] = useState(false);
  const [key, setKey] = useState("home");
  const causeList = useSelector((state) => state.common.causeList);

  useEffect(() => {
    var temp = [];
    causeList.forEach((ele) => temp.push({ value: ele._id, label: ele.name }));
    setData(temp);
  }, []);
  const skillList = useSelector((state) => (state.common && state.common.skillsList) || null);

  const handleSkill = (e) => {
    if (e.target.checked) {
      setSkill([...skill, e.target.value]);
    } else {
      skill.splice(skill.indexOf(e.target.value), 1);
      setSkill([...skill]);
    }
  };
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => {
    setShow(false);
    setSkill([]);
  };
  const selectSkill = () => {
    setShow(false);
  };

  const [cause, setCause] = useState("");
  const [data, setData] = useState([]);
  const searchVol = () => {
    var data = {};
    if (cause !== "" && cause.length > 0 && cause !== "60f0328f478a39331cdcea39") {
      data.user_causes = cause;
    }
    if (location !== "") {
      data.task_location = location;
    }
    if (availableDay !== "Default" && availableDay !== undefined) {
      data.available_day = availableDay;
    }
    var temp = [];
    if (skill) {
      skillList.Professional.forEach((ele) => {
        if (skill.includes(ele.skill_name)) {
          temp.push(ele._id);
        }
      });
    }
    if (skill) {
      skillList.Social.forEach((ele) => {
        if (skill.includes(ele.skill_name)) {
          temp.push(ele._id);
        }
      });
    }
    if (skill) {
      skillList.Creative.forEach((ele) => {
        if (skill.includes(ele.skill_name)) {
          temp.push(ele._id);
        }
      });
    }
    if (temp.length > 0) {
      data.task_skill_mapping = temp.join();
    }
    console.log("values", data);
    dispatch(getAllUsersList(data));
  };
  return (
    <>
      <div style={{ padding: "15px 0" }}>
        <Card style={{ padding: "15px" }}>
          <Form>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Would like to Help on</Form.Label>
              </Form.Row>
              <Col className="p-0">
                <Select
                  options={data}
                  className="green-focus"
                  onChange={(e) => setCause(e.value)}
                />
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Is available on</Form.Label>
              </Form.Row>
              <Col className="p-0">
                <select
                  className="ph-select green-focus"
                  onChange={(e) => setAvailableDay(e.target.value)}
                >
                  <option value="Default">All Day</option>
                  <option value="8">Weekdays</option>
                  <option value="7">Weekends</option>
                  <option value="1">Mondays</option>
                  <option value="2">Tuesdays</option>
                  <option value="3">Wednesdays</option>
                  <option value="4">Thursdays</option>
                  <option value="5">Fridays</option>
                  <option value="6">Saturdays</option>
                  <option value="0">Sundays</option>
                </select>
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Lives in</Form.Label>
              </Form.Row>
              <Col className="p-0">
                <PlacesAutoComplete
                  location={(location) => {
                    setLocation(location);
                  }}
                  lat={(lat) => {
                    setLat(lat);
                  }}
                  lng={(lng) => setLng(lng)}
                />
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Has Following Skills</Form.Label>
              </Form.Row>
              <Col className="form-input-align-center m-0-p-0">
                <InputGroup>
                  <Form.Control
                    size="lg"
                    type="text"
                    onClick={handleShow}
                    id="skills"
                    className="green-focus"
                    value={skill}
                  />
                  <InputGroup.Append style={{ width: "45px" }}>
                    <Button
                      variant="secondary"
                      id=""
                      onClick={() => {
                        setSkill([]);
                      }}
                      style={{ width: "70px" }}
                    >
                      <i class="fas fa-times" style={{ color: "white" }}></i>
                    </Button>
                  </InputGroup.Append>
                </InputGroup>
              </Col>
            </Form.Group>
            {/* <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Is team member of</Form.Label>
              </Form.Row>
              <Col className="form-input-align-center m-0-p-0">
                <select
                  className="ph-select green-focus"
                  // onChange={(e) => setAvailableDay(e.target.value)}
                >
                  <option value="Default">Select Team</option>
                </select>
              </Col>
            </Form.Group> */}
            <div className="center-content">
              <button className="ph-btn-outline" type="button" onClick={() => searchVol()}>
                Search Volunteer
              </button>
            </div>
          </Form>
        </Card>
      </div>
      <Modal show={show} onHide={handleClose} animation={false} centered>
        {/* <Modal.Header closeButton>Select skills</Modal.Header> */}
        <div className="modal-header-custom">
          <h4 className="m-0 font-bold" style={{ fontSize: "14px" }}>
            Select Skills
          </h4>
          <Button variant="secondary" style={{ height: "30px" }} onClick={handleClose}>
            <i class="fas fa-times" style={{ paddingRight: "0", color: "white" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ borderBottom: "1px solid #cccccc" }}>
            <Tabs id="controlled-tab-example" activeKey={key} onSelect={(k) => setKey(k)}>
              <Tab eventKey="home" title="Professional" style={{ opacity: "100" }}>
                <ProfessionalTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
              <Tab eventKey="profile" title="Creative" style={{ opacity: "100" }}>
                <CreativeTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
              <Tab eventKey="contact" title="Social" style={{ opacity: "100" }}>
                <SocialTab
                  handleChange={handleSkill}
                  toggleState={toggleState}
                  alreadyChecked={skill}
                />
              </Tab>
            </Tabs>
          </div>
          <div style={{ marginTop: "50px" }}>
            <p>Maximum skills allowed: 5. ({skill.length} Of 5 Skills selected)</p>
            <p>Select as many skills relevant, for better result</p>
            <p>Selected Skills -</p>
            {skill.map((ele) => (
              <div style={{ display: "flex", flexDirection: "column", width: "fit-content" }}>
                <p className="skill-tags">{ele}</p>
              </div>
            ))}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Select Skills" onClick={selectSkill} />
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default SearchBoxPeople;
