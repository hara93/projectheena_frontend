import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import FileUpload from './FileUpload';

class NewUpdate extends React.Component {
    constructor() {
        super();
        this.state = {
            milestone: false
        }
    }

    handleChange = () => {
        this.setState({ milestone: !this.state.milestone })
    }
    render() {
        return (
            <>




                <ReactBootstrap.Accordion style={{ marginTop: '3vh', marginBottom: '4vh' }}>

                    <ReactBootstrap.Card.Header >
                        <ReactBootstrap.Accordion.Toggle variant="link" eventKey="0" style={{ fontWeight: 'normal', color: '#000', display: 'block', border: 'transparent', backgroundColor: 'transparent' }} >
                            New Update
                        </ReactBootstrap.Accordion.Toggle>
                    </ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Collapse eventKey="0">
                        <ReactBootstrap.Card.Body style={{ borderRight: '1px solid gainsboro', borderLeft: '1px solid gainsboro', borderBottom: '1px solid gainsboro' }}>
                            <div style={{ marginTop: '4vh', marginBottom: '3vh' }}>
                                <textarea name="description" row="5" maxLength="300" placeholder="Share any update with fellow collaborators" className="counter-textarea form-control" style={{ marginTop: '2vh' }} />


                                <div className="row" style={{ margin: '20px' }}>
                                    <div className="col-2">
                                        <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}>Upload</span>

                                    </div>
                                    <div className="col-8">
                                        <FileUpload />
                                    </div>



                                </div>



                                <div className="row" style={{ margin: '20px' }}>
                                    <div className="col-2">
                                        <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}>Comment For Milestone?</span>

                                    </div>
                                    <div className="col-8" >
                                        <input type="checkbox" onChange={this.handleChange} />
                                        <span style={{ marginLeft: '2%' }}>Yes</span><br />

                                    </div>


                                </div>
                                {
                                    this.state.milestone == true ?
                                        <div className="row" style={{ margin: '20px' }}>
                                            <div className="col-2">
                                                <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}> Milestone</span>

                                            </div>
                                            <div className="col-6">
                                                <select >
                                                    <option >FY 2016-17 - Phase I - Advocacy, Mobilization And Survey</option>
                                                    <option >FY 2016-17 - Phase I - Advocacy, Mobilization And Survey</option>
                                                    <option >FY 2016-17 - Phase I - Advocacy, Mobilization And Survey</option>
                                                    <option >FY 2016-17 - Phase I - Advocacy, Mobilization And Survey</option>

                                                </select>

                                            </div>
                                            <div className="col-4">
                                                <p>Select a milestone if the comment belongs to a specific milestone</p>

                                            </div>


                                        </div> : null

                                }



                                <div className="container" style={{ marginTop: '2vh' }}>
                                    <div className="row" style={{ margin: '20px' }}>
                                        <div className="col-2">
                                            <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}>Visibility</span>

                                        </div>
                                        <div className="col-10">
                                            <input type="radio" name="price" />
                                            <span style={{ marginLeft: '2%' }}>Collaborators & Admins</span>
                                            <input type="radio" name="price" style={{ marginLeft: '10px' }} />
                                            <span style={{ marginLeft: '2%' }}>Admins Only</span>
                                            <button style={{ color: 'white', backgroundColor: '#c2c2c2', border: '1px solid #c2c2c2', marginTop: '2vh', display: 'block', padding: '10px' }}>Comment</button>

                                        </div>

                                    </div>

                                </div>




                            </div>
                        </ReactBootstrap.Card.Body>
                    </ReactBootstrap.Accordion.Collapse>

                </ReactBootstrap.Accordion>

            </>
        )
    }
}

export default NewUpdate;
