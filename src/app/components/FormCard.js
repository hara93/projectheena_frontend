import React from 'react'
import {Row, Col, Card} from 'react-bootstrap';


function FormCard (props){
    return(
        <>
       
       <Row style={{backgroundColor:'#f1f1f1'}} id="card_body">
        {props.details.map((ele, index) => (
                            
                             <Col md={4}> 
                             <Card className="p-4" style={{marginBottom:'5%'}}>
                             <h5 style={{fontWeight:'600'}}>{ele.heading}</h5>
                             <p>{ele.content}</p>
                 
                             </Card> 
                             </Col>
                             
                        ))}
           
           </Row>
          
        
        

        </>
    )
}

export default FormCard