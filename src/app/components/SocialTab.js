/* eslint-disable no-restricted-imports */
import React, { useEffect } from "react";
import "../../index.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import { useDispatch, useSelector } from "react-redux";
import { getSkillsList } from "../../redux/reducers/common/actionCreator";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
// const social = [
//   {
//     name: "Social Volunteering",
//     value: 42,
//   },
//   {
//     name: "Fundraising",
//     value: 43,
//   },
//   {
//     name: "Community Management/Engagement",
//     value: 44,
//   },
//   {
//     name: "Administrative Policy Work",
//     value: 45,
//   },
//   {
//     name: "Caregivers",
//     value: 46,
//   },
//   {
//     name: "Data Entry",
//     value: 47,
//   },
// ];
const SocialTab = ({ handleChange, toggleState, alreadyChecked }) => {
  const dispatch = useDispatch();
  const common = useSelector((state) => (state.common && state.common.skillsList) || null);
  useEffect(() => {
    dispatch(getSkillsList);
  }, []);
  return (
    <>
      <div class="table-wrapper-scroll-y my-custom-scrollbar">
        <table class="table table-bordered table-striped mb-0">
          <tbody>
            {common.Social.map((ele, idx) => (
              <tr>
                <td>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label={ele.skill_name}
                    value={ele.skill_name}
                    disabled={toggleState}
                    checked={alreadyChecked.includes(ele.skill_name)}
                    onChange={(e) => handleChange(e)}
                    style={{ marginBottom: "0" }}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default SocialTab;
