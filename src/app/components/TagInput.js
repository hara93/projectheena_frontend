import React, { useState } from "react";
import ".././TagInput.css";

const TagsInput = (props) => {
  const [tags, setTags] = React.useState(props.tags);
  const removeTags = (indexToRemove) => {
    setTags([...tags.filter((_, index) => index !== indexToRemove)]);
  };
  const addTags = (event) => {
    if (event.target.value !== "") {
      setTags([...tags, event.target.value]);

      event.target.value = "";
      setValue("");
    }
  };
  const [value, setValue] = useState("");
  return (
    <div className="tags-input">
      <ul id="tags">
        {tags.map((tag, index) => (
          <li key={index} className="tag">
            <span className="tag-title">{tag}</span>
            <span className="tag-close-icon" onClick={() => removeTags(index)}>
              x
            </span>
          </li>
        ))}
      </ul>
      <input
        type="text"
        style={{ backgroundColor: "white" }}
        onChange={(e) => setValue(e.target.value)}
        value={value}
        className="inputa"
        onKeyDown={(event) => (event.keyCode === 188 ? addTags(event) : null)}
      />
    </div>
  );
};

export default TagsInput;
