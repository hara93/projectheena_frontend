
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import CustomButtonOutline from '../components/CustomButtonOutline'


class SingleTableLayout extends React.Component {
    constructor(props) {
        super(props);


    }
    render() {


        return (
            <div>





                {/* <Row className="no-gutters">
                <Col md={12}>
                <Card className="p-5" >
                    <Row>
                    {this.props.projectdetails.map((ele, index) => ( 
                            <Col className="mb-5" md={6}>
                                <Card className="p-5" style={{height:'350px'}}>
                                    <Row>
                                        <h5>{ele.heading}</h5>
                                        <hr style={{backgroundColor:'gainsboro', width:'107%', marginLeft:'-12px'}}/>
                <table border = "1" style={{marginLeft:'8%', marginTop:'6%', marginBottom:'5%'}}>
                <tr>
                <td style={{padding: '2vh 2vw 2vh 2vw', width:'50%', textAlign:'center'}}>{ele.m11}</td>
                <td style={{padding: '2vh 2vw 2vh 2vw', width:'50%', textAlign:'center'}}>{ele.m12}</td>
                </tr>
                            
                <tr>
                <td style={{padding: '2vh 2vw 2vh 2vw', width:'50%', textAlign:'center'}}>{ele.m21}</td>
                <td style={{padding: '2vh 2vw 2vh 2vw', width:'50%', textAlign:'center'}}>{ele.m22}</td>
                </tr>
                {
                    !ele.m31&&!ele.m32? '': 
                    <tr>
                    <td style={{padding: '2vh 2vw 2vh 2vw', width:'50%', textAlign:'center'}}>{ ele.m31}</td>
                    <td style={{padding: '2vh 2vw 2vh 2vw', width:'50%', textAlign:'center'}}>{ ele.m32}</td>
                    </tr>
                }
        
                
                </table>
                                        
                                    </Row>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                </Card>
            </Col>
                    
         </Row> */}
                <Row >
                    {
                        this.props.projectdetails.map(
                            (item, index) => (
                                <div className="boxy-ibox p-3">
                                    <h3 className="ibox-title boxy-ibox-title">{item.heading}</h3>
                                    <div className="ibox-content boxy-ibox-content" style={{ borderRight: '1px solid gainsboro', borderBottom: '1px solid gainsboro', borderLeft: '1px solid gainsboro' }}>
                                        <table className="table table-bordered no-margins" >
                                            <tbody>
                                                <tr className="csr_dashboard">
                                                    <th style={{ fontSize: '13px', fontWeight: '500', color: '#6e7173' }}>{item.m11}</th>
                                                    <td style={{ fontSize: '13px', fontWeight: '500', color: '#6e7173' }}>{item.m12}</td>
                                                </tr>
                                                <tr className="csr_dashboard">
                                                    <th style={{ fontSize: '13px', fontWeight: '500', color: '#6e7173' }}>{item.m21}</th>
                                                    <td style={{ fontSize: '13px', fontWeight: '500', color: '#6e7173' }}>{item.m22}</td>
                                                </tr>
                                                {
                                                    !item.m31 && !item.m32 ? '' :

                                                        <tr className="csr_dashboard">
                                                            <th style={{ fontSize: '13px', fontWeight: '500', color: '#6e7173' }}>{item.m31}</th>
                                                            <td style={{ fontSize: '13px', fontWeight: '500', color: '#6e7173' }}>{item.m32}</td>
                                                        </tr>
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            )
                        )
                    }



                </Row>



            </div>






        )
    }


}
export default SingleTableLayout;