
import React, { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import { Table, Modal, Form, Row, Col } from 'react-bootstrap';
import NewUpdate from './new_update_budget';
import CustomButton from '../components/CustomButton';
import { eventChannel } from '@redux-saga/core';

const Accordion = withStyles({
    root: {
        backgroundColor: '#e7eaec',
        borderBottom: '1px solid #919fa8',
        border: '1px solid rgba(0, 0, 0, .125)',
        boxShadow: 'none',
        '&:not(:last-child)': {
            borderBottom: 0,
        },
        '&:before': {
            display: 'none',
        },
        '&$expanded': {
            margin: 'auto',
        },
    },
    expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
    root: {
        backgroundColor: '#e7eaec',
        borderBottom: '1px solid #919fa8',
        marginBottom: -1,
        minHeight: 56,
        '&$expanded': {
            minHeight: 56,
        },
    },
    content: {
        '&$expanded': {
            margin: '12px 0',
        },
    },
    expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
        backgroundColor: 'white',
        borderBottom: '1px solid #919fa8',
    },
}))(MuiAccordionDetails);

export default function BudgetAccordion(props) {
    const [expanded, setExpanded] = React.useState('panel1');
    const [show, setShow] = useState(false);
    const [show1, setShow1] = useState(false);
    const [unit, setUnit] = useState(0)
    const [qty, setQty] = useState(0)

    const [budget, setBudget] = useState(true)

    const handleClose = () => setShow(false);
    const handleClose1 = () => setShow1(false);
    const handleShow = () => setShow(true);
    const handleShow1 = () => setShow1(true);

    const handleUnit = (e) => {
        setUnit(e.target.value)
    }
    const handleQuantity = (e) => {
        setQty(e.target.value)
    }

    console.log(unit)
    const handleTimes = () => {
        var proceed = window.confirm("Are you sure you want to delete?")
        console.log(proceed)
        if (proceed) {
            setBudget(false)
        }
    }



    const handleChange = (panel) => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    };

    return (
        <div>

            <Accordion square expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
                    <Typography> Partner On-Boarding</Typography>
                </AccordionSummary>
                <AccordionDetails >
                    <Typography style={{ width: '100%' }}>
                        <Table bordered hover responsive="sm" style={{ borderTop: '1px solid gainsboro' }}>
                            <thead>
                                <tr >
                                    <th>Item List</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Category</th>
                                    <th>Total Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    budget == true ?
                                        <tr>

                                            <td>
                                                <div>
                                                    <strong>Support Classes at community</strong>
                                                    <small style={{ display: 'block' }}>4 Classes @ Rs. 4000 pm</small>
                                                </div>
                                            </td>
                                            <td>4</td>
                                            <td> <i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>3600.00</td>
                                            <td>
                                                Human Resource                                                                    </td>
                                            <td><i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>14400.00</td>
                                            <td>
                                                <button class=" btn-outline btn-circle update-breakup" data-toggle="tooltip" title="Update breakup" onClick={handleShow} data-breakup-id="138" data-milestone-id="180"
                                                    style={{ backgroundColor: '#4caf4f', marginRight: '5px', border: '1px solid #4caf4f' }}>
                                                    <i class="fas fa-pencil-alt" style={{ color: 'white' }}></i>
                                                </button>
                                                <button class="btn-outline btn-circle delete-breakup" data-toggle="tooltip" title="Delete breakup" onClick={handleTimes} data-breakup-id="138"
                                                    style={{ backgroundColor: 'red', border: '1px solid red' }}>
                                                    <i class="fa fa-times" style={{ color: 'white' }}></i>
                                                </button>
                                            </td>
                                        </tr>
                                        :
                                        null
                                }

                                <tr>

                                    <td>
                                        <div>
                                            <strong>Support Classes at community</strong>
                                            <small style={{ display: 'block' }}>4 Classes @ Rs. 4000 pm</small>
                                        </div>
                                    </td>
                                    <td>4</td>
                                    <td> <i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>3600.00</td>
                                    <td>
                                        Human Resource                                                                    </td>
                                    <td><i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>14400.00</td>
                                    <td>
                                        <button class=" btn-outline btn-circle update-breakup" data-toggle="tooltip" title="Update breakup"
                                            onClick={handleShow} data-breakup-id="138" data-milestone-id="180"
                                            style={{ backgroundColor: '#4caf4f', marginRight: '5px', border: '1px solid #4caf4f' }}>
                                            <i class="fas fa-pencil-alt" style={{ color: 'white' }}></i>
                                        </button>
                                        <button class="btn-outline btn-circle delete-breakup" data-toggle="tooltip" title="Delete breakup" data-breakup-id="138"
                                            style={{ backgroundColor: 'red', border: '1px solid red' }}>
                                            <i class="fa fa-times" style={{ color: 'white' }}></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <div>
                                            <strong>Support Classes at community</strong>
                                            <small style={{ display: 'block' }}>4 Classes @ Rs. 4000 pm</small>
                                        </div>
                                    </td>
                                    <td>4</td>
                                    <td> <i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>3600.00</td>
                                    <td>
                                        Human Resource                                                                    </td>
                                    <td><i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>14400.00</td>
                                    <td>
                                        <button class=" btn-outline btn-circle update-breakup" data-toggle="tooltip" title="Update breakup"
                                            onClick={handleShow} data-breakup-id="138" data-milestone-id="180"
                                            style={{ backgroundColor: '#4caf4f', marginRight: '5px', border: '1px solid #4caf4f' }}>
                                            <i class="fas fa-pencil-alt" style={{ color: 'white' }}></i>
                                        </button>
                                        <button class="btn-outline btn-circle delete-breakup" data-toggle="tooltip" title="Delete breakup" data-breakup-id="138"
                                            style={{ backgroundColor: 'red', border: '1px solid red' }}>
                                            <i class="fa fa-times" style={{ color: 'white' }}></i>
                                        </button>
                                    </td>
                                </tr>







                            </tbody>

                        </Table>
                        <div style={{ float: 'right' }} onClick={handleShow1}>

                            <button className="p-2 add_budget" >
                                <i class="fas fa-rupee-sign" style={{ color: 'white' }}></i>
                                Add Budget
                            </button>
                        </div>

                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion square expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
                <AccordionSummary aria-controls="panel3d-content" id="panel3d-header">
                    <Typography>Selection Of Districts And Villages</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography style={{ width: '100%' }}>
                        <Table bordered hover responsive="sm">
                            <thead>
                                <tr >
                                    <th>Item List</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Category</th>
                                    <th>Total Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                    <td>
                                        <div>
                                            <strong>Support Classes at community</strong>
                                            <small style={{ display: 'block' }}>4 Classes @ Rs. 4000 pm</small>
                                        </div>
                                    </td>
                                    <td>4</td>
                                    <td> <i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>3600.00</td>
                                    <td>
                                        Human Resource                                                                    </td>
                                    <td><i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>14400.00</td>
                                    <td>
                                        <button class=" btn-outline btn-circle update-breakup" data-toggle="tooltip" title="Update breakup" onClick={handleShow} data-breakup-id="138" data-milestone-id="180" style={{ backgroundColor: '#4caf4f', marginRight: '5px' }}>
                                            <i class="fas fa-pencil-alt" style={{ color: 'white' }}></i>
                                        </button>
                                        <button class="btn-outline btn-circle delete-breakup" data-toggle="tooltip" title="Delete breakup" onClick={handleTimes} data-breakup-id="138" style={{ backgroundColor: 'red' }}>
                                            <i class="fa fa-times" style={{ color: 'white' }}></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <div>
                                            <strong>Support Classes at community</strong>
                                            <small style={{ display: 'block' }}>4 Classes @ Rs. 4000 pm</small>
                                        </div>
                                    </td>
                                    <td>4</td>
                                    <td> <i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>3600.00</td>
                                    <td>
                                        Human Resource                                                                    </td>
                                    <td><i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>14400.00</td>
                                    <td>
                                        <button class=" btn-outline btn-circle update-breakup" data-toggle="tooltip" title="Update breakup" onClick={handleShow} data-breakup-id="138" data-milestone-id="180" style={{ backgroundColor: '#4caf4f', marginRight: '5px' }}>
                                            <i class="fas fa-pencil-alt" style={{ color: 'white' }}></i>
                                        </button>
                                        <button class="btn-outline btn-circle delete-breakup" data-toggle="tooltip" title="Delete breakup" data-breakup-id="138" style={{ backgroundColor: 'red' }}>
                                            <i class="fa fa-times" style={{ color: 'white' }}></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <div>
                                            <strong>Support Classes at community</strong>
                                            <small style={{ display: 'block' }}>4 Classes @ Rs. 4000 pm</small>
                                        </div>
                                    </td>
                                    <td>4</td>
                                    <td> <i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>3600.00</td>
                                    <td>
                                        Human Resource                                                                    </td>
                                    <td><i class="fa fa-rupee" style={{ color: '#6b6e70' }}></i>14400.00</td>
                                    <td>
                                        <button class=" btn-outline btn-circle update-breakup" data-toggle="tooltip" title="Update breakup" onClick={handleShow} data-breakup-id="138" data-milestone-id="180" style={{ backgroundColor: '#4caf4f', marginRight: '5px' }}>
                                            <i class="fas fa-pencil-alt" style={{ color: 'white' }}></i>
                                        </button>
                                        <button class="btn-outline btn-circle delete-breakup" data-toggle="tooltip" title="Delete breakup" data-breakup-id="138" style={{ backgroundColor: 'red' }}>
                                            <i class="fa fa-times" style={{ color: 'white' }}></i>
                                        </button>
                                    </td>
                                </tr>







                            </tbody>

                        </Table>
                        <div style={{ float: 'right' }} onClick={handleShow1}>

                            <button className="p-2 add_budget" >
                                <i class="fas fa-rupee-sign" style={{ color: 'white' }}></i>
                                Add Budget
                            </button>
                        </div>

                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Row style={{ marginTop: '10px', marginBottom: '10px' }}>
                <div className="col-sm-12 centered-column text-right">
                    <hr class="m-b-xxs m-t-xxs"></hr>
                    <hr class="m-b-xxs m-t-xxs"></hr>
                    <h2 class="m-r-md">Grand Total = ₹ 0</h2>
                    <hr class="m-b-xxs m-t-xxs"></hr>
                    <hr class="m-b-xxs m-t-xxs"></hr>


                </div>
            </Row>

            <NewUpdate />

            <Modal show={show} onHide={handleClose} style={{ opacity: '1' }}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Budget breakup</Modal.Title>
                </Modal.Header>
                <Modal.Body >
                    <div className="p-3">
                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700' }}>Budget Title</label>
                        <input type="text" name="title" className="form-control" placeholder="Write your budget title" />
                        <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '7%' }} />

                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700' }}>Description</label>

                        <Form>
                            <Form.Group controlId="exampleForm.ControlTextarea1">

                                <Form.Control as="textarea" rows={5} />
                            </Form.Group>
                        </Form>
                        <span>Max chars:300 </span>


                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700', display: 'block', marginTop: '3vh' }}>Category</label>
                        <select className="form-control" id="budget-category" name="category" aria-required="true" aria-invalid="true">
                            <option value="0">Select a Category</option>
                            <option value="1">Human Resource</option>
                            <option value="2">Material Resourse</option>
                            <option value="3">Travel</option>
                        </select>

                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700', marginTop: '3vh' }}>Unit Price</label>
                        <input type="number" name="title" className="form-control" placeholder="Enter Unit Price" onChange={(e) => handleUnit(e)} />




                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700', marginTop: '3vh' }}>Quantity</label>
                        <input type="number" name="title" className="form-control" placeholder="Enter Quantity" onChange={(e) => handleQuantity(e)} />


                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700', marginTop: '3vh' }}>Total Price</label>
                        <input type="number" name="title" className="form-control" placeholder={qty * unit} style={{ marginBottom: '3vh' }} />




                    </div>



                </Modal.Body>
                <Modal.Footer>
                    <div onClick={handleClose}>
                        {/* <ReactBootstrap.Button variant="secondary">Close</ReactBootstrap.Button> */}
                        <button className="p-2" onClick={handleClose} style={{ backgroundColor: '#c2c2c2', border: '1px solid #c2c2c2', color: 'white' }}>Close</button>
                    </div>
                    {/* <ReactBootstrap.Button variant="primary">Add meta data</ReactBootstrap.Button> */}
                    <button className="p-2" onClick={handleClose} style={{ backgroundColor: '#4caf4f', border: '1px solid #4caf4f', color: 'white' }}>Submit</button>

                </Modal.Footer>
            </Modal>
            <Modal show={show1} onHide={handleClose1} style={{ opacity: '1' }} >
                <Modal.Header closeButton>
                    <Modal.Title>Add Budget breakup</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="p-3">
                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700' }}>Budget Title</label>
                        <input type="text" name="title" className="form-control" placeholder="Write your budget title" />
                        <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '7%' }} />

                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700' }}>Description</label>
                        <Form>
                            <Form.Group controlId="exampleForm.ControlTextarea1">

                                <Form.Control as="textarea" rows={5} />
                            </Form.Group>
                        </Form>
                        <span>Max chars:300 </span>

                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700', display: 'block', marginTop: '3vh' }}>Category</label>
                        <select className="form-control" id="budget-category" name="category" aria-required="true" aria-invalid="true">
                            <option value="0">Select a Category</option>
                            <option value="1">Human Resource</option>
                            <option value="2">Material Resourse</option>
                            <option value="3">Travel</option>
                        </select>

                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700', marginTop: '3vh' }}>Unit Price</label>
                        <input type="number" name="title" className="form-control" placeholder="Enter Unit Price" onChange={handleUnit} />




                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700', marginTop: '3vh' }}>Quantity</label>
                        <input type="number" name="title" className="form-control" placeholder="Enter Quantity" />


                        <label style={{ display: 'inline-block', maxWidth: '100%', marginBottom: '5px', fontWeight: '700', marginTop: '3vh' }}>Total Price</label>
                        <input type="number" name="title" className="form-control" placeholder="Total Price" style={{ marginBottom: '3vh' }} />



                    </div>




                </Modal.Body>
                <Modal.Footer>
                    <div onClick={handleClose1}>
                        {/* <ReactBootstrap.Button variant="secondary">Close</ReactBootstrap.Button> */}
                        <button className="p-2" onClick={handleClose1} style={{ backgroundColor: '#c2c2c2', border: '1px solid #c2c2c2', color: 'white' }}>Close</button>
                    </div>
                    {/* <ReactBootstrap.Button variant="primary">Add meta data</ReactBootstrap.Button> */}
                    <button className="p-2" onClick={handleClose1} style={{ backgroundColor: '#4caf4f', border: '1px solid #4caf4f', color: 'white' }}>Submit</button>

                </Modal.Footer>
            </Modal>
        </div>
    );
}