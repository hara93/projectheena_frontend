/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import "../../index.scss";
import { Table, Row, Col } from "react-bootstrap";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { useDispatch, useSelector } from "react-redux";
import { getCauseList } from "../../redux/reducers/common/actionCreator";

const CheckboxTable = (props) => {
  const dispatch = useDispatch();
  const causeList = useSelector((state) => (state.common && state.common.causeList) || null);

  useEffect(() => {
    if (props.alreadyChecked) {
      setcausessupported(props.alreadyChecked);
    }
  }, [props.alreadyChecked]);

  const [causessupported, setcausessupported] = useState([]); //checked

  useEffect(() => {
    dispatch(getCauseList());
  }, []);

  const handleChange = (e) => {
    if (e.target.checked) {
      props.cause([...causessupported, e.target.value]);
    } else {
      causessupported.splice(causessupported.indexOf(e.target.value), 1);
      props.cause([...causessupported]);
    }
  };

  return (
    <>
      <Table responsive="xl">
        <tbody>
          <Row>
            {causeList &&
              causeList.map((element, index) => (
                <Col xs={4} key={index}>
                  <FormControlLabel
                    id={index}
                    control={<Checkbox color="primary" />}
                    label={element.name}
                    value={element._id}
                    checked={causessupported.includes(element._id)}
                    onChange={(e) => {
                      handleChange(e);
                    }}
                  />
                </Col>
              ))}
          </Row>
        </tbody>
      </Table>
    </>
  );
};
export default CheckboxTable;
