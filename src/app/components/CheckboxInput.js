import React from "react";
// eslint-disable-next-line no-restricted-imports
// import TextField from "@material-ui/core/TextField";
import { FormControl, FormControlLabel, Checkbox } from "@material-ui/core";
// import { InputLabel } from "@material-ui/core";
// import { MenuItem } from "@material-ui/core";

export default function CheckboxInput(props) {
  const { name, label, value, onChange } = props;

  const convertToDefEventPara = (name, value) => ({
    target: {
      name,
      value,
    },
  });

  return (
    <>
      <FormControl>
        <FormControlLabel
          control={
            <Checkbox
              name={name}
              color="primary"
              checked={value}
              onChange={(e) =>
                onChange(convertToDefEventPara(name, e.target.checked))
              }
            />
          }
          label={label}
        ></FormControlLabel>
      </FormControl>
    </>
  );
}
