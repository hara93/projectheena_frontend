import React from "react";
import * as reactstrap from "react-bootstrap";
import { Card } from "react-bootstrap";
import CustomButton from "./CustomButton";

class SurveyForm extends React.Component {
  render() {
    return (
      <>
        <div style={{ marginLeft: "5%", marginRight: "5%", marginTop: "5%" }}>
          <h3 style={{ fontWeight: "600" }}>Create survey form</h3>
          <p style={{ lineHeight: "30px", fontSize: "13px" }}>
            Following form will help you create / edit surveys. Please make sure
            to capture all the questions AND do ask for respondent details
            (Name, contact, etc) if required You are creating survey for "Every
            Child Counts _ A Citizens Campaign"
          </p>
        </div>
        <div style={{ width: "88%", marginLeft: "4vw", marginTop: "3%" }}>
          <Card>
            <reactstrap.Form>
              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Survey Name
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control
                    type="name"
                    placeholder="Enter Survey Name"
                  />
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextPassword"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Survey Description
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Group controlId="exampleForm.ControlTextarea1">
                    <reactstrap.Form.Control
                      as="textarea"
                      rows={6}
                      placeholder="Survey Description"
                    />
                  </reactstrap.Form.Group>
                  <p>Max: 256 chars | Remaining Chars: 255</p>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextPassword"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Opening Message
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Group controlId="exampleForm.ControlTextarea1">
                    <reactstrap.Form.Control
                      as="textarea"
                      rows={6}
                      placeholder="Opening Message"
                    />
                    <p>
                      This is displayed at the start of the survey. Leave it
                      blank to have no opening message.
                    </p>
                  </reactstrap.Form.Group>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextPassword"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Closing Message
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Group controlId="exampleForm.ControlTextarea1">
                    <reactstrap.Form.Control
                      as="textarea"
                      rows={6}
                      placeholder="Closing Message"
                    />
                    <p>
                      This is displayed at the start of the survey. Leave it
                      blank to have no opening message.
                    </p>
                  </reactstrap.Form.Group>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Limit
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control
                    type="number"
                    placeholder="Survey Limit"
                  />
                  <p>
                    The amount of users you would like to limit to taking this
                    survey. Set to 0 to allow for unlimited.
                  </p>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Status
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control as="select">
                    <option>Inactive</option>
                    <option>Active</option>
                    <option>Old</option>
                  </reactstrap.Form.Control>
                  <p>
                    It's a good idea to put your new surveys as inactive until
                    you have finished creating the questionaire.
                  </p>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Milestones
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <CustomButton content={`Add Milestone`} />
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Fill Once
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Group controlId="formBasicCheckbox">
                    <reactstrap.Form.Check type="checkbox" />
                    <p>
                      Checking this box will make it so a user can only submit
                      answers to the Survey once.
                    </p>
                  </reactstrap.Form.Group>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Registered Users Only
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Group controlId="formBasicCheckbox">
                    <reactstrap.Form.Check type="checkbox" />
                    <p>
                      Only users who are logged in and registered will be able
                      to use this survey when this option is checked.
                    </p>
                  </reactstrap.Form.Group>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Private Survey
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Group controlId="formBasicCheckbox">
                    <reactstrap.Form.Check type="checkbox" />
                    <p>
                      Private Surveys can only be access via their URL. A
                      special token is generated for the URL to make it
                      impossible to guess.
                    </p>
                  </reactstrap.Form.Group>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Has Beneficiary
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Group controlId="formBasicCheckbox">
                    <reactstrap.Form.Check type="checkbox" />
                    <p>
                      If selected, each survey response will be mapped to
                      project beneficiaries
                    </p>
                  </reactstrap.Form.Group>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Go Back URL
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control type="name" />
                  <p>
                    When the user finishes the survey, enter the URL that the Go
                    Back link will point to.
                  </p>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Person Responsible
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control type="name" />
                  <p>
                    If specified, only persons responsible will be able to fill
                    surveys.
                  </p>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Survey Frequency
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="5"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control as="select">
                    <option></option>
                    <option>Daily</option>
                    <option>Weekly</option>
                    <option>Monthly</option>
                    <option>Quarterly</option>
                    <option>Yearly</option>
                  </reactstrap.Form.Control>
                  <p>
                    Specifies frequency in which survey owner has to fill
                    surveys.
                  </p>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <div
                style={{
                  marginLeft: "40%",
                  marginTop: "4vh",
                  marginBottom: "4vh",
                }}
              >
                <CustomButton content={`Create Survey`} />
              </div>
            </reactstrap.Form>
          </Card>
        </div>
      </>
    );
  }
}

export default SurveyForm;
