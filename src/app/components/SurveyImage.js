/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import {
  Form,
  Container,
  Row,
  Col,
  OverlayTrigger,
  Tooltip,
  Modal,
} from "react-bootstrap";
import StyledDropzone from "./Dropzone";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const SurveyImage = () => {
  const [shows, setShows] = useState(false);

  const handleClose = () => setShows(false);
  const handleShow = () => setShows(true);
  return (
    <>
      <Container style={{ padding: "15px 0" }}>
        <Form>
          <Form.Group>
            <Row>
              <Col xs={12} md={9}>
                <Form.Group>
                  <Form.Control
                    type="text"
                    placeholder="Enter a Question title"
                    size="lg"
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={9}>
                <Form.Group>
                  <Form.Label>Description</Form.Label>
                  <Form.Control as="textarea" rows={3} size="lg" />
                </Form.Group>
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col xs={12} md={6}>
                <Form.Label>
                  Keyword
                  <OverlayTrigger
                    placement="right"
                    delay={{ show: 100, hide: 100 }}
                    overlay={
                      <Tooltip>
                        A question keyword is an alias or second name for your
                        questions. It makes your data reports simpler to
                        understand and more analysis-friendly.
                      </Tooltip>
                    }
                  >
                    <i
                      class="fas fa-info-circle"
                      style={{ marginLeft: "10px" }}
                    ></i>
                  </OverlayTrigger>
                </Form.Label>
                <Form.Control type="text" />
              </Col>
              <Col xs={12} md={6}>
                <Form.Label> </Form.Label>
                <div className="project-checkbox" style={{ marginTop: "3%" }}>
                  <label for="1">
                    <input type="checkbox" value="1" id="1"></input>
                    Mandatory Question <span style={{ color: "red" }}>*</span>
                  </label>
                </div>
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <button
              type="button"
              onClick={handleShow}
              className="p-3"
              style={{
                backgroundColor: "#4caf4f",
                border: "1px solid #4caf4f",
                color: "white",
                fontWright: "600",
              }}
            >
              <i className="far fa-images" style={{ color: "white" }}></i>Add
              Help Image
            </button>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col xs={12} md={6}>
                <Form.Label style={{ margin: "0" }}>
                  Image Quality
                  <OverlayTrigger
                    placement="right"
                    delay={{ show: 100, hide: 100 }}
                    overlay={
                      <Tooltip>
                        Quality of original image depends on the device camera.
                      </Tooltip>
                    }
                  >
                    <i
                      class="fas fa-info-circle"
                      style={{ marginLeft: "10px" }}
                    ></i>
                  </OverlayTrigger>
                </Form.Label>
                <RadioGroup name="quality" style={{ flexDirection: "row" }}>
                  <FormControlLabel
                    value="Low"
                    control={<Radio color="primary" />}
                    label="Low"
                  />
                  <FormControlLabel
                    value="Medium"
                    control={<Radio color="primary" />}
                    label=" Medium"
                  />
                  <FormControlLabel
                    value="High"
                    control={<Radio color="primary" />}
                    label="High"
                  />
                </RadioGroup>
              </Col>
            </Row>
          </Form.Group>

          <Form.Group>
            <Row>
              <Col xs={12} md={6}>
                <Form.Label style={{ margin: "0" }}>Settings</Form.Label>{" "}
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={6}>
                <FormControlLabel
                  control={<Checkbox color="primary" />}
                  label="  Allow user to select an image from gallery"
                  value="Allow"
                />
              </Col>
            </Row>
          </Form.Group>
        </Form>
      </Container>
      <Modal
        show={shows}
        onHide={handleClose}
        animation={false}
        size="lg"
        style={{ opacity: "1" }}
      >
        <Modal.Header closeButton>
          <Modal.Title className="p-2" style={{ fontWeight: "700" }}>
            Add question help image
          </Modal.Title>
        </Modal.Header>
        <Modal.Body
          style={{
            border: "2px dashed #cbcbcb",
            margin: "5%",
            borderRadius: "25px",
          }}
          className="p-5"
        >
          <i class="fas fa-images fa-3x" style={{ marginLeft: "45%" }}></i>
          <h4
            style={{ color: "#9e9e9e", fontWeight: "700", marginLeft: "35%" }}
          >
            Drop an image here
          </h4>
          <span
            className="p-2"
            style={{
              backgroundColor: "#9e9e9e",
              color: "white",
              borderRadius: "13px",
              marginLeft: "36%",
            }}
          >
            JPEG
          </span>
          <span
            className="p-2"
            style={{
              backgroundColor: "#9e9e9e",
              color: "white",
              borderRadius: "13px",
              marginLeft: "4%",
            }}
          >
            GIF
          </span>
          <span
            className="p-2"
            style={{
              backgroundColor: "#9e9e9e",
              color: "white",
              borderRadius: "13px",
              marginLeft: "4%",
            }}
          >
            PNG
          </span>
          <p
            style={{
              marginLeft: "40%",
              color: "#9e9e9e",
              fontWeight: "900",
              marginTop: "3%",
            }}
          >
            ---------OR---------
          </p>

          <StyledDropzone />
        </Modal.Body>
        <Modal.Footer>
          <button
            className="p-2"
            onClick={handleClose}
            style={{
              backgroundColor: "#c2c2c2",
              border: "1px solid #c2c2c2",
              color: "white",
            }}
          >
            Close
          </button>

          <button
            className="p-2"
            onClick={handleClose}
            style={{
              backgroundColor: "#4caf4f",
              border: "1px solid #4caf4f",
              color: "white",
            }}
          >
            Submit
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default SurveyImage;
