import React, { useState } from "react";
import { Form, Row, Col, Card, Button } from "react-bootstrap";

const AddNewRule = () => {
  const [questions, setQuestions] = useState([{ question: "" }]);

  const handleAddQuestion = () => {
    setQuestions([...questions, { question: "" }]);
  };

  return (
    <>
      <Card style={{ padding: "20px" }}>
        <Row style={{ display: "flex", justifyContent: "space-between" }}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <div style={{ display: "flex" }}>
              <h5 style={{ marginTop: "auto", marginBottom: "auto" }}>
                Popular Question Types
              </h5>
              <div className="survey-questions-title-no">12</div>
            </div>
            <div>
              <Button
                variant="light"
                // onClick={() => {
                //   setDisplay(false);
                // }}
              >
                <i class="fas fa-times"></i>
              </Button>
            </div>
          </div>
        </Row>
        <Row style={{ margin: "0", display: "flex", flexDirection: "column" }}>
          <div>
            <p>Step 1</p>
          </div>

          <div style={{ display: " flex ", alignItems: "center" }}>
            if
            <Form.Control
              as="select"
              style={{ width: "fit-content", margin: "0px 5px 0 5px" }}
            >
              <option>All</option>
              <option>Any</option>
            </Form.Control>
            of the folling conditions are met
          </div>
        </Row>

        <Row style={{ margin: "20px 0 20px 0" }}>
          {questions.map((question, index) => (
            <Card
              style={{
                padding: "10px",
                width: "100%",
                border: "0",
                borderRadius: "0",
                backgroundColor: "#e3e3e3",
              }}
            >
              <Col xs={12}>
                {" "}
                <div style={{ display: "flex" }}>
                  <p>Condition #{index + 1}</p>
                </div>
              </Col>
              <Row>
                <Col xs={10}>
                  <div style={{ width: "100%" }}>
                    <Form.Group>
                      <Form.Control
                        as="select"
                        placeholder="Select a Question"
                        custom
                      >
                        <option></option>
                      </Form.Control>
                    </Form.Group>
                  </div>
                </Col>
                <Col style={{ display: "flex", justifyContent: "left" }}>
                  <div>
                    <button
                      style={{ backgroundColor: "#ff000000", border: "0" }}
                    >
                      <i
                        class="fas fa-ban"
                        style={{ color: "red", fontSize: "20px" }}
                      ></i>
                    </button>
                  </div>
                </Col>
              </Row>
              <div style={{ display: "flex", flexDirection: "row" }}></div>
            </Card>
          ))}
        </Row>

        <Row style={{ margin: "0" }}>
          <button
            className="p-2 survey-btn"
            style={{ marginTop: "10px", marginBottom: "10px" }}
            onClick={handleAddQuestion}
          >
            <i
              className="fas fa-plus-circle add-ques"
              style={{ color: "#4caf4f" }}
            ></i>
            Add Condition
          </button>
        </Row>

        <Row style={{ margin: "0", display: "flex", flexDirection: "column" }}>
          <div>
            <p>Step 2</p>
          </div>
          <div>
            <p>
              Then <b>show</b> the following questions
            </p>
          </div>

          <div
            style={{ display: " flex ", alignItems: "center", width: "100%" }}
          >
            <Form.Control as="select" style={{ margin: "0px 5px 0 0px" }}>
              <option></option>
              <option></option>
            </Form.Control>
          </div>
        </Row>
      </Card>
    </>
  );
};
export default AddNewRule;
