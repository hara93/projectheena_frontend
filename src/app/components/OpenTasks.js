import React from 'react'
import { Row, Col } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';
class OpenTasks extends React.Component {
    render() {
        return (
            <>
                <Card className="p-5">
                    <p className="ngo-latest-title">Open Tasks</p>
                    <hr className="hr_style"></hr>
                    <div className=" my-5 mx-2">
                        {/* <dl>
                            
                                <dt className="mb-3" >
                                    <a className="ngo-latest-list" href="/search" style={{color:'#4CAF50'}}>
                                        <span>
                                            <i class="fa fa-tasks"></i>
                                        </span>
                                        Record Audiobooks - Panchatantra and Jataka Tales
                                    </a>
                                </dt>
                                <hr style={{backgroundColor:'gainsboro', width:'100%',borderStyle: 'dotted',marginTop: '7%'}}/>
                                <dt className="mb-3" >
                                    <a className="ngo-latest-list" href="/search" style={{color:'#4CAF50'}}>
                                        <span>
                                            <i class="fa fa-tasks"></i>
                                        </span>
                                        Record Audiobooks - Regional Fables
                                    </a>
                                </dt>
                         
                        </dl> */}
                        <Row style={{ borderBottom: '1px solid gainsboro', paddingBottom: '6%' }}>
                            <Col md={3}>
                                <img style={{ borderRadius: '50%' }} src={'http://projectheena.in/assets/img/menu/ph/volunteer.png'}></img>
                            </Col>
                            <Col md={9} style={{ color: '#4CAF50', maxWidth: '100%', fontWeight: '700' }}>
                                Record Audiobooks - Panchatantra and Jataka Tales
                            </Col>
                        </Row>
                        <Row style={{ borderBottom: '1px solid gainsboro', marginTop: '4%', paddingBottom: '6%' }}>
                            <Col md={3}>
                                <img style={{ borderRadius: '50%' }} src={'http://projectheena.in/assets/img/menu/ph/volunteer.png'}></img>
                            </Col>
                            <Col md={9} style={{ color: '#4CAF50', maxWidth: '100%', fontWeight: '700' }}>
                                Record Audiobooks - Panchatantra and Jataka Tales
                            </Col>
                        </Row>
                        <Row style={{ borderBottom: '1px solid gainsboro', marginTop: '4%', paddingBottom: '6%' }}>
                            <Col md={3}>
                                <img style={{ borderRadius: '50%' }} src={'http://projectheena.in/assets/img/menu/ph/volunteer.png'}></img>
                            </Col>
                            <Col md={9} style={{ color: '#4CAF50', maxWidth: '100%', fontWeight: '700' }}>
                                Record Audiobooks - Panchatantra and Jataka Tales
                            </Col>
                        </Row>
                        <Row style={{ borderBottom: '1px solid gainsboro', marginTop: '4%', paddingBottom: '6%' }}>
                            <Col md={3}>
                                <img style={{ borderRadius: '50%' }} src={'http://projectheena.in/assets/img/menu/ph/volunteer.png'}></img>
                            </Col>
                            <Col md={9} style={{ color: '#4CAF50', maxWidth: '100%', fontWeight: '700' }}>
                                Record Audiobooks - Panchatantra and Jataka Tales
                            </Col>
                        </Row>
                    </div>
                </Card>
            </>
        )
    }
}
export default OpenTasks