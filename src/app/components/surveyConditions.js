import React, { useEffect, useState } from "react";
import { Card, Row, Col, Form } from "react-bootstrap";
import Select from "react-select";

function SurveyConditions(props) {
  const index = props.index;
  useEffect(() => {
    console.log("index changes", index);
  }, [index]);
  const options = [
    { value: "Chocolate", label: "Chocolate" },
    { value: "Strawberry", label: "Strawberry" },
    { value: "Vanilla", label: "Vanilla" },
  ];

  const criterias = [
    {
      value: "is any of",
      label: "is any of",
    },
    {
      value: "is none of",
      label: "is none of",
    },
  ];
  const [criteriaType, setCriteriaType] = useState(false);
  const showCriteriaType = (e) => {
    const opt = e.target.value;
    opt !== "" ? setCriteriaType(true) : setCriteriaType(false);
  };

  const [choices, setChoices] = useState(false);
  const handleChoices = (e) => {
    // setCriteria1(e.label);
    // criteria1 !== "" ? setChoices(true) : setChoices(false);
    const opt = e.value;
    opt !== "" ? setChoices(true) : setChoices(false);
  };
  return (
    <>
      {/* {props.conditions.map((condition, index) => (  ))} */}
      <Card
        key={props.key}
        style={{
          padding: "10px",
          width: "100%",
          border: "0",
          borderRadius: "0",
          backgroundColor: "#e3e3e3",
        }}
      >
        <Row>
          <Col xs={12}>
            {" "}
            <div style={{ display: "flex" }}>
              <p>Condition #{props.key}</p>
            </div>
          </Col>
          <Col xs={10}>
            <div style={{ width: "100%" }}>
              <Form.Group>
                <Form.Control
                  name={"s1"}
                  as="select"
                  placeholder="Select a Question"
                  custom
                  value={props.ele.s1}
                  onChange={(e) =>
                    props.handleChange(
                      props.ele.id,
                      e && e.target && e.target.name,
                      e && e.target && e.target.value
                    )
                  }
                >
                  <option></option>
                  <option value="Ex Q1. what is your name ?">
                    Ex Q1. what is your name ?
                  </option>
                  <option value="Q2. what is ur age?">
                    Q2. what is ur age?
                  </option>
                </Form.Control>
              </Form.Group>
            </div>
          </Col>
          <Col style={{ display: "flex", justifyContent: "left" }}>
            <div>
              <button
                style={{
                  backgroundColor: "#ff000000",
                  border: "0",
                }}
                // onClick={(e) => removeCondition(e)}
              >
                <i
                  class="fas fa-ban"
                  style={{ color: "red", fontSize: "20px" }}
                ></i>
              </button>
            </div>
          </Col>
        </Row>
        {props.ele.s1 ? (
          <Row>
            <Col xs={4}>
              {/* <Select
                name={"s2_" + index}
                options={criterias}
                closeMenuOnSelect={false}
                placeholder="Criteria Type"
                onChange={(e) => handleChoices(e)}
              /> */}
              <Form.Control
                as="select"
                name={"s2"}
                // options={criterias}
                closeMenuOnSelect={false}
                placeholder="Criteria Type"
                value={props.ele.s2}
                onChange={(e) =>
                  props.handleChange(
                    props.ele.id,
                    e && e.target && e.target.name,
                    e && e.target && e.target.value
                  )
                }
              >
                <option></option>
                {criterias &&
                  criterias.map((ele) => (
                    <option value={ele.value}>{ele.label}</option>
                  ))}
              </Form.Control>
            </Col>
            {props.ele.s2 ? (
              <Col xs={6}>
                <Form.Control
                  as="select"
                  name={"s3"}
                  // options={options}
                  isMulti
                  value={props.ele.s3}
                  closeMenuOnSelect={false}
                  placeholder="select choice"
                  onChange={(e) =>
                    props.handleChange(
                      props.ele.id,
                      e && e.target && e.target.name,
                      e && e.target && e.target.value
                    )
                  }
                >
                  <option></option>
                  {options &&
                    options.map((ele) => (
                      <option value={ele.value}>{ele.label}</option>
                    ))}
                </Form.Control>
              </Col>
            ) : null}
          </Row>
        ) : null}

        <div style={{ display: "flex", flexDirection: "row" }}></div>
      </Card>
    </>
  );
}
export default SurveyConditions;
