/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import { Col, Card, Form } from "react-bootstrap";

import TagAutocomplete from "../components/tagAutocomplete";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const DonationSearchBox = () => {
  const [amount, setAmount] = useState();
  const [causes, setCauses] = useState();
  const [donation, donationType] = useState();
  const [tag, setTag] = useState([]);
  const [ITBenefit, setITBenefit] = useState();
  console.log(ITBenefit);
  const handleBenefit = (e) => {
    if (e.target.checked) {
      setITBenefit(e.target.value);
    } else {
      setITBenefit(0);
    }
  };
  return (
    <>
      <div style={{ padding: "15px 0" }}>
        <Card style={{ padding: "15px" }}>
          <Form>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Min Amount</Form.Label>
              </Form.Row>
              <Col className="p-0">
                <Form.Control
                  className="green-focus"
                  type="number"
                  value={amount}
                  onChange={(e) => setAmount(e.target.value)}
                />
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Causes</Form.Label>
              </Form.Row>
              <Col className="p-0">
                <select
                  className="ph-select green-focus"
                  onChange={(e) => setCauses(e.target.value)}
                >
                  <option value="100">All Causes</option>
                  <option value="1">Arts, Culture & Sports</option>
                  <option value="2">Animal Welfare</option>
                  <option value="3">Children & Youth</option>
                  <option value="4">Community Development</option>
                  <option value="5">Disabled</option>
                  <option value="6">Education</option>
                  <option value="7">Elderly</option>
                  <option value="8">Environment</option>
                  <option value="9">Health Care</option>
                  <option value="10">Poverty & Hunger</option>
                  <option value="11">Reducing Inequality</option>
                  <option value="12">Skill Development & Livelyhood</option>
                  <option value="13">Social Business</option>
                  <option value="14">Water & Sanitation</option>
                  <option value="15">Welfare of War Veterans</option>
                  <option value="16">Women</option>
                </select>
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Donation Type</Form.Label>
              </Form.Row>
              <Col className="p-0">
                <select
                  className="ph-select green-focus"
                  onChange={(e) => donationType(e.target.value)}
                >
                  <option value="0">All Types</option>
                  <option value="1">Periodic</option>
                  <option value="2">One Time</option>
                </select>
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <Form.Row className="m-0">
                <Form.Label className="font-bold">Has Following Skills</Form.Label>
              </Form.Row>
              <Col className="m-0-p-0">
                <TagAutocomplete
                  tags={(tag) => {
                    var dataArr = tag.map((item) => {
                      return [item.name, item];
                    });
                    var maparr = new Map(dataArr);
                    var result = [...maparr.values()];
                    setTag(result);
                  }}
                  tag={tag}
                />
              </Col>
            </Form.Group>
            <Form.Group className="p-tb-5">
              <FormControlLabel
                id={1}
                control={<Checkbox color="primary" />}
                label="IT Benefit"
                value={1}
                onChange={(e) => {
                  handleBenefit(e);
                }}
              />
            </Form.Group>
            <div className="center-content">
              <button type="submit" className="ph-btn-outline">
                Search Donations
              </button>
            </div>
          </Form>
        </Card>
      </div>
    </>
  );
};
export default DonationSearchBox;
