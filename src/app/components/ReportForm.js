import React from "react";
import * as reactstrap from "react-bootstrap";
import { Card } from "react-bootstrap";
import CustomButton from "./CustomButton";

class ReportForm extends React.Component {
  render() {
    return (
      <>
        <div style={{ marginLeft: "5%", marginRight: "5%", marginTop: "5%" }}>
          <h3 style={{ fontWeight: "600" }}>Create Report</h3>
          <p style={{ lineHeight: "30px", fontSize: "13px" }}>
            for Every Child Counts _ A Citizens Campaign
          </p>
        </div>
        <div style={{ width: "88%", marginLeft: "4vw", marginTop: "3%" }}>
          <Card>
            <reactstrap.Form>
              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Report Name
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control
                    type="name"
                    placeholder="Enter  Name"
                  />
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextPassword"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Description
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Group controlId="exampleForm.ControlTextarea1">
                    <reactstrap.Form.Control
                      as="textarea"
                      rows={6}
                      placeholder="Survey Description"
                    />
                  </reactstrap.Form.Group>
                  <p>Max: 256 chars | Remaining Chars: 255</p>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextPassword"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Receiver Name
                </reactstrap.Form.Label>

                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control
                    type="name"
                    placeholder="Enter  Name"
                  />
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextPassword"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Receiver Email
                </reactstrap.Form.Label>

                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control type="email" />
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Mail Frequency
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="5"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control as="select">
                    <option></option>
                    <option>Daily</option>
                    <option>Weekly</option>
                    <option>Monthly</option>
                    <option>Quarterly</option>
                    <option>Yearly</option>
                  </reactstrap.Form.Control>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Questions
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="10"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <CustomButton content={`Select Questions`} />
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <hr
                style={{
                  backgroundColor: "gainsboro",
                  width: "100%",
                  borderStyle: "dotted",
                  marginTop: "2%",
                }}
              />

              <reactstrap.Form.Group
                as={reactstrap.Row}
                controlId="formPlaintextEmail"
              >
                <reactstrap.Form.Label
                  column
                  sm="2"
                  style={{
                    paddingLeft: "3vw",
                    paddingTop: "5vh",
                    color: " #676a6c",
                    fontFamily: "sans-serif",
                  }}
                >
                  Add to Dashboard
                </reactstrap.Form.Label>
                <reactstrap.Col
                  sm="5"
                  style={{ paddingTop: "4vh", paddingRight: "4vw" }}
                >
                  <reactstrap.Form.Control as="select">
                    <option>Yes</option>
                    <option>No</option>
                  </reactstrap.Form.Control>
                </reactstrap.Col>
              </reactstrap.Form.Group>

              <div
                style={{
                  marginLeft: "40%",
                  marginTop: "4vh",
                  marginBottom: "4vh",
                }}
              >
                <CustomButton content={`Create Report`} />
              </div>
            </reactstrap.Form>
          </Card>
        </div>
      </>
    );
  }
}

export default ReportForm;
