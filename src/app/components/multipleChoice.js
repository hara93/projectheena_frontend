/* eslint-disable no-restricted-imports */
import React, { useEffect, useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import {
  Form,
  Container,
  Row,
  Col,
  OverlayTrigger,
  Tooltip,
  Modal,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import StyledDropzone from "./Dropzone";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";

import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const MultipleChoice = (props) => {
  const [show, setShow] = useState();
  const [shows, setShows] = useState(false);
  const [noneofabove, setNoneOfAbove] = useState(false);
  const [allofabove, setAllOfAbove] = useState(false);
  var daksh = "disabled";
  const [optionsM, setOptionsM] = useState();
  const handleOptionShow = () => {
    setOptionsM(true);
    setAnchorEl(null);
  };
  const handleOptionClose = () => setOptionsM(false);

  const handleClose = () => setShows(false);
  const handleShow = () => setShows(true);

  const [deleteM, setDeleteM] = useState();
  const handleDeleteShow = () => {
    setDeleteM(true);
    setAnchorEl(null);
  };
  const handleDeleteClose = () => setDeleteM(false);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClosed = () => {
    setAnchorEl(null);
  };

  const [inputFields, setInputFields] = useState([{ optionLabel: "" }]);
  const [options, setOptions] = useState([{ option: "" }]);

  const handleAddFields = () => {
    setInputFields([...inputFields, { optionLabel: "" }]);
    setOptions([...options, { option: "" }]);
  };
  const handleRemoveFields = (index) => {
    console.log(index);
    const values = [...inputFields];
    const optionValues = [...options];
    values.splice(index, 1);
    optionValues.splice(index, 1);
    setInputFields(values);
    setOptions(optionValues);
  };
  const onChange = (e, index) => {
    const updateInput = inputFields.map((inputField, i) =>
      index === i
        ? Object.assign(inputField, {
            [e.target.name]: e.target.value,
          })
        : inputField
    );
    setInputFields(updateInput);
  };

  const onChangeOption = (e, index) => {
    const updateOption = options.map((option, i) =>
      index === i
        ? Object.assign(option, { [e.target.name]: e.target.value })
        : option
    );
    setOptions(updateOption);
  };

  const lengthOfArray = inputFields.length === 1;
  const defaultState = {
    questionTitle: "",
    question: "",
  };

  const [data, setData] = useState(defaultState);
  const [title, setTitle] = useState([]);
  useEffect(() => {
    console.log(title);
    if (props.stateData) {
      setData({
        ...defaultState,
        questionTitle: props.stateData.questionTitle,
      });
    }
  }, [props.stateData]);

  return (
    <>
      <Container style={{ padding: "15px 0" }}>
        <Form>
          <Row>
            <Col xs={12} md={9}>
              <Form.Group>
                <Form.Control
                  //value={data.questionTitle}
                  // onChange={(e) =>
                  //   setData({
                  //     ...data,
                  //     questionTitle: (e && e.target && e.target.value) || "",
                  //   })
                  // }
                  value={title.title}
                  onChange={(e) => {
                    setTitle({ title: e.target.value });
                  }}
                  type="text"
                  placeholder="Enter a Question title"
                  size="lg"
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={9}>
              <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control as="textarea" rows={3} size="lg" />
              </Form.Group>
            </Col>
          </Row>

          <Form.Group>
            <Row>
              <Col xs={12} md={6}>
                <Form.Label>
                  Keyword
                  <OverlayTrigger
                    placement="bottom"
                    delay={{ show: 100, hide: 100 }}
                    overlay={
                      <Tooltip>
                        A question keyword is an alias or second name for your
                        questions. It makes your data reports simpler to
                        understand and more analysis-friendly.
                      </Tooltip>
                    }
                  >
                    <i
                      class="fas fa-info-circle"
                      style={{ marginLeft: "10px" }}
                    ></i>
                  </OverlayTrigger>
                </Form.Label>
                <Form.Control type="text" />
              </Col>
              <Col xs={12} md={6}>
                <Form.Label> </Form.Label>
                <div className="project-checkbox" style={{ marginTop: "3%" }}>
                  <label for="1">
                    <input type="checkbox" value="1" id="1"></input>
                    Mandatory Question <span style={{ color: "red" }}>*</span>
                  </label>
                </div>
              </Col>
            </Row>
          </Form.Group>

          <Form.Group>
            <button
              type="button"
              onClick={handleShow}
              className="p-3"
              style={{
                backgroundColor: "#4caf4f",
                border: "1px solid #4caf4f",
                color: "white",
                fontWright: "600",
              }}
            >
              <i className="far fa-images" style={{ color: "white" }}></i>Add
              Help Image
            </button>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col md={8}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <div>
                    {" "}
                    <span style={{ fontSize: "16px", fontWeight: "600" }}>
                      Options
                    </span>
                  </div>
                  <div>
                    <span style={{ fontSize: "16px", fontWeight: "600" }}>
                      Sort Options
                    </span>
                  </div>
                </div>
                <DragDropContext
                  onDragEnd={(...props) => {
                    console.log(props);
                  }}
                >
                  <div>
                    <Droppable droppableId="dropable-1">
                      {(provided, _) => (
                        <div
                          {...provided.droppableProps}
                          ref={provided.innerRef}
                        >
                          {inputFields.map((inputField, index) => (
                            <Draggable
                              key={inputField.id}
                              draggableId={"dragable -" + inputField.id}
                              index={index}
                            >
                              {(provided, snapshot) => (
                                <div
                                  {...provided.draggableProps}
                                  ref={provided.innerRef}
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    width: "100%",
                                  }}
                                >
                                  {/* <div
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    width: "100%",
                                  }}
                                > */}
                                  <div
                                    style={{ margin: "10px 0 10px 0" }}
                                    {...provided.dragHandleProps}
                                  >
                                    <i
                                      class="fas fa-ellipsis-v"
                                      style={{ margin: "8px" }}
                                    ></i>
                                  </div>
                                  <div
                                    style={{
                                      width: "100%",
                                      margin: "10px 0 10px 0",
                                    }}
                                  >
                                    <Form.Control
                                      type="text"
                                      placeholder="Enter option label"
                                      name="optionLabel"
                                      onChange={(e) => onChange(e, index)}
                                      value={inputField.optionLabel}
                                    />
                                  </div>

                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                    }}
                                  >
                                    {/* to add more rows */}
                                    <Button
                                      variant="light"
                                      onClick={() => handleAddFields()}
                                    >
                                      <i class="fas fa-plus"></i>
                                    </Button>
                                    {lengthOfArray ? null : (
                                      <Button
                                        variant="light"
                                        onClick={() =>
                                          handleRemoveFields(index)
                                        }
                                      >
                                        <i class="far fa-trash-alt"></i>
                                      </Button>
                                    )}
                                    {/* to delete a row */}
                                  </div>
                                  {/* </div> */}
                                </div>
                              )}
                            </Draggable>
                          ))}
                          {provided.placeholder}
                        </div>
                      )}
                    </Droppable>
                  </div>
                </DragDropContext>
              </Col>
              <Col>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginBottom: "5px",
                  }}
                >
                  <div style={{ fontSize: "16px", fontWeight: "600" }}>
                    <Form.Check
                      type="switch"
                      id="custom-switch"
                      size="lg"
                      label="Show option code"
                      onChange={(e) => setShow(e.target.checked)}
                    />
                  </div>
                  <Button
                    aria-controls="simple-menu"
                    aria-haspopup="true"
                    onClick={handleClick}
                    style={{ paddingTop: "0", paddingBottom: "0" }}
                  >
                    <i class="fas fa-ellipsis-v"></i>
                  </Button>
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClosed}
                  >
                    <MenuItem>
                      {" "}
                      <FormControlLabel
                        control={<Checkbox name="randomize" />}
                        label="Randomize options on app"
                      />
                    </MenuItem>
                    <MenuItem onClick={handleDeleteShow}>
                      {" "}
                      <i
                        class="far fa-trash-alt"
                        style={{ marginRight: "10px", marginLeft: "2px" }}
                      ></i>{" "}
                      Show deleted options
                    </MenuItem>
                    <MenuItem onClick={handleOptionShow}>
                      <i
                        class="far fa-file-alt"
                        style={{ marginRight: "10px", marginLeft: "4px" }}
                      ></i>
                      Add options in bulk
                    </MenuItem>
                  </Menu>
                </div>
                {show ? (
                  <div>
                    {options.map((option, index) => (
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          width: "100%",
                        }}
                        key={index}
                      >
                        <div style={{ width: "100%", margin: "10px 0 10px 0" }}>
                          <Form.Control
                            type="text"
                            placeholder="Option code"
                            name="option"
                            onChange={(e) => onChangeOption(e, index)}
                            value={option.option}
                          />
                        </div>
                      </div>
                    ))}
                  </div>
                ) : null}
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col>
                <div>
                  {" "}
                  <span style={{ fontSize: "16px", fontWeight: "600" }}>
                    Special Options
                  </span>
                </div>{" "}
              </Col>
            </Row>
            <Row>
              <Col>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: "10px",
                  }}
                >
                  <Checkbox
                    color="secondary"
                    checked={noneofabove}
                    onClick={(e) => setNoneOfAbove(e.target.checked)}
                  />
                  {allofabove ? (
                    <TextField
                      id="outlined-full-width"
                      label="All of the above"
                      defaultValue="All of the above"
                      size="small"
                      style={{ margin: 0 }}
                      placeholder="Enter special label option"
                      fullWidth
                      required
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="outlined"
                    />
                  ) : (
                    <span>Add "All of the above"</span>
                  )}
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: "10px",
                  }}
                >
                  <Checkbox
                    color="secondary"
                    checked={allofabove}
                    onClick={(e) => setAllOfAbove(e.target.checked)}
                  />
                  {noneofabove ? (
                    <TextField
                      id="outlined-full-width"
                      label="None of the above"
                      defaultValue="None of the above"
                      size="small"
                      style={{ margin: 0 }}
                      placeholder="Enter special label option"
                      fullWidth
                      required
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="outlined"
                    />
                  ) : (
                    <span>Add "None of the above"</span>
                  )}
                </div>
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col>
                <div>
                  {" "}
                  <span style={{ fontSize: "16px", fontWeight: "600" }}>
                    Number of options data collector can select
                  </span>
                </div>{" "}
              </Col>
            </Row>
            <Row>
              <Col>
                <div style={{ display: "flex", maxWidth: "225px" }}>
                  <InputGroup style={{ marginRight: "10px" }}>
                    <InputGroup.Text style={{ borderRadius: "0" }}>
                      Min
                    </InputGroup.Text>
                    <FormControl
                      type="number"
                      placeholder=""
                      defaultValue="1"
                      disabled={allofabove}
                      style={{ borderRadius: "0" }}
                    />
                  </InputGroup>

                  <InputGroup>
                    <InputGroup.Text style={{ borderRadius: "0" }}>
                      Max
                    </InputGroup.Text>
                    <FormControl
                      type="number"
                      placeholder=""
                      disabled={noneofabove}
                      defaultValue="1"
                      style={{ borderRadius: "0" }}
                    />
                  </InputGroup>
                </div>
              </Col>
            </Row>
          </Form.Group>
        </Form>
      </Container>

      <Modal
        show={shows}
        onHide={handleClose}
        animation={false}
        size="lg"
        style={{ opacity: "1" }}
      >
        <Modal.Header closeButton>
          <Modal.Title className="p-2" style={{ fontWeight: "700" }}>
            Add question help image
          </Modal.Title>
        </Modal.Header>
        <Modal.Body
          style={{
            border: "2px dashed #cbcbcb",
            margin: "5%",
            borderRadius: "25px",
          }}
          className="p-5"
        >
          <i class="fas fa-images fa-3x" style={{ marginLeft: "45%" }}></i>
          <h4
            style={{ color: "#9e9e9e", fontWeight: "700", marginLeft: "35%" }}
          >
            Drop an image here
          </h4>
          <span
            className="p-2"
            style={{
              backgroundColor: "#9e9e9e",
              color: "white",
              borderRadius: "13px",
              marginLeft: "36%",
            }}
          >
            JPEG
          </span>
          <span
            className="p-2"
            style={{
              backgroundColor: "#9e9e9e",
              color: "white",
              borderRadius: "13px",
              marginLeft: "4%",
            }}
          >
            GIF
          </span>
          <span
            className="p-2"
            style={{
              backgroundColor: "#9e9e9e",
              color: "white",
              borderRadius: "13px",
              marginLeft: "4%",
            }}
          >
            PNG
          </span>
          <p
            style={{
              marginLeft: "40%",
              color: "#9e9e9e",
              fontWeight: "900",
              marginTop: "3%",
            }}
          >
            ---------OR---------
          </p>

          <StyledDropzone />
        </Modal.Body>
        <Modal.Footer>
          <button
            className="p-2"
            onClick={handleClose}
            style={{
              backgroundColor: "#c2c2c2",
              border: "1px solid #c2c2c2",
              color: "white",
            }}
          >
            Close
          </button>

          <button
            className="p-2"
            onClick={handleClose}
            style={{
              backgroundColor: "#4caf4f",
              border: "1px solid #4caf4f",
              color: "white",
            }}
          >
            Submit
          </button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={deleteM}
        onHide={handleDeleteClose}
        animation={false}
        size="lg"
        style={{ opacity: "1" }}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title className="p-2" style={{ fontWeight: "700" }}>
            Delete Choice Options
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            style={{
              minHeight: "200px",
              maxWidth: "570px",
            }}
          >
            <p>
              Choice option which you have deleted in between your data
              collection will appear here. You can change the code of the
              deleted option.
            </p>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            className="p-2"
            onClick={handleDeleteClose}
            style={{
              backgroundColor: "#4caf4f",
              border: "1px solid #4caf4f",
              color: "white",
            }}
          >
            Save
          </button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={optionsM}
        onHide={handleOptionClose}
        animation={false}
        size="lg"
        style={{ opacity: "1" }}
      >
        <Modal.Header closeButton>
          <Modal.Title className="p-2" style={{ fontWeight: "700" }}>
            Delete Choice Options
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <p>Enter each choice on a separate line.</p>
          </div>
          <div>
            <Form.Control as="textarea" rows={10} size="lg" />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            className="p-2"
            onClick={handleOptionClose}
            style={{
              backgroundColor: "#c2c2c2",
              border: "1px solid #c2c2c2",
              color: "white",
            }}
          >
            Close
          </button>

          <button
            className="p-2"
            onClick={handleOptionClose}
            style={{
              backgroundColor: "#4caf4f",
              border: "1px solid #4caf4f",
              color: "white",
            }}
          >
            Add
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default MultipleChoice;
