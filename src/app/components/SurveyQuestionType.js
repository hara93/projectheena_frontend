import React from "react";
import { Row, Col, Card } from "react-bootstrap";

function QuestionType(props) {
  return (
    <>
      {props.bar.map((ele, index) => (
        <Row
          style={{ backgroundColor: "#f1f1f1", margin: "10px 0" }}
          id="card_body"
        >
          <Col md={2} style={{ display: "flex", alignItems: "center" }}>
            <span className="p-2" style={{ backgroundColor: "white" }}>
              {ele.number}
            </span>
          </Col>
          <Col
            style={{
              fontSize: "13px",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <div>{ele.name}</div>
            <div>
              <h4
                style={{
                  color: "#b8b8b7",
                  fontWeight: "600",
                  fontSize: "16px",
                  margin: "0",
                }}
              >
                {ele.value}
              </h4>
            </div>
          </Col>
        </Row>
      ))}
    </>
  );
}

export default QuestionType;
