/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import ReactDOM from "react-dom";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Form } from "react-bootstrap";
import Button from "@material-ui/core/Button";

const initial = Array.from({ length: 10 }, (v, k) => k).map((k) => {
  const custom = {
    id: `id-${k}`,
    content: `List content ${k}`,
  };

  return custom;
});

const grid = 8;
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

function Quote({ quote, index }) {
  return (
    <Draggable draggableId={quote.id} index={index}>
      {(provided) => (
        <div
          className="drag-test-div"
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          {quote.content}
        </div>
      )}
    </Draggable>
  );
}

const QuoteList = React.memo(function QuoteList({ quotes }) {
  return quotes.map((quote, index) => (
    <Quote quote={quote} index={index} key={quote.id} />
  ));
});

function DragTest() {
  const [state, setState] = useState({ quotes: initial });
  const [inputFields, setInputFields] = useState([{ optionLabel: "" }]);
  const handleAddFields = () => {
    setInputFields([...inputFields, { optionLabel: "" }]);
    // setOptions([...options, { option: "" }]);
  };
  const handleRemoveFields = (index) => {
    console.log(index);
    const values = [...inputFields];
    // const optionValues = [...options];
    values.splice(index, 1);
    // optionValues.splice(index, 1);
    setInputFields(values);
    // setOptions(optionValues);
  };
  const onChange = (e, index) => {
    const updateInput = inputFields.map((inputField, i) =>
      index === i
        ? Object.assign(inputField, {
            [e.target.name]: e.target.value,
          })
        : inputField
    );
    setInputFields(updateInput);
  };
  const lengthOfArray = inputFields.length === 1;

  function onDragEnd(result) {
    if (!result.destination) {
      return;
    }

    if (result.destination.index === result.source.index) {
      return;
    }

    const quotes = reorder(
      state.quotes,
      result.source.index,
      result.destination.index
    );

    setState({ quotes });
  }

  return (
    <>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="list">
          {(provided) => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              <QuoteList quotes={state.quotes} />
              <div>
                {inputFields.map((inputField, index) => (
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      width: "100%",
                    }}
                  >
                    <div
                      style={{ margin: "10px 0 10px 0" }}
                      {...provided.dragHandleProps}
                    >
                      <i
                        class="fas fa-ellipsis-v"
                        style={{ margin: "8px" }}
                      ></i>
                    </div>
                    <div
                      style={{
                        width: "100%",
                        margin: "10px 0 10px 0",
                      }}
                    >
                      <Form.Control
                        type="text"
                        placeholder="Enter option label"
                        name="optionLabel"
                        onChange={(e) => onChange(e, index)}
                        value={inputField.optionLabel}
                      />
                    </div>

                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                      }}
                    >
                      {/* to add more rows */}
                      <Button variant="light" onClick={() => handleAddFields()}>
                        <i class="fas fa-plus"></i>
                      </Button>
                      {lengthOfArray ? null : (
                        <Button
                          variant="light"
                          onClick={() => handleRemoveFields(index)}
                        >
                          <i class="far fa-trash-alt"></i>
                        </Button>
                      )}
                      {/* to delete a row */}
                    </div>
                    {/* </div> */}
                  </div>
                ))}
              </div>

              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </>
  );
}

export default DragTest;
