import React from 'react';
import { Table } from 'react-bootstrap';


class Checkbox extends React.Component {
    constructor() {
        super();
        this.state = {
            access: false,
            index: 0
        }
    }

    handleChange = (event, index) => {
        console.log("index value", index, !this.props.collab[index].access)
        // collab[index].access
        this.setState(prevState => ({
            //  access: !prevState.access,
            access: !this.props.collab[index].access,
            index: index
        }));
        this.props.collab[index].access = !this.props.collab[index].access
        console.log("check value", this.state.access, this.state.index)
    }


    render() {
        return (
            <>


                <Table bordered responsive="sm">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Module</th>
                            <th>Access</th>
                            <th>Participate</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            <th>Create</th>
                        </tr>
                    </thead>
                    <tbody>

                        {this.props.collab.map((ele, index) => (

                            <tr>
                                <td>{ele.sr_no}</td>
                                <td>{ele.module}</td>
                                <td><input type="checkbox" id={index} value="Access"
                                    onChange={(e) => this.handleChange(e, index)}
                                // onChange={this.handleChange} 
                                /></td>

                                {
                                    this.props.collab[index].access == true ? <>
                                        <td><input type="checkbox" id={index} disabled={false} /></td>
                                        <td><input type="checkbox" id={index} disabled={false} /></td>
                                        <td><input type="checkbox" id={index} disabled={false} /></td>
                                        <td><input type="checkbox" id={index} disabled={false} /></td>

                                    </>
                                        :
                                        <>
                                            <td><input type="checkbox" id={index} disabled={true} /></td>
                                            <td><input type="checkbox" id={index} disabled={true} /></td>
                                            <td><input type="checkbox" id={index} disabled={true} /></td>
                                            <td><input type="checkbox" id={index} disabled={true} /></td>
                                        </>

                                }


                            </tr>


                        ))}

                    </tbody>
                </Table>

            </>
        )
    }
}

export default Checkbox;




{/* {
                    this.state.access==true?  <td><input type="checkbox" disabled={false} /></td>:  <td><input type="checkbox" disabled={true} /></td>
                }                
                {
                   this.state.access==true?  <td><input type="checkbox" disabled={false} /></td>:  <td><input type="checkbox" disabled={true} /></td>
                }
                {
                   this.state.access==true?  <td><input type="checkbox" disabled={false} /></td>:  <td><input type="checkbox" disabled={true} /></td>
                } */}