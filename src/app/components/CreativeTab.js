/* eslint-disable no-restricted-imports */
import React, { useEffect } from "react";
import "../../index.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { useDispatch, useSelector } from "react-redux";
import { getSkillsList } from "../../redux/reducers/common/actionCreator";
// const creative = [
//   {
//     name: "Teaching / training / coaching",
//     value: 22,
//   },
//   {
//     name: "Content Writing/Blogging/Copywriting",
//     value: 23,
//   },
//   {
//     name: "Social Media",
//     value: 24,
//   },
//   {
//     name: "Illustration/Design/Drawing",
//     value: 25,
//   },
//   {
//     name: "Photography",
//     value: 26,
//   },
//   {
//     name: "Videography",
//     value: 27,
//   },
//   {
//     name: "Brainstorming/Ideation",
//     value: 28,
//   },
//   {
//     name: "Multimedia/Animation",
//     value: 29,
//   },
//   {
//     name: "Consulting/Strategy",
//     value: 30,
//   },
//   {
//     name: "Strategy",
//     value: 31,
//   },
//   {
//     name: "Arts",
//     value: 32,
//   },
//   {
//     name: "Culinary",
//     value: 33,
//   },
//   {
//     name: "Entrepreneurship",
//     value: 34,
//   },
//   {
//     name: "Fasion and Style",
//     value: 35,
//   },
//   {
//     name: "Music",
//     value: 36,
//   },
//   {
//     name: "Modeling/Acting/Compering",
//     value: 37,
//   },
//   {
//     name: "Presentation",
//     value: 38,
//   },
//   {
//     name: "Dancing",
//     value: 39,
//   },
//   {
//     name: "Fashion & Beauty",
//     value: 40,
//   },
//   {
//     name: "Innovation/Out Of box Ideas",
//     value: 41,
//   },
// ];
const CreativeTab = ({ handleChange, toggleState, alreadyChecked }) => {
  const dispatch = useDispatch(getSkillsList);
  const common = useSelector((state) => (state.common && state.common.skillsList) || null);
  useEffect(() => {
    dispatch(getSkillsList());
  }, []);
  return (
    <>
      <div class="table-wrapper-scroll-y my-custom-scrollbar">
        <table class="table table-bordered table-striped mb-0">
          <tbody>
            {common.Creative.map((ele, idx) => (
              <tr>
                <td>
                  <FormControlLabel
                    control={<Checkbox color="primary" />}
                    label={ele.skill_name}
                    value={ele.skill_name}
                    disabled={toggleState}
                    checked={alreadyChecked.includes(ele.skill_name)}
                    onChange={(e) => handleChange(e)}
                    style={{ marginBottom: "0" }}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default CreativeTab;
