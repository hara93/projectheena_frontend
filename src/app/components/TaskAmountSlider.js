import React, { useState } from "react";
import { RangeStepInput } from "react-range-step-input";

function TaskAmountSlider(props) {
    const value = props.donationAmount;
    const [amount, setDonationAmount] = useState(value);

    const handleChange = (e) => {
        const newVal = e.target.value;
        setDonationAmount(newVal);
        props.amount(newVal);
    };
    return (
        <>
            <RangeStepInput
                min={1}
                max={33}
                value={amount}
                step={1}
                onChange={(e) => handleChange(e)}
            />
        </>
    );
}
export default TaskAmountSlider;
