import React from "react";
import { Col, Row, OverlayTrigger, Tooltip, Card } from "react-bootstrap";
// import { Card } from "@material-ui/core";
import { Images } from "../config/Images";
// import CustomButtonOutline from "./CustomButtonOutline";
// import CustomButton from "./CustomButton";
import TurndownService from "turndown";
import ReactMarkdown from "react-markdown";
function SearchTaskCard(props) {
  var turndown = new TurndownService();
  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      This is a continuous task
    </Tooltip>
  );
  return (
    <>
      <Row className="m-0">
        {props.task.map((ele, index) => (
          <Col md={6}>
            <Card className="searchTask-card">
              <Row className="m-0">
                <Col xs={8} className="m-0-p-0">
                  <div style={{ padding: "15px 10px 5px 10px" }}>
                    <h4 className="font-bold" style={{ fontSize: "14px" }}>
                      {/* href={`/createProjects/${ele.project_name}`} */}
                      <a href={`/task-detail/${ele._id}`} className="ph-link">
                        {ele.task_name}
                      </a>
                    </h4>
                    <p style={{ color: "gray", minHeight: "130px" }}>
                      <ReactMarkdown
                        children={turndown.turndown(ele.task_excerpt.substring(0, 256))}
                      />
                      {/* {ele.task_excerpt.substring(0, 256)} */}
                    </p>
                  </div>
                </Col>
                <Col
                  xs={4}
                  className="m-0-p-0"
                  style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
                >
                  <div style={{ padding: "15px 10px 10px 10px " }}>
                    <img
                      style={{ maxHeight: "100px", maxWidth: "100px" }}
                      src={Images.View_Profile}
                      alt=""
                    ></img>
                  </div>
                  <div>
                    <button className="ph-btn" style={{ whiteSpace: "nowrap" }}>
                      Count me in!
                    </button>
                  </div>
                </Col>
              </Row>
              {/* class="flex-container task-info"   */}
              <div
                style={{
                  display: "flex",
                  textAlign: "center",
                  justifyContent: "space-between",
                  color: "gray",
                  padding: "0 10px",
                }}
              >
                <div style={{ display: "flex", flexDirection: "column" }}>
                  Maximum Duration
                  <p>{ele.task_duration_hours}</p>
                </div>
                <div style={{ display: "flex", flexDirection: "column" }}>
                  Max People Required
                  <p>{ele.people_required}</p>
                </div>
                <div style={{ display: "flex", flexDirection: "column" }}>
                  Apply Before
                  <OverlayTrigger
                    placement="bottom"
                    delay={{ show: 250, hide: 400 }}
                    overlay={renderTooltip}
                  >
                    <p>&#8734;</p>
                  </OverlayTrigger>
                </div>
              </div>
            </Card>
          </Col>
        ))}
      </Row>
    </>
  );
}
export default SearchTaskCard;
