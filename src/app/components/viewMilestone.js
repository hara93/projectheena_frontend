import React from 'react';
import { Col, Row, Table, Dropdown, Modal, Button } from "react-bootstrap";


import { FaFacebook, FaTwitterSquare, FaGooglePlus, FaYoutube, FaThumbsUp, FaTwitter } from 'react-icons/fa';
import { Images } from '../config/Images';
import { Card } from 'react-bootstrap';
import {
    FaShieldAlt
} from "react-icons/fa";

class ViewMilestone extends React.Component {
    constructor() {
        super();
        this.state = {
            show: false,
            show1: false
        }
    }


    render() {


        return (
            <>
                {/* <div className="tab-details" > */}

                <Card className="ibox p-3">

                    <Row>


                        <Col md={8}>
                            <div className="item">
                                <h3 className="csr_heading"><i class="fas fa-road"></i>	Partner On-Boarding</h3>
                            </div>
                            <p>On-boarding of NGO partners who will assist in executing the project.On-boarding of NGO partners who will assist in executing the project. </p>



                            <div className="item project-stats">
                                <h3 className="csr_heading" style={{ marginLeft: '2px', marginTop: '32px' }}><i class="fas fa-tag"></i>    Other Statistics</h3>






                                <Table striped bordered hover>

                                    <tbody>
                                        <tr>
                                            <td style={{ fontWeight: '600', color: '#676a6c' }}>Estimated Start and End Date</td>
                                            <td style={{ fontWeight: '500', color: '#676a6c' }}>18th May 2018- 10th June 2018</td>

                                        </tr>
                                        <tr>
                                            <td style={{ fontWeight: '600', color: '#676a6c' }}>Actual Start and End Date</td>
                                            <td style={{ fontWeight: '500', color: '#676a6c' }}>28th May 2018- 8th Feb-2019</td>

                                        </tr>

                                    </tbody>
                                </Table>
                            </div>







                        </Col>
                        <Col md={4}>
                            <div>
                                <h3 className="csr_heading"><i class="fas fa-chart-line"></i> Statistics </h3>
                                <ul className="donation-stats">
                                    <li
                                        style={{
                                            display: "table-row",
                                            listStyleType: "none",
                                        }}
                                    >
                                        <div className="th">Checklist Achieved</div>
                                        <div className="tc"><span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>Yes</span></div>
                                    </li>
                                    <li
                                        style={{
                                            display: "table-row",
                                            listStyleType: "none",
                                        }}
                                    >
                                        <div className="th">Milestone Health</div>
                                        <div className="tc"><span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>Good</span></div>
                                    </li>
                                    <li
                                        style={{
                                            display: "table-row",
                                            listStyleType: "none",
                                        }}
                                    >
                                        <div className="th">Milestone Status</div>
                                        <div className="tc"><span style={{ backgroundColor: '#4caf4f', color: 'white', border: '1px solid #4caf4f', fontSize: '11px' }}>Achieved</span></div>
                                    </li>

                                </ul>
                            </div>





                        </Col>
                    </Row>

                </Card>






            </>
        )
    }
}

export default ViewMilestone;