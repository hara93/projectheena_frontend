import React from 'react';
import TextField from '@material-ui/core/TextField';
import { FormControl, RadioGroup, Radio, FormControlLabel, FormLabel } from '@material-ui/core';


export default function Input(props) {
    const { name, label, value, onChange } = props


    return (
        <>
            <TextField
                variant="outlined"
                label={label}
                name={name}
                value={value}
                style={{ width: '100%', marginTop: '3%' }}
                className="p-2"

                onChange={onChange}
            />
        </>
    )
}