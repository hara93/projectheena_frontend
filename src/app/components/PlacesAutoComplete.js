import React, { Component } from "react";
import "../../index.scss";
import PlacesAutocomplete, {
  geocodeByAddress,
  geocodeByPlaceId,
  getLatLng,
} from "react-places-autocomplete";

export class PlacesAutoComplete extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = { address: "" };
  // }

  // handleChange = (address) => {
  //   console.log("address", address)
  //   this.setState({ address });
  // };
  constructor(props) {
    super(props);
    this.state = { address: "" };
  }
  componentDidMount = () => {
    // console.log("address", this.props.address);
    this.setState({ address: this.props.address });
    if (this.props.address == "Offline") {
      this.setState({ address: "" });
    }
  };
  handleChange = (address) => {
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        this.props.lat(latLng.lat);
        this.props.lng(latLng.lng);
      })
      .catch((error) => console.error("Error", error));

    this.setState({ address });
    this.props.location(address);
  };

  // handleSelect = address => {
  //   geocodeByAddress(address)
  //     .then(results => getLatLng(results[0]))
  //     .then(latLng => console.log('Success', latLng))
  //     .catch(error => console.error('Error', error));
  // };
  render() {
    const searchOptions = {
      types: ["(cities)"],
      componentRestrictions: { country: "in" },
    };
    return (
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
        searchOptions={searchOptions}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            <input
              {...getInputProps({
                placeholder:
                  !this.props.placeholder || this.props.placeholder == ""
                    ? "Enter your city"
                    : this.props.placeholder,
                // placeholder: "Enter your city",
                className: "location-search-input green-focus",
              })}
            />
            <div className="autocomplete-dropdown-container">
              {loading && <div>Loading...</div>}
              {suggestions.map((suggestion) => {
                const className = suggestion.active ? "suggestion-item--active" : "suggestion-item";
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: "#fafafa", cursor: "pointer" }
                  : { backgroundColor: "#ffffff", cursor: "pointer" };
                return (
                  <div
                    style={{ maring: "10px 0 " }}
                    className="input-suggestion"
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                  >
                    <i
                      class="fas fa-map-marker-alt"
                      style={{ color: "inherit", paddingRight: "5px" }}
                    ></i>
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}
export default PlacesAutoComplete;
