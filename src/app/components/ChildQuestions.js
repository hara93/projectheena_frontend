import React, { useState } from 'react';
import CustomButton from './CustomButton';
import { Modal, Button, Form, Col, Row } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";

import PlacesAutoComplete from '../components/PlacesAutoComplete';
import Example from "../components/Phone";
import SignatureCanvas from 'react-signature-canvas'
import camera from "./TakeAPicture";

import { CompareArrowsOutlined } from "@material-ui/icons";


export default function ChildQuestion(props) {
    const [show, setShow] = useState(false);
    const [save, setSave] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleSave = () => {
        setSave(true)
    }




    return (
        <>
            {
                props.member.map(
                    (item, index) => (
                        <>
                            <div style={{ marginBottom: '3%', marginRight: '5%' }} >
                                {/* <CustomButton content={`Add Detail about ${index + 1} family member`} /> */}
                                {
                                    save == true ?
                                        <button type="button" className="p-3" onClick={handleShow} style={{ backgroundColor: '#4caf4f', border: '1px solid #4caf4f', color: 'white', fontWeight: '700' }}>Family member: {index + 1}</button> :
                                        <button type="button" className="p-3" onClick={handleShow} style={{ backgroundColor: 'white', border: '1px solid #4caf4f', color: '#4caf4f', fontWeight: '700' }}>Family member: {index + 1}</button>
                                }

                            </div>
                            <Modal show={show} onHide={handleClose} style={{ opacity: '100' }}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Fill Details</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Form>
                                        <Form.Group>
                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Name
                                                </Form.Label>
                                                <Col>
                                                    <Form.Control
                                                        size="lg"
                                                        type="text"
                                                        placeholder="Enter your name"
                                                    />
                                                </Col>
                                            </Form.Row>
                                            <hr className="form-horizontal-line" />




                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Age
                                                </Form.Label>
                                                <Col>
                                                    <Form.Control
                                                        size="lg"
                                                        type="number"
                                                        placeholder="Enter your Age"
                                                    />

                                                </Col>
                                            </Form.Row>
                                            <hr className="form-horizontal-line" />
                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Gender
                                                </Form.Label>
                                                <Col>
                                                    <div class="radio" style={{ marginTop: '4px' }}>
                                                        <label class="radio-inline"><input type="radio" name="optradio" />Male</label>
                                                        <label class="radio-inline"><input type="radio" name="optradio" />Female</label>
                                                        <label class="radio-inline"><input type="radio" name="optradio" />Other</label>
                                                    </div>
                                                </Col>


                                            </Form.Row>
                                            <hr className="form-horizontal-line" />


                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Hobbies <span style={{ color: 'red' }}>*</span>
                                                </Form.Label>
                                                <Col>
                                                    <div class="checkbox" style={{ marginTop: '4px' }}>
                                                        <label class="radio-inline"><input type="checkbox" />Singing</label>
                                                        <label class="radio-inline"><input type="checkbox" />Dancing</label>
                                                        <label class="radio-inline"><input type="checkbox" />Reading</label>
                                                    </div>


                                                </Col>
                                            </Form.Row>
                                            <hr className="form-horizontal-line" />
                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Date of Data Collection:
                                                </Form.Label>
                                                <Col>
                                                    <Form.Control
                                                        size="lg"
                                                        type="date"
                                                        placeholder="Date of data collection"
                                                    />


                                                </Col>
                                            </Form.Row>
                                            <hr className="form-horizontal-line" />
                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Time of Data Collection:
                                                </Form.Label>
                                                <Col>
                                                    <Form.Control
                                                        size="lg"
                                                        type="time"
                                                        placeholder="Time of data collection"
                                                    />


                                                </Col>
                                            </Form.Row>

                                            <hr className="form-horizontal-line" />
                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Location
                                                </Form.Label>
                                                <Col>
                                                    <PlacesAutoComplete />
                                                </Col>
                                            </Form.Row>

                                            <hr className="form-horizontal-line" />
                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Phone Number
                                                </Form.Label>
                                                <Col>
                                                    <Example />

                                                </Col>
                                            </Form.Row>
                                            <hr className="form-horizontal-line" />
                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Email ID
                                                </Form.Label>
                                                <Col>
                                                    <Form.Control
                                                        size="lg"
                                                        type="email"
                                                        placeholder="Enter email id"
                                                    />

                                                </Col>
                                            </Form.Row>
                                            <hr className="form-horizontal-line" />
                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Signature
                                                </Form.Label>
                                                <Col tyle={{ border: '1px solid gainsboro' }}>

                                                    <SignatureCanvas penColor='green'
                                                        canvasProps={{ width: 500, height: 200, className: 'sigCanvas' }} />
                                                </Col>
                                            </Form.Row>

                                            <hr className="form-horizontal-line" />
                                            <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Picture
                                                </Form.Label>
                                                <Col tyle={{ border: '1px solid gainsboro' }}>
                                                    <div onClick={handleShow}>
                                                        <CustomButton content="Click to take a Picture" />
                                                    </div>


                                                </Col>
                                            </Form.Row>
                                            <hr className="form-horizontal-line" />
                                            {/* <Form.Row>
                                                <Form.Label column="lg" lg={2} className="form-label-custom">
                                                    Number of Family Members
                                                </Form.Label>
                                                <div>
                                                    <Col>
                                                        <Form.Control
                                                            type="number"
                                                            size="lg"
                                                            onChange={handleChange}
                                                        />

                                                    </Col>
                                                </div>


                                            </Form.Row>
                                            <Row style={{ marginLeft: '18%' }}>
                                                <ChildQuestion member={members} />
                                            </Row> */}

                                        </Form.Group>

                                        {/* <button type="submit"> Submit</button> */}
                                    </Form>
                                </Modal.Body>
                                <Modal.Footer>
                                    {/* <Button variant="secondary" onClick={handleClose}>
                                        Close
                                    </Button>
                                    <Button variant="primary" onClick={handleClose}>
                                        Save Changes
                                    </Button> */}
                                    <button
                                        className="p-2"
                                        onClick={handleClose}
                                        style={{
                                            backgroundColor: "#c2c2c2",
                                            border: "1px solid #c2c2c2",
                                            color: "white",
                                        }}
                                    >
                                        Cancel
                                    </button>

                                    <button
                                        className="p-2"
                                        onClick={handleSave}
                                        style={{
                                            backgroundColor: "#4caf4f",
                                            border: "1px solid #4caf4f",
                                            color: "white",
                                        }}
                                    >
                                        Save Changes
                                    </button>
                                </Modal.Footer>
                            </Modal>
                        </>

                    )
                )
            }



        </>
    )
}