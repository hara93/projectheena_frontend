import React from "react";
import cns from "classnames";
import PropTypes from "prop-types";

function Input({ className, touched, error, ...rest }) {
  return (
    <>
      <input className={cns("form_input", className)} {...rest} />
      {touched && error && <span className="text-danger">{error}</span>}
    </>
  );
}

Input.propTypes = {
  className: PropTypes.string,
  touched: PropTypes.bool,
  error: PropTypes.string,
};

Input.defaultProps = {
  className: "",
  touched: false,
  error: "",
};

export default Input;
