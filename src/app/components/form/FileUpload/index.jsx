import React from "react";
import cns from "classnames";
import PropTypes from "prop-types";

function FileUpload({ className, touched, onFileSelect, accept, error, ...rest }) {
  const getBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    getBase64(file)
      .then((data) => {
        console.log("data ==> ", data)
        onFileSelect(data);
      })
      .catch((error) => console.error(error));
  };

  return (
    <>
      <input type="file" className={cns(className)} onChange={handleFileChange} {...rest} />
      {touched && error && <span className="text-danger">{error}</span>}
    </>
  );
}

FileUpload.propTypes = {
  onFileSelect: PropTypes.func.isRequired,
  accept: PropTypes.string,
  className: PropTypes.string,
  touched: PropTypes.bool,
  error: PropTypes.string,
};

FileUpload.defaultProps = {
  accept: "",
  className: "",
  touched: false,
  error: "",
};

export default FileUpload;
