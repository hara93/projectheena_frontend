import React from "react";

function CustomButton(props) {
  return (
    <button type={props.type} onClick={props.onClick} className="button-full">
      {props.content}
    </button>
  );
}
export default CustomButton;
