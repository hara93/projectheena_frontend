import React from "react";
import { Col, Row, ProgressBar, Modal, Button } from "react-bootstrap";
import * as ReactBootstrap from "react-bootstrap";
import { Card } from "@material-ui/core";
import CustomButtonOutline from "../components/CustomButtonOutline";
import { Images } from "../config/Images";

class PeopleHelpCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }
  openModal = () => this.setState({ isOpen: true });
  closeModal = () => this.setState({ isOpen: false });
  myFunction = () => {
    window.confirm("Do you really want to turn down the help request?");
  }

  render() {
    return (
      <>
        <Row className="no-gutters">
          {this.props.help.map((ele, index) => (
            <Col md={6}>
              <Card
                className="p-2"
                style={{ marginRight: "3%", marginBottom: "3%" }}
              >
                <Col md={5} style={{ marginTop: "3%", marginBottom: "3%" }}>
                  <img
                    style={{ borderRadius: "50%", width: "80%" }}
                    src={Images.user_logo}
                  ></img>
                </Col>
                <Col md={7} style={{ marginTop: "3%", marginBottom: "3%" }}>
                  <h6 style={{ width: "70%" }}>
                    <strong>{ele.name}</strong>
                  </h6>
                  <p>
                    <i className="fa fa-map-marker"></i> {ele.location}
                  </p>
                  <p>
                    <i className="fa fa-phone"></i> {ele.phone}
                  </p>
                  <p>
                    <i className="fa fa-envelope"></i> {ele.mail}
                  </p>
                  <Row className="no-gutters">
                    <Col className="px-1">
                      <div onClick={this.openModal} style={{ width: '104%' }}>
                        <CustomButtonOutline content={`Accept Offer`} font_size={11} />
                      </div>
                    </Col>
                    <Col className="px-1">
                      <button
                        className="task-detail-button"
                        onClick={this.myFunction}
                        style={{ fontSize: 11 }}
                      >
                        Reject Offer
                </button>
                    </Col>
                  </Row>
                </Col>
              </Card>
            </Col>
          ))}


          <ReactBootstrap.Modal show={this.state.isOpen} onHide={this.closeModal} style={{ opacity: '1' }}>
            <ReactBootstrap.Modal.Header closeButton>
              <ReactBootstrap.Modal.Title> Accept name Help On Record Audiobooks - Panchatantra and Jataka
                Tales</ReactBootstrap.Modal.Title>
            </ReactBootstrap.Modal.Header>
            <ReactBootstrap.Modal.Body>
              <p>
                Share your Contact Details with Nilesh to collaborate easily to
                accomplish this task
              </p>
              {/* <input type="checkbox"> Share Contact Number</input>
              <input type="checkbox"> Share Email Id</input> */}
              <Row className="p-5">
                <label className="p-5">Share Contact Number
                    <input type="checkbox" ></input>
                </label>
                <label className="p-5">Share Email Id
                  <input type="checkbox" ></input>
                </label>
              </Row>
            </ReactBootstrap.Modal.Body>
            <ReactBootstrap.Modal.Footer>
              <div onClick={this.closeModal}>
                {/* <ReactBootstrap.Button variant="secondary">Close</ReactBootstrap.Button> */}
                <button className="p-2" onClick={this.closeModal} style={{ backgroundColor: '#c2c2c2', border: '1px solid #c2c2c2', color: 'white' }}>Close</button>
              </div>
              {/* <ReactBootstrap.Button variant="primary">Add meta data</ReactBootstrap.Button> */}
              <button className="p-2" onClick={this.closeModal} style={{ backgroundColor: '#4caf4f', border: '1px solid #4caf4f', color: 'white' }}>Submit</button>

            </ReactBootstrap.Modal.Footer>
          </ReactBootstrap.Modal>



        </Row>
      </>
    );
  }
}
export default PeopleHelpCard;
