import React from "react";
import { Col, Row, Card } from "react-bootstrap";
// import { Card } from "@material-ui/core";
import CustomButton from "../components/CustomButton";
import { Images } from "../config/Images";

function FollowerCard(props) {
  return (
    <Row>
      {props.followerlist.map((ele, index) => (
        <Col xs={12} md={6} lg={4}>
          <Card style={{ padding: "20px", marginBottom: "20px" }}>
            <Row>
              <Col xs={6}>
                <img
                  src={Images.user_logo}
                  className="follower-img img-center"
                  alt=""
                ></img>
              </Col>
              <Col xs={6}>
                <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
                  <a href="" className="follower-link">
                    {ele.name}
                  </a>
                </h3>
                <p>
                  <i className="fa fa-tag" style={{ color: "inherit" }}></i>{" "}
                  {ele.details}
                </p>
                <CustomButton content={`Follow`} />
              </Col>
            </Row>
          </Card>
        </Col>
      ))}
      ;
    </Row>
  );
}
export default FollowerCard;
