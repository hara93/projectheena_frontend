import React from 'react';
import PropTypes from "prop-types";

const Pagination = (props) => {

    const pageNumber = [];

    for (let i = 1; i <= Math.ceil(props.totalPeople / props.peoplePerPage); i++) {
        pageNumber.push(i)
    }
    console.log("Page: ", pageNumber)


    return (
        <nav>
            <ul className="pagination">

                {
                    pageNumber.map(
                        number => (
                            <li key={number} className="page-item">
                                <a onClick={() => props.paginate(number)} href="" className="page-link">
                                    {number}
                                </a>
                            </li>
                        )
                    )
                }
            </ul>

        </nav>

    )
}
Pagination.prototype = {

    paginate: PropTypes.func,

}
export default Pagination;