import React from "react";
import { Row, Col } from "react-bootstrap";
import { Images } from "../config/Images";
import CustomButton from "../components/CustomButton";

const PeopleCard = (props) => {
  return (
    <Row style={{ padding: "15px " }}>
      {props.people.map((ele) => (
        <Col md={6}>
          <div className="result-card">
            <div className="result-basic">
              <img className="img-circle" src={Images.user} alt="people"></img>
            </div>
            <div className="result-desc" style={{ minHeight: "" }}>
              <h4 className="font-bold" style={{ fontSize: "14px" }}>
                <a href="" className="ph-link">
                  {ele.name}
                </a>
              </h4>
              <div style={{ height: "110px" }}>
                <div style={{ margin: "10px 0" }}>
                  <div style={{ display: "table-cell", width: "25px" }}>
                    <i
                      class="fas fa-map-marker-alt"
                      style={{ fontSize: "13px", color: "#616161", paddingRight: "8px" }}
                    ></i>
                  </div>
                  <div style={{ display: "table-cell" }}>
                    <span>{ele.place}</span>
                  </div>
                </div>
                <div>
                  <div style={{ display: "table-cell", width: "25px" }}>
                    <i
                      class="fas fa-tags"
                      style={{ fontSize: "13px", color: "#616161", paddingRight: "8px" }}
                    ></i>
                  </div>
                  <div style={{ display: "table-cell" }}>
                    <span>{ele.bio}</span>
                  </div>
                </div>
              </div>
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <CustomButton content={`Get Help`} />
              </div>
            </div>
          </div>
        </Col>
      ))}
    </Row>
  );
};
export default PeopleCard;
