/* eslint-disable no-restricted-imports */
import React from "react";
import { Row, Col, Modal, Button, Tab, Tabs, Card } from "react-bootstrap";
import Form from "react-bootstrap/Form";
// import { Card } from "@material-ui/core";
import { Images } from "../config/Images";
import CustomButtonOutline from "../components/CustomButtonOutline";
import PlacesAutoComplete from "./PlacesAutoComplete";
import ProfessionalTab from "./ProfessionalTab";
import CreativeTab from "./CreativeTab";
import SocialTab from "./SocialTab";
import CustomButton from "./CustomButton";
// import Select from "react-select";

// class PeopleSearchBox extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       key: "home",
//       show: false,
//     };
//   }
//   handleClose = () => {
//     this.setState({ show: false });
//   };
//   handleShow = () => {
//     this.setState({ show: true });
//   };
//   render() {
//     return (
//       <Card className="p-3" style={{ marginTop: "5%" }}>
//         <p style={{ fontWeight: "700", color: "#676a6c", marginTop: "5%", marginBottom: "0%" }}>
//           {" "}
//           Would Like to Help on
//         </p>
//         <select
//           className="p-2"
//           style={{ width: "100%", border: "1px solid gainsboro", borderRadius: "6px" }}
//         >
//           <option>All Causes</option>
//           <option>Animal Welfare</option>
//           <option>Youth and Children </option>
//           <option>Women </option>
//         </select>

//         <p style={{ fontWeight: "700", color: "#676a6c", marginTop: "5%", marginBottom: "0%" }}>
//           {" "}
//           Is available on
//         </p>
//         <select
//           className="p-2"
//           style={{ width: "100%", border: "1px solid gainsboro", borderRadius: "6px" }}
//         >
//           <option>All Days</option>
//           <option>Monday</option>
//           <option>Tues </option>
//           <option>Wed </option>
//         </select>

//         <p style={{ fontWeight: "700", color: "#676a6c", marginTop: "5%", marginBottom: "0%" }}>
//           {" "}
//           Lives In
//         </p>
//         <PlacesAutoComplete />

//         <p
//           style={{
//             fontWeight: "700",
//             color: "#676a6c",
//             marginTop: "7%",
//             marginTop: "5%",
//             marginBottom: "0%",
//           }}
//         >
//           {" "}
//           Which Requires Following Skills
//         </p>
//         <div style={{ width: "100%" }} onClick={this.handleShow}>
//           <input
//             id="skills"
//             onClick={() => {
//               document.getElementById("skills").value = "";
//             }}
//             className="p-2"
//             style={{
//               border: "1px solid #e5e6e7",
//               width: "77%",
//               borderTopLeftRadius: "5px",
//               borderBottomLeftRadius: "5px",
//             }}
//             type="text"
//           />
//           <button
//             className="p-2"
//             style={{
//               border: "1px solid #c2c2c2",
//               backgroundColor: "#c2c2c2",
//               width: "23%",
//               color: "white",
//               fontWeight: "700",
//             }}
//           >
//             x
//           </button>
//         </div>

//         <p style={{ fontWeight: "700", color: "#676a6c", marginTop: "5%", marginBottom: "0%" }}>
//           {" "}
//           Is a team member of
//         </p>
//         <select
//           className="p-2"
//           style={{ width: "100%", border: "1px solid gainsboro", borderRadius: "6px" }}
//         >
//           <option>Select Team</option>
//           <option>Jindal Steel and Power</option>
//           <option>SBI</option>
//           <option>Glenmark</option>
//         </select>

//         <div style={{ margin: "2vh 2vw 2vh 3vw" }}>
//           <CustomButtonOutline content={`Search Volunteers`} />
//         </div>

//         <Modal show={this.state.show} onHide={this.handleClose} animation={false} centered>
//           <div
//             style={{
//               height: "60px",
//               borderBottom: "1px solid #cccccc",
//               padding: "15px 10px 5px 10px",
//               display: "flex",
//               flexDirection: "row",
//               justifyContent: "space-between",
//             }}
//           >
//             <h4>Select Skills</h4>
//             <Button variant="secondary" style={{ height: "30px" }} onClick={this.handleClose}>
//               <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
//             </Button>
//           </div>
//           <Modal.Body>
//             <div style={{ borderBottom: "1px solid #cccccc" }}>
//               <Tabs
//                 id="controlled-tab-example"
//                 activeKey={this.state.key}
//                 onSelect={(k) => this.setState({ key: k })}
//               >
//                 <Tab eventKey="home" title="Professional" style={{ opacity: "100" }}>
//                   <ProfessionalTab />
//                 </Tab>
//                 <Tab eventKey="profile" title="Creative" style={{ opacity: "100" }}>
//                   <CreativeTab />
//                 </Tab>
//                 <Tab eventKey="contact" title="Social" style={{ opacity: "100" }}>
//                   <SocialTab />
//                 </Tab>
//               </Tabs>
//             </div>
//             <div style={{ marginTop: "50px" }}>
//               <p>Maximum skills allowed: 5. (0 Of 5 Skills selected)</p>
//               <p>Select as many skills relevant, for better result</p>
//               <p>Selected Skills -</p>
//             </div>
//           </Modal.Body>
//           <Modal.Footer>
//             <Button variant="secondary" onClick={this.handleClose}>
//               Close
//             </Button>
//             <CustomButton content="Select Skills" />
//           </Modal.Footer>
//         </Modal>
//       </Card>
//     );
//   }
// }
const PeopleSearchBox = () => {
  return (
    <>
      <Card>
        <Form>
          <Form.Group>
            <Form.Label>Email address</Form.Label>
            <Form.Select aria-label="Default select example">
              <option>Open this select menu</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </Form.Select>
          </Form.Group>
        </Form>
      </Card>
    </>
  );
};
export default PeopleSearchBox;
