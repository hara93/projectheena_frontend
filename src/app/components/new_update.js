import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import FileUpload from './FileUpload';
import FileUp from './FileUp';

class NewUpdate extends React.Component {
    constructor() {
        super();
        this.state = {
            milestone: false
        }
    }

    handleChange = () => {
        this.setState({ milestone: !this.state.milestone })
    }
    render() {
        return (
            <>




                <ReactBootstrap.Accordion style={{ marginTop: '3vh', marginBottom: '4vh' }}>

                    <ReactBootstrap.Card.Header >
                        <ReactBootstrap.Accordion.Toggle variant="link" eventKey="0" style={{ fontWeight: 'normal', color: '#000', display: 'block', border: 'transparent', backgroundColor: 'transparent' }} >
                            Post New Update
                        </ReactBootstrap.Accordion.Toggle>
                    </ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Collapse eventKey="0">
                        <ReactBootstrap.Card.Body>
                            <div style={{ marginBottom: '3vh' }}>
                                <textarea name="description" row="5" maxLength="300" placeholder="Share any update with fellow collaborators" className="counter-textarea form-control" />
                                <span style={{ marginBottom: '5vh' }}>Max chars:300 </span>
                                <div style={{ marginTop: '2vh' }}>
                                    <div className="row">
                                        <div className="col-2">
                                            <span style={{ fontWeight: '600' }}>Attach File</span>
                                        </div>
                                        <div className="col-8">
                                            <FileUp />
                                        </div>
                                    </div>

                                    {/* <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2vh', marginBottom: '2vh' }} /> */}
                                </div>

                                {/* <div className="container"> */}
                                <div className="row mt-4" >
                                    <div className="col-2">
                                        <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}>Comment For Milestone?</span>

                                    </div>
                                    <div className="col-6">
                                        <input type="checkbox" onChange={this.handleChange} />
                                        <span style={{ marginLeft: '2%' }}>Yes</span><br />

                                    </div>


                                </div>
                                {
                                    this.state.milestone == true ?
                                        <div className="row" style={{ marginTop: '3%' }}>
                                            <div className="col-2">
                                                <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}> Milestone</span>

                                            </div>

                                            <div className="col-4">
                                                <div className="row">
                                                    <select className="py-2 px-5" >
                                                        <option >Select Milestone</option>
                                                        <option >Partner On-Boarding</option>
                                                        <option >Partner On-Boarding</option>
                                                        <option >Partner On-Boarding</option>

                                                    </select>
                                                    <span>Select a milestone if the comment belongs to a specific milestone</span>
                                                </div>
                                            </div>
                                            <div className="col-1"></div>
                                            <div className="col-4">
                                                <div className="row">
                                                    <select className="py-2 px-5" >
                                                        <option >Partner On-Boarding</option>
                                                        <option >Partner On-Boarding</option>
                                                        <option >Partner On-Boarding</option>
                                                        <option >Partner On-Boarding</option>

                                                    </select>
                                                    <span>
                                                        Select a activity if the comment belongs to a specific activity
                                                </span>
                                                </div>
                                            </div>
                                            {/* <div className="col-4">
                                                <p>Select a milestone if the comment belongs to a specific milestone</p>

                                            </div> */}


                                        </div> : null

                                }

                            </div>

                            {/* <div className="container" style={{ marginTop: '2vh' }}> */}
                            <div className="row">
                                <div className="col-2">
                                    <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}>Visibility</span>

                                </div>
                                <div className="col-10">
                                    <input type="radio" name="price" />
                                    <span style={{ marginLeft: '2%' }}>Collaborators & Admins</span>
                                    <input className="ml-3" type="radio" name="price" />
                                    <span style={{ marginLeft: '2%' }}>Admins Only</span>
                                    <button style={{ color: 'white', backgroundColor: '#c2c2c2', border: '1px solid #c2c2c2', padding: '1vh', marginTop: '2vh', display: 'block' }}>Comment</button>

                                </div>

                            </div>

                            {/* </div> */}

                            {/* </div> */}
                        </ReactBootstrap.Card.Body>
                    </ReactBootstrap.Accordion.Collapse>

                </ReactBootstrap.Accordion>

            </>
        )
    }
}

export default NewUpdate;
// import React, { useState } from 'react';
// import { withStyles } from '@material-ui/core/styles';
// import MuiAccordion from '@material-ui/core/Accordion';
// import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
// import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
// import Typography from '@material-ui/core/Typography';
// import FileUpload from './FileUpload'

// const Accordion = withStyles({
//     root: {
//         border: '1px solid rgba(0, 0, 0, .125)',
//         boxShadow: 'none',
//         '&:not(:last-child)': {
//             borderBottom: 0,
//         },
//         '&:before': {
//             display: 'none',
//         },
//         '&$expanded': {
//             margin: 'auto',
//         },
//     },
//     expanded: {},
// })(MuiAccordion);

// const AccordionSummary = withStyles({
//     root: {
//         backgroundColor: 'rgba(0, 0, 0, .03)',
//         borderBottom: '1px solid rgba(0, 0, 0, .125)',
//         marginBottom: -1,
//         minHeight: 56,
//         '&$expanded': {
//             minHeight: 56,
//         },
//     },
//     content: {
//         '&$expanded': {
//             margin: '12px 0',
//         },
//     },
//     expanded: {},
// })(MuiAccordionSummary);



// const AccordionDetails = withStyles((theme) => ({
//     root: {
//         padding: theme.spacing(2),
//     },
// }))(MuiAccordionDetails);

// export default function NewUpdate() {
//     const [expanded, setExpanded] = React.useState('panel1');
//     const [milestone, setMilestone] = useState(false);

//     const handleChange = (panel) => (event, newExpanded) => {
//         setExpanded(newExpanded ? panel : false);
//     };
//     const handleMilestone = () => {
//         setMilestone(true)
//     }

//     return (
//         <div>
//             <Accordion square expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
//                 <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
//                     <Typography>New Update</Typography>
//                 </AccordionSummary>
//                 <AccordionDetails>
//                     <Typography>
//                         <div style={{ marginTop: '4vh', marginBottom: '3vh' }}>
//                             <textarea name="description" row="5" maxLength="300" placeholder="Share any update with fellow collaborators" className="counter-textarea form-control" style={{ marginTop: '2vh' }} />
//                             <span style={{ marginBottom: '5vh' }}>Max chars:300 </span>
//                             <div style={{ marginTop: '7vh' }}>
//                                 <FileUpload />
//                                 <hr style={{ backgroundColor: 'gainsboro', width: '100%', borderStyle: 'dotted', marginTop: '2vh', marginBottom: '2vh' }} />
//                             </div>

//                             <div className="container">
//                                 <div className="row">
//                                     <div className="col-2">
//                                         <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}>Comment For Milestone?</span>

//                                     </div>
//                                     <div className="col-6">
//                                         {/* <input type="checkbox" onChange={handleMilestone} /> */}
//                                         <input type="checkbox" />
//                                         <span style={{ marginLeft: '2%' }}>Yes</span><br />

//                                     </div>


//                                 </div>
//                                 {/* {
//                                     this.state.milestone == true ?
//                                         <div className="row" style={{ marginTop: '3%' }}>
//                                             <div className="col-2">
//                                                 <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}> Milestone</span>

//                                             </div>
//                                             <div className="col-6">
//                                                 <select >
//                                                     <option >FY 2016-17 - Phase I - Advocacy, Mobilization And Survey</option>
//                                                     <option >FY 2016-17 - Phase I - Advocacy, Mobilization And Survey</option>
//                                                     <option >FY 2016-17 - Phase I - Advocacy, Mobilization And Survey</option>
//                                                     <option >FY 2016-17 - Phase I - Advocacy, Mobilization And Survey</option>

//                                                 </select>

//                                             </div>
//                                             <div className="col-4">
//                                                 <p>Select a milestone if the comment belongs to a specific milestone</p>

//                                             </div>


//                                         </div> : null

//                                 } */}

//                             </div>

//                             <div className="container" style={{ marginTop: '2vh' }}>
//                                 <div className="row">
//                                     <div className="col-2">
//                                         <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}>Visibility</span>

//                                     </div>
//                                     <div className="col-6">
//                                         <input type="radio" name="price" />
//                                         <span style={{ marginLeft: '2%' }}>Collaborators & Admins</span>
//                                         <input type="radio" name="price" />
//                                         <span style={{ marginLeft: '2%' }}>Admins Only</span>
//                                         <button style={{ color: 'white', backgroundColor: '#c2c2c2', border: '1px solid #c2c2c2', marginTop: '2vh', display: 'block' }}>Comment</button>

//                                     </div>

//                                 </div>

//                             </div>




//                         </div>
//                     </Typography>
//                 </AccordionDetails>
//             </Accordion>


//         </div>
//     );
// }