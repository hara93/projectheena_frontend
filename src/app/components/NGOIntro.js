import React from "react";

function NGOIntro(props) {
  return (
    <div className="my-5">
      <p className="ngo-header">NGOs Registered with us</p>
      <p className="ngo-intro">
        Following is a list of all the NGOs registered with us. If you know any
        NGO who would benefit from our technology please ask them to create an
        account with us or contact on team@projectheena.com
      </p>
    </div>
  );
}
export default NGOIntro;
