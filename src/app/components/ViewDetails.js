import React from 'react';
import { Row, Col } from 'react-bootstrap';
import * as ReactBootstrap from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';
import { FaEnvelope, FaMapMarker, FaPhone, FaUser } from 'react-icons/fa';
import { connect } from "react-redux";
import moment from "moment"
import PropTypes from "prop-types";

class ViewDetail extends React.Component {

    constructor(props) {
        var awards = "CNBC Young Turks,Guinness World Record"
        var causes = "All Type, Arts"

        var path_for_images = "http://localhost:3000/uploads/images/ngo/"
        var default_path = "http://localhost:3000/"

        super(props)
        // console.log("COVER IMAGE", this.props.ngo.cover)
        this.state = {
            // cover: this.props.ngo.cover == "" ? default_path + Images.Social_work : path_for_images + this.props.ngo.cover,
            ngo_details: this.props.ngo && this.props.ngo.ngo_details || "",
            overview: this.props.ngo && this.props.ngo.ngo_overview || "",
            awards: awards.split(","), // this.props.ngo.awards_won 
            causes: causes.split(","), // this.props.ngo.causes
            ngo_url: this.props.ngo && this.props.ngo.ngo_url || "not shared",
            joined: this.props.ngo && moment(this.props.ngo.registration_date).format("Do MMM, YYYY") || "1st jan",
            employees_involved: this.props.ngo && this.props.ngo.staff_count || 0,
            volunteer_count: this.props.ngo && this.props.ngo.volunteer_count || 0,
            ngo_address: this.props.ngo && this.props.ngo.ngo_address || "",
            ngo_email_id: this.props.ngo && this.props.ngo.ngo_email_id || "",
            ngo_contact_number: this.props.ngo && this.props.ngo.ngo_contact_number || ""
        }
        console.log("this.props", this.props)
    }
    componentDidMount() {
    }
    handleInit = () => {
        console.log("CLICKED many times")
        this.props.handleInitiatives(true)
    }
    render() {
        return (
            <>
                <h3 className="font-weight-bold">About Us</h3>
                <div style={{ marginBottom: 20 }}>
                    {this.state.ngo_details}
                </div>
                {/* <p>Several times you will be involved in activities where you are directly working for a social cause with the beneficiary.</p>

                <p> While we always have volunteering tasks created by or for NGOs, we realized the need of a 'social work' account where an initiative is taken irrespective whether there is an NGO organization partnering or not.</p>

                <p>  While creating Karma Log, starting new initiatives or  doing advocacy campaigns tasks will be created by this NGO account.</p>

                <p>Note: This is a holder account directly operated by ProjectHeena Team and not an actual Non-Profit entity. Let the volunteering begin!</p> */}

                {/* <img src={this.state.cover} style={{ width: '96%' }}></img> */}
                <button className="p-2" style={{ width: '96%', marginTop: '5%', marginBottom: '10%', backgroundColor: '#4caf4f', border: '1px solid #4caf4f', color: 'white', fontWeight: '600' }} onClick={this.handleInit}>View Initiatives</button>
                <Row className="">
                    <Col md={6}>
                        <h3 className="font-weight-bold">Causes we work on</h3>
                        <ul style={{ padding: 0, margin: 0 }}>
                            {this.state.causes.map((ele, index) => (
                                <li style={{ listStyleType: 'none' }}>
                                    <i class="fa fa-tag" ></i>
                                    <p style={{ display: 'inline-block' }}>
                                        {'' + ele}
                                    </p>
                                </li>
                            ))}
                        </ul>
                    </Col>
                    <Col md={6}>
                        <h3 className="font-weight-bold" >Awards Won</h3>
                        <ul style={{ padding: 0, margin: 0 }}>
                            {this.state.awards.map((ele, index) => (
                                <li style={{ listStyleType: 'none' }}>
                                    <i class="fa fa-trophy" ></i>
                                    <p style={{ display: 'inline-block' }}>
                                        {ele}
                                    </p>
                                </li>
                            ))}
                        </ul>
                    </Col>
                </Row>

                <h3 className="font-weight-bold">Why Work With Us?</h3>
                {/* <p>By volunteering on the ProjectHeena platform you get connected with several legit NGOs who are working on the ground and creating a difference that is required today. Our platform helps you to:</p> */}
                <div>
                    {this.state.ngo_details}
                </div>
                {/* <ol>
                    <li style={{ marginBottom: '-4%', fontWeight: '500' }}>Discover new talent and opportunities to contribute</li>
                    <li style={{ marginBottom: '-4%', fontWeight: '500' }}>Engage and keep every one updated</li>
                    <li style={{ marginBottom: '-4%', fontWeight: '500' }}>Get Accredited for the work done and inspire everyone in your circle to follow   </li>
                </ol> */}

                {/* <ReactBootstrap.Accordion style={{ marginTop: '5vh' }}>
                    <ReactBootstrap.Card style={{ borderStyle: 'none' }}> */}
                {/* <ReactBootstrap.Card.Header style={{ color: 'gainsboro', backgroundColor: 'white', width: '95%', marginLeft: '2%', border: '1px solid gainsboro', borderTop: '5px solid gainsboro', }}>

                            <ReactBootstrap.Accordion.Toggle
                                as={ReactBootstrap.FormLabel}
                                variant="link"
                                eventKey="0"
                                style={{ fontSize: '18px', fontWeight: 'normal', color: '#000' }}
                            >
                                
            </ReactBootstrap.Accordion.Toggle>
                        </ReactBootstrap.Card.Header> */}


                <ReactBootstrap.Accordion style={{ marginTop: '3vh', marginBottom: '4vh' }}>

                    <ReactBootstrap.Card.Header style={{ padding: 0 }} >
                        <ReactBootstrap.Accordion.Toggle className="h5 ml-2" variant="link" eventKey="0" style={{ fontWeight: 'normal', color: '#000', display: 'block', border: 'transparent', backgroundColor: 'transparent' }} >
                            NGO Details
                        </ReactBootstrap.Accordion.Toggle>
                    </ReactBootstrap.Card.Header>


                    <ReactBootstrap.Accordion.Collapse eventKey="0">
                        <ReactBootstrap.Card.Body
                            // style={{ backgroundColor: '#ffffff' }}
                            style={{ fontWeight: 'normal', color: '#000', display: 'block', border: '1px solid gainsboro', backgroundColor: 'transparent' }}>

                            <ul style={{ padding: 0 }}>
                                <li style={{ listStyleType: 'none', padding: 0 }}>
                                    <div className="th" style={{ display: 'inline-block' }}>Website</div>
                                    <div className="tc " style={{ display: 'inline-block' }}>{this.state.ngo_url || ""}</div>
                                </li>

                                <li style={{ listStyleType: 'none', padding: 0 }}>
                                    <div className="th" style={{ display: 'inline-block' }}>Joined</div>
                                    <div className="tc " style={{ display: 'inline-block' }}>{this.state.joined}</div>
                                </li>

                                <li style={{ listStyleType: 'none', padding: 0 }}>
                                    <div className="th" style={{ display: 'inline-block' }}>Employees Involved</div>
                                    <div className="tc " style={{ display: 'inline-block' }}>{this.state.employees_involved}</div>
                                </li>

                                <li style={{ listStyleType: 'none', padding: 0 }}>
                                    <div className="th" style={{ display: 'inline-block' }}>Volunteers working with us</div>
                                    <div className="tc " style={{ display: 'inline-block' }}>{this.state.volunteer_count}</div>
                                </li>

                                <li style={{ listStyleType: 'none', padding: 0 }}>
                                    <div className="th" style={{ display: 'inline-block' }}>URl</div>
                                    <div className="tc " style={{ display: 'inline-block' }}>{this.state.ngo_url} </div>
                                </li>

                            </ul>

                            {/* <ReactBootstrap.Table hover>
                                    <thead>
                                        <tr style={{ borderBottom: '1px solid gainsboro', borderLeft: '1px solid gainsboro' }} >
                                            <td style={{ fontWeight: '500', paddingLeft: '15%' }}>
                                                Website
                                       </td>
                                            <td style={{ fontWeight: '500', paddingLeft: '15%', borderRight: '1px solid gainsboro' }}>Social Work Website</td>
                                        </tr>
                                        <tr style={{ borderBottom: '1px solid gainsboro', borderLeft: '1px solid gainsboro' }}>
                                            <td style={{ fontWeight: '500', paddingLeft: '15%' }}>
                                                
                                       </td>
                                            <td style={{ fontWeight: '500', paddingLeft: '15%', borderRight: '1px solid gainsboro' }}></td>
                                        </tr>
                                        <tr style={{ borderBottom: '1px solid gainsboro', borderLeft: '1px solid gainsboro' }}>
                                            <td style={{ fontWeight: '500', paddingLeft: '15%' }}>
                                                
                                       </td>
                                            <td style={{ fontWeight: '500', paddingLeft: '15%', borderRight: '1px solid gainsboro' }}>7</td>
                                        </tr>
                                        <tr style={{ borderBottom: '1px solid gainsboro', borderLeft: '1px solid gainsboro' }}>
                                            <td style={{ fontWeight: '500', paddingLeft: '15%' }}>
                                                
                                       </td>
                                            <td style={{ fontWeight: '500', paddingLeft: '15%', borderRight: '1px solid gainsboro' }}></td>
                                        </tr>
                                    </thead>
                                    <tbody>







                                    </tbody>






                                </ReactBootstrap.Table> */}

                        </ReactBootstrap.Card.Body>
                    </ReactBootstrap.Accordion.Collapse>
                    {/* </ReactBootstrap.Card> */}
                </ReactBootstrap.Accordion>

                <h3 className="font-weight-bold mt-4">Contact Details</h3>
                <Row>
                    <Col md={12}>
                        <ul style={{ padding: 0 }}>
                            {/* <li style={{ listStyleType: 'none', marginBottom: '-4%' }}><FaUser style={{ marginRight: '2%' }} /><a href="#" style={{ display: 'inline-block', color: '#4caf4f' }}>ProjectHeena Operations </a>(Admin)</li> */}
                            <li style={{ listStyleType: 'none', marginBottom: '-4%' }}><FaMapMarker style={{ marginRight: '2%' }} /><p style={{ display: 'inline-block' }}>{this.state.ngo_address}</p></li>
                            <li style={{ listStyleType: 'none', marginBottom: '-4%' }}><FaPhone style={{ marginRight: '2%' }} /><p style={{ display: 'inline-block' }}>{this.state.ngo_contact_number}</p></li>
                            <li style={{ listStyleType: 'none', marginBottom: '-4%' }}><FaEnvelope style={{ marginRight: '2%' }} /><p style={{ display: 'inline-block' }}>{this.state.ngo_email_id}</p></li>

                        </ul>
                    </Col>


                </Row>


                <button className="p-2" style={{ width: '96%', marginTop: '5%', backgroundColor: '#4caf4f', border: '1px solid #4caf4f', color: 'white', fontWeight: '600' }} onClick={this.handleInit}>View Initiatives</button>
            </>
        )
    }
}
ViewDetail.propTypes = {
    handleInitiatives: PropTypes.bool,
}
const mapStateToProps = function (state) {
    return {
        ngo: state.phAuth.ngo,
    };
};

export default connect(mapStateToProps)(ViewDetail);
