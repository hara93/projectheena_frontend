import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import CustomButton from './CustomButton';


class ProjectTimeline extends React.Component {
    render() {
        return (
            <>

                {this.props.odd.map((ele, index) => (
                    <Row>

                        <Card className="p-4" style={{ backgroundColor: '#f5f5f5', border: 'none' }}>
                            <h2>{ele.name}</h2>
                            <p>{ele.description}</p>
                            <div style={{ width: "25%" }}>
                                <CustomButton content={`More Info`} />
                            </div>

                        </Card>




                    </Row>



                ))}









            </>
        )
    }
}

export default ProjectTimeline;