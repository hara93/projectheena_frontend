/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import {
  Card,
  Container,
  Row,
  Col,
  Modal,
  Button,
  Form,
} from "react-bootstrap";

import "../../index.scss";
import CustomButton from "../components/CustomButton";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

function NotificationPost(props) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [contact, setContact] = useState(false);
  const contactNumber = props.number;
  const [email, setEmail] = useState(false);
  const emailID = props.email;

  const [modalTrigger, setModalTrigger] = useState(true);

  const confirmation = () => {
    const confirmBox = window.confirm(
      "do you really want to turn down the help req?"
    );
  };
  return (
    <>
      <Modal show={show} onHide={handleClose} animation={false} size="lg">
        <div
          style={{
            height: "60px",
            borderBottom: "1px solid #cccccc",
            padding: "15px 10px 5px 10px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h4>Feel free to contact us</h4>
          <Button
            variant="secondary"
            style={{ height: "30px" }}
            onClick={handleClose}
          >
            <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ minHeight: "150px", padding: "20px" }}>
            <div>
              Share your Contact Details with Umang to collaborate easily to
              accomplish this task
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: "15px",
              }}
            >
              <FormControlLabel
                control={<Checkbox color="primary" />}
                label="Share Contact Number"
                value="Share Contact Number"
                checked={contact}
                onChange={() => setContact(!contact)}
              />
              <FormControlLabel
                control={<Checkbox color="primary" />}
                label="Share Email Address"
                value="Share Email Address"
                checked={email}
                onChange={() => setEmail(!email)}
              />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Accept Offer"></CustomButton>
        </Modal.Footer>
      </Modal>

      <Container>
        <Row>
          <Col xs={2} className="notification-date">
            <div className="notification-icon">{props.icon}</div>
            <div style={{ color: "#676a6c" }}>{props.time}</div>
            {props.date}
          </Col>
          <Col xs={10} className="notification-content">
            <div>{props.content}</div>
            <div>
              <p>
                {email ? <>| Email : {emailID}</> : null}
                {contact ? <>Contact Info : {contactNumber}</> : null}
              </p>
            </div>
            {modalTrigger ? (
              <div style={{ display: "flex" }}>
                <div onClick={handleShow}>
                  <a
                    onClick={() => setModalTrigger(!modalTrigger)}
                    style={{ color: "green" }}
                  >
                    Accept offer{" "}
                  </a>
                </div>
                |
                <div onClick={confirmation}>
                  <a
                    onClick={() => setModalTrigger(!modalTrigger)}
                    style={{ color: "green" }}
                  >
                    {" "}
                    Reject offer
                  </a>
                </div>
              </div>
            ) : null}
          </Col>
        </Row>
      </Container>
    </>
  );
}
export default NotificationPost;
