/* eslint-disable no-restricted-imports */
// import React, { useState } from "react";
// import { makeStyles } from "@material-ui/core/styles";
// import Rating from "@material-ui/lab/Rating";
// import Box from "@material-ui/core/Box";

// const labels = {
//   0.5: "1Star",

//   2: "2Star",

//   3: "3Star",

//   4: "4Star",

//   5: "5Star",
// };

// const useStyles = makeStyles({
//   root: {
//     width: 200,
//     display: "flex",
//     alignItems: "center",
//   },
// });

// export default function HoverRating() {
//   const [value, setValue] = useState(2);
//   const [hover, setHover] = useState(-1);
//   const classes = useStyles();

//   return (
//     <div className={classes.root}>
//       <Rating
//         name="hover-feedback"
//         value={value}
//         precision={0.5}
//         onChange={(event, newValue) => {
//           setValue(newValue);
//         }}
//         onChangeActive={(event, newHover) => {
//           setHover(newHover);
//         }}
//         size="large"
//       />
//       {value !== null && (
//         <Box ml={2}>{labels[hover !== -1 ? hover : value]}</Box>
//       )}
//     </div>
//   );
// }
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";

const labels = {
  1: "1 Star",

  2: "2 Star",

  3: "3 Star",

  4: "4 Star",

  5: "5 Star",
};

const useStyles = makeStyles({
  root: {
    width: 200,
    display: "flex",
    alignItems: "center",
  },
});

export default function HoverRating() {
  const [value, setValue] = React.useState(2);
  const [hover, setHover] = React.useState(0);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Rating
        name="hover-feedback"
        value={value}
        precision={1}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
        onChangeActive={(event, newHover) => {
          setHover(newHover);
        }}
      />
      {value !== null && (
        <Box ml={2}>{labels[hover !== -1 ? hover : value]}</Box>
      )}
    </div>
  );
}
