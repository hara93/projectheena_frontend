import React from 'react';
import {Card, Row, Col} from 'react-bootstrap';
import CustomButton from './CustomButton';


class ProjectTimelineEven extends React.Component{
    render(){
        return(
            <>
            
             {this.props.even.map((ele, index) => (
                 <Row>
                      
                      <Card className="p-5" style={{marginBottom:'10%', backgroundColor:'#f5f5f5', border:'none'}}>
                                    <h3>{ele.name}</h3>
                                    <p>{ele.description}</p>
                                    <CustomButton content={`More Info`}/>
                                </Card>

                 </Row>
                            
                               
                            
                        ))}

           

           
           

              


            </>
        )
    }
}

export default ProjectTimelineEven;