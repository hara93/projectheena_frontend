import React from "react";

function CustomButtonOutline(props) {
  return (
    <button className="view-profile" style={{ fontSize: props.font_size }} onClick={props.function}>
      {props.content}
    </button>
  );
}
export default CustomButtonOutline;
