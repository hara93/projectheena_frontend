import React from 'react';
import {Table} from 'react-bootstrap';

class FileTable extends React.Component{
    render(){
        return(
            <>
            
            {this.props.records.map((ele, index) => (
                <Table striped bordered hover>
                     <thead>

                        <tr>
                            <th>Sr. No</th>
                            <th>Image/Icon</th>
                            <th>File Name</th>
                            <th>Upload By</th>
                            <th>Update Visibility</th>
                            <th>Date Added</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{ele.Sr_No}</td>
                            <td></td>
                            <td>{ele.file_name}</td>
                            <td>{ele.upload_by}</td>
                            <td>{ele.visibility}</td>
                            <td>{ele.date}</td>
                        </tr>


                        </tbody>
                </Table>
                            
                        ))}
                       
            </>
        )
    }
}

export default FileTable;