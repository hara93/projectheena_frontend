import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import Geocode from "react-geocode";
import PropTypes from "prop-types";

Geocode.setApiKey("AIzaSyAI_qrVCRazEORV3rSrDrP97S8NzsBqfFc");
Geocode.setLanguage("en");
Geocode.setRegion("in");
Geocode.setLocationType("ROOFTOP");
Geocode.enableDebug();

class GoogleLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            marker_name: "",
            markers: [
                {
                    name: "Current position",
                    position: {
                        lat: '20.5937',
                        lng: '78.9629'
                    }
                }
            ]

        }
    }
    getAddress = (lat, lng) => {
        Geocode.fromLatLng(lat, lng).then(
            (response) => {
                const address = response.results[0].formatted_address;
                let city, state, country, postal_code, political;
                for (let i = 0; i < response.results[0].address_components.length; i++) {
                    for (let j = 0; j < response.results[0].address_components[i].types.length; j++) {
                        // console.log("HERE", response.results[0].address_components[i].types[j], response.results[0].address_components[i].long_name)
                        switch (response.results[0].address_components[i].types[j]) {
                            case "locality":
                                city = response.results[0].address_components[i].long_name;
                                break;
                            case "administrative_area_level_1":
                                state = response.results[0].address_components[i].long_name;
                                break;
                            case "country":
                                country = response.results[0].address_components[i].long_name;
                                break;
                            case "postal_code":
                                postal_code = response.results[0].address_components[i].long_name;
                                break;
                        }
                    }
                }
                var pincode = (!postal_code || postal_code == '') ? '' : ' - ' + postal_code
                this.props.address_selected(city + ', ' + state + ', ' + country + pincode)
                this.props.address_lat(lat)
                this.props.address_lng(lng)
            },
            (error) => {
                console.error(error);
            }
        );
    }
    onMarkerDragEnd = (coord, index) => {
        const { latLng } = coord;
        const lat = latLng.lat();
        const lng = latLng.lng();

        this.setState(prevState => {
            const markers = [...this.state.markers];
            markers[index] = { ...markers[index], position: { lat, lng } };
            return { markers };
        });
        this.getAddress(lat, lng)
    }
    getLocation = () => {
        if (navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(pos => {
                const coords = pos.coords;

                let newState = Object.assign({}, this.state);
                newState.markers[0].position.lat = coords.latitude;
                newState.markers[0].position.lng = coords.longitude;

                this.setState(newState);
                console.log("map", this.state.markers[0].position.lat, this.state.markers[0].position.lng)
                this.getAddress(this.state.markers[0].position.lat, this.state.markers[0].position.lng)
            });
        }
    }
    componentDidMount() {
        this.getLocation()
    }
    render() {
        return (

            <div>
                <Map
                    google={this.props.google}
                    style={{
                        width: "97%",
                        height: "85%"
                    }}
                    zoom={14}
                    initialCenter={{ lat: this.state.markers[0].position.lat, lng: this.state.markers[0].position.lng }}
                >
                    {this.state.markers.map((marker, index) => (
                        <Marker
                            key={index}
                            position={marker.position}
                            draggable={true}
                            onDragend={(t, map, coord) => this.onMarkerDragEnd(coord, index)}
                            name={this.state.marker_name}
                        />
                    ))}
                </Map>
                {/* <button type="submit" onClick={this.handleSubmit} >submit</button> */}
            </div>
        );
    }
}
GoogleLocation.propTypes = {
    address_selected: PropTypes.func,
    address_lat: PropTypes.func,
    address_lng: PropTypes.func,
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAI_qrVCRazEORV3rSrDrP97S8NzsBqfFc'
})(GoogleLocation);