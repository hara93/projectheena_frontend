import React from 'react';
import TextField from '@material-ui/core/TextField';
import { FormControl, RadioGroup, Radio, FormControlLabel, FormLabel } from '@material-ui/core';

export default function RadioItem(props) {
    const { name, label, value, onChange, items } = props


    return (
        <>
            <FormLabel style={{ marginTop: '6%' }}>
                {label}
            </FormLabel>
            <RadioGroup
                row
                name={name}
                value={value}
                style={{ MarginTop: '3%' }}
                className="p-2"
                onChange={onChange}>

                {
                    items.map(
                        (item, index) => (
                            <FormControlLabel key={item.id} value={item.id} control={<Radio />} label={item.title}></FormControlLabel>
                        )
                    )
                }


            </RadioGroup>
        </>
    )
}
