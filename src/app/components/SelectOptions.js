import React from 'react';
import TextField from '@material-ui/core/TextField';
import { FormControl, RadioGroup, Radio, FormControlLabel, FormLabel, Select } from '@material-ui/core';
import { InputLabel } from '@material-ui/core';
import { MenuItem } from '@material-ui/core';


export default function SelectOptions(props) {
    const { name, label, value, onChange, option } = props


    return (
        <>
            <FormControl
                variant="outlined" style={{ width: '100%' }}>
                <InputLabel>{label}</InputLabel>
                <Select
                    label={label}
                    name={name}
                    value={value}
                    onChange={onChange}
                    style={{ width: '100%' }}
                >
                    <MenuItem value="">None</MenuItem>
                    {
                        option.map(
                            item => (<MenuItem key={item.id} value={item.id}>{item.title}</MenuItem>)
                        )
                    }

                </Select>

            </FormControl>

        </>
    )
}
