/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import {
  Form,
  Container,
  Row,
  Col,
  OverlayTrigger,
  Tooltip,
  Modal,
  Button,
} from "react-bootstrap";
import CustomButton from "./CustomButton";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import StyledDropzone from "./Dropzone";

const Number = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [shows, setShows] = useState(false);

  const handleCloseImg = () => setShows(false);
  const handleShowImg = () => setShows(true);

  return (
    <>
      <Container style={{ padding: "15px 0" }}>
        <Form>
          <Row>
            <Col xs={12} md={9}>
              <Form.Group>
                <Form.Control
                  type="text"
                  placeholder="Enter a Question title"
                  size="lg"
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={9}>
              <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control as="textarea" rows={3} size="lg" />
              </Form.Group>
            </Col>
          </Row>

          <Form.Group>
            <Row>
              <Col xs={12} md={6}>
                <Form.Label>
                  Keyword
                  <OverlayTrigger
                    placement="right"
                    delay={{ show: 100, hide: 100 }}
                    overlay={
                      <Tooltip>
                        A question keyword is an alias or second name for your
                        questions. It makes your data reports simpler to
                        understand and more analysis-friendly.
                      </Tooltip>
                    }
                  >
                    <i
                      class="fas fa-info-circle"
                      style={{ marginLeft: "10px" }}
                    ></i>
                  </OverlayTrigger>
                </Form.Label>
                <Form.Control type="text" />
              </Col>
              <Col xs={12} md={6}>
                <Form.Label> </Form.Label>
                <div className="project-checkbox" style={{ marginTop: "3%" }}>
                  <label for="1">
                    <input type="checkbox" value="1" id="1"></input>
                    Mandatory Question <span style={{ color: "red" }}>*</span>
                  </label>
                </div>
              </Col>
            </Row>
          </Form.Group>

          <Form.Group>
            <button
              type="button"
              onClick={handleShowImg}
              className="p-3"
              style={{
                backgroundColor: "#4caf4f",
                border: "1px solid #4caf4f",
                color: "white",
                fontWright: "600",
              }}
            >
              <i className="far fa-images" style={{ color: "white" }}></i>Add
              Help Image
            </button>
          </Form.Group>

          <Form.Group>
            <Row>
              <Col xs={12} md={9}>
                <p className="survey-qtn-titles">Select limit type</p>

                <div
                  className="project-checkbox"
                  style={{
                    marginTop: "3%",
                  }}
                >
                  <label for="1">
                    <input
                      type="radio"
                      value="1"
                      id="1"
                      name="validation"
                    ></input>
                    Value-based validaion{" "}
                    <OverlayTrigger
                      placement="right"
                      delay={{ show: 100, hide: 100 }}
                      overlay={
                        <Tooltip>
                          The answer of this question will be matched against
                          the number of digits you provide below.
                        </Tooltip>
                      }
                    >
                      <i
                        class="fas fa-info-circle"
                        style={{ marginRight: "20px" }}
                      ></i>
                    </OverlayTrigger>{" "}
                  </label>
                  <label for="1">
                    <input
                      type="radio"
                      value="1"
                      id="1"
                      name="validation"
                    ></input>
                    Digit-based validaion
                    <OverlayTrigger
                      placement="right"
                      delay={{ show: 100, hide: 100 }}
                      overlay={
                        <Tooltip>
                          The answer of this question will be matched against
                          the number of digits you provide below.
                        </Tooltip>
                      }
                    >
                      <i
                        class="fas fa-info-circle"
                        style={{ marginLeft: "3px" }}
                      ></i>
                    </OverlayTrigger>
                  </label>
                </div>
              </Col>
            </Row>
            <Row style={{ marginTop: "30px" }}>
              <Col xs={12} md={4}>
                <Form.Label>Lower Limit</Form.Label>
                <Form.Control type="number" placeholder="Lower Limit" />
              </Col>
              <Col xs={12} md={4}>
                <Form.Label>Upper Limit</Form.Label>
                <Form.Control type="number" placeholder="Upper Limit" />
              </Col>
            </Row>
            <Row style={{ marginTop: "15px" }}>
              <Col>
                <div>
                  <FormControlLabel
                    control={<Checkbox />}
                    label="Allow Decimal Numbers"
                  />
                </div>
                <FormControlLabel
                  control={
                    <Checkbox
                      onChange={handleShow}
                      name="warning"
                      style={{ margin: "0" }}
                    />
                  }
                  label="Enable warning"
                />{" "}
                <OverlayTrigger
                  placement="right"
                  delay={{ show: 100, hide: 100 }}
                  overlay={
                    <Tooltip>
                      Warning will be shown when auditor will enter unexpected
                      values.
                    </Tooltip>
                  }
                >
                  <i
                    class="fas fa-info-circle"
                    style={{ marginLeft: "3px" }}
                  ></i>
                </OverlayTrigger>
              </Col>
            </Row>
          </Form.Group>
        </Form>
      </Container>

      <Modal
        show={show}
        onHide={handleClose}
        animation={false}
        size="md"
        centered
      >
        <div
          style={{
            height: "60px",
            borderBottom: "1px solid #cccccc",
            padding: "15px 10px 5px 10px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h4>Set Warning</h4>
          <Button
            variant="secondary"
            style={{ height: "30px" }}
            onClick={handleClose}
          >
            <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
          </Button>
        </div>
        <Modal.Body>
          <div style={{ color: "gray", marginBottom: "40px" }}>
            <p>
              Warning will be shown when auditor will enter unexpected values.
              <br />
              Note: This will not stop form submission.
            </p>
          </div>
          <div style={{ marginBottom: "40px" }}>
            <p className="survey-qtn-titles">Expected Range</p>
            <Row>
              <Col>
                <Form.Label>Lower Limit</Form.Label>
                <Form.Control type="number" placeholder="Lower Limit" />
              </Col>
              <Col>
                <Form.Label>Upper Limit</Form.Label>
                <Form.Control type="number" placeholder="Upper Limit" />
              </Col>
            </Row>
          </div>
          <div>
            <p className="survey-qtn-titles">Message for unexpected value</p>
            <Form.Control as="textarea"></Form.Control>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Set warning" />
        </Modal.Footer>
      </Modal>

      <Modal
        show={shows}
        onHide={handleClose}
        animation={false}
        size="lg"
        style={{ opacity: "1" }}
      >
        <Modal.Header closeButton>
          <Modal.Title className="p-2" style={{ fontWeight: "700" }}>
            Add question help image
          </Modal.Title>
        </Modal.Header>
        <Modal.Body
          style={{
            border: "2px dashed #cbcbcb",
            margin: "5%",
            borderRadius: "25px",
          }}
          className="p-5"
        >
          <i class="fas fa-images fa-3x" style={{ marginLeft: "45%" }}></i>
          <h4
            style={{ color: "#9e9e9e", fontWeight: "700", marginLeft: "35%" }}
          >
            Drop an image here
          </h4>
          <span
            className="p-2"
            style={{
              backgroundColor: "#9e9e9e",
              color: "white",
              borderRadius: "13px",
              marginLeft: "36%",
            }}
          >
            JPEG
          </span>
          <span
            className="p-2"
            style={{
              backgroundColor: "#9e9e9e",
              color: "white",
              borderRadius: "13px",
              marginLeft: "4%",
            }}
          >
            GIF
          </span>
          <span
            className="p-2"
            style={{
              backgroundColor: "#9e9e9e",
              color: "white",
              borderRadius: "13px",
              marginLeft: "4%",
            }}
          >
            PNG
          </span>
          <p
            style={{
              marginLeft: "40%",
              color: "#9e9e9e",
              fontWeight: "900",
              marginTop: "3%",
            }}
          >
            ---------OR---------
          </p>

          <StyledDropzone />
        </Modal.Body>
        <Modal.Footer>
          <button
            className="p-2"
            onClick={handleCloseImg}
            style={{
              backgroundColor: "#c2c2c2",
              border: "1px solid #c2c2c2",
              color: "white",
            }}
          >
            Close
          </button>

          <button
            className="p-2"
            onClick={handleCloseImg}
            style={{
              backgroundColor: "#4caf4f",
              border: "1px solid #4caf4f",
              color: "white",
            }}
          >
            Submit
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default Number;
