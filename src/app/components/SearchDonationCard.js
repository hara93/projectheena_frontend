import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';
import CustomButtonOutline from './CustomButtonOutline';
import CustomButton from './CustomButton';

function SearchTaskCard(props) {
    return (
        <>
            <Row className="p-3">
                {props.donation.map((ele, index) => (

                    <Card className="p-3 donation-card" >
                        <Row>
                            <Col md={8}>
                                <div style={{ height: '46px' }}>
                                    <p style={{ color: '#4caf50', marginBottom: '2%', fontWeight: '600', fontSize: '15px', width: '100%' }}>{ele.name}</p>
                                </div>
                                <p style={{ marginTop: '10px', width: '110%' }}>{ele.description}</p>
                            </Col>
                            <Col md={4}>
                                <img className="p-3" style={{ width: '100%', height: '50%' }} src={Images.View_Profile}></img>
                                <div style={{ marginLeft: '3%' }}>
                                    <CustomButton content={`Contribute`} />

                                </div>

                            </Col>

                        </Row>

                        <Row>
                            <Col md={6} style={{ paddingLeft: '20%' }}>
                                <p style={{ marginBottom: '0%' }}>Requires</p>
                                <p>	&#8377;{ele.requires}</p>

                            </Col>
                            <Col md={6}>
                                {
                                    ele.ends ?
                                        <>
                                            <p style={{ marginBottom: '0%', marginRight: '5%' }}>Ends on</p>
                                            <p>	{ele.ends}</p>
                                        </> : null
                                }
                            </Col>



                        </Row>






                    </Card>




                ))}
            </Row>



        </>



    )
}
export default SearchTaskCard;
