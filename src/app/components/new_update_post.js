import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import FileUpload from './FileUpload';

class NewUpdatePost extends React.Component {
    constructor() {
        super();
        this.state = {
            milestone: false
        }
    }

    handleChange = () => {
        this.setState({ milestone: !this.state.milestone })
    }
    render() {
        return (
            <>




                <ReactBootstrap.Accordion style={{ marginTop: '3vh', marginBottom: '4vh' }}>

                    <ReactBootstrap.Card.Header >
                        <ReactBootstrap.Accordion.Toggle variant="link" eventKey="0" style={{ fontWeight: 'normal', color: '#000', display: 'block', border: 'transparent', backgroundColor: 'transparent' }} >
                            Post New Update
                        </ReactBootstrap.Accordion.Toggle>
                    </ReactBootstrap.Card.Header>
                    <ReactBootstrap.Accordion.Collapse eventKey="0">
                        <ReactBootstrap.Card.Body style={{ borderRight: '1px solid gainsboro', borderLeft: '1px solid gainsboro', borderBottom: '1px solid gainsboro' }}>
                            <div style={{ marginTop: '4vh', marginBottom: '3vh' }}>
                                <textarea name="description" row="5" maxLength="300" placeholder="Share any update or task status with fellow volunteers" className="counter-textarea form-control" style={{ marginTop: '2vh' }} />


                                <div className="row" style={{ margin: '20px' }}>
                                    <div className="col-2">
                                        <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}>Upload</span>

                                    </div>
                                    <div className="col-8">
                                        <FileUpload />
                                    </div>



                                </div>






                                <div className="container" style={{ marginTop: '2vh' }}>
                                    <div className="row" style={{ margin: '20px' }}>
                                        <div className="col-2">
                                            <span style={{ marginLeft: '0%', fontWeight: '600', textAlign: 'right' }}>Visibility</span>

                                        </div>
                                        <div className="col-10">
                                            <div class="radio">
                                                <label className="radio-inline"><input type="radio" name="optradio" />Option 1</label>
                                                <label className="radio-inline"><input type="radio" name="optradio" />Option 2</label>
                                                <label className="radio-inline"><input type="radio" name="optradio" />Option 3</label>
                                            </div>
                                        </div>

                                    </div>

                                </div>




                            </div>
                        </ReactBootstrap.Card.Body>
                    </ReactBootstrap.Accordion.Collapse>

                </ReactBootstrap.Accordion>

            </>
        )
    }
}

export default NewUpdatePost;
