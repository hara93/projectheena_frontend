import React from 'react';
import CustomButton from './CustomButton'

function SingleNGOLayout(props) {
    return (
        <div >
            <p className="ngo-heading">{props.aboutngo.ngo_name}</p>
            <p className="ngo-subheading">{props.aboutngo.ngo_place}</p>
            <div><img className="ngo-icon" src={props.aboutngo.ngo_image} alt="" /></div>
            <div className="ngo-detail-body">
                <div className="container-fluid">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-sm-12 col-lg-7">
                                <div style={{ paddingLeft: '2vw' }}>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                    <div style={{ marginLeft: 190 }}><CustomButton content="Follow" /></div>
                                    <h2 style={{ marginTop: 30 }}>About Us</h2>
                                    <p>{props.aboutngo.about_ngo}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default SingleNGOLayout;
