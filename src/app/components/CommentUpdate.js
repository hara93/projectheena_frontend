import React from "react";
import { Col, Row } from "react-bootstrap";

import { Card, CardHeader } from "@material-ui/core";
import { Images } from "../config/Images";
import { CardBody } from "reactstrap";
import StarBorderIcon from "@material-ui/icons/StarBorder";

const ReplyView = () => (
  <>
    {/* <Row style={{ backgroundColor: "red" }}>
    <Col xs={5}> */}

    <input
      type="text"
      id="reply"
      name="fname"
      style={{
        display: "block",
        // marginLeft: '23%',
        width: "100%",
        height: "51px",
        // marginTop: '5%',
        color: "gainsboro",
        marginBottom: "1%",
      }}
      placeholder="Your reply here"
    ></input>
    <button
      style={{
        display: "block",
        padding: "1%",
        marginBottom: "1%",
        float: "right",
        // width: '10%',
        backgroundColor: "white",
        border: "1px solid gainsboro",
        alignItems: "end",
      }}
    >
      Reply
    </button>
    {/* </div> */}
    {/* </Col>
</Row> */}
  </>
);

class CommentUpdate extends React.Component {
  constructor() {
    super();
    this.state = {
      reply: false,
      star: false,
    };
  }

  handleReply = (event, index) => {
    // console.log("Reply handle", event, index);
    this.setState({ reply: !this.props.update[index].reply });

    this.props.update[index].reply = !this.props.update[index].reply;
    console.log("reply update", index, this.props.update[index].reply);
  };
  handleStar = () => {
    this.setState({ star: !this.state.star });
  };

  render() {
    return (
      <>
        <Row className="no-gutters">
          <Col xs={12}>
            {this.props.update.map((ele, index) => (
              <Row style={{ marginTop: "4vh", marginBottom: "4vh" }}>
                <Col xs={2}>
                  <img src={Images.Social_work} className="comment_img" />
                  <p style={{ marginTop: "-7px", marginBottom: "4px", marginLeft: "7px" }}>
                    Social Work
                  </p>
                  <p style={{ marginLeft: "7px" }}>4 Years Ago</p>
                </Col>

                <Col xs={10}>
                  <Row>
                    <Col xs={3}>
                      <p>{ele.heading}</p>
                    </Col>
                    <Col xs={7}>
                      <span style={{ width: "80%" }}>{ele.milestone}</span>
                    </Col>
                    <Col xs={2}>
                      {/* <a style={{ marginLeft: '12%', color: '#4caf4f' }} onClick={this.handleReply}><i class="fas fa-reply" style={{ color: '#4caf4f', marginRight: '3px' }}></i>Reply </a> */}
                      {/* <span style={{color:'#4CAF50'}}>Reply</span> */}
                      {/* <span><StarBorderIcon /></span> */}
                      <a
                        style={{ marginLeft: "12%", color: "#4caf4f" }}
                        onClick={(e) => this.handleReply(e, index)}
                      >
                        <i
                          class="fas fa-reply"
                          style={{ color: "#4caf4f", marginRight: "3px" }}
                        ></i>
                        Reply{" "}
                      </a>
                      {this.state.star == true ? (
                        <a>
                          <i
                            class="fas fa-star"
                            style={{ color: "#4caf4f", fontSize: "10px", marginLeft: "2%" }}
                          ></i>
                        </a>
                      ) : (
                        <a onClick={this.handleStar}>
                          <i
                            class="far fa-star"
                            style={{ color: "#4caf4f", fontSize: "10px", marginLeft: "2%" }}
                          ></i>
                        </a>
                      )}
                    </Col>
                  </Row>

                  <Row>
                    <Col xs={12}>
                      {this.props.update[index].reply == true ? <ReplyView /> : null}
                    </Col>
                  </Row>

                  <Row>
                    <Col xs={4}>
                      <Card>
                        <img src={Images.Children} style={{ width: "100%" }} />
                        <span
                          style={{
                            display: "block",
                            fontWeight: "600",
                            height: "20px",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            whiteSpace: "nowrap",
                            color: "#4CAF50",
                            padding: "2vh 1vw 5vh 1vw",
                          }}
                        >
                          {" "}
                          {ele.title}
                        </span>
                        <div style={{ marginBottom: "2vh" }}>
                          <xsall style={{ paddingLeft: "1vw", paddingBottom: "2vh" }}>
                            {ele.date}
                          </xsall>
                        </div>
                      </Card>
                    </Col>
                  </Row>
                </Col>
              </Row>
            ))}
          </Col>
        </Row>
      </>
    );
  }
}

export default CommentUpdate;
