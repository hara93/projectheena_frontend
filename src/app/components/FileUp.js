import axios from 'axios';

import React, { Component, useState } from 'react';

const FileInput = () => {

  const [inputList, setInputList] = useState([]);

  const onAddBtnClick = event => {
    setInputList(inputList.concat(<FileInput key={inputList.length} />));
  };

  const onRemoveBtnClick = event => {
    setInputList(inputList.slice(1, 1))
  };

  return <div className="mt-3">
    <input type="file" />
    <button style={{ marginTop: '3vh', border: '1px solid #4CAF50', color: '#4CAF50', backgroundColor: 'white' }}
      onClick={onAddBtnClick}
    >
      Add File
    </button>
    <button style={{ marginTop: '3vh', marginLeft: '2%', border: '1px solid red', color: 'red', backgroundColor: 'white' }}
      onClick={onRemoveBtnClick}
    >
      Remove File
    </button>
  </div>;
};

const FileUp = () => {

  const [inputList, setInputList] = useState([]);

  const onRemoveBtnClick = event => {
    setInputList(inputList.slice(1, 1))
  };


  const onAddBtnClick = event => {
    setInputList(inputList.concat(<FileInput key={inputList.length} />));
  };

  return (
    <div>
      <input type="file" />
      <button style={{ marginTop: '3vh', border: '1px solid #4CAF50', color: '#4CAF50', backgroundColor: 'white' }}
        onClick={onAddBtnClick}
      >
        Add File
    </button>
      <button style={{ marginTop: '3vh', marginLeft: '2%', border: '1px solid red', color: 'red', backgroundColor: 'white' }}
        onClick={onRemoveBtnClick}
      >
        Remove File
    </button>
      { inputList}
    </div >
  );
};

export default FileUp;