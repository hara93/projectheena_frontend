/* eslint-disable no-restricted-imports */
import React from "react";
import { Card } from "react-bootstrap";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import "../../index.scss";



function SurveyFormName(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClosed = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <a href="/survey" style={{ color: "inherit" }}>
        <button type="button" className="survey-form-names">
          <Card style={{ padding: "20px", borderRadius: "10px", border: "0" }}>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <h5 style={{ fontWeight: "bolder", fontSize: "20px" }}>
                {props.name}
              </h5>
              <Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
                style={{ paddingTop: "0", paddingBottom: "0" }}
              >
                <i class="fas fa-ellipsis-v"></i>
              </Button>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClosed}
              >
                <MenuItem>
                  <i
                    class="fas fa-download"
                    style={{ marginRight: "10px" }}
                  ></i>
                  Download Form
                </MenuItem>
                <MenuItem>
                  <i class="far fa-copy" style={{ marginRight: "10px" }}></i>
                  Make a copy
                </MenuItem>
              </Menu>
            </div>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "400px",
                }}
              >
                <div>
                  <p style={{ color: "gray" }}>{props.type}</p>
                </div>
                <div>
                  <p style={{ color: "gray" }}>Published</p>
                  <p>N/A</p>
                </div>
                <div>
                  <p style={{ color: "gray" }}>Modified</p>
                  <p>0 Minutes ago..</p>
                </div>
              </div>
              <div>
                <Button variant="light" style={{ padding: "5px" }}>
                  <div>
                    <p
                      style={{
                        textAlign: "right",
                        fontSize: "20px",
                        marginBottom: "0",
                      }}
                    >
                      8
                    </p>
                    <p style={{ color: "gray", marginBottom: "0" }}>
                      Questions
                    </p>
                  </div>
                </Button>
                <Button variant="light" style={{ padding: "5px" }}>
                  {" "}
                  <div>
                    <p
                      style={{
                        textAlign: "right",
                        fontSize: "20px",
                        marginBottom: "0",
                      }}
                    >
                      0
                    </p>
                    <p style={{ color: "gray", marginBottom: "0" }}>
                      Responses
                    </p>
                  </div>
                </Button>
              </div>
            </div>
          </Card>
        </button>
      </a>
    </>
  );
}
export default SurveyFormName;
