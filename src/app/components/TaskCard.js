import React from "react";
import { Col, Row, ProgressBar, Tab, Tabs, Form } from "react-bootstrap";
import * as ReactBootstrap from "react-bootstrap";
import { Card } from "@material-ui/core";
import CustomButtonOutline from "../components/CustomButtonOutline";
import { Images } from "../config/Images";
import TabNav from "../pages/TabNav";
import { TabContent } from "react-bootstrap";
import HorizontalTabContent from "../pages/HorizonatalTabContent";
import HorizontalTabNav from "../pages/HorizontalTabNav";
import Rating from "@material-ui/lab/Rating";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import HoverRating from "../components/stars";
import CustomButton from "./CustomButton";
import DiscreteSlider from "./Slider";
import DonationAmountSilder from "./donationAmountSlider";
import TaskAmountSilder from "./TaskAmountSlider";


class TaskCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      selected: "Appreciate",
      key: "home",
      DonationAmount: 1,
    };
  }
  openModal = () => this.setState({ isOpen: true });
  closeModal = () => this.setState({ isOpen: false });
  setSelected = (tab) => {
    this.setState({ selected: tab });
  };
  setKey = (k) => {
    this.setState({ key: k })

  }

  //  val = document.getElementById("vol").value;

  render() {
    return (
      <>
        <Row className="no-gutters">
          {this.props.volunteer.map((ele, index) => (
            <Card
              className="p-2"
              style={{
                width: '49%',
                marginRight: '1%',
                marginBottom: '1%',
                marginTop: '5%',
                height: '61%'
              }}

            >
              <Col md={5} style={{ marginTop: "3%", marginBottom: "3%" }}>
                <img
                  style={{ borderRadius: "50%", width: "80%" }}
                  src={Images.user_logo}
                ></img>
              </Col>
              <Col md={7} style={{ marginTop: "3%", marginBottom: "3%" }}>
                <h6 style={{ width: "70%" }}>
                  <strong>{ele.name}</strong>
                </h6>
                <p>
                  <i className="fa fa-map-marker"></i> {ele.location}
                </p>
                <p>
                  <i className="fa fa-phone"></i> {ele.phone}
                </p>
                <p>
                  <i className="fa fa-envelope"></i> {ele.mail}
                </p>
                <div onClick={this.openModal}>
                  <CustomButtonOutline content={`Evaluate`} />
                </div>
              </Col>
            </Card>
          ))}

          <ReactBootstrap.Modal
            show={this.state.isOpen}
            onHide={this.closeModal}
            style={{ opacity: "1" }}
            className="p-4"
            size="70px"
          >
            <ReactBootstrap.Modal.Header closeButton>
              <ReactBootstrap.Modal.Title>
                Evaluate "{this.props.volunteer[0].name}"
              </ReactBootstrap.Modal.Title>
            </ReactBootstrap.Modal.Header>
            <ReactBootstrap.Modal.Body>

              <Tabs
                id="controlled-tab-example"
                activeKey={this.state.key}
                onSelect={(k) => this.setKey(k)}
              >
                <Tab
                  eventKey="home"
                  title="Appreciate"
                  style={{ opacity: "100" }}
                >
                  <h4 className="p-4" style={{ fontWeight: '600' }}>Worked For {this.state.DonationAmount} Hours</h4>
                  <Row>
                    <Col md={6} className="p-4">

                      <div style={{ margin: "10px 0" }}>
                        <TaskAmountSilder
                          donationAmount={this.state.DonationAmount}
                          amount={(amount) => {
                            this.setState({ DonationAmount: amount });
                          }}
                        />{" "}
                      </div>
                    </Col>
                    <Col md={6} className="p-4">
                      <HoverRating />
                    </Col>
                  </Row>
                  <div style={{ margin: '10px' }}>
                    <textarea
                      name="description"
                      row="50"
                      maxLength="500"
                      placeholder="Write a thanks note to the use for completing your task"
                      className="counter-textarea form-control p-4"
                      style={{ height: "50%", marginBottom: '5px' }}
                    />
                    <p>Max: 500 chars | Remaining Chars: 500</p>
                  </div>


                  <div style={{ marginTop: "5%", marginLeft: "5%" }}>
                    <CustomButton content={`Appreciate`} />
                  </div>

                </Tab>
                <Tab
                  eventKey="profile"
                  title="Default"
                  style={{ opacity: "100" }}
                >
                  <h5 className="p-4">Reason for Defaulting</h5>

                  <textarea
                    name="description"
                    row="10"
                    maxLength="500"
                    placeholder="Reason for Defaulting this user on this task"
                    className="counter-textarea form-control p-4"
                    style={{ height: "50%" }}
                  />

                  <div style={{ marginTop: "5%", marginLeft: "5%" }}>
                    <CustomButton content={`Default`} />
                  </div>
                </Tab>
                <Tab eventKey="contact" title="Unutilized" style={{ opacity: "100" }}>
                  <h5 className="p-4">Reason for Unutilized</h5>

                  <textarea
                    name="description"
                    row="10"
                    maxLength="500"
                    placeholder="Reason for unutilizing this user on this task"
                    className="counter-textarea form-control p-4"
                    style={{ height: "50%" }}
                  />

                  <div style={{ marginTop: "5%", marginLeft: "5%" }}>
                    <CustomButton content={`Unutilized`} />
                  </div>
                </Tab>
              </Tabs>

              {/* <HorizontalTabNav
                tabs={["Appreciate", "Default", "Unutilized"]}
                selected={this.state.selected}
                setSelected={this.setSelected}
              >
                <HorizontalTabContent
                  isSelected={this.state.selected === "Appreciate"}
                >
                  <h3 className="p-4">Worked For 40 Hours</h3>
                  <Row>
                    <Col md={6} className="p-4">
                      <input
                        type="range"
                        id="vol"
                        name="vol"
                        min="0"
                        max="120"
                      ></input>
                    </Col>
                    <Col md={6} className="p-4">
                      <HoverRating />
                    </Col>
                  </Row>
                  <textarea
                    name="description"
                    row="50"
                    maxLength="500"
                    placeholder="Write a thanks note to the use for completing your task"
                    className="counter-textarea form-control p-4"
                    style={{ height: "50%" }}
                  />

                  <div style={{ marginTop: "5%", marginLeft: "5%" }}>
                    <CustomButton content={`Appreciate`} />
                  </div>
                </HorizontalTabContent>

                <HorizontalTabContent
                  isSelected={this.state.selected === "Default"}
                >
                  <h5 className="p-4">Reason for Defaulting</h5>

                  <textarea
                    name="description"
                    row="10"
                    maxLength="500"
                    placeholder="Reason for Defaulting this user on this task"
                    className="counter-textarea form-control p-4"
                    style={{ height: "50%" }}
                  />

                  <div style={{ marginTop: "5%", marginLeft: "5%" }}>
                    <CustomButton content={`Default`} />
                  </div>
                </HorizontalTabContent>

                <HorizontalTabContent
                  isSelected={this.state.selected === "Unutilized"}
                >
                  <h5 className="p-4">Reason for Unutilized</h5>

                  <textarea
                    name="description"
                    row="10"
                    maxLength="500"
                    placeholder="Reason for unutilizing this user on this task"
                    className="counter-textarea form-control p-4"
                    style={{ height: "50%" }}
                  />

                  <div style={{ marginTop: "5%", marginLeft: "5%" }}>
                    <CustomButton content={`Unutilized`} />
                  </div>
                </HorizontalTabContent>
              </HorizontalTabNav> */}
            </ReactBootstrap.Modal.Body>
          </ReactBootstrap.Modal>
        </Row>
      </>
    );
  }
}
export default TaskCard;
