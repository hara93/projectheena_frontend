import React from "react";
// import ReactDOM from "react-dom";
import ReactTags from "react-tag-autocomplete";
import { getTagMaster } from "../../redux/reducers/common/actionCreator";
import { connect } from "react-redux";
class TagAutocomplete extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tags: [],
      // suggestions: [
      //   { id: 3, name: "Dancing" },
      //   { id: 7, name: "Singing" },
      //   { id: 8, name: "Playing" },
      //   { id: 9, name: "Sports" },
      //   { id: 4, name: "Development" },
      // ],
      suggestions: [],
    };

    this.reactTags = React.createRef();
  }
  componentDidMount() {
    this.props.dispatch(getTagMaster());

    this.setState({ suggestions: this.props.tagMaster, tags: this.props.tag });
    this.setState({ tags: this.props.tag });
  }

  onDelete(i) {
    const tags = this.state.tags.slice(0);
    tags.splice(i, 1);
    this.setState({ tags });
    this.props.tags(tags);
  }

  onAddition(tag) {
    const tags = [].concat(this.state.tags, tag);

    this.setState({ tags });
    this.props.tags(tags);
  }

  render() {
    return (
      <ReactTags
        ref={this.reactTags}
        tags={this.props.tag}
        suggestions={this.state.suggestions}
        onDelete={this.onDelete.bind(this)}
        onAddition={this.onAddition.bind(this)}
        placeholderText=""
        noSuggestionsText="No such tags"
        minQueryLength={1}
        allowNew={true}
      />
    );
  }
}
const mapStateToProps = function(state) {
  return {
    tagMaster: state.common.tagMaster,
  };
};

export default connect(mapStateToProps)(TagAutocomplete);
