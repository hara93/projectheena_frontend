/* eslint-disable no-restricted-imports */
import React, { useState } from "react";
import "../../index.scss";

import Form from "react-bootstrap/Form";
import { Col, Card, Button, Row } from "react-bootstrap";
import CustomButton from "../components/CustomButton";
import "bootstrap/dist/css/bootstrap.min.css";
import Modal from "react-bootstrap/Modal";
import PlacesAutoComplete from "../components/PlacesAutoComplete";
import Example from "../components/Phone";
import SignatureCanvas from "react-signature-canvas";
import camera from "./TakeAPicture";
import ChildQuestion from "./ChildQuestions";
import { CompareArrowsOutlined } from "@material-ui/icons";

const SurveyFill = () => {
  const [count, setCount] = useState(0);
  const [show, setShow] = useState(false);
  const [number, setNumber] = useState(0);
  const [members, setMembers] = useState([]);
  const [road, setRoad] = useState(false);
  const [flight, setFlight] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleChange = (e) => {
    let arr = [];
    for (var i = 1; i <= e.target.value; i++) {
      arr.push("1");
      console.log("lala=", i);
    }
    console.log(arr);
    setNumber(e.target.value);
    setMembers(arr);
  };

  const handleRoad = () => {
    setRoad(!road);
  };

  const handleFlight = () => {
    setFlight(!flight);
  };

  var maxCount = 256;

  return (
    <div className="container">
      <Modal show={show} onHide={handleClose} animation={false} size="lg">
        {/* <Modal.Header closeButton>
          <Modal.Title>title of this modal</Modal.Title>
        </Modal.Header> */}

        <div
          style={{
            height: "60px",
            borderBottom: "1px solid #cccccc",
            padding: "15px 10px 5px 10px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h4>Select milestones</h4>
          <Button variant="secondary" style={{ height: "30px" }} onClick={handleClose}>
            <i class="fas fa-times" style={{ paddingRight: "0" }}></i>
          </Button>
        </div>
        <Modal.Body>
          {/* {camera.startCamera()}
                    {camera.takeSnapshot()} */}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <CustomButton content="Select milestones" />
        </Modal.Footer>
      </Modal>
      <div>
        <h2
          style={{
            fontWeight: "900",
            textAlign: "center",
            marginTop: "3%",
            marginBottom: "3%",
            color: "#4caf4f",
          }}
        >
          Survey
        </h2>
      </div>
      <Card>
        <span
          className="p-4"
          style={{
            backgroundColor: "#4caf4f",
            color: "white",
            width: "100%",
            marginBottom: "5%",
            fontSize: "24px",
          }}
        >
          Personal Information
        </span>
        <Form style={{ padding: "20px" }}>
          <Form.Group>
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Name
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="text" placeholder="Enter your name" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />

            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Age
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="number" placeholder="Enter your Age" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Gender
              </Form.Label>
              <Col>
                <div class="radio" style={{ marginTop: "4px" }}>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" />
                    Male
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" />
                    Female
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" />
                    Other
                  </label>
                </div>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />

            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Hobbies <span style={{ color: "red" }}>*</span>
              </Form.Label>
              <Col>
                <div class="checkbox" style={{ marginTop: "4px" }}>
                  <label class="radio-inline">
                    <input type="checkbox" />
                    Singing
                  </label>
                  <label class="radio-inline">
                    <input type="checkbox" />
                    Dancing
                  </label>
                  <label class="radio-inline">
                    <input type="checkbox" />
                    Reading
                  </label>
                </div>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Date of Data Collection:
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="date" placeholder="Date of data collection" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Time of Data Collection:
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="time" placeholder="Time of data collection" />
              </Col>
            </Form.Row>

            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Location
              </Form.Label>
              <Col>
                <PlacesAutoComplete />
              </Col>
            </Form.Row>

            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Phone Number
              </Form.Label>
              <Col>
                <Example />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Email ID
              </Form.Label>
              <Col>
                <Form.Control size="lg" type="email" placeholder="Enter email id" />
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Signature
              </Form.Label>
              <Col tyle={{ border: "1px solid gainsboro" }}>
                <SignatureCanvas
                  penColor="green"
                  canvasProps={{
                    width: 500,
                    height: 200,
                    className: "sigCanvas",
                  }}
                />
              </Col>
            </Form.Row>

            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Picture
              </Form.Label>
              <Col tyle={{ border: "1px solid gainsboro" }}>
                <div onClick={handleShow}>
                  <CustomButton content="Click to take a Picture" />
                </div>
              </Col>
            </Form.Row>
            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Number of Family Members
              </Form.Label>
              <div>
                <Col>
                  <Form.Control type="number" size="lg" onChange={handleChange} />
                </Col>
              </div>
            </Form.Row>
            <Row style={{ marginLeft: "18%", marginTop: "10px" }}>
              <ChildQuestion member={members} />
            </Row>

            <hr className="form-horizontal-line" />
            <Form.Row>
              <Form.Label column="lg" lg={2} className="form-label-custom">
                Transport Details<span style={{ color: "red" }}>*</span>
              </Form.Label>

              <Col style={{ marginLeft: "5%" }}>
                <p>Which of the following mode of transport do you use?</p>

                <div class="checkbox" style={{ marginTop: "4px" }}>
                  <label class="radio-inline">
                    <input type="checkbox" onChange={handleRoad} />
                    By Road
                  </label>
                  <label class="radio-inline">
                    <input type="checkbox" onChange={handleFlight} />
                    By Flight
                  </label>
                </div>
                {road == true ? (
                  <div class="checkbox" style={{ marginTop: "4px" }}>
                    <label class="radio-inline">
                      <input type="checkbox" />
                      Highway Road
                    </label>
                    <label class="radio-inline">
                      <input type="checkbox" />
                      Flyover
                    </label>
                  </div>
                ) : null}
                {flight == true ? (
                  <div class="checkbox" style={{ marginTop: "4px" }}>
                    <label class="radio-inline">
                      <input type="checkbox" />
                      Indigo
                    </label>
                    <label class="radio-inline">
                      <input type="checkbox" />
                      United Airways
                    </label>
                  </div>
                ) : null}
              </Col>
            </Form.Row>
            <Form.Row style={{ marginTop: "5%" }}>
              <Form.Label column="lg" lg={2}></Form.Label>
              <Col>
                <CustomButton content="Submit Survey" />
              </Col>
            </Form.Row>
          </Form.Group>

          {/* <button type="submit"> Submit</button> */}
        </Form>
      </Card>
    </div>
  );
};
export default SurveyFill;
