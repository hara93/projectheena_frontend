/* eslint-disable no-restricted-imports */
import React, { useEffect } from "react";
import "../../index.scss";
import { Table, Row, Col } from "react-bootstrap";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { useDispatch, useSelector } from "react-redux";
import { getCauseList } from "../../redux/reducers/common/actionCreator";
const RadiobuttonTable = ({ handleChange, name, touched, error, alreadyChecked }) => {
  // console.log(alreadyChecked);
  const dispatch = useDispatch();
  const common = useSelector((state) => (state.common && state.common.causeList) || null);
  useEffect(() => {
    dispatch(getCauseList());
  }, []);
  return (
    <>
      <RadioGroup name="checkboxTable">
        <Table responsive="xl">
          <tbody>
            <Row>
              {common &&
                common.map((element, index) => (
                  <Col xs={4} key={index}>
                    <FormControlLabel
                      id={index}
                      control={<Radio color="primary" />}
                      label={element.name}
                      value={element._id}
                      checked={alreadyChecked.includes(element._id)}
                      onChange={(e) => {
                        handleChange(e.target.value);
                      }}
                    />
                  </Col>
                ))}
            </Row>
          </tbody>
        </Table>
      </RadioGroup>
      {touched && error && <span className="text-danger">{error}</span>}
    </>
  );
};
export default RadiobuttonTable;
