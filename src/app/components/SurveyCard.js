import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Card } from '@material-ui/core';
import LineWeightIcon from '@material-ui/icons/LineWeight';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';



function SurveyCard(props) {
    return (
        <Row className="no-gutters">
                <Col md={12}>
                {props.survey_details.map((ele, index) => ( 
                    <Card className="p-5" style={{marginBottom:'5%'}}>
                       <Row >
                       
                           <div >
                           <Col md={4}>
                               <span>{ele.heading}</span>
                               <Row>
                                   <Col md={4}>
                                       <span>Status</span>
                                       <button style={{border:'1px solid #4CAF50', color:'#4CAF50', backgroundColor:'white'}}>{ele.status}</button>
                                   </Col>
                                   <Col md={2}>
                                   </Col>
                                   <Col md={4}>
                                       <span>{ele.published}</span>
                                   </Col>
                               </Row>
                           </Col>

                           <Col md={1}>
                               {/* break here */}
                           </Col>

                           <Col md={3} style={{marginTop:'2vh'}}>
                              <span style={{display:'block', marginTop:'2%'}}>Modified</span> 
                              <EditIcon/> <DeleteIcon/> <LineWeightIcon/>
                               
                           </Col>

                           <Col md={1}>
                               {/* break here */}
                           </Col>

                           <Col md={1} style={{marginTop:'2vh'}}>
                               <span style={{display:'block', textAlign:'right'}}>{ele.q_no}</span>
                               <span style={{display:'block'}}>Questions</span>
                           </Col>

                           <Col md={1}>
                               {/* break here */}
                           </Col>

                           <Col md={1} style={{marginTop:'2vh'}}>
                           <span style={{display:'block', textAlign:'right'}}>{ele.res_no}</span>
                               <span style={{display:'block',color:'#4CAF50'}}>Responses</span>
                           </Col>

                           </div>
                            
                       
                           
                       </Row>
                        
                        

                    </Card>
                     ))}
                </Col>

            </Row>
    )
}
export default SurveyCard;
