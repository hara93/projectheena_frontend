import React from 'react';
import { Row, Col } from 'react-bootstrap';
import * as ReactBootstrap from 'react-bootstrap';
import { Card } from '@material-ui/core';
import { Images } from '../config/Images';

class Initiatives extends React.Component {
    render() {
        return (
            <>
                <Row className="no-gutters">
                    <Row>
                        <Col xs={12}>
                            <h3 style={{ marginTop: '3%', marginBottom: '3%' }}>{this.props.init.length} Initiatives Completed by us</h3>
                        </Col>
                        {this.props.init.length > 0 ?
                            this.props.init.map((ele, index) => (
                                <Card className="p-3" style={{ width: '45%', marginRight: '5%', marginBottom: '5%' }} xs={12}>
                                    <Row className="p-3" xs={12}>
                                        <Col xs={8}>
                                            <p style={{ color: '#4CAF50', marginBottom: '5%' }}>{ele.name}</p>
                                            <p>{ele.content}</p>
                                        </Col>
                                        <Col xs={4}>
                                            <img style={{ width: '100%' }} src={Images.Social_work}></img>
                                        </Col>
                                    </Row>
                                    <Row className="p-2" style={{ textAlign: 'center' }}>
                                        <Col xs={4}>
                                            <p>Duration</p>
                                        </Col>
                                        <Col xs={4}>
                                            <p>People Required</p>
                                        </Col>
                                        <Col xs={4}>
                                            <p>Apply Before</p>
                                        </Col>
                                    </Row>
                                    <Row className="p-2" style={{ textAlign: 'center' }}>
                                        <Col xs={4}>
                                            <p>{ele.duration}</p>
                                        </Col>
                                        <Col xs={4}>
                                            <p>{ele.people}</p>
                                        </Col>
                                        <Col xs={4}>
                                            <p>&#8734;</p>
                                        </Col>
                                    </Row>
                                </Card>
                            )) : <p>No initiatives recorded</p>}
                    </Row>
                </Row>
            </>
        )
    }
}

export default Initiatives;