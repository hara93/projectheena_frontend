import React from "react";
import { Col, Card, Row } from "react-bootstrap";

function QuesType(props) {
  return (
    <>
      <Row className="no-gutters" className="p-2">
        {props.type.map((ele, index) => (
          <div style={{ width: "30%", marginBottom: "1%", marginRight: "3%" }}>
            <Card
              style={{
                marginRight: "3%",
                marginBottom: "1%",
                height: "100%",
                backgroundColor: "#f4f5f5",
                border: "1px solid #f4f5f5",
                width: "100%",
              }}
            >
              <Col md={11} style={{ marginTop: "3%", marginBottom: "3%" }}>
                <a href={ele.destination}> {ele.symbol}</a> <a>{ele.name}</a>
              </Col>
            </Card>
          </div>
        ))}
      </Row>
    </>
  );
}

export default QuesType;
