import React from "react";
import { Col, Row, Card } from "react-bootstrap";
// import { Card } from "@material-ui/core";
import { Images } from "../config/Images";

function SearchProjectCard(props) {
  return (
    <>
      <Row className="m-0">
        {props.project.map((ele, index) => (
          <Col lg={6} style={{ margin: "10px 0" }}>
            <Card className="p-2 project-card ">
              <Row>
                <Col xs={8}>
                  <div style={{ padding: "5px" }}>
                    <h4 className="font-bold" style={{ fontSize: "14px" }}>
                      <a href={`/createProjects/${ele.project_name}`} className="ph-link">
                        {ele.project_name}
                      </a>
                    </h4>
                    <p style={{ color: "gray", minHeight: "130px" }}>{ele.project_excerpts}</p>
                  </div>
                </Col>
                <Col xs={4}>
                  <div style={{ padding: "5px" }}>
                    <img
                      style={{ width: "100%", height: "50%" }}
                      src={Images.View_Profile}
                      alt=""
                    ></img>
                  </div>
                </Col>
              </Row>
            </Card>
          </Col>
        ))}
      </Row>
    </>
  );
}
export default SearchProjectCard;
