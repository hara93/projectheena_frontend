import React from "react";
import { Col, Row, Card } from "react-bootstrap";

function NGOCard(props) {
  return (
    <Row>
      <Col md={12} lg={9}>
        <Card style={{ padding: "15px 20px 20px 20px" }}>
          <Row>
            {props.ngolist.map((ele, index) => (
              <Col md={6}>
                <Card className="contact-box ">
                  <Row>
                    <Col md={4}>
                      <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                        <img
                          style={{
                            width: 95,
                            height: 95,
                            marginBottom: "0",
                          }}
                          src={ele.ngo_icon}
                          className="img-center img-circle"
                          alt=""
                        ></img>
                        <button
                          type="button"
                          className="csr-btn"
                          style={{ margin: "10px 0" }}
                        >
                          View Profile
                        </button>
                      </div>
                    </Col>
                    <Col md={8}>
                      <h3 style={{ fontSize: "16px" }}>
                        <a href="#" className="ngo-title">
                          {ele.ngo_name}{" "}
                        </a>
                      </h3>

                      <p>{ele.ngo_details}</p>
                    </Col>
                  </Row>
                </Card>
              </Col>
            ))}
          </Row>
        </Card>
      </Col>

      <Col md={12} lg={3}>
        <Card style={{ padding: "0", border: "0" }}>
          <div className="box-ibox-title">
            <h3 style={{ fontSize: "16px", fontWeight: "600" }}>
              Latest tasks on ProjectHeena
            </h3>
          </div>

          <div className="box-ibox-title">
            <ul className="fa-ul">
              {props.latestTasks.map((ele, index) => (
                <li>
                  <a className="ngo-latest-list" href="/search">
                    <span>
                      <i
                        class="fa fa-tasks"
                        style={{
                          position: "absolute",
                          left: "-20px",
                          top: "6px",
                          color: "inherit",
                        }}
                      ></i>
                    </span>
                    {ele.task_name}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </Card>
      </Col>
    </Row>
  );
}
export default NGOCard;
