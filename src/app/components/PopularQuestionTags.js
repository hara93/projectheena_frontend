import React from "react";
import { Tab, Row, Col, Nav } from "react-bootstrap";
import SingleChoice from "./SingleChoice";
import Text from "./text";
import Number from "./number";
import Location from "./location";
import Date from "./date";
import Time from "./time";
import Note from "./note";
import Signature from "./signature";
import SectionBreak from "./sectionBreak";
import AreaOnMap from "./areaOnMap";
import DistanceOnMap from "./distanceOnMap";
import MultipleChoice from "./multipleChoice";
import AddNewRule from "./addNewRule";
import DragTest from "./DragTest";

const PopularQuestionTags = () => {
  return (
    <>
      <Tab.Container id="left-tabs-example" defaultActiveKey="13">
        <Row>
          <Col sm={3}>
            <Nav variant="pills" className="flex-column">
              <Nav.Item>
                <Nav.Link eventKey="first">Single Choice</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="second">Multiple Choice</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="third">Text</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="fourth">Number</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="fifth">Location</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="sixth">Date</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="seventh">Time</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="8">Note</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="9">Signature</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="10">Section break</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="11">Area on map</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="12">Distance on map</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="13">rule</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="14">drag test</Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>

          <Col sm={9}>
            <Tab.Content>
              <Tab.Pane eventKey="first" style={{ opacity: "100" }}>
                <SingleChoice />
              </Tab.Pane>
              <Tab.Pane eventKey="second" style={{ opacity: "100" }}>
                <MultipleChoice />
              </Tab.Pane>
              <Tab.Pane eventKey="third" style={{ opacity: "100" }}>
                <Text />
              </Tab.Pane>
              <Tab.Pane eventKey="fourth" style={{ opacity: "100" }}>
                <Number />
              </Tab.Pane>
              <Tab.Pane eventKey="fifth" style={{ opacity: "100" }}>
                <Location />
              </Tab.Pane>
              <Tab.Pane eventKey="sixth" style={{ opacity: "100" }}>
                <Date />
              </Tab.Pane>
              <Tab.Pane eventKey="seventh" style={{ opacity: "100" }}>
                <Time />
              </Tab.Pane>
              <Tab.Pane eventKey="8" style={{ opacity: "100" }}>
                <Note />
              </Tab.Pane>
              <Tab.Pane eventKey="9" style={{ opacity: "100" }}>
                <Signature />
              </Tab.Pane>
              <Tab.Pane eventKey="10" style={{ opacity: "100" }}>
                <SectionBreak />
              </Tab.Pane>
              <Tab.Pane eventKey="11" style={{ opacity: "100" }}>
                <AreaOnMap />
              </Tab.Pane>
              <Tab.Pane eventKey="12" style={{ opacity: "100" }}>
                <DistanceOnMap />
              </Tab.Pane>
              <Tab.Pane eventKey="13" style={{ opacity: "100" }}>
                <AddNewRule />
              </Tab.Pane>
              <Tab.Pane eventKey="14" style={{ opacity: "100" }}>
                <DragTest />
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    </>
  );
};
export default PopularQuestionTags;
