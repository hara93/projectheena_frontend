export const Images = {
  // user: require('../assets/images/userlogo.jpg').default

  user: require("../assets/images/userlogo.jpg"),
  userlogin: require("../assets/images/user.png"),

  userlogo: require("../assets/images/userlogo.jpg"),

  volunteer: require("../assets/images/volunteering.png"),
  donation1: require("../assets/images/donation.png"),
  advocate: require("../assets/images/advocate.png"),
  collaborate: require("../assets/images/collaborate.png"),
  CSRDetail: require("../assets/images/CSRDetail.jpeg"),
  Social_work: require("../assets/images/social-work.jpeg"),
  Children: require("../assets/images/children.png"),
  NGOSignup: require("../assets/images/ngoIcon.png"),
  Employee: require("../assets/images/employee.png"),
  Monitoring: require("../assets/images/monitoring.png"),
  Philanthropy: require("../assets/images/philanthropy.png"),
  Advocacy: require("../assets/images/advocacy.png"),
  UserIcon: require("../assets/images/userIcon.png"),
  ngo: require("../assets/images/ngo.png"),

  team: require("../assets/images/team.png"),
  View_Profile: require("../assets/images/viewprofile.jpeg"),
  user_logo: require("../assets/images/userlogo.jpg"),
  gmail: require("../assets/images/gmail.gif"),
  Donation_detail: require("../assets/images/orange.jpeg"),

  animals: require("../assets/images/animals.jpeg"),

  volunteer1: require("../assets/images/volunteer.jpg"),

  csrBackgroundImg: require("../assets/images/csrbackgroundimage.png"),
  volunteers: require("../assets/images/volunteering.jpg"),
  opportunities: require("../assets/images/opportunities.png"),
  oneplat: require("../assets/images/oneplatform.png"),
  ironman: require("../assets/images/ironman.png"),
  idea: require("../assets/images/idea.png"),
  pm: require("../assets/images/pm.jpg"),
  donation: require("../assets/images/donation.jpg"),
  ph: require("../assets/images/me.jpeg"),
  advocacy: require("../assets/images/advocacy.jpg"),
  magic: require("../assets/images/magic.jpg"),
  collaboration: require("../assets/images/collaboration.png"),
  shield: require("../assets/images/shield.png"),
  electrical: require("../assets/images/electrical.png"),
  engineering: require("../assets/images/electrical.png"),
  coins: require("../assets/images/coins.png"),
  texture: require("../assets/images/texture.jpg"),
  sprite: require("../assets/images/sm-sprite.png"),
  // phlogo: require("../assets/images/phlogo.png"),
};
