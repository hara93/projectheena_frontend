export const Products = {
  camera1: require("../../assets/images/camera1.jpg"),
  mouse1: require("../../assets/images/mouse1.jpg"),
  headphone1: require("../../assets/images/headphone1.jpg"),
  banner1: require("../../assets/images/banner1.jpg"),
  banner2: require("../../assets/images/banner2.jpg"),
  banner3: require("../../assets/images/banner3.jpg"),
  physicallychallenged1: require("../../assets/images/physicallychallenged1.jpg"),
  physicallychallenged2: require("../../assets/images/physicallychallenged2.jpg"),
  skilldevelopment1: require("../../assets/images/skilldevelopment1.jpg"),
};
