import React, { useState } from 'react'
import { Row, Col } from "react-bootstrap";
import moment from "moment"

export default function GlenmarkFooter() {
    const [year, setYear] = useState(moment().format("Y"))
    return (
        <section style={{ background: "#facdbd", padding: "20px 60px" }}>
            <Row className="no-gutters">
                <Col md={3} style={{ padding: "10px 30px" }}>
                    <img style={{ width: "100%" }} src="https://glenmarkvolunteers.com/assets/img/team/glenmark/home/glenmark-foundation.png"></img>
                </Col>
                <Col md={9}>
                    <Row className="no-gutters">
                        <h2 style={{ color: "black" }} className="csrFooter">CSR @ Glenmark</h2>
                    </Row>
                    <Row className="no-gutters">
                        <p style={{ color: "black" }}>
                            Creating a healthier and happier world is our key mission, which we steadfastly strive to achieve through our business activities. Our commitment and efforts are not only limited to improvements in our operations. As a responsible corporate citizen, we are cognizant of our ability, means and influence to drive positive change in the society at large.
                        </p>
                    </Row>
                    <Row className="no-gutters">
                        <p style={{ color: "black" }}>
                            Corporate Social Responsibility (CSR) is therefore yet another way in which we strive to enrich lives of people in the larger society. Our CSR ethos complements our approach to doing business responsibly and demonstrates our unwavering commitment to giving back to the society.
                        </p>
                    </Row>
                    <Row className="no-gutters">
                        <p style={{ color: "black", fontSize: 12 }}>
                            © {year} Glenmark Pharma
                        </p>
                    </Row>
                </Col>
            </Row>
        </section>
    )
}