import React, { useMemo, useState } from "react";
import objectPath from "object-path";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../_helpers";
import { useHtmlClassService } from "../../_core/MetronicLayout";
import { Images } from "../../../../app/config/Images";
import { useHistory, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setUserLogout } from "../../../../redux/reducers/auth/actionCreator";

import "../../../../app/MobileHeader.css"
export function HeaderMobile() {
  const history = useHistory();
  const dispatch = useDispatch();
  const uiService = useHtmlClassService();

  const layoutProps = useMemo(() => {
    return {
      headerLogo: uiService.getStickyLogo(),
      asideDisplay: objectPath.get(uiService.config, "aside.self.display"),
      headerMenuSelfDisplay:
        objectPath.get(uiService.config, "header.menu.self.display") === true,
      headerMobileCssClasses: uiService.getClasses("header_mobile", true),
      headerMobileAttributes: uiService.getAttributes("header_mobile")
    };
  }, [uiService]);
  const [showMenu, setShowMenu] = useState(false);
  const [showSubMenu, setShowSubMenu] = useState(false)
  const [showProfileMenu, setShowProfileMenu] = useState(false)
  const handleShowSubmenu = (idx) => {
    showSubMenu === idx ? setShowSubMenu(false) : setShowSubMenu(idx)
  }
  const logoutClick = () => {
    history.push("/home")
    dispatch(setUserLogout())
  };
  const { isAuthorized } = useSelector(
    ({ phAuth }) => ({
      isAuthorized: phAuth.user != null,
    }),

  );
  const navitemsIfAuthorized = [
    {
      name: "Volunteer",
      link: "#",
      icon: "http://projectheena.in/assets/img/menu/ph/volunteer.png",
      submenu: [
        { name: "My Tasks", link: "/myTasks" },
        { name: "Search Tasks", link: "/searchtab" },
        { name: "Search Volunteer", link: "/searchtab" },
        { name: "Create Task", link: "/createTask" },
        { name: "Create Karmalog", link: "#" },
        { name: "Volunteering Certificates", link: "#" },
        { name: "About Volunteering", link: "/help" }
      ]
    },
    {
      name: "Donate",
      link: "#",
      icon: "http://projectheena.in/assets/img/menu/ph/donate.png",
      submenu: [
        { name: "Search Donations", link: "#" },
        { name: "Donations Reports", link: "#" },
        { name: "About Donation", link: "/help" }
      ]
    },
    {
      name: "Advocate",
      link: "#",
      icon: "http://projectheena.in/assets/img/menu/ph/advocate.png",
      submenu: [
        { name: "Amplify", link: "#" },
        { name: "Write Blog", link: "#" },
        { name: "Blog", link: "#" },
        { name: "About Advocacy", link: "/help" }
      ]
    },
    {
      name: "CSR Projects",
      link: "#",
      icon: "http://projectheena.in/assets/img/menu/ph/collaborate.png",
      submenu: [
        { name: "Search Projects", link: "#" },
        { name: "My Projects", link: "#" },
        { name: "About Collaboration", link: "/help" }
      ]
    },
  ]
  const userProfileMenuList = [
    {
      name: "DASHBOARD",
      link: "#"
    },
    {
      name: "EDIT PROFILE",
      link: "/editProfileUser"
    },
    {
      name: "VIEW PROFILE",
      link: "/userprofile"
    },
    {
      name: "ACCOUNT SETTINGS",
      link: "/settings"
    },
    {
      name: "INVITE FRIENDS",
      link: "/invite"
    },
    {
      name: "MY NETWORK",
      link: "/network"
    },
    {
      name: "MY ACHIEVEMENTS",
      link: "#"
    },
    {
      name: "MY BLOG",
      link: "#"
    },
    {
      name: "HELP",
      link: "/help"
    },

  ]
  const navitemsIfNotAuthorized = [
    {
      name: "Volunteer",
      link: "#",
      icon: "http://projectheena.in/assets/img/menu/ph/volunteer.png"
    },
    {
      name: "Donate",
      link: "#",
      icon: "http://projectheena.in/assets/img/menu/ph/donate.png"
    },
    {
      name: "Advocate",
      link: "#",
      icon: "http://projectheena.in/assets/img/menu/ph/advocate.png"
    },
    {
      name: "CSR Projects",
      link: "#",
      icon: "http://projectheena.in/assets/img/menu/ph/collaborate.png"
    },
    {
      name: "Search",
      link: "#",
      icon: null,
    },
    {
      name: "Login",
      link: "#",
      icon: null,
    }
  ]

  return (
    <>
      {/*begin::Header Mobile*/}
      <div
        id="kt_header_mobile"
        className={`header-mobile align-items-center ${layoutProps.headerMobileCssClasses}`}
        {...layoutProps.headerMobileAttributes}
      >
        {<div className={`overlay ${showMenu && 'active'}`} >
          <div className="overlay-container">
            <div className="d-flex justify-content-end">
              <i className="fas fa-times menu-close-icon" onClick={() => setShowMenu(x => !x)} ></i>
            </div>
            {!isAuthorized ? <>
              <div className="menu-sub-container">
                {navitemsIfNotAuthorized.map((x, idx) => (
                  <div key={idx} className="menu-sections">
                    <div className="d-flex justify-content-between" onClick={() => handleShowSubmenu(idx)}>
                      <div>
                        {x.icon && <img src={x.icon} className="menu-icons" alt="" />}
                        <span className="menu-heading">{x.name}</span>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </> :
              <div className="menu-sub-container">
                {navitemsIfAuthorized.map((x, idx) => (
                  <div key={idx} className="menu-sections">
                    <div className="d-flex justify-content-between" onClick={() => handleShowSubmenu(idx)}>
                      <div>
                        <img src={x.icon} className="menu-icons" alt="" />
                        <span className="menu-heading">{x.name}</span>
                      </div>
                      {showSubMenu === idx ? <i className="fas fa-minus" style={{ color: "#fff" }} ></i> : <i className="fas fa-plus" style={{ color: "#fff" }} ></i>}

                    </div>
                    {
                      showSubMenu === idx && <div className="d-flex flex-column">
                        {x.submenu.map((s, idx) => (
                          // <span className="menu-subheading">{s.name}</span>
                          <Link key={idx} className="menu-subheading" to={s.link}>
                            {s.name}
                          </Link>
                        ))}
                      </div>
                    }
                  </div>
                ))}
                <div className="menu-sections" >
                  <img src="http://projectheena.in/assets/img/menu/ph/notification.png" alt="" className="menu-icons" />
                  <span className="menu-heading"> Notifications</span>
                </div>
                <div className="menu-sections">
                  <div className="d-flex justify-content-between" onClick={() => setShowProfileMenu(x => !x)}>
                    <div>
                      <img src={Images.userlogo} alt="" className="menu-icons" style={{ borderRadius: "50%" }} /><span className="menu-heading">ME</span>
                    </div>
                    {showProfileMenu ? <i className="fas fa-minus" style={{ color: "#fff" }} ></i> : <i className="fas fa-plus" style={{ color: "#fff" }} ></i>}

                  </div>
                  <div className="d-flex flex-column">
                    {showProfileMenu && userProfileMenuList.map((x, idx) => (
                      // <span className="menu-subheading"> {x.name}</span>
                      <Link key={idx} className="menu-subheading" to={x.link}>
                        {x.name}
                      </Link>
                    ))
                    }
                  </div>
                </div>
                <div onClick={logoutClick} className="menu-sections">
                  <img src="http://projectheena.in/assets/img/menu/logout.png" alt="" className="menu-icons" style={{ borderRadius: "50%" }} />
                  <span className="menu-heading">Logout</span>
                </div>

              </div>}

          </div>
        </div>}
        {/*begin::Logo*/}
        <Link to="/home">
          {/* <img alt="logo" src={layoutProps.headerLogo} /> */}
          <img width={200} alt="logo" src="http://projectheena.in/assets/img/phlogo.png" />
        </Link>
        {/*end::Logo*/}

        {/*begin::Toolbar*/}
        <div className="d-flex align-items-center">
          {layoutProps.asideDisplay && (
            <>
              {/*begin::Aside Mobile Toggle*/}
              <button className="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
                <span />
              </button>
              {/*end::Aside Mobile Toggle*/}
            </>
          )}

          {layoutProps.headerMenuSelfDisplay && (
            <>
              {/*begin::Header Menu Mobile Toggle*/}
              {/* <button className="btn p-0 burger-icon ml-4" id="kt_header_mobile_toggle" onClick={() => setShowMenu(x => !x)}>
                <span />
              </button> */}
              <i className="fas fa-bars" style={{ fontSize: "25px", color: "#4caf4f" }} onClick={() => setShowMenu(x => !x)}></i>
              {/*end::Header Menu Mobile Toggle*/}
            </>
          )}


          {/*begin::Topbar Mobile Toggle*/}
          <button
            className="btn btn-hover-text-primary p-0 ml-2"
            id="kt_header_mobile_topbar_toggle"
          >
            <span className="svg-icon svg-icon-xl">
              <SVG src={toAbsoluteUrl("/media/svg/icons/General/User.svg")} />
            </span>
          </button>
          {/*end::Topbar Mobile Toggle*/}
        </div>
        {/*end::Toolbar*/}
      </div>
      {/*end::Header Mobile*/}
    </>
  );
}
