import React, { useMemo } from "react";
import objectPath from "object-path";
import { useHtmlClassService } from "../../_core/MetronicLayout";

export function GlenmarkTopbar() {
    const uiService = useHtmlClassService();
    const layoutProps = useMemo(() => {
        return {
            viewSearchDisplay: objectPath.get(uiService.config, "extras.search.display"),
            viewNotificationsDisplay: objectPath.get(uiService.config, "extras.notifications.display"),
            viewQuickActionsDisplay: objectPath.get(uiService.config, "extras.quick-actions.display"),
            viewCartDisplay: objectPath.get(uiService.config, "extras.cart.display"),
            viewQuickPanelDisplay: objectPath.get(uiService.config, "extras.quick-panel.display"),
            viewLanguagesDisplay: objectPath.get(uiService.config, "extras.languages.display"),
            viewUserDisplay: objectPath.get(uiService.config, "extras.user.display"),
        };
    }, [uiService]);

    return (
        <div className="topbar">
            <span
                style={{
                    padding: "17px 34px",
                    lineHeight: "30px",
                    fontSize: "16px",
                    fontWeight: "600",
                    marginLeft: "200%"
                }}
            >
                <a href="/searchTab" style={cssStyle.navFont}>
                    LOGIN
                </a>
            </span>
        </div>
    );
}
const cssStyle = {
    navFont: {
        padding: "3px",
        fontSize: "14px",
        fontWeight: "900",
        color: "red"
    }
}