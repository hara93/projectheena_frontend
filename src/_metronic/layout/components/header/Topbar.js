import React from "react";
import { shallowEqual, useSelector, useDispatch } from "react-redux";

// import objectPath from "object-path";
// import NotificationsIcon from "@material-ui/icons/Notifications";
// import PersonIcon from "@material-ui/icons/Person";
// import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
// import PanToolIcon from "@material-ui/icons/PanTool";
// import SVG from "react-inlinesvg";
// import { OverlayTrigger, Tooltip } from "react-bootstrap";
// import { toAbsoluteUrl } from "../../../_helpers";
// import { useHtmlClassService } from "../../_core/MetronicLayout";
// import { SearchDropdown } from "../extras/dropdowns/search/SearchDropdown";
// import { UserNotificationsDropdown } from "../extras/dropdowns/UserNotificationsDropdown";
// import { QuickActionsDropdown } from "../extras/dropdowns/QuickActionsDropdown";
// import { MyCartDropdown } from "../extras/dropdowns/MyCartDropdown";
// import { LanguageSelectorDropdown } from "../extras/dropdowns/LanguageSelectorDropdown";
// import { QuickUserToggler } from "../extras/QuiclUserToggler";
import { Images } from "../../../../app/config/Images";
import { useHistory, Link } from "react-router-dom";
import { setUserLogout } from "../../../../redux/reducers/auth/actionCreator";
import { NavDropdown } from "react-bootstrap"
export function Topbar() {
  const history = useHistory();
  const dispatch = useDispatch();

  // const uiService = useHtmlClassService();
  // const layoutProps = useMemo(() => {
  //   return {
  //     viewSearchDisplay: objectPath.get(uiService.config, "extras.search.display"),
  //     viewNotificationsDisplay: objectPath.get(uiService.config, "extras.notifications.display"),
  //     viewQuickActionsDisplay: objectPath.get(uiService.config, "extras.quick-actions.display"),
  //     viewCartDisplay: objectPath.get(uiService.config, "extras.cart.display"),
  //     viewQuickPanelDisplay: objectPath.get(uiService.config, "extras.quick-panel.display"),
  //     viewLanguagesDisplay: objectPath.get(uiService.config, "extras.languages.display"),
  //     viewUserDisplay: objectPath.get(uiService.config, "extras.user.display"),
  //   };
  // }, [uiService]);

  const { isAuthorized } = useSelector(
    ({ phAuth }) => ({
      isAuthorized: phAuth.user != null,
    }),
    shallowEqual
  );
  const logoutClick = () => {
    history.push("/home")
    dispatch(setUserLogout())
  };
  const userProfileMenuList = [
    {
      name: "DASHBOARD",
      link: "/me"
    },
    {
      name: "EDIT PROFILE",
      link: "/editProfileUser"
    },
    {
      name: "VIEW PROFILE",
      link: "/userprofile"
    },
    {
      name: "ACCOUNT SETTINGS",
      link: "/settings"
    },
    {
      name: "INVITE FRIENDS",
      link: "/invite"
    },
    {
      name: "MY NETWORK",
      link: "/network"
    },
    {
      name: "MY ACHIEVEMENTS",
      link: "/myAchievements"
    },
    {
      name: "MY BLOG",
      link: "#"
    },
    {
      name: "HELP",
      link: "/help"
    },

  ]
  return (
    <>
      {
        !isAuthorized ?
          <div className="topbar">
            <span
              style={{
                padding: "17px 34px",
                lineHeight: "30px",
                fontSize: "16px",
                fontWeight: "600",

              }}
            >
              <Link to="/searchTab" style={{ color: "#676A6C" }}>
                Search
              </Link>
            </span>

            <span
              style={{
                padding: "17px 34px",
                lineHeight: "30px",
                fontSize: "16px",
                fontWeight: "600",
              }}
            >
              <Link to="/signup" style={{ color: "#676A6C" }}>
                Login
              </Link>
            </span>
          </div>
          :
          <div className="topbar">
            <span
              style={{
                padding: "17px 25px",
                lineHeight: "30px",
                fontSize: "16px",
                fontWeight: "600",

              }}
            >
              <a href="/searchTab" style={{ color: "#676A6C" }}>
                <img src="http://projectheena.in/assets/img/menu/ph/notification.png" alt="" style={{ width: "24px" }} />
                <span class="badge badge-warning text-white" style={{ padding: "3px 4px", top: "13px", position: "absolute", borderRadius: 0, boxShadow: "none" }}>0</span>
              </a>
            </span>
            <span
              style={{
                padding: "17px 5px",
                lineHeight: "30px",
                fontSize: "16px",
                fontWeight: "600",
              }}
            >
              <img src={Images.userlogo} alt="" style={{ width: "30px", borderRadius: "50%" }} />
            </span>
            {/* <span
              style={{
                padding: "18px 5px 17px 5px",
                lineHeight: "30px",
                fontSize: "14px",
                fontWeight: "600",
              }}
            >
              <a href="#" style={{ color: "#676A6C" }}>
                ME
                <span style={{ position: "absolute", padding: "0 5px" }}>
                  <i class="fas fa-caret-down"></i>
                </span>
              </a>
            </span> */}
            <div style={{
              display: 'flex',
              alignItems: "center"
            }}>

              <NavDropdown title="ME" id="nav-dropdown" className="d-flex flex-column" style={{ padding: 0 }}>
                {
                  userProfileMenuList.map((ele, index) => (
                    // <NavDropdown.Item eventKey="4.1" >
                    //   </NavDropdown.Item>
                    <Link key={index} className="userprofile-link d-flex" to={ele.link}>
                      {ele.name}
                    </Link>
                  ))
                }
              </NavDropdown>
            </div>
            <span
              style={{
                padding: "17px 0",
                lineHeight: "30px",
                fontSize: "16px",
                fontWeight: "600",
              }}
              onClick={logoutClick}
            >
              <img src="http://projectheena.in/assets/img/menu/logout.png" alt="" style={{ width: "26px", borderRadius: "50%" }} />
            </span>
          </div>
      }
    </>
  );
}
