import React, { useMemo } from "react";
import objectPath from "object-path";
import { Link } from "react-router-dom";
import { useHtmlClassService } from "../../../_core/MetronicLayout";
import { HeaderMenu } from "./HeaderMenu";
import { HeaderMenuGlenmark } from "./HeaderMenuGlenmark";

export function HeaderMenuWrapper() {
  const uiService = useHtmlClassService();
  var url = window.location.href
  console.log("url-header-glenmark", url)
  const layoutProps = useMemo(() => {
    return {
      config: uiService.config,
      ktMenuClasses: uiService.getClasses("header_menu", true),
      rootArrowEnabled: objectPath.get(uiService.config, "header.menu.self.root-arrow"),
      menuDesktopToggle: objectPath.get(uiService.config, "header.menu.desktop.toggle"),
      headerMenuAttributes: uiService.getAttributes("header_menu"),
      headerSelfTheme: objectPath.get(uiService.config, "header.self.theme"),
      ulClasses: uiService.getClasses("header_menu_nav", true),
      disabledAsideSelfDisplay: objectPath.get(uiService.config, "aside.self.display") === false,
    };
  }, [uiService]);
  const getHeaderLogo = () => {
    return url.match(/glenmark/gi) ? "https://glenmarkvolunteers.com/./uploads/teams/42144472117357/topbar-image.png" : "http://projectheena.in/assets/img/phlogo.png"
  };

  const getHeaderLogoCss = () => {
    return url.match(/glenmark/gi) ? cssStyle.glenLogo : cssStyle.phLogo
  };

  const getHomeLink = () => {
    return url.match(/glenmark/gi) ? "/glenmark/home" : "/home"
  };

  return (
    <>
      {/*begin::Header Menu Wrapper*/}
      <div className="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
        {layoutProps.disabledAsideSelfDisplay && (
          <>
            {/*begin::Header Logo*/}
            <div className="header-logo">
              <Link to={getHomeLink()}>
                <img
                  alt="logo"
                  src={getHeaderLogo()}
                  style={getHeaderLogoCss()}
                />
              </Link>
            </div>
            {/*end::Header Logo*/}
          </>
        )}
        {/*begin::Header Menu*/}
        {url.match(/glenmark/gi) ?
          <HeaderMenuGlenmark layoutProps={layoutProps} style={{ color: "red" }} />
          :
          <HeaderMenu layoutProps={layoutProps} style={{ color: "green" }} />}
        {/*end::Header Menu*/}
      </div>
      {/*Header Menu Wrapper*/}
    </>
  );
}

const cssStyle = {
  phLogo: {
    borderColor: "black",
    width: "175px",

  },
  glenLogo: {
    borderColor: "white",
    width: "95px",
    marginLeft: "-140px",
  }
}