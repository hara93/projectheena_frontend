/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import { useLocation } from "react-router";
import { shallowEqual, useSelector } from "react-redux";
import { NavLink, Link } from "react-router-dom";
// import SVG from "react-inlinesvg";
import { checkIsActive } from "../../../../_helpers";
import { NavDropdown, Nav } from 'react-bootstrap'

const navitemsIfNotAuthorized = [
    {
        name: "Volunteer",
        link: "#",
        icon: "http://projectheena.in/assets/img/menu/ph/volunteer.png"
    },
    {
        name: "Donate",
        link: "#",
        icon: "http://projectheena.in/assets/img/menu/ph/donate.png"
    },
    {
        name: "Advocate",
        link: "#",
        icon: "http://projectheena.in/assets/img/menu/ph/advocate.png"
    },
    {
        name: "CSR Projects",
        link: "#",
        icon: "http://projectheena.in/assets/img/menu/ph/collaborate.png"
    },

]

const navitemsIfAuthorized = [
    {
        name: "Volunteer",
        link: "#",
        icon: "http://projectheena.in/assets/img/menu/ph/volunteer.png",
        submenu: [
            { name: "My Tasks", link: "/myTasks" },
            { name: "Search Tasks", link: "/searchtab?tab=task" },
            { name: "Search Volunteer", link: "/searchtab?tab=people" },
            { name: "Create Task", link: "/createTask" },
            { name: "Create Karmalog", link: "#" },
            { name: "Volunteering Certificates", link: "/view-certificates" },
            { name: "About Volunteering", link: "/help" }
        ]
    },
    {
        name: "Donate",
        link: "#",
        icon: "http://projectheena.in/assets/img/menu/ph/donate.png",
        submenu: [
            { name: "Search Donations", link: "/searchtab?tab=donation" },
            { name: "Donations Reports", link: "/DonationReports" },
            { name: "About Donation", link: "/help" }
        ]
    },
    {
        name: "Advocate",
        link: "#",
        icon: "http://projectheena.in/assets/img/menu/ph/advocate.png",
        submenu: [
            { name: "Amplify", link: "#" },
            { name: "Write Blog", link: "#" },
            { name: "Blog", link: "#" },
            { name: "About Advocacy", link: "/help" }
        ]
    },
    {
        name: "CSR Projects",
        link: "#",
        icon: "http://projectheena.in/assets/img/menu/ph/collaborate.png",
        submenu: [
            { name: "Search Projects", link: "/searchtab?tab=project" },
            { name: "My Projects", link: "/myProjects" },
            { name: "About Collaboration", link: "/help" }
        ]
    },
]

export function HeaderMenu({ layoutProps }) {
    const location = useLocation();
    const getMenuItemActive = (url) => {
        return checkIsActive(location, url) ? "menu-item-active" : "";
    };
    const [navItems, setNavItems] = useState(navitemsIfNotAuthorized)
    const { isAuthorized } = useSelector(
        ({ phAuth }) => ({
            isAuthorized: phAuth.user != null,
        }),
        shallowEqual
    );

    useEffect(() => {
        isAuthorized ? setNavItems(navitemsIfAuthorized) : setNavItems(navitemsIfNotAuthorized)
    }, [isAuthorized])


    return (
        <div id="kt_header_menu" className={`header-menu header-menu-mobile ${layoutProps.ktMenuClasses}`}
            {...layoutProps.headerMenuAttributes}>
            <ul className={`menu-nav ${layoutProps.ulClasses}`} >
                {navItems.map((nav, index) =>
                    <li key={index} data-menu-toggle={layoutProps.menuDesktopToggle} aria-haspopup="true" className={`ph-nav-dropdown menu-item menu-item-submenu menu-item-rel ${getMenuItemActive("/")}`}>
                        <img src={nav.icon} style={{ maxHeight: "24px", verticalAlign: "middle" }} alt="" />
                        {nav.submenu && nav.submenu.length > 0 ?
                            <NavDropdown title={nav.name} id="basic-nav-dropdown">
                                {nav.submenu.map((subnav, index) =>
                                    <div key={index}>
                                        <NavDropdown.Item>
                                            <Link to={subnav.link}>
                                                {subnav.name}
                                            </Link>
                                        </NavDropdown.Item>
                                        {index == nav.submenu.length - 2 ? <NavDropdown.Divider /> : null}
                                        {/* <Link className="userprofile-link d-flex" to={subnav.link}>
                                            {subnav.name}
                                        </Link> */}
                                        {/* {index === nav.submenu.length - 2 ? <NavDropdown.Divider /> : null} */}
                                    </div>
                                )}
                            </NavDropdown> :
                            <NavLink className=" menu-toggle" to="/help">
                                <span className="ph-header-menu-no-submenu">
                                    {nav.name}
                                </span>
                            </NavLink>
                        }
                    </li>
                )}
            </ul>
        </div >
    );
}