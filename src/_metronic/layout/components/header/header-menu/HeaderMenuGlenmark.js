/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";

export function HeaderMenuGlenmark({ layoutProps }) {
    console.log("hello this is glenmark header")
    const location = useLocation();
    const getMenuItemActive = (url) => {
        return checkIsActive(location, url) ? "menu-item-active" : "";
    };
    return (
        <div
            id="kt_header_menu"
            className={`header-menu header-menu-mobile ${layoutProps.ktMenuClasses}`}
            {...layoutProps.headerMenuAttributes}
        >
            {/*begin::Header Nav*/}
            <ul
                className={`menu-nav ${layoutProps.ulClasses}`}
                style={{ marginRight: "100px" }}
            >
                {/*begin::1 Level*/}
                <li
                    data-menu-toggle={layoutProps.menuDesktopToggle}
                    aria-haspopup="true"
                    className={`menu-item menu-item-submenu menu-item-rel ${getMenuItemActive(
                        "/"
                    )}`}
                >
                    <NavLink className=" menu-toggle" to="/help">
                        <span
                            style={cssStyle.navFont}
                        >
                            Home
                        </span>
                    </NavLink>
                </li>
                <li
                    data-menu-toggle={layoutProps.menuDesktopToggle}
                    aria-haspopup="true"
                    className={`menu-item menu-item-submenu menu-item-rel ${getMenuItemActive(
                        "/"
                    )}`}
                >
                    <NavLink className=" menu-toggle" to="/help">
                        <span
                            style={cssStyle.navFont}
                        >
                            Audio Story{" "}
                        </span>
                        <i className="menu-arrow"></i>
                    </NavLink>
                </li>
                <li
                    data-menu-toggle={layoutProps.menuDesktopToggle}
                    aria-haspopup="true"
                    className={` menu-item menu-item-submenu menu-item-rel ${getMenuItemActive(
                        "/"
                    )}`}
                >
                    <NavLink className=" menu-toggle" to="/help">
                        <span
                            style={cssStyle.navFont}
                        >
                            <i class="fa fa-location-arrow" style={cssStyle.iconFont}></i>
                            GlenMark Giving{" "}
                        </span>
                    </NavLink>
                </li>
                <li
                    data-menu-toggle={layoutProps.menuDesktopToggle}
                    aria-haspopup="true"
                    className={`menu-item menu-item-submenu menu-item-rel ${getMenuItemActive(
                        "/"
                    )}`}
                >
                    <NavLink className=" menu-toggle" to="/help">
                        <span
                            style={cssStyle.navFont}
                        >
                            JOG Wall{" "}
                        </span>
                        <i className="menu-arrow"></i>
                    </NavLink>
                </li>
                <li
                    data-menu-toggle={layoutProps.menuDesktopToggle}
                    aria-haspopup="true"
                    className={`menu-item menu-item-submenu menu-item-rel ${getMenuItemActive(
                        "/"
                    )}`}
                >
                    <NavLink className=" menu-toggle" to="/help">
                        <span
                            style={cssStyle.navFont}
                        >
                            <i class="fa fa-question-circle" style={cssStyle.iconFont}></i>
                            FAQ{" "}
                        </span>
                        <i className="menu-arrow"></i>
                    </NavLink>
                </li>
            </ul>
        </div>
    );
}
const cssStyle = {
    navFont: {
        padding: "3px",
        fontSize: "14px",
        fontWeight: "900",
        color: "red",
    },
    iconFont: {
        padding: "3px",
        fontSize: "12px",
        fontWeight: "900",
        color: "red",
    }
}
