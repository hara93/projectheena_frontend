import { createStore, applyMiddleware, compose } from "redux";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { persistStore, persistReducer } from "redux-persist";
import { rootReducer } from "./rootReducer";

const persistConfig = {
  key: "root",
  storage,
};
const middlewares = [thunk];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
let middleware = composeEnhancers(applyMiddleware(...middlewares));
if (process.env.NODE_ENV !== "production") {
  middleware = composeWithDevTools(applyMiddleware(...middlewares));
}

export const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, {}, middleware);

export const persistor = persistStore(store);
export default store;
