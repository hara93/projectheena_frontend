import { actionTypes } from "./types";

const initialState = {
  user: undefined,
  token: undefined,
  ngo: undefined,
  otherUser: undefined,
  profileDeatils: undefined,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN:
      console.log("action.payload", action.payload)
      return { ...state, user: { ...action.payload } };

    case actionTypes.LOGOUT:
      return { ...state, ...initialState };

    case actionTypes.NGOLOGIN:
      return { ...state, ngo: { ...action.payload } };

    case actionTypes.OTHERUSER:
      return { ...state, otherUser: { ...action.payload } };

    case actionTypes.PROFILEDETAILS:
      return { ...state, profileDeatils: { ...action.payload } };

    default:
      return state;
  }
};

export default reducer;
