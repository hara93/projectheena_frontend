import { actionTypes } from "./types";

export const setUserData = (userData) => {
  return { type: actionTypes.LOGIN, payload: userData };
};

export const setNgoData = (userData) => {
  return { type: actionTypes.NGOLOGIN, payload: userData };
};

export const setOtherUser = (userData) => {
  return { type: actionTypes.OTHERUSER, payload: userData };
};

export const setProfileDetails = (profileDetails) => {
  return { type: actionTypes.PROFILEDETAILS, payload: profileDetails };
};

export const setUserLogout = () => {
  return { type: actionTypes.LOGOUT };
};