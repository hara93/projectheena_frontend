import { actionTypes } from "./types";

const initialState = {
  causeList: undefined,
  skillsList: undefined,
  tagMaster: undefined,
  taskList: undefined,
  usersList: undefined,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CAUSES_SUPPORTED_SUCCESS:
      return { ...state, causeList: [...action.payload] };
    case actionTypes.SKILL_LIST_SUCCESS:
      return { ...state, skillsList: { ...action.payload } };
    case actionTypes.TAG_MASTER_LIST:
      return { ...state, tagMaster: [...action.payload] };
    case actionTypes.TASK_LIST_SUCCESS:
      return { ...state, taskList: [...action.payload] };
    case actionTypes.USER_LIST_SUCCESS:
      return { ...state, usersList: [...action.payload] };
    default:
      return state;
  }
};

export default reducer;
