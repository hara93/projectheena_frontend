import { actionTypes } from "./types";
import axios from "../../../app/config/axios";

export const getCauseList = () => (dispatch) => {
  // console.log("in causeList action", process.env.REACT_APP_API_BASE_URL);
  axios
    .get("/cause/list", {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    })
    .then((res) => {
      // console.log("api res", res);
      if (res && res.status) {
        dispatch(causeListSuccess(res.data || []));
      }
    })
    .catch((err) => {
      console.log("api-err", err);
    });
};
export const causeListSuccess = (data) => {
  // console.log("dataaaa", data);
  return {
    type: actionTypes.CAUSES_SUPPORTED_SUCCESS,
    payload: data,
  };
};

export const getSkillsList = () => (dispatch) => {
  // console.log("in skilllist action", process.env.REACT_APP_API_BASE_URL);
  axios
    .get("/skill/list", {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    })
    .then((res) => {
      // console.log("api res", res);
      if (res && res.status) {
        dispatch(skillsListSuccess(res.data || []));
      }
    })
    .catch((err) => {
      console.log("api-err", err);
    });
};
export const skillsListSuccess = (data) => {
  // console.log("skillssss", data);
  return {
    type: actionTypes.SKILL_LIST_SUCCESS,
    payload: data,
  };
};

export const getTagMaster = () => (dispatch) => {
  // console.log("in tagmaster action", process.env.REACT_APP_API_BASE_URL);
  axios
    .get("/tag/list", {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    })
    .then((res) => {
      // console.log("api res", res);
      if (res && res.status) {
        dispatch(tagMasterListSuccess(res.data || []));
      }
    })
    .catch((err) => {
      console.log("api-err", err);
    });
};
export const tagMasterListSuccess = (data) => {
  // console.log("tagMaster", data);
  return {
    type: actionTypes.TAG_MASTER_LIST,
    payload: data,
  };
};

export const getAllTaskList = (data) => (dispatch) => {
  console.log("filters ::::::::::::::: ", data)
  axios.post("/task/taskList", data).then((res) => {
    if (res) {
      dispatch(TaskListSuccess(res.data || []));
    }
  });
};
export const TaskListSuccess = (data) => {
  // console.log("TaskListSuccess", data);
  return {
    type: actionTypes.TASK_LIST_SUCCESS,
    payload: data,
  };
};

export const getAllUsersList = (data) => (dispatch) => {
  console.log("filters :", data);
  axios.post("/user/list", data).then((res) => {
    if (res) {
      dispatch(UserListSuccess(res.data || []));
    }
  });
};
export const UserListSuccess = (data) => {
  // console.log("TaskListSuccess", data);
  return {
    type: actionTypes.USER_LIST_SUCCESS,
    payload: data,
  };
};
