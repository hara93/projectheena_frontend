import { actionTypes } from './types';

const initialState = {
  projectdata: undefined,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PROJECTDATA:
      return { ...state, projectdata: { ...action.payload } };
    default:
      return state;
  }
};

export default reducer;