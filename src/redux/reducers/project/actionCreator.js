import { actionTypes } from './types';

export const setProjectData = (projectData) => {
  return { type: actionTypes.PROJECTDATA, payload: projectData };
};
