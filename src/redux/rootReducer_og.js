import { all } from 'redux-saga/effects';
import { combineReducers } from 'redux';

import * as auth from '../app/modules/Auth/_redux/authRedux';

import phAuth from './reducers/auth';

const Branding = [
  {
    baseColor: '#4caf4f',
    baseColorDark: 'rgba($baseColor, 0.88)',
    titleGray: '#676a6c',
    lightGray: 'gainsboro',
    white: 'white',
    logo: '',
    // baseFontFamily: '"Prompt", sans-serif;',
    // fontSize: "13px",
  },
];

export const rootReducer = combineReducers({
  auth: auth.reducer,
  phAuth: phAuth,
  branding: Branding,
});

export function* rootSaga() {
  yield all([auth.saga()]);
}
