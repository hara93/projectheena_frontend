import { combineReducers } from "redux";
import * as auth from "../app/modules/Auth/_redux/authRedux";
import phAuth from "./reducers/auth";
import project from "./reducers/project";
import common from "./reducers/common";
export const rootReducer = combineReducers({
  auth: auth.reducer,
  phAuth,
  project,
  common,
});
